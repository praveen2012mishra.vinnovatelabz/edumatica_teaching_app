import axios from 'axios';
import { Assignment, AssignedAssignment, saveAssignmentReq, AssignmentRes, AssignedBatch, AssignedAssignmentRes, StudentAssignmentRes, AssignmentTaskPageRes, studentRowData, batchesRowData, answerDoc, studentDetails, saveFeedbackReq, feedbackRes, studentAnswersRes } from '../contracts/assignment';
import {
  Assignment as AssignmentNew,
  StudentAssignment as StudentAssignmentNew,
  AssignedAssignment as AssignedAssignmentNew,
  Document as DocumentNew,
  answerDoc as answerDocNew,
  AssignmentDocType as AssignmentDocTypeNew,
  TutorFeedback as TutorFeedbackNew
} from '../../assignments/contracts/assignment_interface'
import {
  CREATE_ASSIGNMENT,
  GET_STUDENT_ASSIGNMENTS,
  GET_UPLOAD_URL_ASSIGNMENT_DOC,
  GET_DOWNLOAD_URL_ASSIGNMENT_DOC,
  GET_ASSIGNMENTS,
  ASSIGN_ASSIGNMENT,
  GET_ASSIGNED_ASSIGNMENTS,
  SAVE_ASSIGNMENT_ANSWER,
  SAVE_ASSIGNMENT_FEEDBACK,
  GET_ASSIGNED_BATCHES,
  GET_ASSIGNED_STUDENTS,
  GET_ASSIGNMENT,
  GET_ASSIGNMENT_TASK,
  CONVERT_IMAGES,
  GET_STUDENT_ANSWERS,
  GET_FEEDBACK,
  DELETE_ASSIGNED_ASSIGNMENT,
  ZIP_FILES_AND_UPLOAD,
  ASSIGNMENT_ANALYTICS,
  DELETE_ASSIGNMENT,
  GET_ASSIGNMENT_BYID,
  UPDATE_ASSIGNMENT,
  ZIP_FILES_AND_UPLOAD_NEW,
  ASSIGNMENT_COMMON_NEW,
  STUDENT_ASSIGNMENT_NEW,
  ASSIGNED_ASSIGNMENT_NEW,
  ASSIGNMENT_DOWNLOAD_URL_NEW,
  ASSIGNMENT_SUBMIT_NEW,
  CONVERT_TOPDF_NEW,
  ASSIGNMENT_DRAFT_NEW,
  ASSIGNMENT_UPLOAD_URL_NEW,
  ASSIGNMENT_EVALUATE_NEW
} from './routes';

interface Document {
  fileName: string;
  contentType: string;
  uuid?: string;
  docType: string;
}

export const addAssignment = async(assignment: Assignment) => {
  const response = await axios.post(CREATE_ASSIGNMENT, assignment);
  return response.data;
}

export const getAssignments = async(params: {
  tutorId?: string,
}) => {
  const response = await axios.get<{assignments: AssignmentRes[]}>(GET_ASSIGNMENTS, { params });
  return response.data.assignments;
}

export const assignAssignment = async(assignedAssignments: AssignedAssignment[]) => {
  const response = await axios.post(ASSIGN_ASSIGNMENT, assignedAssignments);
  return response.data;
}

export const getAssignedAssignments = async(params: {
  tutorId?: string,
}) => {
  const response = await axios.get<{assignedAssignments: AssignedAssignmentRes[]}>(GET_ASSIGNED_ASSIGNMENTS, { params });
  return response.data.assignedAssignments;
}

export const getAssignedBatches = async(params: {
  assignmentId: string
}) => {
  const response = await axios.get<{
    assignmentRes: AssignmentRes,
    assignedBatches: AssignedBatch[]
  }>(GET_ASSIGNED_BATCHES, { params })
  return response.data
}

export const getAssignedStudents = async(params: {
  assignedId: string
}) => {
  const response = await axios.get<{
    assignedStudents: studentDetails[],
    assignedDetails: batchesRowData
  }>(GET_ASSIGNED_STUDENTS, { params })
  return response.data
}

export const getAssignment = async(params: {
  assignedId: string
}) => {
  const response = await axios.get<{assignmentRes: AssignedAssignmentRes}>(GET_ASSIGNMENT, { params })
  return response.data.assignmentRes
}

export const getStudentAnswers = async(params: {
  assignedId: string,
  groupIndex: number,
  studentIndex: number
}) => {
  const response = await axios.get<{ 
    studentAnswersRes: studentAnswersRes,
    assignmentRes: AssignmentRes
  }>(GET_STUDENT_ANSWERS, { params })
  return response.data
}

export const deleteAssignedAssignment = async(params: { assignedId: string }) => {
  const response = await axios.delete<{ assignedBatches: AssignedBatch[] }>(DELETE_ASSIGNED_ASSIGNMENT, {params})
  return response.data.assignedBatches
}

export const getStudentAssignments = async(params: {
  batches: string[],
}) => {
  const response = await axios.get<{studentAssignmentsRes: StudentAssignmentRes[]}>(GET_STUDENT_ASSIGNMENTS, { params });
  return response.data.studentAssignmentsRes;
}

export const getAssignmentTask = async(params: {
  assignedId: string,
}) => {
  const response = await axios.get<{
    assignmentTaskRes: AssignmentTaskPageRes
  }>(GET_ASSIGNMENT_TASK, { params })
  return response.data.assignmentTaskRes
}

export const fetchUploadUrlForDoc = async (params: Document) => {
  const response = await axios.get(GET_UPLOAD_URL_ASSIGNMENT_DOC, {params});
  return response.data;
};

export const uploadDoc = async (url: string, data: File, fileType: string) => {
  const response = await axios.put(url, data, {headers: {"Content-Type": fileType}});
  return response;
}

export const zipFilesAndUpload = async (data: FormData) => {
  const response = await axios.post(ZIP_FILES_AND_UPLOAD, data)
  return response.data
}

export const convertImages = async (data: FormData) => {
  const response = await axios.post(CONVERT_IMAGES, data, {"headers": {"Content-Type": "application/pdf"}})
  return response.data
}

export const fetchDownloadUrlForDoc = async (params: Document) => {
  const response = await axios.get(GET_DOWNLOAD_URL_ASSIGNMENT_DOC, {params});
  return response.data;
};

export const saveAssignmentAnswer = async (saveAssignmentReq: saveAssignmentReq) => {
  const response = await axios.post(SAVE_ASSIGNMENT_ANSWER, saveAssignmentReq);
  return response.data;
}

export const saveAssignmentFeedback = async (saveFeedback: saveFeedbackReq) => {
  const response = await axios.post(SAVE_ASSIGNMENT_FEEDBACK, saveFeedback);
  return response.data;
}

export const getFeedback = async (params: {assignedId: string}) => {
  const response = await axios.get<{ feedbackRes: feedbackRes }>(GET_FEEDBACK, { params })
  return response.data
}

export const feedbackAssignmentsAnalytics = async(assignmentId: string) => {
  const response = await axios.get(ASSIGNMENT_ANALYTICS, {params: {assignmentId}})
  return response.data.assignments
}
export const deleteAssignment = (assignmentId: string, isOrg: string) => {
  return axios.delete(DELETE_ASSIGNMENT, {
    params: {assignmentId, isOrg}
  });
};

export const getAssignmentDetailsById = async(assignmentId: string) => {
  const response = await axios.get(GET_ASSIGNMENT_BYID, {params: {assignmentId}})
  return response.data.assignments
}

export const updateAssignmentById = async (assignmentId: string, assignment: Assignment) => {
  return await axios.patch(UPDATE_ASSIGNMENT, {assignmentId: assignmentId, assignmentReq: assignment});
};

export const zipFilesAndUploadNew = async (data: FormData) => {
  const response = await axios.post(ZIP_FILES_AND_UPLOAD_NEW, data)
  return response.data
}

export const fetchDownloadUrlForDocNew = async (params: DocumentNew) => {
  const response = await axios.get(ASSIGNMENT_DOWNLOAD_URL_NEW, {params});
  return response.data;
};

export const fetchUploadUrlForDocNew = async (params: Document) => {
  const response = await axios.get(ASSIGNMENT_UPLOAD_URL_NEW, {params});
  return response.data;
};

export const createAssignmentNew = async (assignment: AssignmentNew) => {
  const response = await axios.post(ASSIGNMENT_COMMON_NEW, assignment)
  return response.data
}

export const createAssignmentDraftNew = async (assignment: AssignmentNew) => {
  const response = await axios.post(ASSIGNMENT_DRAFT_NEW, assignment)
  return response.data
}

export const fetchAssignmentNew = async (assignmentId: string | undefined, tutorId: string | undefined) => {
  const response = await axios.get<{assignments: AssignmentNew[]}>(ASSIGNMENT_COMMON_NEW, {params: {assignmentId: assignmentId, tutorId: tutorId}})
  return response.data.assignments
}

export const fetchStudentAssignmentNew = async (studentId: string) => {
  const response = await axios.get<{assignments: StudentAssignmentNew[]}>(STUDENT_ASSIGNMENT_NEW, {params: {studentId: studentId}})
  return response.data.assignments
}

export const fetchAssignedAssignmentNew = async (studentId: string | undefined, assignmentId: string | undefined) => {
  const response = await axios.get<{assignments: AssignedAssignmentNew[]}>(ASSIGNED_ASSIGNMENT_NEW, {params: {studentId: studentId, assignmentId: assignmentId}})
  return response.data.assignments
}

export const submitAssignmentNew = async (studentId: string, assignmentId: string, answerDocs: answerDocNew) => {
  const response = await axios.post(ASSIGNMENT_SUBMIT_NEW, {studentId: studentId, assignmentId: assignmentId, answerDocs: answerDocs})
  return response.data
}

export const evaluateAssignmentNew = async (studentId: string, assignmentId: string, tutorfeedback: TutorFeedbackNew) => {
  const response = await axios.post(ASSIGNMENT_EVALUATE_NEW, {studentId: studentId, assignmentId: assignmentId, tutorfeedback: tutorfeedback})
  return response.data
}

export const convertToPdfNew = async (data: FormData, doctype: AssignmentDocTypeNew) => {
  const response = await axios.post(CONVERT_TOPDF_NEW, data, {"headers": 
    {"Content-Type": "application/pdf", "doctype" : doctype}
  })
  return response.data
}
