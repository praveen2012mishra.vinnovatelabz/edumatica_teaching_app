import axios from 'axios';
import { GET_STUDENT, UPDATE_TUTOR, UPDATE_STUDENT, ONBOARD_STUDENT, ONBOARD_PARENT, UPDATE_PARENT, DELETE_STUDENT, SET_CHANGE_PASSWORD, COURSE_COUNT_CHECK_TUTOR, ONBOARD_TUTOR } from './routes';
import { Student, Tutor, Parent, StudentResponse } from '../contracts/user';
import { Course } from '../../academics/contracts/course';

export const fetchStudent = async () => {
  const response = await axios.get<StudentResponse>(GET_STUDENT);

  return response.data.student;
};

export const updateTutor = async (tutor: Tutor) => {
  return axios.put(UPDATE_TUTOR, tutor);
};

export const onboardTutor = async (tutor: Tutor) => {
  return axios.put(ONBOARD_TUTOR, tutor);
};

export const checkCourseCount = async (courses: Course[]) => {
  return axios.post(COURSE_COUNT_CHECK_TUTOR, {courseDetails: courses});
};

export const updateStudent = async (student: Student) => {
  return axios.put(UPDATE_STUDENT, {student});
};

export const onboardStudent = async (student: Student) => {
  return axios.put(ONBOARD_STUDENT, {student});
};

export const updateParent = async (parent: Parent) => {
  return axios.put(UPDATE_PARENT, parent);
};

export const onboardParent = async (parent: Parent) => {
  return axios.put(ONBOARD_PARENT, parent);
};

export const deleteStudents = async (students: string[]) => {
  return axios.delete<{ removedStudentList: string[] }>(DELETE_STUDENT, {
    data : { studentList: students }
  });
};

export const setChangePassword = async (currPassword: string, newPassword: string) => {
  return axios.post(SET_CHANGE_PASSWORD, {currPassword, newPassword})
}