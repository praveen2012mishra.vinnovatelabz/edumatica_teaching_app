import axios from 'axios';
import {
  GET_TUTOR_SUBJECTS,
  GET_TUTOR_STUDENTS,
  GET_STUDENT,
  GET_TUTOR,
  GET_PARENT,
  ADD_TUTOR_STUDENTS,
  GET_ASSESSMENT,
  UPDATE_TUTOR_STUDENTS,
    GET_PARENT_FOR_TUTOR,
    ADMINTUTOR_VIDEO_KYC,
  ADMINTUTOR_VIDEO_KYC_UPLOAD_LINK,
  ADMINTUTOR_REDO_VIDEO_KYC
} from './routes';
import { Student, TutorResponse, Parent } from '../../common/contracts/user';
import { Course } from '../../academics/contracts/course';
import {CourseListResponse} from "../contracts/academic"
import {Assessment} from "../../assessment/contracts/assessment_interface";
import { getAssessmentsResponse,getAssessmentResponse,getParentResponse } from '../contracts/tutor';



export const getCoursesOfTutor = async () => {
  const response = await axios.get<CourseListResponse>(GET_TUTOR_SUBJECTS);
  return response.data.courseList;
};



export const getAssessments = async () => {
  const response = await axios.get<getAssessmentsResponse>(GET_ASSESSMENT)
  return response.data.assessmentList
}

export const getAssessment  = async (QUERY:string) =>{
  const response = await axios.get<getAssessmentResponse>(GET_ASSESSMENT+QUERY)
  return response.data.assessment
}

export const addStudentsOfTutor = async (
  students: Student[]
) => {
  const response = await axios.post<{addedStudentList: string[]}>(
    ADD_TUTOR_STUDENTS,
    { studentList: students }
  );

  return response.data;
};

export const updateStudentsOfTutor = async (
  student: Student
) => {
  const response = await axios.put<Student>(
    UPDATE_TUTOR_STUDENTS,
    { student: student }
  );

  return response.data;
};

export const getStudentsOfTutor = async () => {
  const response = await axios.get<Student[]>(GET_TUTOR_STUDENTS);

  return response.data;
};

export const getStudent = async () => {
  const response = await axios.get(GET_STUDENT);
  return response.data.student;
};

export const getParent = async () => {
  const response = await axios.get(GET_PARENT);
  return response.data.parent;
};

export const getTutor = async () => {
  const response = await axios.get<TutorResponse>(GET_TUTOR);

  return response.data.tutor;
};

export const getParentForTutor = async(mobileNo:string) =>{
  const response  = await axios.get<getParentResponse>(GET_PARENT_FOR_TUTOR,{params:{mobileNo:mobileNo}})
  return response.data;
}

export const getAdminTutorVideoKycToken = async() => {
  const response = await axios.get(ADMINTUTOR_VIDEO_KYC);

  return response.data.token;
}

export const getAdminTutorVideoKycUploadLink = async(fileType:string) => {
  const response = await axios.post(ADMINTUTOR_VIDEO_KYC_UPLOAD_LINK, {
    fileType
  });

  return response.data;
}

export const uploadAdminTutorVideoKycUuid = async(fileName: string, uuid: string) => {
  const response = await axios.post(ADMINTUTOR_VIDEO_KYC, {
    fileName,
    uuid
  });

  return response.data.videoKycLink;
}

export const getAdminTutorVideoKycDownloadLink = async(uuid:string) => {
  const response = await axios.get(ADMINTUTOR_VIDEO_KYC_UPLOAD_LINK, {params:{uuid}});

  return response.data.videoKycLink;
}

export const redoAdminTutorVideoKyc = async() => {
  const response = await axios.get(ADMINTUTOR_REDO_VIDEO_KYC);

  return response.data;
}


