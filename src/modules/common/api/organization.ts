import axios from 'axios';
import {
  GET_ORGANIZATION,
  UPDATE_ORGANIZATION,  
  ADD_ORGANIZATION_TUTORS,
  GET_ORGANIZATION_TUTORS,
  DELETE_ORGANIZATION_TUTORS,
  UPDATE_ORG_TUTOR,
  ADD_ORG_STUDENTS,
  GET_ORG_STUDENTS,
  DELETE_ORG_STUDENT,
  GET_ORG_SUBJECTS,
  ORG_VIDEO_KYC,
  ORG_VIDEO_KYC_UPLOAD_LINK,
  ORG_REDO_VIDEO_KYC,
  COURSE_COUNT_CHECK_ORGANIZATION,
  ONBOARD_ORGANIZATION,
  UPDATE_ORG_STUDENTS
} from './routes';
import { Organization, TutorsResponse, OrgResponse, Tutor, StudentsResponse, Student, TutorIdsResponse, StudentIdsResponse } from '../contracts/user';
import {
  CourseListResponse
} from '../contracts/academic';
import { Course } from '../../academics/contracts/course';
import { setAuthUser } from '../../auth/store/actions';

export const getOrganization = async () => {
  const response = await axios.get(GET_ORGANIZATION);
  return response.data.organization;
};

export const updateOrganization = async (organization: Organization) => {
  const response = await axios.put<OrgResponse>(UPDATE_ORGANIZATION, organization);
  // setAuthUser(response.data.newOrgData)
  return response
};

export const onboardOrganization = async (organization: Organization) => {
  return axios.put(ONBOARD_ORGANIZATION, organization);
};

export const checkCourseCount = async (courses: Course[]) => {
  return axios.post(COURSE_COUNT_CHECK_ORGANIZATION, {courseDetails: courses});
};

export const addTutorsForOrganization = async (
  tutors: Tutor[]
) => {
  const response = await axios.post< TutorIdsResponse >(ADD_ORGANIZATION_TUTORS, { tutorList: tutors });
  return response.data;
};

export const fetchOrgTutorsList = async () => {
  const response = await axios.get<TutorsResponse>(GET_ORGANIZATION_TUTORS);
  return response.data.tutorList;
};

export const deleteOrganizationTutors = async (tutors: string[]) => {
  return axios.delete(DELETE_ORGANIZATION_TUTORS, {
    data : { tutorList: tutors }
  });
};

export const updateOrgTutor = async(tutor: any) => {
  return axios.put(UPDATE_ORG_TUTOR, tutor);
}

export const getOrgStudentsList = async () => {
  const response = await axios.get<StudentsResponse>(GET_ORG_STUDENTS);
  return response.data.studentList;
};

export const addStudentsOfOrganization = async (
  students: Student[]
) => {
  const response = await axios.post<StudentIdsResponse>(
    ADD_ORG_STUDENTS,
    { studentList: students }
  );
  return response.data;
};

export const updateStudentsOfOrganization = async (
  student: Student
) => {
  const response = await axios.put<Student>(
    UPDATE_ORG_STUDENTS,
    { student: student }
  );

  return response.data;
};

export const deleteStudentsOfOrganization = async (students: string[]) => {
  return axios.delete<{ removedStudentList: string[] }>(DELETE_ORG_STUDENT, {
    data : { studentList: students }
  });
};

export const fetchOrgCoursesList = async () => {
  const response = await axios.get<CourseListResponse>(GET_ORG_SUBJECTS);

  return response.data.courseList;
};

export const getOrgVideoKycToken = async() => {
  const response = await axios.get(ORG_VIDEO_KYC);

  return response.data.token;
}

export const getOrgVideoKycUploadLink = async(fileType:string) => {
  const response = await axios.post(ORG_VIDEO_KYC_UPLOAD_LINK, {
    fileType
  });

  return response.data;
}

export const uploadOrgVideoKycUuid = async(fileName: string, uuid: string) => {
  const response = await axios.post(ORG_VIDEO_KYC, {
    fileName,
    uuid
  });

  return response.data.videoKycLink;
}


export const getOrgVideoKycDownloadLink = async(uuid:string) => {
  const response = await axios.get(ORG_VIDEO_KYC_UPLOAD_LINK, {params:{uuid}});

  return response.data.videoKycLink;
}

export const redoOrgVideoKyc = async() => {
  const response = await axios.get(ORG_REDO_VIDEO_KYC);

  return response.data;
}