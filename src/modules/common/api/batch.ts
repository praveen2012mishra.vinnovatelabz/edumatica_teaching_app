import axios from 'axios';
import {
  CREATE_ORG_BATCH,
  DELETE_ORG_BATCH,
  GET_ORG_BATCHES,
  UPDATE_ORG_BATCH,
  GET_ORG_BATCH_DETAILS
} from './routes';
import { Batch } from '../../academics/contracts/batch';
import {  
  FetchBatchListResponse,
  FetchBatchDetailsResponse,
  FetchBatchDetailsRequest
} from '../contracts/academic';


export const createOrgBatch = async (batch: Batch) => {
  const response = await axios.post(CREATE_ORG_BATCH, batch);
  return response.data;
};

export const deleteOrgBatch = (batch: Batch) => {
  return axios.delete(DELETE_ORG_BATCH, {
    data: batch,
  });
};

export const fetchOrgBatchDetails = async (params: FetchBatchDetailsRequest) => {
  const response = await axios.get<FetchBatchDetailsResponse>(
    GET_ORG_BATCH_DETAILS,
    { params }
  );

  return response.data.batch;
};

export const fetchOrgBatchesList = async () => {
  const response = await axios.get<FetchBatchListResponse>(GET_ORG_BATCHES, {params: {usertype: 'organization'}});
  return response.data.batchList;
};

export const fetchStudentBatchesList = async (studentId: string | null) => {
  const response = await axios.get<FetchBatchListResponse>(GET_ORG_BATCHES, {params: {usertype: 'student', studentId: studentId}});
  return response.data.batchList;
};

export const updateOrgBatch = (batch: Batch) => {
  return axios.put(UPDATE_ORG_BATCH, batch)
};