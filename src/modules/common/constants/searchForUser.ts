import LocalLibraryRoundedIcon from '@material-ui/icons/LocalLibraryRounded';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LibraryBooksRoundedIcon from '@material-ui/icons/LibraryBooksRounded';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DescriptionRoundedIcon from '@material-ui/icons/DescriptionRounded';
import PersonPinRoundedIcon from '@material-ui/icons/PersonPinRounded';
import GroupRoundedIcon from '@material-ui/icons/GroupRounded';
import PaymentRoundedIcon from '@material-ui/icons/PaymentRounded';
import ManageAccountIcon from '../../../assets/svgs/manage_accounts.svg';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import DoneIcon from '@material-ui/icons/Done';

interface RouteList {
  name: string;
  Link: string;
  type: string;
  icon?: any;
  listno?: number;
}

export const OrganizationSearch: RouteList[] = [
  {name: 'Assessment Create', Link: '/profile/assessment/create', type: 'normal'},
  {name: 'Assessments', Link: '/profile/assessment', type: 'navbar', icon: AssignmentTurnedInIcon, listno: 7},
  {name: 'Assignments', Link: '/profile/assignment', type: 'navbar', icon: AssignmentRoundedIcon, listno: 8},
  {name: 'Batches Create', Link: '/profile/org/batches/create', type: 'normal'},
  {name: 'Batches', Link: '/profile/batches', type: 'navbar', icon: GroupRoundedIcon, listno: 4 },
  {name: 'Courses', Link: '/profile/courses', type: 'navbar', icon: LibraryBooksRoundedIcon, listno: 6 },
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'navbar', icon: DashboardIcon, listno: 1 },
  {name: 'Password Reset', Link: '/profile/security', type: 'normal'},
  {name: 'Payment', Link: '/payments/feestructure', type: 'normal'},
  {name: 'Personal Info', Link: '/profile/personal-information', type: 'normal'},
  {name: 'Schedules', Link: '/profile/schedules', type: 'navbar', icon: CalendarTodayIcon, listno: 5 },
  {name: 'Schedule Create', Link: '/profile/org/schedules/create', type: 'normal'},
  {name: 'Students Create', Link: '/profile/students/create', type: 'normal'},
  {name: 'Students', Link: '/profile/students', type: 'navbar', icon: LocalLibraryIcon, listno: 3 },
  {name: 'Tutors Create', Link: '/profile/tutors/create', type: 'normal'},
  {name: 'Tutors', Link: '/profile/tutors', type: 'navbar', icon: PersonPinRoundedIcon, listno: 2 },
]

export const TutorSearch: RouteList[] = [
  {name: 'Assessment Create', Link: '/profile/assessment/create', type: 'normal'},
  {name: 'Assessments', Link: '/profile/assessment', type: 'navbar', icon: AssignmentTurnedInIcon, listno: 6 },
  {name: 'Assignments', Link: '/profile/assignment', type: 'navbar', icon: AssignmentRoundedIcon, listno: 7 },
  {name: 'Batches Create', Link: '/profile/tutor/batches/create', type: 'normal'},
  {name: 'Batches', Link: '/profile/batches', type: 'navbar', icon: GroupRoundedIcon, listno: 3 },
  {name: 'Courses', Link: '/profile/courses', type: 'navbar', icon: LibraryBooksRoundedIcon, listno: 5 },
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'navbar', icon: DashboardIcon, listno: 1 },
  {name: 'Password Reset', Link: '/profile/security', type: 'normal'},
  {name: 'Payment', Link: '/payments/feestructure', type: 'normal'},
  {name: 'Personal Info', Link: '/profile/personal-information', type: 'normal'},
  {name: 'Schedules', Link: '/profile/schedules', type: 'navbar', icon: CalendarTodayIcon, listno: 4 },
  {name: 'Schedule Create', Link: '/profile/tutor/schedules/create', type: 'normal'},
  {name: 'Students Create', Link: '/profile/students/create', type: 'normal'},
  {name: 'Students', Link: '/profile/students', type: 'navbar', icon: LocalLibraryIcon, listno: 2 },
]

export const OrganizationTutorSearch: RouteList[] = [
  {name: 'Assessment Create', Link: '/profile/assessment/create', type: 'normal'},
  {name: 'Assessments', Link: '/profile/assessment', type: 'navbar', icon: AssignmentTurnedInIcon, listno: 4 },
  {name: 'Assignments', Link: '/profile/assignment', type: 'navbar', icon: AssignmentRoundedIcon, listno: 5 },
  {name: 'Batches', Link: '/profile/batches', type: 'navbar', icon: GroupRoundedIcon, listno: 2 },
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'navbar', icon: DashboardIcon, listno: 1 },
  {name: 'Password Reset', Link: '/profile/security', type: 'normal'},
  {name: 'Personal Info', Link: '/profile/personal-information', type: 'normal'},
  {name: 'Schedules', Link: '/profile/schedules', type: 'navbar', icon: CalendarTodayIcon, listno: 3 },
]

export const StudentSearch: RouteList[] = [
  {name: 'Assessments', Link: '/profile/assessments', type: 'navbar', icon: AssignmentTurnedInIcon, listno: 5 },
  {name: 'Assignments', Link: '/profile/assignment', type: 'navbar', icon: AssignmentRoundedIcon, listno: 6 },
  {name: 'Batches', Link: '/profile/batches', type: 'navbar', icon: GroupRoundedIcon, listno: 2 },
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'navbar', icon: DashboardIcon, listno: 1 },
  {name: 'Notes', Link: '/profile/Notes', type: 'navbar', icon: DescriptionRoundedIcon, listno: 4 },
  {name: 'Password Reset', Link: '/profile/security', type: 'normal'},
  {name: 'Payment', Link: '/payments/feestructure', type: 'normal'},
  {name: 'Personal Info', Link: '/profile/personal-information', type: 'normal'},
  {name: 'Schedules', Link: '/profile/schedules', type: 'navbar', icon: CalendarTodayIcon, listno: 3 },
]

export const ParentSearch: RouteList[] = [
  {name: 'Assignments', Link: '/profile/assignment', type: 'navbar', icon: AssignmentRoundedIcon, listno: 4 },
  {name: 'Batches', Link: '/profile/batches', type: 'navbar', icon: GroupRoundedIcon, listno: 2 },
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'navbar', icon: DashboardIcon, listno: 1 },
  {name: 'Password Reset', Link: '/profile/security', type: 'normal'},
  {name: 'Payment', Link: '/payments/feestructure', type: 'normal'},
  {name: 'Personal Info', Link: '/profile/personal-information', type: 'normal'},
  {name: 'Schedules', Link: '/profile/schedules', type: 'navbar', icon: CalendarTodayIcon, listno: 3 },
]

export const AdminSearch: RouteList[] = [
  {name: 'Dashboard', Link: '/profile/dashboard', type: 'normal', icon: DashboardIcon, listno: 1 },
]