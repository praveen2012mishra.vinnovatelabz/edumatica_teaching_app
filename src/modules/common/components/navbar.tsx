import React, { FunctionComponent, useState, useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { 
  setAuthToken, setAuthUser, setAuthUserPermissions, 
  setAuthUserRole, unsetAuthUser, setRefreshToken, setChildrenIfParent 
} from '../../auth/store/actions';
import { Link as RouterLink, Redirect , useLocation} from 'react-router-dom';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Badge,
  Box,
  Button,
  ClickAwayListener,
  Divider,
  Grow,
  IconButton,
  InputBase,
  Link,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Toolbar,
  Tooltip,
  Typography,
  Avatar,
  TextField
} from '@material-ui/core';

import { Role } from '../enums/role';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  OrganizationSearch, TutorSearch, OrganizationTutorSearch,
  StudentSearch, ParentSearch, AdminSearch
} from '../constants/searchForUser'

import {
  // AccountCircle as AccountCircleIcon,
  ExpandMore as ExpandMoreIcon,
  Menu as MenuIcon,
  Search as SearchIcon
} from '@material-ui/icons';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
// import DashboardIcon from '@material-ui/icons/DashboardRounded';
import LocalLibraryRoundedIcon from '@material-ui/icons/LocalLibraryRounded';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import AssessmentIcon from '@material-ui/icons/Assessment';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LibraryBooksRoundedIcon from '@material-ui/icons/LibraryBooksRounded';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DescriptionRoundedIcon from '@material-ui/icons/DescriptionRounded';
import PersonPinRoundedIcon from '@material-ui/icons/PersonPinRounded';
import GroupRoundedIcon from '@material-ui/icons/GroupRounded';
import PaymentRoundedIcon from '@material-ui/icons/PaymentRounded';
import ManageAccountIcon from '../../../assets/svgs/manage_accounts.svg';
import DoneIcon from '@material-ui/icons/Done';
import { RootState } from '../../../store';
import {
  isAdminTutor,
  isStudent,
  isOrganization,
  isOrgTutor,
  isAdmin,
  isParent
} from '../helpers';
import { getStudent, getTutor, getParent } from '../../common/api/tutor';
import { getOrganization } from '../../common/api/organization';
import { getAdmin } from '../../common/api/admin';
import { User, Children, Student } from '../../common/contracts/user';
import Logo from '../../../assets/svgs/book.svg';
import EdumaticaLogo from '../../../assets/images/edumaticaLogo.png'
import BellIcon from '../../../assets/svgs/bell.svg';
import ChildIcon from '../../../assets/svgs/childIcon.svg';
import Scrollbars from 'react-custom-scrollbars';

import ProfileImage from '../../dashboard/containers/profile_image';
import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../common/helpers';
import EventEmitter from '../../../EventEmitter';
import { fontOptions } from '../../../theme';
import clsx from 'clsx';
import { fetchParentStudentsList } from '../../common/api/academics';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    navigationBar: {
      flex: 2,
      textAlign: 'center',
      marginTop: '20px',
      '& > * + *': {
        marginLeft: theme.spacing(2)
      }
    },

    userDropdownTextContainer: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'flex'
      },
      fontWeight: fontOptions.weight.bolder,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '19px',
      textAlign: 'right',
      color: '#22C5F8'
    },
    search: {
      position: 'relative',
      padding: theme.spacing(1, 1),
      borderRadius: '8px',
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto'
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: '#F5F5F7'
    },
    inputRoot: {
      color: 'inherit',
      width: '300px',
      height: '40px'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch'
      }
    },
    navIcon: {
      height: '25px',
      width: '25px'
    },
    bellContainer: {
      paddingTop: '4px',
      marginRight: '15px'
    },
    bellIcon: {
      background: 'rgba(102, 102, 102, 0.2)',
      borderRadius: '100%',
      padding: '6px 8px'
    },
    expandIcon: {
      color: '#666666',
      marginLeft: '10px'
    },
    toolbar: {
      justifyContent: 'space-between',
      minHeight: '72px'
    },
    menuLink: {
      fontWeight: fontOptions.weight.bolder,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#666666',
      padding: '15px 20px'
    },
    selectedMenuLink: {
      display: 'inline-block',
      paddingBottom: '20px',
      width: '10%',
      textAlign: 'center',

      '&:hover': {
        background: 'rgba(102, 102, 102, 0.2)',
        borderRadius: '5px'
      }
    },
    selectedMenuLinkBorder: {
      display: 'inline-block',
      borderBottom: '3px solid #4C8BF5',
      paddingBottom: '20px',
      width: '10%',
      textAlign: 'center'
    },
    otherAcc: {
      background: '#F5F5F5',
    },
    activeIcon: {
      color: '#4C8BF5', 
      fontFamily:fontOptions.family,fontSize: fontOptions.size.mediumPlus
    },
    inActiveIcon: {
      color: '#666666', 
      fontFamily:fontOptions.family,fontSize: fontOptions.size.mediumPlus
    },
    activeLink: {
      color: '#4C8BF5', 
      fontFamily:fontOptions.family,fontSize: fontOptions.size.xSmall,
      fontWeight: fontOptions.weight.normal,
    },
    inActiveLink: {
      color: '#666666', 
      fontFamily:fontOptions.family,fontSize: fontOptions.size.xSmall,
      fontWeight: fontOptions.weight.normal,
    },
    logo: {
      width: '150x',
      height: '43px',
      marginTop: '5px'
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
  })
);

interface Props {
  authUser: User;
  sidebarOpen?: boolean;
}

interface RoleSelect {
  _id: string;
  role: Role;
}

interface AllOwners {
  _id: string;
  ownerId: string;
  roles: RoleSelect[];
}

interface SearchLink {
  name: string;
  Link: string;
}

const Navbar: FunctionComponent<Props> = ({ authUser, sidebarOpen }) => {
  const classes = useStyles();
  const location = useLocation();
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [childSelOpen, setChildSelOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  const anchorRefChildSelection = React.useRef(null);
  const [notificationValue, setNotificationValue] = useState<number>(0)
  const [redirectTo, setRedirectTo] = useState('');
  const [roles, setRoles] = useState<AllOwners[]>([]);
  const [currentRole, setCurrentRole] = useState<string>('');
  const [currentOwner, setCurrentOwner] = useState<string>('');
  const [parentChildren, setParentChildren] = useState<Children>();
  const [pageSearch, setPageSearch] = useState<SearchLink>();

  useEffect(() => {
    if(pageSearch) {
      setRedirectTo(pageSearch.Link)
    }
  }, [pageSearch])
 
  useEffect(() => {
    const authUserRoles:string = localStorage.getItem("authUserRoles") as string;
    if(!authUserRoles) {
      setRedirectTo(`/login`);
    } else {
      const allRoles = JSON.parse(authUserRoles).allOwners.map((elem: any) => {
        return {
          _id: elem._id,
          ownerId: elem.ownerId,
          roles: elem.roles.map((role: any) => {
            return {
              _id: role._id,
              role: String(role.role.name).substring(5)
            }
          })
        }
      })
      
      const [currentOwner, currentRole] = JSON.parse(authUserRoles).ownerRoleCombi.split('|||');

      const ownerObject = JSON.parse(authUserRoles).allOwners.find((data: any) => data._id === currentOwner)

      setRoles(allRoles);
      setCurrentRole(currentRole);
      setCurrentOwner(ownerObject.ownerId);
    }
    // eslint-disable-next-line
  }, [localStorage.getItem("authUserRoles")]);

  useEffect(() => {
    const authUserRoles:string = localStorage.getItem("authUserRoles") as string;
    const parentChildren:string = localStorage.getItem("parentChildren") as string;
    if(!authUserRoles) {
      setRedirectTo(`/login`);
    } else {
      const allRoles = JSON.parse(authUserRoles).allOwners.map((elem: any) => {
        return {
          _id: elem._id,
          ownerId: elem.ownerId,
          roles: elem.roles.map((role: any) => {
            return {
              _id: role._id,
              role: String(role.role.name).substring(5)
            }
          })
        }
      })
      const [currentOwner, currentRole] = JSON.parse(authUserRoles).ownerRoleCombi.split('|||');
      const currentRolename = allRoles.map((data: any) => data.roles).flat().find((data: any) => data._id === currentRole).role;

      if((currentRolename === 'PARENT') && parentChildren) {
        setParentChildren(JSON.parse(parentChildren))
      }
    }
    // eslint-disable-next-line
  }, [localStorage.getItem("parentChildren")]);

  EventEmitter.on("notification", function(payload:any) {
    setNotificationValue(notificationValue+1)
  })

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleToggleChildSel = () => {
    setChildSelOpen((prevChildSelOpen) => !prevChildSelOpen);
  };

  const handleCloseChildSel = () => {
    setChildSelOpen(false);
  };
  
  const logoutHandle = () => {
    dispatch(setAuthToken(''));
    dispatch(setRefreshToken(''))
    dispatch(setAuthUser({}));
    dispatch(setAuthUserRole(''));
    dispatch(setChildrenIfParent({}));
    dispatch(unsetAuthUser());
    localStorage.clear(); 
  }

  const handleSelectRole = async (role: string) => {
    try {
      const authUserRoles:string = localStorage.getItem("authUserRoles") as string;
      const {allOwners} = JSON.parse(authUserRoles)
      const username = authUser.mobileNo;
      const accessToken = localStorage.getItem("accessToken");
      const refreshToken = localStorage.getItem("refreshToken"); 
      const [selectedOwner, selectedRole] = role.split('|||')
      
      dispatch(unsetAuthUser());
      localStorage.clear(); 

      if(accessToken && refreshToken) {
        const currentOwner = allOwners.find((own: any) => own._id === selectedOwner)
        const currentRole = currentOwner.roles.find((rol: any) => rol._id === selectedRole);
        const currentRolename = currentRole.role.name;
        const ownerRoleCombi = currentOwner._id + '|||' + currentRole._id;

        dispatch(setAuthUserRole(currentRolename));
        dispatch(setAuthUserPermissions(currentRole.permissions));
        dispatch(setAuthToken(accessToken));
        dispatch(setRefreshToken(refreshToken));
        dispatch(setChildrenIfParent({}));
        localStorage.setItem('authUserRoles', JSON.stringify({allOwners, ownerRoleCombi}));
        const redPath  = (location.pathname === '/profile/dashboard') ? `/profile/personal-information` : `/profile/dashboard`

        if (currentRolename === Role.ADMIN) {
          try {
            const admin = await getAdmin();
            const updatedUser = {...admin}
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(redPath);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
            setRedirectTo(`/login`);
          }
          return;
        } else if (currentRolename === Role.ORGANIZATION) {
          try {
            const organization = await getOrganization();
            const updatedUser = {...organization}
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(redPath);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
            dispatch(setAuthUser({ mobileNo: username}));
            setRedirectTo(`/profile/org/process/1`);
          }
          return;
        } else if (currentRolename === Role.TUTOR || currentRolename === Role.ORG_TUTOR) {
          try {
            const tutor = await getTutor();
            const updatedUser = {...tutor}
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(redPath);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
            dispatch(setAuthUser({ mobileNo: username }));
            setRedirectTo(`/profile/process/1`);
          }
          return;
        } else if (currentRolename === Role.STUDENT) {
          const student = await getStudent();
          const updatedUser = {...student}
          if(student.roleStatus && (student.roleStatus === 'ACTIVE')) {
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(redPath);
          } else if(student.roleStatus && (student.roleStatus === 'DEACTIVATE')) {
            dispatch(setAuthUser({ mobileNo: username}));
            setRedirectTo(`/profile/deactivated`);
          } else {
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(`/profile/student/process/1`);
          }
        } else if (currentRolename === Role.PARENT) {
          const parent = await getParent();
          const updatedUser = {...parent}
          const children = await fetchParentStudentsList();
          const parentChildren = {
            current: children[0]._id,
            children: children
          }
          if(parent.roleStatus && (parent.roleStatus === 'ACTIVE')) {
            dispatch(setAuthUser(updatedUser));
            dispatch(setChildrenIfParent(parentChildren));
            setRedirectTo(redPath);
          } else if(parent.roleStatus && (parent.roleStatus === 'DEACTIVATE')) {
            dispatch(setAuthUser({ mobileNo: username }));
            setRedirectTo(`/profile/deactivated`);
          } else {
            dispatch(setAuthUser(updatedUser));
            dispatch(setChildrenIfParent(parentChildren));
            setRedirectTo(`/profile/parent/process/1`);
          }
        }
      } else {
        setRedirectTo(`/login`);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      setRedirectTo(`/login`);
    }
  };

  const handleSelectChild = (student: Student) => {
    if(parentChildren && parentChildren.current && student._id !== parentChildren.current) {
      const changeParentChildren = {...parentChildren};
      changeParentChildren.current = student._id as string
      dispatch(setChildrenIfParent(changeParentChildren));
    }
  }

  const navLeft = (userSearch: string) => {
    const permissiontosearch = (
      (((isOrganization(authUser) || isAdminTutor(authUser) || isOrgTutor(authUser) || isAdmin(authUser)) && authUser.emailId) || 
      ((isStudent(authUser) || isParent(authUser)) && authUser.roleStatus === 'ACTIVE')) ? true : false
    )

    let searchOptions;
    switch (userSearch) {
      case 'OrganizationSearch':
        searchOptions = OrganizationSearch;
        break;
      case 'TutorSearch':
        searchOptions = TutorSearch;
        break;
      case 'OrganizationTutorSearch':
        searchOptions = OrganizationTutorSearch;
        break;
      case 'StudentSearch':
        searchOptions = StudentSearch;
        break;
      case 'ParentSearch':
        searchOptions = ParentSearch;
        break;
      case 'AdminSearch':
        searchOptions = AdminSearch;
        break;
      default:
        searchOptions = AdminSearch;
    }

    return (
      <Box display="flex" alignItems="center" flex="1">
        <div >
          <a href="/profile/dashboard"><img className={classes.logo} src={EdumaticaLogo} alt="Logo" /></a>
        </div>
      </Box>
    );
  };

  const navRight = (name: string, mobile: string, user: string, userSearch: string) => {
    const permissiontosearch = (
      (((isOrganization(authUser) || isAdminTutor(authUser) || isOrgTutor(authUser) || isAdmin(authUser)) && authUser.emailId) || 
      ((isStudent(authUser) || isParent(authUser)) && authUser.roleStatus === 'ACTIVE')) ? true : false
    )
    let searchOptions;
    switch (userSearch) {
      case 'OrganizationSearch':
        searchOptions = OrganizationSearch;
        break;
      case 'TutorSearch':
        searchOptions = TutorSearch;
        break;
      case 'OrganizationTutorSearch':
        searchOptions = OrganizationTutorSearch;
        break;
      case 'StudentSearch':
        searchOptions = StudentSearch;
        break;
      case 'ParentSearch':
        searchOptions = ParentSearch;
        break;
      case 'AdminSearch':
        searchOptions = AdminSearch;
        break;
      default:
        searchOptions = AdminSearch;
    }
    return permissiontosearch && (
      <Box zIndex="9" display="flex" alignItems="center">
        <div className={classes.search}>
          <Autocomplete
            options={searchOptions}
            getOptionLabel={(option) => option.name}
            value={pageSearch}
            style={{ width: 200, backgroundColor: '#F5F5F7', margin: '5px 0' }}
            onChange={(e, node) => setPageSearch(node as SearchLink)}
            renderInput={(params) => (
              <TextField {...params} placeholder="Search..." variant="outlined"/>
            )}
          />
        </div>
        {(user === 'Parent' && window.location.pathname !== '/profile/batches/expand') &&
          <div>
            <Button
              ref={anchorRefChildSelection}
              aria-controls={childSelOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggleChildSel}
            >
              <Tooltip title="Child Selection">
                <img className={classes.bellIcon} src={ChildIcon} alt="Logo" />
              </Tooltip>
            </Button>
            <Popper
              open={childSelOpen}
              anchorEl={anchorRefChildSelection.current}
              transition
              disablePortal
            >
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{
                    transformOrigin:
                      placement === 'bottom' ? 'left top' : 'left bottom'
                  }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleCloseChildSel}>
                      <MenuList autoFocusItem={childSelOpen} id="menu-list-grow">
                        <Box display="flex" alignItems="center">
                          <Scrollbars autoHeight autoHeightMax="100px">
                            {parentChildren && parentChildren.current && parentChildren.children.map(child => {
                              return(
                                <MenuItem key={child._id} 
                                  onClick={() => {
                                    handleSelectChild(child)
                                    handleCloseChildSel()
                                  }}
                                >
                                  <Box display="flex" alignItems="center" marginY="7px">
                                    <Badge 
                                      badgeContent={<DoneIcon style={{fontSize: fontOptions.size.xSmall}}/>} 
                                      color="primary" invisible={child._id !== parentChildren.current}
                                    >
                                      <AccountCircleIcon />
                                    </Badge>
                                    <Box
                                      display="flex"
                                      flexDirection="column"
                                      fontWeight="400"
                                      fontSize="12px"
                                      lineHeight="19px"
                                      color="#666666"
                                      marginLeft="15px"
                                    >
                                      <Box
                                        fontWeight="400"
                                        fontSize="15px"
                                        lineHeight="20px"
                                        color="#F9BD33"
                                      >
                                        {child.studentName}
                                      </Box>
                                    </Box>
                                  </Box>
                                </MenuItem>
                              )
                            })}
                          </Scrollbars>
                        </Box>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        }
        <Link
          color="inherit"
          component={RouterLink}
          to={`/notifications`}
          className={classes.bellContainer}
        >
          <Badge badgeContent={notificationValue} color="error">
            <Tooltip title="Notification">
              <img onClick={() => {setNotificationValue(0)}} className={classes.bellIcon} src={BellIcon} alt="Logo" />
            </Tooltip>
          </Badge>
        </Link>
        <Button
          ref={anchorRef}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
        >
          <ProfileImage
            profile={authUser}
            profileUpdated={(profile) => dispatch(setAuthUser(profile))}
            name={name}
            size="small"
          />
          <div className={classes.userDropdownTextContainer}>
            <Box color="secondary" marginLeft="10px">
              {name && name.length > 0 ? name : mobile}
            </Box>
          </div>
          <ExpandMoreIcon className={classes.expandIcon} />
        </Button>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === 'bottom' ? 'left top' : 'left bottom'
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList autoFocusItem={open} id="menu-list-grow">
                    <MenuItem>
                      <Box display="flex" alignItems="center">
                        <ProfileImage
                          profile={authUser}
                          profileUpdated={(profile) =>
                            dispatch(setAuthUser(profile))
                          }
                          name={name}
                          size="small"
                        />
                        <Box
                          display="flex"
                          flexDirection="column"
                          fontWeight={fontOptions.weight.bolder}
                          fontFamily={fontOptions.family}
                          fontSize={fontOptions.size.small}
                          lineHeight="19px"
                          color="#666666"
                          marginLeft="15px"
                          marginBottom="10px"
                        >
                          <Box
                            fontWeight={fontOptions.weight.bolder}
                            fontFamily={fontOptions.family}
                            fontSize={fontOptions.size.medium}
                            lineHeight="25px"
                            color="#22C5F8"
                          >
                            {name && name.length > 0 ? name : mobile}
                          </Box>
                          <Box
                            fontWeight="400"
                            fontFamily={fontOptions.family}
                            fontSize="15px"
                            lineHeight="20px"
                            color="#F9BD33"
                          >
                            Owner: {currentOwner}
                          </Box>
                          <Box
                            fontWeight="400"
                            fontFamily={fontOptions.family}
                            fontSize="15px"
                            lineHeight="20px"
                            color="#666666"
                          >
                            Role: {user}
                          </Box>
                        </Box>
                      </Box>
                    </MenuItem>
                    <Divider variant="fullWidth" />
                    <Scrollbars autoHeight autoHeightMax="100px" className={classes.otherAcc}>
                      {roles?.map(element => {
                        return(
                          element.roles?.filter(elem => elem._id !== currentRole)?.map(elem => {
                            const roleName = elem.role.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ')
                            return (
                              <MenuItem key={element._id + '|||' + elem._id}
                                onClick={() => {
                                  eventTracker(GAEVENTCAT.dashboard, 'Account Switch', 'Switched to ' + elem.role);
                                  handleSelectRole(element._id + '|||' + elem._id)
                                }}
                              >
                                <Box display="flex" alignItems="center">
                                  <Avatar className={classes.small}>
                                    <AccountCircleIcon />
                                  </Avatar>
                                  <Box
                                    display="flex"
                                    flexDirection="column"
                                    fontWeight="400"
                                    fontSize="12px"
                                    lineHeight="19px"
                                    color="#666666"
                                    marginLeft="15px"
                                  >
                                    <Box
                                      fontWeight="400"
                                      fontSize="15px"
                                      lineHeight="20px"
                                      color="#F9BD33"
                                    >
                                      Owner: {element.ownerId}
                                    </Box>
                                    <Box
                                      fontWeight="400"
                                      fontSize="15px"
                                      lineHeight="20px"
                                      color="#666666"
                                    >
                                      Role: {roleName}
                                    </Box>
                                  </Box>
                                </Box>
                              </MenuItem>
                            )
                          })
                        )
                      })}
                    </Scrollbars>
                    <Divider variant="fullWidth" />
                    <MenuItem
                      className={classes.menuLink}
                      component={RouterLink}
                      to={`/profile/personal-information`}
                    >
                      Profile
                    </MenuItem>
                    <MenuItem
                      className={classes.menuLink}
                      component={RouterLink}
                      to={`/privacy-policy`}
                    >
                      Settings &amp; Privacy
                    </MenuItem>
                    <MenuItem
                      className={classes.menuLink}
                      component={RouterLink}
                      to={`/user-guide`}
                    >
                      Help &amp; Support
                    </MenuItem>
                    <MenuItem
                      className={classes.menuLink}
                      onClick={() => {
                        eventTracker(GAEVENTCAT.auth, `${user} Logout`,'Logout Success');
                        logoutHandle();
                      }}
                      component={RouterLink}
                      to={`/login`}
                    >
                      <div>
                        Logout
                      </div>
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Box>
    );
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if (isOrganization(authUser)) {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar>
          {navLeft('OrganizationSearch')}
          {navRight(
            authUser.organizationName,
            authUser.mobileNo,
            'Organization',
            'OrganizationSearch'
          )}
        </Toolbar>
      </AppBar>
    );
  }

  if (isAdmin(authUser)) {
    return (
      <AppBar position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar className={classes.toolbar}>
          {navLeft('AdminSearch')}

          <Box className={classes.navigationBar}>
            <Link
              color="inherit"
              onClick={() => {
                eventTracker(GAEVENTCAT.auth, 'Admin Logout', 'Logout Success');
                logoutHandle();
              }}
              component={RouterLink}
              to={`/login`}
            >
              Logout
            </Link>
            <IconButton edge="end" color="inherit">
              <MenuIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
    );
  }

  if (isAdminTutor(authUser)) {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar className={classes.toolbar}>
          {navLeft('TutorSearch')}

          {navRight(authUser.tutorName, authUser.mobileNo, 'Tutor', 'TutorSearch')}
        </Toolbar>
      </AppBar>
    );
  }

  if (isOrgTutor(authUser)) {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar className={classes.toolbar}>
          {navLeft('OrganizationTutorSearch')}

          {navRight(
            authUser.tutorName,
            authUser.mobileNo,
            'Organization Tutor',
            'OrganizationTutorSearch'
          )}
        </Toolbar>
      </AppBar>
    );
  }

  if (isStudent(authUser)) {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar className={classes.toolbar}>
          {navLeft('StudentSearch')}

          {navRight(authUser.studentName, authUser.mobileNo, 'Student', 'StudentSearch')}
        </Toolbar>
      </AppBar>
    );
  }

  if (isParent(authUser)) {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
        <Toolbar className={classes.toolbar}>
          {navLeft('ParentSearch')}

          {navRight(authUser.parentName, authUser.mobileNo, 'Parent', 'ParentSearch')}
        </Toolbar>
      </AppBar>
    );
  }

  return (
    <AppBar
        position="fixed"
        className={clsx(classes.appBar, {[classes.appBarShift]: sidebarOpen})}
        color="inherit"
        elevation={0}
      >
      <Toolbar>
        <img src={Logo} alt="Logo" />
        <div className={classes.grow} />

        <Box className={classes.navigationBar}>
          <Link
            color="inherit"
            component={RouterLink}
            to={`/profile/personal-information`}
          >
            Profile
          </Link>
          <Link color="inherit" component={RouterLink} to={`/login`}>
            Logout
          </Link>
          <IconButton edge="end" color="inherit">
            <MenuIcon />
          </IconButton>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
});

export default connect(mapStateToProps)(Navbar);