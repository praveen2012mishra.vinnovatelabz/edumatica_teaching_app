import React, { FunctionComponent, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { 
  List, ListItem, ListItemAvatar, Avatar, IconButton, ListItemText, ListItemSecondaryAction,
  Typography, Box, Accordion, AccordionDetails, AccordionSummary, Grid, CardMedia, Card, CardActions, Container, Backdrop
} from '@material-ui/core';
import { DropzoneArea, DropzoneAreaProps } from 'material-ui-dropzone';
//import { Box, Typography } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import VisibilityIcon from '@material-ui/icons/Visibility'
import pdfIcon from "../../../../../assets/images/pdf.png"
import docIcon from "../../../../../assets/images/doc.png"
import ImageIcon from '@material-ui/icons/Image';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import DescriptionIcon from '@material-ui/icons/Description';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import YouTubeIcon from '@material-ui/icons/YouTube';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import lodash from 'lodash'
import Button from '../../../components/form_elements/button'
import { ChapterContent } from '../../../../academics/contracts/chapter_content';
import { uniqBy } from 'lodash' 
import ReactPlayer from 'react-player';
import ImageGallery, { ReactImageGalleryItem } from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import PdfPreview from "./pdf_preview"
import CloseIcon from '@material-ui/icons/Close';
import PreviewDoc from './doc_preview'
import DocPreview from './doc_preview';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dropzoneRoot: {
      borderRadius: '500px',
      width: '100%',
      minHeight: 'auto',
      outline: 'none',
    },

    dropzoneTextContainerRoot: {
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'space-between',
      padding: '0 6px 0 15px',
    },

    dropzoneText: {
      fontSize: '18px',
      margin: '20px 0',
    },

    dropzoneIcon: {
      background:
        'linear-gradient(90deg, rgb(6, 88, 224) 2.53%, rgb(66, 133, 244) 100%)',
      borderRadius: '100%',
      color: '#FFF',
      padding: '10px',
    },

    contentHeading: {
      margin: '13px 0px',
      color: '#666666',
      fontSize: '16px'
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    imageClass: {
      width: "100%",
      height: "auto",
      maxHeight: "80vh",
      objectFit: "cover",
      overflow: "hidden",
      objectPosition: "center center"
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    secondHead: {
      color: '#666666',
      fontSize: '14px',
      margin: '0px 5px'
    },
    yMargin: {
      margin: '0px 5px',
      width: '100%'
    },
    media: {
        height: '230px' // 16:13
      },
      backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
      },
  
  })
);

interface SelectProp {
  label: string | undefined;
  value: string | undefined;
}

interface Props extends DropzoneAreaProps {
  files?: File[];
  onRemoveItem?: (filename:string) => any;
  onRemoveDropFile?: (file:File,index:Number) => any;
  contenttype?: string;
  fileStructure?: SelectProp[];
  data: ChapterContent[];
} 

interface BackDropContentProps {
    currentIndex : number
    contents: ChapterContent[];
    setBackDropHandleToggle : React.Dispatch<React.SetStateAction<boolean>>,
    setCurrentIndex : React.Dispatch<React.SetStateAction<number>>,
}
const BackDropContent : FunctionComponent<BackDropContentProps> = ({contents ,currentIndex, setBackDropHandleToggle,setCurrentIndex}) =>{
    const classes = useStyles()
    const file = contents[currentIndex].signedUrl
    const type = contents[currentIndex].contenttype
    return (
        <div>
            {
             (type =='image' || type=='video' || type =='pdf' || type=='document')? <React.Fragment>
                 <ImageGallery 
                 startIndex={currentIndex} 
                 renderCustomControls = {()=>{
                   return <IconButton
                   style ={{backgroundColor:'white'}} 
                   onClick = {()=>setBackDropHandleToggle(false)}
                   >
                     <CloseIcon />
                   </IconButton>
                 }}
                 showFullscreenButton ={type=='image' || type=='video'}
                 showPlayButton = {type=='image'}
                 items={contents.map(el=>{return {original:el.signedUrl as string,thumbnail:el.thumbnail, originalTitle: el.contentname}})} 
                 
                 renderItem = {type=='video'?(props: ReactImageGalleryItem)=>{
                   return <ReactPlayer
                   url={props.original}
                   controls
                   wrapper='div'
               />
                   
                 }: type == 'pdf' ? (props: ReactImageGalleryItem)=>{
                   return <PdfPreview 
                   file = {props.original} 
                   filename ={props.originalTitle as string}
                   />
                   
                 } : type == 'document' ?
                 (props: ReactImageGalleryItem)=>{
                  return <DocPreview fileUrl={props.original} type='docx' />
                 } 
                  : undefined
                }
                 />
            </React.Fragment> : <React.Fragment>
                        
            </React.Fragment>
            }
        </div>
    )}

  



const UploadedChapterContent: FunctionComponent<Props> = ({
  files = [],
  onRemoveItem,
  onRemoveDropFile,
  contenttype,
  fileStructure,
  data
}) => {
  const classes = useStyles();

  const [expanded, setExpanded] = React.useState<string | false>(false);
  const [backDrop,setBackDrop] = React.useState<boolean>(false)
  const [currentIndex,setCurrentIndex] = React.useState(0)
  useEffect(() => {
    setExpanded(false)
  }, [data, fileStructure, contenttype]);

  const handleChange = (panel: string | undefined) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
    setExpanded(isExpanded ? (panel ? panel : false) : false);
  };

  const contentIcon = [
    {contentname: 'image', icon: <ImageIcon />},
    {contentname: 'pdf', icon: <PictureAsPdfIcon />},
    {contentname: 'document', icon: <DescriptionIcon />},
    {contentname: 'video', icon: <OndemandVideoIcon />},
    {contentname: 'embed-link', icon: <YouTubeIcon />},
    {contentname: 'quiz', icon: <AssignmentIcon />},
    {contenttype: 'other', icon: <AttachFileIcon />}
  ]
  
  return (
    <div>
        <Backdrop open ={backDrop} className={classes.backdrop} >
        {
            (data?.length>0) && 
                <BackDropContent contents = {data as ChapterContent[]} setCurrentIndex={setCurrentIndex} currentIndex = {currentIndex} setBackDropHandleToggle ={setBackDrop} />
            
        }
        </Backdrop>
        {((files.length === 0) && 
        onRemoveItem) && 
          <Typography className={classes.secondHead}>
            No Pending Content to Upload
          </Typography>
        }
        {(files.length > 0) &&
          <List dense={false} className={classes.yMargin}>
            {
              files.map((file, index) => {
                console.log("Returring")
                return (
                  <ListItem key={index}>
                    <ListItemAvatar>
                      <Avatar>
                        {contentIcon.find(content => content.contentname === contenttype)?.icon}
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={file.name}
                      secondary={null}
                    />
                    <ListItemSecondaryAction>
                      <IconButton edge="end" aria-label="delete">
                        <DeleteIcon onClick={() => onRemoveDropFile && onRemoveDropFile(file, index)} />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                )
              })
            }
          </List>
        }
      <Box margin="20px 0 10px">
        <Box
          component="h3"
          fontWeight="500"
          font-size="18.2px"
          line-height="21px"
          color="#666666"
          margin="20px 0"
        >
          Uploaded Contents
        </Box>

        

        

        {(fileStructure && fileStructure.length !== 0) &&
          <div> 
            {uniqBy(fileStructure, 'label').filter(accord => accord.label).map(element => {
                return(
              <Accordion key={element.value} expanded={expanded === element.value} onChange={handleChange(element.value)}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1bh-content"
                  id="panel1bh-header"
                >
                  <Typography className={classes.heading}>{element.label}</Typography>
                </AccordionSummary>

                <AccordionDetails>
                  {(data?.filter((content) => content.contentsubtype && content.contentsubtype === element.label).map(
                    (content) => new File([], content.contentname)).length === 0) &&
                    <Typography className={classes.secondHead}>
                      No Content
                    </Typography>
                  }

                  {(data?.filter((content) => content.contentsubtype && content.contentsubtype === element.label).length !== 0) &&
                    <Grid container className={classes.yMargin} spacing={1}>
                      {
                        (data?.filter((content) => content.contentsubtype && content.contentsubtype === element.label).map(
                          (content, index) => {
                            return (
                              <Grid item sm={11} md={6} lg={4} xl={3} key={index} >
                                <Card style={{height:'300px'}}>
                                <CardMedia
                                  className = {classes.media}
                                  component={contenttype=='video'?()=>{
                                      return <React.Fragment>
                                          <div>
                                          <ReactPlayer
                                      url = {content.signedUrl}
                                      controls
                                      height='250px'
                                      wrapper='div'
                                      />
                                          </div>
                                          </React.Fragment>
                                  }:'image'}
                                  image={(content.contenttype=='image'||content.contenttype=='video')?content.signedUrl:contenttype='pdf'?pdfIcon:docIcon}
                                //   preload = {content.contenttype=='video'?'metadata':undefined}
                                //   controls = {content.contenttype=='video'}
                                //   controlsList={content.contenttype=='video'?'nodownload':undefined}
                                />
                                <CardActions disableSpacing>
                                    <Typography variant="subtitle1" color="textSecondary" noWrap>
                                      { content?.contentname}
                                    </Typography>
                                    {
                                      onRemoveItem && <IconButton aria-label={content?.title}>
                                      <DeleteIcon onClick={() => onRemoveItem && onRemoveItem(content.contentname)} />
                                       </IconButton>
                                    }
                                <IconButton aria-label={content?.title}>
                               <VisibilityIcon onClick={() => {
                                   setCurrentIndex(index)
                                   setBackDrop(true)
                               }} />
                                </IconButton>
                                </CardActions>
                              </Card>
                              </Grid>
                            )
                          })
                        )
                      }
                    </Grid>
                  }
                </AccordionDetails>
              </Accordion>
            )})}
          </div>
        }
      </Box>
    </div>
  );
};

export default UploadedChapterContent
