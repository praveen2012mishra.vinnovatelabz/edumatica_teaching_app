import React, { FunctionComponent } from "react";
// @ts-ignore
import FileViewer from "react-file-viewer";

interface Props {
    fileUrl: string,
    type: string
}
const DocPreview : FunctionComponent<Props> =({fileUrl , type})=>{
  return (
    <div className="App">
      <h1>React File Viewer Demo</h1>
      <h2>Displaying file with extension {type}</h2>
      <FileViewer fileType={type} filePath={fileUrl} onError={(e:any)=>{
          console.log(e, "error in file-viewer");
      }} />
    </div>
  );
}

export default DocPreview