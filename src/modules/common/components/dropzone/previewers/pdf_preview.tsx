import { Box } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import React, { FunctionComponent, useState } from 'react';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';

import './pdf-preview.css';

const options = {
  cMapUrl: 'cmaps/',
  cMapPacked: true,
};

interface Props{
    file : string,   //bucketUrl
    filename: string    //Filename
}

const PdfPreview  : FunctionComponent<Props> = ({file, filename}) =>{
  const [numPages, setNumPages] = useState<number|null>(null);
  const [pageNumber, setPageNumber] = useState(1);


  
    //@ts-ignore
  function onDocumentLoadSuccess({ numPages: nextNumPages }) {
    setNumPages(nextNumPages);
  }

  return (
    <div className="Example">
      <header>
        <h1>{filename}</h1>
      </header>
      <div className="Example__container">
        <div className="Example__container__document">
          <Document
            file={file}
            onLoadSuccess={onDocumentLoadSuccess}
            options={options}
          >
            
            <Page pageNumber={pageNumber} height={0.8*window.innerHeight} />
            
          </Document>
          <Box >
              
          <Pagination color='primary' count={numPages as number} onChange={(e: React.ChangeEvent<unknown>,newPage:number)=>{
              setPageNumber(newPage)
          }} />
          </Box>
          
        </div>
      </div>
    </div>
  );
}

export default PdfPreview