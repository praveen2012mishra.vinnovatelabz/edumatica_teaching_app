import 'date-fns';
import React, { FunctionComponent } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker
} from '@material-ui/pickers';

interface Props {
  selectedDate: Date | null;
  handleDateChange: (date: Date | null) => void;
  minDate?: Date | boolean;
  maxDate?: Date | boolean; 
  disablePast?: boolean
}

const Datetimepickers: FunctionComponent<Props> = ({selectedDate, handleDateChange, minDate, maxDate, disablePast}) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDateTimePicker
        id="date-picker-inline1"
        format="dd/MM/yyyy HH:mm"
        value={selectedDate}
        onChange={handleDateChange}
        minDate={minDate ? minDate as Date : undefined}
        maxDate={maxDate ? maxDate as Date : undefined}
        disablePast={disablePast ? disablePast : false}
      />
    </MuiPickersUtilsProvider>
  );
}

export default Datetimepickers