import React, { FunctionComponent } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: 'auto',
    },
    cardHeader: {
      padding: theme.spacing(1, 2),
    },
    list: {
      width: 200,
      height: 230,
      backgroundColor: theme.palette.background.paper,
      overflow: 'auto',
    },
    button: {
      margin: theme.spacing(0.5, 0),
    },
  }),
);

function not(a: inputs[], b: inputs[]) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a: inputs[], b: inputs[]) {
  return a.filter((value) => b.indexOf(value) !== -1);
}

function union(a: inputs[], b: inputs[]) {
  return [...a, ...not(b, a)];
}

enum Cardname {
  left = 'left',
  right = 'right',
}

interface Searchtext {
  left: string,
  right: string,
}

interface inputs {
  id: string,
  value: string
}

interface Props {
  left: inputs[];
  right: inputs[];
  setLeft: React.Dispatch<React.SetStateAction<inputs[]>>
  setRight: React.Dispatch<React.SetStateAction<inputs[]>>
}

const TransferLists: FunctionComponent<Props> = ({left, right, setLeft, setRight}) => {
  const classes = useStyles();
  const [checked, setChecked] = React.useState<inputs[]>([]);
  const [searchtext, setSearchtext] = React.useState<Searchtext>({left: '', right: ''})

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  const handleToggle = (value: inputs) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const numberOfChecked = (items: inputs[]) => intersection(checked, items).length;

  const handleToggleAll = (items: inputs[]) => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items));
    } else {
      setChecked(union(checked, items));
    }
  };

  const handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };

  const customList = (title: React.ReactNode, items: inputs[], cardname: Cardname ) => (
    <Card>
      <CardHeader
        className={classes.cardHeader}
        avatar={
          <Checkbox
            onClick={handleToggleAll(items)}
            checked={numberOfChecked(items) === items.length && items.length !== 0}
            indeterminate={numberOfChecked(items) !== items.length && numberOfChecked(items) !== 0}
            disabled={items.length === 0}
            inputProps={{ 'aria-label': 'all items selected' }}
          />
        }
        title={title}
        subheader={`${numberOfChecked(items)}/${items.length} selected`}
      />
      <Divider />
      <CardHeader
        className={classes.cardHeader}
        avatar={<SearchIcon style={{marginLeft: '10px', marginTop: '15px', marginBottom: '10px'}} />}
        title={
          <TextField id="standard-basic" placeholder="Search" 
            value={searchtext[cardname]}
            onChange={(e) => {setSearchtext({...searchtext, ...{[cardname]: e.target.value}})}}
          />
        }
      />
      <Divider />
      <List className={classes.list} dense component="div" role="list">
        {items.filter(list => {
          if(searchtext[cardname] === '') {return true}
          else {
            return list.value.toLowerCase().includes(searchtext[cardname].toLowerCase())
          }
        }).map((value: inputs) => {
          const labelId = `transfer-list-all-item-${value}-label`;

          return (
            <ListItem key={value.id} role="listitem" button onClick={handleToggle(value)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={`${value.value}`} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );

  return (
    <Grid container spacing={2} justify="center" alignItems="center" className={classes.root}>
      <Grid item>{customList('Select', left, Cardname.left)}</Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedRight}
            disabled={leftChecked.length === 0}
            aria-label="move selected right"
          >
            &gt;
          </Button>
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedLeft}
            disabled={rightChecked.length === 0}
            aria-label="move selected left"
          >
            &lt;
          </Button>
        </Grid>
      </Grid>
      <Grid item>{customList('Selected', right, Cardname.right)}</Grid>
    </Grid>
  );
}

export default TransferLists