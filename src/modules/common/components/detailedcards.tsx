import React, { FunctionComponent } from 'react';
import { Box, IconButton, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.bold,
      color: '#404040',
    },
    itemHead: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.bold,
      lineHeight: '16px',
      color: '#404040',
      textAlign: 'left',
      marginBottom: '6px'
    },
    itemValue: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '16px',
      color: '#666666',
      textAlign: 'left',
    },
    rightSpacing: {
      marginRight: '10px'
    }
  })
);

interface Action {
  icon: any[],
  function: () => any 
}

interface Subheading {
  icon: any[],
  text: string
}

interface Content {
  head: string,
  value: string
}

interface Fullcontent {
  icon: any[],
  text: Content[]
}

interface Props {
  bgcolor: string;
  heading: string;
  subheading: Subheading[];
  fullcontent: Fullcontent;
  editItem: () => any;
  removeItem: () => any;
}

const DetailedCard: FunctionComponent<Props> = ({bgcolor, heading, subheading, fullcontent, editItem, removeItem}) => {
  const classes = useStyles();

  return (
    <Box
      bgcolor={bgcolor}
      border="1px solid rgba(0, 0, 0, 0.08)"
      borderRadius="2px"
      color="#404040"
      marginTop="10px"
    >
      <Box padding="15px" borderBottom="1px solid rgba(0, 0, 0, 0.1)">
        <Box display="flex" justifyContent="space-between" marginBottom="15px">
          <Typography className={classes.heading}>
            {heading}
          </Typography>

          <Box flex="1" textAlign="right">
            <IconButton
              size="small"
              className={classes.rightSpacing}
              onClick={editItem}
            >
              <EditRoundedIcon />
            </IconButton>
            <IconButton
              size="small"
              className={classes.rightSpacing}
              onClick={removeItem}
            >
              <DeleteRoundedIcon />
            </IconButton>
          </Box>
        </Box>
        <Box
          display="flex"
          alignItems="center"
          marginTop="5px"
          justifyContent="space-between"
        >
          {subheading?.map((item, index) => {
            return (
              <Box
                key={item.text + '|' + String(index)}
                component="span"
                display="flex"
                alignItems="center"
                marginRight="10px"
              >
                
                  {item.icon[0]}
                
                <Box component="span" marginLeft="10px">
                  {item.text}
                </Box>
              </Box>
            )
          })}
        </Box>
      </Box>

      <Box display="flex" alignItems="center" padding="15px">

        {fullcontent?.icon[0]}
        {fullcontent?.text.map((item, index) => {
          return(
            <Box marginLeft="15px" key={item.head + '|' + String(index)}>
              <Typography className={classes.itemHead}>
                {item.head}
              </Typography>
              <Typography className={classes.itemValue}>
                {item.value}
              </Typography>
            </Box>
          )
        })}
      </Box>
    </Box>
  );
}

export default DetailedCard