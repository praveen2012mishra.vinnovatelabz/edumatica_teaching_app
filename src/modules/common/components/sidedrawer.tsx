import React , {FunctionComponent, useState, useEffect} from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {Redirect} from 'react-router'
import Navbar from './navbar'
import { connect, useDispatch } from 'react-redux';
import { RootState } from '../../../store';
import { User, Children, Student } from '../../common/contracts/user';
import {
  OrganizationSearch, TutorSearch, OrganizationTutorSearch,
  StudentSearch, ParentSearch, AdminSearch
} from '../constants/searchForUser'
import {sortBy} from 'lodash';
import {
  isAdminTutor,
  isStudent,
  isOrganization,
  isOrgTutor,
  isAdmin,
  isParent
} from '../helpers';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    activeIcon: {
      color: '#4C8BF5',
    },
    inActiveIcon: {
      color: '#666666',
    },
  }),
);

interface RouteList {
  name: string;
  Link: string;
  type: string;
  icon?: any;
  listno?: number;
}

interface MiniDrawerProps {
  children: React.ReactNode;
  authUser: User;
}

const MiniDrawer: FunctionComponent<MiniDrawerProps> = ({children, authUser}) =>{
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [redirectTo,setRedirectTo] = useState<string>('')
  const [routesList, setRoutesList] = useState<RouteList[]>()

  useEffect(() => {
    if(authUser.mobileNo) {
      if(isOrganization(authUser)) {
        setRoutesList(OrganizationSearch)
      } else if(isAdminTutor(authUser)) {
        setRoutesList(TutorSearch)
      } else if(isOrgTutor(authUser)) {
        setRoutesList(OrganizationTutorSearch)
      } else if(isStudent(authUser)) {
        setRoutesList(StudentSearch)
      } else if(isParent(authUser)) {
        setRoutesList(ParentSearch)
      } else {
        setRoutesList(AdminSearch)
      }
    }
  },[authUser.mobileNo])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />
      {((window.location.pathname.substring(0, 16) !== '/profile/process') && (window.location.pathname.substring(0, 20) !== '/profile/org/process') &&
        (window.location.pathname.substring(0, 24) !== '/profile/student/process') && (window.location.pathname.substring(0, 23) !== '/profile/parent/process')) &&

        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
          onMouseEnter={handleDrawerOpen}
          onMouseLeave={handleDrawerClose}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <List style={{paddingTop: '30px'}}>
            {window.location.pathname.substring(0, 16) !== '/profile/process' && routesList && sortBy((routesList as RouteList[]).filter(el=>el.type === 'navbar'), ['listno']).map((item, index) => {
              return (
                <ListItem button key= {index} onClick={()=>setRedirectTo(item.Link)} style={{paddingLeft: '25px'}}>
                  <ListItemIcon>
                    <item.icon 
                      className={(window.location.pathname.substring(0, item.Link.length) === item.Link) ? classes.activeIcon : classes.inActiveIcon}
                    />
                  </ListItemIcon>
                  <ListItemText primary={item?.name} 
                    className={(window.location.pathname.substring(0, item.Link.length) === item.Link) ? classes.activeIcon : classes.inActiveIcon}
                  />
                </ListItem>
              )
            })}
          </List>
        </Drawer>
      }

      <main className={classes.content}>
      <div className={classes.toolbar} />
      {children}
      </main>

    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
});

export default connect(mapStateToProps)(MiniDrawer);
