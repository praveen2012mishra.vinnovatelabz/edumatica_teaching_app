import { TAsyncAtomicBlockResponse } from "mui-rte";
// import pdfjsLib from 'pdfjs-dist/build/pdf'



const thumbnailGen  = (file: File,contentType:string,thumbWidth:number,thumbHeight:number) =>{
    if(contentType==='image'){
        return uploadImage(file,thumbWidth,thumbHeight)
    }
    else if(contentType=='video'){
        return uploadVideo(file,thumbWidth,thumbHeight)
    }
    // else if(contentType=='pdf') {
    //     return uploadPdf(file,thumbWidth,thumbWidth)
    // }
    else{
        return rejectPromise
    }
}


const rejectPromise = new Promise((resolve,reject)=>{
    reject('contentType not supported')
})
const uploadImage = (file: File,thumbWidth:number,thumbHeight:number) => {
    return new Promise<string>(async (resolve, reject) => {
        const img = new Image();
        img.onload = async () => {
            // Create canvas
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            // Set width and height
            canvas.width = thumbWidth;
            canvas.height = thumbHeight;
            // Draw the image
            if (ctx instanceof CanvasRenderingContext2D) {
                ctx.drawImage(img, 0, 0, thumbWidth,thumbHeight);
            }
            const url = canvas.toDataURL("image/jpeg");
            if (!url) {
                reject()
                return
            }
            resolve(url)
        }
        img.src = URL.createObjectURL(file);
    })
}

const uploadVideo = (file: File,thumbWidth:number,thumbHeight:number) => {
    return new Promise<string>(async (resolve, reject) => {
        console.log('Sending For LG2')
        const img = new Image();
        const video = document.createElement('video')
        video.onloadedmetadata = async () => {
            // Create canvas
            video.currentTime = 1

            video.ontimeupdate = async () =>{
                console.log(video.currentTime)
                console.log(video.videoHeight)
            console.log(video.videoWidth)
            console.log('Sending For LG3')
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            // Set width and height
            canvas.width = thumbWidth;
            canvas.height = thumbHeight;
            // Draw the image
            if (ctx instanceof CanvasRenderingContext2D) {
                ctx.drawImage(video, 0, 0, thumbWidth,thumbHeight);
            }
            console.log('Sending For LG3')
            const url = canvas.toDataURL("image/jpeg");
            console.log(url)
            if (!url) {
                console.log('Sending For LG4')
                reject()
                return
            }
            resolve(url)
            }
            
        }
        video.src = URL.createObjectURL(file);
    })
}

// const uploadPdf  = (file:File, thumbWidth:number, thumbHeight: number) =>{
//     function makeThumb(page:any) {
        
//         var vp = page.getViewport(1);
//         var canvas = document.createElement("canvas");
//         canvas.width = thumbWidth
//         canvas.height = thumbHeight;
//         var scale = Math.min(canvas.width / vp.width, canvas.height / vp.height);
//         return page.render({canvasContext: canvas.getContext("2d"), viewport: page.getViewport(scale)}).promise.then(function () {
//           return canvas.toDataURL();
//         });
//       }
 
//     const fr = new FileReader()
//     pdfjsLib.getDocument(fr.readAsArrayBuffer(file)).promise.then(function (doc : any) {
//     var pages = []; while (pages.length < doc.numPages) pages.push(pages.length + 1);
//     return doc.getPage(0).then((makeThumb))
//   }).catch(console.error);
// }

  
  

export default thumbnailGen