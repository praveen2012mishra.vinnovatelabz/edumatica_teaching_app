import React, { FunctionComponent } from 'react';
import { AppBar, Toolbar, Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme) => ({
  headOne: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bolder
  },
  headTwo: {
    backgroundColor: '#fff',
    color: '#00B9F5',
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bolder,
    padding: '0px 20px',
    marginLeft: '20px',
    borderRadius: '8px'
  },
  sendFront: {
    zIndex: 1
  }
}));

const PaymentNavbar: FunctionComponent = () => {
  const classes = useStyles();

  return (
    <AppBar position="sticky" elevation={0} className={classes.sendFront}>
      <Toolbar>
        <div style={{ width: '100%', padding: '15px 0px' }}>
          <Box display="flex" justifyContent="center">
            <Box>
              <Typography className={classes.headOne}>Payment Desk</Typography>
            </Box>
            <Box>
              <Typography className={classes.headTwo}>Edumatica</Typography>
            </Box>
          </Box>
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default PaymentNavbar