import {Batch} from '../../academics/contracts/batch';

// Assignment interfaces for tutor
export interface AssignedStudent {
  _id?: string,
  approved: boolean,
  studentId: string | {
    studentName: string,
    _id: string,
  },
  uploadedAnswerDocs?: answerDoc[],
  assignmentStatus: string,
  feedback?: string,
  marks?: number,
  tutorHighlights?: any,
}

export interface taskDoc {
  taskDocFormat: string,
  taskDocLocation: string,
  uuid: string,
}

export interface AssignedAssignment {
  tutorId?: string,
  assignment: string,
  group?: boolean,
  batchId: string,
  groups: AssignedStudent[][],
  deadline: Date,
}

export interface AssignedAssignmentRes extends AssignmentRes {
  batches: AssigningDetail[]
}

export interface Assignment {
  tutorId?: string,
  boardname: string,
  classname: string,
  subjectname: string,
  assignmentname: string,
  brief: string,
  marks: number,
  assignmentStatus: string,
  taskText?: string,
  taskDocs?: taskDoc[],
  isAssigned?: boolean,
  assignmentId?: string
}

export interface AssignmentRes extends Assignment {assignmentId: string}

export interface StudentAssignmentRes {
  subjectname: string,
  assignmentStatus: string,
  assignmentname: string,
  deadline: Date,
  marks: number,
  brief: string,
  tutorName: string,
  organizationName: string,
  batchId: string,
  assignedId: string
}

export interface AssignmentTaskPageRes {
  subjectname: string,
  assignmentname: string,
  deadline: Date,
  marks: number,
  brief: string,
  taskDocs: taskDoc[],
  taskText: string,
  assignmentStatus: string,
  tutorName: string,
  organizationName: string,
  uploadedAnswerDocs: answerDoc[],
  assignmentId: string,
  groupIndex: number,
  studentIndex: number,
}

export interface AssigningDetail {
  batchId: string,
  batchName: string,
  students: string[],
  deadline: Date
}

export interface batchesRowData {
  batchName: string,
  totalStudents: number,
  deadline: string,
  noOfSubmitted: number,
  noOfNotSubmitted: number,
}

export interface AssignedBatch extends batchesRowData {assignedId: string}

export interface studentRowData {
  studentName: string,
  assignmentStatus: string,
}

export interface studentDetails extends studentRowData {
  groupIndex: number,
  studentIndex: number
}

export interface evaluateProp {
  batchName: string,
  studentName: string,
  assignmentStatus: string,
  uploadedAnswerDocs?: answerDoc[],
  assignedAssignment: AssignedAssignment,
  studentId: string
}

export interface batchDetailsForTutorView {
  assignment: Assignment,
  batchName: string,
  totalStudents: number,
  deadline: string,
  noOfSubmitted: number,
  noOfNotSubmitted: number,
  evaluateProps: evaluateProp[]
}

export interface studentAnswersRes {
  studentAnswers: answerDoc[],
  studentName: string
}

export interface saveFeedbackReq {
  assignedId: string,
  groupIndex: number,
  studentIndex: number,
  updatedFields: {
    assignmentStatus: string,
    marks: number,
    feedback: string,
    tutorHighlights?: any,
  }
}

// Assignment interfaces for student
export interface answerDoc {
  answerDocFormat: string,
  answerDocLocation: string,
  uuid: string,
}

export interface studentIndices {
  groupIndex: number,
  studentIndex: number,
}

export interface saveAssignmentReq {  
  uploadedAnswerDocs: answerDoc[],
  assignedId: string,
  groupIndex: number,
  studentIndex: number,
}

export interface feedbackRes extends StudentAssignmentRes {
  tutorHighlights?: any,
  marksObtained: number,
  feedback?: string,
  uploadedAnswerDocs: answerDoc[]
}

// Enums
export enum AssignmentStatus {
  SUBMITTED = 'Submitted',
  NOT_SUBMITTED = 'Not submitted',
  EVALUATED = 'Evaluated',
  NOT_ASSIGNED = 'Not assigned'
}

export enum AssignmentDocType {
  TASK_DOC = 'AssignmentTaskDocs',
  ANSWER_DOC = 'AssignmentAnswerDocs'
}
