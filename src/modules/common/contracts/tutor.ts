import {Assessment} from "../../assessment/contracts/assessment_interface"
import { Parent } from "./user";


export interface getAssessmentsResponse {
    assessmentList:Assessment[]
}

export interface getAssessmentResponse {
    assessment:Assessment
}

export interface  getParentResponse {
    parent: Parent
}