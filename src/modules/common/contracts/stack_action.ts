import { QuizMetaData } from '../../academics/contracts/quiz_meta';
import { StackActionType } from '../enums/stack_action_type';

interface StackActionPayload<AD, AT> {
  type: AD;
  data: AT;
  subtype?: AD;
  boardname : AD ;
  classname : AD ;
  subjectname : AD ;
  chaptername : AD; 
  quizMeta? : QuizMetaData,
  thumbnail? : AD
}

export interface StackActionItem<AD = any, AT = any> {
  type: StackActionType;
  payload: StackActionPayload<AD, AT>;
}

export interface ChapterActionItem {
  type: StackActionType;
  payload: {
    batchId: string | undefined,
    batchfriendlyname: string | undefined,
    chaptername: string;
    boardname: string;
    classname: string;
    subjectname: string;
    newchaptername?: string;
  };
}
