export interface KycDocument {
  kycDocFormat: string;
  kycDocLocation: string;
  kycDocType: string;
  uuid: string;
  url: string;
}
