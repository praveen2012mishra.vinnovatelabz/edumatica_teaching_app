//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useState, useEffect, useRef } from 'react';
import 'katex/dist/katex.min.css';
import {
  Box,
  Container,
  Grid,
  Button as MuButton,
  Typography
} from '@material-ui/core';
import MiniDrawer from '../../common/components/sidedrawer';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { RootState } from '../../../store';
import { User } from '../../common/contracts/user';
import { RouteComponentProps } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { useSnackbar } from 'notistack';
import Red from '@material-ui/core/colors/red';
import Blue from '@material-ui/core/colors/blue';
import Green from '@material-ui/core/colors/green';
import { exceptionTracker } from '../../common/helpers';
import { getAttemptAssessments } from '../../student_assessment/helper/api';
import { OngoingAssessment } from '../../student_assessment/contracts/assessment_interface';
import { fontOptions } from '../../../theme';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';

var Latex = require('react-latex');
const RedColor = Red[500];
const GreenColor = Green[600];
const BlueColor = Blue[500];

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    welcomeNote: {
      fontSize: fontOptions.size.large,
      fontWeight: fontOptions.weight.normal,
      lineHeight: fontOptions.size.large
    },
    contentBox: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      paddingTop: '20px'
    },
    studentBox: {
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'row',
      width: '430px',
      height: '100%',
    },
    classBox: {
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'column',
      minHeight: '50px'
    },
    assignmentBox: {
      display: 'flex',
      alignSelf: 'center',
      fontSize: fontOptions.size.medium,
      fontWeight: fontOptions.weight.normal,
      lineHeight: fontOptions.size.medium
    },
    horizontalLine: {
      marginTop: '20px',
      borderBottom: '1px solid rgba(0, 0, 0, 0.3)',
      width: '100%'
    },
    containerBox: {
      paddingLeft: '30px',
      width: '100%',
      display: 'flex'
    },
    rightContainer: {
      width: '40%',
      paddingLeft: '50px',
      borderLeft: '1px solid rgba(0, 0, 0, 0.3)',
      minHeight: '430px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    subjectText: {
      fontSize: fontOptions.size.medium,
      fontWeight: fontOptions.weight.normal,
      lineHeight: fontOptions.size.medium,
      color: '#000'
    },
    generalInstruction: {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.bold,
      lineHeight: fontOptions.size.small,
      color: 'black',
      paddingTop: '5px',
      paddingBottom: '25px',

    },
    normalText: {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      lineHeight: fontOptions.size.small,
      paddingRight: '25px'
    },
    timeText: {
      fontSize: fontOptions.size.medium,
      fontWeight: fontOptions.weight.bold,
      lineHeight: fontOptions.size.xLarge,
      paddingTop: '20px',
      color: '#00B9F5',
      textAlign: 'center',
      margin:  'auto',
      width: '25%'
    },
    mainHeading: {
      fontSize: fontOptions.size.large,
      fontWeight: fontOptions.weight.bold,
      color: '#404040'
    },
    subHeading: {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      color: '#404040'
    },
    instructionHead: {
      fontSize: fontOptions.size.large,
      fontWeight: fontOptions.weight.normal,
      color: '#404040'
    },
    bullets: {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      color: '#404040'
    }
  })
);

function addMinutes(date: Date, minutes: number) {
  return new Date(date.getTime() + minutes * 60000);
}

const StudentAssessmentInstruction: FunctionComponent<Props> = ({
  authUser,
  location,
  history
}) => {

  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [startFlag, setStartFlag ] = useState<boolean>(false)
  const [countDuration,setCountDuration] = useState<number>(0)
  const [countDurationSet,setCountDurationSet] = useState<boolean>(false)
  const [attemptAssessments, setAttemptAssessments] = useState<
    OngoingAssessment[]
  >([]);
  const [currentAttemptAssessments, setCurrentAttemptAssessments] = useState<
    OngoingAssessment
  >(null);

  useEffect(() => {
    const fetchAssignmentValue = async () => {
      try {
        const response = await getAttemptAssessments();
        setAttemptAssessments(response);
        var lastname = sessionStorage.getItem("selectedStudentAssessmentId");

      } catch (err) {
        exceptionTracker(err.response?.data.message);
        if (err.response?.status === 401) {
          setRedirectTo('/login');
        }
      }
    }
    fetchAssignmentValue();

  }, []);

  useEffect(()=>{
    if(!currentAttemptAssessments){
      return
    }
    if(new Date(currentAttemptAssessments.startDate)<new Date()){
      setStartFlag(true)
    }
    else{
      if(new Date(currentAttemptAssessments.startDate)>new Date()){
        setCountDuration((new Date(currentAttemptAssessments.startDate).getTime()- new Date().getTime())/1000)
        setCountDurationSet(true)
      }
    }
  },[currentAttemptAssessments])

  useEffect(() => {
    const fetchAssignmentValue = async () => {
      try {
        if (attemptAssessments.length > 0) {
          var lastname = await sessionStorage.getItem("selectedStudentAssessmentId");

          attemptAssessments.forEach((item) => {
            if (item._id == lastname) {
              setCurrentAttemptAssessments(item)
              return
            }
          })   
        }
      } catch (err) {
        exceptionTracker(err.response?.data.message);
        if (err.response?.status === 401) {
          setRedirectTo('/login');
        }
      }
    }
    fetchAssignmentValue();

  }, [attemptAssessments]);

  if (currentAttemptAssessments?._id) {
    const { subjectname, totalMarks, instructions, assessmentname, duration, ownerId } = currentAttemptAssessments.assessment
    const instr = instructions.split("\n").filter(list=>list)
    const { startDate } = currentAttemptAssessments;
    let startsIn;

    if(new Date(startDate) > new Date()) {
      let diffInMilliSeconds = Math.abs(new Date(startDate) - new Date()) / 1000;
      const days = Math.floor(diffInMilliSeconds / 86400);
      diffInMilliSeconds -= days * 86400;

      const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
      diffInMilliSeconds -= hours * 3600;

      const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
      diffInMilliSeconds -= minutes * 60;

      startsIn = `Starts in ${days} Days ${hours} Hours ${minutes} Minutes`

      if(days === 0 && hours === 0 && minutes ===0) {
        startsIn = 'Starts in a few Moment'
      }
    } else {
      startsIn = 'Start Attempting'
    }

    return (
      <>
        <MiniDrawer>
        <Box>
          <Grid container style={{borderBottom: '1px solid #666666', padding: '20px 50px'}}>
            <Grid item xs={8}>
              <Grid container>
                <Grid item xs={12}>
                  <Typography className={classes.mainHeading}>
                    {assessmentname}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4}>
              <Grid container style={{padding: '0px 20px'}}>
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={6}>
                      <Typography className={classes.subHeading}>
                        Tutor : {ownerId}
                      </Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography className={classes.subHeading}>
                        Subject : {subjectname}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={6}>
                      <Typography className={classes.subHeading}>
                        Full Marks : {totalMarks}
                      </Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography className={classes.subHeading}>
                        Duration : {duration}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid container style={{padding: '0px 50px', height: '69vH'}}>
            <Grid item xs={8} style={{borderRight: '1px solid #666666'}}>
              <Grid container>
                <Grid item xs={12}>
                  <Box className={classes.containerBox}>
                    <Box width='60%' paddingY="50px">
                      <Box className={classes.instructionHead}>General Instructions</Box>
                      <Box className={classes.bullets}>Read carefully before starting test</Box>
                      <Box className={classes.bullets} marginTop="20px">
                        {instr.map((list, count) => {
                          return(
                            <Typography>
                              {count+1}: {' ' + list}
                            </Typography>
                          )
                        })}
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4}>
              <Grid container style={{textAlign: 'center', paddingTop: '40%'}}>
                <Grid item xs={12}>
                  <Box >
                    <Box>
                      <MuButton
                        variant="contained" color="primary" style={{ width: '200px' }}
                        disabled ={!startFlag}
                        onClick={() => {
                          
                          var idx = sessionStorage.getItem("selectedStudentAssessmentId");
                          enqueueSnackbar('Starting Assessment', { variant: 'success' });
                          history.push(
                            '/students' +
                            '/student_assessement_test?attemptassessmentId=' +
                            idx
                          );
                        }}
                      >
                        Start Test
                      </MuButton>
                      <Box className={classes.timeText} >
                        {
                          (currentAttemptAssessments!==null && countDurationSet) &&  
                          <CountdownCircleTimer
                      strokeWidth={4}
                      size={100}
                      isPlaying
                      duration={countDuration}
                      colors={[
                        ['#91c788', 0.25],
                        ['#009dff', 0.25],
                        ['#ce1212', 0.25],
                        ['#810000', 0.25],
                      ]}
                      onComplete={() => {
                        setStartFlag(true)
                      }}
                    >
                      {({ remainingTime }) => remainingTime}
                    </CountdownCircleTimer>
                        }
                          </Box>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
        </MiniDrawer>
      </>


    );
  }
  return (
    <></>
  )
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(StudentAssessmentInstruction);
