export interface BBBAttendance {
  userID?: string
  userName?: string
  entryTime: string | Date;
  attended: string;
  ispresent: string;
}
