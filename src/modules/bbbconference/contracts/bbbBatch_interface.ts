import { Schedule } from "../../academics/contracts/schedule";
import { Tutor } from "../../common/contracts/user";

export interface BatchStudent {
  _id: string,
  mobileNo: string,
  studentName: string
};

export interface BBBBatch {
  _id?: string;
  schedules?: Schedule[];
  content?: string[];
  students: BatchStudent[];
  boardname: string;
  classname: string;
  subjectname: string;
  batchfriendlyname: string;
  batchenddate: string;
  batchstartdate: string;
  batchicon: string;
  tutor: string;
  tutorId?: Tutor;
}
