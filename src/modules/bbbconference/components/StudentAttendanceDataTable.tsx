import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  DataGrid,
  GridCellClassParams,
  GridColumns,
  GridToolbar,
} from '@material-ui/data-grid';
import { Box, Container, makeStyles } from '@material-ui/core';
import clsx from 'clsx';

interface RowData {
  userID?: string
  userName?: string
  entryTime: string | Date;
  attended: string;
  ispresent: string;
}

interface Props {
  gridData: RowData[];
}

const useStyles = makeStyles({
  root: {
    '& .super-app-theme--cell': {
      backgroundColor: 'rgba(224, 183, 60, 0.55)',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.negative': {
      backgroundColor: '#d47483',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.positive': {
      backgroundColor: 'rgba(157, 255, 118, 0.49)',
      color: '#1a3e72',
      fontWeight: '600'
    }
  },
  paginate:{
    display:"flex"
  }
});

export const StudentAttendanceDataTable: FunctionComponent<Props> = ({
  gridData
}) => {
  const classes = useStyles();

  const columns: GridColumns = [
    { field: 'id', headerName: 'S.No', flex:0.5 },
    { field: 'userName', headerName: 'Student Name', flex:1.2 },
    {
      field: 'entryTime',
      headerName: 'Entry Time',
      type: 'string',
      flex:1
    },
    { field: 'attended', headerName: '% Attended', flex:1 },
    { field: 'ispresent', headerName: 'Attendance', flex:1, cellClassName: (params: GridCellClassParams) =>
    clsx('super-app', {
      negative: (params.value as string) === "Absent",
      positive: (params.value as string) === "Present",
    }), },
  ];

  const [rows,setRows] = useState<RowData[]>([])

   useEffect(()=>{
      setRows(()=>{
          return gridData?.map((data,index)=>{
              return {id:index+1, userName:data.userName, entryTime:data.entryTime, attended:data.attended,ispresent:data.ispresent} as RowData
          }) as RowData[]
      })
    },[gridData])

  return (
    <React.Fragment>
      <Box
        className={classes.root}
        style={{
          width: '100%',
          height: '420px',
          marginTop: '10px',
          display: 'flex'
        }}
      >
        <DataGrid
          loading={gridData.length === 0}
          pagination
          components={{
            Toolbar: GridToolbar
          }}
          autoPageSize
          rows={rows}
          columns={columns}
        />
      </Box>
    </React.Fragment>
  );
};
