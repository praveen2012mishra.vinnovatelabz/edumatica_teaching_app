import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../../../store';
import {
  createStyles,
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import { ChevronRight as ChevronRightIcon } from '@material-ui/icons';
import AccessTimeTwoToneIcon from '@material-ui/icons/AccessTimeTwoTone';
import CalendarTodayTwoToneIcon from '@material-ui/icons/CalendarTodayTwoTone';
import { Student } from '../../../common/contracts/user';
import MainLayoutBBB from '../bbb_mainLayout';
import {
  Box,
  Container,
  Divider,
  Grid,
  Typography,
} from '@material-ui/core';
import Button from '../../../common/components/form_elements/button';
import { Scrollbars } from 'react-custom-scrollbars';
import {
  fetchBatchesList,
  fetchSchedulesList
} from '../../../common/api/academics';
import {
  getPostMeetingEventsInfo,
  getPostIndividualMeetingEventsInfo
} from '../../helper/api';
import { Batch } from '../../../academics/contracts/batch';
import { Schedule } from '../../../academics/contracts/schedule';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import { xml2js } from 'xml-js';
import TutorClassHistory from '../../../../assets/svgs/tutor_class_history.svg';
import PlayCircleFilledWhiteIcon from '@material-ui/icons/PlayCircleFilledWhite';
import GroupIcon from '@material-ui/icons/Group';
import { Meeting } from '../../contracts/meeting_interface';
import { BBBEvents } from '../../contracts/bbbevent_interface';
import { AttendanceOption } from '../../enums/attendance_options';
import { StudentAttendanceDataTable } from '../StudentAttendanceDataTable';

const styles = createStyles({
  topHeader: {
    border:"1px solid #00B9F5",
    borderRadius:"15px",
    backgroundColor:"#fff",
    width:"100%",
    padding:"18px 30px",
    display:"flex",
    alignItems:"center",
    justifyContent:"space-between",
    marginBottom:"20px"
  },
  topHolder: {
    border:"1px solid #00B9F5",
    borderRadius:"15px",
    backgroundColor:"#fff",
    width:"100%",
    padding:"18px 30px",
    // display:"flex",
    // alignItems:"center",
    // justifyContent:"space-between",
    marginBottom:"20px",
    color: '#7D7D7D'
  },
  typography_1: {
    color: '#4285F4'
  },
  typography_2: {
    color: 'red',
    paddingTop: '160px'
  },
});

// interface TabPanelProps {
//   children?: React.ReactNode;
//   index: any;
//   value: any;
// }

// function TabPanel(props: TabPanelProps) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`scrollable-auto-tabpanel-${index}`}
//       aria-labelledby={`scrollable-auto-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box p={3}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// function a11yProps(index: any) {
//   return {
//     id: `scrollable-auto-tab-${index}`,
//     'aria-controls': `scrollable-auto-tabpanel-${index}`
//   };
// }

interface Props extends WithStyles<typeof styles> {
  profile: Student;
}

const StudentMeetingDashboard: FunctionComponent<Props> = ({
  classes,
  profile
}) => {
  const user = localStorage.getItem('authUser');
  const [batches, setBatches] = useState<Batch[]>([]);
  // eslint-disable-next-line
  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  let [batchMeetings, setBatchMeetings] = useState<Meeting[]>([]);
  let [individualMeetingInfo, setIndividualMeetingInfo] = useState<Meeting>();
  let [recording, setRecording] = useState<Boolean>(false);
  let [attendance, setAttendance] = useState<Boolean>(false);
  const [thumbnail, setThumbnail] = useState('');
  let [attandanceChart, setAttandanceChart] = useState<any[]>([]);

  //MUI TABS
  // eslint-disable-next-line
  const [value, setValue] = React.useState(0);

  // const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
  //   setValue(newValue);
  // };

  useEffect(() => {
    (async () => {
      try {
        if (user === null) return;
        const batchesListResponse = await fetchBatchesList();
        const schedulesListResponse = await fetchSchedulesList();
        const [batchesList, schedulesList] = await Promise.all([
          batchesListResponse,
          schedulesListResponse
        ]);
        const sortedbatches = batchesList.filter(function (batch) {
          return JSON.parse(user).batches.includes(batch._id);
        });
        setBatches(sortedbatches);
        setSchedules(schedulesList);
        getBatchClasses(sortedbatches[0]._id)
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [profile.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const getBatchClasses = (batchID: string | undefined) => {
    setAttendance(false);
    setRecording(false);
    setThumbnail('');
    setIndividualMeetingInfo(undefined);
    setBatchMeetings([]);
    if (batchID === undefined) return;
    getPostMeetingEventsInfo(batchID).then((result) => {
      if (result.message === 'Success') setBatchMeetings(result.data);
    });
  };

  const getPostIndividualMeetingEventDetails = (internalMeetingID: string) => {
    setAttendance(true);
    setRecording(false);
    setIndividualMeetingInfo(undefined);
    getPostIndividualMeetingEventsInfo(internalMeetingID).then((result) => {
      if (result.message === 'Success') {
        setIndividualMeetingInfo(result.data);
        createTable(result.data);
      }
    });
  };

  const getmetadata = (internlMeetingID: string | undefined) => {
    axios
      .get(
        `https://bbb-testbucket.s3.ap-south-1.amazonaws.com/${internlMeetingID}/metadata.xml`
      )
      .then((result) => {
        const js_res: any = xml2js(result.data, { compact: true });
        var thumbnailURL =
          js_res.recording.playback.extensions.preview.images.image[0]._text;
        var length = thumbnailURL.length;
        var newthumbnailURL = `https://bbb-testbucket.s3.ap-south-1.amazonaws.com${thumbnailURL.substring(
          32,
          length
        )}`;
        setThumbnail(newthumbnailURL);
      });
  };

  const viewRecording = (recordingURL: string | undefined) => {
    if (recordingURL) window.open(recordingURL);
  };

  // const deleteRecording = (internalMeetingID: string | undefined) => {
  //   if (internalMeetingID === undefined) return;
  //   deleteRecordingData(internalMeetingID).then((result) => {
  //     getPostIndividualMeetingEventDetails(result.internalMeetingID);
  //   });
  // };

  const createTable = (individualMeetingInfo: Meeting | undefined) => {
    if (individualMeetingInfo === undefined) return;
    const events: BBBEvents[] = individualMeetingInfo.events;
    function createData(
      userID: string | undefined,
      userName: string | undefined,
      entryTime: string | Date,
      attended: string,
      ispresent: string
    ) {
      return {userID, userName, entryTime, attended, ispresent };
    }
    var rows: any[] = []
    const meetingEndEvent = events.filter(function (items) {
      return items.type === 'meeting-ended';
    });
    //console.log(meetingEndEvent)
    var totalMeetingTime =
      meetingEndEvent[0].eventTriggerTime - individualMeetingInfo.createTime;

    const students_array = (batches.find(batch => batch._id === individualMeetingInfo.meetingID))?.students
    // const tutorID = (batches.find(batch => batch._id === individualMeetingInfo.meetingID))?.tutorId
    // console.log(batches.find(batch => batch._id === individualMeetingInfo.meetingID))

    if (user !== null) {
      // console.log(JSON.parse(user))
      let total_time = 0
      // eslint-disable-next-line
      const userEvent = events.filter(function (item) {
        if(item.userID === JSON.parse(user)._id) {
          return item.type === 'user-joined' || item.type === 'user-left';
        }
      });
      userEvent.forEach(eachEvent => {
        if(eachEvent.type === "user-joined"){
          if(eachEvent.userID === undefined) return
            if( JSON.parse(user)._id !== eachEvent.userID) {
              rows.push(createData(JSON.parse(user)._id, JSON.parse(user).studentName, '-', '-', AttendanceOption.IS_ABSENT))
            }
        }
      })
      if(userEvent.length === 0){
        rows.push(
          createData(JSON.parse(user)._id, JSON.parse(user).studentName, '-', '-', AttendanceOption.IS_ABSENT)
        )
      }
      console.log(userEvent)
      const length = userEvent.length
      if(userEvent.length !== 0 && (length % 2) !== 0) {
        for (let i=0; i<userEvent.length - 1; i+=2){
          const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
          total_time = total_time + time
        }
        total_time = total_time + (meetingEndEvent[0].eventTriggerTime - userEvent[length-1].eventTriggerTime)
        const percentage = (total_time/totalMeetingTime)*100
        var ispresent = AttendanceOption.IS_ABSENT;
        if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
        rows.push(
          createData(userEvent[0].userID, userEvent[0]?.name, (new Date(userEvent[0].eventTriggerTime)).toLocaleTimeString(), percentage.toFixed(2), ispresent)
        )
      }
      if(userEvent.length !== 0 && (length % 2) === 0) {
        for (let i=0; i<userEvent.length; i+=2){
          const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
          total_time = total_time + time
        }
        const percentage = (total_time/totalMeetingTime)*100;
        // eslint-disable-next-line
        var ispresent = AttendanceOption.IS_ABSENT;
        if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
        rows.push(
          createData(userEvent[0].userID, userEvent[0]?.name, (new Date(userEvent[0].eventTriggerTime)).toLocaleTimeString(), percentage.toFixed(2), ispresent)
        )
      }
    }
    students_array?.filter(function(student){

    })
    setAttandanceChart(rows);
    console.log(rows)
  };

  return (
    <MainLayoutBBB>
      <Container maxWidth="lg">
        <Box className={classes.topHeader}>
          <Box>
            <Typography component="span" color="secondary">
              <Box component="h1" fontWeight="600" margin="0">
                Meeting History
              </Box>
            </Typography>
            <h4>
              Check your class progress, meeting recordings, student attendance
              details{' '}
            </h4>
          </Box>
          <img height="110px" src={TutorClassHistory} alt="Class History" />
        </Box>
        <Box>
          <Grid container spacing={3}>
            <Grid item lg={6} md={12} xs={12}>
              <Box minHeight="420px" padding="0 !important" className={classes.topHolder}>
                <Box display="flex" alignItems="center"justifyContent="space-evenly">
                  <Box display="flex" alignItems="center">
                  <GroupIcon style={{marginRight:"8px"}}/>
                  <h2>Batches</h2>
                  </Box>
                  <h2>Action</h2>
                </Box>
                <Divider/>
                <Scrollbars style={{height:"350px"}}>
                  <Box lineHeight="40px" padding="18px 30px">
                    {
                      batches.map(eachBatch => {
                        return(
                          <Box key={eachBatch._id} display="flex" justifyContent="space-between" alignItems="center">
                            <h4 style={{fontWeight:"bold"}}>{`${eachBatch.batchfriendlyname.toUpperCase()} - ${eachBatch.classname} - ${eachBatch.subjectname}`}</h4>
                            <Button onClick={() => getBatchClasses(eachBatch._id)} style={{height:"25px"}} size="small" variant="outlined">View</Button>
                          </Box>
                        )
                      })
                    }
                  </Box>
                </Scrollbars>
              </Box>
            </Grid>
            <Grid item lg={6} md={12} xs={12}>
              <Box minHeight="320px" padding="0 !important" className={classes.topHolder}>
                <Box display="flex" alignItems="center"justifyContent="space-evenly">
                  <h2>Class Details</h2>
                </Box>
                <Divider/>
                <Scrollbars style={{height:"280px"}}>
                    <Box lineHeight="20px" padding="18px 30px">
                        {batchMeetings.map((meeting) => {
                          const date = new Date(meeting.createTime);
                          const meetingEndEvent = meeting.events.find(function(item){
                            return item.type==="meeting-ended"
                          })
                          // eslint-disable-next-line
                          if(!meetingEndEvent) return
                          // const meetingEndTime = new Date(meetingEndEvent?.eventTriggerTime)
                          const meetingLength = (meetingEndEvent?.eventTriggerTime - meeting.createTime)/60000
                          // console.log(meetingEndTime.toLocaleTimeString())
                          // console.log(meetingLength/60000)
                          return (
                            <Box
                              width="100%"
                              display="flex"
                              justifyContent="space-between"
                              alignItems="center"
                              marginY="12px"
                              bgcolor="#c1e3f2"
                              padding="0 8px"
                              color="#373737"
                              // border="2px solid #373737"
                              borderRadius="15px"
                            >
                              <Box display="flex" flexDirection="column"justifyContent="center" alignItems="center">
                                <Box display="flex" alignItems="center">
                                  <CalendarTodayTwoToneIcon
                                    style={{ marginRight: '3px' }}
                                  />
                                  <p>{date.toLocaleDateString()}</p>
                                </Box>
                                <Box display="flex" alignItems="center">
                                  <AccessTimeTwoToneIcon
                                    style={{ marginRight: '3px', marginLeft: '10px' }}
                                  />
                                  <p>{date.toLocaleTimeString()}</p>
                                </Box>
                              </Box>
                              <Box display="flex" flexDirection="column"justifyContent="center">
                              <p style={{ marginLeft: '5px' }}>
                                {meeting.meetingName}
                              </p>
                              <p style={{ marginLeft: '5px' }}>
                                {`Class Duration - ${meetingLength.toFixed(2)} mins`}
                              </p>
                              </Box>
                              
                              <Button
                                onClick={() => {
                                  getPostIndividualMeetingEventDetails(
                                    meeting.internalMeetingID
                                  );
                                  
                                }
                                }
                              >
                                <ChevronRightIcon />
                              </Button>
                            </Box>
                          );
                        })}
                    </Box>
                </Scrollbars>
              </Box>
              <Box>
              {!individualMeetingInfo ? (
                  ''
                ) : (
                  <Box
                    border="1px solid lightgray"
                    borderRadius="15px"
                    bgcolor="#fff"
                    width="100%"
                    display="flex"
                    flexDirection="column"
                  >
                    <Box
                      borderBottom="2px solid lightgray"
                      display="flex"
                      alignItems="center"
                      justifyContent="space-evenly"
                    >
                      <Button
                        onClick={() => {
                          getmetadata(individualMeetingInfo?.internalMeetingID);
                          setRecording(true);
                          setAttendance(false);
                        }}
                        style={{ color: '#4C8BF5' }}
                      >
                        Recording
                      </Button>
                      <Divider
                        orientation="vertical"
                        flexItem
                        style={{ backgroundColor: 'lightgray' }}
                      />
                      <Button
                        onClick={() => {
                          createTable(individualMeetingInfo);
                          setAttendance(true);
                          setRecording(false);
                        }}
                        style={{ color: '#4C8BF5' }}
                      >
                        Attendance
                      </Button>
                    </Box>
                    {recording === false ? (
                      ''
                    ) : (
                      <Box
                        width="100%"
                        minHeight="360px"
                        display="flex"
                        justifyContent="center"
                        alignContent="center"
                      >
                        {individualMeetingInfo.isRecorded === false ? (
                          <h4 className={classes.typography_2}>
                            Meeting was not recorded
                          </h4>
                        ) : individualMeetingInfo.isRecordingDeleted ===
                          true ? (
                          <h4 className={classes.typography_2}>
                            Recording was deleted by Tutor
                          </h4>
                        ) : (
                          <Box
                            display="flex"
                            flexDirection="column"
                            alignItems="center"
                            style={{ paddingTop: '70px' }}
                          >
                            <img
                              style={{ height: '240px', width: '420px' }}
                              src={thumbnail}
                              alt="ThumbNail"
                            />
                            <Box
                              width="420px"
                              marginTop="12px"
                              display="flex"
                              justifyContent="space-evenly"
                              alignItems="center"
                            >
                              <Button
                                onClick={() =>
                                  viewRecording(
                                    individualMeetingInfo?.recordingURL
                                  )
                                }
                                variant="outlined"
                                size="small"
                                style={{
                                  width: '100px',
                                  color: '#fff',
                                  border: '1px solid #4C8BF5'
                                }}
                              >
                                Play  <PlayCircleFilledWhiteIcon fontSize='small'/>
                              </Button>
                            </Box>
                          </Box>
                        )}
                      </Box>
                    )}
                    {attendance === false ? (
                      ''
                    ) : (
                      <Box>
                        {attandanceChart.length === 0 ? (
                            ''
                          ) : (
                            <Box padding="1px 0px 9px 0px">
                              <StudentAttendanceDataTable gridData={attandanceChart} />
                            </Box>
                          )}
                      </Box>
                    )}
                  </Box>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </MainLayoutBBB>
  );
};

function mapStateToProps(state: RootState) {
  return {
    meetingState: state.meetingReducer
  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(StudentMeetingDashboard)
);
