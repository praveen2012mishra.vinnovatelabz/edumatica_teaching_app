import React, { FunctionComponent } from 'react'
import { Box, Theme } from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';

const styles = (theme: Theme) => createStyles({
  layout: {
    padding: '12px',
    marginTop: '8px',
    height: "100%",
  }
});

interface Props extends WithStyles<typeof styles> {
  
}

const MainLayoutBBB: FunctionComponent<Props> = ({children, classes}) => {
  return (
    <div>
      <MiniDrawer>
      <Box className={classes.layout}>
        {children}
      </Box>
      </MiniDrawer>
    </div>
  )
}

export default withStyles(styles)(MainLayoutBBB)
