import React, { FunctionComponent, useEffect, useState } from 'react';
import { Box, FormControl, Grid, Select, MenuItem, Typography } from '@material-ui/core';
import Button from '../../common/components/form_elements/button';
import Modal from '../../common/components/modal';
import { Role } from '../../common/enums/role';

interface RoleSelect {
  _id: string;
  role: Role;
}

interface AllOwners {
  _id: string;
  ownerId: string;
  roles: RoleSelect[];
}

interface Props {
  openModal: boolean;
  handleClose: () => any;
  handleSelectRole: (role: string) => any;
  roles: AllOwners[];
}

const SelectRoleModal: FunctionComponent<Props> = ({
  openModal,
  handleClose,
  handleSelectRole,
  roles
}) => {
  const [role, setRole] = useState<string>('');

  useEffect(() => {
    if(roles.length > 0) {
      const defaultRoles = roles.map(list => {
        return list.roles.filter(ownrole => ownrole._id).map(ownrole => list._id + '|||' + ownrole._id)
      }).flat()

      if(defaultRoles.length === 1) {
        setRole(defaultRoles[0])
      }
    }
  },[roles])

  const handleFormSubmission = (e: React.FormEvent) => {
    e.preventDefault();
    if(role){
      handleSelectRole(role);
    }
  };

  return (
    <Modal
      open={openModal}
      handleClose={() => {
        setRole('')
        handleClose()
      }}
      header={
        <Box display="flex" alignItems="center">
          <Box marginLeft="15px">
            <Typography component="span" color="secondary">
              <Box component="h3" color="white" fontWeight="400" margin="0">
                Please Select a Role
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <form onSubmit={handleFormSubmission}>
        <Grid container>
          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              {(roles.length > 0) &&
                <Select
                  value={role}
                  onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                  setRole(e.target.value as string)
                  }
                  displayEmpty
                >
                  {roles.length > 0 && roles.map((item) => {
                    return(item.roles.map((elem) => {
                      const roleName = elem.role.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ')
                      return(
                        <MenuItem key={item._id + '|||' + elem._id} value={item._id + '|||' + elem._id} >
                          From {item.ownerId} as {roleName}
                        </MenuItem>
                      )
                    }))
                  })}
                </Select>
              }
            </FormControl>
          </Grid>
        </Grid>

        <Box display="flex" justifyContent="flex-end" marginTop="10px">
          <Button
            disableElevation
            variant="contained"
            color="primary"
            type="submit"
          >
            Submit
          </Button>
        </Box>
      </form>
    </Modal>
  );
};

export default SelectRoleModal;
