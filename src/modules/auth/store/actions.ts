import { createAction } from '@reduxjs/toolkit';
import { Role } from '../../common/enums/role';
import { User, Children } from '../../common/contracts/user';
// eslint-disable-next-line
import { permissions } from '../../common/contracts/user';
import { askForPermissioToReceiveNotifications } from '../../../../src/push-notification';
import { setDeviceToken } from '../../notifications/helper/api';
export const setAuthToken = createAction(
  'SET_AUTH_TOKEN',
  (accessToken: string) => {
    // We'll retain the access token in the local storage in case the
    // browser tab is refreshed then we'd still have the token to fetch the
    // user details of the authenticated user from it.
    localStorage.setItem('accessToken', accessToken);

    return {
      payload: accessToken
    };
  }
);

export const setRefreshToken = createAction(
  'SET_REFRESH_TOKEN',
  (refreshToken: string) => {
    // We'll retain the access token in the local storage in case the
    // browser tab is refreshed then we'd still have the token to fetch the
    // user details of the authenticated user from it.
    localStorage.setItem('refreshToken', refreshToken);

    return {
      payload: refreshToken
    };
  }
);

export const setAuthUser = createAction('SET_AUTH_USER', (user: User | {}) => {
  // We'll retain the authenticated user in the local storage in case the
  // browser tab is refreshed then we'd still have the details to fetch
  // the user details of the authenticated user from it.
  localStorage.setItem('authUser', JSON.stringify(user));
  askForPermissioToReceiveNotifications().then((token) => {
    let usr: any = user;
    if (token && usr._id) {
      // console.log(
      //   '',
      //   'user: ',
      //   usr._id,
      //   '\n',
      //   'user_token: ',
      //   token,
      //   '\n',
      //   'mobileNo: ',
      //   usr.mobileNo
      // );
      setDeviceToken(usr._id, usr.mobileNo, token).then((response) => {
        // console.log(response.data);
      });
    }
  });
  return {
    payload: user
  };
});

export const unsetAuthUser = createAction('USER_LOGOUT', () => {
  return {
    payload: ''
  };
});

export const setAuthUserPermissions = createAction(
  'SET_AUTH_USER_PERMISSIONS',
  (permissions: permissions) => {
    // We'll retain the authenticated user in the local storage in case the
    // browser tab is refreshed then we'd still have the details to fetch
    // the user details of the authenticated user from it.
    localStorage.setItem('authPermissions', JSON.stringify(permissions));
    return {
      payload: permissions
    };
  }
);

export const setAuthUserRole = createAction(
  'SET_AUTH_USER_ROLE',
  (role: Role | '') => {
    // We'll retain the authenticated user role in the local storage in case
    // the browser tab is refreshed then we'd still have the details to
    // fetch the user details of the authenticated user from it.
    localStorage.setItem('authUserRole', role);

    return {
      payload: role
    };
  }
);

export const setChildrenIfParent = createAction(
  'SET_CHILDREN_IF_PARENT',
  (children: Children | {}) => {
    // We'll retain the authenticated user role in the local storage in case
    // the browser tab is refreshed then we'd still have the details to
    // fetch the user details of the authenticated user from it.
    localStorage.setItem('parentChildren', JSON.stringify(children));

    return {
      payload: children
    };
  }
);
