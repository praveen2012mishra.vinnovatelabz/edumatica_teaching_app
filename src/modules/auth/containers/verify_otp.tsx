import React, { FunctionComponent, useEffect, useState } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import ConfirmationModal from '../../common/components/confirmation_modal';
import {
  Box,
  FormControl,
  Grid,
  Link,
  Theme,
  Typography
} from '@material-ui/core';
import OtpInput from 'react-otp-input';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { verifyOTP } from '../../common/api/auth';
import Button from '../../common/components/form_elements/button';
import { Role } from '../../common/enums/role';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { fontOptions } from '../../../theme';
import { RootState } from '../../../store';
import { connect } from 'react-redux';
import { useSnackbar } from 'notistack';
import EdumaticaLogo from '../../../assets/images/edumaticaLogo.png'
import SignupImg from '../../../assets/svgs/signup.svg'

const styles = (theme: Theme) =>
  createStyles({
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    },
    imageGrid: {
      height: '100vH',
      backgroundColor: 'rgba(34, 197, 248, 0.1)'
    },
    registerGrid: {
      height: '100vH',
      backgroundColor: '#FFFFFF'
    },
    edumaticaLogo: {
      width: '174x',
      height: '50px',
    },
    registerHead: {
      fontSize: fontOptions.size.mediumPlus,
      fontWeight: fontOptions.weight.bold,
      color: '#3D3D3D',
      paddingTop: '3px'
    },
    registerBtn: {
      paddingTop: '50px',
      '& button': {
        padding: '12px 60px 12px 60px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        color: '#FFFFFF',
        borderRadius: '3px',
      },
    },
  });

interface Props
  extends RouteComponentProps<{referralCode?: string;}> {}
interface Props extends WithStyles<typeof styles> {}

const OtpVerification: FunctionComponent<Props> = ({ classes, match, location }) => {
  const {enqueueSnackbar} = useSnackbar()
  const [redirectTo, setRedirectTo] = useState('');
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [orgCode, setOrgCode] = useState('')
  const [mobileNumber, setMobileNumber] = useState('')
  const [otp, setOtp] = useState('')
  const [role, setRole] = useState('')

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const orgCodeP = params.get('org');
    const mobileNumberP = params.get('phone');
    const otpP = params.get('otp');
    const roleP = params.get('role');

    if(!orgCodeP || !mobileNumberP || !otpP || !roleP) {
      enqueueSnackbar('Login Parameters missing', {variant: 'warning'});
      
    } else {
      setOrgCode(orgCodeP);
      setMobileNumber(mobileNumberP)
      setOtp(otpP)
      setRole(roleP)
    }
  }, [location.search])
  

  eventTracker(GAEVENTCAT.registration, 'Registration Page', 'Landed Registeration Page');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleVerifyOTP = async () => {
    if(!orgCode || !mobileNumber || !role) {
      enqueueSnackbar('Login Parameters missing', {variant: 'warning'});
      setRedirectTo('/login');
    } else if(otp.length !== 6) {
      enqueueSnackbar('Wrong OTP Format', {variant: 'warning'});
    } else {
      try {
        let userType = Role.TUTOR;
        switch (role) {
          case 'STUDENT':
            userType = Role.STUDENT;
            break;
          case 'TUTOR':
            userType = Role.TUTOR;
            break;
          case 'INSTITUTE':
            userType = Role.ORGANIZATION;
            break;
          case 'PARENT':
            userType = Role.PARENT;
            break;
          default:
            userType = Role.TUTOR;
        }
        await verifyOTP(orgCode, mobileNumber, otp, userType, false);
        eventTracker(GAEVENTCAT.registration, 'Registration Page', 'Registeration Started');

        setRedirectTo(
          `/register/security?org=${orgCode}&phone=${mobileNumber}&otp=${otp}&role=${role}`
        );
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if((error.response?.status === 422) && (error.response?.data.message === "Mobile Number Already Registered")) {
          setOpenConfirmationModal(true)
        }
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
  };

  const addRoleToExistingUser = async () => {
    if(!orgCode || !mobileNumber || !role) {
      enqueueSnackbar('Login Parameters missing', {variant: 'warning'});
      setRedirectTo('/login');
    } else if(otp.length !== 6) {
      enqueueSnackbar('Wrong OTP Format', {variant: 'warning'});
    } else {
      try {
        eventTracker(GAEVENTCAT.registration, 'Registration Page', 'Registeration Started');
        setRedirectTo(
          `/register/security/confirm?org=${orgCode}&phone=${mobileNumber}&otp=${otp}&role=${role}`
        );
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
  }

  return (
    <div>
      <Grid container>
        <Grid item xs={12} md={6} className={classes.imageGrid}>
          <Box padding="30px" height="100%">
            <Box style={{height: '100%'}}>
              <img className={classes.edumaticaLogo} src={EdumaticaLogo} alt="Logo" />
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '95%', textAlign: 'center' }}
              >
                <Grid item xs={12}>
                  <img  src={SignupImg} alt="Signup" />
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.registerGrid}>
          <Box paddingY="30px" paddingX="100px" height="100%">
            <Box style={{height: '100%'}}>
              <Typography className={classes.registerHead}>Verify your account</Typography>
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '90%' }}
              >
                <Grid item xs={12}>
                  <form>
                    <Typography style={{color: '#666666', fontSize: fontOptions.size.small}}>
                      We have sent you a 6-character code to mobile number, so check it and enter as soon as possible
                    </Typography>
                    <FormControl fullWidth margin="normal" style={{paddingTop: '70px'}}>
                      <OtpInput
                        value={otp}
                        onChange={setOtp}
                        numInputs={6}
                        inputStyle={{fontSize: fontOptions.size.xLarge, width: '50%', border: '2px solid #666666'}}
                        separator={<span>{` `}</span>}
                      />
                    </FormControl>
                    <FormControl margin="normal">
                      <Box fontSize={fontOptions.size.small}>
                        <span style={{paddingRight: '6px'}}>Can't find a code</span>
                        <Link color="primary" style={{textDecorationLine: 'underline'}}>
                          Send again
                        </Link>
                      </Box>
                    </FormControl>

                    <Box className={classes.registerBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="large"
                        onClick={handleVerifyOTP}
                      >
                        Verify
                      </Button>
                    </Box>
                  </form>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
      <ConfirmationModal
        header="Mobile Number Already Registered"
        helperText="Continue adding a new Role ?"
        openModal={openConfirmationModal}
        onClose={() => setOpenConfirmationModal(false)}
        handleDelete={addRoleToExistingUser}
      />
    </div>
  )
};

const mapStateToProps = (state: RootState) => ({

});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(OtpVerification))
);
