import React, { FunctionComponent, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link as RouterLink, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { RootState } from '../../../store';
import { connect } from 'react-redux';
import * as yup from 'yup';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  Link,
  Theme,
  Typography
} from '@material-ui/core';
import EdumaticaLogo from '../../../assets/images/edumaticaLogo.png'
import { MobileFriendly as MobileFriendlyIcon } from '@material-ui/icons';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { loginUser, verifyCaptcha } from '../../common/api/auth';
import { getStudent, getTutor, getParent } from '../../common/api/tutor';
import { getOrganization } from '../../common/api/organization';
import { getAdmin } from '../../common/api/admin';
import { fetchParentStudentsList } from '../../common/api/academics';
import {
  setAuthToken,
  setAuthUser,
  setAuthUserPermissions,
  setAuthUserRole,
  setRefreshToken,
  setChildrenIfParent
} from '../store/actions';
import {
  PHONE_PATTERN,
  PASSWORD_PATTERN,
  PASSWORD_LENGTH
} from '../../common/validations/patterns';
import { Role } from '../../common/enums/role';

import Button from '../../common/components/form_elements/button';
import PasswordVisibilityButton from '../../common/components/password_visibility_button';
import ReCAPTCHA from 'react-google-recaptcha';
import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../common/helpers';
import { fontOptions } from '../../../theme';
import {useSnackbar} from "notistack"
import { sortBy } from 'lodash'
import LoginImg from '../../../assets/svgs/login.svg'

const styles = (theme: Theme) =>
  createStyles({
    navLink: {
      fontFamily:fontOptions.family,
      fontSize: fontOptions.size.small,
      lineHeight: '134.69%',
      letterSpacing: '0.0125em',
      textDecorationLine: 'underline'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    },
    imageGrid: {
      height: '100vH',
      backgroundColor: 'rgba(34, 197, 248, 0.1)'
    },
    loginGrid: {
      height: '100vH',
      backgroundColor: '#FFFFFF'
    },
    edumaticaLogo: {
      width: '174x',
      height: '50px',
    },
    loginHead: {
      fontSize: fontOptions.size.mediumPlus,
      fontWeight: fontOptions.weight.bold,
      color: '#3D3D3D',
      paddingTop: '3px'
    },
    loginBtn: {
      paddingTop: '50px',
      '& button': {
        padding: '12px 60px 12px 60px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        color: '#FFFFFF',
        borderRadius: '3px',
      },
    },
  });

interface Props
  extends RouteComponentProps<{usertype?: string;}> {}
interface Props extends WithStyles<typeof styles> {}

interface RoleSelect {
  _id: string;
  role: Role;
}

interface AllOwners {
  _id: string;
  ownerId: string;
  roles: RoleSelect[];
}

interface FormData {
  mobileNumber: string;
  password: string;
  serverError: string;
}

const ValidationSchema = yup.object().shape({
  mobileNumber: yup
    .string()
    .required('mobile number is a required field')
    .matches(PHONE_PATTERN, 'mobile number is invalid'),
  password: yup
    .string()
    .required()
    .matches(
      PASSWORD_PATTERN,
      'Password must contain at least one uppercase, lowercase, alphanumeric & special character'
    )
    .matches(
      PASSWORD_LENGTH,
      'Password must contain at least 8 characters'
    )
});

interface RedirectProp {
  pathname: string;
  state: {
    allRoles: AllOwners[];
    username: any;
  }
}

const Login: FunctionComponent<Props> = ({ classes }) => {
  const { handleSubmit, register, errors, setError, clearError } = useForm<
    FormData
  >({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const {enqueueSnackbar} = useSnackbar()
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isCaptchaVisible, setCaptchaVisible] = useState(false);
  const [isLoginButtonDisabled, setLoginButtonDisabled] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const [spRedirectTo, setSpRedirectTo] = useState<RedirectProp>()
  const [captcha, setCaptcha] = useState<ReCAPTCHA | null>();
  const recaptchaKey = process.env.REACT_APP_RECAPTCHA_KEY
    ? process.env.REACT_APP_RECAPTCHA_KEY
    : '';
  const dispatch = useDispatch();
  const [regLink, setRegLink] = useState('/register')

  const onChange = async (value: string | null) => {
    clearError('serverError');
    if (value) {
      try {
        const response = await verifyCaptcha(value);
        if (response?.data.code === 0) {
          setLoginButtonDisabled(false);
        } else {
          setError('serverError', 'Server response', response?.data.message);
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
      }
    }
  };

  const handleLogin = async ({ mobileNumber, password }: FormData) => {
    clearError('serverError');
    try {
      const response = await loginUser(mobileNumber, password);
      const user = response.data;
      const allOwners = user.owner;
      
      dispatch(setAuthToken(user.accessToken));
      dispatch(setRefreshToken(user.refreshToken));
      dispatch(setChildrenIfParent({}));
      localStorage.setItem('authUserRoles', JSON.stringify({allOwners}));

      if(user.owner.length > 1 || user.owner[0].roles.length > 1) {       
        const allRoles = sortBy(user.owner.map((elem: any) => {
          return {
            _id: elem._id,
            ownerId: elem.ownerId,
            roles: sortBy(elem.roles.map((role: any) => {
              return {
                _id: role._id,
                role: String(role.role.name).substring(5)
              }
            }), '_id')
          }
        }), '_id')

        setSpRedirectTo({pathname: '/login/chooserole', state: {allRoles: allRoles, username: user.username}})
        return;
      }

      const currentOwner = allOwners[0]
      const currentRole = currentOwner.roles[0];
      const currentRolename = currentRole.role.name;
      const ownerRoleCombi = currentOwner._id + '|||' + currentRole._id;
      dispatch(setAuthUserRole(currentRolename));
      dispatch(setAuthUserPermissions(currentRole.permissions));
      localStorage.setItem('authUserRoles', JSON.stringify({allOwners, ownerRoleCombi}));

      if (currentRolename === Role.ADMIN) {
        try {
          const admin = await getAdmin();
          const updatedUser = {...admin}
          dispatch(setAuthUser(updatedUser));
          eventTracker(GAEVENTCAT.auth, 'Admin Login', 'Login Success');
          setRedirectTo(`/profile/dashboard`);
        } catch (error) {
          exceptionTracker(error.response?.data.message);
        }

        return;
      } else if(currentRolename === Role.ORGANIZATION) {
        try {
          const organization = await getOrganization();
          const updatedUser = {...organization}
          dispatch(setAuthUser(updatedUser));
          eventTracker(GAEVENTCAT.auth, 'Organization Login', 'Login Success');
          setRedirectTo(`/profile/dashboard`);
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          dispatch(setAuthUser({ mobileNo: mobileNumber }));
          // Redirect the user to complete his profile, if he has recently
          // registered or not yet filled his profile up.
          setRedirectTo(`/profile/org/process/1`);
        }

        return;
      } else if(currentRolename === Role.TUTOR || currentRolename === Role.ORG_TUTOR) {
        try {
          const tutor = await getTutor();
          const updatedUser = {...tutor}
          dispatch(setAuthUser(updatedUser));
          eventTracker(
            GAEVENTCAT.auth,
            currentRolename === Role.TUTOR ? 'Tutor Login' : 'Org Tutor Login',
            'Login Success'
          );
          setRedirectTo(`/profile/dashboard`);
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          dispatch(setAuthUser({ mobileNo: mobileNumber }));
          // Redirect the user to complete his profile, if he has recently
          // registered or not yet filled his profile up.
          setRedirectTo(`/profile/process/1`);
        }

        return;
      } else if(currentRolename === Role.STUDENT) {
        const student = await getStudent();
        const updatedUser = {...student}
        if(student.roleStatus && (student.roleStatus === 'ACTIVE')) {
          dispatch(setAuthUser(updatedUser));
          eventTracker(GAEVENTCAT.auth, 'Student Login', 'Login Success');
          setRedirectTo(`/profile/dashboard`);
        } else if(student.roleStatus && (student.roleStatus === 'DEACTIVATE')) {
          dispatch(setAuthUser({ mobileNo: mobileNumber }));
          setRedirectTo(`/profile/deactivated`);
        } else {
          dispatch(setAuthUser(updatedUser));
          setRedirectTo(`/profile/student/process/1`);
        }

        return;
      } else if (currentRolename === Role.PARENT) {
        const parent = await getParent();
        const updatedUser = {...parent}
        const children = await fetchParentStudentsList();
        const parentChildren = {
          current: children[0]._id,
          children: children
        }
        if(parent.roleStatus && (parent.roleStatus === 'ACTIVE')) {
          dispatch(setAuthUser(updatedUser));
          dispatch(setChildrenIfParent(parentChildren));
          eventTracker(GAEVENTCAT.auth, 'Parent Login', 'Login Success');
          setRedirectTo(`/profile/dashboard`);
        } else if(parent.roleStatus && (parent.roleStatus === 'DEACTIVATE')) {
          dispatch(setAuthUser({ mobileNo: mobileNumber }));
          setRedirectTo(`/profile/deactivated`);
        } else {
          dispatch(setAuthUser(updatedUser));
          dispatch(setChildrenIfParent(parentChildren));
          setRedirectTo(`/profile/parent/process/1`);
        }
      }

    } catch (error) {
      exceptionTracker(error.response?.data.message);
      enqueueSnackbar(error.response?.data.message,{variant:'warning'})
      mobileNumber = '';
      password = '';
      captcha?.reset();
      setCaptchaVisible(true);
      setLoginButtonDisabled(true);
    }
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if (spRedirectTo) {
    return <Redirect to={spRedirectTo} />;
  }

  return (
    <div>
      <Grid container>
        <Grid item xs={12} md={6} sm={12} className={classes.imageGrid}>
          <Box padding="30px" height="100%">
            <Box style={{height: '100%'}}>
              <img className={classes.edumaticaLogo} src={EdumaticaLogo} alt="Logo" />
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '95%', textAlign: 'center' }}
              >
                <Grid item xs={12}>
                  <img  src={LoginImg} alt="Login" style={{width:'100%'}}/>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={6} sm={12} className={classes.loginGrid}>
          <Box paddingY="10%" paddingX="25%" height="100%">
            <Box style={{height: '100%'}}>
              <Typography className={classes.loginHead}>Login</Typography>
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '95%' }}
              >
                <Grid item xs={12}>
                  <form onSubmit={handleSubmit(handleLogin)}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        name="mobileNumber"
                        inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                        placeholder="Mobile Number"
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton disabled size="small">
                              <MobileFriendlyIcon />
                            </IconButton>
                          </InputAdornment>
                        }
                        inputRef={register}
                        className={errors.mobileNumber ? classes.error : ''}
                      />
                      {errors.mobileNumber && (
                        <FormHelperText error>
                          {errors.mobileNumber.message}
                        </FormHelperText>
                      )}
                    </FormControl>

                    <FormControl fullWidth margin="normal">
                      <Input
                        name="password"
                        placeholder="Password"
                        inputProps={{ maxLength: 50 }}
                        type={isPasswordVisible ? 'text' : 'password'}
                        endAdornment={
                          <InputAdornment position="end">
                            <PasswordVisibilityButton
                              isVisible={isPasswordVisible}
                              handleChange={(isVisible) =>
                                setIsPasswordVisible(isVisible)
                              }
                            />
                          </InputAdornment>
                        }
                        inputRef={register}
                        className={errors.password ? classes.error : ''}
                      />
                      {errors.password && (
                        <FormHelperText error>
                          {errors.password.message}
                        </FormHelperText>
                      )}
                    </FormControl>
                    <Box
                      textAlign="right"
                      marginBottom="5px"
                      className={classes.navLink}
                    >
                      <Link
                        color="primary"
                        to="/forgot-password"
                        component={RouterLink}
                      >
                        Forgot Password?
                      </Link>
                    </Box>
                    {isCaptchaVisible && (
                      <FormControl fullWidth margin="normal">
                        <ReCAPTCHA
                          ref={(e) => setCaptcha(e)}
                          sitekey={recaptchaKey}
                          onChange={onChange}
                        />
                      </FormControl>
                    )}
                    <Box className={classes.loginBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="large"
                        type="submit"
                        disabled={isLoginButtonDisabled}
                      >
                        Login
                      </Button>
                    </Box>
                    {errors.serverError && (
                      <FormHelperText error>
                        {errors.serverError.message}
                      </FormHelperText>
                    )}
                    <FormControl margin="normal">
                      <Box fontSize={fontOptions.size.small}>
                        <span style={{paddingRight: '6px'}}>Don't have an account?</span>
                        <Link color="primary" to={regLink} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                          Signup
                        </Link>
                      </Box>
                    </FormControl>
                  </form>
                </Grid>   
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Box
          position="absolute"
          bottom="20px"
          left="20px"
          color="#666666"
          fontFamily={fontOptions.family}
          fontSize={fontOptions.size.small}
        >
          V 1.0
        </Box>
      </Grid>
    </div>
  )
};

const mapStateToProps = (state: RootState) => ({

});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(Login))
);
