import { Box, CircularProgress } from '@material-ui/core'
import { createStyles, withStyles, WithStyles } from '@material-ui/styles'
import { useSnackbar } from 'notistack'
import React, { FunctionComponent, useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { match, RouteComponentProps, useHistory, withRouter } from 'react-router'
import { RootState } from '../../../store'
import { verifyEmail } from '../../common/api/auth'

const styles = createStyles({

})

interface Props extends WithStyles<typeof styles> {}
interface Props
  extends RouteComponentProps<{
    role: string;
    entityId: string;
    token: string;
  }> {
}

const ConfirmEmail: FunctionComponent<Props> = ({classes, match}) => {
  const role= match.params.role;
  const entityId= match.params.entityId;
  const token= match.params.token; 
  const [userType, setUserType] = useState<string>('')
  const history = useHistory()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
        verifyEmail(userType, entityId, token).then(response => {
          if(response.code === 0) {
            enqueueSnackbar("Email verified successfully.", {
              variant: 'success', preventDuplicate: true, persist: false,
            });
            history.push('/login')
          } else{
            enqueueSnackbar("Email verification unsuccessful.", {
              variant: 'warning', preventDuplicate: true, persist: false,
            });
            history.push('/login')
          }
        }).catch(err => {
          enqueueSnackbar("Email verification unsuccessful.", {
            variant: 'warning', preventDuplicate: true, persist: false,
          });
          history.push('/login')
        })
  }, [userType]);

  useEffect(() => {
    if(role === "tutor") setUserType("ROLE_TUTOR")
    if(role === "organization") setUserType("ROLE_ORGANIZATION")
    if(role === "student") setUserType("ROLE_STUDENT")
  }, [])

  return (
    <Box height="100vH" display="flex" flexDirection="column" justifyContent="center" alignItems="center">
      <CircularProgress />
      <p style={{fontSize:"14px"}}>Your email is being verified.</p>
    </Box>
  )
}

const mapStateToProps = (state: RootState) => ({

});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(ConfirmEmail))
);
