import React, { FunctionComponent, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { RootState } from '../../../store';
import { connect } from 'react-redux';
import {
  Box,
  Grid,
  Theme,
  Typography
} from '@material-ui/core';
import Scrollbars from 'react-custom-scrollbars';
import EdumaticaLogo from '../../../assets/images/edumaticaLogo.png'
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { getStudent, getTutor, getParent } from '../../common/api/tutor';
import { getOrganization } from '../../common/api/organization';
import { getAdmin } from '../../common/api/admin';
import { fetchParentStudentsList } from '../../common/api/academics';
import {
  setAuthUser,
  setAuthUserPermissions,
  setAuthUserRole,
  setChildrenIfParent
} from '../store/actions';
import { Role } from '../../common/enums/role';

import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../common/helpers';
import { fontOptions } from '../../../theme';
import {useSnackbar} from "notistack"
import StudentImg from '../../../assets/svgs/student_role.svg'
import TutorImg from '../../../assets/svgs/tutor_role.svg'

const styles = (theme: Theme) =>
  createStyles({
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    },
    imageGrid: {
      height: '100vH',
      backgroundColor: '#FFFFFF'
    },
    loginGrid: {
      height: '100vH',
      backgroundColor: '#FFFFFF'
    },
    edumaticaLogo: {
      width: '174x',
      height: '50px',
    },
    loginHead: {
      fontSize: fontOptions.size.mediumPlus,
      fontWeight: fontOptions.weight.bold,
      color: '#3D3D3D',
      paddingTop: '3px'
    },
    loginBtn: {
      paddingTop: '50px',
      '& button': {
        padding: '12px 60px 12px 60px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        color: '#FFFFFF',
        borderRadius: '3px',
      },
    },
    roleBox: {
      '&:hover': {
        backgroundColor: '#E0E0E0',
        cursor: 'pointer'
      }
    }
});

interface RoleSelect {
  _id: string;
  role: Role;
}

interface AllOwners {
  _id: string;
  ownerId: string;
  roles: RoleSelect[];
}

interface Props  {
  history: object,
  location: {
    state: {
      allRoles: AllOwners[];
      username: any;
    }
  },
  match: object
}

interface Props extends WithStyles<typeof styles> {}

const RoleSelection: FunctionComponent<Props> = ({ classes, location }) => {
  const {enqueueSnackbar} = useSnackbar()
  const [redirectTo, setRedirectTo] = useState('');
  const dispatch = useDispatch();
  const [roles, setRoles] = useState<AllOwners[]>([]);
  const [username, setUsername] = useState('');

  useEffect(() => {
    if(location.state.allRoles.length > 0) {
      setRoles(location.state.allRoles)
      setUsername(location.state.username)
    }
  }, [location.state])

  const handleSelectRole = async (role: string) => {
    const allOwnersString = localStorage.getItem('authUserRoles');
    const [selectedOwner, selectedRole] = role.split('|||')
    if(allOwnersString) {
      const { allOwners } = JSON.parse(allOwnersString);
      const currentOwner = allOwners.find((own: any) => own._id === selectedOwner)
      const currentRole = currentOwner.roles.find((rol: any) => rol._id === selectedRole);
      const currentRolename = currentRole.role.name;
      const ownerRoleCombi = currentOwner._id + '|||' + currentRole._id;
      dispatch(setAuthUserRole(currentRolename));
      dispatch(setAuthUserPermissions(currentRole.permissions));
      localStorage.setItem('authUserRoles', JSON.stringify({allOwners, ownerRoleCombi}));
      try {
        if (currentRolename === Role.ADMIN) {
          try {
            const admin = await getAdmin();
            const updatedUser = {...admin}
            dispatch(setAuthUser(updatedUser));
            eventTracker(GAEVENTCAT.auth, 'Admin Login', 'Login Success');
            setRedirectTo(`/profile/dashboard`);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
          }

          return;
        } else if(currentRolename === Role.ORGANIZATION) {
          try {
            const organization = await getOrganization();
            const updatedUser = {...organization}
            dispatch(setAuthUser(updatedUser));
            eventTracker(GAEVENTCAT.auth, 'Organization Login', 'Login Success');
            setRedirectTo(`/profile/dashboard`);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
            dispatch(setAuthUser({ mobileNo: username }));
            // Redirect the user to complete his profile, if he has recently
            // registered or not yet filled his profile up.
            setRedirectTo(`/profile/org/process/1`);
          }

          return;
        } else if(currentRolename === Role.TUTOR || currentRolename === Role.ORG_TUTOR) {
          try {
            const tutor = await getTutor();
            const updatedUser = {...tutor}
            dispatch(setAuthUser(updatedUser));
            eventTracker(
              GAEVENTCAT.auth,
              currentRolename === Role.TUTOR ? 'Tutor Login' : 'Org Tutor Login',
              'Login Success'
            );
            setRedirectTo(`/profile/dashboard`);
          } catch (error) {
            exceptionTracker(error.response?.data.message);
            dispatch(setAuthUser({ mobileNo: username }));
            // Redirect the user to complete his profile, if he has recently
            // registered or not yet filled his profile up.
            setRedirectTo(`/profile/process/1`);
          }

          return;
        } else if(currentRolename === Role.STUDENT) {
          const student = await getStudent();
          const updatedUser = {...student}
          if(student.roleStatus && (student.roleStatus === 'ACTIVE')) {
            dispatch(setAuthUser(updatedUser));
            eventTracker(GAEVENTCAT.auth, 'Student Login', 'Login Success');
            setRedirectTo(`/profile/dashboard`);
          } else if(student.roleStatus && (student.roleStatus === 'DEACTIVATE')) {
            dispatch(setAuthUser({ mobileNo: username }));
            setRedirectTo(`/profile/deactivated`);
          } else {
            dispatch(setAuthUser(updatedUser));
            setRedirectTo(`/profile/student/process/1`);
          }

          return;
        } else if (currentRolename === Role.PARENT) {
          const parent = await getParent();
          const updatedUser = {...parent}
          const children = await fetchParentStudentsList();
          const parentChildren = {
            current: children[0]._id,
            children: children
          }
          if(parent.roleStatus && (parent.roleStatus === 'ACTIVE')) {
            dispatch(setAuthUser(updatedUser));
            dispatch(setChildrenIfParent(parentChildren));
            eventTracker(GAEVENTCAT.auth, 'Parent Login', 'Login Success');
            setRedirectTo(`/profile/dashboard`);
          } else if(parent.roleStatus && (parent.roleStatus === 'DEACTIVATE')) {
            dispatch(setAuthUser({ mobileNo: username }));
            setRedirectTo(`/profile/deactivated`);
          } else {
            dispatch(setAuthUser(updatedUser));
            dispatch(setChildrenIfParent(parentChildren));
            setRedirectTo(`/profile/parent/process/1`);
          }
        }
        
      } catch (error) {
        exceptionTracker(error.response?.data.message);
      }
    }
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <Grid container>
        <Grid item xs={12} className={classes.imageGrid}>
          <Box paddingY="30px" paddingX="100px" height="100%">
            <Box style={{height: '100%', textAlign: 'center'}}>
              <img className={classes.edumaticaLogo} src={EdumaticaLogo} alt="Logo" />
              <Box style={{height: '90%'}}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  style={{ height: '100%' }}
                >
                  <Grid item xs={12}>
                    <Typography className={classes.loginHead}>Select Your Role</Typography>
                    <Scrollbars autoHeight autoHeightMax="400px" autoHeightMin="400px">
                      <Grid container>
                        {roles.length > 0 && roles.map((item) => {
                          return(item.roles.map((elem) => {
                            const roleName = elem.role.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ')
                            return(
                              <Grid item xs={12} md={6}>
                                <Box style={{height: '100%', paddingTop: '40px', paddingLeft: '40px', paddingRight: '40px'}}>
                                  <Grid
                                    container
                                    style={{ height: '350px', border: '2px solid #4C8BF5', borderRadius: '10px'}}
                                    className={classes.roleBox}
                                    onClick={() => {handleSelectRole(item._id + '|||' + elem._id)}}
                                  >
                                    <Grid
                                      container
                                      direction="row"
                                      alignItems="center"
                                      justify="center"
                                      style={{ height: '255px', textAlign: 'center'}}
                                    >
                                      <Grid item xs={12}>
                                        <img src={roleName === 'Student' ? StudentImg : TutorImg} alt="Role" />
                                      </Grid>
                                    </Grid>
                                    <Box textAlign="center" width="100%">
                                      <Typography style={{fontSize: fontOptions.size.mediumPlus, fontWeight: fontOptions.weight.bold, color: '#3D3D3D'}}>
                                        {roleName}
                                      </Typography>
                                      <Typography style={{fontSize: fontOptions.size.medium, color: '#666666'}}>
                                        {item.ownerId}
                                      </Typography>
                                    </Box>
                                </Grid>
                                </Box>
                              </Grid>
                            )
                          }))
                        })}
                      </Grid>
                    </Scrollbars>
                  </Grid>   
                </Grid>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  )
};

const mapStateToProps = (state: RootState) => ({

});

export default withStyles(styles)(
  connect(mapStateToProps)(RoleSelection)
);
