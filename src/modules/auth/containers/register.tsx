import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  MenuItem,
  Select,
  Link,
  Theme,
  Typography
} from '@material-ui/core';
import { MobileFriendly as MobileFriendlyIcon } from '@material-ui/icons';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { generateOTP, checkOrgCode } from '../../common/api/auth';
import {
  PHONE_PATTERN,
  ORG_CODE_PATTERN,
  ORG_CODE_LENGTH
} from '../../common/validations/patterns';
import Button from '../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { fontOptions } from '../../../theme';
import { RootState } from '../../../store';
import { connect } from 'react-redux';
import { useSnackbar } from 'notistack';
import EdumaticaLogo from '../../../assets/images/edumaticaLogo.png'
import SignupImg from '../../../assets/svgs/signup.svg'

const styles = (theme: Theme) =>
  createStyles({
    navLink: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '134.69%',
      letterSpacing: '0.0125em',
      fontWeight: fontOptions.weight.bold
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    },

    imageGrid: {
      height: '100vH',
      backgroundColor: 'rgba(34, 197, 248, 0.1)'
    },
    registerGrid: {
      height: '100vH',
      backgroundColor: '#FFFFFF'
    },
    edumaticaLogo: {
      width: '174x',
      height: '50px',
    },
    registerHead: {
      fontSize: fontOptions.size.mediumPlus,
      fontWeight: fontOptions.weight.bold,
      color: '#3D3D3D',
      paddingTop: '3px'
    },
    registerBtn: {
      paddingTop: '50px',
      '& button': {
        padding: '12px 60px 12px 60px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        color: '#FFFFFF',
        borderRadius: '3px',
      },
    },
  });

interface Props
  extends RouteComponentProps<{referralCode?: string;}> {}
interface Props extends WithStyles<typeof styles> {}

interface FormData {
  orgCode: string;
  mobileNumber: string;
  accountType: string;
  serverError: string;
}

const ValidationSchema = yup.object().shape({
  orgCode: yup
    .string()
    .required('Code is a required field')
    .matches(
      ORG_CODE_PATTERN,
      'Only alphanumerics and _(underscore) characters starting with an alphabet allowed. '
    )
    .matches(
      ORG_CODE_LENGTH,
      'Code should have atleast 5 Characters'
    ),
  mobileNumber: yup
    .string()
    .required('mobile number is a required field')
    .matches(PHONE_PATTERN, 'mobile number is invalid'),
});

const Register: FunctionComponent<Props> = ({ classes, match, location }) => {
  const {
    errors,
    getValues,
    handleSubmit,
    register,
    setError,
    clearError,
  } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const [role, setRole] = useState('');
  const [roles] = useState<string[]>([
    'STUDENT',
    'TUTOR',
    'PARENT',
    'INSTITUTE'
  ]);
  const {enqueueSnackbar} = useSnackbar()
  const [referral, setReferral] = useState<string | null>(null)
  const [redirectTo, setRedirectTo] = useState('');
  const [ownerError, setOwnerError] = useState(false);
  
  useEffect(() => {
    if(match.params.referralCode) {
      setReferral(match.params.referralCode)
      enqueueSnackbar('Referral Code detected', {
        variant: 'info'
      });
    }
  }, [])

  eventTracker(GAEVENTCAT.registration, 'Registration Page', 'Landed Registeration Page');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const checkCodeAvailability = async () => {
    const orgCode = getValues('orgCode');
    if (!orgCode) {
      return setError('orgCode', 'required', 'Code is a required field');
    }

    try {
      const response = await checkOrgCode(orgCode);
      if(response.data.code === 0) {
        enqueueSnackbar('This Code is NOT Registered with Edumatica', {variant: 'info'});
      } else {
        enqueueSnackbar(response.data.message, {variant: 'info'});
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const handleGenerateOTP = async () => {
    const mobileNumber = getValues('mobileNumber');
    const orgCode = getValues('orgCode')
    if (!mobileNumber) {
      return setError(
        'mobileNumber',
        'required',
        'mobile number is a required field'
      );
    } else {
      clearError('mobileNumber');
    }
    if (!role) {
      return setError('accountType', 'required', 'Select Account-Type');
    } else {
      clearError('accountType');
    }

    try {
      const response = await generateOTP('+91', mobileNumber);

      eventTracker(GAEVENTCAT.registration, 'Registration Page', 'Mobile OTP Sent');

      // TODO: Remove this in production.
      setRedirectTo(
        `/register/verifyotp?org=${orgCode}&phone=${mobileNumber}&otp=${response.data.otp}&role=${role}`
      );
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <Grid container style={{position:'relative'}} className='sign-up'>
        <Grid item xs={12} md={6} className={classes.imageGrid}>
          <Box padding="30px" height="100%">
            <Box style={{height: '100%'}}>
              <img className={classes.edumaticaLogo} src={EdumaticaLogo} alt="Logo" />
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '95%', textAlign: 'center' }}
              >
                <Grid item xs={12}>
                  <img  src={SignupImg} alt="Signup" style={{width:'100%'}}/>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={6} className={classes.registerGrid}>
          <Box paddingY="10%" paddingX="25%" height="100%">
            <Box style={{height: '100%'}}>
              <Typography className={classes.registerHead}>Signup</Typography>
              <Grid
                container
                direction="row"
                justify="center"
                className={'signupBox'}
              >
                <Grid item xs={12}>
                  <form onSubmit={handleSubmit(handleGenerateOTP)}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        name="orgCode"
                        inputProps={{ maxLength: 10 }}
                        placeholder="Institute / Tutor Code"
                        inputRef={register}
                        onChange={() => {
                          if(ownerError) {
                            setOwnerError(false);
                          }
                        }}
                        className={errors.orgCode ? classes.error : ''}
                        endAdornment={
                          <Box textAlign="right" className={classes.navLink} style={{width: '100%', textDecorationLine: 'underline'}}>
                            <Link href="#" onClick={checkCodeAvailability}>
                              Check availability
                            </Link>
                          </Box>
                        }
                      />
                      {errors.orgCode && (
                        <FormHelperText error>
                          {errors.orgCode.message}
                        </FormHelperText>
                      )}
                    </FormControl>

                    <FormControl fullWidth margin="normal">
                      <Input
                        name="mobileNumber"
                        inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                        placeholder="Mobile Number"
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton disabled size="small">
                              <MobileFriendlyIcon />
                            </IconButton>
                          </InputAdornment>
                        }
                        inputRef={register}
                        className={errors.mobileNumber ? classes.error : ''}
                      />
                      {errors.mobileNumber && (
                        <FormHelperText error>
                          {errors.mobileNumber.message}
                        </FormHelperText>
                      )}
                    </FormControl>

                    <FormControl fullWidth margin="normal">
                      <Select
                        value={role}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setRole(e.target.value as string)
                        }
                        displayEmpty
                        className={errors.accountType ? classes.error : ''}
                      >
                        <MenuItem value="">Select account-type</MenuItem>
                        {roles.length > 0 &&
                          roles.map((item) => (
                            <MenuItem key={item} value={item}>{item}</MenuItem>
                          ))}
                      </Select>
                      {errors.accountType && (
                        <FormHelperText error>
                          {errors.accountType.message}
                        </FormHelperText>
                      )}
                    </FormControl>

                    <Box className={classes.registerBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="large"
                        type="submit"
                        disabled={role==='INSTITUTE' && ownerError}
                      >
                        Sign up
                      </Button>
                    </Box>
                    {errors.serverError && (
                      <FormHelperText error>
                        {errors.serverError.message}
                      </FormHelperText>
                    )}
                    <FormControl margin="normal">
                      <Box fontSize={fontOptions.size.small}>
                        <span style={{paddingRight: '6px'}}>Already have an account?</span>
                        <Link color="primary" to={'/login'} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                          Login
                        </Link>
                      </Box>
                    </FormControl>
                  </form>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Box
            position="absolute"
            bottom="50px"
            color="#666666"
            fontFamily={fontOptions.family}
            fontSize={fontOptions.size.small}
            paddingX="5%"
          >
            <Box>
              By tapping Login, you agree with our{' '}
              <Link
                align="left"
                target="_blank"
                to="/terms-conditions"
                color="secondary"
                component={RouterLink}
              >
                Terms
              </Link>
              , Learn how we process your data in our{' '}
              <Link
                align="left"
                target="_blank"
                to="/privacy-policy"
                color="secondary"
                component={RouterLink}
              >
                {' '}
                Privacy Policy
              </Link>
              , and{' '}
              <Link
                align="left"
                target="_blank"
                to="/privacy-policy"
                color="secondary"
                component={RouterLink}
              >
                {' '}
                Cookies Policy
              </Link>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  )
};

const mapStateToProps = (state: RootState) => ({

});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(Register))
);