import React, { FunctionComponent, useState } from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import {
  Box,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  InputAdornment,
  Link,
  Theme,
  Typography
} from '@material-ui/core';
import { MobileFriendly as MobileFriendlyIcon } from '@material-ui/icons';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { generateOTP, verifyOTP, verifyOTPPwd } from '../../common/api/auth';
import { OTP_PATTERN, PHONE_PATTERN } from '../../common/validations/patterns';
import BannerImage from '../../../assets/images/yellow-tshirt-guy.jpg';
import Button from '../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import './style.css'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      background:
        'radial-gradient(47.89% 47.89% at 64.2% 52.11%, rgba(41, 75, 100, 0) 0%, rgba(41, 75, 100, 0.27) 67.71%), url(' +
        BannerImage +
        ')',
      backgroundSize: 'cover',
      backgroundPosition: 'center right',
      backgroundRepeat: 'no-repeat',
      minHeight: '100vh',
      padding: '20px 0'
    },

    formContainer: {
      justifyContent: 'center',
      [theme.breakpoints.up('sm')]: {
        justifyContent: 'flex-start'
      }
    },
    boxLayout: {
      position: 'absolute',
      top: '140px',
      left: '145px',
      maxWidth: '425px'
    },
    heading: {
      fontSize: '24px',
      lineHeight: '28px'
    },
    navLink: {
      fontSize: '16px',
      lineHeight: '134.69%',
      letterSpacing: '0.0125em',
      fontWeight: 500
    },
    helperText: {
      fontSize: '12px',
      lineHeight: '14px'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  });

interface Props extends WithStyles<typeof styles> {}

interface FormData {
  mobileNumber: string;
  otp: string;
}

const ValidationSchema = yup.object().shape({

  mobileNumber: yup
    .string()
    .required('mobile number is a required field')
    .matches(PHONE_PATTERN, 'mobile number is invalid'),
  otp: yup.string().required().matches(OTP_PATTERN, 'otp is invalid')
});

const ForgotPassword: FunctionComponent<Props> = ({ classes }) => {
  const {
    errors,
    getValues,
    handleSubmit,
    register,
    setError,
    setValue
  } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const [orgCode,setOrgCode] = useState<string>('')
  eventTracker(GAEVENTCAT.forgotPassword, 'Forgot Password Page', 'Landed Forgot Password Page');

  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleVerifyOTP = async ({ mobileNumber, otp }: FormData) => {
    try {
      await verifyOTP(orgCode, mobileNumber, otp, '', true);


      setRedirectTo(
        `/forgot-password/reset?phone=${mobileNumber}&otp=${otp}`
      );
    } catch (err) {
      exceptionTracker(err.response?.data.message);
    }
  };

  const handleGenerateOTP = async () => {
    const mobileNumber = getValues('mobileNumber');

    if (!mobileNumber) {
      return setError(
        'mobileNumber',
        'required',
        'mobile number is a required field'
      );
    }

    try {
      const response = await generateOTP('+91', mobileNumber);
      eventTracker(GAEVENTCAT.forgotPassword, 'Forgot Password Page', 'Mobile OTP Sent');
      // TODO: Remove this in production.
      setValue('otp', response.data.otp, true);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
    }
  };

  return (
    <div className={classes.root}>
      <Container>
        <Grid container alignItems="center" className={classes.formContainer}>
          <Grid item sm={6} md={4} className={classes.boxLayout}>
            <Box
              bgcolor="white"
              borderRadius="10px"
              padding="20px 40px"
              textAlign="center"
            >
              <Box className={classes.heading} component="h2">
                Forgot Password
              </Box>

              <Typography className={classes.helperText}>
                Just enter the phone you've used to
                <br />
                login with us!
              </Typography>

              <form onSubmit={handleSubmit(handleVerifyOTP)}>


                <FormControl fullWidth margin="normal">
                  <Input
                    name="mobileNumber"
                    inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                    placeholder="Mobile Number"
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton disabled size="small">
                          <MobileFriendlyIcon />
                        </IconButton>
                      </InputAdornment>
                    }
                    className={errors.mobileNumber ? classes.error : ''}
                    inputRef={register}
                  />
                  {errors.mobileNumber && (
                    <FormHelperText error>
                      {errors.mobileNumber.message}
                    </FormHelperText>
                  )}
                </FormControl>

                <FormControl fullWidth margin="normal">
                  <Input
                    name="otp"
                    placeholder="OTP"
                    inputProps={{ maxLength: 6 }}
                    inputRef={register}
                    className={errors.otp ? classes.error : ''}
                  />
                  {errors.otp && (
                    <FormHelperText error>{errors.otp.message}</FormHelperText>
                  )}
                </FormControl>

                <Box textAlign="right" className={classes.navLink}>
                  <Link href="#" onClick={handleGenerateOTP}>
                    Click Here To Get OTP
                  </Link>
                </Box>

                <FormControl fullWidth margin="normal">
                  <Button
                    disableElevation
                    color="primary"
                    type="submit"
                    size="large"
                    variant="contained"
                  >
                    Submit
                  </Button>
                </FormControl>
              </form>

              <FormControl fullWidth margin="normal">
                <Box
                  display="flex"
                  justifyContent="flex-end"
                  className={classes.navLink}
                >
                  <Link color="primary" to="/login" component={RouterLink}>
                    Back To Login
                  </Link>
                </Box>
              </FormControl>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default withStyles(styles)(ForgotPassword);
