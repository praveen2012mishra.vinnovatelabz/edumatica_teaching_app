import React, { FunctionComponent, useEffect, useState } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { setAuthUser } from '../../auth/store/actions';
import { useSnackbar } from 'notistack';
import { RootState } from '../../../store';
import { Tutor, Organization } from '../../common/contracts/user';
import { isTutor, isOrganization } from '../../common/helpers';
import Dropzone from '../../common/components/dropzone/dropzone';
import UploadedContent from '../../common/components/dropzone/previewers/uploadedContent'
import Button from '../../common/components/form_elements/button';
import { Box, Divider, FormControl, Grid, Input } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import TutorLayout from '../components/tutor_layout';
import OrganizationLayout from '../components/organization_layout';
import { KycDocument } from '../../common/contracts/kyc_document';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import {
  uploadAadhaarZip,
} from '../../common/api/document';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import KycStepsModal from '../components/modals/kyc_Steps_modal';
import { getOrgVideoKycDownloadLink, getOrgVideoKycToken, getOrgVideoKycUploadLink, redoOrgVideoKyc, uploadOrgVideoKycUuid } from '../../common/api/organization';
import { getAdminTutorVideoKycDownloadLink, getAdminTutorVideoKycToken, getAdminTutorVideoKycUploadLink, redoAdminTutorVideoKyc, uploadAdminTutorVideoKycUuid } from '../../common/api/tutor';
import axios from 'axios';
import WebcamStreamCapture from '../components/video_kyc';
interface Props extends RouteComponentProps<{ username: string }> {
  authUser: Tutor | Organization;
}

const ProfileKyc: FunctionComponent<Props> = ({ authUser }) => {
  console.log(authUser)
  const { enqueueSnackbar } = useSnackbar();
  const [openHelp, setOpenHelp] = useState<boolean>(false)
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [aadhaarKycShareCode, setAadhaarKycShareCode] = useState('');
  const [aadhaarKycStatus, setAadhaarKycStatus] = useState(
    authUser.aadhaarKycStatus || ''
  );
  // eslint-disable-next-line
  const [document, setDocument] = useState<KycDocument[]>([]);
  // const [dropzoneKey, setDropzoneKey] = useState(0);
  const [redirectTo, setRedirectTo] = useState('');

  const [mobile, setMobile] = useState('');
  const [lastDigit, setLastDigit] = useState('');

  const [kycCode, setKycCode] = useState<string | null>(null)
  const [videoKycFile, setVideoKycFile] = useState<File[]>([]);
  const [videoKycStatus, setVideoKycStatus] = useState<boolean  | undefined>(authUser.isVideoKycDone);
  const [videoKycVerified, setVideoKycVerified] = useState<boolean  | undefined>(authUser.videoKycVerified);

  const dispatch = useDispatch();
  eventTracker(GAEVENTCAT.profile, 'KYC Info', 'Landed KYC Info');

  const handleOpen = () => {
    setOpenHelp(true);
  };

  const handleClose = () => {
    setOpenHelp(false);
  };

  const toBase64 = (file: File) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const createFormData = (user: Tutor | Organization, file: File) => {
    const formData = new FormData();
    const toExclude = [
      'kycDetails',
      'courseDetails',
      'package',
      'scheduleList',
      'batchList',
      'studentList',
      'tutorList'
    ];
    for (const [k, v] of Object.entries(user)) {
      if (!toExclude.includes(k)) formData.append(k, v);
    }
    formData.append('shareCode', aadhaarKycShareCode);
    formData.append('mobile', mobile);
    formData.append('lastDigit', lastDigit);
    formData.append('files', file);
    return formData;
  };

  const processZip = async () => {
    const file = droppedFiles[0];
    const user = Object.assign({}, authUser, {
      aadhaarKycZip: await toBase64(droppedFiles[0]),
      aadhaarKycShareCode: aadhaarKycShareCode
    });

    const profileUpdated = (profile: Tutor | Organization) =>
      dispatch(setAuthUser(profile));

    const formData = createFormData(user, file);

    try {
      await uploadAadhaarZip(formData).then(async (value) => {
        user.aadhaarKycStatus = value.message;
        profileUpdated(user);
        setAadhaarKycStatus(value.message);
        eventTracker(GAEVENTCAT.profile, 'KYC Info', 'Updated KYC Info');
      });
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) setRedirectTo('/login');
      setAadhaarKycStatus('Rejected');
    }
  };

  const handleRedoCall = () =>{
    setAadhaarKycStatus('');
    const user = Object.assign({},authUser,{aadhaarKycStatus :''})
    const profileUpdated = (profile: Tutor | Organization) =>
      dispatch(setAuthUser(profile));
    profileUpdated(user)

  }

  // const storeZip = async () => {
  //   const file = droppedFiles[0];

  //   setDocument([
  //     {
  //       kycDocFormat: file.type,
  //       kycDocType: DocumentType.AADHAR_ZIP,
  //       kycDocLocation: file.name
  //     }
  //   ]);
  //   setDroppedFiles([]);
  //   setDropzoneKey(dropzoneKey + 1);

  //   const formData = new FormData();
  //   formData.append('document', file);

  //   try {
  //     const awsBucket = await fetchUploadUrlForKycDocument({
  //       fileName: file.name,
  //       contentType: file.type,
  //       contentLength: file.size
  //     });

  //     await uploadKycDocument(awsBucket.url, formData);
  //   } catch (error) {
  //     exceptionTracker(error.response?.data.message);
  //     if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //       setRedirectTo('/login');
  //     }
  //     console.log(error);
  //   }
  // };

  const handleFormSubmit = async () => {
    if (droppedFiles.length < 1) {
      enqueueSnackbar('No zip file selected', {
        variant: 'error'
      });
      return;
    }
    // Store zip input in s3 bucket
    //storeZip();

    // Process Zip in backend and then save the user
    processZip();
  };

  const fetchKycCode = () => {
    // setKycCode('yuR8LzK')
    if (isTutor(authUser)) {
      getAdminTutorVideoKycToken().then(token => {
        setKycCode(token)
      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }
  
    if (isOrganization(authUser)) {
      getOrgVideoKycToken().then(token => {
        setKycCode(token)
      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }
  }

  const fetchVideoKycStatus = async(fileName:string ,awsUuid:string) => {
    const user = Object.assign({}, authUser, {
      videoKycUuid: awsUuid,
      isVideoKycDone: true,
    });

    const profileUpdated = (profile: Tutor | Organization) =>
      dispatch(setAuthUser(profile));
    
    try {
      if (isTutor(authUser)) {
        await uploadAdminTutorVideoKycUuid(fileName, awsUuid).then(async (value) => {
        profileUpdated(user);
        setVideoKycStatus(true);
      });
      }
      if (isOrganization(authUser)) {
        await uploadOrgVideoKycUuid(fileName, awsUuid).then(async (value) => {
        console.log(value)
        profileUpdated(user);
        setVideoKycStatus(true);
      });
      }
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) setRedirectTo('/login');
      setAadhaarKycStatus('Rejected');
    }
  }

  const handleVideoKycSubmit = () => {
    console.log(videoKycFile)
    const video = videoKycFile[0]
    // mime type
    const fileName = video.name
    const fileType = video.type
    if (videoKycFile.length < 1) {
      enqueueSnackbar('No video selected', {
        variant: 'error'
      });
      return;
    }

    if (isTutor(authUser)) {
      getAdminTutorVideoKycUploadLink(fileType).then(response => {
        const fetchedUploadLink = response.uploadLink;
        const awsUuid = response.uuid;
        // const formData = new FormData();
        // formData.append('file', video);

        axios.put(fetchedUploadLink, video, {headers: {"Content-Type": fileType}}).then(uploadResponse => {
          if(uploadResponse.status === 200) {
            fetchVideoKycStatus(fileName, awsUuid)
          }
        }).catch(err => {
          console.log(err)
        })
      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }

    if (isOrganization(authUser)) {
      getOrgVideoKycUploadLink(fileType).then(response => {
        console.log(response)
        const fetchedUploadLink = response.uploadLink;
        const awsUuid = response.uuid;
        const formData = new FormData();
        formData.append('file', video);

        axios.put(fetchedUploadLink, video, {headers: {"Content-Type": fileType}}).then(uploadResponse => {
          console.log(uploadResponse)
          if(uploadResponse.status === 200) {
            fetchVideoKycStatus(fileName, awsUuid)
          }
        }).catch(err => {
          console.log(err)
        })

      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }
  }

  const viewVideoKycHandler = () => {
    if(isTutor(authUser)) {
      if(!authUser.videoKycUuid) return
      getAdminTutorVideoKycDownloadLink(authUser.videoKycUuid).then(link => {
        window.open(link, "blank")
      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }
    if(isOrganization(authUser)) {
      if(!authUser.videoKycUuid) return
      getOrgVideoKycDownloadLink(authUser.videoKycUuid).then(link => {
        window.open(link, "blank")
      }).catch(error => {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
          setAadhaarKycStatus('Rejected');
        }
      })
    }
    
  }

  const redoVideoKycHandler = () => {
    const user = Object.assign({}, authUser, {
      videoKycUuid: '',
      isVideoKycDone: false,
      videoKycVerified: false
    });

    const profileUpdated = (profile: Tutor | Organization) =>
      dispatch(setAuthUser(profile));

    if(isTutor(authUser)) {
      redoAdminTutorVideoKyc().then(response => {
        if(response.code === 0) {
          profileUpdated(user)
          setVideoKycStatus(false)
          setVideoKycVerified(false)
        }
      })
    }
    if(isOrganization(authUser)) {
      redoOrgVideoKyc().then(response => {
        if(response.code === 0) {
          profileUpdated(user)
          setVideoKycStatus(false)
          setVideoKycVerified(false)
        }
      })
    }
  }


  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const alertKycStatus = () => {
    return (
      <div>
        <Alert severity={aadhaarKycStatus === 'Verified' ? 'success' : 'error'}>
          <AlertTitle>{aadhaarKycStatus}</AlertTitle>
          Your KYC status is {aadhaarKycStatus} <br /> <br />
          {aadhaarKycStatus === 'Verified' ? '' : 'Enter correct details !!'}
        </Alert>

        {/* {aadhaarKycStatus === 'Rejected' && */}
        <Box display="flex" justifyContent="center" marginY="20px">
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={handleRedoCall}
          >
            Redo KYC process
          </Button>
        </Box>
        {/*}*/}
      </div>
    );
  };

  const toRenderKyc = () => {
    return (
      <div>
        <KycStepsModal openModal={openHelp} onClose={handleClose}/>
        {aadhaarKycStatus === '' && (
          <div>
            <Box marginBottom="30px" width="100%" display="flex" alignItems="center">
              To know more about the KYC process click 
              <Button style={{padding:"0 0"}} color="primary" variant="text" disableElevation onClick={handleOpen}>
                here
              </Button>
            </Box>
            <Grid container>
              <Grid item xs={12} md={3}>
                <FormControl fullWidth margin="normal">
                  <Box fontWeight="bold" marginTop="5px">
                    Upload Aadhaar KYC Zip File
                  </Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={9}>
                <FormControl fullWidth margin="normal">
                  <Dropzone
                    onChange={(files) => setDroppedFiles(files)}
                    files={droppedFiles}
                    acceptedFiles={[
                      'application/zip,application/zip-compressed,application/x-zip-compressed'
                    ]}
                    maxFileSize={102400} // 100 KB
                    contenttype='other'
                  />

                  <UploadedContent
                    files={droppedFiles}
                    onRemoveItem={() => setDroppedFiles([])}
                    contenttype='other'
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Grid container>
              <Grid item xs={12} md={3}>
                <FormControl fullWidth margin="normal">
                  <Box fontWeight="bold" marginTop="5px">
                    Share Code
                  </Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={9}>
                <FormControl fullWidth margin="normal">
                  <Input
                    placeholder="Enter share code"
                    required
                    value={aadhaarKycShareCode}
                    onChange={(e) => setAadhaarKycShareCode(e.target.value)}
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Grid container>
              <Grid item xs={12} md={3}>
                <FormControl fullWidth margin="normal">
                  <Box fontWeight="bold" marginTop="5px">
                    Mobile number
                  </Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={9}>
                <FormControl fullWidth margin="normal">
                  <Input
                    placeholder="Enter mobile mumber(as given in aadhaar card)"
                    required
                    value={mobile}
                    inputProps={{ maxLength: 10 }}
                    onChange={(e) => setMobile(e.target.value)}
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Grid container>
              <Grid item xs={12} md={3}>
                <FormControl fullWidth margin="normal">
                  <Box fontWeight="bold" marginTop="5px">
                    Last digit of Aadhaar number
                  </Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={9}>
                <FormControl fullWidth margin="normal">
                  <Input
                    placeholder="Enter last digit of aadhaar number"
                    required
                    value={lastDigit}
                    inputProps={{ maxLength: 1 }}
                    onChange={(e) => setLastDigit(e.target.value)}
                  />
                </FormControl>
              </Grid>
            </Grid>

            <Box display="flex" justifyContent="flex-end" marginY="20px">
              <Button
                disableElevation
                variant="contained"
                color="primary"
                size="large"
                onClick={handleFormSubmit}
              >
                Save Changes
              </Button>
            </Box>
          </div>
        )}
        {aadhaarKycStatus !== '' && alertKycStatus()}

        {
          videoKycStatus === true ? '' :
        
        <Box fontWeight="bold" marginTop="25px">
              Start video KYC process
              <Box width="100%" display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                {
                  kycCode ? '' : 
                  <>
                  <Button onClick={fetchKycCode} style={{width:"100%", height:"60px", marginTop:"18px", borderRadius:"30px"}} color="primary" variant="outlined" size="small">
                    Get Code
                  </Button>
                  </>
                }
                {
                  !kycCode ? '' : 
                  <>
                  <Box width="100%" display="flex" justifyContent="space-evenly" alignItems="center">
                    <h1 style={{fontSize:"70px"}}>{kycCode}</h1>
                    <CountdownCircleTimer
                      strokeWidth={4}
                      size={100}
                      isPlaying
                      duration={10}
                      colors={[
                        ['#91c788', 0.25],
                        ['#eeebdd', 0.25],
                        ['#ce1212', 0.25],
                        ['#810000', 0.25],
                      ]}
                      onComplete={() => {
                        setKycCode(null)
                      }}
                    >
                      {({ remainingTime }) => remainingTime}
                    </CountdownCircleTimer>
                  </Box>
                  
                  <Box width="100%">

                  <Grid container>
                    <Grid item xs={12} md={3}>
                      <FormControl fullWidth margin="normal">
                        <Box fontWeight="bold" marginTop="5px">
                          Upload your Video
                        </Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={9}>
                      <FormControl fullWidth margin="normal">
                        {/* <Dropzone
                          onChange={(files) => setVideoKycFile(files)}
                          files={droppedFiles}
                          acceptedFiles={[
                            'video/mp4,application/x-mpegURL,video/MP2T,video/3gpp'
                          ]}
                          maxFileSize={20971520} // 20 MB
                          contenttype='other'
                        /> */}

                        {/* <UploadedContent
                          files={videoKycFile}
                          onRemoveItem={() => setVideoKycFile([])}
                          contenttype='other'
                        /> */}
                        
                        <WebcamStreamCapture />
                      </FormControl>
                    </Grid>
                  </Grid>
                  </Box>
                  </>
                }
              </Box>

              { !kycCode ? '' :
                <Box display="flex" justifyContent="flex-end" marginY="20px">
                  <Button
                    disableElevation
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={handleVideoKycSubmit}
                  >
                    Save Video KYC
                  </Button>
                </Box>
                }
            </Box>
            }

            {
              videoKycVerified === false ? 
              <Box>
                {
                  videoKycStatus === false ? '' :
                  <Box marginBottom="30px" width="100%" display="flex" alignItems="center">
                    You have completed your Video KYC. Verified Mark will be awarded shortly.
                    <Button onClick={viewVideoKycHandler} style={{padding:"0 0", marginLeft:'10px'}} color="primary" variant="text" disableElevation>
                        View video KYC
                    </Button>
                  </Box>
                }
              </Box>
              
              :

              <Box marginBottom="30px" width="100%" display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Box fontWeight="bold" display="flex" alignItems="center" padding="10px" borderRadius="30px" boxShadow="2px 2px 10px gray">
                  <VerifiedUserIcon style={{color:"#4C8BF5", marginRight:"5px"}} /> Video KYC Verified
                </Box>
                <Box marginTop="20px">
                    <Button onClick={redoVideoKycHandler} style={{padding:"0 0"}} color="primary" variant="text" disableElevation>
                        Redo
                    </Button>
                    <Button onClick={viewVideoKycHandler} style={{padding:"0 0", marginLeft:'10px'}} color="primary" variant="text" disableElevation>
                        View
                    </Button>
                </Box>
              </Box>

            }

      </div>
    );
  };

  if (isTutor(authUser)) {
    return <TutorLayout profile={authUser}>{toRenderKyc()}</TutorLayout>;
  }

  if (isOrganization(authUser)) {
    return (
      <OrganizationLayout profile={authUser}>
        {toRenderKyc()}
      </OrganizationLayout>
    );
  }

  return <div></div>;
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as Tutor | Organization
});
export default connect(mapStateToProps)(withRouter(ProfileKyc));
