import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Redirect,
  RouteComponentProps,
  withRouter
} from 'react-router-dom';
import {
  createStyles,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Box, Container } from '@material-ui/core';
import { RootState } from '../../../store';
import { User } from '../../common/contracts/user';
import MiniDrawer from '../../common/components/sidedrawer';
import { eventTracker, GAEVENTCAT } from '../../common/helpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textCenter:{
      textAlign: 'center'
    }
  })
);

interface Props
  extends RouteComponentProps<{ username: string; step: string }> {
  authUser: User;
}

const ProfileDeactivated: FunctionComponent<Props> = ({
  authUser
}) => {
  const classes = useStyles();
  eventTracker(GAEVENTCAT.auth, 'Login Failed', 'Account Deactivated');

  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    if (!authUser.mobileNo) {
      setRedirectTo(`/profile/personal-information`);
    }
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="md">
        <Box
          component="h1"
          fontWeight="1000"
          fontSize="50px"
          line-height="21px"
          color="#666666"
          margin="250px 0"
          className={classes.textCenter}
        >
          Account Deactivated. Please Contact Your Institute/Tutor
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(withRouter(ProfileDeactivated));
