import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
  withRouter
} from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { Box, Container, Grid } from '@material-ui/core';
import {
  createStyles,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import {
  InfoOutlined as InfoIcon,
} from '@material-ui/icons';
import { RootState } from '../../../store';
import { setAuthUser } from '../../auth/store/actions';
import { Student, User } from '../../common/contracts/user';
import { onboardStudent } from '../../common/api/profile';
import { getStudent } from '../../common/api/tutor';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker } from '../../common/helpers';
import StudentPersonalInformation from '../components/process_forms/student_personal_information';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formHeader: {
      backgroundColor: theme.palette.primary.main,
      minHeight: '150px',
      padding: '20px',
      width: '100%'
    },
    headerIcon: {
      alignItems: 'center',
      border: '1px solid #fff',
      borderRadius: '1000px',
      display: 'flex',
      height: '48px',
      justifyContent: 'center',
      margin: '0 auto 10px',
      padding: '10px',
      width: '48px',

      '& svg': {
        fill: '#fff'
      }
    },
    headerIconActive: {
      backgroundColor: '#fff',
      '& svg': {
        fill: theme.palette.primary.main
      }
    },
    headerIconTitle: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      },
      fontWeight: 500,
      fontSize: '15px',
      lineHeight: '18px',
      letterSpacing: '1px',
      textTransform: 'uppercase',
      color: '#FFFFFF'
    }
  })
);

interface Props
  extends RouteComponentProps<{ username: string; step: string }> {
  authUser: User;
}

const MAX_STEPS = 1;

const ProfileProcessStudent: FunctionComponent<Props> = ({
  match,
  history,
  authUser
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const [profile, setProfile] = useState(authUser);
  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    if (!authUser.mobileNo) {
      setRedirectTo(`/profile/personal-information`);
    }
  }, [authUser.mobileNo]);

  const step = parseInt(match.params.step);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveUser = async (data: Student) => {
    data = {...data, ...{roleStatus: 'ACTIVE'}}
    setProfile(data);
    if (step < MAX_STEPS) {
      history.push(`/profile/student/process/${step + 1}`);
      return;
    }
    try {
      dispatch(setAuthUser(data));
      await onboardStudent(data);
      const student = await getStudent();
      const updatedUser = {...student}
      dispatch(setAuthUser(updatedUser));
      history.push(`/profile/dashboard`);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const StudentEnrollmentForm = () => (
    <Switch>
      <Route
        path="/profile/student/process/1"
        render={() => (
          <StudentPersonalInformation
            user={profile as Student}
            saveUser={saveUser}
            submitButtonText="Submit"
          />
        )}
      />
    </Switch>
  );

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="md">
        <Box
          marginY="30px"
          bgcolor="white"
          boxShadow="0px 28px 50px rgba(19, 8, 64, 0.0981753)"
        >
          <Grid
            container
            alignItems="center"
            justify="center"
            className={classes.formHeader}
          >
            <Grid item xs={4}>
              <Box textAlign="center" fontWeight="bold" position="relative">
                <span
                  className={`${classes.headerIcon} ${
                    step === 1 ? classes.headerIconActive : ''
                  }`}
                >
                  <InfoIcon />
                </span>

                <span className={classes.headerIconTitle}>
                  PERSONAL DETAILS
                </span>
              </Box>
            </Grid>
          </Grid>

          <Box display="flex" justifyContent="center" padding="20px">
            <Box maxWidth="600px" width="100%">
              <StudentEnrollmentForm />
            </Box>
          </Box>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(withRouter(ProfileProcessStudent));
