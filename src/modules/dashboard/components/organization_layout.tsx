import React, { FunctionComponent } from 'react';
import {
  Link as RouterLink,
  RouteComponentProps,
  withRouter
} from 'react-router-dom';
import {
  Box,
  Container,
  Grid,
  List,
  ListItem,
  ListItemText,
  Theme,
  Typography
} from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Organization } from '../../common/contracts/user';
import MiniDrawer from '../../common/components/sidedrawer';
import ProfileImage from '../containers/profile_image';
import { setAuthUser } from '../../auth/store/actions';
import { useDispatch } from 'react-redux';
import { fontOptions } from '../../../theme';

const styles = (theme: Theme) =>
  createStyles({
    bodyContainer: {
      [theme.breakpoints.up('md')]: {
        padding: '0 40px'
      }
    },

    welcomeHeading: {
      [theme.breakpoints.up('md')]: {
        padding: '0 40px'
      },
      fontWeight: 'normal',
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#333333'
    },
    navList: {
      background: '#fff',
      marginBottom: '20px'
    },
    navItem: {
      padding: '8px 0px 8px 20px'
    }
  });

interface Props extends WithStyles<typeof styles>, RouteComponentProps {
  profile: Organization;
}

const OrganizationLayout: FunctionComponent<Props> = ({
  children,
  classes,
  match,
  profile
}) => {
  const dispatch = useDispatch();

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box paddingY="20px">

          <Grid container>
            <Grid item xs={12} md={3}>
              <Box className={classes.navList}>
                <List dense>
                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/personal-information`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/personal-information'
                            ? 'primary'
                            : 'inherit'
                        }
                      >
                        Personal Information
                      </Typography>
                    </ListItemText>
                  </ListItem>

                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/subjects`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/subjects'
                            ? 'primary'
                            : 'inherit'
                        }
                      >
                        Course Details
                      </Typography>
                    </ListItemText>
                  </ListItem>

                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/others`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/others'
                            ? 'primary'
                            : 'inherit'
                        }
                      >
                        Business Details
                      </Typography>
                    </ListItemText>
                  </ListItem>

                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/security`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/security'
                            ? 'primary'
                            : 'inherit'
                        }
                      >
                        Security
                      </Typography>
                    </ListItemText>
                  </ListItem>

                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/kyc`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/kyc' ? 'primary' : 'inherit'
                        }
                      >
                        KYC
                      </Typography>
                    </ListItemText>
                  </ListItem>
                </List>
              </Box>
            </Grid>

            <Grid item xs={12} md={9}>
              <Box className={classes.bodyContainer}>
                {match.path.search('personal-information') !== -1 && (<Grid container style={{ backgroundColor: '#fff', paddingTop: '3em', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <ProfileImage
                    profileUpdated={(profile) => dispatch(setAuthUser(profile))}
                    profile={profile}
                    name={profile.organizationName}
                  />
                </Grid>)}
                {children}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

export default withStyles(styles)(withRouter(OrganizationLayout));
