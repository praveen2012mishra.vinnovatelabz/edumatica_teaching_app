import React, { Fragment } from "react";
import Webcam from "react-webcam";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import VideocamIcon from "@material-ui/icons/Videocam";
import StopRoundedIcon from "@material-ui/icons/StopRounded";
import PlayArrowRoundedIcon from "@material-ui/icons/PlayArrowRounded";
import ReactPlayer from "react-player";
import PublishRoundedIcon from '@material-ui/icons/PublishRounded';
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import { Grid } from "@material-ui/core";

const WebcamStreamCapture = () => {
  const webcamRef = React.useRef<Webcam|null>(null);
  const mediaRecorderRef = React.useRef<MediaRecorder|null>(null);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [recordedUrl, setRecordedUrl] = React.useState("");
  const [previewMode, setPreviewMode] = React.useState(false);
  const [recordedBlob, setRecordedBlob] = React.useState<Blob|null>(null)
  const playerRef= React.useRef<ReactPlayer|null>(null)
  React.useEffect(() => {
    console.log("Checeked");
    if (recordedChunks.length) {
      console.log("Checekeds");
      const blob = new Blob(recordedChunks, {
        type: "video/webm"
      });
      console.log(URL.createObjectURL(blob));
      setRecordedBlob(blob)
      setRecordedUrl(URL.createObjectURL(blob));
    } else {
      setRecordedBlob(null)
      setRecordedUrl("");
    }
  }, [recordedChunks]);
  const handleStartCaptureClick = React.useCallback(() => {
    setCapturing(true);
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current?.stream as MediaStream, {
      mimeType: "video/webm"
    });
    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  const handleUpload = () =>{
      if(recordedBlob!==null){
        console.log(playerRef?.current?.getDuration())
        // @ts-ignore
        recordedBlob.name = 'KYC Video'
        // @ts-ignore
        recordedBlob.timeModified = new Date()
        console.log(recordedBlob)
      }
  }

  const handleStopCaptureClick = React.useCallback(() => {
    mediaRecorderRef?.current?.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  const handlePlay = () => {
    if (recordedUrl.length > 0) {
      setPreviewMode(true);
    } else {
      window.alert("No Video Available");
    }
  };

  const handleCancel = () =>{
    setPreviewMode(false)
    setRecordedBlob(null)
    setRecordedUrl('')
    
  }

  return (
    <>
      {previewMode ? (
        <Fragment>
          <ReactPlayer
          url={recordedUrl}
          controls
          playing
          width="640px"
          height="480px"
          ref={el=>playerRef.current=el}
          
        />
        <Grid container justify='center'>
            <Grid item sm={5} xs={6} md={4} lg={2} xl={2}>
            <IconButton onClick={handleCancel} ><ClearRoundedIcon /></IconButton>
            </Grid>
            <Grid item sm={5} xs={6} md={4} lg={2} xl={2}>
            <IconButton onClick={handleUpload} ><PublishRoundedIcon /></IconButton>
            </Grid>
        </Grid>
        
        
          </Fragment>
        

      ) : (
        <Fragment>
          <Webcam audio={true}  ref={el=>webcamRef.current=el} />
          {capturing ? (
            <Tooltip title="Start Capture">
              <IconButton color="secondary" onClick={handleStopCaptureClick}>
                <StopRoundedIcon style={{width:'32px'}} />
              </IconButton>
            </Tooltip>
          ) : (
            // <button onClick={handleStopCaptureClick}>Stop Capture</button>
            <Tooltip title="Stop Capture">
              <IconButton color="primary" onClick={handleStartCaptureClick}>
                <VideocamIcon style={{width:"32px"}} />
              </IconButton>
            </Tooltip>
          )}
        </Fragment>
      )}

      {(recordedChunks.length > 0 && !(previewMode))&& (
        <Tooltip title="Play Preview">
          <IconButton onClick={handlePlay}>
            <PlayArrowRoundedIcon />
          </IconButton>
        </Tooltip>
      )}
    </>
  );
};

export default WebcamStreamCapture;
