import React, { FunctionComponent } from 'react';
import { Box, Container, Theme } from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Admin } from '../../common/contracts/user';
import MiniDrawer from '../../common/components/sidedrawer';

const styles = (theme: Theme) => createStyles({});

interface Props extends WithStyles<typeof styles> {
  profile: Admin;
}

const AdminLayout: FunctionComponent<Props> = ({
  children,
  classes,
  profile
}) => (
  <div>
    <MiniDrawer>

    <Container maxWidth="md">
      <Box paddingY="20px">
        <Box component="h2">Welcome Admin, {profile.adminName}!</Box>

        <Box>{children}</Box>
      </Box>
    </Container>
    </MiniDrawer>
  </div>
);

export default withStyles(styles)(AdminLayout);
