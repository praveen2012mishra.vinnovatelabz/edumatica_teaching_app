import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  MenuItem,
  Select,
  FormHelperText,
  Typography
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { PAN_PATTERN, GSTIN_PATTERN, AADHAAR_PATTERN } from '../../../common/validations/patterns';
import {
  KeyboardArrowLeft as KeyboardArrowLeftIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
} from '@material-ui/icons';
import { Organization } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { Redirect } from 'react-router-dom';
import { BusinessType } from '../../../common/enums/business_types';
import { useForm } from 'react-hook-form';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';
import { eventTracker, GAEVENTCAT } from '../../../common/helpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 25px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          marginLeft: '20px'
        }
      }
    },
    skipBtn: {
      '& button': {
        padding: '10px 65px 12px 65px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',
      },
    },
    previousBtn: {
      '& button': {
        padding: '10px 40px 10px 25px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',

        '& svg': {
          marginRight: '20px'
        }
      }
    },
    helperText: {
      fontWeight: 500,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: 500,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    removeIcon: {
      color: '#F9BD33'
    },
    docList: {
      borderBottom: '1px dashed #EAE9E4',
      borderTop: '1px dashed #EAE9E4',
      margin: '25px 0'
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  pageError: string;
  businessType: string;
  businessPAN: string;
  businessName: string;
  ownerPAN: string;
  ownerName: string;
  gstin: string;
  documentNumber: string;
}

interface Props {
  user: Organization;
  submitButtonText: string;
  saveUser: (data: Organization) => any;
}

interface DateError {
  dob: string | boolean;
}

enum Action {
  SKIP,
  SUBMIT
}

const OrganizationOtherInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  // const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  // const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  // const [dropzoneKey, setDropzoneKey] = useState(0);
  const [businessType, setBusinessType] = useState(user.businessType || '');
  const [dob, setDob] = useState<Date | null>(user.dob ? new Date(user.dob) : null);
  const [dateError, setDateError] = useState<DateError>({dob: false})
  const [businessPAN, setBusinessPAN] = useState(user.businessPAN || '');
  const [businessName, setBusinessName] = useState(user.businessName || '');
  const [ownerPAN, setOwnerPAN] = useState(user.ownerPAN || '');
  const [ownerName, setOwnerName] = useState(user.ownerName || '');
  const [gstin, setGstin] = useState(user.gstin || '')
  const [aadhaar, setAadhaar] = useState(user.aadhaar || '');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Organization Login Form', 'Landed Business Info');

  // const [documents, setDocuments] = useState<KycDocument[]>(
  //   user.kycDetails || []
  // );
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDobChange = (date: Date | null) => {
    if(date) {
      setDateError({dob: false})
    }
    setDob(date);
  };

  // const addDocument = async () => {
  //   if (droppedFiles.length < 1) return;

  //   const file = droppedFiles[0];

  //   const clonedDocuments = [...documents];
  //   const documentIndex = clonedDocuments.findIndex(
  //     (document) =>
  //       document.kycDocType.toLowerCase() === documentType.toLowerCase()
  //   );

  //   if (documentIndex > -1) {
  //     clonedDocuments.splice(documentIndex, 1);
  //   }

  //   setDocuments([
  //     ...clonedDocuments,
  //     {
  //       kycDocFormat: file.type,
  //       kycDocType: documentType,
  //       kycDocLocation: file.name,
  //     },
  //   ]);
  //   setDroppedFiles([]);
  //   setDropzoneKey(dropzoneKey + 1);

  //   const formData = new FormData();

  //   formData.append('document', file);

  //   try {
  //     const awsBucket = await fetchUploadUrlForKycDocument({
  //       fileName: file.name,
  //       contentType: file.type,
  //       contentLength: file.size,
  //     });

  //     await uploadKycDocument(awsBucket.url, formData);
  //   }
  //   catch(error){
  //     if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //       setRedirectTo('/login');
  //     }
  //   }
  // };

  // const removeDocument = (index: number) => {
  //   const clonedDocuments = [...documents];

  //   clonedDocuments.splice(index, 1);

  //   setDocuments(clonedDocuments);
  // };

  const handleFormSubmit = async (action: Action) => {
    if (action === Action.SKIP) {
      // if(!PAN_PATTERN.test(ownerPAN)){
      //   setError(
      //     'emailId',
      //     'Invalid Data',
      //     'Invalid Email'
      //   );
      //   return;
      // }
      // else{
      //   clearError('emailId');
      // }
      saveUser(user);
      return;
    }

    //if(documents.length < 1) return;
    if (businessType.length) {
      if (businessType.length < 3) {
        setError(
          'businessType',
          'Invalid Data',
          'Business type should be minimum 3 characters long'
        );
        return;
      } else {
        clearError('businessType');
      }
    } else {
      clearError('businessType');
    }

    // if (!dob) {
    //   setDateError({dob: 'Date of Incorporation Required'})
    //   return;
    // }

    if (businessPAN.length) {
      if (!PAN_PATTERN.test(businessPAN)) {
        setError('businessPAN', 'Invalid Data', 'Invalid PAN data');
        return;
      } else {
        clearError('businessPAN');
      }
    } else {
      clearError('businessPAN');
    }
    

    if (businessName.length) {
      const remSpaceB = businessName.replace(/ /g, "")
      if (remSpaceB.length < 5) {
        setError(
          'businessName',
          'Invalid Data',
          'Business name should be minimum 5 characters long excluding spaces'
        );
        return;
      } else {
        clearError('businessName');
      }
    } else {
      clearError('businessName');
    }
    

    if (!ownerPAN.length) {
      setError('ownerPAN', 'Invalid Data', 'Director PAN cannot be empty');
      return;
    } else {
      clearError('ownerPAN');
    }
    if (!PAN_PATTERN.test(ownerPAN)) {
      setError('ownerPAN', 'Invalid Data', 'Invalid Director PAN');
      return;
    } else {
      clearError('ownerPAN');
    }

    if (!aadhaar.length) {
      setError('documentNumber', 'Invalid Data', 'Director AADHAAR cannot be empty');
      return;
    } else {
      clearError('documentNumber');
    }

    if (!AADHAAR_PATTERN.test(aadhaar)) {
      setError(
        'documentNumber',
        'Invalid Director Aadhaar',
        'Invalid Director Aadhaar'
      );
      return;
    } else {
      clearError('documentNumber');
    }

    if (businessName.length && ownerPAN.length && ownerPAN.toUpperCase() === businessPAN.toUpperCase()) {
      setError(
        'ownerPAN',
        'Invalid Data',
        'Business PAN and Director PAN could not be same'
      );
      return;
    } else {
      clearError('ownerPAN');
    }

    if (gstin.length) {
      if (!GSTIN_PATTERN.test(gstin)) {
        setError('gstin', 'Invalid Data', 'Invalid GSTIN');
        return;
      } else {
        clearError('gstin');
      }
    } else {
      clearError('gstin');
    }
    

    if (ownerName.length) {
      const remSpaceO = ownerName.replace(/ /g, "")
      if (remSpaceO.length < 5) {
        setError(
          'ownerName',
          'Invalid Data',
          'Director name should be minimum 5 characters long excluding spaces'
        );
        return;
      } else {
        clearError('ownerName');
      }
    } else {
      clearError('ownerName');
    }
    

    saveUser({
      ...user,
      businessType: businessType,
      dob: dob ? dob.toLocaleDateString("fr-CA") : undefined,
      businessPAN: businessPAN,
      businessName: businessName,
      ownerPAN: ownerPAN,
      ownerName: ownerName,
      gstin: gstin,
      aadhaar: aadhaar ? aadhaar : undefined,
      //kycDetails: documents
    });
  };

  return (
    <div>
      <Grid container>
        {/* <Grid item xs={12} md={4} /> */}
        <Grid item xs={12} md={8}>
          <Box component="h2" className={classes.helperText}>
            Please Enter the Business Details
          </Box>
        </Grid>
      </Grid>

      <Typography color="textSecondary"><span style={{color: 'crimson'}}>*</span> Fields are Mandatory</Typography>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Business Type</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Select
              value={businessType}
              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                setBusinessType(e.target.value as BusinessType)
              }
              className={errors.businessType ? classes.error : ''}
            >
              <MenuItem value={BusinessType.PVT_LTD}>Private Limited</MenuItem>
              <MenuItem value={BusinessType.PROPRIETORSHIP}>
                Proprietorship
              </MenuItem>
              <MenuItem value={BusinessType.PARTNERSHIP}>Partnership</MenuItem>
              <MenuItem value={BusinessType.PUBLIC_LTD}>
                Public Limited
              </MenuItem>
              <MenuItem value={BusinessType.LLP}>LLP</MenuItem>
              <MenuItem value={BusinessType.SOCIETY}>Society</MenuItem>
              <MenuItem value={BusinessType.TRUST}>Trust</MenuItem>
              <MenuItem value={BusinessType.NGO}>NGO</MenuItem>
              <MenuItem value={BusinessType.NOT_REG}>Not Registered</MenuItem>
            </Select>
          </FormControl>
          {errors.businessType && (
            <FormHelperText error>{errors.businessType.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Date Of Incorporation</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Datepickers selectedDate={dob} handleDateChange={handleDobChange} 
              maxDate={new Date()}
            />
          </FormControl>
          {dateError.dob &&
            <Typography className={classes.dateError}>{dateError.dob}</Typography>
          }
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Business PAN</Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="PAN of the company"
              value={businessPAN}
              inputProps={{ maxLength: 10 }}
              onChange={(e) => setBusinessPAN(e.target.value)}
              className={errors.businessPAN ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.businessPAN && (
            <FormHelperText error>{errors.businessPAN.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Business Name</Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Business Name as per PAN"
              value={businessName}
              inputProps={{ maxLength: 50 }}
              onChange={(e) => setBusinessName(e.target.value)}
              className={errors.businessName ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.businessName && (
            <FormHelperText error>{errors.businessName.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Director PAN<span style={{color: 'crimson'}}>*</span></Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="PAN of one of the directors"
              value={ownerPAN}
              inputProps={{ maxLength: 10 }}
              onChange={(e) => setOwnerPAN(e.target.value)}
              className={errors.ownerPAN ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.ownerPAN && (
            <FormHelperText error>{errors.ownerPAN.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Director Name</Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Name as per PAN"
              value={ownerName}
              inputProps={{ maxLength: 50 }}
              onChange={(e) => setOwnerName(e.target.value)}
              className={errors.ownerName ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.ownerName && (
            <FormHelperText error>{errors.ownerName.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Director Aadhaar<span style={{color: 'crimson'}}>*</span></Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Aadhaar of one of the Director"
              value={aadhaar}
              inputProps={{ maxLength: 12 }}
              onChange={(e) => setAadhaar(e.target.value)}
              className={errors.documentNumber ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.documentNumber && (
            <FormHelperText error>{errors.documentNumber.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>GSTIN</Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="GSTIN"
              value={gstin}
              inputProps={{ maxLength: 15 }}
              onChange={(e) => setGstin(e.target.value)}
              className={errors.gstin ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.gstin && (
            <FormHelperText error>{errors.gstin.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      {/* <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Document Type
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Select
              value={documentType}
              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                setDocumentType(e.target.value as DocumentType)
              }
            >
              <MenuItem value={DocumentType.PAN}>PAN Card</MenuItem>
              <MenuItem value={DocumentType.AADHAR}>Aadhar Card</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Upload
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Dropzone
              key={dropzoneKey}
              acceptedFiles={['image/jpeg', 'image/png']}
              maxFileSize={104857600} // 100 MB
              files={droppedFiles}
              onChange={(files) => setDroppedFiles(files)}
              onRemoveItem={(file, index) => setDroppedFiles([])}
            />
          </FormControl>
        </Grid>
      </Grid>

      <FormControl fullWidth margin="normal">
        <Box display="flex" justifyContent="flex-end">
          <Button
            disableElevation
            color="primary"
            size="small"
            variant="contained"
            onClick={addDocument}
          >
            <AddIcon /> Add
          </Button>
        </Box>
      </FormControl>

      <Grid container>
        <Grid item xs={12} md={4}></Grid>

        <Grid item xs={12} md={8}>
          <Box>
            {documents.map((document, index) => (
              <Box display="flex" justifyContent="space-between">
                <Box component="h4" style={{ textTransform: 'uppercase' }}>
                  {document.kycDocType}
                </Box>

                <IconButton size="small" onClick={() => removeDocument(index)}>
                  <RemoveCircleIcon color="error" />
                </IconButton>
              </Box>
            ))}
          </Box>
        </Grid>
      </Grid> */}

      <Box display="flex" justifyContent="flex-end" marginY="20px">
        <Box className={classes.previousBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => setRedirectTo('/profile/org/process/2')}
          >
            <KeyboardArrowLeftIcon /> Previous
          </Button>
        </Box>

        {/* <Box className={classes.skipBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => handleFormSubmit(Action.SKIP)}
          >
            Skip
          </Button>
        </Box> */}

        <Box className={classes.nextBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => handleFormSubmit(Action.SUBMIT)}
          >
            {submitButtonText} <KeyboardArrowRightIcon />
          </Button>
        </Box>
      </Box>
    </div>
  );
};

export default OrganizationOtherInformation;
