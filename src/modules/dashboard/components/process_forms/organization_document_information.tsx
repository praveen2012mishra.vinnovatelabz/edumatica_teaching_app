import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  MenuItem,
  Select,
  FormHelperText,
  Typography
} from '@material-ui/core';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument
} from '../../../common/api/document';
import { KycDocument } from '../../../common/contracts/kyc_document';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { DocumentType } from '../../../common/enums/document_type';
import { PAN_PATTERN, GSTIN_PATTERN } from '../../../common/validations/patterns';
import {
  KeyboardArrowLeft as KeyboardArrowLeftIcon
} from '@material-ui/icons';
import { Organization } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { Redirect } from 'react-router-dom';
import { BusinessType } from '../../../common/enums/business_types';
import { useForm } from 'react-hook-form';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';
import { eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Dropzone from '../../../common/components/dropzone/dropzone';
import UploadedContent from '../../../common/components/dropzone/previewers/uploadedContent'
import { exceptionTracker } from '../../../common/helpers';
import { uploadFileOnUrl } from '../../../common/api/academics';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 40px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
      }
    },
    skipBtn: {
      '& button': {
        padding: '10px 25px 10px 25px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',
      },
    },
    previousBtn: {
      '& button': {
        padding: '9px 20px 9px 10px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',

        '& svg': {
          marginRight: '20px'
        }
      }
    },
    helperText: {
      fontWeight: 500,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: 500,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    removeIcon: {
      color: '#F9BD33'
    },
    docList: {
      borderBottom: '1px dashed #EAE9E4',
      borderTop: '1px dashed #EAE9E4',
      margin: '25px 0'
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  pageError: string;
  businessType: string;
  businessPAN: string;
  businessName: string;
  ownerPAN: string;
  ownerName: string;
  gstin: string;
}

interface Props {
  user: Organization;
  submitButtonText: string;
  saveUser: (data: Organization) => any;
}

interface DateError {
  dob: string | boolean;
}

enum Action {
  SKIP,
  SUBMIT
}

const OrganizationDocumentInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  // const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  // const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  // const [dropzoneKey, setDropzoneKey] = useState(0);
  const [businessType, setBusinessType] = useState(user.businessType || '');
  const [dob, setDob] = useState<Date | null>(user.dob ? new Date(user.dob) : null);
  const [dateError, setDateError] = useState<DateError>({dob: false})
  const [businessPAN, setBusinessPAN] = useState(user.businessPAN || '');
  const [businessName, setBusinessName] = useState(user.businessName || '');
  const [ownerPAN, setOwnerPAN] = useState(user.ownerPAN || '');
  const [ownerName, setOwnerName] = useState(user.ownerName || '');
  const [gstin, setGstin] = useState(user.gstin || '')

  const [droppedFilesAdh, setDroppedFilesAdh] = useState<File[]>([]);
  const [droppedFilesBPan, setDroppedFilesBPan] = useState<File[]>([]);
  const [droppedFilesDPan, setDroppedFilesDPan] = useState<File[]>([]);
  const [droppedFilesGN, setDroppedFilesGN] = useState<File[]>([]);
  const [droppedFilesBK, setDroppedFilesBK] = useState<File[]>([]);

  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Organization Login Form', 'Landed Business Info');

  // const [documents, setDocuments] = useState<KycDocument[]>(
  //   user.kycDetails || []
  // );
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDobChange = (date: Date | null) => {
    if(date) {
      setDateError({dob: false})
    }
    setDob(date);
  };

  // const addDocument = async () => {
  //   if (droppedFiles.length < 1) return;

  //   const file = droppedFiles[0];

  //   const clonedDocuments = [...documents];
  //   const documentIndex = clonedDocuments.findIndex(
  //     (document) =>
  //       document.kycDocType.toLowerCase() === documentType.toLowerCase()
  //   );

  //   if (documentIndex > -1) {
  //     clonedDocuments.splice(documentIndex, 1);
  //   }

  //   setDocuments([
  //     ...clonedDocuments,
  //     {
  //       kycDocFormat: file.type,
  //       kycDocType: documentType,
  //       kycDocLocation: file.name,
  //     },
  //   ]);
  //   setDroppedFiles([]);
  //   setDropzoneKey(dropzoneKey + 1);

  //   const formData = new FormData();

  //   formData.append('document', file);

  //   try {
  //     const awsBucket = await fetchUploadUrlForKycDocument({
  //       fileName: file.name,
  //       contentType: file.type,
  //       contentLength: file.size,
  //     });

  //     await uploadFileOnUrl(awsBucket.url, file);
  //   }
  //   catch(error){
  //     if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //       setRedirectTo('/login');
  //     }
  //   }
  // };

  // const removeDocument = (index: number) => {
  //   const clonedDocuments = [...documents];

  //   clonedDocuments.splice(index, 1);

  //   setDocuments(clonedDocuments);
  // };

  const handleFormSubmit = async (action: Action) => {
    if (action === Action.SKIP) {
      saveUser(user);
      return;
    }

    let clonedDocuments:KycDocument[] = [];
    if(droppedFilesAdh[0]) {
      const file = droppedFilesAdh[0];

      const formData = new FormData();
      formData.append('document', file);
      try {
        const awsBucket = await fetchUploadUrlForKycDocument({
          fileName: file.name,
          contentType: file.type,
          contentLength: file.size
        });

        await uploadFileOnUrl(awsBucket.url, file);

        const newDoc = [{
          kycDocFormat: file.type,
          kycDocType: DocumentType.AADHAR,
          kycDocLocation: file.name,
          uuid: awsBucket.uuid,
          url: awsBucket.url
        }]
  
        clonedDocuments = [...clonedDocuments, ...newDoc]
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }

    if(droppedFilesBPan[0]) {
      const file = droppedFilesBPan[0];

      const formData = new FormData();
      formData.append('document', file);
      try {
        const awsBucket = await fetchUploadUrlForKycDocument({
          fileName: file.name,
          contentType: file.type,
          contentLength: file.size
        });

        await uploadFileOnUrl(awsBucket.url, file);

        const newDoc = [{
          kycDocFormat: file.type,
          kycDocType: DocumentType.BUSINESS_PAN,
          kycDocLocation: file.name,
          uuid: awsBucket.uuid,
          url: awsBucket.url
        }]
  
        clonedDocuments = [...clonedDocuments, ...newDoc]
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }

    if(droppedFilesDPan[0]) {
      const file = droppedFilesDPan[0];
      
      const formData = new FormData();
      formData.append('document', file);
      try {
        const awsBucket = await fetchUploadUrlForKycDocument({
          fileName: file.name,
          contentType: file.type,
          contentLength: file.size
        });

        await uploadFileOnUrl(awsBucket.url, file);

        const newDoc = [{
          kycDocFormat: file.type,
          kycDocType: DocumentType.OWNER_PAN,
          kycDocLocation: file.name,
          uuid: awsBucket.uuid,
          url: awsBucket.url
        }]
  
        clonedDocuments = [...clonedDocuments, ...newDoc]
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }

    if(droppedFilesGN[0]) {
      const file = droppedFilesGN[0];
      
      const formData = new FormData();
      formData.append('document', file);
      try {
        const awsBucket = await fetchUploadUrlForKycDocument({
          fileName: file.name,
          contentType: file.type,
          contentLength: file.size
        });

        await uploadFileOnUrl(awsBucket.url, file);

        const newDoc = [{
          kycDocFormat: file.type,
          kycDocType: DocumentType.GST,
          kycDocLocation: file.name,
          uuid: awsBucket.uuid,
          url: awsBucket.url
        }]
  
        clonedDocuments = [...clonedDocuments, ...newDoc]
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }

    if(droppedFilesBK[0]) {
      const file = droppedFilesBK[0];

      const formData = new FormData();
      formData.append('document', file);
      try {
        const awsBucket = await fetchUploadUrlForKycDocument({
          fileName: file.name,
          contentType: file.type,
          contentLength: file.size
        });

        await uploadFileOnUrl(awsBucket.url, file);

        const newDoc = [{
          kycDocFormat: file.type,
          kycDocType: DocumentType.BANK,
          kycDocLocation: file.name,
          uuid: awsBucket.uuid,
          url: awsBucket.url
        }]
  
        clonedDocuments = [...clonedDocuments, ...newDoc]
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    } 

    saveUser({
      ...user,
      kycDetails: clonedDocuments
    });
  };

  return (
    <div>
      <Grid container>
        {/* <Grid item xs={12} md={4} /> */}
        <Grid item xs={12} md={8}>
          <Box component="h2" className={classes.helperText}>
            Please Upload Documents
          </Box>
        </Grid>
      </Grid>

      <Typography color="textSecondary">This Section is not Mandatory</Typography>

        <Grid container>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>Document Types</Typography>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '20px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>1. Aadhaar Card</Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesAdh(files)}}
                  files={droppedFilesAdh}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesAdh}
                  onRemoveItem={(file, index) => setDroppedFilesAdh([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesAdh[0] &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesAdh[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '20px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>2. Business PAN</Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesBPan(files)}}
                  files={droppedFilesBPan}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesBPan}
                  onRemoveItem={(file, index) => setDroppedFilesBPan([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesBPan[0] &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesBPan[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '20px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>3. Director PAN</Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesDPan(files)}}
                  files={droppedFilesDPan}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesDPan}
                  onRemoveItem={(file, index) => setDroppedFilesDPan([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesDPan[0] &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesDPan[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '20px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>4. GSTIN</Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesGN(files)}}
                  files={droppedFilesGN}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesGN}
                  onRemoveItem={(file, index) => setDroppedFilesGN([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesGN[0] &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesGN[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '20px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>5. Cancelled Cheque</Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesBK(files)}}
                  files={droppedFilesBK}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesBK}
                  onRemoveItem={(file, index) => setDroppedFilesBK([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesBK[0] &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesBK[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
        </Grid>

      <Box display="flex" justifyContent="flex-end" marginY="20px">
        <Box className={classes.previousBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => setRedirectTo('/profile/org/process/2')}
          >
            <KeyboardArrowLeftIcon /> Previous
          </Button>
        </Box>

        <Box className={classes.skipBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => handleFormSubmit(Action.SKIP)}
          >
            Skip and Save
          </Button>
        </Box>

        <Box className={classes.nextBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => handleFormSubmit(Action.SUBMIT)}
          >
            {submitButtonText}
          </Button>
        </Box>
      </Box>
    </div>
  );
};

export default OrganizationDocumentInformation;
