import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  MenuItem,
  Select,
  FormHelperText,
  Typography
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { PAN_PATTERN, GSTIN_PATTERN, AADHAAR_PATTERN } from '../../../common/validations/patterns';
import {
  KeyboardArrowLeft as KeyboardArrowLeftIcon
} from '@material-ui/icons';
import { Tutor } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { Redirect } from 'react-router-dom';
import { BusinessType } from '../../../common/enums/business_types';
import { useForm } from 'react-hook-form';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';
import { eventTracker, GAEVENTCAT } from '../../../common/helpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 40px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
      }
    },
    skipBtn: {
      '& button': {
        padding: '10px 25px 10px 25px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',
      },
    },
    previousBtn: {
      '& button': {
        padding: '9px 20px 9px 10px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',

        '& svg': {
          marginRight: '20px'
        }
      }
    },
    helperText: {
      fontWeight: 500,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: 500,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    removeIcon: {
      color: '#F9BD33'
    },
    docList: {
      borderBottom: '1px dashed #EAE9E4',
      borderTop: '1px dashed #EAE9E4',
      margin: '25px 0'
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  pageError: string;
  aadhaar: string;
  pan: string;
  gstin: string;
}

interface Props {
  user: Tutor;
  submitButtonText: string;
  saveUser: (data: Tutor) => any;
}

enum Action {
  SKIP,
  SUBMIT
}

const TutorOtherInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  // const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  // const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  // const [dropzoneKey, setDropzoneKey] = useState(0);
  
  const [pan, setPan] = useState(user.pan || '');
  const [aadhaar, setAadhaar] = useState(user.aadhaar || '');
  const [gstin, setGstin] = useState(user.gstin || '')

  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Tutor Login Form', 'Landed Business Info');

  // const [documents, setDocuments] = useState<KycDocument[]>(
  //   user.kycDetails || []
  // );
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  // const addDocument = async () => {
  //   if (droppedFiles.length < 1) return;

  //   const file = droppedFiles[0];

  //   const clonedDocuments = [...documents];
  //   const documentIndex = clonedDocuments.findIndex(
  //     (document) =>
  //       document.kycDocType.toLowerCase() === documentType.toLowerCase()
  //   );

  //   if (documentIndex > -1) {
  //     clonedDocuments.splice(documentIndex, 1);
  //   }

  //   setDocuments([
  //     ...clonedDocuments,
  //     {
  //       kycDocFormat: file.type,
  //       kycDocType: documentType,
  //       kycDocLocation: file.name,
  //     },
  //   ]);
  //   setDroppedFiles([]);
  //   setDropzoneKey(dropzoneKey + 1);

  //   const formData = new FormData();

  //   formData.append('document', file);

  //   try {
  //     const awsBucket = await fetchUploadUrlForKycDocument({
  //       fileName: file.name,
  //       contentType: file.type,
  //       contentLength: file.size,
  //     });

  //     await uploadKycDocument(awsBucket.url, formData);
  //   }
  //   catch(error){
  //     if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //       setRedirectTo('/login');
  //     }
  //   }
  // };

  // const removeDocument = (index: number) => {
  //   const clonedDocuments = [...documents];

  //   clonedDocuments.splice(index, 1);

  //   setDocuments(clonedDocuments);
  // };

  const handleFormSubmit = async (action: Action) => {
    if (action === Action.SKIP) {
      // if(!PAN_PATTERN.test(ownerPAN)){
      //   setError(
      //     'emailId',
      //     'Invalid Data',
      //     'Invalid Email'
      //   );
      //   return;
      // }
      // else{
      //   clearError('emailId');
      // }
      saveUser(user);
      return;
    }

    //if(documents.length < 1) return;

    if (!pan.length) {
      setError('pan', 'Invalid Data', 'PAN cannot be empty');
      return;
    } else {
      clearError('pan');
    }
    if (!PAN_PATTERN.test(pan)) {
      setError('pan', 'Invalid Data', 'Invalid PAN data');
      return;
    } else {
      clearError('pan');
    }

    if (!aadhaar.length) {
      setError('aadhaar', 'Invalid Data', 'AADHAAR cannot be empty');
      return;
    } else {
      clearError('aadhaar');
    }
    if (!AADHAAR_PATTERN.test(aadhaar)) {
      setError('aadhaar', 'Invalid Data', 'Invalid AADHAAR');
      return;
    } else {
      clearError('aadhaar');
    }

    if (gstin.length) {
      if (!GSTIN_PATTERN.test(gstin)) {
        setError('gstin', 'Invalid Data', 'Invalid GSTIN');
        return;
      } else {
        clearError('gstin');
      }
    }

    saveUser({
      ...user,
      aadhaar: aadhaar,
      pan: pan,
      gstin: gstin
      //kycDetails: documents
    });
  };

  return (
    <div>
      <Grid container>
        {/* <Grid item xs={12} md={4} /> */}
        <Grid item xs={12} md={8}>
          <Box component="h2" className={classes.helperText}>
            Please Enter the Business Details
          </Box>
        </Grid>
      </Grid>

      <Typography color="textSecondary"><span style={{color: 'crimson'}}>*</span> Fields are Mandatory</Typography>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>PAN<span style={{color: 'crimson'}}>*</span></Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="PAN of the Tutor"
              value={pan}
              inputProps={{ maxLength: 10 }}
              onChange={(e) => setPan(e.target.value)}
              className={errors.pan ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.pan && (
            <FormHelperText error>{errors.pan.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Aadhaar<span style={{color: 'crimson'}}>*</span></Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Aadhaar"
              value={aadhaar}
              inputProps={{ maxLength: 12 }}
              onChange={(e) => setAadhaar(e.target.value)}
              className={errors.aadhaar ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.aadhaar && (
            <FormHelperText error>{errors.aadhaar.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>GSTIN</Box>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="GSTIN"
              value={gstin}
              inputProps={{ maxLength: 15 }}
              onChange={(e) => setGstin(e.target.value)}
              className={errors.gstin ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.gstin && (
            <FormHelperText error>{errors.gstin.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      {/* <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Document Type
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Select
              value={documentType}
              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                setDocumentType(e.target.value as DocumentType)
              }
            >
              <MenuItem value={DocumentType.PAN}>PAN Card</MenuItem>
              <MenuItem value={DocumentType.AADHAR}>Aadhar Card</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Upload
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Dropzone
              key={dropzoneKey}
              acceptedFiles={['image/jpeg', 'image/png']}
              maxFileSize={104857600} // 100 MB
              files={droppedFiles}
              onChange={(files) => setDroppedFiles(files)}
              onRemoveItem={(file, index) => setDroppedFiles([])}
            />
          </FormControl>
        </Grid>
      </Grid>

      <FormControl fullWidth margin="normal">
        <Box display="flex" justifyContent="flex-end">
          <Button
            disableElevation
            color="primary"
            size="small"
            variant="contained"
            onClick={addDocument}
          >
            <AddIcon /> Add
          </Button>
        </Box>
      </FormControl>

      <Grid container>
        <Grid item xs={12} md={4}></Grid>

        <Grid item xs={12} md={8}>
          <Box>
            {documents.map((document, index) => (
              <Box display="flex" justifyContent="space-between">
                <Box component="h4" style={{ textTransform: 'uppercase' }}>
                  {document.kycDocType}
                </Box>

                <IconButton size="small" onClick={() => removeDocument(index)}>
                  <RemoveCircleIcon color="error" />
                </IconButton>
              </Box>
            ))}
          </Box>
        </Grid>
      </Grid> */}

      <Box display="flex" justifyContent="flex-end" marginY="20px">
        <Box className={classes.previousBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => setRedirectTo('/profile/process/2')}
          >
            <KeyboardArrowLeftIcon /> Previous
          </Button>
        </Box>

        <Box className={classes.nextBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => handleFormSubmit(Action.SUBMIT)}
          >
            {submitButtonText}
          </Button>
        </Box>
      </Box>
    </div>
  );
};

export default TutorOtherInformation;
