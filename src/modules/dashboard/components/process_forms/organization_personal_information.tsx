import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  FormHelperText,
  Typography
} from '@material-ui/core';
import { KeyboardArrowRight as KeyboardArrowRightIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { fetchCitiesByPinCode } from '../../../common/api/academics';
import { Organization } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { useForm } from 'react-hook-form';
import {
  EMAIL_PATTERN,
  ORG_NAME_PATTERN,
  PIN_PATTERN
} from '../../../common/validations/patterns';
import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 25px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          marginLeft: '20px'
        }
      }
    },
    helperText: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props {
  user: Organization;
  submitButtonText: string;
  saveUser: (data: Organization) => any;
}

interface FormData {
  pageError: string;
  organizationName: string;
  emailId: string;
  address: string;
  cityName: string;
  pinCode: string;
  stateName: string;
}

const OrganizationPersonalInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  const { enqueueSnackbar } = useSnackbar();
  const [name, setName] = useState(user.organizationName || '');
  const [email, setEmail] = useState(user.emailId || '');
  const [address, setAddress] = useState(user.address || '');
  const [pinCode, setPinCode] = useState(user.pinCode || '');
  const [cityName, setCityName] = useState(user.city || '');
  const [stateName, setStateName] = useState(user.stateName || '');
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Organization Login Form', 'Landed Personal Info');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  // const TypeOther = () => (
  //   <FormControl fullWidth margin="normal">
  //     <Input placeholder="Others" />
  //   </FormControl>
  // );

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };

  const submitPersonalInformation = (e: React.FormEvent) => {
    e.preventDefault();
    if (!name.length) {
      setError('organizationName', 'Invalid Data', 'Name cannot be empty');
      return;
    } else {
      clearError('organizationName');
    }

    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'organizationName',
        'Invalid Data',
        'Institute name should be minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('organizationName');
    }

    if (!ORG_NAME_PATTERN.test(name)) {
      setError('organizationName', 'Invalid Data', 'Invalid name');
      return;
    } else {
      clearError('organizationName');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email sholud not be empty');
      return;
    } else {
      clearError('emailId');
    }

    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (!address.length) {
      setError('address', 'Invalid Data', 'Address cannot be empty');
      return;
    } else {
      clearError('address');
    }

    if (address.length < 5) {
      setError(
        'address',
        'Invalid Data',
        'Address should be minimum 5 characters long'
      );
      return;
    } else {
      clearError('address');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'PinCode cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    // TODO: Pincode length changes
    if (pinCode.length < 5) {
      setError(
        'pinCode',
        'Invalid Data',
        'PinCode should be minimum 5 characters long'
      );
      return;
    } else {
      clearError('pinCode');
    }

    saveUser({
      ...user,
      organizationName: name,
      emailId: email,
      address: address,
      city: cityName,
      pinCode: pinCode,
      stateName: stateName
    });
  };

  return (
    <div>
      <form onSubmit={submitPersonalInformation}>
        <Grid container>
          {/* <Grid item xs={12} md={4} /> */}
          <Grid item xs={12} md={8}>
            <Box component="h2" className={classes.helperText}>
              Please Enter the Details
            </Box>
          </Grid>
        </Grid>

        <Typography color="textSecondary">All Fields are Mandatory</Typography>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Institute Name</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Institute Name"
                value={name}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setName(e.target.value)}
                className={errors.organizationName ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.organizationName && (
              <FormHelperText error>
                {errors.organizationName.message}
              </FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Email ID</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Institute Email"
                value={email}
                inputProps={{ maxLength: 100 }}
                onChange={(e) => setEmail(e.target.value)}
                className={errors.emailId ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.emailId && (
              <FormHelperText error>{errors.emailId.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Address</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                inputProps={{ maxLength: 250 }}
                placeholder="Institute Address"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                className={errors.address ? `${classes.error} ${classes.formInput}` : classes.formInput}
                multiline={true}
              />
            </FormControl>
            {errors.address && (
              <FormHelperText error>{errors.address.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Pin Code</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                inputProps={{
                  inputMode: 'numeric',
                  minLength: 6,
                  maxLength: 6
                }}
                placeholder="Pin Code"
                value={pinCode}
                onChange={(e) => onPinCodeChange(e.target.value)}
                className={errors.pinCode ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.pinCode && (
              <FormHelperText error>{errors.pinCode.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>City</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Box>{cityName}</Box>
            </FormControl>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>State</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Box>{stateName}</Box>
            </FormControl>
          </Grid>
        </Grid>

        {/* <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Location
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Autocomplete
              options={locationsList}
              getOptionLabel={(option: AutocompleteOption) => option.title}
              autoComplete
              includeInputInList
              value={{ title: address, value: address }}
              onInputChange={(_, value) => { fetchAddresses(value); setAddress(value); }}
              // onChange={(e, node) => setAddress(node && node.value ? node.value : '')}
              renderInput={(params) => (
                <TextField {...params} placeholder="Enter Location" />
              )}
            />
          </FormControl>
        </Grid>
      </Grid> */}

        <Box
          display="flex"
          justifyContent="flex-end"
          marginY="20px"
          className={classes.nextBtn}
        >
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="medium"
            type="submit"
          >
            {submitButtonText} <KeyboardArrowRightIcon />
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default OrganizationPersonalInformation;
