import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  FormHelperText,
  Select,
  MenuItem
} from '@material-ui/core';
import { KeyboardArrowRight as KeyboardArrowRightIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Parent } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { useForm } from 'react-hook-form';
import {
  NAME_PATTERN,
  EMAIL_PATTERN,
} from '../../../common/validations/patterns';
import Datepickers from '../../../common/components/datepickers'
import { eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { genders } from '../../../common/constants/genders'
import { countries } from '../../../common/constants/countries'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '15px 25px 18px 70px',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '10px',

        '& svg': {
          marginLeft: '20px'
        }
      }
    },
    helperText: {
      fontWeight: 500,
      fontSize: '20px',
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: 500,
      fontSize: '16px',
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: '16px',
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    }
  })
);

interface Props {
  user: Parent;
  submitButtonText: string;
  saveUser: (data: Parent) => any;
}

interface FormData {
  pageError: string;
  parentName: string;
  emailId: string;
  dob: Date;
  gender: string;
  country: string;
}

const ParentPersonalInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Parent Login Form', 'Landed Personal Info');

  const { errors, setError, clearError } = useForm<FormData>();
  const [name, setName] = useState(user.parentName || '');
  const [email, setEmail] = useState(user.emailId || '');
  const [dob, setDob] = useState<Date | null>(new Date(user.dob ? user.dob : new Date()));
  const [gender, setGender] = useState<string>(user.gender || '');
  const [country, setCountry] = useState<string>(user.country || 'India');

  const handleDobChange = (date: Date | null) => {
    setDob(date);
  };

  const submitPersonalInformation = (e: React.FormEvent) => {
    e.preventDefault();
    if (!name.length) {
      setError('parentName', 'Invalid Data', 'Guardian Name cannot be empty');
      return;
    } else {
      clearError('parentName');
    }

    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'parentName',
        'Invalid Data',
        'Guardian Name should be minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('parentName');
    }

    if (!NAME_PATTERN.test(name)) {
      setError('parentName', 'Invalid Data', 'Invalid tutor name');
      return;
    } else {
      clearError('parentName');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email cannot be empty');
      return;
    } else {
      clearError('emailId');
    }

    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (!dob) {
      setError('dob', 'Invalid Data', 'Invalid Date');
      return;
    } else {
      clearError('dob');
    }

    if (!country.length) {
      setError('country', 'Invalid Data', 'Country cannot be empty');
      return;
    } else {
      clearError('country');
    }

    if (!gender.length) {
      setError('gender', 'Invalid Data', 'Gender cannot be empty');
      return;
    } else {
      clearError('gender');
    }

    saveUser({
      ...user,
      parentName: name,
      emailId: email,
      dob: dob.toLocaleDateString("fr-CA"),
      gender: gender,
      country: country
    });
  };

  return (
    <div>
      <form onSubmit={submitPersonalInformation}>
        <Grid container>
          {/* <Grid item xs={12} md={4} /> */}
          <Grid item xs={12} md={8}>
            <Box component="h2" className={classes.helperText}>
              Please enter correct details
            </Box>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Guardian Name</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Full Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                className={classes.formInput}
              />
            </FormControl>
            {errors.parentName && (
              <FormHelperText error>{errors.parentName.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Country</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Select
                value={country}
                onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                  setCountry(e.target.value as string)
                }
                displayEmpty
                className={classes.formInput}
              >
                <MenuItem value="">Select Country</MenuItem>
                {countries.length > 0 &&
                  countries.map((item) => (
                    <MenuItem key={item} value={item}>{item}</MenuItem>
                  ))}
              </Select>
            </FormControl>
            {errors.country && (
              <FormHelperText error>{errors.country.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight="bold" className={classes.label}>
                Email Address
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Email Address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className={classes.formInput}
              />
            </FormControl>
            {errors.emailId && (
              <FormHelperText error>{errors.emailId.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight="bold" className={classes.label}>
                Date Of Birth
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Datepickers selectedDate={dob} handleDateChange={handleDobChange} 
                maxDate={new Date()}
              />
            </FormControl>
            {errors.dob && (
              <FormHelperText error>{errors.dob.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Gender</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Select
                value={gender}
                onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                  setGender(e.target.value as string)
                }
                displayEmpty
                className={classes.formInput}
              >
                <MenuItem value="">Select Gender</MenuItem>
                {genders.length > 0 &&
                  genders.map((item) => (
                    <MenuItem key={item} value={item}>{item}</MenuItem>
                  ))}
              </Select>
            </FormControl>
            {errors.gender && (
              <FormHelperText error>{errors.gender.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Box
          display="flex"
          justifyContent="flex-end"
          marginY="20px"
          className={classes.nextBtn}
        >
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            type="submit"
          >
            {submitButtonText} <KeyboardArrowRightIcon />
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default ParentPersonalInformation;
