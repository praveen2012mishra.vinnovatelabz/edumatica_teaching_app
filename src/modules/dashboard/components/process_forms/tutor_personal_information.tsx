import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Box,
  FormControl,
  Grid,
  Input,
  TextField,
  FormHelperText,
  Typography
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { KeyboardArrowRight as KeyboardArrowRightIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { AutocompleteOption } from '../../../common/contracts/autocomplete_option';
import * as dateFns from "date-fns";
import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode
} from '../../../common/api/academics';
import { Tutor } from '../../../common/contracts/user';
import Button from '../../../common/components/form_elements/button';
import { useForm } from 'react-hook-form';
import {
  NAME_PATTERN,
  EMAIL_PATTERN,
  PIN_PATTERN
} from '../../../common/validations/patterns';
import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 25px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          marginLeft: '20px'
        }
      }
    },
    helperText: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props {
  user: Tutor;
  submitButtonText: string;
  saveUser: (data: Tutor) => any;
}

interface DateError {
  dob: string | boolean;
}

interface FormData {
  pageError: string;
  tutorName: string;
  emailId: string;
  qualifications: string;
  schoolName: string;
  cityName: string;
  pinCode: string;
  stateName: string;
  address: string;
  otherQualifications: string;
  almaMater: string;
}

const TutorPersonalInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  submitButtonText
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  const { enqueueSnackbar } = useSnackbar();
  const [name, setName] = useState(user.tutorName || '');
  const [email, setEmail] = useState(user.emailId || '');
  const [dob, setDob] = useState<Date | null>(user.dob ? new Date(user.dob) : null);
  const [dateError, setDateError] = useState<DateError>({dob: false})
  const [qualification, setQualification] = useState<AutocompleteOption | null>(
    user.qualifications && user.qualifications.length > 0
      ? { title: user.qualifications[0], value: user.qualifications[0] }
      : null
  );
  const [school, setSchool] = useState<AutocompleteOption | null>(
    user.schoolName && user.schoolName.length > 0
      ? { title: user.schoolName, value: user.schoolName }
      : null
  );
  const [address, setAddress] = useState(user.address || '');
  const [otherQualifications, setOtherQualifications] = useState(user.otherQualifications || '');
  const [almaMater, setAlmaMater] = useState(user.almaMater || '');
  const [pinCode, setPinCode] = useState(user.pinCode || '');
  const [cityName, setCityName] = useState(user.cityName || '');
  const [stateName, setStateName] = useState(user.stateName || '');
  const [qualificationsList, setQualificationsList] = useState<
    AutocompleteOption[]
  >([]);
  const [schoolsList, setSchoolsList] = useState<AutocompleteOption[]>([]);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.registration, 'Tutor Login Form', 'Landed Personal Info');

  useEffect(() => {
    (async () => {
      try {
        const qualificationsListResponse = await fetchQualificationsList();

        const structuredQualificationsList = qualificationsListResponse.map(
          (qualification) => ({
            title: `${qualification.degree} (${qualification.subjectName})`,
            value: qualification.degree
          })
        );

        setQualificationsList([
          ...structuredQualificationsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, []);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDobChange = (date: Date | null) => {
    if(date) {
      setDateError({dob: false})
    }
    setDob(date);
  };

  // const TypeOther = () => (
  //   <FormControl fullWidth margin="normal">
  //     <Input placeholder="Others" />
  //   </FormControl>
  // );

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchool(null);
        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };

  const submitPersonalInformation = (e: React.FormEvent) => {
    e.preventDefault();
    if (!name.length) {
      setError('tutorName', 'Invalid Data', 'Tutor Name cannot be empty');
      return;
    } else {
      clearError('tutorName');
    }

    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'tutorName',
        'Invalid Data',
        'Tutor Name should be minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('tutorName');
    }

    if (!NAME_PATTERN.test(name)) {
      setError('tutorName', 'Invalid Data', 'Invalid tutor name');
      return;
    } else {
      clearError('tutorName');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email cannot be empty');
      return;
    } else {
      clearError('emailId');
    }

    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (!address.length) {
      setError('address', 'Invalid Data', 'Address cannot be empty');
      return;
    } else {
      clearError('address');
    }

    if (!dob) {
      setDateError({dob: 'Date of Birth Required'})
      return;
    }

    if (dateFns.differenceInYears(new Date(), dob) < 16) {
      setDateError({dob: 'You Should be atleast 16 years in age'})
      return;
    }

    if (qualification === null || !qualification.value.length) {
      setError(
        'qualifications',
        'Invalid Data',
        'qualification cannot be empty'
      );
      return;
    } else {
      clearError('qualifications');
    }
    if (qualification && qualification.value.length < 3) {
      setError(
        'qualifications',
        'Invalid Data',
        'qualification should be minimum 3 characters long'
      );
      return;
    } else {
      clearError('qualifications');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'Pin Code cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    if (!PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid Pin Code');
      return;
    } else {
      clearError('pinCode');
    }

    if (school === null || !school.value.length) {
      setError('schoolName', 'Invalid Data', 'school name cannot be empty');
      return;
    } else {
      clearError('schoolName');
    }
    if (school && school.value.length < 5) {
      setError(
        'schoolName',
        'Invalid Data',
        'school name should be minimum 5 characters long'
      );
      return;
    } else {
      clearError('schoolName');
    }

    saveUser({
      ...user,
      tutorName: name,
      emailId: email,
      dob: dob.toLocaleDateString("fr-CA"),
      qualifications: qualification?.value ? [qualification.value] : [],
      schoolName: school?.value ? school.value : '',
      pinCode: pinCode,
      cityName: cityName,
      stateName: stateName,
      address: address,
      otherQualifications: otherQualifications ? otherQualifications : undefined,
      almaMater: almaMater ? almaMater : undefined
    });
  };

  return (
    <div>
      <form onSubmit={submitPersonalInformation}>
        <Grid container>
          {/* <Grid item xs={12} md={4} /> */}
          <Grid item xs={12} md={8}>
            <Box component="h2" className={classes.helperText}>
              Please Enter the Details
            </Box>
          </Grid>
        </Grid>

        <Typography color="textSecondary"><span style={{color: 'crimson'}}>*</span> Fields are Mandatory</Typography>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Tutor Name<span style={{color: 'crimson'}}>*</span></Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                className={errors.tutorName ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.tutorName && (
              <FormHelperText error>{errors.tutorName.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                Email Address<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className={errors.emailId ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.emailId && (
              <FormHelperText error>{errors.emailId.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                Date Of Birth<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Datepickers selectedDate={dob} handleDateChange={handleDobChange} 
                maxDate={new Date()}
              />
            </FormControl>
            {dateError.dob &&
              <Typography className={classes.dateError}>{dateError.dob}</Typography>
            }
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>Address<span style={{color: 'crimson'}}>*</span></Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                inputProps={{ maxLength: 250 }}
                placeholder="Your Address"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                className={errors.address ? `${classes.error} ${classes.formInput}` : classes.formInput}
                multiline={true}
              />
            </FormControl>
            {errors.address && (
              <FormHelperText error>{errors.address.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                Qualifications<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Autocomplete
                options={qualificationsList}
                getOptionLabel={(option: AutocompleteOption) => option.title}
                autoComplete
                includeInputInList
                onChange={(e, node) => setQualification(node)}
                value={qualification}
                renderInput={(params) => (
                  <TextField {...params} placeholder="Select Qualification" />
                )}
                className={errors.qualifications ? classes.error : ''}
              />
            </FormControl>
            {/* {qualification?.value === 'Other' && <TypeOther />} */}
            {errors.qualifications && (
              <FormHelperText error>
                {errors.qualifications.message}
              </FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>Other Qualifications</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Other Qualifications"
                value={otherQualifications}
                onChange={(e) => setOtherQualifications(e.target.value)}
                className={errors.otherQualifications ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.otherQualifications && (
              <FormHelperText error>{errors.otherQualifications.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>University</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Alma Mater"
                value={almaMater}
                onChange={(e) => setAlmaMater(e.target.value)}
                className={errors.almaMater ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.almaMater && (
              <FormHelperText error>{errors.almaMater.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                Pin Code<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Institute PIN Code"
                value={pinCode}
                inputProps={{ maxLength: 6 }}
                onChange={(e) => onPinCodeChange(e.target.value)}
                className={errors.pinCode ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.pinCode && (
              <FormHelperText error>{errors.pinCode.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                City<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Box>{cityName}</Box>
            </FormControl>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                State<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Box>{stateName}</Box>
            </FormControl>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth margin="normal">
              <Box fontWeight={fontOptions.weight.bold} className={classes.label}>
                Schools / Others<span style={{color: 'crimson'}}>*</span>
              </Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={8}>
            <FormControl fullWidth margin="normal">
              <Autocomplete
                options={schoolsList}
                getOptionLabel={(option: AutocompleteOption) => option.title}
                autoComplete
                includeInputInList
                value={school}
                onChange={(e, node) => setSchool(node)}
                renderInput={(params) => (
                  <TextField {...params} placeholder="Select School" />
                )}
                className={errors.schoolName ? classes.error : ''}
              />
            </FormControl>
            {/* {school?.value === 'Other' && <TypeOther />} */}
            {errors.schoolName && (
              <FormHelperText error>{errors.schoolName.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        {/* <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Location
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Autocomplete
              options={locationsList}
              getOptionLabel={(option: AutocompleteOption) => option.title}
              autoComplete
              includeInputInList
              value={{ title: address, value: address }}
              onInputChange={(_, value) => { fetchAddresses(value); setAddress(value); }}
              // onChange={(e, node) => setAddress(node && node.value ? node.value : '')}
              renderInput={(params) => (
                <TextField {...params} placeholder="Enter Location" />
              )}
            />
          </FormControl>
        </Grid>
      </Grid> */}

        <Box
          display="flex"
          justifyContent="flex-end"
          marginY="20px"
          className={classes.nextBtn}
        >
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="medium"
            type="submit"
          >
            {submitButtonText} <KeyboardArrowRightIcon />
          </Button>
        </Box>
      </form>
    </div>
  );
};

export default TutorPersonalInformation;
