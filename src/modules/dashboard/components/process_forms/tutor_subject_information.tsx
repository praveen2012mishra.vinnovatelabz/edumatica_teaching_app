import React, { FunctionComponent, useState, useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  Switch,
  Input,
  FormHelperText,
  Grid,
  MenuItem,
  Select,
  Typography
} from '@material-ui/core';
import {
  Add as AddIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
  KeyboardArrowLeft as KeyboardArrowLeftIcon
} from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { BoardClassSubjectsMap } from '../../../academics/contracts/board_class_subjects_map';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchSubjectsList
} from '../../../common/api/academics';
import { RootState } from '../../../../store';
import { setSubjects } from '../../../academics/store/actions';
import { Subject } from '../../../academics/contracts/subject';
import { Tutor } from '../../../common/contracts/user';
import { Course } from '../../../academics/contracts/course';
import CourseTable from '../course_table';
import Button from '../../../common/components/form_elements/button';
import { eventTracker, exceptionTracker, GAEVENTCAT } from '../../../common/helpers';
import { Standard } from '../../../academics/contracts/standard';
import { Board } from '../../../academics/contracts/board';
import { Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { fontOptions } from '../../../../theme';
import { checkCourseCount } from '../../../common/api/profile';
import {useSnackbar} from "notistack"
import { NAME_PATTERN } from '../../../common/validations/patterns';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nextBtn: {
      '& button': {
        padding: '10px 25px 10px 40px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          marginLeft: '20px'
        }
      }
    },
    skipBtn: {
      '& button': {
        padding: '10px 65px 12px 65px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',
      },
    },
    previousBtn: {
      '& button': {
        padding: '10px 40px 10px 25px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '21px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',
        marginRight: '40px',

        '& svg': {
          marginRight: '20px'
        }
      }
    },
    helperText: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#1C2559'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },

    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },

    inputCheck: {
      '& svg': {
        fill: '#000'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props {
  user: Tutor;
  subjects: Subject[];
  submitButtonText: string;
  saveUser: (data: Tutor) => any;
}

interface FormData {
  board: string;
  standard: string;
  subjects: string;
  customBoard: string;
  customStandard: string;
  customSubject: string;
}

enum Action {
  SKIP,
  SUBMIT
}

const TutorSubjectInformation: FunctionComponent<Props> = ({
  user,
  saveUser,
  subjects,
  submitButtonText
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [tutorSubjects, setTutorSubjects] = useState<Course[]>(
    user.courseDetails || []
  );
  const [board, setBoard] = useState('');
  const [standard, setStandard] = useState(''); // Standard describes Class of the Tutor.
  const [subjectsList, setSubjectsList] = useState<
    { name: string; checked: boolean }[]
  >([]);
  const [boards, setBoards] = useState<Board[]>([]);
  const [classes, setClasses] = useState<Standard[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  const { errors, setError, clearError } = useForm<FormData>();
  const [customCourse, setCustomCourse] = useState(false);
  const [customBoard, setCustomBoard] = useState('');
  const styles = useStyles();
  const [customStandard, setCustomStandard] = useState('');
  const [customSubject, setCustomSubject] = useState('');
  eventTracker(GAEVENTCAT.registration, 'Tutor Login Form', 'Landed Subject Info');

  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      try {
        const [boardsList] = await Promise.all([fetchBoardsList()]);
        setBoards(boardsList);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, []);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const setBoardAndFetchClasses = async (board: string) => {
    try {
      setBoard(board);
      if (board.length > 1) {
        const classListResponse = await fetchClassesList({ boardname: board });
        setClasses(classListResponse);
      } else {
        setClasses([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const setClassAndFetchSubjects = async (standard: string) => {
    try {
      setStandard(standard);
      if (board.length > 1 && standard.length > 1) {
        const response = await fetchSubjectsList({
          boardname: board,
          classname: standard
        });
        dispatch(setSubjects(response));
        const structuredSubjectsList = response.map((subject) => ({
          name: subject.subjectName,
          checked: false
        }));
        setSubjectsList(structuredSubjectsList);
      } else {
        setSubjectsList([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const handleChangeInSubjectCheckbox = (index: number) => {
    const subjects = subjectsList.map((subject, sIndex) => {
      if (index !== sIndex) return subject;

      return { ...subject, checked: !subject.checked };
    });

    setSubjectsList(subjects);
  };

  const addSubjectsToClass = () => {
    if (!customCourse) {
      if (board.length < 1) {
        setError('board', 'Invalid Data', 'Please select board name');
        return;
      } else {
        clearError('board');
      }
  
      if (standard.length < 1) {
        setError('standard', 'Invalid Data', 'Please select a class');
        return;
      } else {
        clearError('standard');
      }
  
      const map: Course[] = [];
  
      subjectsList
        .filter((subject) => subject.checked)
        .filter((subject) => {
          const subjectExists = tutorSubjects.find(
            (tutorSubject) =>
              tutorSubject.board.toLowerCase() === board.toLowerCase() &&
              tutorSubject.className.toString().toLowerCase() ===
                standard.toString().toLowerCase() &&
              tutorSubject.subject.toLowerCase() === subject.name.toLowerCase()
          );
  
          return subjectExists === undefined;
        })
        .forEach((subject) => {
          map.push({ board, className: standard, subject: subject.name });
        });
  
      if (!map.length) {
        setError('subjects', 'Invalid Data', 'Subject should not be empty');
        return;
      } else {
        clearError('subjects');
      }
  
      setTutorSubjects([...tutorSubjects, ...map]);
      //setSubjectsList([]);
    }else{
      const remSpace = customBoard.replace(/ /g, "")
      if (customBoard.length < 1) {
        setError(
          'customBoard',
          'Invalid Data',
          'Custom board name should not be empty'
        );
        return;
      } else if(remSpace.length < 4) {
        setError(
          'customBoard',
          'Invalid Data',
          'Custom board name should have atleast 4 Characters excluding spaces'
        );
        return;
      } else {
        clearError('customBoard');
      }
      if (!NAME_PATTERN.test(customBoard)) {
        setError('customBoard', 'Invalid Data', 'Invalid custom board name');
        return;
      } else {
        clearError('customBoard');
      }
      if (customStandard.length < 1) {
        setError(
          'customStandard',
          'Invalid Data',
          'Custom class name should not be empty'
        );
        return;
      } else {
        clearError('customStandard');
      }
      const remSpaceS = customStandard.replace(/ /g, "")
      if (remSpaceS.length < 3) {
        setError(
          'customStandard',
          'Invalid Data',
          'Custom class should be minimum 3 characters long excluding spaces'
        );
        return;
      } else {
        clearError('customStandard');
      }
      const map: Course[] = [];

      if (!customSubject.length) {
        setError(
          'customSubject',
          'Invalid Data',
          'Custom subject should not be empty'
        );
        return;
      } else {
        clearError('customSubject');
      }
      const remSpaceSu = customSubject.replace(/ /g, "")
      if (remSpaceSu.length < 3) {
        setError(
          'customSubject',
          'Invalid Data',
          'Custom subject should be minimum 3 characters long excluding spaces'
        );
        return;
      } else {
        clearError('customSubject');
      }
      const subjectExists = tutorSubjects.find(
        (tutorSubject) =>
          tutorSubject.board.toLowerCase() === customBoard.toLowerCase() &&
          tutorSubject.className.toString().toLowerCase() ===
            customStandard.toString().toLowerCase() &&
          tutorSubject.subject.toLowerCase() === customSubject.toLowerCase()
      );

      if (!subjectExists) {
        map.push({
          board: customBoard,
          className: customStandard,
          subject: customSubject
        });
      }
      if (!map.length) {
        setError('subjects', 'Invalid Data', 'Subject should not be empty');
        return;
      } else {
        clearError('subjects');
      }
      setTutorSubjects([...tutorSubjects, ...map]);
      setBoard('');
      setCustomBoard('');
      setStandard('');
      setCustomStandard('');
      //setSubjectsList([]);
      setCustomSubject('');
    }
    
  };

  const removeTutorSubjects = (map: BoardClassSubjectsMap) => {
    const mapSubjects = map.subjects.map((subject) => subject.toLowerCase());

    const tutorSubjectsMap = tutorSubjects.filter(
      (subjectItem) =>
        subjectItem.board !== map.boardname ||
        subjectItem.className.toString().toLowerCase() !==
          map.classname.toString().toLowerCase() ||
        mapSubjects.indexOf(subjectItem.subject.toLowerCase()) === -1
    );

    setTutorSubjects(tutorSubjectsMap);
  };

  const submitCourseInformation = async(action: Action) => {
    if (action === Action.SKIP) {
      saveUser(user);
      return;
    }
    if (tutorSubjects.length < 1) return;
    const check = await checkCourseCount(tutorSubjects)
    if(check.data.message !== 'Success') {
      enqueueSnackbar("Course Limit Exceeded", {variant: 'warning'});
    } else {
      saveUser({ ...user, courseDetails: tutorSubjects });
    }
  };

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < tutorSubjects.length; ++i) {
    const subject = tutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase()
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: []
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject
    );
  }

  const SubjectsCheckboxes = () => (
    <FormControl fullWidth margin="normal">
      <Box display="flex" justifyContent="space-between">
        {subjectsList.map((subject, index) => (
          <FormControlLabel
            key={index}
            label={subject.name}
            control={
              <Checkbox
                className={styles.inputCheck}
                name={subject.name}
                checked={subject.checked}
                onChange={() => handleChangeInSubjectCheckbox(index)}
              />
            }
          />
        ))}
      </Box>
      {errors.subjects && (
        <FormHelperText error>{errors.subjects.message}</FormHelperText>
      )}
    </FormControl>
  );

  return (
    <div>
      <Grid container>
        {/* <Grid item xs={12} md={4} /> */}
        <Grid item xs={12} md={8}>
          <Box component="h2" className={styles.helperText}>
            Please Enter the Course details
          </Box>
          <FormControlLabel
            control={
              <Switch
                checked={customCourse}
                onChange={(e) => setCustomCourse(e.target.checked)}
                color="secondary"
              />
            }
            label="Custom Course"
          />
        </Grid>
      </Grid>

      <Typography color="textSecondary">This Section is not Mandatory</Typography>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Board</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
        {!customCourse ? (
          <FormControl fullWidth margin="normal">
            <Select
              value={board}
              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                setBoardAndFetchClasses(e.target.value as string)
              }
              displayEmpty
              className={errors.board ? styles.error : ''}
            >
              <MenuItem value="">Select a board</MenuItem>
              {boards.length > 0 &&
                boards.map((item) => (
                  <MenuItem value={item.boardName} key={item.boardID}>
                    {item.boardName} ({item.boardDescriptions})
                  </MenuItem>
                ))}
            </Select>
            {errors.board && (
              <FormHelperText error>{errors.board.message}</FormHelperText>
            )}
          </FormControl>
        ):(
          <FormControl fullWidth margin="normal">
              <Input
                placeholder="Enter board name"
                value={customBoard}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setCustomBoard(e.target.value as string)}
                className={errors.customBoard ? `${styles.error} ${styles.formInput}` : styles.formInput}
              />
              {errors.customBoard && (
                <FormHelperText error>
                  {errors.customBoard.message}
                </FormHelperText>
              )}
            </FormControl>
        )}
          
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Classes</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
        {!customCourse ? (
          <FormControl fullWidth margin="normal">
            <Select
              displayEmpty
              value={standard}
              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                setClassAndFetchSubjects(e.target.value as string)
              }
              className={errors.standard ? styles.error : ''}
            >
              <MenuItem value="">Select a class</MenuItem>
              {classes.length > 0 &&
                classes.map((standard) => (
                  <MenuItem value={standard.className} key={standard.classID}>
                    {standard.className}
                  </MenuItem>
                ))}
            </Select>
            {errors.standard && (
              <FormHelperText error>{errors.standard.message}</FormHelperText>
            )}
          </FormControl>
        ):(
          <FormControl fullWidth margin="normal">
              <Input
                placeholder="Enter class name"
                value={customStandard}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setCustomStandard(e.target.value as string)}
                className={errors.customStandard ? `${styles.error} ${styles.formInput}` : styles.formInput}
              />
              {errors.customStandard && (
                <FormHelperText error>
                  {errors.customStandard.message}
                </FormHelperText>
              )}
            </FormControl>
        )}
          
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Subjects</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
        {!customCourse ? (
          <SubjectsCheckboxes />
        ):(
          <FormControl fullWidth margin="normal">
              <Input
                placeholder="Enter subject name"
                value={customSubject}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setCustomSubject(e.target.value as string)}
                className={errors.customSubject ? `${styles.error} ${styles.formInput}` : styles.formInput}
              />
              {errors.customSubject && (
                <FormHelperText error>
                  {errors.customSubject.message}
                </FormHelperText>
              )}
            </FormControl>
        )}
          
        </Grid>
      </Grid>

      <FormControl fullWidth margin="normal">
        <Box display="flex" justifyContent="flex-end">
          <Button
            disableElevation
            color="primary"
            size="small"
            variant="contained"
            onClick={addSubjectsToClass}
          >
            <Box component="span" display="flex" marginRight="5px">
              <AddIcon />
            </Box>
            Add
          </Button>
        </Box>
      </FormControl>

      {tutorSubjects && tutorSubjects.length > 0 && (
        <Grid container>
          <Grid item xs={12}>
            <CourseTable
              boardClassSubjectsMap={boardClassSubjectsMap}
              handleRemoveItem={removeTutorSubjects}
            />
          </Grid>
        </Grid>
      )}

      <Box display="flex" justifyContent="flex-end" marginY="20px">
        <Box className={styles.previousBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => setRedirectTo('/profile/process/1')}
          >
            <KeyboardArrowLeftIcon /> Previous
          </Button>
        </Box>
        <Box className={styles.skipBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => submitCourseInformation(Action.SKIP)}
          >
            Skip
          </Button>
        </Box>
        <Box className={styles.nextBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={() => submitCourseInformation(Action.SUBMIT)}
          >
            {submitButtonText} <KeyboardArrowRightIcon />
          </Button>
        </Box>
      </Box>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  subjects: state.academicsReducer.subjects
});

export default connect(mapStateToProps)(TutorSubjectInformation);
