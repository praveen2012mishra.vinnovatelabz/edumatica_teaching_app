import React, { FunctionComponent, useEffect, useState, Fragment } from 'react';
import { connect, useDispatch } from 'react-redux';
import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  Input,
  MenuItem,
  Select,
  Switch,
  Typography
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchSubjectsList
} from '../../../common/api/academics';
import { RootState } from '../../../../store';
import { setSubjects } from '../../../academics/store/actions';
import { Subject } from '../../../academics/contracts/subject';
import { Tutor } from '../../../common/contracts/user';
import { Course } from '../../../academics/contracts/course';
import SubjectWhite from '../../../../assets/images/course-book-white.png';
import Button from '../../../common/components/form_elements/button';
import Modal from '../../../common/components/modal';
import { NAME_PATTERN } from '../../../common/validations/patterns';
import { exceptionTracker } from '../../../common/helpers';
import CourseTable from '../course_table';
import { BoardClassSubjectsMap } from '../../../academics/contracts/board_class_subjects_map';
import { Standard } from '../../../academics/contracts/standard';
import { Board } from '../../../academics/contracts/board';
import { Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { fontOptions } from '../../../../theme';

interface Props {
  openModal: boolean;
  onClose: () => any;
  saveUser: (user: Tutor) => any;
  subjects: Subject[];
  user: Tutor;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#ffffff',
      margin: '0px'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    inputCheck: {
      '& svg': {
        fill: '#000'
      }
    },
    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    addBtn: {
      '& button': {
        padding: '8px 12px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          fontSize: fontOptions.size.medium
        }
      }
    },
    submitBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: '15px',

      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        lineHeight: '21px',
        letterSpacing: '1px',
        padding: '14px 24px'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  board: string;
  customBoard: string;
  standard: string;
  customStandard: string;
  customSubject: string;
}

const TutorSubjectsModal: FunctionComponent<Props> = ({
  openModal,
  onClose,
  saveUser,
  subjects,
  user
}) => {
  const [tutorSubjects, setTutorSubjects] = useState<Course[]>(
    user.courseDetails
  );
  const [board, setBoard] = useState('');
  const [customBoard, setCustomBoard] = useState('');
  const [standard, setStandard] = useState(''); // Standard describes Class of the Tutor.
  const [customStandard, setCustomStandard] = useState('');
  const [subjectsList, setSubjectsList] = useState<
    { name: string; checked: boolean }[]
  >([]);
  const [customSubject, setCustomSubject] = useState('');
  const [boards, setBoards] = useState<Board[]>([]);
  const [classes, setClasses] = useState<Standard[]>([]);
  const [customCourse, setCustomCourse] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const { errors, setError, clearError } = useForm<FormData>();
  const dispatch = useDispatch();

  const styles = useStyles();

  useEffect(() => {
    (async () => {
      try {
        const [boardsList, classesList] = await Promise.all([
          fetchBoardsList(),
          fetchClassesList({ boardname: board })
        ]);

        setBoards(boardsList);
        setClasses(classesList);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [board]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const setBoardAndFetchClasses = async (board: string) => {
    try {
      setBoard(board);
      setStandard('')
      setSubjectsList([])
      if (board.length > 1) {
        const classListResponse = await fetchClassesList({ boardname: board });
        setClasses(classListResponse);
      } else {
        setClasses([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const setClassAndFetchSubjects = async (standard: string) => {
    try {
      setStandard(standard);
      if (board.length > 1 && standard.length > 1) {
        const response = await fetchSubjectsList({
          boardname: board,
          classname: standard
        });
        dispatch(setSubjects(response));
        const structuredSubjectsList = response.map((subject) => ({
          name: subject.subjectName,
          checked: false
        }));
        setSubjectsList(structuredSubjectsList);
      } else {
        setSubjectsList([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const handleChangeInSubjectCheckbox = (index: number) => {
    const subjects = subjectsList.map((subject, sIndex) => {
      if (index !== sIndex) return subject;

      return { ...subject, checked: !subject.checked };
    });

    setSubjectsList(subjects);
  };

  const addSubjectsToClass = () => {
    if (!customCourse) {
      if (board.length < 1) {
        setError('board', 'Invalid Data', 'Please select board name');
        return;
      } else {
        clearError('board');
      }
      if (!NAME_PATTERN.test(board)) {
        setError('board', 'Invalid Data', 'Invalid board name');
        return;
      } else {
        clearError('board');
      }
    } else {
      const remSpace = customBoard.replace(/ /g, "")
      if (customBoard.length < 1) {
        setError(
          'customBoard',
          'Invalid Data',
          'Custom board name should not be empty'
        );
        return;
      } else if(remSpace.length < 4) {
        setError(
          'customBoard',
          'Invalid Data',
          'Custom board name should have atleast 4 Characters excluding spaces'
        );
        return;
      } else {
        clearError('customBoard');
      }
      if (!NAME_PATTERN.test(customBoard)) {
        setError('customBoard', 'Invalid Data', 'Invalid custom board name');
        return;
      } else {
        clearError('customBoard');
      }
    }

    if (!customCourse) {
      if (standard.length < 1) {
        setError('standard', 'Invalid Data', 'Please select a class');
        return;
      } else {
        clearError('standard');
      }
    } else {
      if (customStandard.length < 1) {
        setError(
          'customStandard',
          'Invalid Data',
          'Custom class name should not be empty'
        );
        return;
      } else {
        clearError('customStandard');
      }
      const remSpaceS = customStandard.replace(/ /g, "")
      if (remSpaceS.length < 3) {
        setError(
          'customStandard',
          'Invalid Data',
          'Custom class should be minimum 3 characters long excluding spaces'
        );
        return;
      } else {
        clearError('customStandard');
      }
    }

    const map: Course[] = [];

    if (!customCourse) {
      subjectsList
        .filter((subject) => subject.checked)
        .filter((subject) => {
          const subjectExists = tutorSubjects.find(
            (tutorSubject) =>
              tutorSubject.board.toLowerCase() === board.toLowerCase() &&
              tutorSubject.className.toString().toLowerCase() ===
                standard.toString().toLowerCase() &&
              tutorSubject.subject.toLowerCase() === subject.name.toLowerCase()
          );

          return subjectExists === undefined;
        })
        .forEach((subject) => {
          map.push({ board, className: standard, subject: subject.name });
        });
    } 
    else {
      if (!customSubject.length) {
        setError(
          'customSubject',
          'Invalid Data',
          'Custom subject should not be empty'
        );
        return;
      } else {
        clearError('customSubject');
      }
      const remSpaceSu = customSubject.replace(/ /g, "")
      if (remSpaceSu.length < 3) {
        setError(
          'customSubject',
          'Invalid Data',
          'Custom subject should be minimum 3 characters long excluding spaces'
        );
        return;
      } else {
        clearError('customSubject');
      }
      const subjectExists = tutorSubjects.find(
        (tutorSubject) =>
          tutorSubject.board.toLowerCase() === customBoard.toLowerCase() &&
          tutorSubject.className.toString().toLowerCase() ===
            customStandard.toString().toLowerCase() &&
          tutorSubject.subject.toLowerCase() === customSubject.toLowerCase()
      );

      if (!subjectExists) {
        map.push({
          board: customBoard,
          className: customStandard,
          subject: customSubject
        });
      }
    }

    setTutorSubjects([...tutorSubjects, ...map]);
    setBoard('');
    setCustomBoard('');
    setStandard('');
    setCustomStandard('');
    setSubjectsList([]);
    setCustomSubject('');
  };

  const removeTutorSubjects = (map: BoardClassSubjectsMap) => {
    const mapSubjects = map.subjects.map((subject) => subject.toLowerCase());

    const tutorSubjectsMap = tutorSubjects.filter(
      (subjectItem) =>
        subjectItem.board !== map.boardname ||
        subjectItem.className.toString().toLowerCase() !==
          map.classname.toString().toLowerCase() ||
        mapSubjects.indexOf(subjectItem.subject.toLowerCase()) === -1
    );

    setTutorSubjects(tutorSubjectsMap);
  };

  const submitCourseInformation = () => {
    if (tutorSubjects.length < 1) return;
    saveUser({ ...user, courseDetails: tutorSubjects });
    onClose();
    setCustomCourse(false);
  };

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < tutorSubjects.length; ++i) {
    const subject = tutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname === subject.board &&
        boardClassSubject.classname === subject.className
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: []
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject
    );
  }

  const SubjectsCheckboxes = () => (
    <FormControl fullWidth margin="normal">
      <Box display="flex" justifyContent="space-between">
        {subjectsList.map((subject, index) => (
          <FormControlLabel
            key={index}
            label={subject.name}
            control={
              <Checkbox
                className={styles.inputCheck}
                name={subject.name}
                checked={subject.checked}
                onChange={() => handleChangeInSubjectCheckbox(index)}
              />
            }
          />
        ))}
      </Box>
    </FormControl>
  );

  const handleCourseModalClose = () => {
    onClose();
    setCustomCourse(false);
    setBoard('');
    setCustomBoard('');
    setStandard('');
    setCustomStandard('');
    setSubjectsList([]);
    setCustomSubject('');
    setTutorSubjects(user.courseDetails);
  };

  return (
    <Modal
      open={openModal}
      handleClose={handleCourseModalClose}
      header={
        <Box display="flex" alignItems="center">
          <img src={SubjectWhite} alt="Course Details" />

          <Box marginLeft="15px">
            <Typography component="span" color="primary">
              <Box component="h3" className={styles.modalHeading}>
                Course Details
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <FormControlLabel
        control={
          <Switch
            checked={customCourse}
            onChange={(e) => setCustomCourse(e.target.checked)}
            color="secondary"
          />
        }
        label="Custom Course"
      />
      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Board</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <Fragment>
            {!customCourse ? (
              <FormControl fullWidth margin="normal">
                <Select
                  value={board}
                  onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                    setBoardAndFetchClasses(e.target.value as string)
                  }
                  displayEmpty
                  className={errors.board ? styles.error : ''}
                >
                  <MenuItem value="">Select a board</MenuItem>
                  {boards.length > 0 &&
                    boards.map((item) => (
                      <MenuItem value={item.boardName} key={item.boardID}>
                        {item.boardName} ({item.boardDescriptions})
                      </MenuItem>
                    ))}
                </Select>
                {errors.board && (
                  <FormHelperText error>{errors.board.message}</FormHelperText>
                )}
              </FormControl>
            ) : (
              <FormControl fullWidth margin="normal">
                <Input
                  placeholder="Enter board name"
                  value={customBoard}
                  inputProps={{ maxLength: 50 }}
                  onChange={(e) => setCustomBoard(e.target.value as string)}
                  className={errors.customBoard ? `${styles.error} ${styles.formInput}` : styles.formInput}
                />
                {errors.customBoard && (
                  <FormHelperText error>
                    {errors.customBoard.message}
                  </FormHelperText>
                )}
              </FormControl>
            )}
          </Fragment>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Classes</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <Fragment>
            {!customCourse ? (
              <FormControl fullWidth margin="normal">
                <Select
                  displayEmpty
                  value={standard}
                  onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                    setClassAndFetchSubjects(e.target.value as string)
                  }
                  className={errors.standard ? styles.error : ''}
                >
                  <MenuItem value="">Select a class</MenuItem>
                  {classes.length > 0 &&
                    classes.map((standard) => (
                      <MenuItem
                        value={standard.className}
                        key={standard.classID}
                      >
                        {standard.className}
                      </MenuItem>
                    ))}
                </Select>
                {errors.standard && (
                  <FormHelperText error>
                    {errors.standard.message}
                  </FormHelperText>
                )}
              </FormControl>
            ) : (
              <FormControl fullWidth margin="normal">
                <Input
                  placeholder="Enter class name"
                  value={customStandard}
                  inputProps={{ maxLength: 50 }}
                  onChange={(e) => setCustomStandard(e.target.value as string)}
                  className={errors.customStandard ? `${styles.error} ${styles.formInput}` : styles.formInput}
                />
                {errors.customStandard && (
                  <FormHelperText error>
                    {errors.customStandard.message}
                  </FormHelperText>
                )}
              </FormControl>
            )}
          </Fragment>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={styles.label}>Subjects</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          {!customCourse ? (
            <SubjectsCheckboxes />
          ) : (
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Enter subject name"
                value={customSubject}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setCustomSubject(e.target.value as string)}
                className={errors.customSubject ? `${styles.error} ${styles.formInput}` : styles.formInput}
              />
              {errors.customSubject && (
                <FormHelperText error>
                  {errors.customSubject.message}
                </FormHelperText>
              )}
            </FormControl>
          )}
        </Grid>
      </Grid>

      <FormControl fullWidth margin="normal">
        <Box display="flex" justifyContent="flex-end" className={styles.addBtn}>
          <Button
            disableElevation
            color="primary"
            size="small"
            variant="contained"
            onClick={addSubjectsToClass}
          >
            <AddIcon /> Add
          </Button>
        </Box>
      </FormControl>

      <Grid container>
        <Grid item xs={12}>
          <CourseTable
            boardClassSubjectsMap={boardClassSubjectsMap}
            handleRemoveItem={removeTutorSubjects}
          />
        </Grid>
      </Grid>

      <Box className={styles.submitBtn}>
        <Button
          variant="contained"
          color="primary"
          onClick={submitCourseInformation}
        >
          Save Changes
        </Button>
      </Box>
    </Modal>
  );
};

const mapStateToProps = (state: RootState) => ({
  subjects: state.academicsReducer.subjects
});

export default connect(mapStateToProps)(TutorSubjectsModal);
