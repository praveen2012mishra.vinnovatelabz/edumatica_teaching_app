import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  Input,
  TextField,
  Typography
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { AutocompleteOption } from '../../../common/contracts/autocomplete_option';
import * as dateFns from "date-fns";
import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode
} from '../../../common/api/academics';

import { Tutor } from '../../../common/contracts/user';
import ContactWhite from '../../../../assets/images/contact-white.png';
import Button from '../../../common/components/form_elements/button';
import Modal from '../../../common/components/modal';
import { exceptionTracker } from '../../../common/helpers';
import { useForm } from 'react-hook-form';
import {
  EMAIL_PATTERN,
  PIN_PATTERN
} from '../../../common/validations/patterns';
import { Redirect } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';

interface Props {
  openModal: boolean;
  onClose: () => any;
  saveUser: (user: Tutor) => any;
  user: Tutor;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#ffffff',
      margin: '0px'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    submitBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: '30px',

      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        lineHeight: '21px',
        letterSpacing: '1px',
        padding: '14px 24px'
      }
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  emailId: string;
  qualifications: string;
  schoolName: string;
  cityName: string;
  pinCode: string;
  stateName: string;
  address: string;
  otherQualifications: string;
  almaMater: string;
}

interface DateError {
  dob: string | boolean;
}

const TutorPersonalInformationModal: FunctionComponent<Props> = ({
  openModal,
  onClose,
  saveUser,
  user
}) => {
  const [dob, setDob] = useState<Date | null>(null);
  const [dateError, setDateError] = useState<DateError>({dob: false})
  const { errors, setError, clearError } = useForm<FormData>();
  const { enqueueSnackbar } = useSnackbar();
  const [enrollmentId, setEnrollmentId] = useState('');
  const [email, setEmail] = useState('');
  const [
    qualification,
    setQualification
  ] = useState<AutocompleteOption | null>();
  const [school, setSchool] = useState<AutocompleteOption | null>();
  const [pinCode, setPinCode] = useState('');
  const [cityName, setCityName] = useState('');
  const [stateName, setStateName] = useState('');
  const [address, setAddress] = useState('');
  const [otherQualifications, setOtherQualifications] = useState('');
  const [almaMater, setAlmaMater] = useState('');
  const [qualificationsList, setQualificationsList] = useState<
    AutocompleteOption[]
  >([]);
  const [schoolsList, setSchoolsList] = useState<AutocompleteOption[]>([]);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();

  useEffect(() => {
    initializePersonalInfo();
    clearError('address')
    clearError('cityName')
    clearError('emailId')
    clearError('almaMater')
    clearError('pinCode')
    clearError('stateName')
    clearError('otherQualifications')
    clearError('schoolName')
    clearError('qualifications')
    setDateError({dob: false})
    // eslint-disable-next-line
  }, [openModal]);

  const initializePersonalInfo = () => {
    setDob(user.dob ? new Date(user.dob) : null);
    setEnrollmentId(user.enrollmentId as string);
    setEmail(user.emailId);
    setQualification({
      title: user.qualifications[0],
      value: user.qualifications[0]
    });
    setSchool({ title: user.schoolName, value: user.schoolName });
    setPinCode(user.pinCode);
    setCityName(user.cityName);
    setStateName(user.stateName);
    setAddress(user.address as string);
    setOtherQualifications(user.otherQualifications as string);
    setAlmaMater(user.almaMater as string);
  };

  useEffect(() => {
    (async () => {
      try {
        const qualificationsListResponse = await fetchQualificationsList();

        const structuredQualificationsList = qualificationsListResponse.map(
          (qualification) => ({
            title: `${qualification.degree} (${qualification.subjectName})`,
            value: qualification.degree
          })
        );

        setQualificationsList([
          ...structuredQualificationsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, []);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDobChange = (date: Date | null) => {
    if(date) {
      setDateError({dob: false})
    }
    setDob(date);
  };

  // const TypeOther = () => (
  //   <FormControl fullWidth margin="normal">
  //     <Input placeholder="Others" />
  //   </FormControl>
  // );

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchool(null);
        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
          setCityName('');
          setStateName('');
          setSchoolsList([]);
        }
      }
    } else {
      setCityName('');
      setStateName('');
      setSchoolsList([]);
    }
  };

  const submitPersonalInformation = (e: React.FormEvent) => {
    e.preventDefault();
    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (!dob) {
      setDateError({dob: 'Date of Birth Required'})
      return;
    }

    if (dateFns.differenceInYears(new Date(), dob) < 16) {
      setDateError({dob: 'You Should be atleast 16 years in age'})
      return;
    }

    if (address.length < 1) {
      setError('address', 'Invalid Data', 'Invalid Address');
      return;
    } else {
      clearError('address');
    }

    if (otherQualifications.length < 1) {
      setError('otherQualifications', 'Invalid Data', 'Invalid Other Qualifications');
      return;
    } else {
      clearError('otherQualifications');
    }

    if (almaMater.length < 1) {
      setError('almaMater', 'Invalid Data', 'Invalid University');
      return;
    } else {
      clearError('almaMater');
    }

    if (qualification == null || qualification.value.length < 3) {
      setError(
        'qualifications',
        'Invalid Data',
        'qualification cannot be empty'
      );
      return;
    } else {
      clearError('qualifications');
    }

    if (school === null || (school && !school.value.length)) {
      setError('schoolName', 'Invalid Data', 'school name cannot be empty');
      return;
    } else {
      clearError('schoolName');
    }
    if (school && school.value.length < 5) {
      setError(
        'schoolName',
        'Invalid Data',
        'school name should be minimum 5 characters long'
      );
      return;
    } else {
      clearError('schoolName');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'Pin Code cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    if (!PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid pin code');
      return;
    } else {
      clearError('pinCode');
    }

    saveUser({
      ...user,
      enrollmentId: enrollmentId !== '' ? enrollmentId : undefined,
      emailId: email,
      dob: dob.toLocaleDateString("fr-CA"),
      qualifications: qualification?.value ? [qualification.value] : [],
      schoolName: school?.value ? school.value : '',
      pinCode: pinCode,
      cityName: cityName,
      stateName: stateName,
      address: address,
      otherQualifications: otherQualifications,
      almaMater: almaMater
    });

    onClose();
  };

  return (
    <Modal
      open={openModal}
      handleClose={onClose}
      header={
        <Box display="flex" alignItems="center">
          <img src={ContactWhite} alt="Personal Info" />

          <Box marginLeft="15px">
            <Typography component="span" color="primary">
              <Box component="h3" className={classes.modalHeading}>
                Personal Information
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Tutor Name</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{user.tutorName}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Enrollment ID</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Enter enrollment id"
              inputProps={{ maxLength: 50 }}
              value={enrollmentId}
              onChange={(e) => setEnrollmentId(e.target.value)}
              className={classes.formInput}
            />
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Phone Number</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{user.mobileNo}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Email Address</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Your Email Address"
              value={email}
              inputProps={{ maxLength: 100 }}
              onChange={(e) => setEmail(e.target.value)}
              className={errors.emailId ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.emailId && (
            <FormHelperText error>{errors.emailId.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Date Of Birth</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Datepickers selectedDate={dob} handleDateChange={handleDobChange} 
              maxDate={new Date()}
            />
          </FormControl>
          {dateError.dob &&
            <Typography className={classes.dateError}>{dateError.dob}</Typography>
          }
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Address</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
              <Input
                inputProps={{ maxLength: 250 }}
                placeholder="Your Address"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                className={errors.address ? `${classes.error} ${classes.formInput}` : classes.formInput}
                multiline={true}
              />
          </FormControl>
          {errors.address && (
            <FormHelperText error>{errors.address.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Qualifications</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Autocomplete
              options={qualificationsList}
              getOptionLabel={(option: AutocompleteOption) => option.title}
              autoComplete
              includeInputInList
              onChange={(e, node) => setQualification(node)}
              value={qualification}
              renderInput={(params) => (
                <TextField {...params} placeholder="Select Qualification" />
              )}
              className={errors.qualifications ? classes.error : ''}
            />
          </FormControl>
          {/* {qualification?.value === 'Other' && <TypeOther />} */}
          {errors.qualifications && (
            <FormHelperText error>
              {errors.qualifications.message}
            </FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Other Qualifications</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Other Qualifications"
                value={otherQualifications}
                onChange={(e) => setOtherQualifications(e.target.value)}
                className={errors.otherQualifications ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
          </FormControl>
          {errors.otherQualifications && (
            <FormHelperText error>{errors.otherQualifications.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>University</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
              <Input
                placeholder="Your Alma Mater"
                value={almaMater}
                onChange={(e) => setAlmaMater(e.target.value)}
                className={errors.almaMater ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
          </FormControl>
          {errors.almaMater && (
            <FormHelperText error>{errors.almaMater.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>PIN Code</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="PIN Code"
              value={pinCode}
              inputProps={{ maxLength: 6 }}
              onChange={(e) => onPinCodeChange(e.target.value)}
              className={errors.pinCode ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.pinCode && (
            <FormHelperText error>{errors.pinCode.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>City</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{cityName}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>State</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{stateName}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Schools / Others</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Autocomplete
              options={schoolsList}
              getOptionLabel={(option: AutocompleteOption) => option.title}
              autoComplete
              includeInputInList
              value={school}
              onChange={(e, node) => setSchool(node)}
              renderInput={(params) => (
                <TextField {...params} placeholder="Select School" />
              )}
              className={errors.schoolName ? classes.error : ''}
            />
          </FormControl>
          {/* {school?.value === 'Other' && <TypeOther />} */}
          {errors.schoolName && (
            <FormHelperText error>{errors.schoolName.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      {/* <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box fontWeight="bold" marginTop="5px">
              Locations
            </Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Autocomplete
              options={locationsList}
              getOptionLabel={(option: AutocompleteOption) => option.title}
              autoComplete
              includeInputInList
              value={{ title: address, value: address }}
              onInputChange={(_, value) => { fetchAddresses(value); setAddress(value); }}
              // onChange={(e, node) => setAddress(node && node.value ? node.value : '')}
              renderInput={(params) => (
                <TextField {...params} placeholder="Enter Location" />
              )}
            />
          </FormControl>
        </Grid>
      </Grid> */}

      <Box className={classes.submitBtn} marginBottom="20px">
        <Button
          variant="contained"
          color="primary"
          onClick={submitPersonalInformation}
        >
          Save Changes
        </Button>
      </Box>
    </Modal>
  );
};

export default TutorPersonalInformationModal;
