import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  MenuItem,
  Select,
  Typography
} from '@material-ui/core';
import {
  Add as AddIcon,
  RemoveCircle as RemoveCircleIcon,
  Visibility as ViewIcon
} from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { DocumentType } from '../../../common/enums/document_type';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument
} from '../../../common/api/document';
import { KycDocument } from '../../../common/contracts/kyc_document';
import { Organization } from '../../../common/contracts/user';
import Dropzone from '../../../common/components/dropzone/dropzone';
import UploadedContent from '../../../common/components/dropzone/previewers/uploadedContent'

import IdCardWhite from '../../../../assets/images/id-card-white.png';
import Button from '../../../common/components/form_elements/button';
import Modal from '../../../common/components/modal';
import { exceptionTracker } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { BusinessType } from '../../../common/enums/business_types';
import {
  AADHAAR_PATTERN,
  IFSC_PATTERN,
  PAN_PATTERN,
  ACCOUNT_NO_PATTERN
} from '../../../common/validations/patterns';
import { isGstValid } from '../../../common/helpers';
import Datepickers from '../../../common/components/datepickers';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalHeading: {
      fontWeight: 500,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#ffffff',
      margin: '0px'
    },
    label: {
      fontWeight: 500,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    viewIcon: {
      color: '#4C8BF5'
    },
    removeIcon: {
      color: '#F9BD33'
    },
    docList: {
      borderBottom: '1px dashed #EAE9E4',
      borderTop: '1px dashed #EAE9E4',
      margin: '25px 0'
    },
    iconBtn: {
      marginRight: '20px'
    },
    addBtn: {
      '& button': {
        padding: '8px 12px',
        fontWeight: 'bold',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          fontSize: fontOptions.size.medium
        }
      }
    },
    submitBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: '30px',

      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        lineHeight: '21px',
        letterSpacing: '1px',
        padding: '14px 24px'
      }
    },
    dateError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface FormData {
  pageError: string;
  businessType: string;
  businessPAN: string;
  businessName: string;
  ownerPAN: string;
  ownerName: string;
  documentNumber: string;
  bankAccount: string;
  bankIfsc: string;
  gstin: string;
}

interface DateError {
  dob: string | boolean;
}

interface Props {
  openModal: boolean;
  onClose: () => any;
  saveUser: (user: Organization) => any;
  user: Organization;
}

const OrganizationBusinessInformationModal: FunctionComponent<Props> = ({
  openModal,
  onClose,
  saveUser,
  user
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [dropzoneKey, setDropzoneKey] = useState(0);
  const [documents, setDocuments] = useState<KycDocument[]>(
    user.kycDetails || []
  );
  const [businessType, setBusinessType] = useState(user.businessType || '');
  const [dob, setDob] = useState<Date | null>(user.dob ? new Date(user.dob) : null);
  const [dateError, setDateError] = useState<DateError>({dob: false})
  const [businessPAN, setBusinessPAN] = useState(user.businessPAN || '');
  const [businessName, setBusinessName] = useState(user.businessName || '');
  const [ownerPAN, setOwnerPAN] = useState(user.ownerPAN || '');
  const [ownerName, setOwnerName] = useState(user.ownerName || '');
  const [gstin, setGstin] = useState(user.gstin || '');
  const [aadhaar, setAadhaar] = useState('');
  const [currentAadhaarNo, setCurrentAadhaarNo] = useState('');
  const [bankAccount, setBankAccount] = useState('');
  const [bankIfsc, setBankIfsc] = useState('');
  const [currentBankDetails, setCurrentBankDetails] = useState({
    accountNo: '',
    ifsc: ''
  });
  const [redirectTo, setRedirectTo] = useState('');

  React.useEffect(() => {
    
    clearError('bankAccount')
    clearError('bankIfsc')
    clearError('businessName')
    clearError('businessPAN')
    clearError('businessType')
    clearError('documentNumber')
    clearError('gstin')
    clearError('ownerName')
    clearError('ownerPAN')    
  }, [openModal])

  const classes = useStyles();

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleDobChange = (date: Date | null) => {
    if(date) {
      setDateError({dob: false})
    }
    setDob(date);
  };

  const handleFormSubmit = async () => {
    if (!businessType.length) {
      setError('businessType', 'Invalid Data', 'Business type cannot be empty');
      return;
    } else {
      clearError('businessType');
    }
    if (businessType.length < 3) {
      setError(
        'businessType',
        'Invalid Data',
        'Business type should be minimum 3 characters long'
      );
      return;
    } else {
      clearError('businessType');
    }

    if (!dob) {
      setDateError({dob: 'Date of Incorporation Required'})
      return;
    }

    if (!businessName.length) {
      setError('businessName', 'Invalid Data', 'Business name cannot be empty');
      return;
    } else {
      clearError('businessName');
    }
    const remSpace = businessName.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'businessName',
        'Invalid Data',
        'Business name should be minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('businessName');
    }

    if (!ownerName.length) {
      setError('ownerName', 'Invalid Data', 'Director name cannot be empty');
      return;
    } else {
      clearError('ownerName');
    }

    const remSpaceO = ownerName.replace(/ /g, "")
    if (remSpaceO.length < 5) {
      setError(
        'ownerName',
        'Invalid Data',
        'Director name should be minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('ownerName');
    }

    saveUser({
      ...user,
      businessType: businessType,
      dob: dob.toLocaleDateString("fr-CA"),
      businessName: businessName,
      ownerName: ownerName,
    });

    onClose();
  };

  return (
    <Modal
      open={openModal}
      handleClose={onClose}
      header={
        <Box display="flex" alignItems="center">
          <img src={IdCardWhite} alt="Other Information" />

          <Box marginLeft="15px">
            <Typography component="span" color="primary">
              <Box component="h3" className={classes.modalHeading}>
                Business Information
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <div>
        <Grid container>
          <Grid item xs={12} md={3} />
          <Grid item xs={12} md={9}>
            <Box component="h2" fontWeight={fontOptions.weight.bold}>
              Please enter correct details
            </Box>
          </Grid>
        </Grid>
        <Typography component="span" color="secondary">
          <Box component="h3" color="primary" fontWeight={fontOptions.weight.normal} margin="0">
            Mandatory
          </Box>
        </Typography>
        <Grid container>
          <Grid item xs={12} md={3}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Business Type</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={9}>
            <FormControl fullWidth margin="normal">
              <Select
                value={businessType}
                onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                  setBusinessType(e.target.value as BusinessType)
                }
                className={errors.businessType ? classes.error : ''}
              >
                <MenuItem value={BusinessType.PVT_LTD}>
                  Private Limited
                </MenuItem>
                <MenuItem value={BusinessType.PROPRIETORSHIP}>
                  Proprietorship
                </MenuItem>
                <MenuItem value={BusinessType.PARTNERSHIP}>
                  Partnership
                </MenuItem>
                <MenuItem value={BusinessType.PUBLIC_LTD}>
                  Public Limited
                </MenuItem>
                <MenuItem value={BusinessType.LLP}>LLP</MenuItem>
                <MenuItem value={BusinessType.SOCIETY}>Society</MenuItem>
                <MenuItem value={BusinessType.TRUST}>Trust</MenuItem>
                <MenuItem value={BusinessType.NGO}>NGO</MenuItem>
                <MenuItem value={BusinessType.NOT_REG}>Not Registered</MenuItem>
              </Select>
            </FormControl>
            {errors.businessType && (
              <FormHelperText error>
                {errors.businessType.message}
              </FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={3}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Date Of Incorporation</Box>
            </FormControl>
          </Grid>

          <Grid item xs={12} md={9}>
            <FormControl fullWidth margin="normal">
              <Datepickers selectedDate={dob} handleDateChange={handleDobChange} 
                maxDate={new Date()}
              />
            </FormControl>
            {dateError.dob &&
              <Typography className={classes.dateError}>{dateError.dob}</Typography>
            }
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={3}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Business Name</Box>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={9}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Business Name as per PAN"
                value={businessName}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setBusinessName(e.target.value)}
                className={errors.businessName ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.businessName && (
              <FormHelperText error>
                {errors.businessName.message}
              </FormHelperText>
            )}
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} md={3}>
            <FormControl fullWidth margin="normal">
              <Box className={classes.label}>Director Name</Box>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={9}>
            <FormControl fullWidth margin="normal">
              <Input
                placeholder="Name as per PAN"
                value={ownerName}
                inputProps={{ maxLength: 50 }}
                onChange={(e) => setOwnerName(e.target.value)}
                className={errors.ownerName ? `${classes.error} ${classes.formInput}` : classes.formInput}
              />
            </FormControl>
            {errors.ownerName && (
              <FormHelperText error>{errors.ownerName.message}</FormHelperText>
            )}
          </Grid>
        </Grid>

        <Box className={classes.submitBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={handleFormSubmit}
          >
            Save Changes
          </Button>
        </Box>
      </div>
    </Modal>
  );
};

export default OrganizationBusinessInformationModal;
