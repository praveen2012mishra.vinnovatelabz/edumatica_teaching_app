import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  MenuItem,
  Select,
  Typography
} from '@material-ui/core';
import {
  Add as AddIcon,
  RemoveCircle as RemoveCircleIcon,
  Visibility as ViewIcon
} from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { DocumentType } from '../../../common/enums/document_type';
import {
  fetchUploadUrlForKycDocument,
  uploadKycDocument,
  delteUploadKycDocument
} from '../../../common/api/document';
import { KycDocument } from '../../../common/contracts/kyc_document';
import { Tutor } from '../../../common/contracts/user';
import Dropzone from '../../../common/components/dropzone/dropzone';
import UploadedContent from '../../../common/components/dropzone/previewers/uploadedContent'
import IdCardWhite from '../../../../assets/images/id-card-white.png';
import Button from '../../../common/components/form_elements/button';
import Modal from '../../../common/components/modal';
import { exceptionTracker } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import {
  AADHAAR_PATTERN,
  PAN_PATTERN,
  IFSC_PATTERN,
  ACCOUNT_NO_PATTERN
} from '../../../common/validations/patterns';
import { fontOptions } from '../../../../theme';
import lodash from "lodash"
import { uploadFileOnUrl } from '../../../common/api/academics';

interface Props {
  openModal: boolean;
  onClose: () => any;
  saveUser: (user: Tutor) => any;
  user: Tutor;
}

interface FormData {
  document: string;
  documentNumber: string;
  bankAccount: string;
  bankIfsc: string;
  pan: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalHeading: {
      fontWeight: 500,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#ffffff',
      margin: '0px'
    },
    label: {
      fontWeight: 500,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: 'normal',
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    viewIcon: {
      color: '#4C8BF5'
    },
    removeIcon: {
      color: '#F9BD33'
    },
    docList: {
      borderBottom: '1px dashed #EAE9E4',
      borderTop: '1px dashed #EAE9E4',
      margin: '25px 0'
    },
    iconBtn: {
      marginRight: '20px'
    },
    addBtn: {
      '& button': {
        padding: '8px 12px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          fontSize: fontOptions.size.medium
        }
      }
    },
    submitBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: '30px',

      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        lineHeight: '21px',
        letterSpacing: '1px',
        padding: '14px 24px'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

const TutorOtherInformationModal: FunctionComponent<Props> = ({
  openModal,
  onClose,
  saveUser,
  user
}) => {
  const [documentType, setDocumentType] = useState(DocumentType.AADHAR);
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [dropzoneKey, setDropzoneKey] = useState(0);

  const [droppedFilesAdh, setDroppedFilesAdh] = useState<File[]>([]);
  const [droppedFilesPan, setDroppedFilesPan] = useState<File[]>([]);
  const [droppedFilesBK, setDroppedFilesBK] = useState<File[]>([]);

  const [documents, setDocuments] = useState<KycDocument[]>(
    user.kycDetails || []
  );
  const [documentNumber, setDocumentNumber] = useState('');
  const [aadhaar, setAadhaar] = useState(user.aadhaar || '');
  const [pan, setPan] = useState(user.pan || '');
  const [bankAccount, setBankAccount] = useState(user.bankAccount || '');
  const [bankIfsc, setBankIfsc] = useState(user.bankIfsc || '');
  const [redirectTo, setRedirectTo] = useState('');
  const { errors, setError, clearError } = useForm<FormData>();

  const classes = useStyles();

  React.useEffect(() => {
    clearError('bankAccount')
    clearError('bankIfsc')
    clearError('documentNumber')
   
  }, [])

  useEffect(() => {
    if(documents.find(doc => doc.kycDocType === DocumentType.AADHAR)) {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.AADHAR)
      const extfile = new File([""], documents[index].kycDocLocation, {type: documents[index].kycDocFormat})
      setDroppedFilesAdh([extfile])
    }

    if(documents.find(doc => doc.kycDocType === DocumentType.PAN)) {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.PAN)
      const extfile = new File([""], documents[index].kycDocLocation, {type: documents[index].kycDocFormat})
      setDroppedFilesPan([extfile])
    }

    if(documents.find(doc => doc.kycDocType === DocumentType.BANK)) {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.BANK)
      const extfile = new File([""], documents[index].kycDocLocation, {type: documents[index].kycDocFormat})
      setDroppedFilesBK([extfile])
    }
  }, [])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleFormSubmit = async () => {
    let clonedDocuments = [...documents];

    if (!aadhaar.length) {
      setError('documentNumber', 'Invalid Data', 'AADHAAR cannot be empty');
      return;
    } else {
      clearError('documentNumber');
    }

    if (!AADHAAR_PATTERN.test(aadhaar)) {
      setError(
        'documentNumber',
        'Invalid aadhaar number',
        'Invalid aadhaar number'
      );
      return;
    } else {
      clearError('documentNumber');
    }

    if (!pan.length) {
      setError('pan', 'Invalid Data', 'Business PAN cannot be empty');
      return;
    } else {
      clearError('pan');
    }
    
    if (!PAN_PATTERN.test(pan)) {
      setError('pan', 'Invalid Data', 'Invalid business PAN data');
      return;
    } else {
      clearError('pan');
    }
    
    if (bankAccount.length || droppedFilesBK.length) {
      if (!ACCOUNT_NO_PATTERN.test(bankAccount)) {
        setError(
          'bankAccount',
          'Invalid account number',
          'Invalid account number'
        );
        return;
      } else {
        clearError('bankAccount');
      }
    }
    
    if (bankIfsc.length || droppedFilesBK.length) {
      if (!IFSC_PATTERN.test(bankIfsc)) {
        setError('bankIfsc', 'Invalid IFSC code', 'Invalid IFSC code');
        return;
      } else {
        clearError('bankIfsc');
      }
    }

    if(droppedFilesAdh[0]) {
      if(droppedFilesAdh[0].size !== 0) {
        const file = droppedFilesAdh[0];
        const documentIndex = clonedDocuments.findIndex(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.AADHAR.toLowerCase()
        );
        if (documentIndex > -1) {
          const documentPr = clonedDocuments.find(
            (document) =>
              document.kycDocType.toLowerCase() === DocumentType.AADHAR.toLowerCase()
          );
          if(documentPr) {
            await delteUploadKycDocument(documentPr.uuid as string)
          }
          clonedDocuments.splice(documentIndex, 1);
        }
  
        const formData = new FormData();
        formData.append('document', file);
        try {
          const awsBucket = await fetchUploadUrlForKycDocument({
            fileName: file.name,
            contentType: file.type,
            contentLength: file.size
          });

          await uploadFileOnUrl(awsBucket.url, file);
  
          const newDoc = [{
            kycDocFormat: file.type,
            kycDocType: DocumentType.AADHAR,
            kycDocLocation: file.name,
            uuid: awsBucket.uuid,
            url: awsBucket.url
          }]
    
          clonedDocuments = [...clonedDocuments, ...newDoc]
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      }
    } else {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.AADHAR)
      if(index > -1) {
        const documentPr = documents.find(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.AADHAR.toLowerCase()
        );
        if(documentPr) {
          await delteUploadKycDocument(documentPr.uuid as string)
        }
        clonedDocuments = clonedDocuments.filter(doc => !(doc.kycDocType === DocumentType.AADHAR))
      }
    }

    if(droppedFilesPan[0]) {
      if(droppedFilesPan[0].size !== 0) {
        const file = droppedFilesPan[0];
        const documentIndex = clonedDocuments.findIndex(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.PAN.toLowerCase()
        );
        if (documentIndex > -1) {
          const documentPr = clonedDocuments.find(
            (document) =>
              document.kycDocType.toLowerCase() === DocumentType.PAN.toLowerCase()
          );
          if(documentPr) {
            await delteUploadKycDocument(documentPr.uuid as string)
          }
          clonedDocuments.splice(documentIndex, 1);
        }
  
        const formData = new FormData();
        formData.append('document', file);
        try {
          const awsBucket = await fetchUploadUrlForKycDocument({
            fileName: file.name,
            contentType: file.type,
            contentLength: file.size
          });
  
          await uploadFileOnUrl(awsBucket.url, file);
  
          const newDoc = [{
            kycDocFormat: file.type,
            kycDocType: DocumentType.PAN,
            kycDocLocation: file.name,
            uuid: awsBucket.uuid,
            url: awsBucket.url
          }]
    
          clonedDocuments = [...clonedDocuments, ...newDoc]
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      }
    } else {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.PAN)
      if(index > -1) {
        const documentPr = documents.find(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.PAN.toLowerCase()
        );
        if(documentPr) {
          await delteUploadKycDocument(documentPr.uuid as string)
        }
        clonedDocuments = clonedDocuments.filter(doc => !(doc.kycDocType === DocumentType.PAN))
      }
    }

    if(droppedFilesBK[0]) {
      if(droppedFilesBK[0].size !== 0) {
        const file = droppedFilesBK[0];
        const documentIndex = clonedDocuments.findIndex(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.BANK.toLowerCase()
        );
        if (documentIndex > -1) {
          const documentPr = clonedDocuments.find(
            (document) =>
              document.kycDocType.toLowerCase() === DocumentType.BANK.toLowerCase()
          );
          if(documentPr) {
            await delteUploadKycDocument(documentPr.uuid as string)
          }
          clonedDocuments.splice(documentIndex, 1);
        }
  
        const formData = new FormData();
        formData.append('document', file);
        try {
          const awsBucket = await fetchUploadUrlForKycDocument({
            fileName: file.name,
            contentType: file.type,
            contentLength: file.size
          });
  
          await uploadFileOnUrl(awsBucket.url, file);
  
          const newDoc = [{
            kycDocFormat: file.type,
            kycDocType: DocumentType.BANK,
            kycDocLocation: file.name,
            uuid: awsBucket.uuid,
            url: awsBucket.url
          }]
    
          clonedDocuments = [...clonedDocuments, ...newDoc]
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      }
    } else {
      const index = documents.findIndex(doc => doc.kycDocType === DocumentType.BANK)
      if(index > -1) {
        const documentPr = documents.find(
          (document) =>
            document.kycDocType.toLowerCase() === DocumentType.BANK.toLowerCase()
        );
        if(documentPr) {
          await delteUploadKycDocument(documentPr.uuid as string)
        }
        clonedDocuments = clonedDocuments.filter(doc => !(doc.kycDocType === DocumentType.BANK))
      }
    }

    saveUser({
      ...user,
      kycDetails: clonedDocuments,
      aadhaar,
      pan,
      bankAccount,
      bankIfsc
    });

    onClose();
  };

  return (
    <Modal
      open={openModal}
      handleClose={() => {
        setDroppedFilesAdh([])
        setDroppedFilesPan([])
        setDroppedFilesBK([])
        onClose()
      }}
      header={
        <Box display="flex" alignItems="center">
          <img src={IdCardWhite} alt="Other Information" />

          <Box marginLeft="15px">
            <Typography component="span" color="primary">
              <Box component="h3" className={classes.modalHeading}>
                Other Details
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <div>
      <Grid container style={{maxWidth: '800px'}}>
          <Grid item xs={12}>
            <Typography style={{fontSize:fontOptions.size.medium, textAlign: 'center', fontWeight: fontOptions.weight.bold}}>Please Upload Your documents</Typography>
          </Grid>
          <Grid item xs={12} style={{marginTop: '40px'}}>
            <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>Document Types</Typography>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '5px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>1. Aadhaar Card</Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <FormControl fullWidth>
                  <Input
                    value={aadhaar}
                    placeholder={`Your Aadhaar number`}
                    onChange={(e) => {
                      setAadhaar(e.target.value);
                    }}
                    inputProps={{ maxLength: 12 }}
                    className={errors.documentNumber ? `${classes.error} ${classes.formInput}` : classes.formInput}
                  />
                </FormControl>
                {errors.documentNumber && (
                  <FormHelperText error>
                    {errors.documentNumber.message}
                  </FormHelperText>
                )}
              </Grid>
              <Grid item xs={12} md={4} style={{marginTop: '5px'}}>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesAdh(files)}}
                  files={droppedFilesAdh}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesAdh}
                  onRemoveItem={(file, index) => setDroppedFilesAdh([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesAdh[0] && (droppedFilesAdh[0].size !== 0) &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesAdh[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '22px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>2. PAN</Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <FormControl fullWidth margin="normal">
                  <Input
                    placeholder="PAN of one of the directors"
                    value={pan}
                    inputProps={{ maxLength: 10 }}
                    onChange={(e) => setPan(e.target.value)}
                    className={errors.pan ? `${classes.error} ${classes.formInput}` : classes.formInput}
                  />
                </FormControl>
                {errors.pan && (
                  <FormHelperText error>{errors.pan.message}</FormHelperText>
                )}
              </Grid>
              <Grid item xs={12} md={4} style={{marginTop: '22px'}}>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesPan(files)}}
                  files={droppedFilesPan}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesPan}
                  onRemoveItem={(file, index) => setDroppedFilesPan([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesPan[0] && (droppedFilesPan[0].size !== 0) &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesPan[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container>
              <Grid item xs={12} md={4} style={{marginTop: '22px'}}>
                <Typography style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.bold}}>3. Bank Details</Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <FormControl fullWidth margin="normal">
                  <Input
                    value={bankAccount}
                    placeholder={`Your bank account number`}
                    onChange={(e) => {
                      setBankAccount(e.target.value);
                    }}
                    className={errors.bankAccount ? `${classes.error} ${classes.formInput}` : classes.formInput}
                  />
                </FormControl>
                {errors.bankAccount && (
                  <FormHelperText error>
                    {errors.bankAccount.message}
                  </FormHelperText>
                )}
              </Grid>
              <Grid item xs={12} md={4} style={{marginTop: '22px'}}>
              </Grid>
              <Grid item xs={12} md={8}>
                <FormControl fullWidth margin="normal">
                  <Input
                    value={bankIfsc}
                    placeholder={`Your bank IFSC code`}
                    onChange={(e) => {
                      setBankIfsc(e.target.value);
                    }}
                    className={errors.bankIfsc ? `${classes.error} ${classes.formInput}` : classes.formInput}
                  />
                </FormControl>
                {errors.bankIfsc && (
                  <FormHelperText error>
                    {errors.bankIfsc.message}
                  </FormHelperText>
                )}
              </Grid>
              <Grid item xs={12} md={4} style={{marginTop: '22px'}}>
                <Typography style={{color:'#666666', fontWeight: fontOptions.weight.bold, fontSize: fontOptions.size.small}}>
                  Upload Cancelled Cheque
                </Typography>
              </Grid>
              <Grid item xs={12} md={8} style={{marginTop: '20px'}}>
                <Dropzone
                  key={0}
                  onChange={(files) => {if(files.length > 0) setDroppedFilesBK(files)}}
                  files={droppedFilesBK}
                  acceptedFiles={['image/jpeg', 'image/png']}
                  maxFileSize={104857600} // 100 MB
                  contenttype='image'
                />

                <UploadedContent
                  files={droppedFilesBK}
                  onRemoveItem={(file, index) => setDroppedFilesBK([])}
                  contenttype='image'
                />
              </Grid>
              {droppedFilesBK[0] && (droppedFilesBK[0].size !== 0) &&
                <Grid item xs={12} style={{marginTop: '20px'}}>
                  <img style={{float: 'right'}} src={URL.createObjectURL(droppedFilesBK[0])} />
                </Grid>
              }
            </Grid>
          </Grid>
        </Grid>

        <Box className={classes.submitBtn}>
          <Button
            disableElevation
            variant="contained"
            color="primary"
            size="large"
            onClick={handleFormSubmit}
          >
            Save Changes
          </Button>
        </Box>
      </div>
    </Modal>
  );
};

export default TutorOtherInformationModal;
