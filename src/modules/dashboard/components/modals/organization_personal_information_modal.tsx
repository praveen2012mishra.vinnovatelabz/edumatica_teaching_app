import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  Input,
  Typography
} from '@material-ui/core';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Organization } from '../../../common/contracts/user';
import ContactWhite from '../../../../assets/images/contact-white.png'
import Button from '../../../common/components/form_elements/button';
import Modal from '../../../common/components/modal';
import { exceptionTracker } from '../../../common/helpers';
import { useForm } from 'react-hook-form';
import {
  EMAIL_PATTERN,
  ORG_NAME_PATTERN,
  PIN_PATTERN
} from '../../../common/validations/patterns';
import { fetchCitiesByPinCode } from '../../../common/api/academics';
import { Redirect } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { fontOptions } from '../../../../theme'
//import { clear } from 'node:console';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modalHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#ffffff',
      margin: '0px'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#1C2559',
      marginTop: '5px'
    },
    formInput: {
      lineHeight: '26px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    submitBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: '30px',

      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        lineHeight: '21px',
        letterSpacing: '1px',
        padding: '14px 24px'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props {
  openModal: boolean;
  onClose: () => any;
  saveUser: (user: Organization) => any;
  user: Organization;
}

interface FormData {
  emailId: string;
  organizationName: string;
  address: string;
  cityName: string;
  pinCode: string;
  stateName: string;
}

const OrganizationPersonalInformationModal: FunctionComponent<Props> = ({
  openModal,
  onClose,
  saveUser,
  user
}) => {
  const { errors, setError, clearError } = useForm<FormData>();
  const { enqueueSnackbar } = useSnackbar();
  const [email, setEmail] = useState('');
  const [organizationName, setOrganizationName] = useState('');
  const [address, setAddress] = useState('');
  const [pinCode, setPinCode] = useState('');
  const [cityName, setCityName] = useState('');
  const [stateName, setStateName] = useState('');
  const [redirectTo, setRedirectTo] = useState('');
  const classes = useStyles();

  useEffect(() => {
    initializePersonalInfo();
    clearError('address')
    clearError('cityName')
    clearError('emailId')
    clearError('organizationName')
    clearError('pinCode')
    clearError('stateName')
    // eslint-disable-next-line
  }, [openModal]);

  const initializePersonalInfo = () => {
    setEmail(user.emailId);
    setOrganizationName(user.organizationName);
    setAddress(user.address);
    setPinCode(user.pinCode);
    setCityName(user.city);
    setStateName(user.stateName);
    //setOtherQualification(user.otherQualification);
    //setQualifications(user.qualifications);
    //setUniversity(user.university);
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          console.log(error);
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
        }
      }
    } else {
      setCityName('');
      setStateName('');
    }
  };

  const submitPersonalInformation = (e: React.FormEvent) => {
    e.preventDefault();
    const remSpace = organizationName.replace(/ /g, "")
    if (remSpace.length <= 4) {
      setError(
        'organizationName',
        'Invalid Data',
        'Institute name minimum 5 characters long excluding spaces'
      );
      return;
    } else {
      clearError('organizationName');
    }

    if (!ORG_NAME_PATTERN.test(organizationName)) {
      setError('organizationName', 'Invalid Data', 'Invalid institute name');
      return;
    } else {
      clearError('organizationName');
    }

    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (address == null || address.length < 5) {
      setError('address', 'Invalid Data', 'Address cannot be empty');
      return;
    } else {
      clearError('address');
    }

    if (pinCode == null || !PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid Data');
      return;
    } else {
      clearError('pinCode');
    }

    saveUser({
      ...user,
      organizationName: organizationName,
      emailId: email,
      address: address,
      city: cityName,
      pinCode: pinCode,
      stateName: stateName,
      courseDetails: user.courseDetails,
      //qualifications:qualifications,
      //otherQualification:otherQualification,
      //university:university
    });
    onClose();
  };

  return (
    <Modal
      open={openModal}
      handleClose={onClose}
      header={
        <Box display="flex" alignItems="center">
          <img src={ContactWhite} alt="Personal Info" />

          <Box marginLeft="15px">
            <Typography component="span" color="primary">
              <Box component="h3" className={classes.modalHeading}>
                Personal Information
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Phone Number</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{user.mobileNo}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Institute Name</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Your Name"
              value={organizationName}
              inputProps={{ maxLength: 50 }}
              onChange={(e) => setOrganizationName(e.target.value)}
              className={errors.organizationName ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.organizationName && (
            <FormHelperText error>
              {errors.organizationName.message}
            </FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Email Address</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="Your Email Address"
              value={email}
              inputProps={{ maxLength: 100 }}
              onChange={(e) => setEmail(e.target.value)}
              className={errors.emailId ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.emailId && (
            <FormHelperText error>{errors.emailId.message}</FormHelperText>
          )}
        </Grid>
      </Grid>      

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>Address</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              inputProps={{ maxLength: 250 }}
              placeholder="Your Address"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
              className={errors.address ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.address && (
            <FormHelperText error>{errors.address.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>PIN Code</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Input
              placeholder="PIN Code"
              value={pinCode}
              inputProps={{ maxLength: 6 }}
              onChange={(e) => onPinCodeChange(e.target.value)}
              className={errors.pinCode ? `${classes.error} ${classes.formInput}` : classes.formInput}
            />
          </FormControl>
          {errors.pinCode && (
            <FormHelperText error>{errors.pinCode.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>City</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{cityName}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth margin="normal">
            <Box className={classes.label}>State</Box>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={8}>
          <FormControl fullWidth margin="normal">
            <Box>{stateName}</Box>
          </FormControl>
        </Grid>
      </Grid>

      <Box className={classes.submitBtn} marginBottom="20px">
        <Button
          variant="contained"
          color="primary"
          onClick={submitPersonalInformation}
        >
          Save Changes
        </Button>
      </Box>
    </Modal>
  );
};

export default OrganizationPersonalInformationModal;
