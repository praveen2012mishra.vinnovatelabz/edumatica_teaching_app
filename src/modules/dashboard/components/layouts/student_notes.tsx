import React, { FunctionComponent, useEffect, useState } from 'react';
import FileDownload from 'js-file-download';
import {
  Box,
  Button as MuButton,
  Container,
  Grid,
  IconButton,
  Typography,
  CardContent
} from '@material-ui/core';
import {
  ExpandLess as ChevronUpIcon,
  ExpandMore as ChevronDownIcon
} from '@material-ui/icons';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import {
  getStudentChapters,
  fetchStudentContentDownloadUrl,
  downloadFile
} from '../../../common/api/academics';
import { Student } from '../../../common/contracts/user';
import { BatchChapter } from '../../../common/contracts/academic';
import Button from '../../../common/components/form_elements/button';
import MiniDrawer from '../../../common/components/sidedrawer';
import { exceptionTracker } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';

const styles = createStyles({
  boyWavingHard: {
    maxHeight: '145px',
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  heading: {
    fontSize: fontOptions.size.large,
    fontFamily:fontOptions.family,
    fontWeight: fontOptions.weight.bold,
    color: '#1C2559',
    marginLeft: '15px'
  },
});

interface ChapterBlockProps {
  item: BatchChapter;
}

const ChapterBlock: FunctionComponent<ChapterBlockProps> = ({ item }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const downloadContent = async (index: number) => {
    try {
      const content = item.chapter.contents[index];
      const serverBucket = await fetchStudentContentDownloadUrl({
        boardname: item.chapter.boardname,
        chaptername: item.chapter.chaptername,
        classname: item.chapter.classname,
        subjectname: item.chapter.subjectname,
        uuid: content.uuid,
        tutorId: content.ownerId
      });

      const file = await downloadFile(serverBucket.url);
      FileDownload(file, content.contentname);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <Box bgcolor="white" marginTop="10px" padding="30px">
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" alignItems="center">
          <Box>
            <Typography variant="subtitle2">
              {item.chapter.subjectname}
            </Typography>
            <Typography variant="subtitle1">
              <Box fontWeight="fontWeightBold">{item.chapter.chaptername}</Box>
            </Typography>
          </Box>
        </Box>
        <Box display="flex" alignItems="center">
          <Box component="span" marginLeft="10px">
            <IconButton size="small" onClick={() => setIsExpanded(!isExpanded)}>
              {isExpanded ? <ChevronUpIcon /> : <ChevronDownIcon />}
            </IconButton>
          </Box>
        </Box>
      </Box>

      <Box
        bgcolor="#f2d795"
        marginTop="15px"
        style={{ display: isExpanded ? 'block' : 'none' }}
      >
        <Box padding="15px" display="flex" alignItems="center">
          <Box marginLeft="15px">
            <Box>
              {item.chapter.contents.map((content, index) => (
                <Grid>
                  <Box>{content.contentname}</Box>
                  <Button
                    color="primary"
                    size="small"
                    onClick={() => downloadContent(index)}
                  >
                    Download
                  </Button>
                </Grid>
              ))}
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

interface Props extends WithStyles<typeof styles> {
  profile: Student;
}

const StudentNotes: FunctionComponent<Props> = ({ classes, profile }) => {
  const [subject, setSubject] = useState('');
  const [chapters, setChapters] = useState<BatchChapter[]>([]);
  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const chapterList = await getStudentChapters();
        console.log(chapterList)
        setChapters(chapterList);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>

      <Box marginY="25px">
        <Container>
          <Grid container>
            <Grid item  xs={12} md={8} lg={12}>
              <Box paddingX="10px">
                <Typography className={classes.heading}>Notes</Typography>

                <Grid style={{ marginBottom: '2em' }}>
                  <CardContent >
                    <MuButton
                      color={'primary'}
                      size="large"
                      variant={subject==='' ? 'contained' : 'text'}
                      onClick={() => setSubject('')}
                    >
                      All
                    </MuButton>
                  </CardContent>
                </Grid>

                <Box>
                  {chapters.map((item, index) => (
                    <ChapterBlock key={index} item={item} />
                  ))}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      </MiniDrawer>
    </div>
  );
};

export default withStyles(styles)(StudentNotes);
