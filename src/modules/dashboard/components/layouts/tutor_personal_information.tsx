import React, { FunctionComponent, useState } from 'react';
import { Box, Grid, IconButton, Input, TextField, Tooltip, Typography } from '@material-ui/core';
import {
  AccountCircle as AccountCircleIcon,
  Create as CreateIcon,
  Email as EmailIcon,
  School as SchoolIcon,
  Subscriptions as SubscriptionIcon,
  LocationCity as LocationIcon,
  Smartphone as SmartPhoneIcon
} from '@material-ui/icons';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Tutor } from '../../../common/contracts/user';
import { updateTutor } from '../../../common/api/profile';
import ProfileEditIcon from '../../../../assets/svgs/profile-edit.svg';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../tutor_layout';
import TutorPersonalInformationModal from '../modals/tutor_personal_information_modal';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useSnackbar } from 'notistack';
import { registerReferralCode } from '../../../payments/helper/api';

interface Props {
  profile: Tutor;
  profileUpdated: (user: Tutor) => any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#010101'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    blueBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #659BF6',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#659BF6',
        padding: '7px 15px'
      }
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      color: '#606A7B',
      marginBottom: '12px',
      display:'flex',
      alignItems:'center'
    },
    inputValueContainer: {
      marginBottom: '20px',
      display: 'flex',
      alignItems: 'center'
    },
    inputValue: {
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#151522',
      marginLeft: '30px'
    },
    infoHead: {
      marginTop: '1px',
      marginLeft: '5px' 
    }
  })
);

const TutorPersonalInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const [referral, setReferral] = useState('')

  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Tutor Personal Info', 'Landed Tutor Personal Info');
  
  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const savePersonalInformation = async (data: Tutor) => {
    const user = Object.assign({}, profile, data);
     
    try {
      profileUpdated(user);
      await updateTutor(user);
      eventTracker(GAEVENTCAT.profile, 'Tutor Personal Info', 'Completed Tutor Personal Info Edit');
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  const handleReferral = () => {
    if(referral === '') {
      enqueueSnackbar('Please enter a referral code', {
        variant: 'error'
      });
    }else {
      registerReferralCode("Tutor", referral).then(response => {
        if(response.code === 0) {
          enqueueSnackbar('Successfully registered referral code', {
            variant: 'success'
          });
        }
      })
    }
  }

  return (
    <Layout profile={profile}>
      <TutorPersonalInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={savePersonalInformation}
        user={profile}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={8}>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={ProfileEditIcon} alt="Personal Info" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box className={classes.profileheading}>
                      Personal Information
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View and Edit Your Personal &amp; Contact Details
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <Box className={classes.blueBtn}>
                <Button variant="outlined" onClick={() => {
                    eventTracker(GAEVENTCAT.profile, 'Tutor Personal Info', 'Start Tutor Personal Info Edit');
                    setOpenModal(true);
                  }}
                >
                  <Box display="flex" alignItems="center">
                    Edit
                    <Box component="span" display="flex" marginLeft="10px">
                      <CreateIcon fontSize="small" />
                    </Box>
                  </Box>
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.tutorName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Enrollment Id</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.enrollmentId ? profile.enrollmentId : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <EmailIcon color="primary" />
                <Typography className={classes.infoHead}>Email Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.emailId}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SmartPhoneIcon color="primary" />
                <Typography className={classes.infoHead}>Phone Number</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.mobileNo}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <CalendarTodayIcon color="primary" />
                <Typography className={classes.infoHead}>Date of Birth</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.dob && profile.dob.length > 0
                    ? new Date(profile.dob).toDateString()
                    : '-'
                  }
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.address ? profile.address : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SubscriptionIcon color="primary" />
                <Typography className={classes.infoHead}>Package</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.package && profile.package.name}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SchoolIcon color="primary" />
                <Typography className={classes.infoHead}>Qualifications</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.qualifications && profile.qualifications.join(', ')}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SchoolIcon color="primary" />
                <Typography className={classes.infoHead}>Other Qualifications</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.otherQualifications ? profile.otherQualifications : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountBalanceIcon color="primary" />
                <Typography className={classes.infoHead}>University / College</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.almaMater ? profile.almaMater : '-'}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>City</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.cityName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>PIN Code</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.pinCode}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>State</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.stateName}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              {!profile.userReferralCode ? '' :
                <>
                <Box className={classes.label}>
                  <CardMembershipIcon color="primary" />
                  <Typography className={classes.infoHead}>Referral Code</Typography>
                </Box>
                <Box className={classes.inputValueContainer}>
                  <Box style={{display:"flex", alignItems:"center", columnGap:"8px"}} className={classes.inputValue}>
                    <Input defaultValue={profile.userReferralCode} disabled inputProps={{ 'aria-label': 'description' }} />
                    <CopyToClipboard text={profile.userReferralCode}>
                      <Tooltip title="Copy">
                        <IconButton>
                          <FileCopyIcon/>
                        </IconButton>
                      </Tooltip>
                    </CopyToClipboard>
                  </Box>
                </Box>
                </>
              }
            </Grid>
            <Grid item xs={12} md={6}>
              {
                profile.isReferralCodeUsed ? '' :
                <>
                  <Box style={{display:"flex", alignItems:"center", columnGap:"8px"}} className={classes.inputValue}>
                    <TextField
                      size="small"
                      onChange={(e)=> {setReferral(e.target.value)}}
                      id="outlined-secondary"
                      label="Use a referral code"
                      variant="outlined"
                      color="secondary"
                    />
                      <Tooltip title="Use">
                        <IconButton onClick={handleReferral}>
                          <AddCircleIcon/>
                        </IconButton>
                      </Tooltip>
                  </Box>
                </>
              }
            </Grid>
          </Grid>
        </Box>

      </Box>
    </Layout>
  );
};

export default TutorPersonalInformation;
