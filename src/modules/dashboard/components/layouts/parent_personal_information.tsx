import React, { FunctionComponent, useState } from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import {
  AccountCircle as AccountCircleIcon,
  Create as CreateIcon,
  Email as EmailIcon,
  School as SchoolIcon,
  Smartphone as SmartPhoneIcon,
  LocationCity as LocationIcon
} from '@material-ui/icons';
import { connect } from 'react-redux';
import { Children } from '../../../common/contracts/user';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Parent } from '../../../common/contracts/user';
import { updateParent } from '../../../common/api/profile';
import ProfileEditIcon from '../../../../assets/svgs/profile-edit.svg';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../parent_layout';
import ParentPersonalInformationModal from '../modals/parent_personal_information_modal';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';
import { RootState } from '../../../../store';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#010101'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    yellowBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #4285F4',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#4285F4',
        padding: '7px 15px'
      }
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      color: '#606A7B',
      marginBottom: '12px',
      display:'flex',
      alignItems:'center'
    },
    inputValueContainer: {
      marginBottom: '20px',
      display: 'flex',
      alignItems: 'center'
    },
    inputValue: {
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#151522',
      marginLeft: '30px'
    },
    infoHead: {
      marginTop: '1px',
      marginLeft: '5px' 
    }
  })
);

interface Props {
  profile: Parent;
  profileUpdated: (user: Parent) => any;
  parentChildren: Children;
}

const ParentPersonalInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated,
  parentChildren
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Parent Personal Info', 'Landed Parent Personal Info');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const savePersonalInformation = async (data: Parent) => {
    const user = Object.assign({}, profile, data);

    try {
      profileUpdated(user);
      await updateParent(user);
      eventTracker(GAEVENTCAT.profile, 'Parent Personal Info', 'Completed Parent Personal Info Edit');
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  const selectedChild = parentChildren.children.find(child => child._id === parentChildren.current)

  return (
    <Layout profile={profile}>
      <ParentPersonalInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={savePersonalInformation}
        user={profile}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={8}>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={ProfileEditIcon} alt="Personal Info" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box className={classes.profileheading}>
                      Personal Information
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View &amp; Edit Your Personal &amp; Contact Details
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <Box className={classes.yellowBtn}>
                <Button variant="outlined" onClick={() => {
                    eventTracker(GAEVENTCAT.profile, 'Parent Personal Info', 'Start Parent Personal Info Edit');
                    setOpenModal(true);
                  }}
                >
                  <Box display="flex" alignItems="center">
                    Edit
                    <Box component="span" display="flex" marginLeft="10px">
                      <CreateIcon fontSize="small" />
                    </Box>
                  </Box>
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.parentName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <EmailIcon color="primary" />
                <Typography className={classes.infoHead}>Email Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.emailId}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SmartPhoneIcon color="primary" />
                <Typography className={classes.infoHead}>Phone Number</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.mobileNo}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12}>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={ProfileEditIcon} alt="Personal Info" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box className={classes.profileheading}>
                      Selected Child Information
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View Your Child's Details
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.studentName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Enrollment Id</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.enrollmentId ? selectedChild?.enrollmentId : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <EmailIcon color="primary" />
                <Typography className={classes.infoHead}>Email Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.emailId}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SmartPhoneIcon color="primary" />
                <Typography className={classes.infoHead}>Phone Number</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.mobileNo}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountBalanceIcon color="primary" />
                <Typography className={classes.infoHead}>Board</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.boardName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountBalanceIcon color="primary" />
                <Typography className={classes.infoHead}>Class</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.className}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SchoolIcon color="primary" />
                <Typography className={classes.infoHead}>School</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.schoolName}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>City</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.cityName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>State</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.stateName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>PIN Code</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {selectedChild?.pinCode}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Layout>
  );
};

const mapStateToProps = (state: RootState) => ({
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(ParentPersonalInformation);
