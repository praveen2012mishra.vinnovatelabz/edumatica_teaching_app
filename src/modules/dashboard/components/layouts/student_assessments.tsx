import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  Box,
  Button as MuButton,
  Container,
  Grid,
  Typography,
  CardContent
} from '@material-ui/core';

import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import { Student } from '../../../common/contracts/user';
import MiniDrawer from '../../../common/components/sidedrawer';
import { exceptionTracker } from '../../../common/helpers';
import { Redirect, useHistory } from 'react-router-dom';
import { getAttemptAssessments } from '../../../student_assessment/helper/api';
import { OngoingAssessment } from '../../../student_assessment/contracts/assessment_interface';

import ScienceIco from '../../../../assets/images/science.png'
import MathsIco from '../../../../assets/images/math.png'
import OthersIco from '../../../../assets/images/english.png'
import { useSnackbar } from 'notistack';
import { fontOptions } from '../../../../theme';
import NoContent from '../../../../assets/images/noContent.png'

interface RowData {
  id: string;
  status: string;
  assessmentname: string;
  subject: string;
  startTime: Date;
  endTime: Date;
  solutionTime: Date;
  button: string;
  ownerId:string;
  totalMarks:number;
}
const styles = createStyles({
  boyWavingHard: {
    maxHeight: '145px',
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  heading: {
    fontSize: fontOptions.size.large,
    fontFamily:fontOptions.family,
    fontWeight: fontOptions.weight.bold,
    color: '#1C2559',
    //marginLeft: '15px'
  },
  itemhead: {
    fontSize: fontOptions.size.medium,
    fontFamily:fontOptions.family,
    fontWeight: fontOptions.weight.bold,
    color: '#1C2559',
    marginLeft: '10px'
  },
  itemsubhead: {
    fontSize: fontOptions.size.small,
    fontFamily:fontOptions.family,
    fontWeight: fontOptions.weight.normal,
    color: '#1C2559',
    margin: '10px'
  },
  root_container_tutor: {
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  rootTutor: {
    width: '36.00em',
    minHeight: '13.25em',
    marginBottom: '4em',
    padding: '.5em',
  },
});

function onlyUnique(value: string, index: number, self: string[]) {
  return self.indexOf(value) === index;
}

interface Props extends WithStyles<typeof styles> {
  profile: Student;
}

const StudentAssessments: FunctionComponent<Props> = ({ classes, profile }) => {
  //const [subject, setSubject] = useState('');
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [attemptAssessments, setAttemptAssessments] = useState<
    OngoingAssessment[]
  >([]);
  const [subjects, setSubjects] = useState<string[]>([]);
  const [selectedSubject, setSelectedSubject] = useState<string>('All');
  const [rows, setRows] = useState<RowData[]>([]);
  const [submittedAssessment, setSubmittedAssessment] = useState<string[]>([]);

  interface DateTimeFormatOptions {
    localeMatcher?: 'lookup' | 'best fit';
    weekday?: 'long' | 'short' | 'narrow';
    era?: 'long' | 'short' | 'narrow';
    year?: 'numeric' | '2-digit';
    month?: 'numeric' | '2-digit' | 'long' | 'short' | 'narrow';
    day?: 'numeric' | '2-digit';
    hour?: 'numeric' | '2-digit';
    minute?: 'numeric' | '2-digit';
    second?: 'numeric' | '2-digit';
    timeZoneName?: 'long' | 'short';
    formatMatcher?: 'basic' | 'best fit';
    hour12?: boolean;
    timeZone?: string; // this is more complicated than the others, not sure what I expect here
  }

  const dateOptions: DateTimeFormatOptions = {
    day: '2-digit',
    month: 'short',
    hour: '2-digit',
    minute: '2-digit'
  };

  useEffect(() => {
    if (attemptAssessments.length > 0) {
      setSubjects(
        attemptAssessments
          .map((el) => el.assessment.subjectname)
          .filter(onlyUnique)
      );
    }
  }, [attemptAssessments]);

  const getAllAttemptAssessments = async () => {
    try {
      const response = await getAttemptAssessments();
      setAttemptAssessments(response);
      setSubmittedAssessment(
        response.filter((val) => val.isSubmitted).map((val) => val._id)
      );
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        setRedirectTo('/login');
      }
    }
  };
  useEffect(() => {
    getAllAttemptAssessments();
  }, []);


  useEffect(() => {
    setRows(
      attemptAssessments
        .filter((el) =>
          selectedSubject === 'All'
            ? true
            : el.assessment.subjectname === selectedSubject
        )
        .map((data, index) => {
          
          return {
            id: (index + 1).toString(),
            status: data.isSubmitted
              ? 'Submitted'
              : data.isStarted
              ? 'Ongoing'
              : 'Not Attempted',
            assessmentname: data.assessment.assessmentname,
            totalMarks:data.assessment.totalMarks,
            ownerId:data.ownerId,
            subject: data.assessment.subjectname,
            startTime: new Date(data.startDate),
            endTime: new Date(data.endDate),
            solutionTime: new Date(data.solutionTime),
            button: data._id
          };
        })
    );
  }, [attemptAssessments, selectedSubject]);

  const handleAssessmentStart = (id: string) => {
    sessionStorage.setItem("selectedStudentAssessmentId",id)
    // console.log(id)
    // console.log(attemptAssessments)
    if (
      new Date(attemptAssessments.filter((el) => el._id === id)[0].startDate).getTime() - 5*60*1000 <
        new Date().getTime() &&
      new Date(attemptAssessments.filter((el) => el._id === id)[0].endDate) >
        new Date()
    ) {
      if (attemptAssessments.filter((el) => el._id === id)[0].isSubmitted) {
        enqueueSnackbar('Assessment already submitted', { variant: 'info' });
      } else {
        enqueueSnackbar('Starting Assessment', { variant: 'success' });
        history.push(
          '/students' +
            '/student_assessement_instruction'
        );
        // history.push(
        //   '/students' +
        //     '/student_assessement_test?attemptassessmentId=' +
        //     id
        // );
        
      }
    } else {
      enqueueSnackbar(
        "Couldn't initiate Assessment. Please attempt in Time window specified",
        { variant: 'warning' }
      );
      if(new Date(attemptAssessments.filter((el) => el._id === id)[0].startDate).getTime() - 5*60*1000 >
      new Date().getTime()){
        enqueueSnackbar('Instructions window opens 5 mins before exam', {variant:'info'})
      }
    }
  };

  const handleSolutionStart = (id: string) => {
    if (
      new Date(
        attemptAssessments.filter((el) => el._id === id)[0].solutionTime
      ) < new Date()
    ) {
      enqueueSnackbar('Opening Solutions', { variant: 'success' });
      history.push(
        '/students' +
          '/student_assessement_test?attemptassessmentId=' +
          id
      );
    } else {
      enqueueSnackbar(
        'Solutions will be revealed at ' +
          new Date(
            attemptAssessments.filter((el) => el._id === id)[0].solutionTime
          ).toLocaleString(),
        { variant: 'warning' }
      );
    }
  };

  const subjectColor = [
    {name: 'Mathematics', color: '#FFB2B2', icon: [<img src={MathsIco} alt="maths"/>]},
    {name: 'Science', color: '#7EFBA', icon: [<img src={ScienceIco} alt="science"/>]}
  ]

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>

      <Box marginY="25px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={8} lg={12}>
              <Box paddingX="10px">
                <Typography className={classes.heading}>Assessments</Typography>

                <Grid style={{ marginBottom: '2em' }}>
                  <CardContent >
                    <MuButton
                      color={'primary'}
                      size="large"
                      variant={selectedSubject==='All' ? 'contained' : 'text'}
                      onClick={() => setSelectedSubject('All')}
                    >
                      All
                    </MuButton>
                    {subjects.map((el) => {
                      return (
                        <MuButton
                          color={'primary'}
                          size="large"
                          variant={selectedSubject===el ? 'contained' : 'text'}
                          onClick={() => setSelectedSubject(el)}
                        >
                          {el}
                        </MuButton>
                      );
                    })}
                  </CardContent>
                </Grid>
                
                {(rows.length === 0) &&
                  <Box style={{textAlign:"center"}}>
                    <img src={NoContent} alt=""  style={{height: '200px', width: '200px'}} />
                    <Typography style={{color: '#666666'}}>No Assessment</Typography>
                  </Box>
                }
                <Grid container className={classes.root_container_tutor} spacing={2}>
                  {rows.map(row => {
                    return (
                      <Grid item xs={6} lg={6} md={6} >
                        <Box
                          bgcolor={
                            subjectColor.some(item => item.name === row.subject) ?
                            subjectColor.find(item => item.name === row.subject)?.color :
                            '#80E0FF'
                          }
                          border="1px solid rgba(0, 0, 0, 0.08)"
                          borderRadius="5px"
                          color="#666666"
                          marginTop="10px"
                        >
                          <Box padding="15px" borderBottom="1px solid rgba(0, 0, 0, 0.1)">
                            <Typography className={classes.itemhead}>{row.subject}</Typography>
                            <Typography className={classes.itemhead}>{row.assessmentname}</Typography>
                            <Typography className={classes.itemsubhead}>
                              Starts At: {row.startTime.toLocaleString()}
                            </Typography>
                            <Typography className={classes.itemsubhead}>
                              Last Entry At: {row.endTime.toLocaleString()}
                            </Typography>
                            <Box display="flex" justifyContent="space-between" marginY="15px" marginLeft='10px'>
                              {subjectColor.some(item => item.name === row.subject) &&
                                subjectColor.find(item => item.name === row.subject)?.icon[0]
                              }
                              {!subjectColor.some(item => item.name === row.subject) &&
                                <img src={OthersIco} alt="others"/>
                              } 
                              <Box 
                              fontSize= {fontOptions.size.small}
                              fontWeight= {fontOptions.weight.bold}
                              alignSelf='flex-end'
                              color='#000'
                              >
                              {row.ownerId}</Box> 
                              <Box>
                              <Typography className={classes.itemsubhead}>
                              Full marks : {row.totalMarks}
                            </Typography>
                              <MuButton
                                disableElevation
                                variant="contained"
                                color="primary"
                                size="small"
                                style={{height:'fit-content',width: '-webkit-fill-available'}}
                                onClick={() => {
                                  console.log(row)
                                  
                                  if (submittedAssessment.includes(row.button as string)) {
                                    handleSolutionStart(row.button as string);
                                  } else {
                                    handleAssessmentStart(row.button as string);
                                  }
                                }}
                              >
                                {submittedAssessment.includes(row.button as string)
                                  ? 'View Solution'
                                  : 'Attempt'}
                              </MuButton>
                              </Box>
                              
                            </Box>
                          </Box>
                        </Box>
                      </Grid>
                    )
                  })}
                </Grid>

              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      </MiniDrawer>
    </div>
  );
};

export default withStyles(styles)(StudentAssessments);