import React, { FunctionComponent, useState } from 'react';
import { Box, Grid, Typography, IconButton, Tooltip } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Create as CreateIcon } from '@material-ui/icons';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import PaymentIcon from '@material-ui/icons/Payment';
import PersonIcon from '@material-ui/icons/Person';
import { Organization } from '../../../common/contracts/user';
import { updateOrganization } from '../../../common/api/organization';
import IdCard from '../../../../assets/images/id-card-blue.png';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../organization_layout';
import OrganizationBusinessInformationModal from '../modals/organization_business_information_modal';
import OrganizationDocumentModal from '../modals/organization_document_model'
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';
import GetAppIcon from '@material-ui/icons/GetApp';
import { KycDocument } from '../../../common/contracts/kyc_document';
import { fetchDownloadUrlForKycDocument } from '../../../common/api/document';
import { DocumentType } from '../../../common/enums/document_type';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    badge: {
      background: theme.palette.primary.main,
      borderRadius: '9999px',
      color: '#FFF',
      marginLeft: '5px',
      padding: '5px 10px'
    },
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#212121'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    blueBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #659BF6',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#659BF6',
        padding: '7px 15px'
      }
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      color: '#606A7B',
      marginBottom: '12px',
      display:'flex',
      alignItems:'center'
    },
    inputValueContainer: {
      marginBottom: '20px',
      display: 'flex',
      alignItems: 'center'
    },
    inputValue: {
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#151522',
      marginLeft: '30px'
    },
    infoHead: {
      marginTop: '1px',
      marginLeft: '5px' 
    },
    docList: {
      fontWeight: fontOptions.weight.normal,
      textTransform: 'uppercase',
      fontSize: fontOptions.size.small,
      lineHeight: '24px',
      color: 'rgba(0, 0, 0, 0.87)',
      padding: '12px 30px',
      borderBottom: '1px solid rgb(239, 239, 239)'
    }
  })
);

interface BusinessInformationProps {
  profile: Organization;
}

const BusinessInformation: FunctionComponent<BusinessInformationProps> = ({
  profile
}) => {
  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Organization Business Info', 'Landed Organization Business Info');

  const downloadDocs = async (doc: KycDocument, index: number) => {
    const awsBucket = await fetchDownloadUrlForKycDocument(doc.uuid as string)
    setTimeout(() => {
      const response = {
        file: awsBucket.url,
      };
      window.open(response.file);
    }, 100);
  }

  const displayKycDocType = (docType: string) => {
    const {AADHAR, BUSINESS_PAN, OWNER_PAN} = DocumentType
    switch (docType) {
      case AADHAR: return 'Aadhaar'

      case BUSINESS_PAN: return 'Business Pan'

      case OWNER_PAN: return 'Owner Pan'

      default: return docType
    }
  }

  if (profile.kycDetails && profile.kycDetails.length > 0) {
    return (
      <Box>
        <Box padding="20px">
          <Typography variant="subtitle2" className={classes.label}>
            Documents{' '}
            <span className={classes.badge}>{profile.kycDetails.length}</span>
          </Typography>

          <Box
            marginTop="20px"
            border="1px solid #EFEFEF"
            boxShadow="0px 1px 2px rgba(0, 0, 0, 0.25)"
            maxWidth="400px"
          >
            {profile.kycDetails?.map((document, index) => (
              <Box key={index} className={classes.docList}>
                <Grid>
                  <Grid>
                    {displayKycDocType(document.kycDocType)}
                    <Tooltip key={index} title="Download">
                      <IconButton
                        style={{float: 'right'}}
                        onClick={() => {
                          downloadDocs(document, index)
                        }}
                        size="small"
                      >
                        <GetAppIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    );
  }

  return (
    <Box marginTop="10px">
      {/* <Typography>No documents added.</Typography> */}
    </Box>
  );
};

interface Props {
  profile: Organization;
  profileUpdated: (user: Organization) => any;
}

const OrganizationBusinessInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [openDModal, setOpenDModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveBusinessInformation = async (data: Organization) => {
    const user = Object.assign({}, profile, data);

    try {
      profileUpdated(user);
      await updateOrganization(user);
      eventTracker(GAEVENTCAT.profile, 'Organization Business Info', 'Completed Organization Business Info Edit');
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  return (
    <Layout profile={profile}>
      <OrganizationBusinessInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={saveBusinessInformation}
        user={profile as Organization}
      />
      <OrganizationDocumentModal
        openModal={openDModal}
        onClose={() => setOpenDModal(false)}
        saveUser={saveBusinessInformation}
        user={profile as Organization}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={8}>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={IdCard} alt="Business Info" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box className={classes.profileheading}>
                      Business Information
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View and Edit Your Business Details
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <Box className={classes.blueBtn}>
                <Button variant="outlined" onClick={() => {
                    eventTracker(GAEVENTCAT.profile, 'Organization Business Info', 'Start Organization Business Info Edit');
                    setOpenDModal(true)
                  }}
                  style={{marginRight: '15px'}}
                >
                  <Box display="flex" alignItems="center">
                    Documents
                    <Box component="span" display="flex" marginLeft="10px">
                      <CreateIcon fontSize="small" />
                    </Box>
                  </Box>
                </Button>
                <Button variant="outlined" onClick={() => {
                    eventTracker(GAEVENTCAT.profile, 'Organization Business Info', 'Start Organization Business Info Edit');
                    setOpenModal(true)
                  }}
                >
                  <Box display="flex" alignItems="center">
                    Info
                    <Box component="span" display="flex" marginLeft="10px">
                      <CreateIcon fontSize="small" />
                    </Box>
                  </Box>
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountBalanceIcon color="primary" />
                <Typography className={classes.infoHead}>Business Type</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.businessType ? profile.businessType : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <CalendarTodayIcon color="primary" />
                <Typography className={classes.infoHead}>Date of Incorporation</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.dob ? new Date(profile.dob).toLocaleDateString("es-CL") : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountBalanceIcon color="primary" />
                <Typography className={classes.infoHead}>Business Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.businessName ? profile.businessName : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <RecentActorsIcon color="primary" />
                <Typography className={classes.infoHead}>Business PAN</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.businessPAN ? profile.businessPAN : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <PersonIcon color="primary" />
                <Typography className={classes.infoHead}>Director Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.ownerName ? profile.ownerName : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <RecentActorsIcon color="primary" />
                <Typography className={classes.infoHead}>Director PAN</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.ownerPAN ? profile.ownerPAN : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <PaymentIcon color="primary" />
                <Typography className={classes.infoHead}>GSTIN</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.gstin ? profile.gstin : '-'}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <BusinessInformation profile={profile} />
      </Box>
    </Layout>
  );
};

export default OrganizationBusinessInformation;
