import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Tutor } from '../../../common/contracts/user';
import { updateTutor } from '../../../common/api/profile';
import Subject from '../../../../assets/images/course-book.png';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../tutor_layout';
import TutorSubjectInformationModal from '../modals/tutor_subject_information_modal';
import { BoardClassSubjectsMap } from '../../../academics/contracts/board_class_subjects_map';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';

interface Props {
  profile: Tutor;
  profileUpdated: (user: Tutor) => any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#212121'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    yellowBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #4285F4',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#4285F4',
        padding: '7px 15px'
      }
    },
    subHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#333333'
    },
    courseHeading: {
      fontWeight: fontOptions.weight.bolder,
      fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '2px',
      textTransform: 'uppercase',
      marginBottom: '10px'
    },
    courseSubHeading: {
      fontWeight: fontOptions.weight.normal,
      fontSize: fontOptions.size.small,
      lineHeight: '15px',
      color: '#000000'
    },
    standard: {
      fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#405169'
    },
    subjects: {
      fontSize: fontOptions.size.small,
      lineHeight: '19px',
      color: '#405169'
    },
    tableContainer: {
      width: 'auto',
      border: '0.5px solid #E3E3E3',

      '& th': {
        border: '0.5px solid #E3E3E3'
      }
    }
  })
);

const TutorSubjectInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Tutor Subject Info', 'Landed Tutor Subject Info');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveSubjectInformation = async (data: Tutor) => {
    const user = Object.assign({}, profile, data);

    try {
      await updateTutor(user);
      eventTracker(GAEVENTCAT.profile, 'Tutor Subject Info', 'Completed Tutor Subject Info Edit');
      profileUpdated(user);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        //profileUpdated(profile);
      }
    }
  };

  const TutorSubjects = [...profile.courseDetails];

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < TutorSubjects.length; ++i) {
    const subject = TutorSubjects[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase()
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: []
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject
    );
  }

  const boards = Array.from(
    new Set(TutorSubjects.map((subject) => subject.board))
  );

  return (
    <Layout profile={profile as Tutor}>
      <TutorSubjectInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={saveSubjectInformation}
        user={profile as Tutor}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={Subject} alt="Course Details" />

                <Box marginLeft="15px">
                  <Typography component="span" color="primary">
                    <Box className={classes.profileheading}>
                      Course Details
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View &amp; Edit Your Class &amp; Subjects
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box padding="20px 0px 20px 40px">
          <Box
            marginBottom="20px"
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <Box className={classes.subHeading}>Courses</Box>
            <Box className={classes.yellowBtn}>
              <Button variant="outlined" onClick={() => {
                  eventTracker(GAEVENTCAT.profile, 'Tutor Subject Info', 'Start Tutor Subject Info Edit');
                  setOpenModal(true)
                }}
              >
                <Box display="flex" alignItems="center">
                  <Box component="span" display="flex" marginRight="10px">
                    <AddIcon fontSize="small" />
                  </Box>
                  Add
                </Box>
              </Button>
            </Box>
          </Box>

          {boards &&
            boards.length > 0 &&
            boards.map((board, boardIndex) => (
              <Box marginBottom="20px" key={boardIndex}>
                <Typography
                  variant="h5"
                  color="primary"
                  className={classes.courseHeading}
                >
                  {board}
                </Typography>

                <TableContainer>
                  <Table className={classes.tableContainer}>
                    <TableHead>
                      <TableRow>
                        <TableCell className={classes.courseSubHeading}>
                          Classes
                        </TableCell>
                        <TableCell className={classes.courseSubHeading}>
                          Subjects
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>
                      {boardClassSubjectsMap
                        .filter(
                          (boardClassSubjectMap) =>
                            boardClassSubjectMap.boardname === board
                        )
                        .map((boardClassSubjectMap, index) => (
                          <TableRow key={index}>
                            <TableCell
                              component="th"
                              scope="row"
                              className={classes.standard}
                            >
                              {boardClassSubjectMap.classname}
                            </TableCell>
                            <TableCell className={classes.subjects}>
                              {boardClassSubjectMap.subjects.join(', ')}
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Box>
            ))}
        </Box>
      </Box>
    </Layout>
  );
};

export default TutorSubjectInformation;
