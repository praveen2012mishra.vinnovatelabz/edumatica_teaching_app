import React, { FunctionComponent, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
// import { setAuthUser } from '../../../auth/store/actions';
import { Link as RouterLink } from 'react-router-dom';
import {
  Box,
  Button as MuButton,
  Container,
  Grid,
  // IconButton,
  Typography,
  LinearProgress,
  Link,
  TextField,
  IconButton
} from '@material-ui/core';
import {
  AllInclusive as AllInclusiveIcon,
  CheckCircle as CheckCircleIcon,
  CalendarToday as CalendarIcon,
  // ExpandLess as ChevronUpIcon,
  // ExpandMore as ChevronDownIcon,
  ArrowForward as ArrowForwardIcon,
  WatchLater as ClockIcon,
  Lock as LockIcon,
  Schedule as ScheduleIcon
} from '@material-ui/icons';
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TwitterIcon from '@material-ui/icons/Twitter';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import Chart from 'react-google-charts';
import {
  fetchBatchDetails,
  fetchBatchesList,
  fetchSchedulesList
} from '../../../common/api/academics';
import { Tutor } from '../../../common/contracts/user';
import { AppointmentSchedule } from '../../../academics/contracts/schedule';
import CourseBlue from '../../../../assets/svgs/course-blue.svg';
import { ScheduleDetail } from '../../../common/contracts/academic';
import DoughnutGraph from '../../../analytics/components/DoughnutGraph';
// import pdf from '../../../../assets/images/pdf.png';
// import rightArrow from '../../../../assets/images/right-arrow.png';
// import StudentWithMonitor from '../../../../assets/images/student-with-monitor.png';
// import DashPay from '../../../../assets/images/dashPay.png'
import DashStorage from '../../../../assets/images/dashStorage.png'
import BoyWavingHand from '../../../../assets/images/boy-waving-hand.png';
import Analytics from '../../../../assets/images/analytics_dashboard.png';
import Announcement from '../../../../assets/images/announcement_icon.png';
import Meeting_History from '../../../../assets/images/meeting_history.png'
import Button from '../../../common/components/form_elements/button';
import MiniDrawer from '../../../common/components/sidedrawer';
import { generateSchedulerSchema } from '../../../common/helpers';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
// import ProfileImage from '../../containers/profile_image';
import {
  createMeeting,
  createWebHook,
  getMeetingInfo,
  getPostMeetingEventsInfo,
  joinMeeting
} from '../../../bbbconference/helper/api';
import {
  createNewMeeting,
  joinMeetingById
} from '../../../bbbconference/store/actions';
import axios from 'axios';
import { xml2js } from 'xml-js';
import { MeetingRoles } from '../../../bbbconference/enums/meeting_roles';
import { fontOptions } from '../../../../theme';
import { Batch } from '../../../academics/contracts/batch';
import DashTutor from '../../../../assets/svgs/dashTutor.svg';
import DashSub from '../../../../assets/svgs/dashSub.svg';
import DashPay from '../../../../assets/svgs/dashPay.svg';
import DashBook from '../../../../assets/svgs/dashBook.svg';
import DashCap from '../../../../assets/svgs/dashCap.svg';
import './style.css'

const useStyles = makeStyles({
  purchaseBox:{
    backgroundColor: '#4C8BF5', 
    color: '#fff',
    width:'200px'
  },
  usageBox:{
    width:'100%',
    minHeight:'90px',
    display:'flex',
    flexDirection:'column',
    justifyContent:'space-between'
  },
  hourseText:{
    fontSize: fontOptions.size.medium,
    lineHeight: '20px',
    color:'#000',    
  },
  utiliseText:{
    fontSize: fontOptions.size.small,
    lineHeight: '20px',
    color:'#000',
    
  },
  boxText:{
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bolder,
    lineHeight: '20px',
    color:'#4C8BF5',
    
  },
  paymentBox: {
    width: '100%',
    minHeight: '60px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  englishText: {
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bolder,
    lineHeight: '20px',
  },
  welcomeNote: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    color: '#fff',
    marginBottom: '10px'
  },
  boyWavingHard: {
    maxHeight: '165px',
    position: 'absolute',
    bottom: 0,
    right: '50px'
  },
  heading: {
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '26px',
    color: '#4E4E4E',
    marginTop: '30px'
  },
  allClasses: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#666666'
  },
  selectedClass: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#4C8BF5'
  },
  courseImage: {
    background: '#FBFAF9',
    borderRadius: '12px',
    padding: '12px 13px'
  },
  courseName: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '19px',
    color: '#405169',
    marginBottom: '8px'
  },
  batchName: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '15px',
    color: '#000000'
  },
  courseDetails: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    color: '#000000'
  },
  startBtn: {
    '& a': {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  },
  halfOpacity: {
    opacity: '0.5'
  },
  thirdOpacity: {
    opacity: '0.75'
  },
  progressContainer: {
    width: '185px',

    '& p': {
      marginBottom: '10px'
    },
    '& span': {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      color: '#333333',
      textAlign: 'center',
      paddingTop: '5px',
      display: 'block'
    }
  },
  commonBox: {
    bgcolor: '#FFFFFF',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: '5px',
    padding: '16px',
    textAlign: 'center',
    width: '185px'
  },
  boxHeading: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '21px',
    color: '#212121',
    marginBottom: '10px'
  },
  boxNav: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '21px',
    color: '#4C8BF5'
  },
  topicsHeading: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '21px',
    color: '#00B9F5',
    marginBottom: '10px'
  },
  topicsList: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '16px',
    color: '#000000'
  },
  classDetails: {
    display: 'flex',
    marginBottom: '10px',

    '& h5': {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '23px',
      color: '#333333',
      marginRight: '10px'
    },
    '& h6': {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '21px',
      color: '#333333',
      display: 'flex',
      alignItems: 'center',

      '& svg': {
        marginRight: '6px'
      }
    },
    '& span': {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      color: '#4C8BF5',
      marginRight: '20px'
    }
  },
  studentList: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    lineHeight: '19px',
    letterSpacing: '0.25px',
    color: '#000000'
  },
  noSched: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    fontWeight: fontOptions.weight.normal,
    color: '#666666',
    margin: '5px'
  },
  syllabusText: {
    fontSize: fontOptions.size.mediumPlus,
    fontFamily: fontOptions.family,
    fontWeight: fontOptions.weight.bold,
  },
  subtexts: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    color: 'rgba(0, 0, 0, 0.4)'
  },
  textField: {
    [`& fieldset`]: {
      borderRadius: 0,
      height: '118%'
    },
}
});

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 10,
    border: '1px solid #00B9F5'
  },
  colorPrimary: {
    backgroundColor: '#fff'
  },
  bar: {
    borderRadius: 10,
    backgroundColor: '#4C8BF5'
    // margin: '1px 0'
  }
}))(LinearProgress);

interface UpcomingCourseBlockProps {
  item: AppointmentSchedule;
}

const UpcomingCourseBlock: FunctionComponent<UpcomingCourseBlockProps> = ({
  item
}) => {
  const dispatch = useDispatch();
  // const [isExpanded, setIsExpanded] = useState(false);
  // eslint-disable-next-line
  const [students, setStudents] = useState('');
  // eslint-disable-next-line
  const [isActive, setIsActive] = useState(true);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();

  useEffect(() => {
    (async () => {
      try {
        const batch = await fetchBatchDetails({
          batchfriendlyname: item.schedule.batch
            ? item.schedule.batch.batchfriendlyname
            : ''
        });

        setStudents(
          batch.students.map((student) => student.studentName).join(', ')
        );

        const dt = new Date();
        dt.setHours( dt.getHours() + 2 );
        if(new Date(item.startDate) >= dt) {
          setIsActive(false)
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [item]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const CourseIcon = () => {
    // if (isActive) {
    return (
      <Box marginRight="20px">
        <img
          src={CourseBlue}
          className={`${classes.courseImage} ${isActive ? classes.halfOpacity : ''
            }`}
          alt="Course"
        />
      </Box>
    );
    // }

    // return (
    //   <Box marginRight="20px">
    //     <img src={CourseOrange} alt="Course" />
    //   </Box>
    // );
  };

  const joinBlock = (MEETING_ID: string, userName: string) => {
    const uuid = JSON.parse(userName)._id;
    let u_name = JSON.parse(userName).tutorName;
    u_name = u_name.split(' ');
    u_name = u_name.join('+');
    const USER_NAME = `Tutor:+${u_name}`;
    joinMeeting(MEETING_ID, USER_NAME, MeetingRoles.TUTOR, uuid).then((res) => {
      eventTracker(GAEVENTCAT.bbb, 'BBB JOIN', 'Join Meeting by Tutor');
      dispatch(joinMeetingById(res.data));
    });
  };

  const join_Meeting = () => {
    //console.log(item.schedule.batch)
    const MEETING_ID = item.schedule.batch?._id;
    const m_name = `${item.schedule.batch?.batchfriendlyname}+${item.title}`;
    const userName = localStorage.getItem('authUser');
    const scheduleID = item.schedule._id;

    if (
      MEETING_ID !== undefined &&
      m_name !== undefined &&
      scheduleID !== undefined
    ) {
      const MEETING_NAME = m_name.split(' ').join('+');
      getMeetingInfo(MEETING_ID).then((res) => {
        axios.get(res.data).then((res) => {
          const js_res: any = xml2js(res.data, { compact: true });
          if (js_res.response.returncode._text === 'FAILED') {
            //create meeting if no meeting found
            //create meeting specific hook before creating meeting
            createWebHook(MEETING_ID);
            //create meeting after hook is created
            createMeeting(MEETING_ID, MEETING_NAME, scheduleID).then((res) => {
              dispatch(createNewMeeting(res.data));
              if (userName !== null) joinBlock(MEETING_ID, userName);
            });
          } else {
            if (userName !== null) joinBlock(MEETING_ID, userName);
          }
        });
      });
    }
  };

  const CourseActionButton = () => {
    return (
      <Box
        className={`${classes.startBtn} ${isActive ? classes.thirdOpacity : ''
          }`}
      >
        <Button
          disableElevation
          color="primary"
          variant="contained"
          // component={RouterLink}
          // to={`/quizes?batchname=${
          //   item.schedule.batch && item.schedule.batch.batchfriendlyname
          // }&fromhour=${item.schedule.fromhour}&weekday=${
          //   item.schedule.dayname
          // }`}
          onClick={join_Meeting}
          style={isActive ? {backgroundColor: '#F9BD33'} : {backgroundColor: '#CBCBCB'}}
        >
          {`${isActive ? 'Start Class' : ''}`}
          {isActive ? (
            <span></span>
          ) : (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <LockIcon fontSize="small" />
            </Box>
          )}
          {`${isActive ? '' : 'Start Class'}`}
        </Button>
      </Box>
    );

    // return (
    // <Button disabled disableElevation variant="contained">
    //   Start Class{' '}
    //   <Box
    //     component="span"
    //     display="flex"
    //     alignItems="center"
    //     marginLeft="5px"
    //   >
    //     <LockIcon fontSize="small" />
    //   </Box>
    // </Button>
    // );
  };

  const scheduleSessionStart = moment(item.startDate);

  return (
    <Box
      bgcolor="white"
      borderRadius="3px"
      marginTop="16px"
      padding="8px 20px 8px 8px"
    >
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <Box>
            <img
              src={DashCap}
              alt="Course"
              style={{paddingTop: '20px'}}
            />
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="subtitle2" className={classes.courseName} style={{fontWeight: fontOptions.weight.bold}}>
            {item.schedule.batch && item.schedule.batch.classname} -{' '}
            {item.schedule.batch && item.schedule.batch.subjectname}
          </Typography>
          <Typography variant="subtitle1" className={classes.batchName}>
            {item.schedule.batch && item.schedule.batch.batchfriendlyname},{' '}
            {item.schedule.batch && item.schedule.batch.boardname}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="subtitle2" className={classes.courseName} style={{color: '#3D3D3D'}}>
            {scheduleSessionStart.format('hh:mm A')}
          </Typography>
          <Typography variant="subtitle1" className={classes.batchName} style={{color: '#666666'}}>
            {(new Date(scheduleSessionStart.format('YYYY-MM-DD')).toLocaleDateString() === new Date().toLocaleDateString()) ?
              'Today' : scheduleSessionStart.format('DD/MM/YYYY')
            }
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <CourseActionButton />
        </Grid>
      </Grid>

      {/* <Box
        bgcolor="#f2d795"
        marginTop="15px"
        style={{ display: isExpanded ? 'block' : 'none' }}
      >
        <Box padding="15px" display="flex" alignItems="center">
          <img src={StudentWithMonitor} alt="Student with Monitor" />
          <Box marginLeft="15px">
            <Typography>{students}</Typography>
          </Box>
        </Box>
      </Box> */}
    </Box>
  );
};

interface Props {
  profile: Tutor;
}

const OrgTutorDashboard: FunctionComponent<Props> = ({ profile }) => {
  const [batches, setBatches] = useState<Batch[] | null>(null)
  const [schedules, setSchedules] = useState<AppointmentSchedule[]>([]);
  const [subject, setSubject] = useState('');
  const [redirectTo, setRedirectTo] = useState('');
  const [referLink, setReferLink] = useState('');
  const [noOfClasses, setNoOfClasses] = useState<number>(0)
  const [classesInProgress, setClassesInProgress] = useState<number>(0)
  const [allSched, setAllSched] = useState<ScheduleDetail[]>()
  const [totalhr, setTotalhr] = useState(0)
  const [complhr, setComplhr] = useState(0)
  const [ongoinghr, setOngoinghr] = useState(0)
  // const dispatch = useDispatch();
  const classes = useStyles();

  eventTracker(GAEVENTCAT.dashboard, 'Org Tutor Dashboard', 'Landed Org Tutor Dashboard');

  useEffect(() => {
    (async () => {
      try {
        const batchesList = await fetchBatchesList()
        const schedulesList = await fetchSchedulesList();
        setAllSched(schedulesList)
        const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        const dayname = days[new Date().getDay()]
        const todayClass = schedulesList.filter(list => list.dayname === dayname)
        const now = new Date();
        let totalHrs = 0;
        todayClass.forEach(list => {
          const startSplit = list.fromhour.split(':')
          const endSplit = list.tohour.split(":")
          const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
          const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
          totalHrs = totalHrs + (end.getTime() - start.getTime())
        })
        setTotalhr(totalHrs)
        const schedulerSchema = generateSchedulerSchema(
          schedulesList
        ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));
        setBatches(batchesList)
        setSchedules(schedulerSchema);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo, subject]);

  useEffect(() => {
    const now = new Date();
    let comHrs = 0;
    let ongHr = 0;
    batches?.forEach(el => {
      if(!el._id) return
      getMeetingInfo(el._id).then(link => {
        axios.get(link.data).then(meetings => {
          const js_res: any = xml2js(meetings.data, { compact: true });
          // console.log(js_res)
          if (js_res.response.returncode._text !== 'FAILED') {
            setClassesInProgress(classesInProgress + 1)
            const thisSched = allSched?.find(list => list._id === js_res.response.metadata.scheduleid._text)
            if(thisSched) {
              const startSplit = thisSched.fromhour.split(':')
              const endSplit = thisSched.tohour.split(":")
              const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
              const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
              ongHr = ongHr + (end.getTime() - start.getTime())
              setOngoinghr(ongHr)
            }
          }
        }).catch(err => console.log(err))
      })
      getPostMeetingEventsInfo(el._id).then(response => {
        setNoOfClasses(noOfClasses + response.data.length)
        response.data.forEach((element: any) => {
          const meetDate = new Date(element.createTime)
          if(isToday(meetDate)) {
            const thisSched = allSched?.find(list => list._id === element.scheduleID)
            if(thisSched) {
              const startSplit = thisSched.fromhour.split(':')
              const endSplit = thisSched.tohour.split(":")
              const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
              const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
              comHrs = comHrs + (end.getTime() - start.getTime())
              setComplhr(comHrs)
            }
          }
        });
      })
    })

  }, [batches])

  const isToday = (someDate: Date) => {
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  }

  const data = [
    ['Element', 'Hours', { role: 'style' }],
    ['Vanshika', 28, '#F9E5A2'],
    ['Antanu', 30, '#98F3B2'],
    ['Pravez', 20, '#e5e4e2'],
    ['Sudir', 12, '#EDADAD']
  ];

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const subjects = Array.from(
    new Set(profile.courseDetails?.map((course) => course.subject))
  );

  // TODO: Filter schedules based on the upcoming date and time.
  const filteredSchedules = schedules
    .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
    .filter((item) =>
      subject.length > 0
        ? item.schedule.batch &&
        item.schedule.batch.subjectname.toLowerCase() ===
        subject.toLowerCase()
        : true
    );

  return (
    <div>
      <MiniDrawer>

      <Box marginY="50px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={8} style={{marginBottom:'20px'}} className='org-dashboard'>
              <Box paddingX="15px">
                <Box
                  bgcolor="#4C8BF5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                  color='#fff'
                >
                  <Grid item container>
                    <Grid item  md={12} sm={12} xs={12}>
                      <Box className='welcomeBox'>
                        <Typography style={{fontSize: fontOptions.size.medium}}>
                          Hello, {profile.tutorName} !
                        </Typography>
                        <Typography className={classes.welcomeNote}>Welcome Back</Typography>
                      </Box>
                    </Grid>

                    <img
                     //className={classes.boyWavingHard}
                     className='boyWavingHard'
                    src={DashTutor}
                    alt={`Hello ${profile.tutorName}!`}
                  />
                    <Grid item md={3}>
                      <Box>
                        {/* <img
                          className={classes.boyWavingHard}
                          src={BoyWavingHand}
                          alt={`Hello ${profile.tutorName}!`}
                        /> */}
                        {/* <ProfileImage
                          profile={profile}
                          name={profile.tutorName}
                          profileUpdated={(profile) =>
                            dispatch(setAuthUser(profile))
                          }
                        /> */}
                      </Box>
                    </Grid>
                  </Grid>
                </Box>

                <Grid container>
                  <Grid item xs={12} sm={7} style={{paddingRight: '30px'}}>
                    <Typography className={classes.heading}>
                      Upcoming Classes
                    </Typography>
                    <Box display="flex" margin="30px 0 15px 0">
                      <Box marginRight="20px" 
                      >
                        <MuButton
                          size="small"
                          onClick={() => setSubject('')}
                          className={subject === '' ? classes.selectedClass : classes.allClasses}
                        >
                          All Courses
                          </MuButton>
                      </Box>

                      {subjects.map((thissubject, index) => (
                        <Box
                          key={index}
                          marginRight="20px"
                        >
                          <MuButton
                            size="small"
                            onClick={() => setSubject(thissubject)}
                            className={subject === thissubject ? classes.selectedClass : classes.allClasses}
                          >
                            {thissubject}
                          </MuButton>
                        </Box>
                      ))}
                    </Box>
                    {filteredSchedules.length > 0 && (
                      <Box>
                        {filteredSchedules.slice(0, 5).map((item, index) => (
                          <UpcomingCourseBlock key={index} item={item} />
                        ))}
                      </Box>
                    )}
                    {filteredSchedules.length == 0 && (
                      <Box>
                        <Typography className={classes.noSched}>
                          No Schedules in Selected Subject
                        </Typography>
                      </Box>
                    )}
                  </Grid>
                  <Grid item xs={12} lg={5}>
                    <Typography className={classes.heading}>
                      Working Hours
                    </Typography>
                    <Box 
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      paddingY="0px"
                      marginTop="15px"
                    >
                      <div style={{padding: '0px 30px'}}>
                        <Typography style={{float: 'right', marginTop: '15px'}}>Today</Typography>
                      </div>
                      <DoughnutGraph
                        dataset={[
                          { name: 'Completed', value: ((complhr/totalhr)*100) },
                          { name: 'Progress', value: ((ongoinghr/totalhr)*100) },
                          { name: 'TRANSPARENT', value: (((totalhr - (complhr + ongoinghr))/totalhr)*100)},
                        ]}
                        COLORS={['#0088FE', '#EB5757', '#DCDCDC']}
                        size={250}
                      />
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>

            <Grid item xs={12} md={4} sm={12}>
              <Box paddingX="15px">
                <Grid container alignItems="center" alignContent="center" style={{marginBottom: '10px'}}>
                <Grid item xs={12} sm={12} className='classProgressBox'>
                    <Box
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      height="85px"
                    >
                      <Box display="flex" alignItems="center" justifyContent='space-around' className='classAlignBox'>
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={DashBook}
                              alt="Course"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography variant="h4">{noOfClasses}</Typography>
                          <Typography variant="subtitle1">
                            Classes Completed
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={12} className='classProgressBox'>
                    <Box
                      bgcolor="#FFFFFF"
                      borderRadius="3px"
                      color="#3D3D3D"
                      height="85px"
                      marginTop="20px"
                    >
                      <Box display="flex" alignItems="center" justifyContent='space-around' className='classAlignBox'>
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={DashBook}
                              alt="Course"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography variant="h4">
                            {classesInProgress}
                          </Typography>
                          <Typography variant="subtitle1">
                            Classes in Progress
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Meeting History</Box>
                      <Box className={classes.subtexts} marginTop="20px">View previous live classes</Box>
                      <Box className={classes.subtexts}>Class History | Attendance | Recording</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/meetings/dashboard`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Meeting Dashboard
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={Meeting_History}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Student Analytics</Box>
                      <Box className={classes.subtexts} marginTop="20px">View all student related analytics</Box>
                      <Box className={classes.subtexts}>Assessment | Assignments | Attendance</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/profile/analytics`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Analytics Dashboard
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={Analytics}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>
                
                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Announcement</Box>
                      <Box className={classes.subtexts} marginTop="20px">Reach out students</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/pushNotify`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Student Announcement
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                    style={{height:"100px"}}
                      src={Announcement}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                {/* <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>My Usage</Box>
                      <Box className={classes.subtexts} marginTop="20px">You have utilized {} % of Avail Space</Box>
                      <Box className={classes.subtexts}>{} Hours of {} Hours</Box>
                    </Box>
                    <Box  width='100%'><MuButton
                      variant="contained"
                      size="small"
                      onClick={() => ''}
                      style={{  }}
                      color="primary"
                    >
                      Purchase space
                      </MuButton></Box>
                  </Box>
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      src={DashStorage}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box> */}


                {/* <Box display="flex" marginTop='20px' minHeight='100px'>
                  <Box
                    bgcolor="#4C8BF5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-around'
                    alignItems="center"
                    padding="15px"
                    width='25%'
                  >
                    <Box style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Refer &amp; Earn
                    </Box>
                  </Box>
                  <Box
                    bgcolor="#FFFFFF"
                    borderRadius="7px"
                    color="#000"
                    display="flex"
                    alignItems="center"
                    justifyContent='space-around'
                    padding="7px"
                    width='75%'
                  >
                    <Box
                      display="flex"
                      flexDirection='Column'
                      justifyContent='space-around'
                      alignItems="center"
                    >
                      <Grid container style={{textAlign: 'center'}}>
                        <Grid item xs={12} style={{fontSize: fontOptions.size.xSmall}}>
                          <Typography>For each Person you refer, you'll earn 3 classes</Typography>
                        </Grid>
                        <Grid item xs={12} style={{marginTop: '10px'}}>
                          <TextField id="outlined-basic" 
                            label="Refer Link" variant="outlined" 
                            value={referLink} onChange={(e) => {setReferLink(e.target.value)}}
                            size="small"
                            className={classes.textField}
                          />
                          <Button size='small' style={{backgroundColor: '#C4C4C4', borderRadius: '0%'}}>
                            Copy
                          </Button>
                        </Grid>
                        <Grid item xs={12} style={{marginTop: '10px'}}>
                          <div style={{ width: '100%' }}>
                            <Box display="flex" justifyContent="center">
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <MailOutlineIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <WhatsAppIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <FacebookIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <TwitterIcon/>
                                </IconButton>
                              </Box>
                            </Box>
                          </div>
                        </Grid>
                      </Grid>
                    </Box>
                  </Box>
                </Box> */}


                {/* <Box
                  bgcolor="white"
                  marginY="20px"
                  padding="10px 30px"
                  boxShadow="1px 1px 1px rgba(0, 0, 0, 0.25), -1px -1px 1px rgba(0, 0, 0, 0.25)"
                  borderRadius="14px"
                >
                 
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box className={classes.progressContainer}>
                      <Typography>Progress</Typography>
                      <BorderLinearProgress variant="determinate" value={70} />
                      <Typography component="span">30 of 42 hour</Typography>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Class History
                      </Typography>
                      <Link component={RouterLink}
                        to={`/meetings/dashboard`} className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box
                      className={classes.commonBox}
                      border="1px solid #22C5F8"
                    >
                      <Typography className={classes.topicsHeading}>
                        Announcements
                      </Typography>
                      <Link component={RouterLink}
                        to={`/pushNotify`} className={classes.boxNav}>View</Link>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Analytics
                      </Typography>
                      <Link component={RouterLink}
                        to={`/profile/analytics`} className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box>
                    <Typography variant="subtitle1">
                      Student Progress
                    </Typography>
                    <Box>
                      <Chart
                        chartType="ColumnChart"
                        width="100%"
                        height="200px"
                        data={data}
                      />
                    </Box>
                  </Box>
                </Box>
                 */}
                {/* <Box
                  bgcolor="white"
                  borderRadius="7px"
                  padding="20px"
                  display="flex"
                  alignItems="center"
                >
                  <Box marginRight="20px">
                    <img src={StudentWithMonitor} alt="Student with Monitor" />
                  </Box>
                  <Box>
                    <Box className={classes.classDetails}>
                      <Typography variant="h5">Class VII</Typography>
                      <Typography component="span">[ English 2020]</Typography>
                      <Typography variant="h6">
                        <ScheduleIcon /> 06 PM-07 PM
                      </Typography>
                    </Box>
                    <Box>
                      <Typography className={classes.studentList}>
                        Vanshika, Sumit, Antanu, Parvexz, Sudhir, +5...{' '}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
               */}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      </MiniDrawer>
    </div>
  );
};

export default OrgTutorDashboard;
