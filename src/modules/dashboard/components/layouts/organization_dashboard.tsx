import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Box,
  Button as MuButton,
  Container,
  Grid,
  Typography,
  LinearProgress,
  ListItem,
  ListItemIcon,
  ListItemText,
  Link,
  TextField,
  IconButton
} from '@material-ui/core';
import {
  AllInclusive as AllInclusiveIcon,
  CheckCircle as CheckCircleIcon,
  CalendarToday as CalendarIcon,
  ArrowForward as ArrowForwardIcon,
  WatchLater as ClockIcon,
  Lock as LockIcon,
  Schedule as ScheduleIcon
} from '@material-ui/icons';
import randomColor from 'randomcolor'
import { Student } from '../../../common/contracts/user';
import DoughnutGraph from '../../../analytics/components/DoughnutGraph';
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TwitterIcon from '@material-ui/icons/Twitter';
// import DashPay from '../../../../assets/images/dashPay.png'
import DashStorage from '../../../../assets/images/dashStorage.png'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import Chart from 'react-google-charts';
import {
  fetchOrgBatchDetails,
  fetchSchedulesList
} from '../../../common/api/academics';
import { Organization } from '../../../common/contracts/user';
import { AppointmentSchedule } from '../../../academics/contracts/schedule';
import CourseBlue from '../../../../assets/svgs/course-blue.svg';
import StudentWithMonitor from '../../../../assets/images/student-with-monitor.png';
import BoyWavingHand from '../../../../assets/images/boy-waving-hand.png';
import Announcement from '../../../../assets/images/announcement_icon.png';
import Button from '../../../common/components/form_elements/button';
import MiniDrawer from '../../../common/components/sidedrawer';
import Analytics from '../../../../assets/images/analytics_dashboard.png';
import Meeting_History from '../../../../assets/images/meeting_history.png';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { generateSchedulerSchema } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
// import ProfileImage from '../../containers/profile_image';
// import { setAuthUser } from '../../../auth/store/actions';
// import { useDispatch } from 'react-redux';
import { fontOptions } from '../../../../theme';
import { fetchOrgBatchesList } from '../../../common/api/batch';
import { Batch } from '../../../academics/contracts/batch';
import { getMeetingInfo, getPostMeetingEventsInfo } from '../../../bbbconference/helper/api';
import axios from 'axios';
import { xml2js } from 'xml-js';
import DashTutor from '../../../../assets/svgs/dashTutor.svg';
import DashSub from '../../../../assets/svgs/dashSub.svg';
import DashPay from '../../../../assets/svgs/dashPay.svg';
import DashBook from '../../../../assets/svgs/dashBook.svg';
import DashCap from '../../../../assets/svgs/dashCap.svg';
import * as dateFns from "date-fns"
import {
  getOrgStudentsList,
} from '../../../common/api/organization';
import { fetchStudentPaymentAccount } from '../../../payments/helper/api';
import BarGraph from '../../../analytics/components/BarGraph';
import { ScheduleDetail } from '../../../common/contracts/academic';
import './style.css'

const useStyles = makeStyles({
  welcomeNote: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    color: '#fff',
    marginBottom: '10px'
  },
  boyWavingHard: {
    maxHeight: '165px',
    position: 'absolute',
    bottom: 0,
    right: '50px'
  },
  heading: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '26px',
    color: '#4E4E4E',
    marginTop: '30px'
  },
  allClasses: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: 'rgba(0,0,0,0.3)'
  },
  usageBox:{
    width:'100%',
    minHeight:'90px',
    display:'flex',
    flexDirection:'column',
    justifyContent:'space-between'
  },
  selectedClass: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: '#4C8BF5'
  },
  courseImage: {
    background: '#FBFAF9',
    borderRadius: '12px',
    padding: '12px 13px'
  },
  courseName: {
    fontWeight: 'normal',
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: '#405169',
    marginBottom: '8px'
  },
  batchName: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '15px',
    color: '#000000'
  },
  courseDetails: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    color: '#000000'
  },
  startBtn: {
    '& a': {
      fontWeight: 'bold',
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  },
  halfOpacity: {
    opacity: '0.5'
  },
  thirdOpacity: {
    opacity: '0.75'
  },
  progressContainer: {
    width: '185px',

    '& p': {
      marginBottom: '10px'
    },
    '& span': {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      color: '#333333',
      textAlign: 'center',
      paddingTop: '5px',
      display: 'block'
    }
  },
  commonBox: {
    bgcolor: '#FFFFFF',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: '5px',
    padding: '16px',
    textAlign: 'center',
    width: '185px'
  },
  boxHeading: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#212121',
    marginBottom: '10px'
  },
  boxNav: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#4C8BF5'
  },
  topicsHeading: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#00B9F5',
    marginBottom: '10px'
  },
  topicsList: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '16px',
    color: '#000000'
  },
  classDetails: {
    display: 'flex',
    marginBottom: '10px',

    '& h5': {
      fontWeight: 'bold',
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#333333',
      marginRight: '10px'
    },
    '& h6': {
      fontWeight: 'normal',
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '21px',
      color: '#333333',
      display: 'flex',
      alignItems: 'center',

      '& svg': {
        marginRight: '6px'
      }
    },
    '& span': {
      fontWeight: 500,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      color: '#4C8BF5',
      marginRight: '20px'
    }
  },
  studentList: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    letterSpacing: '0.25px',
    color: '#000000'
  },
  noSched: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    fontWeight: fontOptions.weight.normal,
    color: '#666666',
    margin: '5px'
  },
  boxText:{
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bolder,
    lineHeight: '20px',
    color:'#4C8BF5',
    
  },
  paymentBox: {
    width: '100%',
    minHeight: '60px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  subtexts: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    color: 'rgba(0, 0, 0, 0.4)'
  },
  textField: {
    [`& fieldset`]: {
      borderRadius: 0,
      height: '118%'
    },
  }
});

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 10,
    border: '1px solid #00B9F5'
  },
  colorPrimary: {
    backgroundColor: '#fff'
  },
  bar: {
    borderRadius: 10,
    backgroundColor: '#4C8BF5'
    // margin: '1px 0'
  }
}))(LinearProgress);

interface UpcomingCourseBlockProps {
  item: AppointmentSchedule;
}

const UpcomingCourseBlock: FunctionComponent<UpcomingCourseBlockProps> = ({
  item
}) => {
  // const [isExpanded, setIsExpanded] = useState(false);
  // eslint-disable-next-line
  const [students, setStudents] = useState('');
  // eslint-disable-next-line
  const [isActive, setIsActive] = useState(true);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();

  useEffect(() => {
    (async () => {
      try {
        const batch = await fetchOrgBatchDetails({
          batchfriendlyname: item.schedule.batch
            ? item.schedule.batch.batchfriendlyname
            : ''
        });

        setStudents(
          batch.students.map((student) => student.studentName).join(', ')
        );

        const dt = new Date();
        dt.setHours( dt.getHours() + 2 );
        if(new Date(item.startDate) >= dt) {
          setIsActive(false)
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [item]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const CourseIcon = () => {
    // if (isActive) {
    return (
      <Box marginRight="20px">
        <img
          src={CourseBlue}
          className={`${classes.courseImage} ${
            isActive ? classes.halfOpacity : ''
          }`}
          alt="Course"
        />
      </Box>
    );
    // }

    // return (
    //   <Box marginRight="20px">
    //     <img src={CourseOrange} alt="Course" />
    //   </Box>
    // );
  };

  const CourseActionButton = () => {
    return (
      <Box
        className={`${classes.startBtn} ${
          isActive ? classes.thirdOpacity : ''
        }`}
      >
        <Button
          disableElevation
          color="primary"
          variant="contained"
          component={RouterLink}
          // to={`/quizes?batchname=${
          //   item.schedule.batch && item.schedule.batch.batchfriendlyname
          // }&fromhour=${item.schedule.fromhour}&weekday=${
          //   item.schedule.dayname
          // }`}
          style={isActive ? {backgroundColor: '#F9BD33'} : {backgroundColor: '#CBCBCB'}}
        >
          {`${isActive ? 'Start Class' : ''}`}
          {isActive ? (
            <span></span>
          ) : (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <LockIcon fontSize="small" />
            </Box>
          )}
          {`${isActive ? '' : 'Start Class'}`}
        </Button>
      </Box>
    );

    // return (
    // <Button disabled disableElevation variant="contained">
    //   Start Class{' '}
    //   <Box
    //     component="span"
    //     display="flex"
    //     alignItems="center"
    //     marginLeft="5px"
    //   >
    //     <LockIcon fontSize="small" />
    //   </Box>
    // </Button>
    // );
  };

  const scheduleSessionStart = moment(item.startDate);

  return (
    <Box
      bgcolor="white"
      borderRadius="3px"
      marginTop="16px"
      padding="8px 20px 8px 8px"
    >
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <Box>
            <img
              src={DashCap}
              alt="Course"
              style={{paddingTop: '20px'}}
            />
          </Box>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="subtitle2" className={classes.courseName} style={{fontWeight: fontOptions.weight.bold}}>
            {item.schedule.batch && item.schedule.batch.classname} -{' '}
            {item.schedule.batch && item.schedule.batch.subjectname}
          </Typography>
          <Typography variant="subtitle1" className={classes.batchName}>
            {item.schedule.batch && item.schedule.batch.batchfriendlyname},{' '}
            {item.schedule.batch && item.schedule.batch.boardname}
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="subtitle2" className={classes.courseName} style={{color: '#3D3D3D'}}>
            {scheduleSessionStart.format('hh:mm A')}
          </Typography>
          <Typography variant="subtitle1" className={classes.batchName} style={{color: '#666666'}}>
            {(new Date(scheduleSessionStart.format('YYYY-MM-DD')).toLocaleDateString() === new Date().toLocaleDateString()) ?
              'Today' : scheduleSessionStart.format('DD/MM/YYYY')
            }
          </Typography>
        </Grid>
        {/* <Grid item xs={3}>
          <CourseActionButton />
        </Grid> */}
      </Grid>

      {/* <Box
        bgcolor="#f2d795"
        marginTop="15px"
        style={{ display: isExpanded ? 'block' : 'none' }}
      >
        <Box padding="15px" display="flex" alignItems="center">
          <img src={StudentWithMonitor} alt="Student with Monitor" />

          <Box marginLeft="15px">
            <Typography>{students}</Typography>
          </Box>
        </Box>
      </Box> */}
    </Box>
  );
};

interface Props {
  profile: Organization;
}

const OrganizationDashboard: FunctionComponent<Props> = ({ profile }) => {
  const [batches, setBatches] = useState<Batch[] | null>(null)
  const [schedules, setSchedules] = useState<AppointmentSchedule[]>([]);
  const [subject, setSubject] = useState('');
  const [redirectTo, setRedirectTo] = useState('');
  const [referLink, setReferLink] = useState('')
  const [noOfClasses, setNoOfClasses] = useState<number>(0)
  const [classesInProgress, setClassesInProgress] = useState<number>(0)
  const [studentList, setStudentList] = useState<Student[] | null>(null);
  const [allTransactionBarData, setAllTransactionBarData] = useState<any[] | null>(null);
  const [dataKeyOne, setDataKeyOne] = useState<any | null>(null);
  const [allSched, setAllSched] = useState<ScheduleDetail[]>()
  const [totalhr, setTotalhr] = useState(0)
  const [complhr, setComplhr] = useState(0)
  const [ongoinghr, setOngoinghr] = useState(0)

  const classes = useStyles();
  eventTracker(GAEVENTCAT.dashboard, 'Organization Dashboard', 'Landed Organization Dashboard');

  // const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      try {
        const batchesList = await fetchOrgBatchesList()
        const schedulesList = await fetchSchedulesList();
        setAllSched(schedulesList)
        const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        const dayname = days[new Date().getDay()]
        const todayClass = schedulesList.filter(list => list.dayname === dayname)
        const now = new Date();
        let totalHrs = 0;
        todayClass.forEach(list => {
          const startSplit = list.fromhour.split(':')
          const endSplit = list.tohour.split(":")
          const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
          const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
          totalHrs = totalHrs + (end.getTime() - start.getTime())
        })
        setTotalhr(totalHrs)
        const schedulerSchema = generateSchedulerSchema(
          schedulesList
        ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));
        setBatches(batchesList)
        setSchedules(schedulerSchema);
        const studentsListRes = await getOrgStudentsList();
        setStudentList(studentsListRes)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo, subject]);

  useEffect(() => {
    const now = new Date();
    let comHrs = 0;
    let ongHr = 0;
    batches?.forEach(el => {
      if(!el._id) return
      getMeetingInfo(el._id).then(link => {
        axios.get(link.data).then(meetings => {
          // console.log(meetings)
          const js_res: any = xml2js(meetings.data, { compact: true });
          if (js_res.response.returncode._text !== 'FAILED') {
            setClassesInProgress(classesInProgress + 1)
            const thisSched = allSched?.find(list => list._id === js_res.response.metadata.scheduleid._text)
            if(thisSched) {
              const startSplit = thisSched.fromhour.split(':')
              const endSplit = thisSched.tohour.split(":")
              const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
              const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
              ongHr = ongHr + (end.getTime() - start.getTime())
              setOngoinghr(ongHr)
            }
          }
        }).catch(err => console.log(err))
      })
      getPostMeetingEventsInfo(el._id).then(response => {
        setNoOfClasses(noOfClasses + response.data.length)
        
        response.data.forEach((element: any) => {
          const meetDate = new Date(element.createTime)
          if(isToday(meetDate)) {
            const thisSched = allSched?.find(list => list._id === element.scheduleID)
            if(thisSched) {
              const startSplit = thisSched.fromhour.split(':')
              const endSplit = thisSched.tohour.split(":")
              const start = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(startSplit[0]), Number(startSplit[1]))
              const end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), Number(endSplit[0]), Number(endSplit[1]))
              comHrs = comHrs + (end.getTime() - start.getTime())
              setComplhr(comHrs)
            }
          }
        });
      })
    })
  }, [batches])

  useEffect(() => {
    function graphData(
      name: string,
      earning: number
    ) {
      return {name, earning};
    }
    var barData: any[] = []
    const dataKeys = {
      earning:randomColor()
    }
    var monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
    var monthlySpread = [0,0,0,0,0,0,0,0,0,0,0,0]
    let studentIdArr:string[] = [];
    studentList?.map(el => {
      if(!el._id) return
      studentIdArr.push(el._id)
    })
    fetchStudentPaymentAccount(studentIdArr).then(response => {
      // console.log(response)
      // @ts-ignore
      response.map(ele => {
        // @ts-ignore
        ele.purchasedPackage.map(each => {
          // @ts-ignore 
          each.paymentTransactions.map(elem => {
            const month = dateFns.getMonth(elem.created_at)
            monthlySpread[month-1] = monthlySpread[month-1] + elem.netAmountToBene
            // console.log(elem,month,monthlySpread)
          })
        })
      })
      monthlySpread.map(el => {
        barData.push(
          graphData(monthName[monthlySpread.indexOf(el)],el)
        )
      })
      setDataKeyOne(dataKeys)

      const barDataLstFive = barData.slice(Math.max(barData.length - 4, 1))

      setAllTransactionBarData(barDataLstFive)
    })
  }, [studentList])

  const isToday = (someDate: Date) => {
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  }
  

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const data = [
    ['Element', 'Hours', { role: 'style' }],
    ['Vanshika', 28, '#F9E5A2'],
    ['Antanu', 30, '#98F3B2'],
    ['Pravez', 20, '#e5e4e2'],
    ['Sudir', 12, '#EDADAD']
  ];

  const subjects = Array.from(
    new Set(profile.courseDetails?.map((course) => course.subject))
  );

  // TODO: Filter schedules based on the upcoming date and time.
  const filteredSchedules = schedules
    .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
    .filter((item) =>
      subject.length > 0
        ? item.schedule.batch &&
          item.schedule.batch.subjectname.toLowerCase() ===
            subject.toLowerCase()
        : true
    );

  const HelpText = () => {
    return (
      <Grid item md={12}>
        <Box>
          <ListItem dense>
            <ListItemIcon>
              <AllInclusiveIcon color="primary" />
            </ListItemIcon>

            <ListItemText>
              <Box component="h2">Usage Tips</Box>
            </ListItemText>
          </ListItem>

          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>

            <Link color="inherit" component={RouterLink} to={`/profile/tutors`}>
              Add your Tutor details
            </Link>
          </ListItem>

          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>

            <Link
              color="inherit"
              component={RouterLink}
              to={`/profile/students`}
            >
              Add your Student details
            </Link>
          </ListItem>

          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>
            <Link
              color="inherit"
              component={RouterLink}
              to={`/profile/org/batches/create`}
            >
              Create BATCH and include students to Batch
            </Link>
          </ListItem>

          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>
            <Link
              color="inherit"
              component={RouterLink}
              to={`/profile/schedules`}
            >
              Create weekly SCHEDULE for each batch
            </Link>
          </ListItem>

          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>
            <Link
              color="inherit"
              component={RouterLink}
              to={`/profile/courses`}
            >
              Add content (Doc/Image/PDF) to your syllabus and publish during
              classes
            </Link>
          </ListItem>
          <ListItem dense>
            <ListItemIcon>
              <CheckCircleIcon color="primary" />
            </ListItemIcon>
            <Link
              color="inherit"
              component={RouterLink}
              to={`/profile/personal-information`}
            >
              Review/Update your profile
            </Link>
          </ListItem>
        </Box>
      </Grid>
    );
  };

  const subsPlan = profile.package && profile.package.planId && profile.package.planId.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ')

  return (
    <div>
      {/* <MiniDrawer> */}
      <MiniDrawer>

      <Box marginY="50px" className='org-dashboard'>
        <Container>
          <Grid container>
            <Grid item xs={12} md={8}>
              <Box paddingX="15px">
                <Box
                  bgcolor="#4C8BF5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                  color='#fff'
                >
                  <Grid item container>
                    <Grid item  md={12} sm={12} xs={12}>
                      <Box className='welcomeBox'>
                        <Typography style={{fontSize: fontOptions.size.medium}}>
                          Hello, {profile.organizationName} !
                        </Typography>
                        <Typography className={classes.welcomeNote}>Welcome Back</Typography>
                      </Box>
                    </Grid>

                    <img
                    //className={classes.boyWavingHard}
                    className='boyWavingHard'
                    src={DashTutor}
                    alt={`Hello ${profile.organizationName}!`}
                  />
                    <Grid item >
                      <Box>
                        
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
                {/* {filteredSchedules.length < 1 && <HelpText />} */}

                <Grid container>
                  <Grid item xs={12} lg={7} style={{paddingRight: '30px'}}>
                    <Typography className={classes.heading}>
                      Upcoming Classes
                    </Typography>

                    <Box display="flex" margin="30px 0 15px 0">
                      <Box marginRight="20px" 
                      >
                        <MuButton
                          size="small"
                          onClick={() => setSubject('')}
                          className={subject === '' ? classes.selectedClass : classes.allClasses}
                        >
                          All Courses
                          </MuButton>
                      </Box>

                      {subjects.map((thissubject, index) => (
                        <Box
                          key={index}
                          marginRight="20px"
                        >
                          <MuButton
                            size="small"
                            onClick={() => setSubject(thissubject)}
                            className={subject === thissubject ? classes.selectedClass : classes.allClasses}
                          >
                            {thissubject}
                          </MuButton>
                        </Box>
                      ))}
                    </Box>
                    {filteredSchedules.length > 0 && (
                      <Box>
                        {filteredSchedules.slice(0, 5).map((item, index) => (
                          <UpcomingCourseBlock key={index} item={item} />
                        ))}
                      </Box>
                    )}
                    {filteredSchedules.length == 0 && (
                      <Box>
                        <Typography className={classes.noSched}>
                          No Schedules in Selected Subject
                        </Typography>
                      </Box>
                    )}
                  </Grid>
                  <Grid item xs={12} lg={5}>
                    <Typography className={classes.heading}>
                      Working Hours
                    </Typography>
                    <Box 
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      paddingY="0px"
                      marginTop="15px"
                    >
                      <div style={{padding: '0px 30px'}}>
                        <Typography style={{float: 'right', marginTop: '15px'}}>Today</Typography>
                      </div>
                      <DoughnutGraph
                        dataset={[
                          { name: 'Completed', value: ((complhr/totalhr)*100) },
                          { name: 'Progress', value: ((ongoinghr/totalhr)*100) },
                          { name: 'TRANSPARENT', value: (((totalhr - (complhr + ongoinghr))/totalhr)*100)},
                        ]}
                        COLORS={['#0088FE', '#EB5757', '#DCDCDC']}
                        size={250}
                      />
                    </Box>
                    <Typography className={classes.heading} style={{marginTop: '20px'}}>
                      Earnings
                    </Typography>
                    {
                      !allTransactionBarData ? '' :
                      <Box 
                        bgcolor="#FFFFFF"
                        color="#3D3D3D"
                        borderRadius="3px"
                        marginTop="15px"
                        paddingY="30px"
                      >
                        <BarGraph dataset={allTransactionBarData} datakeys={dataKeyOne} width={270} height={250} right={0} left={0}/>
                      </Box>
                    }
                  </Grid>
                </Grid>
              </Box>
            </Grid>

            <Grid item xs={12} md={4} sm={12}>
              <Box paddingX="15px">
                <Grid container alignItems="center" alignContent="center" style={{marginBottom: '10px'}}>
                  
                  <Grid item xs={12} sm={12} className='classProgressBox'>
                    <Box
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      height="85px"
                    >
                      <Box display="flex" alignItems="center" className='classAlignBox'>
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={DashBook}
                              alt="Course"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography variant="h4">{noOfClasses}</Typography>
                          <Typography variant="subtitle1">
                            Classes Completed
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>

                  <Grid item xs={12} sm={12}>
                    <Box
                      bgcolor="#FFFFFF"
                      borderRadius="3px"
                      color="#3D3D3D"
                      height="85px"
                      marginTop="20px"
                    >
                      <Box display="flex" alignItems="center" className='classAlignBox'>
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={DashBook}
                              alt="Course"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography variant="h4">
                            {classesInProgress}
                          </Typography>
                          <Typography variant="subtitle1">
                            Classes in Progress
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Meeting History</Box>
                      <Box className={classes.subtexts} marginTop="20px">View previous live classes</Box>
                      <Box className={classes.subtexts}>Class History | Attendance | Recording</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/meetings/dashboard`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Meeting Dashboard
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={Meeting_History}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>My Analytics</Box>
                      <Box className={classes.subtexts} marginTop="20px">View all student related analytics</Box>
                      <Box className={classes.subtexts}>Assessment | Assignments | Attendance</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/profile/analytics`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Analytics Dashboard
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={Analytics}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Announcement</Box>
                      <Box className={classes.subtexts} marginTop="20px">Reach out students</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/pushNotify`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Student Announcement
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                    style={{height:"100px"}}
                      src={Announcement}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    width='60%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Payment</Box>
                      <Box className={classes.subtexts} marginTop="20px">Manage your Payment Activities</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link component={RouterLink} to={`/payments/feestructure`} style={{textDecorationLine: 'underline'}} >
                        Payment History
                      </Link>
                      <br />
                      <Link component={RouterLink} to={`/payments/feestructure`} style={{textDecorationLine: 'underline'}} >
                        Add Fee Structure
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='40%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={DashPay}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>My Subscription Plan</Box>
                      <Box className={classes.subtexts} style={{color: "#F9BD33", fontSize: fontOptions.size.medium }} marginTop="20px">{subsPlan}</Box>
                    </Box>
                    <Box  width='100%' fontSize={fontOptions.size.small}>
                      <Link color="primary" to={`/payments/edumaticapackage`} component={RouterLink} style={{textDecorationLine: 'underline'}}>
                        Renew Plan
                      </Link>
                    </Box>
                  </Box>
                  <Box
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{height:"100px"}}
                      src={DashSub}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                {/* <Box
                  bgcolor="#00B9F5"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  minHeight='120px'
                  marginTop="20px"
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    width='100%'
                    minHeight='120px'
                  >
                    <Grid container style={{textAlign: 'center'}}>
                      <Grid item xs={12} >
                        <IconButton style={{backgroundColor: '#ffffff', fontSize:fontOptions.size.large}}>
                          <MailOutlineIcon/>
                        </IconButton>
                      </Grid>
                      <Grid item xs={12} style={{marginTop: '10px'}} >
                        <Typography 
                          style={{fontSize:fontOptions.size.large, fontWeight: fontOptions.weight.bold, color: '#ffffff'}} 
                        >
                          My Subscription Plan
                        </Typography>
                      </Grid>
                    </Grid>
                  </Box>
                </Box>
                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  minHeight='120px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    width='100%'
                    minHeight='120px'
                  >
                    <Grid container style={{textAlign: 'center'}}>
                      <Grid item xs={12} >
                        <Typography 
                          style={{fontSize:fontOptions.size.mediumPlus, fontWeight: fontOptions.weight.bold, color: '#00B9F5'}} 
                        >
                          Edumatica Plus
                        </Typography>
                      </Grid>
                      <Grid item xs={12} style={{marginTop: '10px'}}>
                        <Button component={RouterLink} to={`/payments/edumaticapackage`} disableElevation size="large" variant="contained" color="primary">
                          <Typography
                            style={{fontSize:fontOptions.size.medium, padding:"0px 70px"}} 
                          >
                            Renew Plan
                          </Typography>
                        </Button>
                      </Grid>
                      <Grid item xs={12} style={{marginTop: '10px'}}>
                        <Typography
                          style={{fontSize:fontOptions.size.small, fontWeight: fontOptions.weight.normal, color: 'crimson'}} 
                        >
                          Ends On {}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Box>
                </Box> */}

                {/* <Box display="flex" marginTop='20px' minHeight='100px'>
                  <Box
                    bgcolor="#4C8BF5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-around'
                    alignItems="center"
                    padding="15px"
                    width='25%'
                  >
                    <Box style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Refer &amp; Earn
                    </Box>
                  </Box>
                  <Box
                    bgcolor="#FFFFFF"
                    borderRadius="7px"
                    color="#000"
                    display="flex"
                    alignItems="center"
                    justifyContent='space-around'
                    padding="7px"
                    width='75%'
                  >
                    <Box
                      display="flex"
                      flexDirection='Column'
                      justifyContent='space-around'
                      alignItems="center"
                    >
                      <Grid container style={{textAlign: 'center'}}>
                        <Grid item xs={12} style={{fontSize: fontOptions.size.xSmall}}>
                          <Typography>For each Person you refer, you'll earn 3 classes</Typography>
                        </Grid>
                        <Grid item xs={12} style={{marginTop: '10px'}}>
                          <TextField id="outlined-basic" 
                            label="Refer Link" variant="outlined" 
                            value={referLink} onChange={(e) => {setReferLink(e.target.value)}}
                            size="small"
                            className={classes.textField}
                          />
                          <Button size='small' style={{backgroundColor: '#C4C4C4', borderRadius: '0%'}}>
                            Copy
                          </Button>
                        </Grid>
                        <Grid item xs={12} style={{marginTop: '10px'}}>
                          <div style={{ width: '100%' }}>
                            <Box display="flex" justifyContent="center">
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <MailOutlineIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <WhatsAppIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <FacebookIcon/>
                                </IconButton>
                              </Box>
                              <Box marginX="7px">
                                <IconButton style={{backgroundColor: '#E0E0E0', borderRadius: '0%', padding: '5px'}}>
                                  <TwitterIcon/>
                                </IconButton>
                              </Box>
                            </Box>
                          </div>
                        </Grid>
                      </Grid>
                    </Box>
                  </Box>
                </Box> */}


                {/* <Box
                  bgcolor="white"
                  marginY="20px"
                  padding="10px 30px"
                  boxShadow="1px 1px 1px rgba(0, 0, 0, 0.25), -1px -1px 1px rgba(0, 0, 0, 0.25)"
                  borderRadius="14px"
                >
                 
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box className={classes.progressContainer}>
                      <Typography>Progress</Typography>
                      <BorderLinearProgress variant="determinate" value={70} />
                      <Typography component="span">30 of 42 hour</Typography>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Class History
                      </Typography>
                      <Link component={RouterLink}
                        to={`/meetings/dashboard`} className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box
                      className={classes.commonBox}
                      border="1px solid #22C5F8"
                    >
                      <Typography className={classes.topicsHeading}>
                        Announcements
                      </Typography>
                      <Link component={RouterLink}
                        to={`/pushNotify`} className={classes.boxNav}>View</Link>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Analytics
                      </Typography>
                      <Link component={RouterLink}
                        to={`/profile/analytics`} className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box>
                    <Typography variant="subtitle1">
                      Student Progress
                    </Typography>
                    <Box>
                      <Chart
                        chartType="ColumnChart"
                        width="100%"
                        height="200px"
                        data={data}
                      />
                    </Box>
                  </Box>
                </Box>
                 */}
                {/* <Box
                  bgcolor="white"
                  borderRadius="7px"
                  padding="20px"
                  display="flex"
                  alignItems="center"
                >
                  <Box marginRight="20px">
                    <img src={StudentWithMonitor} alt="Student with Monitor" />
                  </Box>
                  <Box>
                    <Box className={classes.classDetails}>
                      <Typography variant="h5">Class VII</Typography>
                      <Typography component="span">[ English 2020]</Typography>
                      <Typography variant="h6">
                        <ScheduleIcon /> 06 PM-07 PM
                      </Typography>
                    </Box>
                    <Box>
                      <Typography className={classes.studentList}>
                        Vanshika, Sumit, Antanu, Parvexz, Sudhir, +5...{' '}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
               */}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>

      {/* <Box marginY="50px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={7}>
              <Box paddingX="15px">
                <Box
                  bgcolor="white"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                >
                  <Grid item container>
                    <Grid item md={9}>
                      <Box>
                        <Typography
                          variant="h5"
                          className={classes.welcomeNote}
                        >
                          Hello {profile.organizationName}!
                        </Typography>
                        <Typography>It's good to see you again.</Typography>
                      </Box>
                    </Grid> */}

                    {/* <img
                    className={classes.boyWavingHard}
                    src={BoyWavingHand}
                    alt={`Hello ${profile.organizationName}!`}
                  /> */}
                    {/* <Grid item md={3}>
                      <Box>
                        <img
                          className={classes.boyWavingHard}
                          src={BoyWavingHand}
                          alt={`Hello ${profile.organizationName}!`}
                        /> */}
                        {/* <ProfileImage
                          profile={profile}
                          name={profile.organizationName}
                          profileUpdated={(profile) =>
                            dispatch(setAuthUser(profile))
                          }
                        /> */}
                      {/* </Box>
                    </Grid>
                  </Grid>
                </Box>
                {<HelpText />}

                <Typography variant="h6" className={classes.heading}>
                  Upcoming Classes
                </Typography>

                <Box display="flex" margin="30px 0 15px 0">
                  <Box marginRight="20px" className={classes.selectedClass}>
                    <MuButton
                        size="small"
                        onClick={() => setSubject('')}
                      >
                        All Courses
                      </MuButton>
                  </Box>

                  {subjects.map((subject, index) => (
                    <Box
                      key={index}
                      marginRight="20px"
                      className={classes.allClasses}
                    >
                      <MuButton
                        size="small"
                        onClick={() => setSubject(subject)}
                      >
                        {subject}
                      </MuButton>
                    </Box>
                  ))}
                </Box>
                {filteredSchedules.length > 0 && (
                  <Box>
                    {filteredSchedules.slice(0, 5).map((item, index) => (
                      <UpcomingCourseBlock key={index} item={item} />
                    ))}
                  </Box>
                )}
                {filteredSchedules.length == 0 && (
                  <Box>
                    <Typography className={classes.noSched}>
                      No Schedules in Selected Subject
                    </Typography>
                  </Box>
                )}
              </Box>
            </Grid>

            <Grid item xs={12} md={5}>
              <Box paddingX="15px">
                <Box display="flex">
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    marginRight="10px"
                    padding="15px"
                  >
                    <Box marginRight="15px">
                      <Typography variant="h2">11</Typography>
                    </Box>

                    <Typography variant="subtitle1">
                      Classes Completed
                    </Typography>
                  </Box>

                  <Box
                    bgcolor="#4C8BF5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    padding="15px"
                  >
                    <Box marginRight="15px">
                      <Typography variant="h2">14</Typography>
                    </Box>

                    <Typography variant="subtitle1">
                      Classes in Progress
                    </Typography>
                  </Box>
                </Box>

                <Box
                  bgcolor="white"
                  marginY="20px"
                  padding="10px 30px"
                  boxShadow="1px 1px 1px rgba(0, 0, 0, 0.25), -1px -1px 1px rgba(0, 0, 0, 0.25)"
                  borderRadius="14px"
                > */}
                  {/* <Typography variant="h6">Your statistics</Typography>

                  <Box display="flex" alignItems="center" marginY="10px">
                    <Box marginRight="5px">
                      <MuButton size="small">Teaching Hours</MuButton>
                    </Box>

                    <Box>
                      <MuButton size="small">My Classes</MuButton>
                    </Box>
                  </Box> */}
                  {/* <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box className={classes.progressContainer}>
                      <Typography>Progress</Typography>
                      <BorderLinearProgress variant="determinate" value={70} />
                      <Typography component="span">30 of 42 hour</Typography>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Class History
                      </Typography>
                      <Link className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    marginBottom="15px"
                  >
                    <Box
                      className={classes.commonBox}
                      border="1px solid #22C5F8"
                    >
                      <Typography className={classes.topicsHeading}>
                        Announcement
                      </Typography>
                      <Link component={RouterLink}
                      to={`/pushNotify`} className={classes.boxNav}>View</Link>
                    </Box>
                    <Box
                      className={classes.commonBox}
                      border="1px solid #4C8BF5"
                    >
                      <Typography className={classes.boxHeading}>
                        Syllabus
                      </Typography>
                      <Link className={classes.boxNav}>View</Link>
                    </Box>
                  </Box>
                  <Box>
                    <Typography variant="subtitle1">
                      Student Progress
                    </Typography>
                    <Box>
                      <Chart
                        chartType="ColumnChart"
                        width="100%"
                        height="200px"
                        data={data}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  bgcolor="white"
                  borderRadius="7px"
                  padding="20px"
                  display="flex"
                  alignItems="center"
                >
                  <Box marginRight="20px">
                    <img src={StudentWithMonitor} alt="Student with Monitor" />
                  </Box>
                  <Box>
                    <Box className={classes.classDetails}>
                      <Typography variant="h5">Class VII</Typography>
                      <Typography component="span">[ English 2020]</Typography>
                      <Typography variant="h6">
                        <ScheduleIcon /> 06 PM-07 PM
                      </Typography>
                    </Box>
                    <Box>
                      <Typography className={classes.studentList}>
                        Vanshika, Sumit, Antanu, Parvexz, Sudhir, +5...{' '}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box> */}
      </MiniDrawer>
    </div>
  );
};

export default OrganizationDashboard;
