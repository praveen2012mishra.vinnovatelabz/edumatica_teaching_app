import React, { FunctionComponent, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { connect } from 'react-redux';
import { RootState } from '../../../../store';
import {
  Box,
  Button as MuButton,
  Container,
  Card,
  CardActions,
  CardContent,
  Grid,
  Avatar,
  Link,
  LinearProgress,
  Typography,
  FormControl,
  Select
} from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import {
  CalendarViewDayRounded as CalendarIcon,
  ArrowForward as ArrowForwardIcon,
  Lock as LockIcon,
  WatchLater as ClockIcon
} from '@material-ui/icons';
import {
  createStyles,
  withStyles,
  WithStyles,
  makeStyles
} from '@material-ui/core/styles';
import moment from 'moment';
import { fetchStudentSchedulesList, fetchBatchesList, fetchDownloadUrlForSyllabus } from '../../../common/api/academics';
import { fetchStudentBatchesList } from '../../../common/api/batch';
import { Student, User, Children } from '../../../common/contracts/user';
import { AppointmentSchedule } from '../../../academics/contracts/schedule';
import CourseBlue from '../../../../assets/svgs/course-blue.svg';
import Button from '../../../common/components/form_elements/button';
import MiniDrawer from '../../../common/components/sidedrawer';
import { generateSchedulerSchema } from '../../../common/helpers';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { getAttemptAssessments } from '../../../student_assessment/helper/api';
import { OngoingAssessment } from '../../../student_assessment/contracts/assessment_interface';
import GraduatedStudent from '../../../../assets/images/student.png';
import Knowledge from '../../../../assets/images/knowledge.png';
import DashPay from '../../../../assets/images/dashPay.png'
import Syllabus from '../../../../assets/images/syllabus.png';
import Clock from '../../../../assets/images/clock.png';
import Analytics from '../../../../assets/images/analytics_dashboard.png';
import Meeting_History from '../../../../assets/images/meeting_history.png';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  attemptedAssessmentAnswers,
  attemptedAssessmentData
} from '../../../analytics/helpers/api';
import {
  getMeetingInfo,
  isMeetingRunning,
  joinMeeting
} from '../../../bbbconference/helper/api';
import DashCap from '../../../../assets/svgs/dashCap.svg';
import { joinMeetingById } from '../../../bbbconference/store/actions';
import { xml2js } from 'xml-js';
import axios from 'axios';
import { MeetingRoles } from '../../../bbbconference/enums/meeting_roles';
import { fontOptions } from '../../../../theme';
import studentDashboardImage from '../../../../assets/svgs/studentDashboardImage.svg'
import studentCap from '../../../../assets/images/studentCap.png'
import ClassHistory from '../../../../assets/svgs/classHistory.svg'
import knowledgeHover from '../../../../assets/svgs/knowledgeHover.svg'
//knowledgeHover.png
import { Link as RouterLink } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import attendance from '../../../../assets/images/attendenceStudent.svg'
import {useSnackbar} from "notistack"
import { getPostIndividualMeetingEventsInfo, getPostMeetingEventsInfo } from '../../../bbbconference/helper/api';
import { Meeting } from '../../../bbbconference/contracts/meeting_interface';
import { BBBEvents } from '../../../bbbconference/contracts/bbbevent_interface';
import { AttendanceOption } from '../../../bbbconference/enums/attendance_options';
import { AssessmentAnswers } from '../../../student_assessment/contracts/assessment_answers';

const styles = createStyles({
  boyWavingHard: {
    maxHeight: '165px',
    position: 'absolute',
    bottom: 0,
    right: '50px'
  },
  root: {
    minWidth: 275
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
  },
  pos: {
    marginBottom: 12
  },
  usageBox: {
    width: '100%',
    minHeight: '90px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  boxText: {
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bolder,
    lineHeight: '20px',
    color: '#3D3D3D',
  },
  subtexts: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal
  },
  paymentBox: {
    width: '100%',
    minHeight: '60px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
});

const useStyles = makeStyles({
  learnText: {
    fontSize: fontOptions.size.medium,
    lineHeight: '18px',
    color: '#000',
    fontWeight: fontOptions.weight.bold,
    paddingBottom: '10px'
  },
  classesText: {
    fontSize: fontOptions.size.small,
    lineHeight: '18px',
    color: 'rgba(0, 0, 0, 0.3)',
    paddingBottom: '10px'
  },
  topicText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
  },
  topicTextContent: {
    fontSize: fontOptions.size.small,
    lineHeight: '18px',
    color: '#3D3D3D'
  },
  root: {
    minWidth: 275,
    fontFamily: fontOptions.family,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
    fontFamily: fontOptions.family,
  },
  title: {
    fontSize: 14,
    fontFamily: fontOptions.family,
  },
  pos: {
    marginBottom: 12,

  },
  welcomeNote: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    color: '#fff',
    marginBottom: '10px'
  },
  welcomeText: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    marginBottom: '10px'
  },
  helperText: {
    color: '#FFFFFF',
    fontFamily: fontOptions.family
  },
  heading: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '26px',
    color: '#4E4E4E',
    marginTop: '30px'
  },
  allClasses: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: 'rgba(0,0,0,0.3)'
  },
  scoreText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '20px',
    fontFamily: fontOptions.family,
    color: '#000',
  },
  scoreBox: {
  },
  attendenceBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    margin: '25px 0',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  topicBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    //marginBottom: '25px',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  noSched: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    fontWeight: fontOptions.weight.normal,
    color: '#666666',
    margin: '5px'
  },
  syllabusBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  selectedClass: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: '#4C8BF5'
  },
  courseImage: {
    background: '#FBFAF9',
    borderRadius: '12px', fontFamily: fontOptions.family,
    padding: '12px 13px'
  },
  courseName: {
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.small,
    lineHeight: '19px',
    fontFamily: fontOptions.family,
    color: '#405169',
    marginBottom: '8px'
  },
  batchName: {
    fontSize: fontOptions.size.small,
    lineHeight: '15px',
    fontFamily: fontOptions.family,
    color: '#000000'
  },
  courseDetails: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    color: '#000000'
  },
  startBtn: {
    '& a': {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  },

  boxHeading: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#212121',
    marginBottom: '10px'
  },
  boxNav: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#4C8BF5'
  },
  topicsHeading: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
  },
  subhead: {
    marginTop: '40px',
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.medium,
    fontFamily: fontOptions.family,
    color: '#666666'
  },
  subvalues: {
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.medium,
    fontFamily: fontOptions.family,
    color: '#404040'
  },
  topicsList: {
    fontSize: fontOptions.size.small,
    lineHeight: '16px',
    color: '#000000'
  },
  halfOpacity: {
    opacity: '0.5'
  },
  thirdOpacity: {
    opacity: '0.75'
  },
  progressContainer: {
    width: '185px',

    '& p': {
      marginBottom: '10px'
    },
    '& span': {
      fontSize: fontOptions.size.small,
      color: '#333333',
      textAlign: 'center',
      paddingTop: '5px',
      display: 'block'
    }
  },
  commonBox: {
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: '5px',
    textAlign: 'center',
    width: '185px'
  },
  commonBoxO: {
    backgroundColor: '#4C8BF5',
    color: 'white',
    padding: '9px'
  },
  commonBoxT: {
    backgroundColor: 'white',
    padding: '16px',
    color: '#666666'
  },
  navItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '8px',
    marginBottom: '13px',
    fontFamily: fontOptions.family,
    '& p': {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: 'black',
      marginLeft: '10px'
    }
  },
  navLinks: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: fontOptions.family,
    '& a': {
      fontFamily: fontOptions.family,
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      color: 'black',
      marginRight: '14px'
    }
  },
  formControl: {
    minWidth: 120,
  },
  studentProgressContainer: {
    '& a': {
      display: 'inline-block',
      marginTop: '10px',
      marginLeft: '25px'
    }
  },
  subjectText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#000'
  },
  chapterName: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    color: 'rgba(0, 0, 0, 0.3)'
  },
  studentProgress: {
    textAlign: 'center',
    flex: 1,

    '& h3': {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '21px',
      color: '#212121',
      marginBottom: '10px'
    },
    '& h4': {
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '28px',
      color: '#212121',
      width: '37%',
      margin: '0 auto',
      marginTop: '5px'
    },
    '& h5': {
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '21px',
      color: '#212121',
      marginTop: '5px'
    }
  },
  learnMore: {
    '& h5': {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: '#000000',
      marginBottom: '12px'
    },
    '& p': {
      fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
      lineHeight: '24px',
      color: '#000000',
      marginBottom: '15px',
      width: '70%'
    }
  },
  btnYellow: {
    '& button': {
      background: '#F9BD33',
      borderRadius: '8px',
      padding: '12px 24px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  },
  allClassesText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#666666'
  },
  selectedClassText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#4C8BF5'
  },
});

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 10,
    border: '1px solid #00B9F5'
  },
  colorPrimary: {
    backgroundColor: '#fff'
  },
  bar: {
    borderRadius: 10,
    backgroundColor: '#4C8BF5'
    // margin: '1px 0'
  }
}))(LinearProgress);

interface UpcomingCourseBlockProps {
  item: AppointmentSchedule;
}

interface CardProps {
  data: OngoingAssessment;
  profile: Student;
}

const SimpleCard: FunctionComponent<CardProps> = ({ profile, data }) => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {data.isSubmitted
            ? 'Submitted'
            : data.isStarted
              ? 'Ongoing'
              : 'Not Attempted'}
        </Typography>
        <Typography variant="h5" component="h2">
          {data.assessment.assessmentname}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {data.assessment.boardname +
            '-' +
            data.assessment.classname +
            '-' +
            data.assessment.subjectname}
        </Typography>
        <Typography variant="body2" component="p">
          Attempt Window
          <br />
          Start Time : {new Date(data.startDate).toLocaleString()}
          <br />
          End Time : {new Date(data.endDate).toLocaleString()}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={() => {
            history.push(
              '/students/' +
              profile.mobileNo +
              '/student_assessement_test?attemptassessmentId=' +
              data._id
            );
          }}
        >
          Open
        </Button>
      </CardActions>
    </Card>
  );
};

const UpcomingCourseBlock: FunctionComponent<UpcomingCourseBlockProps> = ({
  item
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  // eslint-disable-next-line
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    const dt = new Date();
    dt.setHours( dt.getHours() + 2 );
    if(new Date(item.startDate) >= dt) {
      setIsActive(false)
    }
  }, [item]);

  const styles = useStyles();

  const CourseIcon = () => {
    // if (isActive) {
    return (
      <Box marginRight="20px">
        <img
          src={CourseBlue}
          className={`${styles.courseImage} ${isActive ? styles.halfOpacity : ''
            }`}
          alt="Course"
        />
      </Box>
    );
    // }

    // return (
    //   <Box marginRight="20px">
    //     <img src={CourseOrange} alt="Course" />
    //   </Box>
    // );
  };

  const join_Meeting = () => {
    var flag = 0;
    const MEETING_NAME = `${item.schedule.dayname} - ${item.schedule.fromhour} to ${item.schedule.tohour} | ${item.schedule.batch?.batchfriendlyname} | ${item.title}`;
    const MEETING_ID = item.schedule.batch?._id;
    const userName = localStorage.getItem('authUser');
    if (MEETING_ID !== undefined && userName !== null) {
      const uuid = JSON.parse(userName)._id;
      const u_name = JSON.parse(userName).studentName;
      const user_name = u_name.split(' ');
      const USER_NAME = `${user_name.join('+')}+ID:${JSON.parse(userName).enrollmentId
        }`;
      //checking if meeting is running
      isMeetingRunning(MEETING_ID).then((response) => {
        const js_response: any = response.data;
        if (js_response.response.running._text === 'false') {
          //window.alert('Class yet to be started by Tutor')
          //history.push(`/meetings/${JSON.parse(userName).mobileNo}/${MEETING_ID}/${uuid}/${USER_NAME}`)
          history.push(
            `/meetings/${MEETING_ID}/${uuid}/${USER_NAME}/${MEETING_NAME}`
          );
          return;
        }
        // Check if user already present in meeting room
        getMeetingInfo(MEETING_ID).then((res) => {
          axios.get(res.data).then((res) => {
            const js_res: any = xml2js(res.data, { compact: true });

            if (Array.isArray(js_res.response.attendees.attendee) === true) {
              const members: any[] = js_res.response.attendees.attendee;
              members.forEach((member) => {
                if (member.userID._text === uuid) flag = 1;
              });
            }
            if (flag === 0) {
              //joining meeting
              joinMeeting(
                MEETING_ID,
                USER_NAME,
                MeetingRoles.STUDENT,
                uuid
              ).then((res) => {
                eventTracker(GAEVENTCAT.bbb, 'BBB JOIN', 'Join Meeting by Student');
                dispatch(joinMeetingById(res.data));
              });
            } else {
              window.alert(
                'Multiple Instance Alert. You are already in the classroom'
              );
            }
          });
        });
      });
    }
  };

  const CourseActionButton = () => {
    return (
      <Box
        className={`${styles.startBtn} ${isActive ? styles.thirdOpacity : ''}`}
      >
        <Button
          disableElevation
          color="primary"
          variant="contained"
          style={{ background: isActive ? '#F9BD33' : '#CBCBCB' }}
          // component={RouterLink}
          // to={`/quizes?batchname=${
          //   item.schedule.batch && item.schedule.batch.batchfriendlyname
          // }&tutorId=${item.schedule.tutorId}&fromhour=${
          //   item.schedule.fromhour
          // }&weekday=${item.schedule.dayname}`}
          onClick={() => {
            join_Meeting();
          }}
        >
          {`${isActive ? 'Join Class' : ''}`}
          {isActive ? (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <ArrowForwardIcon fontSize="small" />
            </Box>
          ) : (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <LockIcon fontSize="small" />
            </Box>
          )}
          {`${isActive ? '' : 'Start Class'}`}
        </Button>
      </Box>
    );
  };

  const scheduleSessionStart = moment(item.startDate);

  return (
    <Box
      bgcolor="white"
      borderRadius="3px"
      marginTop="16px"
      padding="8px 20px 8px 8px"
    >
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <Box>
            <img
              src={DashCap}
              alt="Course"
              style={{paddingTop: '20px'}}
            />
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="subtitle2" className={styles.courseName} style={{fontWeight: fontOptions.weight.bold}}>
            {item.schedule.batch && item.schedule.batch.classname} -{' '}
            {item.schedule.batch && item.schedule.batch.subjectname}
          </Typography>
          <Typography variant="subtitle1" className={styles.batchName}>
            {item.schedule.batch && item.schedule.batch.batchfriendlyname},{' '}
            {item.schedule.batch && item.schedule.batch.boardname}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="subtitle2" className={styles.courseName} style={{color: '#3D3D3D'}}>
            {scheduleSessionStart.format('hh:mm A')}
          </Typography>
          <Typography variant="subtitle1" className={styles.batchName} style={{color: '#666666'}}>
            {(new Date(scheduleSessionStart.format('YYYY-MM-DD')).toLocaleDateString() === new Date().toLocaleDateString()) ?
              'Today' : scheduleSessionStart.format('DD/MM/YYYY')
            }
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <CourseActionButton />
        </Grid>
      </Grid>
    </Box>
  );
};

interface Props extends WithStyles<typeof styles> {
  authUser: User,
  profile: Student;
}

function CircularProgressWithLabel(props: any) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" {...props} style={{ width: '70px', height: '70px', padding: '5px' }} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="textSecondary">
          30/42 <br />
         Hour
        </Typography>
      </Box>
    </Box>
  );
}

export interface AssessmentDetail {
  assessmentname: string;
  boardname: string;
  classname: string;
  duration: number;
  instructions: string;
  ownerId: string;
  sections: string[];
  status: number;
  subjectname: string;
  totalMarks: number;
  updatedby: string;
  updatedon: string;
  userId: string;
  marks?:string;
  _id?:string;
}

interface selectedAssessmentType {
  answers: any;
  assessment: AssessmentDetail;
  assessmentData: any;
  attemptedQuestions: any;
  currentQuestion: number;
  endDate: string;
  endTime: string;
  isStarted: boolean;
  isSubmitted?: boolean;
  ownerId: string;
  solutionTime: string;
  startDate: string;
  studentId: string;
  updatedby: string;
  updatedon: string;
  _id:string;
}

const isGetSelectedAssessmentRes = (x: any): x is AssessmentDetailres => x.assessment

interface AssessmentDetailres {
  assessment: AssessmentDetail;
  isSubmitted?: boolean;
  _id:string;
}

export interface Schedule {
  _id?: string;
  ownerId?: string;
  tutorId?: Tutor;
  tutor?: string;
  mobileNo?: string;
  dayname: string;
  fromhour: string;
  tohour: string;
  batch?: Batch;
}

export interface Tutor {
  _id?: string;
}

interface AttendanceData {
  id: number,
  subject: string,
  overallAttendance: number
}

interface Batch {
  _id?: string;
  schedules?: Schedule[];
  content?: string[];
  students: string[];
  boardname: string;
  classname: string;
  subjectname: string;
  batchfriendlyname: string;
  batchenddate: string;
  batchstartdate: string;
  batchicon: string;
  tutor: string;
  tutorId?: Tutor;
  syllabus?: string;
}

const StudentDashboard: FunctionComponent<Props> = ({ classes, profile, authUser }) => {
  const [schedules, setSchedules] = useState<AppointmentSchedule[]>([]);
  const [subject, setSubject] = useState('course');
  const [redirectTo, setRedirectTo] = useState('');
  const [batchList, setBatchList] = useState<Batch[]>([]);
  const [getSelectedAssessment, setSelectedAssessment] = useState<selectedAssessmentType[]>([]);
  const [attemptAssessmentData, setAttemptAssessmentData] = useState<
  OngoingAssessment[] | null
  >(null);
  const [dashboardObject, setDashboardObject] = useState<string>('courses');
  const history = useHistory();
  const styles = useStyles();
  const [subjectList, setSubjectList] = useState<string[]>([]);
  eventTracker(GAEVENTCAT.dashboard, 'Student Dashboard', 'Landed Student Dashboard');
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { enqueueSnackbar } = useSnackbar();
  const [overAllAttendanceData, setOverAllAttendanceData] = useState<AttendanceData[]>([])
  const [activeAssessmentIndex, setActiveAssessmentIndex] = useState(-1)
  const [totalMarks, setTotalMarks] = useState<number>(0)
  const [mythismark, setMythismark] = useState<string>('')
  const [
    attemptAssessment,
    setAttemptAssessment
  ] = useState<OngoingAssessment | null>(null);

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (e: any, item: any) => {
    setSelectedAssessment(item)
    setAnchorEl(null);
  };

  const [progress, setProgress] = React.useState(10);

  const downloadSyllabus = async(batch: Batch) => {
    if(batch.syllabus) {
      const awsBucket = await fetchDownloadUrlForSyllabus(batch._id as string, batch.syllabus as string);
      setTimeout(() => {
        const response = {
          file: awsBucket.url,
        };
        window.open(response.file);
      }, 100);
    } else {
      enqueueSnackbar("Syllabus NOT available", {variant: 'warning'});
    }
  }

  function allAttendanceData(
    id: number,
    subject: string,
    overallAttendance: number
  ) {
    return { id, subject, overallAttendance };
  }

  const getAnswers = (
    assessmentId: string,
    eachAssessment: OngoingAssessment
  ) => {
    if (!profile._id) return;
    attemptedAssessmentAnswers(profile._id, assessmentId)
      .then((result) => {
        let assessmentAnswers: AssessmentAnswers = result;
        let myMark = 0;
        assessmentAnswers.forEach((answer) => {
          answer.forEach((eachAnswer) => {
            myMark = myMark + Number(eachAnswer.marks)
          });
        });

        setMythismark(String(myMark))
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    (async () => {
      try {
        const schedulesList = await fetchStudentSchedulesList();
        const schedulerSchema = generateSchedulerSchema(
          schedulesList
        ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));

        const batchesListResponse = await fetchStudentBatchesList(authUser._id ? authUser._id : '')
        setBatchList(batchesListResponse)

        const attemptedAssessmentDataResponse = await attemptedAssessmentData(
          authUser._id as string
        );
        const [attemptedAssessment, batchesList] = await Promise.all([
          attemptedAssessmentDataResponse, batchesListResponse
        ]);
        setAttemptAssessmentData(attemptedAssessment);

        setSchedules(schedulerSchema);
        setSubject('')
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo]);

  useEffect(() => {
    if(attemptAssessmentData && activeAssessmentIndex>=0) {
      setAttemptAssessment(attemptAssessmentData[activeAssessmentIndex])
      setTotalMarks(attemptAssessmentData[activeAssessmentIndex].assessment.totalMarks)
      getAnswers(attemptAssessmentData[activeAssessmentIndex]._id, attemptAssessmentData[activeAssessmentIndex])
    }
  }, [activeAssessmentIndex])

  useEffect(() => {
    const getList = async () => {
      const schedulesList = await fetchStudentSchedulesList();
      const schedulerSchema = generateSchedulerSchema(
        schedulesList
      ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));
      const setSubjects = schedulerSchema
        .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
        .filter((item) =>
          subject.length > 0 && true
        );
      const subjects: string[] = Array.from(
        new Set(setSubjects?.map((course) => course.subject))
      );
      setSubjectList(subjects)

      //get batches list
    }
    getList();

  }, []);

  // Attendance
  useEffect(() => {
    if (!authUser) return
    batchList?.map(batch => {
      // @ts-ignore
      getPostMeetingEventsInfo(batch._id).then((result) => {
        if (result.message !== 'Success') return
        const meetings: Meeting[] = result.data
        var noPresent = 0;
        var noAbsent = 0;
        meetings.forEach(eachClass => {
          getPostIndividualMeetingEventsInfo(eachClass.internalMeetingID).then((result) => {
            if (result.message !== 'Success') return
            var ispresent = AttendanceOption.IS_ABSENT;
            const individualMeetingInfo: Meeting = result.data;
            const events: BBBEvents[] = individualMeetingInfo.events;
            const userEvent = events.filter(function (item) {
              if(item.userID === authUser._id) {
                return item.type === 'user-joined' || item.type === 'user-left';
              }
            });
            let total_time = 0
            const meetingEndEvent = events.filter(function (items) {
              return items.type === 'meeting-ended';
            });
            var totalMeetingTime = meetingEndEvent[0].eventTriggerTime - individualMeetingInfo.createTime;
            userEvent.forEach(eachEvent => {
              if(eachEvent.type === "user-joined"){
                if(eachEvent.userID === undefined) return
                  if( authUser._id !== eachEvent.userID) {
                    ispresent = AttendanceOption.IS_ABSENT
                  }
              }
            })
            if(userEvent.length === 0){
              ispresent = AttendanceOption.IS_ABSENT
            }
            const length = userEvent.length
            if(userEvent.length !== 0 && (length % 2) !== 0) {
              for (let i=0; i<userEvent.length - 1; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              total_time = total_time + (meetingEndEvent[0].eventTriggerTime - userEvent[length-1].eventTriggerTime)
              const percentage = (total_time/totalMeetingTime)*100
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            if(userEvent.length !== 0 && (length % 2) === 0) {
              for (let i=0; i<userEvent.length; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              const percentage = (total_time/totalMeetingTime)*100;
              // eslint-disable-next-line
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            if(ispresent === AttendanceOption.IS_PRESENT) {
              noPresent++
            } else{
              noAbsent++
            }
            if(meetings.indexOf(eachClass) === (meetings.length - 1)){
              setOverAllAttendanceData([...overAllAttendanceData,allAttendanceData(batchList.indexOf(batch)+1, batch.subjectname, (noPresent/(noPresent+noAbsent))*100)])
            }
          });
        })
      });
    })
  }, [batchList])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  // TODO: Filter schedules based on the upcoming date and time.
  const filteredSchedules = schedules
    .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
    .filter((item) =>
      subject.length > 0
        ? item.schedule.batch &&
        item.schedule.batch.subjectname.toLowerCase() ===
        subject.toLowerCase()
        : true
    );


  return (
    <div>
      <MiniDrawer>

      <Box marginY="50px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={8}>
              <Box marginRight='15px'>
                <Box
                  bgcolor="#4C8BF5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                  color='#fff'
                >
                  <Grid item container>
                    <Grid item md={9}>
                      <Box>
                        <Typography style={{fontSize: fontOptions.size.medium}}>
                          Hello, {profile.studentName} !
                        </Typography>
                        <Typography className={styles.welcomeNote}>Welcome Back</Typography>
                      </Box>
                    </Grid>
                    <img
                      className={classes.boyWavingHard}
                      src={studentDashboardImage}
                      alt={`Hello ${profile.studentName}!`}
                    />
                    <Grid item md={3}>
                      <Box>
                        {/* <img
                          className={classes.boyWavingHard}
                          src={BoyWavingHand}
                          alt={`Hello ${profile.organizationName}!`}
                        /> */}
                        {/* <ProfileImage
                          profile={profile}
                          name={profile.tutorName}
                          profileUpdated={(profile) =>
                            dispatch(setAuthUser(profile))
                          }
                        /> */}
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
              </Box>

              <Grid container spacing={2}>
                <Grid item xs={12} md={7}>
                  <Typography className={styles.heading}>
                    Upcoming Classes
                  </Typography>

                  <Box display="flex" margin="30px 0 15px 0">
                    <Box marginRight="20px" 
                    >
                      <MuButton
                        size="small"
                        onClick={() => {
                          setDashboardObject('courses');
                          setSubject('');
                        }}
                        className={subject === '' ? styles.selectedClass : styles.allClasses}
                      >
                        All Courses
                      </MuButton>
                    </Box>
                    {subjectList.map((thissubject, index) => (
                      <Box
                        key={index}
                        marginRight="20px"
                      >
                        <MuButton
                          size="small"
                          onClick={() => setSubject(thissubject)}
                          className={subject === thissubject ? styles.selectedClass : styles.allClasses}
                        >
                          {thissubject}
                        </MuButton>
                      </Box>
                    ))}
                  </Box>
                  {filteredSchedules.length > 0 && (
                    <Box>
                      {filteredSchedules.slice(0, 5).map((item, index) => (
                        <UpcomingCourseBlock key={index} item={item} />
                      ))}
                    </Box>
                  )}
                  {filteredSchedules.length == 0 && (
                    <Box>
                      <Typography className={styles.noSched}>
                        No Schedules in Selected Subject
                      </Typography>
                    </Box>
                  )}
                </Grid>
                <Grid item xs={12} md={5}>
                  <Typography className={styles.heading}>
                    Progress
                  </Typography>

                  <Box 
                    bgcolor="#FFFFFF"
                    color="#3D3D3D"
                    borderRadius="3px"
                    paddingY="10px"
                    marginTop="15px"
                  >
                    {
                      !attemptAssessmentData ? '' :
                      <Box padding="0 8px">
                        <FormControl className={styles.formControl}>
                          <Select
                            value={activeAssessmentIndex}
                            onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                              { 
                                setMythismark('')
                                setActiveAssessmentIndex(e.target.value as number)
                              }
                            }
                          >
                            <MenuItem value="-1">Assessments</MenuItem>
                            {attemptAssessmentData.map((each, index) => (
                              <MenuItem key={index} value={index}>
                                {each.assessment.assessmentname} - {each.assessment.boardname} {each.assessment.classname} {each.assessment.subjectname}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Box>
                    }

                    {!mythismark ? '' :
                      <Grid container direction="row"
                        alignItems="center"
                        justify="center"
                        style={{ height: '100%', padding:"0 8px",  marginTop: '15px'}}
                      >
                      <Grid item xs={12} md={8}>
                        <Typography style={{color: '#3D3D3D'}}>
                          Score
                        </Typography>
                      </Grid>
                      <Grid item xs={12} md={4}>
                        <Typography style={{color: '#3D3D3D', float: 'right'}}>
                          {mythismark}/{totalMarks}
                        </Typography>
                      </Grid>
                      </Grid>
                    }
                  </Box>
                    
                  <Box
                    bgcolor="#FFFFFF"
                    color="#3D3D3D"
                    borderRadius="3px"
                    paddingY="10px"
                    marginTop="15px"
                  >
                    <Box display="flex" alignItems="center">
                      <Box marginTop="7px" paddingX="20px">
                        <Box>
                          <img
                            src={attendance}
                            alt="classHistory"
                          />
                        </Box>
                      </Box>
                      <Box marginTop="7px" marginLeft="7px" width="100%">
                        <Typography style={{fontWeight: fontOptions.weight.bold}}>Attendance</Typography>
                          {overAllAttendanceData.map(list => {
                            return (
                              <Grid key={list.id} container style={{paddingTop: '15px'}}>
                                <Grid item xs={6}>
                                  <Typography>{String(list.subject)}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                  <Typography>{String(list.overallAttendance) + ' %'}</Typography>
                                </Grid>
                              </Grid>
                            )
                          })}
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              </Grid>

            </Grid>

            <Grid item xs={12} md={4}>
              <Box paddingX="15px">
                <Grid container alignItems="center" alignContent="center" style={{marginBottom: '10px'}}>
                  <Grid item xs={12}>
                    <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Timetable
                    </Typography>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"7px" }}>
                    <Scrollbars autoHeight autoHeightMax="135px"
                      style={{
                        backgroundColor: "white",
                        borderRadius: '10px',
                        color: '#3D3D3D'
                      }}
                    >
                      {batchList.length < 1 && 
                        <Box padding="10px">
                          <Typography style={{color: '#3D3D3D'}}>
                            No Timetable to show
                          </Typography>
                        </Box>
                      }
                      {(batchList.length > 0) && batchList.map(bat => {
                        return (
                          <Box padding="10px">
                            <Grid container direction="row"
                              alignItems="center"
                              justify="center"
                              style={{ height: '100%' }}
                            >
                              <Grid item xs={12} md={8}>
                                <Typography style={{color: '#3D3D3D'}}>
                                  {bat.boardname} - {bat.classname} - {bat.subjectname}
                                </Typography>
                                <Typography style={{color: 'rgba(0, 0, 0, 0.3)'}}>
                                  {bat.batchfriendlyname}
                                </Typography>
                              </Grid>
                              <Grid item xs={12} md={4}>
                                <Link style={{float: 'right', paddingRight: '10px', cursor: 'pointer'}}
                                  component={RouterLink}
                                  to={`/profile/schedules?batchId=${bat._id as string}`} 
                                >
                                  View
                                </Link>
                              </Grid>
                            </Grid>
                          </Box>
                        )
                      })}
                    </Scrollbars>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Syllabus
                    </Typography>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"7px" }}>
                    <Scrollbars autoHeight autoHeightMax="135px"
                      style={{
                        backgroundColor: "white",
                        borderRadius: '10px',
                        color: '#3D3D3D'
                      }}
                    >
                      {batchList.length < 1 && 
                        <Box padding="10px">
                          <Typography style={{color: '#3D3D3D'}}>
                            No Syllabus to show
                          </Typography>
                        </Box>
                      }
                      {(batchList.length > 0) && batchList.map(bat => {
                        return (
                          <Box padding="10px">
                            <Grid container direction="row"
                              alignItems="center"
                              justify="center"
                              style={{ height: '100%' }}
                            >
                              <Grid item xs={12} md={8}>
                                <Typography style={{color: '#3D3D3D'}}>
                                  {bat.boardname} - {bat.classname} - {bat.subjectname}
                                </Typography>
                                <Typography style={{color: 'rgba(0, 0, 0, 0.3)'}}>
                                  {bat.batchfriendlyname}
                                </Typography>
                              </Grid>
                              <Grid item xs={12} md={4}>
                                <Link style={{float: 'right', paddingRight: '10px', cursor: 'pointer'}}
                                  onClick={() => downloadSyllabus(bat)}
                                >
                                  Download
                                </Link>
                              </Grid>
                            </Grid>
                          </Box>
                        )
                      })}
                    </Scrollbars>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Box
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      height="60px"
                    >
                      <Box display="flex" alignItems="center">
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={ClassHistory}
                              alt="classHistory"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography style={{fontWeight: fontOptions.weight.bold}}>Class History</Typography>
                          <Link component={RouterLink}
                            to={`/meetings/dashboard`}
                          >
                            View
                          </Link>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Box
                      bgcolor="#ffffff"
                      borderRadius="5px"
                      display="flex"
                      alignItems="center"
                      padding="15px"
                      marginTop='20px'
                      minHeight='150px'
                    >
                      <Box
                        display="flex"
                        flexDirection='Column'
                        justifyContent='space-between'
                        alignItems="center"
                        paddingY="10px"
                        width='70%'
                        minHeight='140px'
                      >
                        <Box className={classes.usageBox}>
                          <Box className={classes.boxText}>Learn even More !</Box>
                          <Box className={classes.subtexts} marginTop="20px">
                            Classes related Information will be come here
                          </Box>
                          <Button 
                            variant="contained" color="primary" 
                            style={{width: '150px', marginTop: '15px'}}
                            onClick={() => setRedirectTo(`/profile/batches`)}
                          >
                            More Details
                          </Button>
                        </Box>
                      </Box>
                      <Box
                        borderRadius="5px"
                        color="#ffffff"
                        display="flex"
                        alignItems="center"
                        justifyContent='center'
                        padding="15px"
                        width='30%'
                        minHeight='140px'
                      >
                        <Box><img
                          style={{height:"100px"}}
                          src={knowledgeHover}
                          alt="Course"
                          onClick={() => ''}
                        /></Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>


          {/* <Grid container style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Grid item xs={12} md={9}>
              <Box paddingX="15px" marginRight='15px'>
                <Box
                  bgcolor="#4C8BF5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                >
                  <Grid item container>
                    <Grid item md={9}>
                      <Box>
                        <Typography className={styles.welcomeNote}>
                          Hello {profile.studentName}!
                        </Typography>
                        <Typography>
                          It's good to see you again.
                        </Typography>
                      </Box>
                    </Grid>

                    <Grid item md={3}>
                      <Box>
                        <img
                          className={classes.boyWavingHard}
                          src={GraduatedStudent}
                          alt={`Hello ${profile.studentName}!`}
                        />
                      </Box>
                    </Grid>
                  </Grid>
                </Box>

                <Typography variant="h6" className={styles.heading}>
                  Upcoming Classes
                </Typography>

                <Box display="flex" margin="30px 0 15px 0">
                  <Box marginRight="20px" className={styles.selectedClass}>
                    <MuButton
                      size="small"
                      onClick={() => {
                        setDashboardObject('courses');
                        setSubject('course');
                      }}
                    >
                      All Courses
                    </MuButton>
                  </Box>
                </Box>
              </Box>

              <Box display="flex" marginTop="30px" marginRight='15px'>
                <Box>
                  {dashboardObject === 'assessments' && (
                    <Grid container>
                      {attemptAssessments.map((el, index) => {
                        return (
                          <Grid item md={6} lg={6} sm={6} xs={6} key={index}>
                            <Box marginTop="10px" marginLeft="10px">
                              <SimpleCard data={el} profile={profile} />
                            </Box>
                          </Grid>
                        );
                      })}
                    </Grid>
                  )}

                  {dashboardObject === 'courses' &&
                    filteredSchedules.slice(0, 5).map((item, index) => (
                      <UpcomingCourseBlock key={index} item={item} />
                    ))
                  }
                </Box>
              </Box>

            </Grid>

            <Grid item xs={12} md={3}>
              <Box>

                <Grid container spacing={4} alignItems="center" alignContent="center" style={{ marginBottom: '10px', textAlign: 'center' }}>
                  <Grid item xs={6}>
                    <Box
                      bgcolor="#00B9F5"
                      color="#ffffff"
                      boxShadow="0px 2px 6px gray"
                      borderRadius="3px"
                      height="85px"
                      alignItems="center"
                    >
                      <Box className={styles.navItem}>
                        <img src={Syllabus} alt="Syllabus" />
                        <Typography>Syllabus</Typography>
                      </Box>
                      <Box className={styles.navLinks}>
                        <Link>View</Link>
                        <Link>Download</Link>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={6}>
                    <Box
                      bgcolor="#4C8BF5"
                      borderRadius="3px"
                      color="#ffffff"
                      boxShadow="0px 2px 6px gray"
                      height="85px"
                      alignItems="center"
                    >
                      <Box className={styles.navItem}>
                        <img src={Clock} alt="Clock" />
                        <Typography>Time Table</Typography>
                      </Box>
                      <Box className={styles.navLinks}>
                        <Link>View</Link>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>Meeting History</Box>
                      <Box className={classes.subtexts} marginTop="20px">View previous live classes</Box>
                      <Box className={classes.subtexts}>Class History | Attendance | Recording</Box>
                    </Box>
                    <Box width='100%'><MuButton
                      variant="contained"
                      size="small"
                      component={RouterLink}
                      to={`/meetings/dashboard`}
                      color="primary"
                    >
                      Meeting Dashboard
                      </MuButton></Box>
                  </Box>
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{ height: "100px" }}
                      src={Meeting_History}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>My Analytics</Box>
                      <Box className={classes.subtexts} marginTop="20px">View all student related analytics</Box>
                      <Box className={classes.subtexts}>Assessment | Assignments | Attendance</Box>
                    </Box>
                    <Box width='100%'><MuButton
                      variant="contained"
                      size="small"
                      component={RouterLink}
                      to={`/profile/analytics`}
                      color="primary"
                    >
                      Analytics Dashboard
                      </MuButton></Box>
                  </Box>
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{ height: "100px" }}
                      src={Analytics}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='130px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    width='70%'
                    minHeight='130px'
                  >
                    <Box width='100%'>
                      <Link href="#" className={classes.boxText}>
                        My Payment
                   </Link>
                    </Box>
                    <Box className={classes.paymentBox}
                    >
                      <Box>
                        <Link component={RouterLink} to={`/payments/feestructure`} >
                          <Typography className={classes.subtexts}>Payment History</Typography>
                        </Link>
                      </Box>
                      <Box>
                        <Link component={RouterLink} to={`/payments/feestructure`} >
                          <Typography className={classes.subtexts}>Add Fee Scedule</Typography>
                        </Link>
                      </Box>
                    </Box>
                  </Box>
                  <Box
                    bgcolor="#37DBFF"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      src={DashPay}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

               
                <Box
                  display="flex"
                  bgcolor="#F5F5F7"
                  borderRadius="14px"
                  padding="20px"
                >
                  <Box className={styles.learnMore}>
                    <Typography variant="h5">Learn even more!</Typography>
                    <Typography>
                      Classes related Information will be come here
                    </Typography>
                    <Box className={styles.btnYellow}>
                      <Button>More Details</Button>
                    </Box>
                  </Box>
                  <Box>
                    <img src={Knowledge} alt="Knowledge" />
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
         */}
        </Container>
      </Box>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default withStyles(styles)(connect(mapStateToProps)(StudentDashboard));
