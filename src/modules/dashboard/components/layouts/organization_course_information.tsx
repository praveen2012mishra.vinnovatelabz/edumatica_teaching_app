import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Organization } from '../../../common/contracts/user';
import { updateOrganization } from '../../../common/api/organization';
import Subject from '../../../../assets/images/course-book.png';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../organization_layout';
import OrganizationCourseInformationModal from '../modals/organization_course_information_modal';
import { BoardClassSubjectsMap } from '../../../academics/contracts/board_class_subjects_map';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#212121'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    yellowBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #4285F4',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#4285F4',
        padding: '7px 15px'
      }
    },
    subHeading: {
      fontWeight: fontOptions.weight.bold,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#333333'
    },
    courseHeading: {
      fontWeight: fontOptions.weight.bolder,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '2px',
      textTransform: 'uppercase',
      marginBottom: '10px'
    },
    courseSubHeading: {
      fontWeight: fontOptions.weight.normal,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '15px',
      color: '#000000',
      paddingRight:'2em'
    },
    standard: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#405169'
    },
    subjects: {
      fontFamily:fontOptions.family,fontSize: '16px',
      lineHeight: '19px',
      color: '#405169'
    },
    tableContainer: {
      width: 'auto',
      border: '0.5px solid #E3E3E3',

      '& th': {
        border: '0.5px solid #E3E3E3'
      }
    }
  })
);

interface Props {
  profile: Organization;
  profileUpdated: (user: Organization) => any;
}

const OrganizationCourseInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Organization Subject Info', 'Landed Organization Subject Info');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const saveSubjectInformation = async (data: Organization) => {
    const user = Object.assign({}, profile, data);

    try {
      console.log(user)
      await updateOrganization(user);
      eventTracker(GAEVENTCAT.profile, 'Organization Subject Info', 'Completed Organization Subject Info Edit');
      profileUpdated(user);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  const courses = [...profile.courseDetails];

  const boardClassSubjectsMap: BoardClassSubjectsMap[] = [];

  for (let i = 0; i < courses.length; ++i) {
    const subject = courses[i];

    let boardClassSubjectIndex = boardClassSubjectsMap.findIndex(
      (boardClassSubject) =>
        boardClassSubject.boardname.toLowerCase() ===
          subject.board.toLowerCase() &&
        boardClassSubject.classname.toString().toLowerCase() ===
          subject.className.toString().toLowerCase()
    );

    if (boardClassSubjectIndex === -1) {
      boardClassSubjectsMap.push({
        boardname: subject.board,
        classname: subject.className,
        subjects: []
      });

      boardClassSubjectIndex = boardClassSubjectsMap.length - 1;
    }

    boardClassSubjectsMap[boardClassSubjectIndex].subjects.push(
      subject.subject
    );
  }

  const boards = Array.from(new Set(courses.map((course) => course.board)));

  return (
    <Layout profile={profile as Organization}>
      <OrganizationCourseInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={saveSubjectInformation}
        user={profile as Organization}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={Subject} alt="Course Details" />

                <Box marginLeft="15px">
                  <Typography component="span" color="primary">
                    <Box className={classes.profileheading}>
                      Course Details
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View &amp; Edit Your Class &amp; Subjects
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box padding="20px 0px 20px 40px">
          <Box
            marginBottom="20px"
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <Box className={classes.subHeading}>Courses</Box>
            <Box className={classes.yellowBtn}>
              <Button variant="outlined" onClick={() => {
                  eventTracker(GAEVENTCAT.profile, 'Organization Subject Info', 'Start Organization Subject Info Edit');
                  setOpenModal(true)
                }}
              >
                <Box display="flex" alignItems="center">
                  <Box component="span" display="flex" marginRight="10px">
                    <AddIcon fontSize="small" />
                  </Box>
                  Add
                </Box>
              </Button>
            </Box>
          </Box>
          {boards &&
            boards.length > 0 &&
            boards.map((board, boardIndex) => (
              <Box marginBottom="20px" key={boardIndex}>
                <Typography
                  variant="h5"
                  color="primary"
                  className={classes.courseHeading}
                >
                  {board}
                </Typography>

                <TableContainer>
                  <Table className={classes.tableContainer}>
                    <TableHead>
                      <TableRow>
                        <TableCell className={classes.courseSubHeading}>
                          Classes
                        </TableCell>
                        <TableCell className={classes.courseSubHeading}>
                          Subjects
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>
                      {boardClassSubjectsMap
                        .filter(
                          (boardClassSubjectMap) =>
                            boardClassSubjectMap.boardname === board
                        )
                        .map((boardClassSubjectMap, index) => (
                          <TableRow key={index}>
                            <TableCell
                              component="th"
                              scope="row"
                              className={classes.standard}
                            >
                              {boardClassSubjectMap.classname}
                            </TableCell>
                            <TableCell className={classes.subjects}>
                              {boardClassSubjectMap.subjects.join(', ')}
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Box>
            ))}
        </Box>
      </Box>
    </Layout>
  );
};

export default OrganizationCourseInformation;
