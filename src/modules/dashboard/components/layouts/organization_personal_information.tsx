import React, { FunctionComponent, useState } from 'react';
import { Box, Grid, IconButton, Input, TextField, Tooltip, Typography } from '@material-ui/core';
import {
  AccountCircle as AccountCircleIcon,
  Create as CreateIcon,
  Subscriptions as SubscriptionIcon,
  LocationCity as LocationIcon,
  Smartphone as SmartPhoneIcon,
  Email as EmailIcon
} from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Organization } from '../../../common/contracts/user';
import { updateOrganization } from '../../../common/api/organization';
import ProfileEditIcon from '../../../../assets/svgs/profile-edit.svg';
import Button from '../../../common/components/form_elements/button';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import Layout from '../organization_layout';
import OrganizationPersonalInformationModal from '../modals/organization_personal_information_modal';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useSnackbar } from 'notistack';
import { registerReferralCode } from '../../../payments/helper/api';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    imageBox: {
      width: '1.1em',
      height: '1.1em',
      marginRight: '.7em'
    },
    profileContainer: {
      background: '#fff',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px'
    },
    profileSection: {
      borderBottom: '0.5px solid #E3E3E3',
      padding: '20px 0px 10px 40px'
    },
    profileheading: {
      fontWeight: fontOptions.weight.bold,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#010101'
    },
    helperText: {
      fontWeight: fontOptions.weight.light,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#606A7B'
    },
    blueBtn: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginRight: '40px',
      '& button': {
        border: '2px solid #659BF6',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bolder,
        fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#659BF6',
        padding: '7px 15px'
      }
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      color: '#606A7B',
      marginBottom: '12px',
      display:'flex',
      alignItems:'center'
    },
    inputValueContainer: {
      marginBottom: '20px',
      display: 'flex',
      alignItems: 'center'
    },
    inputValue: {
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '18px',
      color: '#151522',
      marginLeft: '30px'
    },
    infoHead: {
      marginTop: '1px',
      marginLeft: '5px' 
    }
  })
);

interface Props {
  profile: Organization;
  profileUpdated: (user: Organization) => any;
}

const OrganizationPersonalInformation: FunctionComponent<Props> = ({
  profile,
  profileUpdated
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [openModal, setOpenModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const [referral, setReferral] = useState('')

  const classes = useStyles();
  eventTracker(GAEVENTCAT.profile, 'Organization Personal Info', 'Landed Organization Personal Info');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const savePersonalInformation = async (data: Organization) => {
    const user = Object.assign({}, profile, data);

    try {
      profileUpdated(user);
      await updateOrganization(user);
      eventTracker(GAEVENTCAT.profile, 'Organization Personal Info', 'Completed Organization Personal Info Edit');
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        profileUpdated(profile);
      }
    }
  };

  const handleReferral = () => {
    if(referral === '') {
      enqueueSnackbar('Please enter a referral code', {
        variant: 'error'
      });
    }else {
      registerReferralCode("Organization", referral).then(response => {
        if(response.code === 0) {
          enqueueSnackbar('Successfully registered referral code', {
            variant: 'success'
          });
        }
      })
    }
  }

  return (
    <Layout profile={profile}>
      <OrganizationPersonalInformationModal
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        saveUser={savePersonalInformation}
        user={profile}
      />

      <Box className={classes.profileContainer}>
        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={8}>
              <Box display="flex" alignItems="center" marginBottom="20px">
                <img src={ProfileEditIcon} alt="Personal Info" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box className={classes.profileheading}>
                      Personal Information
                    </Box>
                  </Typography>

                  <Typography className={classes.helperText}>
                    View and Edit Your Personal &amp; Contact Details
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} sm={6} md={4}>
              <Box className={classes.blueBtn}>
                <Button variant="outlined" onClick={() => {
                  eventTracker(GAEVENTCAT.profile, 'Organization Personal Info', 'Start Organization Personal Info Edit');
                  setOpenModal(true)
                  }}
                >
                  <Box display="flex" alignItems="center">
                    Edit
                    <Box component="span" display="flex" marginLeft="10px">
                      <CreateIcon fontSize="small" />
                    </Box>
                  </Box>
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <AccountCircleIcon color="primary" />
                <Typography className={classes.infoHead}>Institute Name</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.organizationName}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <EmailIcon color="primary" />
                <Typography className={classes.infoHead}>Email Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.emailId ? profile.emailId : '-'}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SmartPhoneIcon color="primary" />
                <Typography className={classes.infoHead}>Phone Number</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.mobileNo}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <SubscriptionIcon color="primary" />
                <Typography className={classes.infoHead}>Package</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.package ? profile.package.name : '-'}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              <Box className={classes.label}>
                <LocationIcon color="primary" />
                <Typography className={classes.infoHead}>Address</Typography>
              </Box>
              <Box className={classes.inputValueContainer}>
                <Box className={classes.inputValue}>
                  {profile.address}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box className={classes.profileSection}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              {!profile.userReferralCode ? '' :
                <>
                <Box className={classes.label}>
                  <CardMembershipIcon color="primary" />
                  <Typography className={classes.infoHead}>Referral Code</Typography>
                </Box>
                <Box className={classes.inputValueContainer}>
                  <Box style={{display:"flex", alignItems:"center", columnGap:"8px"}} className={classes.inputValue}>
                    <Input defaultValue={profile.userReferralCode} disabled inputProps={{ 'aria-label': 'description' }} />
                    <CopyToClipboard text={profile.userReferralCode}>
                      <Tooltip title="Copy">
                        <IconButton>
                          <FileCopyIcon/>
                        </IconButton>
                      </Tooltip>
                    </CopyToClipboard>
                  </Box>
                </Box>
                </>
              }
            </Grid>
            <Grid item xs={12} md={6}>
              {
                profile.isReferralCodeUsed ? '' :
                <>
                  <Box style={{display:"flex", alignItems:"center", columnGap:"8px"}} className={classes.inputValue}>
                    <TextField
                      size="small"
                      onChange={(e)=> {setReferral(e.target.value)}}
                      id="outlined-secondary"
                      label="Use a referral code"
                      variant="outlined"
                      color="secondary"
                    />
                      <Tooltip title="Use">
                        <IconButton onClick={() => handleReferral()}>
                          <AddCircleIcon/>
                        </IconButton>
                      </Tooltip>
                  </Box>
                </>
              }
            </Grid>
          </Grid>
        </Box>

      </Box>
    </Layout>
  );
};

export default OrganizationPersonalInformation;
