import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  Box,
  Button as MuButton,
  Container,
  Avatar,
  Grid,
  Typography,
  Link,
  LinearProgress,
  FormControl,
  Select
} from '@material-ui/core';
import { AssessmentAnswers } from '../../../student_assessment/contracts/assessment_answers';
import { attemptedAssessmentAnswers, attemptedAssessmentData } from '../../../analytics/helpers/api';
import { fetchStudentBatchesList } from '../../../common/api/batch';
import { Student, User } from '../../../common/contracts/user';
import Scrollbars from 'react-custom-scrollbars';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Link as RouterLink } from 'react-router-dom';
import Button from '../../../common/components/form_elements/button';
import DashCap from '../../../../assets/svgs/dashCap.svg';
import ClassHistory from '../../../../assets/svgs/classHistory.svg'
import knowledgeHover from '../../../../assets/svgs/knowledgeHover.svg'
import {useSnackbar} from "notistack"
import { Meeting } from '../../../bbbconference/contracts/meeting_interface';
import { BBBEvents } from '../../../bbbconference/contracts/bbbevent_interface';
import { AttendanceOption } from '../../../bbbconference/enums/attendance_options';

import { fetchBBBBatches, getPostIndividualMeetingEventsInfo, getPostMeetingEventsInfo } from '../../../bbbconference/helper/api';
import {
  CalendarViewDayRounded as CalendarIcon,
  WatchLater as ClockIcon
} from '@material-ui/icons';
import { connect } from 'react-redux';
import { createStyles, withStyles, WithStyles, makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { fetchParentSchedulesList, fetchBatchesList,fetchDownloadUrlForSyllabus } from '../../../common/api/academics';
import { Parent, Children } from '../../../common/contracts/user';
import { AppointmentSchedule } from '../../../academics/contracts/schedule';
import BoyWavingHand from '../../../../assets/svgs/boy-waving-hand.svg';
import CourseBlue from '../../../../assets/svgs/course-blue.svg';
import MiniDrawer from '../../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { generateSchedulerSchema } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import { fontOptions } from '../../../../theme';
import Knowledge from '../../../../assets/images/knowledge.png';
import Syllabus from '../../../../assets/images/syllabus.png';
import Clock from '../../../../assets/images/clock.png';
import { RootState } from '../../../../store';
import { useHistory } from 'react-router-dom';
import Analytics from '../../../../assets/images/analytics_dashboard.png';
import Meeting_History from '../../../../assets/images/meeting_history.png';
import studentDashboardImage from '../../../../assets/svgs/studentDashboardImage.svg'
import { getAttemptAssessments } from '../../../student_assessment/helper/api';
import { OngoingAssessment } from '../../../student_assessment/contracts/assessment_interface';
import attendance from '../../../../assets/images/attendenceStudent.svg'
import CircularProgress from '@material-ui/core/CircularProgress';
import classHistory from '../../../../assets/images/classHistory.png'

const styles = createStyles({
  boyWavingHard: {
    maxHeight: '165px',
    position: 'absolute',
    bottom: 0,
    right: '50px'
  },
  root: {
    minWidth: 275
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
  },
  pos: {
    marginBottom: 12
  },
  usageBox: {
    width: '100%',
    minHeight: '90px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  boxText: {
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.bolder,
    lineHeight: '20px',
    color: '#3D3D3D',
  },
  subtexts: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal
  },
});

const useStyles = makeStyles({
  learnText: {
    fontSize: fontOptions.size.medium,
    lineHeight: '18px',
    color: '#000',
    fontWeight: fontOptions.weight.bold,
    paddingBottom: '10px'
  },
  classesText: {
    fontSize: fontOptions.size.small,
    lineHeight: '18px',
    color: 'rgba(0, 0, 0, 0.3)',
    paddingBottom: '10px'
  },
  formControl: {
    minWidth: 120,
  },
  subjectText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#000'
  },
  chapterName: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.normal,
    color: 'rgba(0, 0, 0, 0.3)'
  },
  syllabusBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  topicText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
  },
  topicTextContent: {
    fontSize: fontOptions.size.small,
    lineHeight: '18px',
    color: '#3D3D3D'
  },
  topicBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    //marginBottom: '25px',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  attendenceBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
    margin: '25px 0',
    padding: '10px',
    display: 'flex',
    alignItems: 'center'
  },
  scoreText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '20px',
    fontFamily: fontOptions.family,
    color: '#000',
  },
  scoreBox: {
    borderRadius: '5px',
    background: '#fff',
    minHeight: '50px',
  },
  allClassesText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#666666'
  },
  selectedClassText: {
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.bold,
    color: '#4C8BF5'
  },
  welcomeText: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    marginBottom: '10px'
  },
  root: {
    minWidth: 275,
    fontFamily: fontOptions.family,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
    fontFamily: fontOptions.family,
  },
  title: {
    fontSize: 14,
    fontFamily: fontOptions.family,
  },
  pos: {
    marginBottom: 12,

  },
  welcomeNote: {
    fontSize: fontOptions.size.large,
    fontWeight: fontOptions.weight.bold,
    lineHeight: '37px',
    color: '#fff',
    marginBottom: '10px'
  },
  helperText: {
    color: '#FFFFFF',
    fontFamily: fontOptions.family
  },
  heading: {
    fontFamily:fontOptions.family,fontSize: fontOptions.size.medium,
    lineHeight: '26px',
    color: '#4E4E4E',
    marginTop: '30px'
  },
  allClasses: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: 'rgba(0,0,0,0.3)'
  },
  noSched: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    fontWeight: fontOptions.weight.normal,
    color: '#666666',
    margin: '5px'
  },
  selectedClass: {
    fontWeight: 500,
    fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
    lineHeight: '19px',
    color: '#4C8BF5'
  },
  courseImage: {
    background: '#FBFAF9',
    borderRadius: '12px', fontFamily: fontOptions.family,
    padding: '12px 13px'
  },
  courseName: {
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.small,
    lineHeight: '19px',
    fontFamily: fontOptions.family,
    color: '#405169',
    marginBottom: '8px'
  },
  batchName: {
    fontSize: fontOptions.size.small,
    lineHeight: '15px',
    fontFamily: fontOptions.family,
    color: '#000000'
  },
  courseDetails: {
    fontSize: fontOptions.size.small,
    fontFamily: fontOptions.family,
    color: '#000000'
  },
  startBtn: {
    '& a': {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  },

  boxHeading: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#212121',
    marginBottom: '10px'
  },
  boxNav: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
    color: '#4C8BF5'
  },
  topicsHeading: {
    fontSize: fontOptions.size.medium,
    lineHeight: '21px',
  },
  subhead: {
    marginTop: '40px',
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.medium,
    fontFamily: fontOptions.family,
    color: '#666666'
  },
  subvalues: {
    fontWeight: fontOptions.weight.normal,
    fontSize: fontOptions.size.medium,
    fontFamily: fontOptions.family,
    color: '#404040'
  },
  topicsList: {
    fontSize: fontOptions.size.small,
    lineHeight: '16px',
    color: '#000000'
  },
  halfOpacity: {
    opacity: '0.5'
  },
  thirdOpacity: {
    opacity: '0.75'
  },
  progressContainer: {
    width: '185px',

    '& p': {
      marginBottom: '10px'
    },
    '& span': {
      fontSize: fontOptions.size.small,
      color: '#333333',
      textAlign: 'center',
      paddingTop: '5px',
      display: 'block'
    }
  },
  commonBox: {
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: '5px',
    textAlign: 'center',
    width: '185px'
  },
  commonBoxO: {
    backgroundColor: '#4C8BF5',
    color: 'white',
    padding: '9px'
  },
  commonBoxT: {
    backgroundColor: 'white',
    padding: '16px',
    color: '#666666'
  },
  navItem: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '8px',
    marginBottom: '13px',
    fontFamily: fontOptions.family,
    '& p': {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: 'black',
      marginLeft: '10px'
    }
  },
  navLinks: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: fontOptions.family,
    '& a': {
      fontFamily: fontOptions.family,
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      color: 'black',
      marginRight: '14px'
    }
  },
  studentProgressContainer: {
    '& a': {
      display: 'inline-block',
      marginTop: '10px',
      marginLeft: '25px'
    }
  },
  studentProgress: {
    textAlign: 'center',
    flex: 1,

    '& h3': {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '21px',
      color: '#212121',
      marginBottom: '10px'
    },
    '& h4': {
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '28px',
      color: '#212121',
      width: '37%',
      margin: '0 auto',
      marginTop: '5px'
    },
    '& h5': {
      fontSize: fontOptions.size.medium, fontFamily: fontOptions.family,
      lineHeight: '21px',
      color: '#212121',
      marginTop: '5px'
    }
  },
  learnMore: {
    '& h5': {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: '#000000',
      marginBottom: '12px'
    },
    '& p': {
      fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
      lineHeight: '24px',
      color: '#000000',
      marginBottom: '15px',
      width: '70%'
    }
  },
  btnYellow: {
    '& button': {
      background: '#F9BD33',
      borderRadius: '8px',
      padding: '12px 24px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small, fontFamily: fontOptions.family,
      lineHeight: '15px',
      color: '#FFFFFF'
    }
  }
});

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 10,
    border: '1px solid #00B9F5'
  },
  colorPrimary: {
    backgroundColor: '#fff'
  },
  bar: {
    borderRadius: 10,
    backgroundColor: '#4C8BF5'
    // margin: '1px 0'
  }
}))(LinearProgress);

interface UpcomingCourseBlockProps {
  item: AppointmentSchedule;
}

function CircularProgressWithLabel(props: any) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" {...props} style={{ width: '70px', height: '70px', padding: '5px' }} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="textSecondary">
          30/42 <br />
         Hour
        </Typography>
      </Box>
    </Box>
  );
}

const UpcomingCourseBlock: FunctionComponent<UpcomingCourseBlockProps> = ({
  item
}) => {
  const [isActive, setIsActive] = useState(true);
  const styles = useStyles();

  useEffect(() => {
    const dt = new Date();
    dt.setHours( dt.getHours() + 2 );
    if(new Date(item.startDate) >= dt) {
      setIsActive(false)
    }
  }, [item]);
  const CourseIcon = () => {
    // if (isActive) {
    return (
      <Box marginRight="20px">
        <img src={CourseBlue} alt="Course" />
      </Box>
    );
    // }

    // return (
    //   <Box marginRight="20px">
    //     <img src={CourseOrange} alt="Course" />
    //   </Box>
    // );
  };

  const scheduleSessionStart = moment(item.startDate);

  return (
    <Box
      bgcolor="white"
      borderRadius="3px"
      marginTop="16px"
      padding="8px 20px 8px 8px"
    >
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <Box>
            <img
              src={DashCap}
              alt="Course"
              style={{paddingTop: '20px'}}
            />
          </Box>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="subtitle2" className={styles.courseName} style={{fontWeight: fontOptions.weight.bold}}>
            {item.schedule.batch && item.schedule.batch.classname} -{' '}
            {item.schedule.batch && item.schedule.batch.subjectname}
          </Typography>
          <Typography variant="subtitle1" className={styles.batchName}>
            {item.schedule.batch && item.schedule.batch.batchfriendlyname},{' '}
            {item.schedule.batch && item.schedule.batch.boardname}
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="subtitle2" className={styles.courseName} style={{color: '#3D3D3D'}}>
            {scheduleSessionStart.format('hh:mm A')}
          </Typography>
          <Typography variant="subtitle1" className={styles.batchName} style={{color: '#666666'}}>
            {(new Date(scheduleSessionStart.format('YYYY-MM-DD')).toLocaleDateString() === new Date().toLocaleDateString()) ?
              'Today' : scheduleSessionStart.format('DD/MM/YYYY')
            }
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

interface Props extends WithStyles<typeof styles> {
  profile: Parent;
  authUser: User;
  parentChildren: Children;
}

export interface Schedule {
  _id?: string;
  ownerId?: string;
  tutorId?: Tutor;
  tutor?: string;
  mobileNo?: string;
  dayname: string;
  fromhour: string;
  tohour: string;
  batch?: Batch;
}

export interface Tutor {
  _id?: string;
}

interface Batch {
  _id?: string;
  schedules?: Schedule[];
  content?: string[];
  students: string[];
  boardname: string;
  classname: string;
  subjectname: string;
  batchfriendlyname: string;
  batchenddate: string;
  batchstartdate: string;
  batchicon: string;
  tutor: string;
  tutorId?: Tutor;
  syllabus?: string;
}

export interface AssessmentDetail {
  assessmentname: string;
  boardname: string;
  classname: string;
  duration: number;
  instructions: string;
  ownerId: string;
  sections: string[];
  status: number;
  subjectname: string;
  totalMarks: number;
  updatedby: string;
  updatedon: string;
  userId: string;
  marks?: string;
  _id?: string;
}

const isGetSelectedAssessmentRes = (x: any): x is AssessmentDetailres => x.assessment

interface AttendanceData {
  id: number,
  subject: string,
  overallAttendance: number
}

interface AssessmentDetailres {
  assessment: AssessmentDetail;
  isSubmitted?: boolean;
  _id: string;
}

interface selectedAssessmentType {
  answers: any;
  assessment: AssessmentDetail;
  assessmentData: any;
  attemptedQuestions: any;
  currentQuestion: number;
  endDate: string;
  endTime: string;
  isStarted: boolean;
  isSubmitted?: boolean;
  ownerId: string;
  solutionTime: string;
  startDate: string;
  studentId: string;
  updatedby: string;
  updatedon: string;
  _id: string;
}

interface RedirectProp {
  pathname: string;
  state: {
    batchItem:any;
  }
}

const ParentDashboard: FunctionComponent<Props> = ({ classes, profile, parentChildren, authUser }) => {
  const [schedules, setSchedules] = useState<AppointmentSchedule[]>([]);
  const [subject, setSubject] = useState('');
  const history = useHistory();
  const [redirectTo, setRedirectTo] = useState('');
  const [getSelectedAssessment, setSelectedAssessment] = useState<selectedAssessmentType[]>([]);
  const [dashboardObject, setDashboardObject] = useState<string>('courses');
  const [subjectList, setSubjectList] = useState<string[]>([]);
  const styles = useStyles();
  const [batchList, setBatchList] = useState<Batch[]>([]);
  eventTracker(GAEVENTCAT.dashboard, 'Parent Dashboard', 'Landed Parent Dashboard');
  const [attemptAssessments, setAttemptAssessments] = useState<
    OngoingAssessment[]
  >([]);
  const [overAllAttendanceData, setOverAllAttendanceData] = useState<AttendanceData[]>([])
  const [spRedirectTo, setSpRedirectTo] = useState<RedirectProp>()
  const { enqueueSnackbar } = useSnackbar();
  const [attemptAssessmentData, setAttemptAssessmentData] = useState<
  OngoingAssessment[] | null
  >(null);
  const [activeAssessmentIndex, setActiveAssessmentIndex] = useState(-1)
  const [totalMarks, setTotalMarks] = useState<number>(0)
  const [mythismark, setMythismark] = useState<string>('')
  const [
    attemptAssessment,
    setAttemptAssessment
  ] = useState<OngoingAssessment | null>(null);

  useEffect(() => {
    loadSchedules()
    // eslint-disable-next-line
  }, [parentChildren, profile.mobileNo]);

  // const getDownloadSyllabus = async (id?: string, tutorId?: string) => {
  //   const awsBucket = await fetchDownloadUrlForSyllabus(id as string, tutorId as string);
  //   setTimeout(() => {
  //     const response = {
  //       file: awsBucket.url,
  //     };
  //     window.open(response.file);
  //   }, 100);
  // }

  function allAttendanceData(
    id: number,
    subject: string,
    overallAttendance: number
  ) {
    return { id, subject, overallAttendance };
  }

  const downloadSyllabus = async(batch: Batch) => {
    if(batch.syllabus) {
      const awsBucket = await fetchDownloadUrlForSyllabus(batch._id as string, batch.syllabus as string);
      setTimeout(() => {
        const response = {
          file: awsBucket.url,
        };
        window.open(response.file);
      }, 100);
    } else {
      enqueueSnackbar("Syllabus NOT available", {variant: 'warning'});
    }
  }

  useEffect(() => {
    const getList = async () => {
      if (parentChildren.current) {
        const schedulesList = await fetchParentSchedulesList(parentChildren.current);
        const schedulerSchema = generateSchedulerSchema(
          schedulesList
        ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));
        const setSubjects = schedulerSchema
          .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
          .filter((item) => true
          );
        const subjects: string[] = Array.from(
          new Set(setSubjects?.map((course) => course.subject))
        );
        setSubjectList(subjects)

        //get batches list
      }
    }
    getList();

  }, []);

  useEffect(() => {
    if (!authUser) return
    batchList?.map(batch => {
      // @ts-ignore
      getPostMeetingEventsInfo(batch._id).then((result) => {
        if (result.message !== 'Success') return
        const meetings: Meeting[] = result.data
        var noPresent = 0;
        var noAbsent = 0;
        meetings.forEach(eachClass => {
          getPostIndividualMeetingEventsInfo(eachClass.internalMeetingID).then((result) => {
            if (result.message !== 'Success') return
            var ispresent = AttendanceOption.IS_ABSENT;
            const individualMeetingInfo: Meeting = result.data;
            const events: BBBEvents[] = individualMeetingInfo.events;
            const userEvent = events.filter(function (item) {
              if(item.userID === parentChildren.current) {
                return item.type === 'user-joined' || item.type === 'user-left';
              }
            });
            let total_time = 0
            const meetingEndEvent = events.filter(function (items) {
              return items.type === 'meeting-ended';
            });
            var totalMeetingTime = meetingEndEvent[0].eventTriggerTime - individualMeetingInfo.createTime;
            userEvent.forEach(eachEvent => {
              if(eachEvent.type === "user-joined"){
                if(eachEvent.userID === undefined) return
                  if( parentChildren.current !== eachEvent.userID) {
                    ispresent = AttendanceOption.IS_ABSENT
                  }
              }
            })
            if(userEvent.length === 0){
              ispresent = AttendanceOption.IS_ABSENT
            }
            const length = userEvent.length
            if(userEvent.length !== 0 && (length % 2) !== 0) {
              for (let i=0; i<userEvent.length - 1; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              total_time = total_time + (meetingEndEvent[0].eventTriggerTime - userEvent[length-1].eventTriggerTime)
              const percentage = (total_time/totalMeetingTime)*100
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            if(userEvent.length !== 0 && (length % 2) === 0) {
              for (let i=0; i<userEvent.length; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              const percentage = (total_time/totalMeetingTime)*100;
              // eslint-disable-next-line
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            console.log(ispresent)
            if(ispresent === AttendanceOption.IS_PRESENT) {
              noPresent++
            } else{
              noAbsent++
            }
            if(meetings.indexOf(eachClass) === (meetings.length - 1)){
              setOverAllAttendanceData([...overAllAttendanceData,allAttendanceData(batchList.indexOf(batch)+1, batch.subjectname, (noPresent/(noPresent+noAbsent))*100)])
            }
          });
          // console.log(overAllAttendanceData)
        })
      });
    })
  }, [batchList])

  useEffect(() => {
    if(attemptAssessmentData && activeAssessmentIndex>=0) {
      setAttemptAssessment(attemptAssessmentData[activeAssessmentIndex])
      setTotalMarks(attemptAssessmentData[activeAssessmentIndex].assessment.totalMarks)
      getAnswers(attemptAssessmentData[activeAssessmentIndex]._id, attemptAssessmentData[activeAssessmentIndex])
    }
  }, [activeAssessmentIndex])

  const getAllAttemptAssessments = async () => {
    try {
      const response = await getAttemptAssessments();
      setAttemptAssessments(response);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        setRedirectTo('/login');
      }
    }
  };

  const getAnswers = (
    assessmentId: string,
    eachAssessment: OngoingAssessment
  ) => {
    if (!parentChildren.current) return;
    attemptedAssessmentAnswers(parentChildren.current, assessmentId)
      .then((result) => {
        let assessmentAnswers: AssessmentAnswers = result;
        let myMark = 0;
        assessmentAnswers.forEach((answer) => {
          answer.forEach((eachAnswer) => {
            myMark = myMark + Number(eachAnswer.marks)
          });
        });

        setMythismark(String(myMark))
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const loadSchedules = async () => {
    try {
      if (parentChildren.current) {
        const schedulesList = await fetchParentSchedulesList(parentChildren.current);
        const schedulerSchema = generateSchedulerSchema(
          schedulesList
        ).sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));

        const batchesListResponse = await fetchStudentBatchesList(parentChildren.current as string)
        setBatchList(batchesListResponse)

        const attemptedAssessmentDataResponse = await attemptedAssessmentData(
          parentChildren.current
        );
        const batchesListResponseBBB = await fetchBBBBatches();
        const [attemptedAssessment, batchesList] = await Promise.all([
          attemptedAssessmentDataResponse, batchesListResponseBBB
        ]);
        setAttemptAssessmentData(attemptedAssessment);

        setSchedules(schedulerSchema);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  }  

  //get option assessment
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (e: any, item: any) => {
    setSelectedAssessment(item)
    setAnchorEl(null);
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  //const subjects = Array.from(
  //  new Set(profile.courseDetails.map((course) => course.subject))
  //);

  // TODO: Filter schedules based on the upcoming date and time.
  const filteredSchedules = schedules
    .filter((item) => moment(item.startDate).isSameOrAfter(moment()))
    .filter((item) =>
      subject.length > 0
        ? item.schedule.batch &&
        item.schedule.batch.subjectname.toLowerCase() ===
        subject.toLowerCase()
        : true
    );

  return (
    <div>
      <MiniDrawer>

      <Box marginY="50px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={8}>
              <Box marginRight='15px'>
                <Box
                  bgcolor="#4C8BF5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                  color='#fff'
                >
                  <Grid item container>
                  <Grid item md={9}>
                      <Box>
                        <Typography style={{fontSize: fontOptions.size.medium}}>
                          Hello, {profile.parentName} !
                        </Typography>
                        <Typography className={styles.welcomeNote}>Welcome Back</Typography>
                      </Box>
                    </Grid>
                    <img
                      className={classes.boyWavingHard}
                      src={studentDashboardImage}
                      alt={`Hello ${profile.parentName}!`}
                    />

                    <Grid item md={3}>
                      <Box>
                        {/* <img
                          className={classes.boyWavingHard}
                          src={studentDashboardImage}
                          alt={`Hello ${profile.parentName}!`}
                        /> */}
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
              </Box>

              <Grid container>
                <Grid item xs={12} md={7}>
                  <Typography className={styles.heading}>
                    Upcoming Classes
                  </Typography>

                  <Box display="flex" margin="30px 0 15px 0">
                    <Box marginRight="20px" 
                    >
                      <MuButton
                        size="small"
                        onClick={() => {
                          setDashboardObject('courses');
                          setSubject('');
                        }}
                        className={subject === '' ? styles.selectedClassText : styles.allClassesText}
                      >
                        All Courses
                    </MuButton>
                    </Box>
                    {subjectList.map((thissubject, index) => (
                      <Box
                        key={index}
                        marginRight="20px"
                      >
                        <MuButton
                          size="small"
                          onClick={() => setSubject(thissubject)}
                          className={subject === thissubject ? styles.selectedClassText : styles.allClassesText}
                        >
                          {thissubject}
                        </MuButton>
                      </Box>
                    ))}
                  </Box>

                  {filteredSchedules.length > 0 && (
                    <Box>
                      {filteredSchedules.slice(0, 5).map((item, index) => (
                        <UpcomingCourseBlock key={index} item={item} />
                      ))}
                    </Box>
                  )}
                  {filteredSchedules.length == 0 && (
                    <Box>
                      <Typography className={styles.noSched}>
                        No Schedules in Selected Subject
                      </Typography>
                    </Box>
                  )}
                </Grid>
                <Grid item xs={12} md={5}>
                  <Typography className={styles.heading}>
                    Progress
                  </Typography>

                  <Box 
                    bgcolor="#FFFFFF"
                    color="#3D3D3D"
                    borderRadius="3px"
                    paddingY="10px"
                    marginTop="15px"
                  >
                    {
                      !attemptAssessmentData ? '' :
                      <Box padding="0 8px">
                        <FormControl className={styles.formControl}>
                          <Select
                            value={activeAssessmentIndex}
                            onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                              { 
                                setMythismark('')
                                setActiveAssessmentIndex(e.target.value as number)
                              }
                            }
                          >
                            <MenuItem value="-1">Assessments</MenuItem>
                            {attemptAssessmentData.map((each, index) => (
                              <MenuItem key={index} value={index}>
                                {each.assessment.assessmentname} - {each.assessment.boardname} {each.assessment.classname} {each.assessment.subjectname}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Box>
                    }

                    {!mythismark ? '' :
                      <Grid container direction="row"
                        alignItems="center"
                        justify="center"
                        style={{ height: '100%', padding:"0 8px",  marginTop: '15px'}}
                      >
                      <Grid item xs={12} md={8}>
                        <Typography style={{color: '#3D3D3D'}}>
                          Score
                        </Typography>
                      </Grid>
                      <Grid item xs={12} md={4}>
                        <Typography style={{color: '#3D3D3D', float: 'right'}}>
                          {mythismark}/{totalMarks}
                        </Typography>
                      </Grid>
                      </Grid>
                    }
                  </Box>

                  <Box
                    bgcolor="#FFFFFF"
                    color="#3D3D3D"
                    borderRadius="3px"
                    paddingY="10px"
                    marginTop="15px"
                  >
                    <Box display="flex" alignItems="center">
                      <Box marginTop="7px" paddingX="20px">
                        <Box>
                          <img
                            src={attendance}
                            alt="classHistory"
                          />
                        </Box>
                      </Box>
                      <Box marginTop="7px" marginLeft="7px" width="100%">
                        <Typography style={{fontWeight: fontOptions.weight.bold}}>Attendance</Typography>
                          {overAllAttendanceData.map(list => {
                            return (
                              <Grid key={list.id} container style={{paddingTop: '15px'}}>
                                <Grid item xs={6}>
                                  <Typography>{String(list.subject)}</Typography>
                                </Grid>
                                <Grid item xs={6}>
                                  <Typography>{String(list.overallAttendance) + ' %'}</Typography>
                                </Grid>
                              </Grid>
                            )
                          })}
                      </Box>
                    </Box>
                  </Box>
                </Grid>

              </Grid>

            </Grid>
            
            <Grid item xs={12} md={4}>
              <Box paddingX="15px">
                <Grid container alignItems="center" alignContent="center" style={{marginBottom: '10px'}}>
                  <Grid item xs={12}>
                    <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Timetable
                    </Typography>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"7px" }}>
                    <Scrollbars autoHeight autoHeightMax="135px"
                      style={{
                        backgroundColor: "white",
                        borderRadius: '10px',
                        color: '#3D3D3D'
                      }}
                    >
                      {batchList.length < 1 && 
                        <Box padding="10px">
                          <Typography style={{color: '#3D3D3D'}}>
                            No Timetable to show
                          </Typography>
                        </Box>
                      }
                      {(batchList.length > 0) && batchList.map(bat => {
                        return (
                          <Box padding="10px">
                            <Grid container direction="row"
                              alignItems="center"
                              justify="center"
                              style={{ height: '100%' }}
                            >
                              <Grid item xs={12} md={8}>
                                <Typography style={{color: '#3D3D3D'}}>
                                  {bat.boardname} - {bat.classname} - {bat.subjectname}
                                </Typography>
                                <Typography style={{color: 'rgba(0, 0, 0, 0.3)'}}>
                                  {bat.batchfriendlyname}
                                </Typography>
                              </Grid>
                              <Grid item xs={12} md={4}>
                                <Link style={{float: 'right', paddingRight: '10px', cursor: 'pointer'}}
                                  component={RouterLink}
                                  to={`/profile/schedules?batchId=${bat._id as string}`} 
                                >
                                  View
                                </Link>
                              </Grid>
                            </Grid>
                          </Box>
                        )
                      })}
                    </Scrollbars>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold}}>
                      Syllabus
                    </Typography>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"7px" }}>
                    <Scrollbars autoHeight autoHeightMax="135px"
                      style={{
                        backgroundColor: "white",
                        borderRadius: '10px',
                        color: '#3D3D3D'
                      }}
                    >
                      {batchList.length < 1 && 
                        <Box padding="10px">
                          <Typography style={{color: '#3D3D3D'}}>
                            No Syllabus to show
                          </Typography>
                        </Box>
                      }
                      {(batchList.length > 0) && batchList.map(bat => {
                        return (
                          <Box padding="10px">
                            <Grid container direction="row"
                              alignItems="center"
                              justify="center"
                              style={{ height: '100%' }}
                            >
                              <Grid item xs={12} md={8}>
                                <Typography style={{color: '#3D3D3D'}}>
                                  {bat.boardname} - {bat.classname} - {bat.subjectname}
                                </Typography>
                                <Typography style={{color: 'rgba(0, 0, 0, 0.3)'}}>
                                  {bat.batchfriendlyname}
                                </Typography>
                              </Grid>
                              <Grid item xs={12} md={4}>
                                <Link style={{float: 'right', paddingRight: '10px', cursor: 'pointer'}}
                                  onClick={() => downloadSyllabus(bat)}
                                >
                                  Download
                                </Link>
                              </Grid>
                            </Grid>
                          </Box>
                        )
                      })}
                    </Scrollbars>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Box
                      bgcolor="#FFFFFF"
                      color="#3D3D3D"
                      borderRadius="3px"
                      height="60px"
                    >
                      <Box display="flex" alignItems="center">
                        <Box marginTop="7px" paddingX="20px">
                          <Box>
                            <img
                              src={ClassHistory}
                              alt="classHistory"
                            />
                          </Box>
                        </Box>
                        <Box marginTop="7px" marginLeft="7px">
                          <Typography>Class History</Typography>
                          <Link component={RouterLink}
                            to={`/meetings/dashboard`}
                          >
                            View
                          </Link>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>

                  <Grid item xs={12} style={{marginTop:"25px" }}>
                    <Box
                      bgcolor="#ffffff"
                      borderRadius="5px"
                      display="flex"
                      alignItems="center"
                      padding="15px"
                      marginTop='20px'
                      minHeight='150px'
                    >
                      <Box
                        display="flex"
                        flexDirection='Column'
                        justifyContent='space-between'
                        alignItems="center"
                        paddingY="10px"
                        width='70%'
                        minHeight='140px'
                      >
                        <Box className={classes.usageBox}>
                          <Box className={classes.boxText}>Learn even More !</Box>
                          <Box className={classes.subtexts} marginTop="20px">
                            Classes related Information will be come here
                          </Box>
                          <Button 
                            variant="contained" color="primary" 
                            style={{width: '150px', marginTop: '15px'}}
                            onClick={() => setRedirectTo(`/profile/batches`)}
                          >
                            More Details
                          </Button>
                        </Box>
                      </Box>
                      <Box
                        borderRadius="5px"
                        color="#ffffff"
                        display="flex"
                        alignItems="center"
                        justifyContent='center'
                        padding="15px"
                        width='30%'
                        minHeight='140px'
                      >
                        <Box><img
                          style={{height:"100px"}}
                          src={knowledgeHover}
                          alt="Course"
                          onClick={() => ''}
                        /></Box>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>

      {/* <Box marginY="50px">
        <Container>
          <Grid container>
            <Grid item xs={12} md={7}>
              <Box paddingX="15px">
                <Box
                  bgcolor="#00B9F5"
                  padding="35px 30px"
                  marginBottom="30px"
                  position="relative"
                  borderRadius="14px"
                >
                  <Grid item container>
                    <Grid item md={9}>
                      <Box>
                        <Typography className={styles.welcomeNote}>
                          Hello {profile.parentName}!
                        </Typography>
                        <Typography>
                          It's good to see you again.
                        </Typography>
                      </Box>
                    </Grid>

                    <Grid item md={3}>
                      <Box>
                        <img
                          className={classes.boyWavingHard}
                          src={BoyWavingHand}
                          alt={`Hello ${profile.parentName}!`}
                        />
                      </Box>
                    </Grid>
                  </Grid>
                </Box>

                <Typography variant="h6" className={styles.heading}>
                  Upcoming Classes
                </Typography>

                <Box display="flex" margin="30px 0 15px 0">
                  <Box marginRight="20px" className={styles.selectedClass}>
                    <MuButton
                      size="small"
                      onClick={() => {
                        setDashboardObject('courses');
                        setSubject('');
                      }}
                    >
                      All Courses
                    </MuButton>
                  </Box>
                </Box>
              </Box>

              <Box display="flex" marginTop="30px">
                <Box>

                  {dashboardObject === 'courses' &&
                    filteredSchedules.slice(0, 5).map((item, index) => (
                      <UpcomingCourseBlock key={index} item={item} />
                    ))
                  }
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={5}>
              <Box>

                <Grid container spacing={4} alignItems="center" alignContent="center" style={{ marginBottom: '10px', textAlign: 'center' }}>
                  <Grid item xs={6}>
                    <Box
                      bgcolor="#00B9F5"
                      color="#ffffff"
                      boxShadow="0px 2px 6px gray"
                      borderRadius="3px"
                      height="85px"
                      alignItems="center"
                    >
                      <Box className={styles.navItem}>
                        <img src={Syllabus} alt="Syllabus" />
                        <Typography>Syllabus</Typography>
                      </Box>
                      <Box className={styles.navLinks}>
                        <Link>View</Link>
                        <Link>Download</Link>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={6}>
                    <Box
                      bgcolor="#4C8BF5"
                      borderRadius="3px"
                      color="#ffffff"
                      boxShadow="0px 2px 6px gray"
                      height="85px"
                      alignItems="center"
                    >
                      <Box className={styles.navItem}>
                        <img src={Clock} alt="Clock" />
                        <Typography>Time Table</Typography>
                      </Box>
                      <Box className={styles.navLinks}>
                        <Link>View</Link>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>

                <Box
                  bgcolor="#ffffff"
                  borderRadius="5px"
                  display="flex"
                  alignItems="center"
                  padding="15px"
                  marginTop='20px'
                  minHeight='150px'
                >
                  <Box
                    display="flex"
                    flexDirection='Column'
                    justifyContent='space-between'
                    alignItems="center"
                    //padding="15px"
                    width='70%'
                    minHeight='140px'
                  >
                    <Box className={classes.usageBox}>
                      <Box className={classes.boxText}>All Analytics</Box>
                      <Box className={classes.subtexts} marginTop="20px">View all student related analytics</Box>
                      <Box className={classes.subtexts}>Assessment | Assignments | Attendance</Box>
                    </Box>
                    <Box width='100%'><MuButton
                      variant="contained"
                      size="small"
                      component={RouterLink}
                      to={`/profile/analytics`}
                      color="primary"
                    >
                      Analytics Dashboard
                      </MuButton></Box>
                  </Box>
                  <Box
                    bgcolor="#00B9F5"
                    borderRadius="5px"
                    color="#ffffff"
                    display="flex"
                    alignItems="center"
                    justifyContent='center'
                    padding="15px"
                    width='30%'
                    minHeight='140px'
                  >
                    <Box><img
                      style={{ height: "100px" }}
                      src={Analytics}
                      alt="Course"
                      onClick={() => ''}
                    /></Box>
                  </Box>
                </Box>

                <Box
                  display="flex"
                  bgcolor="#F5F5F7"
                  borderRadius="14px"
                  padding="20px"
                >
                  <Box className={styles.learnMore}>
                    <Typography variant="h5">Learn even more!</Typography>
                    <Typography>
                      Classes related Information will be come here
                    </Typography>
                    <Box className={styles.btnYellow}>
                      <Button>More Details</Button>
                    </Box>
                  </Box>
                  <Box>
                    <img src={Knowledge} alt="Knowledge" />
                  </Box>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
     */}
     </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withStyles(styles)(ParentDashboard));
