import React, { FunctionComponent } from 'react';
import { useDispatch } from 'react-redux';
import { setAuthUser } from '../../auth/store/actions';
import {
  Link as RouterLink,
  RouteComponentProps,
  withRouter
} from 'react-router-dom';
import {
  Box,
  Container,
  Grid,
  List,
  ListItem,
  ListItemText,
  Theme,
  Typography
} from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Student } from '../../common/contracts/user';
import MiniDrawer from '../../common/components/sidedrawer';
import ProfileImage from '../containers/profile_image';
import { fontOptions } from '../../../theme';
const styles = (theme: Theme) =>
  createStyles({
    bodyContainer: {
      [theme.breakpoints.up('lg')]: {
        padding: '0 40px'
      }
    },

    welcomeHeading: {
      [theme.breakpoints.up('lg')]: {
        padding: '0 40px'
      },
      fontWeight: 'normal',
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      lineHeight: '30px',
      letterSpacing: '1px',
      color: '#333333'
    },
    navList: {
      background: '#fff',
      marginBottom: '20px',
      paddingTop: '10px'
    },
    navItem: {
      padding: '8px 0px 8px 20px'
    },
    
    inputValue: {
      fontSize: '16px',
      fontFamily:fontOptions.family,
      lineHeight: '18px',
      color: '#151522',
      //marginLeft: '25px'
    }
  });

interface Props extends WithStyles<typeof styles>, RouteComponentProps {
  profile: Student;
}

const StudentLayout: FunctionComponent<Props> = ({
  children,
  classes,
  profile,
  match
}) => {
  const dispatch = useDispatch();

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box paddingY="20px">

          <Grid container>
            <Grid item xs={12} md={3}>
              <Box className={classes.navList}>
                <ListItem
                  button
                  component={RouterLink}
                  to={`/profile/personal-information`}
                  className={classes.navItem}
                >
                  <ListItemText>
                    <Typography
                      color={
                        match.path === '/profile/personal-information'
                          ? 'primary'
                          : 'inherit'
                      }
                    >
                      Personal Information
                    </Typography>
                  </ListItemText>
                </ListItem>
                <List dense>
                  <ListItem
                    button
                    component={RouterLink}
                    to={`/profile/security`}
                    className={classes.navItem}
                  >
                    <ListItemText>
                      <Typography
                        color={
                          match.path === '/profile/security'
                            ? 'primary'
                            : 'inherit'
                        }
                      >
                        Security
                      </Typography>
                    </ListItemText>
                  </ListItem>
                </List>
              </Box>
            </Grid>

            <Grid item xs={12} md={9}>

              <Box className={classes.bodyContainer}>
                {/* <Grid item container>
                  <Grid item md={3}>
                    <Box>
                      <ProfileImage
                        profileUpdated={(profile) => dispatch(setAuthUser(profile))}
                        profile={profile}
                        name={profile.studentName}
                      />
                    </Box>
                  </Grid>
                  <Grid item md={9}>
                    <Box component="h2" className={classes.welcomeHeading}>
                      Welcome, {profile.studentName}!
              </Box>
                  </Grid>
                </Grid> */}
                {match.path.search('personal-information') !== -1 && (<Grid container style={{ backgroundColor: '#fff', paddingTop: '3em', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <ProfileImage
                        profileUpdated={(profile) => dispatch(setAuthUser(profile))}
                        profile={profile}
                        name={profile.studentName}
                      />
                      <Box className={classes.inputValue}>
                      Upload your image
                </Box>
                </Grid>)}

                {children}
              </Box>
            </Grid>

          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

export default withStyles(styles)(withRouter(StudentLayout));