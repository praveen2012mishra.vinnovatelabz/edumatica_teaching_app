import React, { FunctionComponent } from 'react';
import { Box, Grid, IconButton, Typography } from '@material-ui/core';
import { RemoveCircleOutline as RemoveCircleIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { BoardClassSubjectsMap } from '../../academics/contracts/board_class_subjects_map';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    courseHeading: {
      fontWeight: fontOptions.weight.bold,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '20px',
      letterSpacing: '1px',
      textTransform: 'capitalize',
      color: '#4C8BF5'
    },
    courseContent: {
      fontWeight: fontOptions.weight.bold,
      fontFamily:fontOptions.family,fontSize: fontOptions.size.small,
      lineHeight: '20px',
      letterSpacing: '1px',
      textTransform: 'capitalize',
      color: '#000000'
    },
    removeIcon: {
      fill: '#000000'
    },
    break: {
      wordBreak: 'break-all'
    }
  })
);

interface BoardClassSubjectRowProps {
  item: BoardClassSubjectsMap;
  handleRemoveItem: () => any;
}

const BoardClassSubjectsRow: FunctionComponent<BoardClassSubjectRowProps> = ({
  item,
  handleRemoveItem
}) => {
  const classes = useStyles();
  return (
    <Box marginTop="5px">
      <Grid container alignItems="center" spacing={3}>
        <Grid item xs={3}>
          <Box className={classes.courseContent}>
            <Typography 
            variant="subtitle2"
            className={classes.break}>
                          {item.boardname}
            </Typography>
</Box>
        </Grid>

        <Grid item xs={3}>
          <Box className={classes.courseContent}><Typography 
            variant="subtitle2"
            className={classes.break}>
                          {item.classname}
            </Typography></Box>
        </Grid>

        <Grid item xs={4}>
          <Box className={classes.courseContent}>
          <Typography 
            variant="subtitle2"
            className={classes.break}
            >
                          {item.subjects.join(', ')}
            </Typography>
            
          </Box>
        </Grid>

        <Grid item xs={2}>
          <Box display="flex" justifyContent="flex-end">
            <IconButton size="small" onClick={handleRemoveItem}>
              <RemoveCircleIcon className={classes.removeIcon} />
            </IconButton>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

interface Props {
  boardClassSubjectsMap: BoardClassSubjectsMap[];
  handleRemoveItem: (map: BoardClassSubjectsMap) => any;
}

const CourseTable: FunctionComponent<Props> = ({
  boardClassSubjectsMap,
  handleRemoveItem
}) => {
  const classes = useStyles();
  return (
    <Box
      borderBottom="1px dashed #EAE9E4"
      borderTop="1px dashed #EAE9E4"
      marginY="15px"
      paddingTop="18px"
      paddingBottom="18px"
    >
      <Box marginBottom="10px">
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <Box className={classes.courseHeading}>BOARD</Box>
          </Grid>

          <Grid item xs={3}>
            <Box className={classes.courseHeading}>CLASS</Box>
          </Grid>

          <Grid item xs={6}>
            <Box className={classes.courseHeading}>SUBJECTS</Box>
          </Grid>
        </Grid>
      </Box>

      {boardClassSubjectsMap.map((map, index) => (
        <BoardClassSubjectsRow
          key={index}
          item={map}
          handleRemoveItem={() => handleRemoveItem(map)}
        />
      ))}
    </Box>
  );
};

export default CourseTable;
