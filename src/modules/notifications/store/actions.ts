import { createAction } from '@reduxjs/toolkit';

export const updateNotificationCount = createAction(
  'UPDATE_NOTIFICATION_COUNT',
  (count: number) => {
    //Do Something
    //const jsResponse = xml2js(createMeetingInfo, { compact: true })
    return {
      payload: count
    };
  }
);