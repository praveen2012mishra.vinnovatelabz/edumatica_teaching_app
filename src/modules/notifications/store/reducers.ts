import { combineReducers, createReducer } from '@reduxjs/toolkit';
import { updateNotificationCount } from './actions';

const INITIAL_COUNT_RESPONSE = 0;
const updateNotificationCountResponse = createReducer(INITIAL_COUNT_RESPONSE, {
  [updateNotificationCount.type]: (_, action) => action.payload
});

export const meetingReducer = combineReducers({
  updateNotificationCountResponse
});
