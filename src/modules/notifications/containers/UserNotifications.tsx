import { createStyles, WithStyles, withStyles } from '@material-ui/styles';
import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { RootState } from '../../../store';
import { User } from '../../common/contracts/user';
import MiniDrawer from '../../common/components/sidedrawer';
import { Box, Button, Container, Divider } from '@material-ui/core';
import NotificationSvg from '../../../assets/svgs/notification_svg.svg';
import { getAllNotifications, updateNotifications } from '../helper/api';
import { exceptionTracker } from '../../common/helpers';
import InfoIcon from '@material-ui/icons/Info';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';import EventEmitter from '../../../EventEmitter';
import Scrollbars from 'react-custom-scrollbars';
;

const styles = createStyles({
  tophead: {
    borderRadius: '10px',
    padding: '18px 50px',
    marginTop: '28px',
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  content: {
    minHeight: '70vH',
    borderRadius: '10px',
    marginTop: '18px',
    width: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  hover : {
    "&:hover": {
      cursor: "pointer",
      background: "#efefef"
    },
  }
});

interface Props extends WithStyles<typeof styles> {
  authUser: User;
}

const UserNotifications: FunctionComponent<Props> = ({ authUser, classes }) => {
  const loggedUser: any = authUser
  const [redirectTo, setRedirectTo] = useState('');
  const [notifications, setNotifications] = useState<any[]>([])
  const [filteredNotifications, setFilteredNotifications] = useState<any[]>([])
  const [allSwitch, setAllSwitch] = useState<Boolean>(true)
  const [readSwitch, setReadSwitch] = useState<Boolean>(false)
  const [unReadSwitch, setUnReadSwitch] = useState<Boolean>(false)
  console.log(filteredNotifications)

  const allRef: any = useRef();
  const readRef: any = useRef();
  const unreadRef: any = useRef();

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo('/login');
    }
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      try {
        const notificationsListResponse = await getAllNotifications(loggedUser._id);
        const [notificationsList] = await Promise.all([
          notificationsListResponse
        ]);
        // setNotifications(notificationsList.data.notifications);
        var data: any[] = notificationsList.data.notifications
        if(data) {
          setNotifications(data.reverse())
          setFilteredNotifications(data)
        } 
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [authUser]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const focusAll = () => {
    setAllSwitch(true)
    setReadSwitch(false)
    setUnReadSwitch(false)
    if(!notifications) return
    setFilteredNotifications(notifications)
    // if(notifications) {
    //   const reversed = notifications.map(item => item).reverse()
    //   setFilteredNotifications(reversed)
    // }
    allRef.current.style.backgroundColor = '#4C8BF5';
    allRef.current.style.color = '#FFF';
    readRef.current.style.backgroundColor = '#fff';
    readRef.current.style.color = 'GrayText';
    unreadRef.current.style.backgroundColor = '#fff';
    unreadRef.current.style.color = 'GrayText';
  };

  const focusRead = () => {
    setAllSwitch(false)
    setReadSwitch(true)
    setUnReadSwitch(false)
    if(!notifications) return
    const filter = notifications.filter(function(item) {
      return item.isViewedStatus === true
    })
    setFilteredNotifications(filter)
    // if(filter){
    //   const reversed = filter.map(item => item).reverse()
    //   setFilteredNotifications(reversed)
    // }
    readRef.current.style.backgroundColor = '#4C8BF5';
    readRef.current.style.color = '#FFF';
    allRef.current.style.backgroundColor = '#fff';
    allRef.current.style.color = 'GrayText';
    unreadRef.current.style.backgroundColor = '#fff';
    unreadRef.current.style.color = 'GrayText';
  };

  const focusUnread = () => {
    setAllSwitch(false)
    setReadSwitch(false)
    setUnReadSwitch(true)
    if(!notifications) return
    const filter = notifications.filter(function(item) {
      return item.isViewedStatus === false
    })
    var notificationIdArray:string[] = []
    setFilteredNotifications(filter)
    filter.forEach(each => {
      notificationIdArray.push(each._id)
    })
    console.log(notificationIdArray)
    updateNotifications(loggedUser._id,notificationIdArray)
    // if(filter) {
    //   const reversed = filter.map(item => item).reverse()
    //   setFilteredNotifications(reversed)
    // }
    unreadRef.current.style.backgroundColor = '#4C8BF5';
    unreadRef.current.style.color = '#FFF';
    readRef.current.style.backgroundColor = '#fff';
    readRef.current.style.color = 'GrayText';
    allRef.current.style.backgroundColor = '#fff';
    allRef.current.style.color = 'GrayText';
  };
  
  EventEmitter.on("notification", function(payload:any) {
    // console.log(payload)
    getAllNotifications(loggedUser._id).then(response => {
      if(response.data.notifications) {
        var data: any[] = response.data.notifications
        setNotifications(data.reverse())
        // setFilteredNotifications(data)
        if(allSwitch === true) setFilteredNotifications(data)
        if(readSwitch === true) {
          const filter = data.filter(function(item) {
            return item.isViewedStatus === true
          })
          setFilteredNotifications(filter)
        }
        if(unReadSwitch === true) {
          const filter = data.filter(function(item) {
            return item.isViewedStatus === false
          })
          setFilteredNotifications(filter)
        }
      } 
    })
  })

  return (
    <div>
      <MiniDrawer>
      <Container style={{ width:'max-content' }}>
        <Box bgcolor="#fff" className={classes.tophead}>
          <Box>
            <h1 style={{ marginTop: '-3px' }}>Notifications</h1>
            <Box minWidth="150px" display="flex" justifyContent="space-between">
              <Button
                ref={allRef}
                onClick={focusAll}
                style={{ backgroundColor: '#4C8BF5', color: '#fff' }}
                disableElevation
                variant="contained"
                size="small"
              >
                All
              </Button>
              <Button
                ref={readRef}
                onClick={focusRead}
                style={{ backgroundColor: '#fff', color: 'GrayText' }}
                disableElevation
                variant="contained"
                size="small"
              >
                Read
              </Button>
              <Button
                ref={unreadRef}
                onClick={focusUnread}
                style={{ backgroundColor: '#fff', color: 'GrayText' }}
                disableElevation
                variant="contained"
                size="small"
              >
                Unread
              </Button>
            </Box>
          </Box>
          <img height="110px" src={NotificationSvg} alt="Class History" />
        </Box>
        <Box bgcolor="#fff" className={classes.content}>
          <Scrollbars style={{height:"70vH"}}>
          {
            filteredNotifications.map(notification => {
              const dataLink = JSON.parse(notification.data)
              return (
                <>
                {
                  notification.data ? 
                    <Box onClick={() => {window.open(dataLink.link)}} className={classes.hover} key={notification._id} display="flex" alignItems="center" borderRadius="5px" bgcolor="#fafafa" margin="6px 8px" padding="12px 50px">
                      <Box display="flex" alignItems="center" borderRadius="50%" color="#4C8BF5" padding="12px" border="1px solid #4C8BF5"><InfoIcon/></Box>
                      <h4 style={{marginLeft:"15px"}}>{notification.message}</h4>
                    </Box>
                  :
                    <Box className={classes.hover} key={notification._id} display="flex" alignItems="center" borderRadius="5px" bgcolor="#fafafa" margin="6px 8px" padding="12px 50px">
                      <Box display="flex" alignItems="center" borderRadius="50%" color="#4C8BF5" padding="12px" border="1px solid #4C8BF5"><NotificationsActiveIcon/></Box>
                      <h4 style={{marginLeft:"15px"}}>{notification.message}</h4>
                    </Box>
                }
                <Box padding="0 10px"><Divider></Divider></Box>
                </>
              )
            })
          }
          </Scrollbars>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default withStyles(styles)(connect(mapStateToProps)(UserNotifications));
