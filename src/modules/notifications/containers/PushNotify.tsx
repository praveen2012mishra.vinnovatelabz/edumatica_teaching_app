import React, { useState, useEffect, FunctionComponent } from 'react';
import { Link as RouterLink, } from 'react-router-dom';
import PersonWithBook from '../../../assets/images/study.png';
import MiniDrawer from '../../common/components/sidedrawer';
import { connect } from 'react-redux';
import { RootState } from '../../../store';
import { Student, User } from '../../common/contracts/user'
import { Box, Button, Card, CardHeader, Checkbox, Container, Divider, Grid, IconButton, List, ListItem, ListItemIcon, ListItemText, TextField, Typography } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { fetchBatchesList, fetchTutorStudentsList } from '../../common/api/academics';
import { getOrgStudentsList } from '../../common/api/organization';
import { exceptionTracker, isOrganization, isStudent, isTutor } from '../../common/helpers';
import { sendCustomPush, sendMail } from '../helper/api';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Redirect, RouteComponentProps, useHistory, withRouter } from 'react-router';
import { createStyles, WithStyles, withStyles } from '@material-ui/styles';
import { fontOptions } from '../../../theme';
import { Batch } from '../../academics/contracts/batch';
import { fetchOrgBatchesList } from '../../common/api/batch';

const styles = createStyles({
  heading: {
    margin: '0',
    fontWeight: 500,
    fontSize: fontOptions.size.medium,
    letterSpacing: '1px',
    color: '#212121'
  },
  btn: {
    border: '1px solid #4C8BF5'
  },
  root: {
    margin: 'auto',
  },
  cardHeader: {
    padding: "5px 10px",
  },
  list: {
    width: 200,
    height: 230,
    backgroundColor: "#fff",
    overflow: 'auto',
  },
  button: {
    margin: "5px 0",
  },
})

function not(a: Student[], b: Student[]) {
  return a.filter((value) => b.findIndex(el => el._id === value._id) === -1);
}

function intersection(a: Student[], b: Student[]) {
  return a.filter((value) => b.findIndex(el => el._id === value._id) !== -1);
}

function union(a: Student[], b: Student[]) {
  return [...a, ...not(b, a)];
}

interface Props extends WithStyles<typeof styles> { }
interface Props extends RouteComponentProps {
  authUser: User;
}

// for Batch
function notBatch(a: Batch[], b: Batch[]) {
  return a.filter((value) => b.findIndex(el => el._id === value._id) === -1);
}

function intersectionBatch(a: Batch[], b: Batch[]) {
  return a.filter((value) => b.findIndex(el => el._id === value._id) !== -1);
}

function unionBatch(a: Batch[], b: Batch[]) {
  return [...a, ...notBatch(b, a)];
}

interface Props extends WithStyles<typeof styles> { }
interface Props extends RouteComponentProps {
  authUser: User;
}

const PushNotify: FunctionComponent<Props> = ({ classes, authUser }) => {
  const history = useHistory()
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [students, setStudents] = React.useState<Student[]>([]);
  const [batches, setBatches] = useState<Batch[] | null>(null);
  const [msg, setMsg] = useState('')

  const [left, setLeft] = React.useState<Student[]>([]);
  const [right, setRight] = React.useState<Student[]>([]);
  const [checked, setChecked] = React.useState<Student[]>([]);
  const [leftBatch, setLeftBatch] = React.useState<Batch[]>([]);
  const [rightBatch, setRightBatch] = React.useState<Batch[]>([]);
  const [checkedBatch, setCheckedBatch] = React.useState<Batch[]>([]);

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);
  const leftCheckedBatch = intersectionBatch(checkedBatch, leftBatch);
  const rightCheckedBatch = intersectionBatch(checkedBatch, rightBatch);


  const [switchStudent, setSwitchStudent] = useState<boolean>(true)
  const [switchBatch, setSwitchBatch] = useState<boolean>(false)

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo('/login');
    }
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      try {
        var studentsList: Student[] = [];
        var batchesList: Batch[] = [];
        if (isTutor(authUser)) {
          studentsList = await fetchTutorStudentsList();
          batchesList = await fetchBatchesList();
        }
        if (isOrganization(authUser)) {
          studentsList = await getOrgStudentsList();
          batchesList = await fetchOrgBatchesList();
        }
        if (isStudent(authUser)) {
          history.push('/profile/dashboard')
        }
        setBatches(batchesList);
        setStudents(studentsList);
        setLeft(studentsList)
        setLeftBatch(batchesList)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      try {

      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleToggle = (value: Student) => () => {
    const currentIndex = checked.findIndex(el => el._id === value._id);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const numberOfChecked = (items: Student[]) => intersection(checked, items).length;

  const handleToggleAll = (items: Student[]) => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items));
    } else {
      setChecked(union(checked, items));
    }
  };

  const handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };


  const customList = (title: React.ReactNode, items: Student[]) => (
    <Card>
      <CardHeader
        className={classes.cardHeader}
        avatar={
          <Checkbox
            onClick={handleToggleAll(items)}
            checked={numberOfChecked(items) === items.length && items.length !== 0}
            indeterminate={numberOfChecked(items) !== items.length && numberOfChecked(items) !== 0}
            disabled={items.length === 0}
            inputProps={{ 'aria-label': 'all items selected' }}
          />
        }
        title={title}
        subheader={`${numberOfChecked(items)}/${items.length} selected`}
      />
      <Divider />
      <List className={classes.list} dense component="div" role="list">
        {items.map((value: Student) => {
          const labelId = `transfer-list-all-item-${value.studentName}-label`;

          return (
            <ListItem key={value._id} role="listitem" button onClick={handleToggle(value)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.findIndex(el => el._id === value._id) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={value.studentName} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );

  // For Batches
  const handleToggleBatch = (value: Batch) => () => {
    const currentIndex = checkedBatch.findIndex(el => el._id === value._id);
    const newCheckedBatch = [...checkedBatch];

    if (currentIndex === -1) {
      newCheckedBatch.push(value);
    } else {
      newCheckedBatch.splice(currentIndex, 1);
    }

    setCheckedBatch(newCheckedBatch);
  };

  const numberOfCheckedBatch = (items: Batch[]) => intersectionBatch(checkedBatch, items).length;

  const handleToggleAllBatch = (items: Batch[]) => () => {
    if (numberOfCheckedBatch(items) === items.length) {
      setCheckedBatch(notBatch(checkedBatch, items));
    } else {
      setCheckedBatch(unionBatch(checkedBatch, items));
    }
  };

  const handleCheckedRightBatch = () => {
    setRightBatch(rightBatch.concat(leftCheckedBatch));
    setLeftBatch(notBatch(leftBatch, leftCheckedBatch));
    setCheckedBatch(notBatch(checkedBatch, leftCheckedBatch));
  };

  const handleCheckedLeftBatch = () => {
    setLeftBatch(leftBatch.concat(rightCheckedBatch));
    setRightBatch(notBatch(rightBatch, rightCheckedBatch));
    setCheckedBatch(notBatch(checkedBatch, rightCheckedBatch));
  };

  const customListBatch = (title: React.ReactNode, items: Batch[]) => (
    <Card>
      <CardHeader
        className={classes.cardHeader}
        avatar={
          <Checkbox
            onClick={handleToggleAllBatch(items)}
            checked={numberOfCheckedBatch(items) === items.length && items.length !== 0}
            indeterminate={numberOfCheckedBatch(items) !== items.length && numberOfCheckedBatch(items) !== 0}
            disabled={items.length === 0}
            inputProps={{ 'aria-label': 'all items selected' }}
          />
        }
        title={title}
        subheader={`${numberOfCheckedBatch(items)}/${items.length} selected`}
      />
      <Divider />
      <List className={classes.list} dense component="div" role="list">
        {items.map((value: Batch) => {
          const labelId = `transfer-list-all-item-${value.batchfriendlyname}-label`;

          return (
            <ListItem key={value._id} role="listitem" button onClick={handleToggleBatch(value)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.findIndex(el => el._id === value._id) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={value.batchfriendlyname} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );

  const notify = () => {
    if (msg === '') {
      enqueueSnackbar('Please enter custom notification to be sent', {
        variant: 'error'
      });
      return
    }
    if (right.length < 1 && rightBatch.length < 1) {
      enqueueSnackbar('Please select students/batches to be notified', {
        variant: 'error'
      });
      return
    }
    let recipients: string[] = [];
    rightBatch.forEach(elem => {
      recipients = [...recipients, ...elem.students]
    })
    right.forEach(ele => {
      if (ele._id && recipients.includes(ele._id) !== true)
        recipients.push(ele._id)
    })
    console.log(recipients)
    if (recipients.length > 0) {
      sendCustomPush(recipients, msg).then(response => {
        if (response.data.code === 0) {
          enqueueSnackbar('Students successfully notified', {
            variant: 'success'
          });
        }
      })
    }
  }

  return (
    <div>
      <MiniDrawer>
        <Container maxWidth="md">
          <Box
            bgcolor="#FFF"
            marginY="20px"
            boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
            borderRadius="4px"
          >
            <Box borderBottom="1px solid rgba(224, 224, 224, 0.5)">
              <Grid container>
                <Grid item xs={12} md={7}>
                  <Box padding="20px 30px" display="flex" alignItems="center">
                    <img src={PersonWithBook} alt="Person with Book" />

                    <Box marginLeft="15px" display="flex" alignItems="center">
                      <Typography component="span" color="secondary">
                        <Box component="h3" className={classes.heading}>
                          Announcement
                      </Box>
                      </Typography>
                      <Box
                        marginLeft="10px"
                        display="flex"
                        justifyContent="flex-end"
                      >
                        <Box marginLeft="20px">
                          <IconButton
                            color="secondary"
                            size="small"
                            component={RouterLink}
                            to={`/profile/dashboard`}
                            className={classes.btn}
                          >
                            <ArrowBackIcon />
                          </IconButton>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Box>

          <Box padding="20px" width="100%" bgcolor="#fff" margin="auto" display="flex" flexDirection="column" alignItems="center" justifyContent="center">

            <Box width="100%" display="flex" justifyContent="space-between">
              <Button onClick={() => { setSwitchBatch(false); setSwitchStudent(true) }} size="small" disableElevation color="primary" variant="contained">
                Students
          </Button>
              <Button onClick={() => { setSwitchBatch(true); setSwitchStudent(false) }} size="small" disableElevation color="primary" variant="contained">
                Batches
          </Button>
            </Box>

            {
              switchStudent !== true ? '' :

                <Grid container style={{ margin: "30px 0",justifyContent:'space-between',display:'flex' }} justify="center" alignItems="center" className={classes.root}>
                  <Grid item md={4} sm={4} xs={12}>{customList('Available Students', left)}</Grid>
                  <Grid item md={3} sm={3} xs={12}>
                    <Grid container direction="column" alignItems="center">
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleCheckedRight}
                        disabled={leftChecked.length === 0}
                        aria-label="move selected right"
                      >
                        &gt;
              </Button>
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleCheckedLeft}
                        disabled={rightChecked.length === 0}
                        aria-label="move selected left"
                      >
                        &lt;
              </Button>
                    </Grid>
                  </Grid>
                  <Grid item md={4} sm={4} xs={12}>{customList('Selected Students', right)}</Grid>
                </Grid>
            }

            {
              switchBatch !== true ? '' :

                <Grid container justify="center" alignItems="center" className={classes.root}>
                  <Grid style={{ margin: "10px", marginLeft: "20px" }} item>{customListBatch('Available Batches', leftBatch)}</Grid>
                  <Grid item>
                    <Grid container direction="column" alignItems="center">
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleCheckedRightBatch}
                        disabled={leftCheckedBatch.length === 0}
                        aria-label="move selected right"
                      >
                        &gt;
              </Button>
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleCheckedLeftBatch}
                        disabled={rightCheckedBatch.length === 0}
                        aria-label="move selected left"
                      >
                        &lt;
              </Button>
                    </Grid>
                  </Grid>
                  <Grid style={{ margin: "18px" }} item>{customListBatch('Selected Batches', rightBatch)}</Grid>
                </Grid>
            }
            <Grid container style={{ marginBottom: "30px" }} justify="center" alignItems="center" className={classes.root}>
              <Grid item xs={12}>
                <TextField value={msg} multiline rows={3} style={{ marginBottom: "20px", width: "100%" }} onChange={(e) => setMsg(e.target.value)} label="Notification" variant="outlined" />
              </Grid>
              <Grid item xs={12}>
                <Button style={{ width: "100%", padding: "10px", marginBottom: "30px" }} variant='contained' color="primary" onClick={notify}>
                  Send notification
          </Button>
              </Grid>


            </Grid>

          </Box>
        </Container>
      </MiniDrawer>
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(PushNotify))
);
