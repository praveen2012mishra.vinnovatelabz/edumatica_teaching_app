const BASE_ROUTE = process.env.REACT_APP_API;
export const GET_ALL_NOTIFICATIONS = BASE_ROUTE + '/notifications'
export const SAVE_DEVICE_TOKEN = BASE_ROUTE + '/notifications/devicetoken';
export const MSG_TEMPLATE = BASE_ROUTE + '/notifications/msgtemplate';
export const PUSH_TEMPLATE = BASE_ROUTE + '/notifications/pushtemplate';
export const SEND_CUSTOM_PUSH = BASE_ROUTE + '/notifications/sendcustompush';
export const SEND_MAIL = BASE_ROUTE + '/notifications/sendmail';