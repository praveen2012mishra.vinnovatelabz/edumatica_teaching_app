import axios from 'axios';
import { GET_ALL_NOTIFICATIONS, SAVE_DEVICE_TOKEN, MSG_TEMPLATE, PUSH_TEMPLATE, SEND_CUSTOM_PUSH, SEND_MAIL } from './routes';

export const setDeviceToken = async (userId: string, mobileNo: string, deviceToken: string) => {
  return axios.post(SAVE_DEVICE_TOKEN, {userId, mobileNo, deviceToken})
}

export const saveMsgTemplate = async (type: string, template: string, icon: string) => {
  return axios.post(MSG_TEMPLATE, {type, template, icon})
}

export const savePushTemplate = async (type: string, template: string, icon: string) => {
  return axios.post(PUSH_TEMPLATE, {type, template, icon})
}

export const switchMsgTemplate = async (type: string, state: boolean) => {
  return axios.patch(MSG_TEMPLATE, {type, state})
}

export const switchPushTemplate = async (type: string, state: boolean) => {
  return axios.patch(PUSH_TEMPLATE, {type, state})
}

export const sendCustomPush = async (recipients: string[], msg: string) => {
  return axios.post(SEND_CUSTOM_PUSH, {recipients, msg})
}

export const sendMail = async (mailID: string) => {
  return axios.get(SEND_MAIL, {params: {mailID}})
}

export const getAllNotifications = async (userId: string) => {
  return axios.get(GET_ALL_NOTIFICATIONS, {params: {userId}})
}

export const updateNotifications = async (userId: string, notificationIdArr:string[]) => {
  return axios.patch(GET_ALL_NOTIFICATIONS,{userId, notificationIdArr})
}