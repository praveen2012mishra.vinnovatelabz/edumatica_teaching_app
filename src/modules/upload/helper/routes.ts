const BASE_ROUTE =  process.env.REACT_APP_API

export const UPLOAD_DATA = BASE_ROUTE +"/upload/uploadData";

export const GET_FILENAMES = BASE_ROUTE + '/upload/filenames';

export const DELETE_BY_FILENAME = BASE_ROUTE + '/upload/deletebyfilename';