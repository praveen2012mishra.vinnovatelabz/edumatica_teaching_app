import axios from "axios"
import { UPLOAD_DATA,GET_FILENAMES ,DELETE_BY_FILENAME} from "./routes"
import {UploadDataResponse, getFilenameResponse} from "./contracts"

export const dataupload =async  (formData: FormData) =>{
    const response = await axios.post<UploadDataResponse>(UPLOAD_DATA, formData)
    return response.data.excelData
}


export const getFilename = async () =>{
    const response = await axios.get<getFilenameResponse>(GET_FILENAMES)
    return response.data.filenames
}


export const deleteByFilename = async (filename:string) =>{
    const response = await axios.delete(DELETE_BY_FILENAME,{params:{filename}})
    return response
}