import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, FormControl, Input, Select, MenuItem, Backdrop, CircularProgress, FormHelperText, Checkbox, ListItemText } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import AssiSheet from '../../../assets/svgs/assiSheet.svg'
import { taskDoc, AssignmentDocType, Assignment } from '../contracts/assignment_interface';
import { useForm } from 'react-hook-form';
import { User, Children, Student, Tutor } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { exceptionTracker } from '../../common/helpers';
import { Course } from '../../academics/contracts/course';
import { Batch } from '../../academics/contracts/batch';
import { isOrganization, isStudent, isParent, isAdminTutor, isOrgTutor } from '../../common/helpers';
import {
  fetchTutorCoursesList,
} from '../../common/api/academics';
import {
  fetchOrgCoursesList
} from '../../common/api/organization';
import Datetimepickers from '../../common/components/dateTimePickers';
import { useSnackbar } from 'notistack';
import { FileWithPath } from 'react-dropzone';
import Dropzone from '../../common/components/dropzone/dropzone';
import GridPreview from '../../common/components/dropzone/previewers/gridViewUploadContent'
import { fetchBatchesList, fetchTutorStudentsListid } from '../../common/api/academics';
import { fetchOrgBatchesList, fetchStudentBatchesList } from '../../common/api/batch';
import Button from '../../common/components/form_elements/button';
import { createAssignmentNew, convertToPdfNew, createAssignmentDraftNew, fetchUploadUrlForDocNew, uploadDoc, fetchAssignmentNew } from '../../common/api/assignment';
import { some, uniq } from 'lodash'
import { getOrgStudentsList } from '../../common/api/organization';
import './style.css';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    },
    formInput: {
      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    spError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    addAssi: {
      '& button': {
        padding: '10px 30px 10px 30px',
      },
    },
    dropzoneRoot: {
      width: '100%',
      minHeight: 'auto',
      border: '2.5px dashed rgba(76, 139, 245, 0.5)',
      boxSizing: 'border-box',
      borderRadius: '5px',
      paddingBottom: '15px'
    },
    dropzoneTextContainerRoot: {
      paddingTop: '3%'
    },
    dropzoneText: {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.bold,
      color: '#4C8BF5',
      margin: '20px 0',
    },
    dropzoneIcon: {
      color: '#4C8BF5'
    },
  }),
);

interface FormData {
  pageError: string;
  courseIndex: number;
  assignmentname: string;
  marks: number;
  instructions: string;
  brief: string;
  startDate: Date;
  endDate: Date;
}

interface SpError {
  startDate: string | boolean;
  endDate: string | boolean;
  batchSelList: string | boolean;
  droppedFiles: string | boolean;
  studentSelList: string | boolean;
}

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const CreateAssignment: FunctionComponent<Props> = ({ authUser, parentChildren, location }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { errors, setError, clearError } = useForm<FormData>();

  const [courses, setCourses] = useState<Course[]>([]);
  const [batches, setBatches] = useState<Batch[]>([]);
  const [students, setStudents] = useState<Student[]>([]);
  const [spError, setSpError] = useState<SpError>({ startDate: false, endDate: false, batchSelList: false, droppedFiles: false, studentSelList: false })

  const [courseIndex, setCourseIndex] = useState(-1);
  const [assignmentname, setAssignmentname] = useState('');
  const [instructions, setInstructions] = useState('');
  const [marks, setMarks] = useState(0);
  const [batchSelList, setBatchSelList] = React.useState<string[]>([]);
  const [studentSelList, setStudentSelList] = React.useState<string[]>([]);
  const [brief, setBrief] = useState('');
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(new Date());
  const [droppedFiles, setDroppedFiles] = useState<FileWithPath[]>([]);
  const [draftAssignment, setDraftAssignment] = useState<Assignment>();
  const [isDraftBatchSet, setIsDraftBatchSet] = useState(false)
  const [isDraftStudentSet, setIsDraftStudentSet] = useState(false)
  const [open, setOpen] = React.useState(false);

  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const params = new URLSearchParams(location.search);
        const assignmentId = params.get('assignmentId')
        if (assignmentId) {
          if (courses.length > 0 && !draftAssignment) {
            const assignmentResp = await fetchAssignmentNew(assignmentId, undefined);
            const draftAsgmt = assignmentResp[0]
            setDraftAssignment(draftAsgmt)
            const draftCourseIndex = courses.findIndex(list => (list.board === draftAsgmt.boardname) && (list.className === draftAsgmt.classname) && (list.subject === draftAsgmt.subjectname))
            const draftDroppedFile = [new File([""], draftAsgmt.taskDocs.taskDocLocation, { type: draftAsgmt.taskDocs.taskDocFormat })]
            setCourseIndex(draftCourseIndex)
            setAssignmentname(draftAsgmt.assignmentname);
            setInstructions(draftAsgmt.instructions)
            setMarks(draftAsgmt.marks)
            setBrief(draftAsgmt.brief)
            setStartDate(draftAsgmt.startDate)
            setEndDate(draftAsgmt.endDate)
            setDroppedFiles(draftDroppedFile)
          }

          if (batches.length > 0 && draftAssignment && !isDraftBatchSet) {
            setBatchSelList(draftAssignment.batches)
            setIsDraftBatchSet(true)
          }

          if (students.length > 0 && draftAssignment && !isDraftStudentSet) {
            setStudentSelList(draftAssignment.students)
            setIsDraftStudentSet(true)
          }
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [location.search, courses, batches, students])

  useEffect(() => {
    (async () => {
      try {
        const coursesList = isOrganization(authUser) ? await fetchOrgCoursesList() : await fetchTutorCoursesList();
        setCourses(coursesList);

        if (coursesList.length === 1) {
          setCourseIndex(0)
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      try {
        setStudentSelList([])
        setBatchSelList([])
        if (courseIndex > -1) {
          let batchesList = (isOrganization(authUser) ?
            await fetchOrgBatchesList() :
            (isStudent(authUser) ?
              await fetchStudentBatchesList(authUser._id ? authUser._id : '') :
              (isParent(authUser) ?
                (parentChildren.current ?
                  await fetchStudentBatchesList(parentChildren.current ? parentChildren.current : '') :
                  []
                ) :
                await fetchBatchesList()
              )
            )
          );

          const selectCourse = courses[courseIndex];
          batchesList = (batchesList.filter((bat) =>
            (bat.boardname === selectCourse.board) && (bat.classname === selectCourse.className) && (bat.subjectname === selectCourse.subject))
          )

          setBatches(batchesList);

          if (batchesList.length === 1) {
            setBatchSelList([batchesList[0]._id as string])
          }
        } else {
          setBatches([]);
          setBatchSelList([])
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [courseIndex])

  useEffect(() => {
    (async () => {
      try {
        if (batchSelList.length > 0) {
          const thisbatch = batches.find(list => list._id === batchSelList[0])
          if (thisbatch) {
            const tutorid = (isAdminTutor(authUser) || isOrgTutor(authUser)) ? (thisbatch.tutorId) : (thisbatch.tutorId as Tutor)._id
            let studentsList = isOrganization(authUser) ? await getOrgStudentsList() : await fetchTutorStudentsListid(tutorid as string);
            const allBatchStudent = uniq(batches.filter(list => batchSelList.includes(list._id as string)).map(list => list.students).flat());
            studentsList = studentsList.filter(list => allBatchStudent.includes(list._id as string))
            setStudentSelList(studentSelList.filter(list => allBatchStudent.includes(list)));
            setStudents(studentsList)
          }
        } else {
          setStudentSelList([])
          setStudents([])
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [batchSelList])

  const handleBackdropClose = () => {
    setOpen(false);
  };

  const handleBackdropOpen = () => {
    setOpen(true);
  };

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if ((event.target.value as string[]).length > 0) {
      setSpError({ ...spError, ...{ batchSelList: false } })
    }
    setBatchSelList(event.target.value as string[]);
  };

  const handleStudentChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if ((event.target.value as string[]).length > 0) {
      setSpError({ ...spError, ...{ studentSelList: false } })
    }
    setStudentSelList(event.target.value as string[]);
  };

  const handleStartDateChange = (date: Date | null) => {
    if (date) {
      setSpError({ ...spError, ...{ startDate: false } })
      setStartDate(date)
    } else {
      enqueueSnackbar('Start Date cannot be Empty', { variant: 'warning' });
    }
  };

  const handleEndDateChange = (date: Date | null) => {
    if (date) {
      setSpError({ ...spError, ...{ endDate: false } })
      setEndDate(date)
    } else {
      enqueueSnackbar('End Date cannot be Empty', { variant: 'warning' });
    }
  };

  const addingFile = (file: FileWithPath[]) => {
    if (file.length > 0) {
      if (droppedFiles[0]) {
        const addedtype = droppedFiles[0].type.includes('image') ? 'image' : 'pdf';
        if (addedtype === 'pdf' && file.some(list => list.type.includes(addedtype))) {
          setSpError({ ...spError, ...{ droppedFiles: 'Sorry. Cannot Upload more than 1 PDF' } })
          return;
        }

        if (file.some(list => list.type.includes(addedtype))) {
          if (droppedFiles[0].size === 0) {
            setSpError({ ...spError, ...{ droppedFiles: 'Previous Item was Replaced' } })
            setDroppedFiles([...file])
          } else {
            setSpError({ ...spError, ...{ droppedFiles: false } })
            setDroppedFiles([...droppedFiles, ...file])
          }
        } else {
          setSpError({ ...spError, ...{ droppedFiles: 'Please upload files with same filetype' } })
        }
      } else {
        setSpError({ ...spError, ...{ droppedFiles: false } })
        setDroppedFiles([...droppedFiles, ...file])
      }
    }
  }

  const removingFile = (file: FileWithPath, index: number) => {
    let newFileSet = [...droppedFiles];
    newFileSet.splice(index, 1)
    setDroppedFiles(newFileSet)
    setSpError({ ...spError, ...{ droppedFiles: false } })
  }

  const addNAssign = async (mode: string) => {
    if (courseIndex < 0) {
      setError(
        'courseIndex',
        'Invalid Data',
        'Please Select a Course'
      );
      return;
    } else {
      clearError('courseIndex');
    }

    if (assignmentname.length < 1) {
      setError(
        'assignmentname',
        'Invalid Data',
        'Please fill in Assignment name'
      );
      return;
    } else {
      clearError('assignmentname');
    }

    if (marks < 1) {
      setError(
        'marks',
        'Invalid Data',
        'Marks cannot be less than 1'
      );
      return;
    } else {
      clearError('marks');
    }

    if (batchSelList.length < 1) {
      setSpError({ ...spError, ...{ batchSelList: 'Please select a Batch' } })
      return;
    } else {
      setSpError({ ...spError, ...{ batchSelList: false } })
    }

    if (studentSelList.length < 1) {
      setSpError({ ...spError, ...{ studentSelList: 'Please select Students' } })
      return;
    } else {
      setSpError({ ...spError, ...{ studentSelList: false } })
    }

    if (brief.length < 1) {
      setError(
        'brief',
        'Invalid Data',
        'Please fill in Brief'
      );
      return;
    } else {
      clearError('brief');
    }

    if (instructions.length < 1) {
      setError(
        'instructions',
        'Invalid Data',
        'Please fill in the Instructions'
      );
      return;
    } else {
      clearError('instructions');
    }

    if (!startDate) {
      setSpError({ ...spError, ...{ startDate: 'Please Enter a Start Date' } })
      return;
    } else {
      setSpError({ ...spError, ...{ startDate: false } })
    }

    if (startDate < new Date()) {
      setSpError({ ...spError, ...{ startDate: 'Start Date cannot be in the past' } })
      return;
    } else {
      setSpError({ ...spError, ...{ startDate: false } })
    }

    if (!endDate) {
      setSpError({ ...spError, ...{ endDate: 'Please Enter an End Date' } })
      return;
    } else {
      setSpError({ ...spError, ...{ endDate: false } })
    }

    if (endDate < startDate) {
      setSpError({ ...spError, ...{ endDate: 'End Date cannot be less than Start Date' } })
      return;
    } else {
      setSpError({ ...spError, ...{ endDate: false } })
    }

    if (droppedFiles.length < 1) {
      setSpError({ ...spError, ...{ droppedFiles: 'Please drop an Attachment' } })
      return;
    } else {
      setSpError({ ...spError, ...{ droppedFiles: false } })
    }

    let selectedBatTutor: string[] = []
    if (isOrganization(authUser)) {
      const selectedBat = batches.filter(bat => batchSelList.includes(bat._id as string))
      selectedBatTutor = selectedBat.map(bat => (bat.tutorId as Tutor)._id as string)
    }

    try {
      handleBackdropOpen()
      if (droppedFiles[0].type.includes('image')) {
        const formData = new FormData();
        droppedFiles.forEach((file) => formData.append('documents', file))

        await convertToPdfNew(formData, AssignmentDocType.TASK_DOC).then(async (res) => {
          const newTaskDoc: taskDoc = {
            taskDocType: AssignmentDocType.TASK_DOC,
            taskDocFormat: 'application/pdf',
            taskDocLocation: 'images.pdf',
            uuid: res.uuid,
          }

          const thiscourse = courses[courseIndex]
          const newAssignment: Assignment = {
            ...draftAssignment,
            ...{
              boardname: thiscourse.board,
              classname: thiscourse.className,
              subjectname: thiscourse.subject,
              assignmentname: assignmentname,
              marks: marks,
              instructions: instructions,
              brief: brief,
              startDate: startDate,
              endDate: endDate,
              batches: batchSelList,
              students: studentSelList,
              taskDocs: newTaskDoc,
              tutorId: isOrganization(authUser) ? selectedBatTutor : [authUser._id as string]
            }
          }

          if (mode === 'draft') {
            await createAssignmentDraftNew(newAssignment);
            handleBackdropClose()
            setRedirectTo('/profile/assignment')
          } else {
            await createAssignmentNew(newAssignment)
            handleBackdropClose()
            setRedirectTo('/profile/assignment/published')
          }
        })
      } else {
        if (draftAssignment && droppedFiles[0].size === 0) {
          const thiscourse = courses[courseIndex]
          const newAssignment: Assignment = {
            ...draftAssignment,
            ...{
              boardname: thiscourse.board,
              classname: thiscourse.className,
              subjectname: thiscourse.subject,
              assignmentname: assignmentname,
              marks: marks,
              instructions: instructions,
              brief: brief,
              startDate: startDate,
              endDate: endDate,
              batches: batchSelList,
              students: studentSelList,
              taskDocs: draftAssignment.taskDocs,
              tutorId: isOrganization(authUser) ? selectedBatTutor : [authUser._id as string]
            }
          }
          if (mode === 'draft') {
            await createAssignmentDraftNew(newAssignment)
            handleBackdropClose()
            setRedirectTo('/profile/assignment')
          } else {
            await createAssignmentNew(newAssignment)
            handleBackdropClose()
            setRedirectTo('/profile/assignment/published')
          }
        } else {
          const file = droppedFiles[0]
          const awsBucket = await fetchUploadUrlForDocNew({
            fileName: file.name,
            contentType: file.type,
            docType: AssignmentDocType.TASK_DOC,
          });

          await uploadDoc(awsBucket.url, file, file.type).then(async () => {
            const newTaskDoc: taskDoc = {
              taskDocType: AssignmentDocType.TASK_DOC,
              taskDocFormat: file.type,
              taskDocLocation: file.name,
              uuid: awsBucket.uuid,
            }

            const thiscourse = courses[courseIndex]
            const newAssignment: Assignment = {
              ...draftAssignment,
              ...{
                boardname: thiscourse.board,
                classname: thiscourse.className,
                subjectname: thiscourse.subject,
                assignmentname: assignmentname,
                marks: marks,
                instructions: instructions,
                brief: brief,
                startDate: startDate,
                endDate: endDate,
                batches: batchSelList,
                students: studentSelList,
                taskDocs: newTaskDoc,
                tutorId: isOrganization(authUser) ? selectedBatTutor : [authUser._id as string]
              }
            }

            if (mode === 'draft') {
              await createAssignmentDraftNew(newAssignment)
              handleBackdropClose()
              setRedirectTo('/profile/assignment')
            } else {
              await createAssignmentNew(newAssignment)
              handleBackdropClose()
              setRedirectTo('/profile/assignment/published')
            }
          })
        }
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      handleBackdropClose()
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }

  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
        <Container className='create-assignment' maxWidth="lg" style={{ padding: '30px 2.5%' }}>
          <Box
            bgcolor="#F9BD33"
            padding="20px 30px"
            marginBottom="30px"
            position="relative"
            borderRadius="14px"
            color='#fff'
          >
            <Grid item container>
              <Grid item lg={8} xl={8} md={8} xs={12} sm={12}>
                <Box style={{ height: '100%' }}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <Typography className={`header-content ${classes.title}`}>
                        Create an Assignment
                    </Typography>
                      <Typography className='header-content'>
                        Create Assignment | Assign it to Batches
                    </Typography>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
              <Grid item lg={4} xl={4} md={4} xs={12} sm={12}>
                <Box style={{ height: '100%' }}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12} className='create-assignment-img'>
                      <img src={AssiSheet} alt="books" style={{ height: '150px', float: 'right' }} />
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="25px"
            marginTop='25px'
          >
            <Grid container spacing={1}>
              <Grid item xs={12} style={{ marginBottom: '25px' }}>
                <Typography style={{ color: '#3D3D3D', fontSize: fontOptions.size.medium }}>
                  Assignment Information
              </Typography>
              </Grid>
              <Grid item xs={12} md={6} sm={12} lg={6} xl={6}>
                <Grid container>

                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Course</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Select
                        value={courseIndex}
                        className={errors.courseIndex ? classes.error : ''}
                        onChange={(
                          e: React.ChangeEvent<{ value: unknown }>
                        ) => setCourseIndex(e.target.value as number)}
                      >
                        <MenuItem value="-1">Select course</MenuItem>
                        {courses.map((course, index) => (
                          <MenuItem key={index} value={index}>
                            {course.board} - {course.className} -{' '}
                            {course.subject}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {errors.courseIndex && (
                      <FormHelperText error>
                        {errors.courseIndex.message}
                      </FormHelperText>
                    )}
                  </Grid>

                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Name</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Input
                        placeholder="Assignment Name"
                        value={assignmentname}
                        inputProps={{ maxLength: 50 }}
                        onChange={(e) => setAssignmentname(e.target.value)}
                        className={errors.assignmentname ? `${classes.error} ${classes.formInput}` : classes.formInput}
                      />
                    </FormControl>
                    {errors.assignmentname && (
                      <FormHelperText error>
                        {errors.assignmentname.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Total Marks</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Input
                        placeholder="Total Marks"
                        type="number"
                        value={marks}
                        inputProps={{ maxLength: 4 }}
                        onChange={(e) => setMarks(Number(e.target.value))}
                        className={errors.marks ? `${classes.error} ${classes.formInput}` : classes.formInput}
                      />
                    </FormControl>
                    {errors.marks && (
                      <FormHelperText error>
                        {errors.marks.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Batch</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Select
                        labelId="demo-mutiple-checkbox-label"
                        id="demo-mutiple-checkbox"
                        multiple
                        value={batchSelList}
                        onChange={handleChange}
                        input={<Input />}
                        renderValue={(selected) => {
                          return (
                            (selected as string[]).map(sel => {
                              const selbat = (batches.find(bat => bat._id === sel)) as Batch
                              return selbat.batchfriendlyname
                            }).join(', ')
                          )
                        }}
                        MenuProps={MenuProps}
                      >
                        {batches.map((bat) => (
                          <MenuItem key={bat._id} value={bat._id}>
                            <Checkbox color="primary" checked={batchSelList.indexOf(bat._id as string) > -1} />
                            <ListItemText primary={bat.batchfriendlyname} />
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {spError.batchSelList &&
                      <Typography className={classes.spError}>{spError.batchSelList}</Typography>
                    }
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Students</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Select
                        labelId="demo-mutiple-checkbox-label"
                        id="demo-mutiple-checkbox"
                        multiple
                        value={studentSelList}
                        onChange={handleStudentChange}
                        input={<Input />}
                        renderValue={(selected) => {
                          return (
                            (selected as string[]).map(sel => {
                              const selstu = (students.find(stu => stu._id === sel)) as Student
                              return selstu.studentName
                            }).join(', ')
                          )
                        }}
                        MenuProps={MenuProps}
                      >
                        {students.map((stu) => (
                          <MenuItem key={stu._id} value={stu._id}>
                            <Checkbox color="primary" checked={studentSelList.indexOf(stu._id as string) > -1} />
                            <ListItemText primary={stu.studentName} />
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {spError.studentSelList &&
                      <Typography className={classes.spError}>{spError.studentSelList}</Typography>
                    }
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12} md={6}>
                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Brief</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Input
                        placeholder="Brief"
                        value={brief}
                        inputProps={{ maxLength: 200 }}
                        onChange={(e) => setBrief(e.target.value)}
                        className={errors.brief ? `${classes.error} ${classes.formInput}` : classes.formInput}
                      />
                    </FormControl>
                    {errors.brief && (
                      <FormHelperText error>
                        {errors.brief.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Instructions</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Input
                        placeholder="Instructions"
                        value={instructions}
                        multiline
                        rows={4}
                        inputProps={{ maxLength: 200 }}
                        onChange={(e) => setInstructions(e.target.value)}
                        className={errors.instructions ? `${classes.error} ${classes.formInput}` : classes.formInput}
                      />
                    </FormControl>
                    {errors.instructions && (
                      <FormHelperText error>
                        {errors.instructions.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Start Date</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Datetimepickers selectedDate={startDate} handleDateChange={handleStartDateChange}
                        minDate={new Date()} disablePast={true}
                      />
                    </FormControl>
                    {spError.startDate &&
                      <Typography className={classes.spError}>{spError.startDate}</Typography>
                    }
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} sm={12} lg={4} xl={4} md={4}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>End Date</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8} lg={8} xl={8} sm={12}>
                    <FormControl style={{ width: '100%' }} margin="normal">
                      <Datetimepickers selectedDate={endDate} handleDateChange={handleEndDateChange}
                        minDate={startDate} disablePast={true}
                      />
                    </FormControl>
                    {spError.endDate &&
                      <Typography className={classes.spError}>{spError.endDate}</Typography>
                    }
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} md={2}>
                    <FormControl style={{ width: '100%', paddingTop: '7px' }} margin="normal">
                      <Box>Upload Content</Box>
                      <Typography style={{ fontSize: fontOptions.size.small, color: '#666666' }}>Drag and arrange</Typography>
                      <Typography style={{ fontSize: fontOptions.size.small, color: '#666666' }}>Support Format: Either JPG/PNG or PDF</Typography>
                      {draftAssignment &&
                        <Typography style={{ fontSize: fontOptions.size.small, color: '#666666' }}>On Reupload, Existing file will be replaced</Typography>
                      }
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={10}>
                    <FormControl style={{ width: '91%' }} margin="normal">
                      <GridPreview
                        files={droppedFiles}
                        onRemoveItem={(file, index) => removingFile(file, index)}
                        setDroppedFiles={setDroppedFiles}
                      />
                      <Dropzone
                        acceptedFiles={['image/jpeg', 'image/png', 'application/pdf']}
                        maxFileSize={104857600} // 100 MB
                        files={droppedFiles}
                        onChange={(files) => addingFile(files)}
                        xtraClasses={{
                          root: classes.dropzoneRoot,
                          textContainer: classes.dropzoneTextContainerRoot,
                          text: classes.dropzoneText,
                          icon: classes.dropzoneIcon
                        }}
                        dropzoneText="Upload your file here"
                      />
                    </FormControl>
                    {spError.droppedFiles &&
                      <Typography className={classes.spError}>{spError.droppedFiles}</Typography>
                    }
                  </Grid>
                </Grid>
              </Grid>              

              <Grid item xs={12}>
                <span style={{ float: 'right', paddingRight: '7%', paddingTop: '25px' }} className={`${classes.addAssi} button-box`}>
                  <Button color="primary" style={{marginBottom:'20px'}}  disableElevation variant="outlined" onClick={() => addNAssign('draft')}>
                    Save in Drafts
                </Button>
                  <Button style={{ marginLeft: '15px' }} className='box-container' color="primary" disableElevation variant="contained" onClick={() => addNAssign('create')}>
                    Create
                </Button>
                </span>
              </Grid>
            </Grid>
          </Box>
        </Container>
        <Backdrop className={classes.backdrop} open={open}>
          <div style={{ textAlign: 'center' }}>
            <CircularProgress color="inherit" />
            <Typography style={{ fontSize: fontOptions.size.medium, marginTop: '5px' }}>Your file are being uploaded. Please Wait</Typography>
          </div>
        </Backdrop>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(CreateAssignment));
