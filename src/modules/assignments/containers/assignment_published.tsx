import React, { FunctionComponent, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import Button from '../../common/components/form_elements/button';
import { fontOptions } from '../../../theme';
import AssiPubBg from '../../../assets/svgs/assiPubBg.svg'
import AssiPubThank from '../../../assets/svgs/assiPubThank.svg'
import AssiPubSuccess from '../../../assets/svgs/assiPubSuccess.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    layoutRoot: {
      background:'url(' +AssiPubBg +')',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom right',
      backgroundRepeat: 'no-repeat',
      minHeight: '100vh',
      zIndex: -1
    },
    layoutGrid: {
      height: '86vH',
    },
    backBtn: {
      '& button': {
        fontSize: fontOptions.size.medium,
        padding: '15px'
      }
    }
  }),
);

const PublishedAssignment: FunctionComponent = () => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState('');

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div className={classes.layoutRoot}>
      <MiniDrawer>
      <Grid container className={classes.layoutGrid}>
        <Grid item xs={12} md={5}>
          <Box height="100%">
            <Box style={{height: '100%'}}>
              <Box
                position="relative"
                top="20%"
                textAlign='center'
              >
                <img src={AssiPubThank} alt="Thank" style={{ height: '25vH'}} />
              </Box>
              <Box
                position="relative"
                top="50%"
                textAlign='center'
                className={classes.backBtn}
              >
                <Button color="primary" disableElevation variant="contained" onClick={() => setRedirectTo('/profile/assignment')}>
                  Assignment Home
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} md={5}>
          <Box height="100%">
            <Box style={{height: '100%'}}>
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '100%', textAlign: 'center' }}
              >
                <Grid item xs={12}>
                  <img src={AssiPubSuccess} alt="success" style={{paddingLeft: '60%', height: '70vH'}} />
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
      </MiniDrawer>
    </div>
  );
}

export default PublishedAssignment