export { default as ListAssignment } from "./assignment_list";
export { default as CreateAssignment } from "./assignment_create";
export { default as PublishedAssignment } from "./assignment_published";
export { default as StudentUpload } from "./student_upload";
export { default as BatchSubmissions } from "./assignment_batchSub";
export { default as TutorEvaluation } from "./assignment_evaluation";
export { default as TutorCorrection } from "./assignment_correction";
export { default as SubmittedAssignment } from "./assignment_submitted";
export { default as StudentViewMarks } from "./assignment_viewmarks";
export { default as ViewPDF } from "./pdf_view";
export { default as StudentReupload } from "./student_reupload";
export { default as StudentWait } from "./student_wait";
