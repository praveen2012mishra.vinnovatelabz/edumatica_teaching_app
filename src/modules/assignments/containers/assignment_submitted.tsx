import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import Button from '../../common/components/form_elements/button';
import { fontOptions } from '../../../theme';
import AssiPubBg from '../../../assets/svgs/assiPubBg.svg'
import ThankSubmit from '../../../assets/svgs/thankSubmit.svg'
import ThankReview from '../../../assets/svgs/thankReview.svg'
import StuThank from '../../../assets/svgs/stuThank.svg'
import { RouteComponentProps, withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    layoutRoot: {
      background:'url(' +AssiPubBg +')',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom right',
      backgroundRepeat: 'no-repeat',
      minHeight: '100vh',
      zIndex: -1
    },
    layoutGrid: {
      height: '86vH',
    },
    backBtn: {
      '& button': {
        fontSize: fontOptions.size.medium,
        padding: '15px'
      }
    }
  }),
);

interface Props extends RouteComponentProps { }

const SubmittedAssignment: FunctionComponent<Props> = ({location}) => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState('');
  const [image, setImage] = useState('')

  useEffect(() => {
    const params = new URLSearchParams(location.search);
      const type = params.get('type');
      if(type && type === 'review') {
        setImage(ThankReview)
      } else {
        setImage(ThankSubmit)
      }
  }, [location.search])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div className={classes.layoutRoot}>
      <MiniDrawer>
      <Grid container className={classes.layoutGrid}>
        <Grid item xs={12}>
          <Box height="100%">
            <Box style={{height: '100%'}}>
              <Box
                position="relative"
                top="10%"
                textAlign='center'
              >
                <img src={image} alt="Thank" style={{ height: '15vH'}} />
                <br /><br />
                <img src={StuThank} alt="Thank" style={{ height: '35vH'}} />
              </Box>
              <Box
                position="relative"
                top="25%"
                textAlign='center'
                className={classes.backBtn}
              >
                <Button color="primary" disableElevation variant="contained" onClick={() => setRedirectTo('/profile/assignment')}>
                  Assignment Home
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
      </MiniDrawer>
    </div>
  );
}

export default withRouter(SubmittedAssignment);
