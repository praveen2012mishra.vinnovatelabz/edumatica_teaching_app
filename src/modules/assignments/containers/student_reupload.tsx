import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Backdrop, CircularProgress, FormControl, Paper } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import { User, Children } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAssignedAssignmentNew } from '../../common/api/assignment';
import { exceptionTracker } from '../../common/helpers';
import Button from '../../common/components/form_elements/button';
import { isParent } from '../../common/helpers';
import { AssignedAssignment, AssignmentDocType, answerDoc, AssignedStudent } from '../contracts/assignment_interface';
import AssiUploadDet from '../../../assets/svgs/assiUploadDet.svg'
import Dropzone from '../../common/components/dropzone/dropzone';
import GridPreview from '../../common/components/dropzone/previewers/gridViewUploadContent'
import IconCal from '../../../assets/svgs/iconCal.svg'
import IconNumb from '../../../assets/svgs/iconNumb.svg'
import IconUser from '../../../assets/svgs/iconUser.svg'
import { FileWithPath } from 'react-dropzone';
import { fetchDownloadUrlForDocNew, convertToPdfNew, submitAssignmentNew, fetchUploadUrlForDocNew, uploadDoc } from '../../common/api/assignment';
import {useSnackbar} from "notistack"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    batchSelect: {
      paddingTop: '12px'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    dsnBtn: {
      '& button': {
        backgroundColor: '#F5CC0A',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#F5CC0A',
        }
      },
    },
    submitBtn: {
      '& button': {
        padding: '10px 20px',
      },
      float: 'right'
    },
    spError: {
      fontSize: fontOptions.size.small,
      color: '#F44E41'
    },
    dropzoneRoot: {
      width: '100%',
      minHeight: '200px',
      border: '2.5px dashed rgba(76, 139, 245, 0.5)',
      boxSizing: 'border-box',
      borderRadius: '5px'
    },
    dropzoneTextContainerRoot: {
     paddingTop: '3%' 
    },
    dropzoneText: {
      fontFamily:fontOptions.family,
      fontSize: fontOptions.size.medium,
      fontWeight: fontOptions.weight.bold,
      color: '#4C8BF5',
      margin: '20px 0',
    },
    dropzoneIcon: {
      color: '#4C8BF5'
    },
  }),
);

interface SpError {
  droppedFiles: string | boolean;
}

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const StudentReupload: FunctionComponent<Props> = ({authUser, parentChildren, location})  => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignment>()
  const [droppedFiles, setDroppedFiles] = useState<FileWithPath[]>([]);
  const [spError, setSpError] = useState<SpError>({droppedFiles: false})
  const [stuId, setStuId] = useState('');
  const [assiId, setAssiId] = useState('');
  const [reupload, setReupload] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [myDetails, setMyDetails] = useState<AssignedStudent>()

  useEffect(() => {
    (async () => {
      try {
        const params = new URLSearchParams(location.search);
        const studentId = params.get('studentId');
        const assignmentId = params.get('assignmentId')
        if(studentId && assignmentId) {
          const assignedAssignmentResp = await fetchAssignedAssignmentNew(studentId, assignmentId)
          setStuId(studentId)
          setAssiId(assignmentId)
          setAssignedAssignment(assignedAssignmentResp[0])
          setAssignedAssignment(assignedAssignmentResp[0])
          if(assignedAssignmentResp[0]) {
            const thisStu = assignedAssignmentResp[0].students.find(list => list.studentId === studentId)
            setMyDetails(thisStu)
          }
        } else {
          setRedirectTo('/profile/assignment')
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [location.search])

  const downloadAttachment = async() => {
    if(assignedAssignment && assignedAssignment.assignment.taskDocs.uuid) {
      const { uuid, taskDocFormat, taskDocLocation } = assignedAssignment.assignment.taskDocs;
        await fetchDownloadUrlForDocNew({
          uuid: uuid,
          contentType: taskDocFormat,
          fileName: taskDocLocation,
          docType: AssignmentDocType.TASK_DOC,
        }).then((value) => {
          setTimeout(() => {
            const response = {
              file: value.url,
            };
            window.open(response.file);
          }, 100);
        })
    }
  }

  const reuploadRedirect = async() => {
    setRedirectTo(`/profile/assignment/studentupload?type=reupload&assignmentId=${assiId}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`)
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg" style={{padding: '30px 2.5%'}}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8}>
            <Box
              bgcolor="#4C8BF5"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              color='#fff'
              height="250px"
            >
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '100%' }}
              >
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12}>
                      <Box className={classes.dsnBtn}>
                        <Button disableElevation disableFocusRipple disableRipple disableTouchRipple>
                          {assignedAssignment?.assignment.subjectname}
                        </Button>
                      </Box>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '10px'}}>
                      <Typography style={{color: "#FFFFFF", fontSize: fontOptions.size.large, fontWeight: fontOptions.weight.bold}}>
                        Assignment:{' ' + assignedAssignment?.assignment.assignmentname}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '7px'}}>
                      <Typography style={{color: "#FFFFFF"}}>
                        {assignedAssignment?.assignment.brief}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box
              bgcolor="#FFFFFF"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              height="250px"
            >
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '100%' }}
              >
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconNumb} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Marks</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{assignedAssignment?.assignment.marks}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconCal} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Deadline</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{new Date(assignedAssignment?.assignment.endDate as Date).toLocaleString()}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                         <img src={IconUser} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Owner</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{assignedAssignment?.assignment.ownerId}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '15px'}}>
                      <Button color="primary" variant="outlined" style={{padding: '10px 20px'}}
                        onClick={downloadAttachment}
                      >
                        View Attachment
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
        <Box
          bgcolor="#ffffff"
          borderRadius="14px"
          padding="25px"
          marginTop='15px'
        >
          <Grid container>
            <Grid item xs={12} md={6} style={{paddingBottom: '15px'}}>
              <Typography style={{fontSize: fontOptions.size.mediumPlus, fontWeight: fontOptions.weight.bold, color: '#3D3D3D'}}>
                Submission
              </Typography>
              <div style={{ width: '100%', marginTop: '15px' }}>
                <Box display="flex" p={1} border="2px solid #4C8BF5">
                  <Box p={1} flexGrow={1}>
                    <Typography style={{marginTop: '5px'}}>{myDetails?.uploadedAnswerDocs?.answerDocLocation}</Typography>
                  </Box>
                  <Box p={1}>
                    <Button color="primary" variant="contained" onClick={reuploadRedirect}>
                      Reupload
                    </Button>
                  </Box>
                </Box>
              </div>
            </Grid>
            <Grid item xs={12} md={6} style={{paddingBottom: '15px'}}>

            </Grid>
          </Grid>
        </Box>
      </Container>
      <Backdrop className={classes.backdrop} open={open}>
        <div style={{textAlign: 'center'}}>
          <CircularProgress color="inherit" />
          <Typography style={{fontSize: fontOptions.size.medium, marginTop: '5px'}}>Your file are being uploaded. Please Wait</Typography>
        </div>
      </Backdrop>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(StudentReupload));
