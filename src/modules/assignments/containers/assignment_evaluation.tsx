import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Select, MenuItem, FormControl, Paper } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import { User, Children } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAssignedAssignmentNew } from '../../common/api/assignment';
import { exceptionTracker } from '../../common/helpers';
import Button from '../../common/components/form_elements/button';
import { AssignedAssignment, AssignmentDocType, answerDoc, AssignedStudent } from '../contracts/assignment_interface';
import AssiUploadDet from '../../../assets/svgs/assiUploadDet.svg'
import Dropzone from '../../common/components/dropzone/dropzone';
import GridPreview from '../../common/components/dropzone/previewers/gridViewUploadContent'
import { FileWithPath } from 'react-dropzone';
import { fetchDownloadUrlForDocNew, convertToPdfNew, submitAssignmentNew } from '../../common/api/assignment';
import {useSnackbar} from "notistack"
import AssiEvaluation from '../../../assets/svgs/assiEvaluation.svg'
import { isOrganization, isAdminTutor, isOrgTutor } from '../../common/helpers';
import { fetchTutorStudentsListid } from '../../common/api/academics';
import { getOrgStudentsList } from '../../common/api/organization';
import IconCal from '../../../assets/svgs/iconCal.svg'
import IconNumb from '../../../assets/svgs/iconNumb.svg'
import IconUser from '../../../assets/svgs/iconUser.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    batchSelect: {
      paddingTop: '12px'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    dsnBtn: {
      '& button': {
        backgroundColor: '#F5CC0A',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#F5CC0A',
        }
      },
    },
    submitBtn: {
      '& button': {
        padding: '10px 20px',
      },
      float: 'right'
    },
  }),
);

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const TutorEvaluation: FunctionComponent<Props> = ({authUser, parentChildren, location})  => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignment>()
  const [stuId, setStuId] = useState('');
  const [assiId, setAssiId] = useState('');
  const [ansDocs, setAnsDocs] = useState('')
  const [studentDetails, setStudentDetails] = useState<AssignedStudent>()
  const [studentName, setStudentName] = useState('')

  useEffect(() => {
    (async () => {
      try {
        const params = new URLSearchParams(location.search);
        const assignmentId = params.get('assignmentId');
        const studentId = params.get('studentId');
        if(studentId && assignmentId) {
          const assignedAssignmentResp = await fetchAssignedAssignmentNew(studentId, assignmentId)
          setStuId(studentId)
          setAssiId(assignmentId)
          setAssignedAssignment(assignedAssignmentResp[0])
          const ansDoc = assignedAssignmentResp[0].students.find(list => list.studentId === studentId)?.uploadedAnswerDocs?.answerDocLocation
          setAnsDocs(ansDoc as string)
          if(assignedAssignmentResp[0]) {
            const thisStu = assignedAssignmentResp[0].students.find(list => list.studentId === studentId)
            setStudentDetails(thisStu)

            const tutorid = (isAdminTutor(authUser) || isOrgTutor(authUser)) ? (authUser._id) : (authUser)._id
            const studentsList = isOrganization(authUser) ? await getOrgStudentsList() : await fetchTutorStudentsListid(tutorid as string);
            const studentname = studentsList.find(list => list._id === studentId)?.studentName
            setStudentName(studentname as string)
          }
        } else {
          setRedirectTo('/profile/assignment')
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [location.search])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg" style={{padding: '30px 2.5%'}}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8}>
          <Box
              bgcolor="#4C8BF5"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              color='#fff'
            >
              <Grid item container>
                <Grid item sm={8}>
                  <Box style={{height: '100%'}}>
                    <Grid
                      container
                      direction="row"
                      alignItems="center"
                      justify="center"
                      style={{ height: '100%' }}
                    >
                      <Grid item xs={12}>
                        <Typography style={{color: "#FFFFFF", fontSize: fontOptions.size.small}}>
                          {assignedAssignment?.assignment.subjectname}
                        </Typography>
                        <Typography className={classes.title}>
                          Assignment:{' ' + assignedAssignment?.assignment.assignmentname}
                        </Typography>
                        <Typography style={{color: "#FFFFFF"}}>
                          {assignedAssignment?.assignment.brief}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
                <Grid item sm={4}>
                  <Box style={{height: '100%'}}>
                    <Grid
                      container
                      direction="row"
                      alignItems="center"
                      justify="center"
                      style={{ height: '100%' }}
                    >
                      <Grid item xs={12}>
                        <img src={AssiEvaluation} alt="books" style={{height: '150px', float: 'right'}}/>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box
              bgcolor="#FFFFFF"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              height="200px"
            >
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '100%' }}
              >
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconUser} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Name</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{studentName}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                         <img src={IconCal} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                        <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Submitted On</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{new Date(studentDetails?.submittedOn as Date).toLocaleString()}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconNumb} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Full Marks</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{assignedAssignment?.assignment.marks}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
        <Box
          bgcolor="#ffffff"
          borderRadius="14px"
          padding="25px"
          marginTop='15px'
        >
          <Grid container>
            <Grid item xs={12} md={6} style={{paddingBottom: '15px'}}>
              <Typography style={{fontSize: fontOptions.size.mediumPlus, fontWeight: fontOptions.weight.bold, color: '#3D3D3D'}}>
                Submission
              </Typography>
              <div style={{ width: '100%', marginTop: '15px' }}>
                <Box display="flex" p={1} border="2px solid #4C8BF5">
                  <Box p={1} flexGrow={1}>
                    <Typography style={{marginTop: '5px'}}>{ansDocs}</Typography>
                  </Box>
                  <Box p={1}>
                    <Button color="primary" variant="contained" 
                      onClick={() => {
                        setRedirectTo(`/profile/assignment/correction?assignmentId=${assiId}&studentId=${stuId}`)
                      }}
                    >
                      View
                    </Button>
                  </Box>
                </Box>
              </div>
            </Grid>
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(TutorEvaluation));
