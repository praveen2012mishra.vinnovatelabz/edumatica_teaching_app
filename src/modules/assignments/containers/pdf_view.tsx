import React, { FunctionComponent, useEffect,useState } from 'react';
import { answerDoc } from '../contracts/assignment_interface';
import AssignmentPdfViewer from '../components/pdf-viewer/assignment_pdf_viewer'

interface Props {
  history: object,
  location: {
    state: {
      pdfUrl: string;
      tutorHighlights: any;
      selectedDoc: answerDoc;
    }
  },
  match: object
}

const ViewPDF: FunctionComponent<Props> = ({location}) => {
  const [tutorHighlights, setTutorHighlights] = useState<any>()
  const [selectedDoc, setSelectedDoc] = useState<answerDoc>()
  const [pdfUrl, setPdfUrl] = useState('')

  useEffect(() => {
    if(location.state && location.state.pdfUrl && location.state.tutorHighlights && location.state.selectedDoc) {
      setTutorHighlights(location.state.tutorHighlights)
      setSelectedDoc(location.state.selectedDoc)
      setPdfUrl(location.state.pdfUrl)
    }
  }, [location])
  return (
    <div>
      {pdfUrl &&
        <AssignmentPdfViewer url={pdfUrl} highlights={tutorHighlights ? tutorHighlights[(selectedDoc as answerDoc).answerDocLocation] : undefined} isStudent={true} />
      }
    </div>
  );
}

export default ViewPDF;
