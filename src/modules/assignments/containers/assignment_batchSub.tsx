import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Select, MenuItem, FormControl, Paper, Chip } from '@material-ui/core';
import { makeStyles, createStyles, Theme, lighten } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import { User, Children, Tutor, Student } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAssignedAssignmentNew } from '../../common/api/assignment';
import { exceptionTracker } from '../../common/helpers';
import Button from '../../common/components/form_elements/button';
import { AssignedAssignment, AssignmentDocType, answerDoc } from '../contracts/assignment_interface';
import AssiUploadDet from '../../../assets/svgs/assiUploadDet.svg'
import AssiEvaluation from '../../../assets/svgs/assiEvaluation.svg'
import Dropzone from '../../common/components/dropzone/dropzone';
import GridPreview from '../../common/components/dropzone/previewers/gridViewUploadContent'
import { FileWithPath } from 'react-dropzone';
import { fetchDownloadUrlForDocNew, convertToPdfNew, submitAssignmentNew } from '../../common/api/assignment';
import {useSnackbar} from "notistack"
import { isOrganization, isStudent, isParent, isAdminTutor, isOrgTutor } from '../../common/helpers';
import { fetchBatchesList, fetchTutorStudentsListid } from '../../common/api/academics';
import { fetchOrgBatchesList } from '../../common/api/batch';
import { Batch } from '../../academics/contracts/batch';
import {
  getOrgStudentsList,
} from '../../common/api/organization';
import IconCal from '../../../assets/svgs/iconCal.svg'
import IconNumb from '../../../assets/svgs/iconNumb.svg'
import IconUser from '../../../assets/svgs/iconUser.svg'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    batchSelect: {
      paddingTop: '12px'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    dsnBtn: {
      '& button': {
        backgroundColor: '#F5CC0A',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#F5CC0A',
        }
      },
    },
    submitBtn: {
      '& button': {
        padding: '10px 20px',
      },
      float: 'right'
    },
  }),
);

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const BatchSubmissions: FunctionComponent<Props> = ({authUser, parentChildren, location})  => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignment>()
  const [batId, setBatId] = useState('');
  const [assiId, setAssiId] = useState('');
  const [batches, setBatches] = useState<Batch[]>([])
  const [selBatch, setSelBatch] = useState<Batch>()
  const [ourStudent, setOurStudent] = useState<Student[]>()

  useEffect(() => {
    (async () => {
      try {
        const params = new URLSearchParams(location.search);
        const assignmentId = params.get('assignmentId');
        const batchId = params.get('batchId')
        if(batchId && assignmentId) {
          const assignedAssignmentResp = await fetchAssignedAssignmentNew(undefined, assignmentId)
          const batchesList = (isOrganization(authUser) ?
            await fetchOrgBatchesList() : 
            (isStudent(authUser) ? 
              []: 
              (isParent(authUser) ? 
                [] :
                await fetchBatchesList()
              )
            )
          );
          setBatId(batchId)
          setAssiId(assignmentId)
          setBatches(batchesList)
          setAssignedAssignment(assignedAssignmentResp[0])
          const seleBatch = batchesList.find(list => list._id === batchId)
          const tutorid = (isAdminTutor(authUser) || isOrgTutor(authUser)) ? ((seleBatch as Batch).tutorId) : ((seleBatch as Batch).tutorId as Tutor)._id
          const studentsList = isOrganization(authUser) ? await getOrgStudentsList() : await fetchTutorStudentsListid(tutorid as string);
          setOurStudent(studentsList)
          setSelBatch(seleBatch)
        } else {
          setRedirectTo('/profile/assignment')
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [location.search])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg" style={{padding: '30px 2.5%'}}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8}>
            <Box
              bgcolor="#4C8BF5"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              color='#fff'
            >
              <Grid item container>
                <Grid item sm={8}>
                  <Box style={{height: '100%'}}>
                    <Grid
                      container
                      direction="row"
                      alignItems="center"
                      justify="center"
                      style={{ height: '100%' }}
                    >
                      <Grid item xs={12}>
                        <Typography style={{color: "#FFFFFF", fontSize: fontOptions.size.small}}>
                          {assignedAssignment?.assignment.subjectname}
                        </Typography>
                        <Typography className={classes.title}>
                          Assignment:{' ' + assignedAssignment?.assignment.assignmentname}
                        </Typography>
                        <Typography style={{color: "#FFFFFF"}}>
                          {assignedAssignment?.assignment.brief}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
                <Grid item sm={4}>
                  <Box style={{height: '100%'}}>
                    <Grid
                      container
                      direction="row"
                      alignItems="center"
                      justify="center"
                      style={{ height: '100%' }}
                    >
                      <Grid item xs={12}>
                        <img src={AssiEvaluation} alt="books" style={{height: '150px', float: 'right'}}/>
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box
              bgcolor="#FFFFFF"
              padding="20px 30px"
              marginBottom="30px"
              position="relative"
              borderRadius="14px"
              height="200px"
            >
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                style={{ height: '100%' }}
              >
                <Grid item xs={12}>
                  <Grid container>
                    <Grid item xs={12}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconNumb} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Marks</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{assignedAssignment?.assignment.marks}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                          <img src={IconCal} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Deadline Date</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{new Date(assignedAssignment?.assignment.endDate as Date).toLocaleDateString()}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: '5px'}}>
                      <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                        <Grid item xs={2} style={{paddingTop: '5px'}}>
                         <img src={IconCal} alt="bullets" />
                        </Grid>
                        <Grid item xs={10}>
                          <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Deadline Time</Typography>
                          <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{new Date(assignedAssignment?.assignment.endDate as Date).toLocaleTimeString()}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
        <Box
          bgcolor="#ffffff"
          borderRadius="14px"
          padding="25px"
          marginTop='15px'
        >
          <Grid container>
            <Grid item xs={12}>
              <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold, color: '#3D3D3D'}}>Submissions</Typography>
            </Grid>
            {assignedAssignment?.students.filter(list => selBatch?.students.includes(list.studentId)).map(list => {
              return (
                <Grid key={list.studentId} item xs={12} style={{paddingTop: '10px'}}>
                  <Grid container style={{textAlign: 'left'}}>
                    <Grid item xs={12} sm={6} md={3}>
                      <Typography style={{color: '#3D3D3D'}}>
                        {ourStudent?.find(stu => stu._id === list.studentId)?.studentName}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                      {list.isSubmitted ? <Chip label="Submitted" variant="outlined" color="primary" style={{backgroundColor: lighten('#4C8BF5', 0.9)}} />  : <Chip label="Not Submitted" variant="outlined" style={{color: '#DC143C', border: '1px solid #DC143C', backgroundColor: lighten('#DC143C', 0.9)}} />}
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                      <Typography style={{color: '#3D3D3D'}}>
                        {list.submittedOn ? new Date(list.submittedOn).toLocaleString() : '-'}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3}>
                      <Button disableElevation variant="contained" color="primary"
                        onClick={() => {
                          setRedirectTo(`/profile/assignment/evaluate?assignmentId=${assiId}&studentId=${list.studentId}`)
                        }}
                        disabled={!list.isSubmitted}
                      >
                        View
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              )
            })}
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(BatchSubmissions));
