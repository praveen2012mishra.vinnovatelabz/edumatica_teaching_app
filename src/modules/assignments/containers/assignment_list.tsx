import React, { FunctionComponent} from 'react';
import { User } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { isStudent, isParent } from '../../common/helpers';
import TutorView from '../components/layouts/tutor_view'
import StudentView from '../components/layouts/student_view'

interface Props extends RouteComponentProps {
  authUser: User;
}

const ListAssignment: FunctionComponent<Props> = ({authUser})  => {
  if(isStudent(authUser) || isParent(authUser)) {
    return (
      <StudentView />
    )
  } else {
    return (
      <TutorView />
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(withRouter(ListAssignment));
