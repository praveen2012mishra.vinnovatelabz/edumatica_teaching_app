import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Select, MenuItem, FormControl, Paper, TextField } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import { User, Children } from '../../common/contracts/user';
import { RootState } from '../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAssignedAssignmentNew } from '../../common/api/assignment';
import { exceptionTracker } from '../../common/helpers';
import Button from '../../common/components/form_elements/button';
import { AssignedAssignment, AssignmentDocType, answerDoc, TutorFeedback } from '../contracts/assignment_interface';
import AssiUploadDet from '../../../assets/svgs/assiUploadDet.svg'
import Dropzone from '../../common/components/dropzone/dropzone';
import GridPreview from '../../common/components/dropzone/previewers/gridViewUploadContent'
import { FileWithPath } from 'react-dropzone';
import { fetchDownloadUrlForDocNew, convertToPdfNew, evaluateAssignmentNew } from '../../common/api/assignment';
import {useSnackbar} from "notistack"
import AssignmentPdfViewer from '../components/pdf-viewer/assignment_pdf_viewer'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    batchSelect: {
      paddingTop: '12px'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    dsnBtn: {
      '& button': {
        backgroundColor: '#F5CC0A',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#F5CC0A',
        }
      },
    },
    submitBtn: {
      '& button': {
        padding: '10px 20px',
      },
      float: 'right'
    },
  }),
);

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const TutorCorrection: FunctionComponent<Props> = ({authUser, parentChildren, location})  => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignment>()
  const [stuId, setStuId] = useState('');
  const [assiId, setAssiId] = useState('');
  const [ansDocs, setAnsDocs] = useState('')
  const [comment, setComment] = useState('')
  const [indiMarks, setIndiMarks] = useState(0)
  const [pdfUrl, setPdfUrl] = useState('')
  const [tutorHighlights, setTutorHighlights] = useState<any>()
  const [selectedDoc, setSelectedDoc] = useState<answerDoc>()

  useEffect(() => {
    (async () => {
      try {
        const params = new URLSearchParams(location.search);
        const assignmentId = params.get('assignmentId');
        const studentId = params.get('studentId');
        if(studentId && assignmentId) {
          const assignedAssignmentResp = await fetchAssignedAssignmentNew(studentId, assignmentId)
          setStuId(studentId)
          setAssiId(assignmentId)
          setAssignedAssignment(assignedAssignmentResp[0])
          
          const ansDoc = assignedAssignmentResp[0].students.find(list => list.studentId === studentId)?.uploadedAnswerDocs

          const { answerDocFormat, answerDocLocation, uuid } = ansDoc as answerDoc;
          setSelectedDoc(ansDoc)

          await fetchDownloadUrlForDocNew({
            uuid: uuid,
            contentType: answerDocFormat,
            fileName: answerDocLocation,
            docType: AssignmentDocType.ANSWER_DOC,
          }).then((value) => {
            setPdfUrl(value.url)
          })
        } else {
          setRedirectTo('/profile/assignment')
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [location.search])

  const getHighlights = (highlights: any) => {
    if (selectedDoc) {
      const toSet: any = {}
      Object.assign(toSet, tutorHighlights)
      toSet[selectedDoc.answerDocLocation] = highlights
      setTutorHighlights(toSet)
    }
  }

  const confirmEval = async() => {
    if(comment.length < 1) {
      enqueueSnackbar("Please enter Comment", {variant: 'warning'});
      return;
    }

    if(indiMarks < 0) {
      enqueueSnackbar("Marks cannot be less than Zero", {variant: 'warning'});
      return;
    }

    if(assignedAssignment && (assignedAssignment.assignment.marks < indiMarks) || indiMarks < 0) {
      enqueueSnackbar("Invalid Marks", {variant: 'warning'});
      return;
    }
    const tutorfeedback:TutorFeedback = {
      feedback: comment,
      marks: indiMarks,
      tutorHighlights: tutorHighlights
    }

    await evaluateAssignmentNew(stuId, assiId, tutorfeedback)
    setRedirectTo('/profile/assignment/submitted?type=review')
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Grid container style={{paddingBottom: '20px', padding: '0px 20px'}}>
        <Grid item sm={12} md={8}>
          <Box
            bgcolor="#FFFFFF"
            padding="20px 30px"
            position="relative"
            borderRadius="14px"
            marginTop="30px"
          >
            {pdfUrl &&
              <AssignmentPdfViewer key={pdfUrl} url={pdfUrl} getHighlights={getHighlights} highlights={tutorHighlights ? tutorHighlights[(selectedDoc as answerDoc).answerDocLocation] : undefined} />
            }
          </Box>
        </Grid>
        <Grid item sm={12} md={4}>
          <Box
            bgcolor="#FFFFFF"
            padding="20px 30px"
            position="relative"
            borderRadius="14px"
            marginTop="30px"
          >
            <Grid container>
              <Grid item xs={12} style={{paddingTop: '15px'}}>
                <TextField id="standard-required" label="Marks" type="number" value={indiMarks} style={{width: '85%'}}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {setIndiMarks(Number(event.target.value))}}
                  InputProps={{ inputProps: { min: 0, max: assignedAssignment?.assignment.marks } }}
                />
              </Grid>
              <Grid item xs={12} style={{paddingTop: '15px'}}>
                <TextField id="standard-required" label="Feedback" value={comment} multiline rows={4} style={{width: '85%'}}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {setComment(event.target.value)}}
                />
              </Grid>
              <Grid item xs={12} style={{paddingTop: '20px', paddingRight: '15%'}}>
                <Button disableElevation color="primary" variant="contained" style={{float: 'right'}}
                  onClick={confirmEval}
                >
                  Confirm
                </Button>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(TutorCorrection));
