//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Select, MenuItem, FormControl, Input, InputAdornment, IconButton } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../../common/components/sidedrawer';
import Button from '../../../common/components/form_elements/button';
import { fontOptions } from '../../../../theme';
import AssiBooks from '../../../../assets/svgs/assiBooks.svg'
import { User, Children } from '../../../common/contracts/user';
import { RootState } from '../../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { exceptionTracker, isAdminTutor, isOrgTutor } from '../../../common/helpers';
import { fetchAssignmentNew, fetchAssignedAssignmentNew } from '../../../common/api/assignment';
import { Batch } from '../../../academics/contracts/batch';
import { isOrganization, isStudent, isParent } from '../../../common/helpers';
import { fetchBatchesList } from '../../../common/api/academics';
import { fetchOrgBatchesList, fetchStudentBatchesList } from '../../../common/api/batch';
import { Assignment, AssignedAssignment, AssignedStudent } from '../../contracts/assignment_interface';
import Datagrids, { datagridCellExpand } from '../../../common/components/datagrids'
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
} from '@material-ui/icons';
import './style.css'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    addAssi: {
      '& button': {
        color: '#4C8BF5',
        backgroundColor: '#FFFFFF',
        '&:hover': {
          backgroundColor: darken('#FFFFFF', 0.1),
        }
      },
      paddingTop: '12px'
    },
    drafts: {
      '& button': {
        color: '#FFFFFF',
        backgroundColor: '#F9BD33',
        '&:hover': {
          backgroundColor: darken('#F9BD33', 0.1),
        }
      }
    },
  }),
);

interface Data {
  id: string;
  assignmentname: string;
  brief: string;
  marks: number;
  endDate: Date;
  startDate: Date;
  publishedDate: Date | undefined;
  submissions: string;
  isPublished: boolean;
}

function createData(
  id: string,
  assignmentname: string,
  brief: string,
  marks: number,
  endDate: Date,
  startDate: Date,
  publishedDate: Date | undefined,
  submissions: string,
  isPublished: boolean
): Data {
  return { id, assignmentname, brief, marks, endDate, startDate, publishedDate, submissions, isPublished };
}

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const TutorView: FunctionComponent<Props> = ({ authUser, parentChildren }) => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState('');
  const [batches, setBatches] = useState<Batch[]>([])
  const [assignment, setAssignment] = useState<Assignment[]>([])
  const [batchIndex, setBatchIndex] = useState(-1);
  const [assignmentFiltered, setAssignmentFiltered] = useState<Assignment[]>([])
  const [searchText, setSearchText] = useState<string>('');
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignment[]>()
  const [focused, setFocused] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const batchesList = (isOrganization(authUser) ?
          await fetchOrgBatchesList() :
          (isStudent(authUser) ?
            await fetchStudentBatchesList(authUser._id ? authUser._id : '') :
            (isParent(authUser) ?
              (parentChildren.current ?
                await fetchStudentBatchesList(parentChildren.current ? parentChildren.current : '') :
                []
              ) :
              await fetchBatchesList()
            )
          )
        );
        const assignmentResp = await fetchAssignmentNew(undefined, ((isOrgTutor(authUser) || isAdminTutor(authUser)) ? authUser._id : undefined));
        const assignedAssignmentResp = await fetchAssignedAssignmentNew(undefined, undefined)

        setAssignment(assignmentResp)
        setAssignedAssignment(assignedAssignmentResp)
        setBatches(batchesList);

        if (batchesList.length > 0) {
          setBatchIndex(0)
        } else {
          setBatchIndex(-1)
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      if (batchIndex < 0) {
        setAssignmentFiltered([])
      } else {
        const thisbatch = batches[batchIndex]
        let filtered = assignment.filter(assi => assi.batches.includes(thisbatch._id as string))
        if (searchText !== '') {
          filtered = filtered.filter(fil =>
            fil.assignmentname.toLowerCase().includes(searchText.toLowerCase())
          )
        }
        setAssignmentFiltered(filtered)
      }
    })();
  }, [batchIndex, searchText]);


  const viewAssignment = (selectedRowDetails: Data) => {
    const thisbat = batches[batchIndex]
    if (selectedRowDetails && thisbat) {
      setRedirectTo(`/profile/assignment/batches?assignmentId=${selectedRowDetails.id}&batchId=${thisbat._id as string}`)
    }
  }

  const editDraft = (selectedRowDetails: Data) => {
    if (selectedRowDetails) {
      setRedirectTo(`/profile/assignment/create?assignmentId=${selectedRowDetails.id}`)
    }
  }

  const buttonData = [
    {
      title: 'View',
      action: viewAssignment,
      color: '#4C8BF5'
    }
  ];

  const rows = assignmentFiltered.map((assi) => {
    const thisAssignedAssignment = assignedAssignment?.find(list => list.assignment._id === assi._id)
    let totalStu: AssignedStudent[] = [];
    let submittedStu: AssignedStudent[] = []
    if (thisAssignedAssignment) {
      const thisbatch = batches[batchIndex]
      const thisbatchStudents = thisbatch.students;
      totalStu = thisAssignedAssignment.students.filter(list => thisbatchStudents.includes(list.studentId))
      submittedStu = totalStu.filter(list => list.isSubmitted)
    }
    return (
      createData(
        assi._id as string,
        assi.assignmentname,
        assi.brief,
        assi.marks,
        assi.endDate,
        assi.startDate,
        assi.publishedDate,
        String(submittedStu.length + '/' + totalStu.length),
        assi.isPublished as boolean
      )
    )
  });

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'assignmentname', headerName: 'Name', flex: 1, renderCell: datagridCellExpand },
    { field: 'brief', headerName: 'Brief', flex: 1, renderCell: datagridCellExpand },
    { field: 'marks', headerName: 'Full Marks', flex: 1, renderCell: datagridCellExpand },
    { field: 'endDate', headerName: 'Due Date', flex: 1, renderCell: datagridCellExpand },
    { field: 'publishedDate', headerName: 'Published On', flex: 1, renderCell: datagridCellExpand },
    { field: 'submissions', headerName: 'Submissions', flex: 1, renderCell: datagridCellExpand },
    {
      field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          assignmentname: params.getValue("assignmentname") as string,
          publishedDate: params.getValue("publishedDate") as Date
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.assignmentname === selectedRow.assignmentname && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          const btnText = selectedRowDetails?.isPublished ? 'View' : 'Draft'
          return (
            <Box key={index} className={selectedRowDetails?.isPublished ? '' : classes.drafts}>
              <Button
                onClick={() => {
                  (selectedRowDetails?.isPublished) ? button.action(selectedRowDetails as Data) : editDraft(selectedRowDetails as Data)
                }}
                disabled={(selectedRowDetails?.isPublished) ? ((new Date(selectedRowDetails?.endDate as Date) > new Date()) ? true : false) : ((new Date(selectedRowDetails?.startDate as Date) < new Date()) ? true : false)}
                variant="contained"
                color="primary"
                disableElevation
              >
                {btnText}
              </Button>
            </Box>
          );
        })

        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = rows.map((row, index) => {
    return ({
      id: (index + 1),
      assignmentname: row.assignmentname,
      brief: row.brief,
      marks: String(row.marks),
      endDate: new Date(row.endDate).toLocaleString(),
      publishedDate: row.publishedDate ? new Date(row.publishedDate as Date).toLocaleString() : '-',
      submissions: String(row.submissions),
    })
  })

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
        <Container maxWidth="lg" style={{ padding: '30px 2.5%' }} className='add-assignment'>
          <Box
            bgcolor="#4C8BF5"
            padding="20px 30px"
            marginBottom="30px"
            position="relative"
            borderRadius="14px"
            color='#fff'
          >
            <Grid item container className='heading-container'>
              <Grid item md={8} sm={12} xs={12}>
                <Box style={{ height: '100%' }}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <Typography className={`${classes.title} title-text`}>
                        All Assignment
                    </Typography>
                      <Typography>
                        Create Assignments | Assign it to Batches | Evaluate Assignments
                    </Typography>
                      <Box className={classes.addAssi}>
                        <Button variant="contained" disableElevation onClick={() => setRedirectTo('/profile/assignment/create')}>
                          Add Assignment
                      </Button>
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
              <Grid item md={4} sm={12} xs={12} className='book-box'>
                <Box style={{ height: '100%' }}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <img src={AssiBooks} alt="books" style={{ height: '150px', float: 'right' }} />
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="25px"
            marginTop='25px'
          >
            <Grid container>
              <Grid className='assignment-heading' item xs={12} sm={12} md={6} lg={6} xl={6} style={{ paddingTop: '10px', marginBottom: '10px' }}>
                <Typography style={{ fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.bold, color: '#3D3D3D' }}>
                  Assignments
              </Typography>
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6} style={{ marginBottom: '10px' }}>
                <Box display="flex" flexDirection="row-reverse" className='search-box'>
                  <Grid item xs={12} sm={12}>
                    <Box p={1}>
                      <Select
                        style={{ marginLeft: '15px' }}
                        value={batchIndex}
                        onChange={(
                          e: React.ChangeEvent<{ value: unknown }>
                        ) => setBatchIndex(e.target.value as number)}
                      >
                        <MenuItem value="-1">Select Batch</MenuItem>
                        {batches.map((bat, index) => (
                          <MenuItem key={index} value={index}>
                            {bat.batchfriendlyname}
                          </MenuItem>
                        ))}
                      </Select>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Box p={1}>
                      <Input
                        name="search"
                        inputProps={{ inputMode: 'search' }}
                        placeholder="Search"
                        onFocus={() => setFocused(true)}
                        onBlur={() => setFocused(false)}
                        value={searchText}
                        onChange={(e) => {
                          setSearchText(e.target.value);
                        }}
                        endAdornment={
                          searchText.trim() !== '' ? (
                            <InputAdornment position="end">
                              <IconButton size="small" onClick={() => setSearchText('')}>
                                <ClearIcon />
                              </IconButton>
                            </InputAdornment>
                          ) : (
                            <InputAdornment position="end">
                              <IconButton disabled size="small">
                                <SearchIcon />
                              </IconButton>
                            </InputAdornment>
                          )
                        }
                      />
                      {(searchText.trim() !== '') &&
                        <Typography style={{ marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666' }}>
                          Filtered Table using Keyword '{searchText}'
                    </Typography>
                      }
                    </Box>

                  </Grid>
                </Box>
              </Grid>

              <Grid item xs={12} style={{ marginTop: '15px' }}>
                <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
              </Grid>
            </Grid>
          </Box>
        </Container>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(TutorView));
