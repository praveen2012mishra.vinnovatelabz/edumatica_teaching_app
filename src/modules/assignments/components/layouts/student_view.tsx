import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Box, Grid, Typography, Select, MenuItem, FormControl, Tooltip } from '@material-ui/core';
import { makeStyles, createStyles, Theme, darken } from '@material-ui/core/styles';
import MiniDrawer from '../../../common/components/sidedrawer';
import Button from '../../../common/components/form_elements/button';
import { fontOptions } from '../../../../theme';
import AssiOther from '../../../../assets/svgs/assiOther.svg'
import AssiView from '../../../../assets/svgs/stuAssiView.svg'
import AssiMath from '../../../../assets/svgs/assiMath.svg'
import { User, Children } from '../../../common/contracts/user';
import { RootState } from '../../../../store';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { exceptionTracker } from '../../../common/helpers';
import { fetchStudentAssignmentNew } from '../../../common/api/assignment';
import { Batch } from '../../../academics/contracts/batch';
import { isOrganization, isStudent, isParent } from '../../../common/helpers';
import { fetchBatchesList } from '../../../common/api/academics';
import { fetchOrgBatchesList, fetchStudentBatchesList } from '../../../common/api/batch';
import { StudentAssignment } from '../../contracts/assignment_interface';
import { truncate } from 'lodash';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: fontOptions.size.large
    },
    batchSelect: {
      paddingTop: '12px'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    dsnBtn1: {
      '& button': {
        backgroundColor: '#F5CC0A',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#F5CC0A',
        }
      },
    },
    dsnBtn2: {
      '& button': {
        backgroundColor: '#E5E5E5',
        padding: '10px 20px',
        color: '#4C8BF5',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#E5E5E5',
        }
      },
    },
    dsnBtn3: {
      '& button': {
        backgroundColor: '#B8D671',
        padding: '10px 20px',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#B8D671',
        }
      },
    },
    dsnBtn4: {
      '& button': {
        backgroundColor: '#4C8BF5',
        color: '#FFFFFF',
        cursor: 'context-menu',
        '&:hover': {
          backgroundColor: '#4C8BF5',
        }
      },
    },
  }),
);

interface Data {
  id: string;
  assignmentname: string;
  brief: string;
  marks: number;
  startDate: Date;
  endDate: Date;
  publishedDate: Date;
  isSubmitted: boolean;
  isEvaluated: boolean;
  ownerId: string
}

function createData(
  id: string,
  assignmentname: string,
  brief: string,
  marks: number,
  startDate: Date,
  endDate: Date,
  publishedDate: Date,
  isSubmitted: boolean,
  isEvaluated: boolean,
  ownerId: string
): Data {
  return { id, assignmentname, brief, marks, startDate, endDate, publishedDate, isSubmitted, isEvaluated , ownerId};
}

interface Props extends RouteComponentProps {
  authUser: User;
  parentChildren: Children;
}

const StudentView: FunctionComponent<Props> = ({authUser, parentChildren})  => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState('');
  const [batches, setBatches] = useState<Batch[]>([])
  const [assignment, setAssignment] = useState<StudentAssignment[]>([])
  const [batchIndex, setBatchIndex] = useState(-1);
  const [assignmentFiltered, setAssignmentFiltered] = useState<StudentAssignment[]>([])  

  useEffect(() => {
    (async () => {
      try {
        const batchesList = (isOrganization(authUser) ?
          await fetchOrgBatchesList() : 
          (isStudent(authUser) ? 
            await fetchStudentBatchesList(authUser._id ? authUser._id : '') : 
            (isParent(authUser) ? 
              (parentChildren.current ? 
                await fetchStudentBatchesList(parentChildren.current ? parentChildren.current : '') : 
                []
              ) :
              await fetchBatchesList()
            )
          )
        );

        let assignmentResp: StudentAssignment[];
        if(isParent(authUser) && parentChildren.current) {
          assignmentResp = await fetchStudentAssignmentNew(parentChildren.current)
        } else if(isStudent(authUser)) {
          assignmentResp = await fetchStudentAssignmentNew(authUser._id as string)
        } else {
          assignmentResp = []
        }
        
        setAssignment(assignmentResp)
        setBatches(batchesList);

        if(batchesList.length > 0) {
          setBatchIndex(0)
        } else {
          setBatchIndex(-1)
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    (async () => {
      if(batchIndex < 0) {
        setAssignmentFiltered([])
      } else {
        const thisbatch = batches[batchIndex]
        const filtered = assignment.filter(assi => assi.batches.includes(thisbatch._id as string))
        setAssignmentFiltered(filtered)
      }
    })();
  }, [batchIndex]);

  const rows = assignmentFiltered.map((assi) => {
    return(
      createData(
        assi._id as string,
        assi.assignmentname,
        assi.brief,
        assi.marks,
        assi.startDate,
        assi.endDate,
        (assi.publishedDate as Date),
        assi.isSubmitted,
        assi.isEvaluated,
        assi.ownerId as string
      )
    )
  });

  const parentAction = (status: string, id: string) => {
    if(status === 'Evaluated') {
      setRedirectTo(`/profile/assignment/studentviewmarks?assignmentId=${id}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`)
    }
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg" style={{padding: '30px 2.5%'}}>
        <Box
          bgcolor="#4C8BF5"
          padding="20px 30px"
          marginBottom="30px"
          position="relative"
          borderRadius="14px"
          color='#fff'
        >
          <Grid item container>
            <Grid item sm={8}>
              <Box style={{height: '100%'}}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  style={{ height: '100%' }}
                >
                  <Grid item xs={12}>
                    <Typography className={classes.title}>
                      All Assignment
                    </Typography>
                    <Typography>
                      Complete your Assignments before time
                    </Typography>
                    <Box className={classes.batchSelect}>
                      <FormControl variant="outlined" className={classes.formControl}>
                        <Select
                          value={batchIndex}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => setBatchIndex(e.target.value as number)}
                          style={{backgroundColor: '#FFFFFF'}}
                        >
                          <MenuItem value="-1">Select Batch</MenuItem>
                          {batches.map((bat, index) => (
                            <MenuItem key={index} value={index}>
                              {bat.batchfriendlyname}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
            <Grid item sm={4}>
              <Box style={{height: '100%'}}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  style={{ height: '100%' }}
                >
                  <Grid item xs={12}>
                    <img src={AssiView} alt="students" style={{height: '150px', float: 'right'}}/>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box
          borderRadius="14px"
        >
          <Grid container spacing={2}>
            {rows.map(row => {
              const status = (!row.isSubmitted && !row.isEvaluated) ? 'New' : ((row.isSubmitted && !row.isEvaluated && new Date(row.endDate) > new Date()) ? 'Uploaded' : (row.isSubmitted && !row.isEvaluated ? 'Under Evaluation' : 'Evaluated'))
              return (
                <Grid key={row.id} item xs={12} sm={6} md={4}>
                  <Box
                    bgcolor="#ffffff"
                    borderRadius="5px"
                    display="flex"
                    padding="15px"
                    marginTop="20px"
                  >
                    <Grid container>
                      <Grid item xs={12}>
                        <Box className={(status === 'New' || status === 'Uploaded') ? classes.dsnBtn1 : (status === 'Under Evaluation' ? classes.dsnBtn2 : classes.dsnBtn3)}>
                          <Button disableElevation disableFocusRipple disableRipple disableTouchRipple>
                            {status}
                          </Button>
                        </Box>
                      </Grid>
                      <Grid item xs={12} style={{paddingTop: '15px'}}>
                        <Tooltip title={row.assignmentname} disableHoverListener={row.assignmentname.length <= 20}>
                          <Typography style={{fontSize: fontOptions.size.mediumPlus, fontWeight: fontOptions.weight.bold, color: '#3D3D3D'}}>
                            {truncate(row.assignmentname, {length: 25})}
                          </Typography>
                        </Tooltip>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography style={{fontSize: fontOptions.size.small, color: '#979797'}}>
                          Starts At {' ' + new Date(row.startDate).toLocaleString()}
                        </Typography>
                        <Typography style={{fontSize: fontOptions.size.small, color: '#979797'}}>
                          Last Date to Submit {' ' + new Date(row.endDate).toLocaleString()}
                        </Typography>
                      </Grid>
                      <Grid item xs={7}>
                        <Grid
                          container
                          direction="row"
                          alignItems="center"
                          justify="center"
                          style={{ height: '100%' }}
                        >
                          <Grid item xs={12}>
                            <Typography style={{fontSize: fontOptions.size.small, color: '#3D3D3D'}}>
                              {row.ownerId}
                            </Typography>
                            <Box className={(isParent(authUser) && (status !== 'Evaluated')) ? classes.dsnBtn4 : ''}>
                              <Button variant={status === 'New' ? 'contained' : 'outlined'} color="primary" 
                                disableElevation style={{marginTop: '10px', padding: '10px 25px'}}
                                disableRipple={isParent(authUser) && (status !== 'Evaluated')}
                                onClick={() => {
                                  (isParent(authUser) ? parentAction(status, row.id) : 
                                    ((status === 'New') ? setRedirectTo(`/profile/assignment/studentupload?assignmentId=${row.id}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`): 
                                      ((status === 'Uploaded') ? setRedirectTo(`/profile/assignment/studentreupload?assignmentId=${row.id}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`):
                                        ((status === 'Under Evaluation')? setRedirectTo(`/profile/assignment/studentwait?assignmentId=${row.id}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`):
                                          setRedirectTo(`/profile/assignment/studentviewmarks?assignmentId=${row.id}&studentId=${isParent(authUser) ? (parentChildren.current as string) : (authUser._id as string)}`)
                                        )
                                      )
                                    )
                                  )
                                }}
                                disabled={(isParent(authUser) ? false : status === 'New' ? (new Date(row.startDate) > new Date() ? true : false) : (status === 'Under Evaluation' ? false : false))}
                              >
                                {status === 'New' ? (isParent(authUser) ? 'Not Submitted' : 'View') : ((status === 'Under Evaluation' || status === 'Uploaded') ? 'Submitted' : 'View Results')}
                              </Button>
                            </Box>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item xs={5}>
                        <img src={AssiMath} alt="subject" />
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              )
            })}
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(StudentView));
