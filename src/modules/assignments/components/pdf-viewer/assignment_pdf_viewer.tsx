// @flow
/* eslint import/no-webpack-loader-syntax: 0 */

import React, { Component } from "react";
import PDFWorker from "worker-loader!pdfjs-dist/lib/pdf.worker";

import {
  PdfLoader,
  PdfHighlighter,
  Tip,
  Highlight,
  Popup,
  AreaHighlight,
  setPdfWorker
} from "react-pdf-highlighter";

import Spinner from "./spinner";
import Sidebar from "./sidebar";

import "./style/app.css"

setPdfWorker(PDFWorker);

type Props = {
  url: string,
  getHighlights?: (highlights: any) => void,
  highlights?: any[],
  isStudent?: boolean
};

type State = {
  url: string,
  highlights: Array<any>,
  isStudent?: boolean,
};

const getNextId = () => String(Math.random()).slice(2);

const parseIdFromHash = () =>
  document.location.hash.slice("#highlight-".length);

const resetHash = () => {
  document.location.hash = "";
};

const HighlightPopup = ({ comment }: { comment: any}) =>
  comment.text ? (
    <div className="Highlight__popup">
      {comment.emoji} {comment.text}
    </div>
  ) : null;

class AssignmentPdfViewer extends Component<Props, State> {
  state: State = {
    url: '',
    highlights: [],
    isStudent: false,
  };

  resetHighlights = () => {
    this.setState({
      highlights: []
    });
  };

  scrollViewerTo = (highlight: any) => {};

  scrollToHighlightFromHash = () => {
    const highlight = this.getHighlightById(parseIdFromHash());

    if (highlight) {
      this.scrollViewerTo(highlight);
    }
  };

  componentDidMount() {
    this.setState({
      url: this.props.url,
      highlights: this.props.highlights ?  this.props.highlights : [],
      isStudent: this.props.isStudent
    })
    window.addEventListener(
      "hashchange",
      this.scrollToHighlightFromHash,
      false
    );
  }

  getHighlightById(id: string) {
    const { highlights } = this.state;

    return highlights.find(highlight => highlight.id === id);
  }

  addHighlight(highlight: any) {
    const { highlights } = this.state;

    this.setState({
      highlights: [{ ...highlight, id: getNextId() }, ...highlights]
    });
    if(this.props.getHighlights) {
      this.props.getHighlights([{ ...highlight, id: getNextId() }, ...highlights])
    }
  }

  updateHighlight(highlightId: string, position: Object, content: Object) {

    this.setState({
      highlights: this.state.highlights.map(h => {
        const {
          id,
          position: originalPosition,
          content: originalContent,
          ...rest
        } = h;
        return id === highlightId
          ? {
              id,
              position: { ...originalPosition, ...position },
              content: { ...originalContent, ...content },
              ...rest
            }
          : h;
      })
    });
    if(this.props.getHighlights) {
      this.props.getHighlights(this.state.highlights.map(h => {
        const {
          id,
          position: originalPosition,
          content: originalContent,
          ...rest
        } = h;
        return id === highlightId
          ? {
              id,
              position: { ...originalPosition, ...position },
              content: { ...originalContent, ...content },
              ...rest
            }
          : h;
      }))
    }
  }

  render() {
    const { url, highlights, isStudent } = this.state;

    if(isStudent) {
      return (
        <>
        <div className="App" style={{ display: "flex", height: "100vh" }}>
          <Sidebar highlights={highlights} isTutor={false} />
          <div
            style={{
              height: "100vh",
              width: "75vw",
              position: "relative"
            }}
          >
            <PdfLoader url={url} beforeLoad={<Spinner />}>
              {(pdfDocument: any) => (
                <PdfHighlighter 
                  pdfDocument={pdfDocument}
                  onScrollChange={resetHash}
                  scrollRef={(scrollTo: any) => {
                    this.scrollViewerTo = scrollTo;

                    this.scrollToHighlightFromHash();
                  }}
                  highlightTransform={(
                    highlight: any,
                    index: any,
                    setTip: any,
                    hideTip: any,
                    viewportToScaled: any,
                    screenshot: any,
                    isScrolledTo: any
                  ) => {
                    const isTextHighlight = !Boolean(
                      highlight.content && highlight.content.image
                    );
  
                    const component = isTextHighlight ? (
                      <Highlight
                        isScrolledTo={isScrolledTo}
                        position={highlight.position}
                        comment={highlight.comment}
                      />
                    ) : (
                      <AreaHighlight
                        highlight={highlight}
                        onChange={(boundingRect: any) => {
                          this.updateHighlight(
                            highlight.id,
                            { boundingRect: viewportToScaled(boundingRect) },
                            { image: screenshot(boundingRect) }
                          );
                        }}
                      />
                    );
  
                    return (
                      <Popup
                        popupContent={<HighlightPopup {...highlight} />}
                        onMouseOver={(popupContent: any) =>
                          setTip(highlight, (highlight: any) => popupContent)
                        }
                        onMouseOut={hideTip}
                        key={index}
                        children={component}
                      />
                    );
                  }}
                  highlights={highlights}
                />
              )}
            </PdfLoader>
          </div>
        </div>
        </>
      )
    }

    return (
      <div className="App" style={{ display: "flex", height: "100vh" }}>
        <Sidebar
          highlights={highlights}
          resetHighlights={this.resetHighlights}
          isTutor={true}
        />
        <div
          style={{
            height: "100vh",
            width: "100vw",
            position: "relative"
          }}
        >
          <PdfLoader url={url} beforeLoad={<Spinner />}>
            {(pdfDocument: any) => (
              <PdfHighlighter
                pdfDocument={pdfDocument}
                enableAreaSelection={(event: any) => event.altKey}
                onScrollChange={resetHash}
                // pdfScaleValue="page-width"
                scrollRef={(scrollTo: any) => {
                  this.scrollViewerTo = scrollTo;

                  this.scrollToHighlightFromHash();
                }}
                onSelectionFinished={(
                  position: any,
                  content: any,
                  hideTipAndSelection: any,
                  transformSelection: any
                ) => (
                  <Tip
                    onOpen={transformSelection}
                    onConfirm={(comment: any) => {
                      this.addHighlight({ content, position, comment });

                      hideTipAndSelection();
                    }}
                  />
                )}
                highlightTransform={(
                  highlight: any,
                  index: any,
                  setTip: any,
                  hideTip: any,
                  viewportToScaled: any,
                  screenshot: any,
                  isScrolledTo: any
                ) => {
                  const isTextHighlight = !Boolean(
                    highlight.content && highlight.content.image
                  );

                  const component = isTextHighlight ? (
                    <Highlight
                      isScrolledTo={isScrolledTo}
                      position={highlight.position}
                      comment={highlight.comment}
                    />
                  ) : (
                    <AreaHighlight
                      highlight={highlight}
                      onChange={(boundingRect: any) => {
                        this.updateHighlight(
                          highlight.id,
                          { boundingRect: viewportToScaled(boundingRect) },
                          { image: screenshot(boundingRect) }
                        );
                      }}
                    />
                  );

                  return (
                    <Popup
                      popupContent={<HighlightPopup {...highlight} />}
                      onMouseOver={(popupContent: any) =>
                        setTip(highlight, (highlight: any) => popupContent)
                      }
                      onMouseOut={hideTip}
                      key={index}
                      children={component}
                    />
                  );
                }}
                highlights={highlights}
              />
            )}
          </PdfLoader>
        </div>
      </div>
    );
  }
}

export default AssignmentPdfViewer;