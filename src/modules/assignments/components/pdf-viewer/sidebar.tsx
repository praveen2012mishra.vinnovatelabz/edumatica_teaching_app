import React from "react";

import type { T_Highlight } from "react-pdf-highlighter/src/types";

type Props = {
  highlights: Array<any>,
  isTutor: Boolean,
  resetHighlights?: () => void
};

const updateHash = (highlight: any) => {
  document.location.hash = `highlight-${highlight.id}`;
};

function Sidebar({ highlights, isTutor, resetHighlights }: Props) {
  return (
    <div className="sidebar" style={{ width: "25vw" }}>
      {isTutor && <div className="description" style={{ padding: "1rem" }}>
        <p>
          <small>
            To create area highlight hold ⌥ Option key (Alt), then click and
            drag.
          </small>
        </p>
      </div>}

      <ul className="sidebar__highlights">
        {highlights.map((highlight, index) => (
          <li
            key={index}
            className="sidebar__highlight"
            onClick={() => {
              updateHash(highlight);
            }}
          >
            <div>
              <strong>{highlight.comment.text}</strong>
              {highlight.content.text ? (
                <blockquote style={{ marginTop: "0.5rem" }}>
                  {`${highlight.content.text.slice(0, 90).trim()}…`}
                </blockquote>
              ) : null}
              {highlight.content.image ? (
                <div
                  className="highlight__image"
                  style={{ marginTop: "0.5rem" }}
                >
                  <img src={highlight.content.image} alt={"Screenshot"} />
                </div>
              ) : null}
            </div>
            <div className="highlight__location">
              Page {highlight.position.pageNumber}
            </div>
          </li>
        ))}
      </ul>
      {(isTutor && highlights.length > 0) ? (
        <div style={{ padding: "1rem" }}>
          <button onClick={resetHighlights}>Reset highlights</button>
        </div>
      ) : null}
    </div>
  );
}

export default Sidebar;