export enum AssignmentDocType {
  TASK_DOC = 'AssignmentTaskDocs',
  ANSWER_DOC = 'AssignmentAnswerDocs'
}

export interface taskDoc {
  taskDocType: AssignmentDocType,
  taskDocFormat: string,
  taskDocLocation: string,
  uuid: string,
}

export interface answerDoc {
  answerDocType: AssignmentDocType, 
  answerDocFormat: string,
  answerDocLocation: string,
  uuid: string
}

export interface TutorFeedback {
  feedback: string;
  marks: number;
  tutorHighlights: any;
}

export interface Document {
  fileName: string;
  contentType: string;
  uuid?: string;
  docType: string;
}

export interface Assignment {
  boardname: string,
	classname: string,
	subjectname: string,
  assignmentname: string,
  marks: number,
  instructions: string,
  brief: string,
  startDate: Date,
  endDate: Date,
  batches: string[],
  students: string[],
  taskDocs: taskDoc,
  ownerId?: string,
  tutorId?: string[],
  _id?: string,
  isPublished?: boolean,
  publishedDate?: Date
}

export interface StudentAssignment extends Assignment {
  isSubmitted: boolean,
  isEvaluated: boolean,
}

export interface AssignedStudent {
  studentId: string,
  batchId: string,
  isSubmitted: boolean,
  isEvaluated: boolean,
  uploadedAnswerDocs?: answerDoc,
  marks?: number,
  feedback?: number,
  submittedOn?: Date,
  evaluatedOn?: Date,
  tutorHighlights?: any,
}

export interface AssignedAssignment {
  ownerId: string,
  tutorId: string[],
  assignment: Assignment,
  students: AssignedStudent[],
}
