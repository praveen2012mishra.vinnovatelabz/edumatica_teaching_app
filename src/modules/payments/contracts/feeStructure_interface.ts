export interface PaymentPackageInterfaceReq {
  totalprice: number,
  discount: number,
  paymentplan: string,
  paymentcycle: string,
  graceperiod: number,
}

export interface PaymentPackageInterface {
  _id: string,
  totalprice: number,
  discount: number,
  paymentplan: string,
  paymentcycle: string,
  graceperiod: number,
}