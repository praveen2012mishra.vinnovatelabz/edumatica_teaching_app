import { Batch } from "../../academics/contracts/batch";

export interface Transaction {
  _id: string;
  orderId: string;
  batchId: Batch;
  purchasedPackageId: string;
  purchasedEdumacPackageId: string;
  paymentcycle: string;
  authenticationDetail: string;
  entity: string;
  amount: number; //paisa to rupee
  amount_paid: number,
  gatewayServiceCharge: number,
  serviceChargeTax: number,
  settlementAmount: number,
  settlementUTR:  string,
  convenience_fee: number,
  convenience_fee_tax: number,
  edumatica_earning: number,
  edumatica_earning_tax: number,
  tds: number,
  studentPayableAmt: number,
  netAmountToBene: number,
  currency: string;
  receipt: string;
  status: string;
  attempts: number;
  notes: string;
  paymentType: string,
  isTransferMade: boolean,
  packageUpdate: boolean
  newPackagePlanId: string,
  created_at: number;
}