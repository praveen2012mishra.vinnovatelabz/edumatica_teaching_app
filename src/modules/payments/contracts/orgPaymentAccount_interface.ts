import { Transaction } from "./transaction_interface";

export interface PurchasedEdumacPackage {
  _id: string;
  accountType: string;
  name: string;
  planId: string; // can be trial | Edumatica_Basic | Edumatica_Plus
  graceperiod: number;
  paymentplan: string;
  paymentcycle: string; //discount not included , Discount will be kept seperate.
  cost: number;
  perstudentcost: number;
  recordingQuota: number; //In hours
  courseCount: number;
  studentCount: number;
  tutorCount: number;
  batchCount: number;
  sessionCount: number;
  contentSize: number;
  customChapter: number;
}

export interface StudentPayable {
  _id: string;
  accountType: string;
  student: string;
  batch: string;
  addedOn: Date;
  renewalDate: Date; //batchstartdate + 1 month batch already started then grace date now + 7   // in cron job check if tutor renewalDate+30days =< this renewalDate if true move to next
  graceperiod: number; //7days
  currentPaybleAmountToEdumac: number; //from masterplan
  // payed for no. of days
  currentStatus: string; // Payment Process
  currentStatusChangeDate: Date; // Deactivate activate date
}

export interface OrganizationPaymentAccount {
  _id: string;
  accountType: string;
  purchasedEdumacPackage: PurchasedEdumacPackage;
  retainerShipFeeBalance: number;
  studentPaybleFeeBalance: number; //or add to retainershipFeeBalance
  totaldebitBalance: number;
  currentgraceperiod: number;
  renewalDate: Date;
  vpaId: string;
  currentStudentCount: number;
  currentRecordingQuota: number;
  currentTutorCount: number;
  currentBatchCount: number;
  currentCourseCount: number;
  paymentOrder: [Transaction];
  totalcreditBalance: number;
  studentPayable: [StudentPayable];
}
