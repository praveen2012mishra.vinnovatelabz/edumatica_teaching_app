import { Batch } from "../../academics/contracts/batch";
import { PaymentPackageInterfaceReq } from "./feeStructure_interface";
import { Transaction } from "./transaction_interface";

export interface PurchasedCoursePackage {
    _id: string,
    batch: Batch,
    paymentPlans: [PaymentPackageInterfaceReq],
    paymentTransactions : [Transaction],
    currentGracePeriod : number,
    renewalDate : Date,
    paymentComplete: Boolean,
}

export interface Invoice {

}

export interface StudentPaymentAccount {
  student:  string,
  purchasedPackage:[PurchasedCoursePackage],
  vpaId: string,
  invoices : [Invoice],
}