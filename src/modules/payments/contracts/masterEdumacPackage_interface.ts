export interface MasterEdumacPackage {
  _id: string;
  name: string;
  graceperiod: number;
  paymentplan: string;
  paymentcycle: string; //discount not included , Discount will be kept seperate.
  cost: number;
  perstudentcost: number;
  recordingQuota: number; //In hours
  courseCount: number;
  studentCount: number;
  tutorCount: number;
  batchCount: number;
  sessionCount: number;
  contentSize: number;
  customChapter: number;
}