import { Batch } from "../../academics/contracts/batch";
import { Student } from "../../common/contracts/user";
import { PaymentPackageInterface } from "./feeStructure_interface";

export interface CourseBundle {
  _id: string,
  batch: Batch,
  participants: string[],
  paymentpackages: PaymentPackageInterface[],
};