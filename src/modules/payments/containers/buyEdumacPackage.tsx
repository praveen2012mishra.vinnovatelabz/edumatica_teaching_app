import { createStyles, withStyles, WithStyles } from '@material-ui/styles'
import React, { FunctionComponent, useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux';
import { RootState } from '../../../store';
import { Redirect, RouteComponentProps, useHistory, withRouter } from 'react-router';
import { Organization, Tutor, User } from '../../common/contracts/user'
import MiniDrawer from '../../common/components/sidedrawer';
import { fontOptions } from '../../../theme';
import { exceptionTracker, isAdmin, isAdminTutor, isOrganization, isOrgTutor, isParent, isStudent } from '../../common/helpers';
import { useSnackbar } from 'notistack';
import { fetchOrgPaymentAccount, generateEdumacPackagePaymentOrder, getMasterPackages } from '../helper/api'
import { OrganizationPaymentAccount } from '../contracts/orgPaymentAccount_interface';
import { Box, Button, Container, Grid } from '@material-ui/core';
import EdumacServices from '../../../assets/svgs/edumac_services.svg'
import EdumacFree from '../../../assets/svgs/edumac_free.svg'
import EdumacPlus from '../../../assets/svgs/edumac_plus.svg'
import EdumacUltra from '../../../assets/svgs/edumac_ultra.svg'
import { MasterEdumacPackage } from '../contracts/masterEdumacPackage_interface';
import crypto from 'crypto'
import { AddBoxRounded } from '@material-ui/icons';

const styles = createStyles({
  topBox: {
    color: "#fff",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    flexDirection: "column",
    height: "340px",
    borderRadius: "15px",
    textAlign: "center",
    paddingBottom: "22px"
  },
  topHolder: {
    textAlign: "center",
    padding: "12px",
    width: "100%",
  }
})

interface Props extends WithStyles<typeof styles> { }
interface Props extends RouteComponentProps {
  authUser: User;
}

const PurchaseEdumacPackage: FunctionComponent<Props> = ({ classes, authUser }) => {

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [availableEdumacPackages, setAvailableEdumacPackages] = useState<MasterEdumacPackage[] | null>(null)
  const [paymentAcc, setPaymentAcc] = useState<OrganizationPaymentAccount | null>(null);
  const [redirectTo, setRedirectTo] = useState('');
  const formRef = useRef<HTMLFormElement | null>(null);
  const [signature, setSignature] = useState("")
  const [postData, setPostData] = useState<{ [key: string]: string; }>({})
  const [buyPackage, setBuyPackage] = useState<boolean>(false)

  const history = useHistory()
  // console.log(availableEdumacPackages, paymentAcc)
  useEffect(() => {
    (async () => {
      try {
        if (authUser === null) return;
        if (isStudent(authUser) === true || isOrgTutor(authUser) === true || isAdmin(authUser) === true || isParent(authUser) === true) history.push('/profile/dashboard')
        const availablePackageResponse = await getMasterPackages()
        const paymentAccountResponse = await fetchOrgPaymentAccount()
        const [edumacPackageArr, orgPaymentAccRes] = await Promise.all([
          availablePackageResponse, paymentAccountResponse
        ]);
        // console.log(edumacPackageArr, orgPaymentAccRes)
        setAvailableEdumacPackages(edumacPackageArr)
        setPaymentAcc(orgPaymentAccRes)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    if (Object.keys(postData).length !== 0) {
      const secretKey = "ab34a3156630e16436d838aeee81613d2b3df653";
      const sortedkeys = Object.keys(postData);
      var signatureData = "";

      sortedkeys.sort();
      for (var i = 0; i < sortedkeys.length; i++) {
        var k = sortedkeys[i];
        // @ts-ignore
        signatureData += k + postData[k];
      }
      console.log(signatureData)
      var signature = crypto.createHmac('sha256', secretKey).update(signatureData).digest('base64');
      setSignature(signature)
    }
  }, [postData])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const onPayment = (edumacPackage: MasterEdumacPackage) => {
    if (!paymentAcc) return
    // @ts-ignore
    const name = isAdminTutor(authUser) === true ? authUser.tutorName : authUser.organizationName
    const accountType = isAdminTutor(authUser) === true ? "Tutor" : "Organization"
    // 2.5% add to paymentAcc.totaldebitBalance
    const amount = edumacPackage.cost;
    // const amount_with_charge = (102.5 * amount)/100;
    const purchasedEdumacPackageId = paymentAcc.purchasedEdumacPackage._id
    generateEdumacPackagePaymentOrder(accountType, purchasedEdumacPackageId, parseFloat(amount.toFixed(2)), "LUMPSUM", edumacPackage._id).then(response => {
      console.log(response)
      setPostData({
        appId: response.appId,
        orderId: response.id,
        orderAmount: response.amount.toString(),
        orderCurrency: response.currency,
        orderNote: "test",
        customerName: name,
        customerPhone: authUser.mobileNo,
        // @ts-ignore
        customerEmail: authUser.emailId,
        returnUrl: response.returnUrl,
        notifyUrl: response.notifyUrl
        // notifyUrl : "http://8c4caa1fdfaa.ngrok.io/paymenthooks/payorder/authenticate"
      })
      setBuyPackage(true)
    }).catch(err => {
      console.log(err)
    })
  }

  return (
    <div>
      <MiniDrawer>
        <Container style={{ maxWidth: "1400px", marginTop: "28px" }}>
          <Box bgcolor="#fff" paddingBottom="70px" padding="18px" width="100%">
          <Grid style={{ justifyContent: 'space-between',flexDirection:'column',padding:'12px' }} container spacing={1}>
            <h2>Choose the plan that’s right for you</h2>
          Use our plus and ultra plan for unlimited access, storage and conducting classes<br />
          Change or cancel your plan anytime.
          </Grid>
          <br />
            {
              !availableEdumacPackages ? '' :
                <>
                  <Grid style={{ justifyContent: 'space-between' }} container spacing={1}>
                    <Grid item xs={12} lg={6} md={6} sm={12}>
                      <Box className={classes.topHolder} width="100%">
                        <Box bgcolor="#BAB8B8" className={classes.topBox}>
                          <img src={EdumacServices} alt="Edumatica Services" />
                          <h1>Services</h1>
                        </Box>
                      </Box>
                    </Grid>
                    <Grid item xs={3} lg={3} md={3} sm={3}>
                      <Grid container>
                        {
                          availableEdumacPackages.map(each => {
                            const bgColor = each.name === "Basic" ? "#F9BD33" : each.name === "Plus" ? "#00B9F5" : each.name === "Max" ? "#4C8BF5" : "black"
                            const source = each.name === "Basic" ? EdumacFree : each.name === "Plus" ? EdumacPlus : each.name === "Max" ? EdumacUltra : undefined
                            return (

                              <Grid item xs={12} lg={12} md={12} sm={12}>
                                <Box className={classes.topHolder}  width="100%">
                                  <Box bgcolor={bgColor} className={classes.topBox}>
                                    <img src={source} alt="Edumatica Free" />
                                    <h1>{each.name}</h1>
                                    {
                                      each.name === "Basic" ? <p>Free for<br />first month</p> : <h2>{each.cost}</h2>
                                    }
                                    {
                                      buyPackage === false ?
                                        <Button onClick={() => { onPayment(each) }} style={{ color: "#fff", borderColor: "#fff" }} size="large" variant="outlined">
                                          Buy Now
                            </Button>
                                        :
                                        <form ref={(el) => formRef.current = el} id="redirectForm" method="post" action="https://test.cashfree.com/billpay/checkout/post/submit">
                                          {Object.keys(postData).map((el) => {
                                            return <input type="hidden" name={el} value={postData[el]} />
                                          })}
                                          <input type="hidden" name="signature" value={signature} />
                                          <Button type="submit" style={{ color: "black", borderColor: "#fff" }} size="large" variant="contained" value="Pay" >Confirm</Button>

                                        </form>
                                    }
                                  </Box>
                                </Box>
                              </Grid>

                            )
                          })
                        }
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid style={{ maxHeight: "50px",justifyContent:'space-between' }} container spacing={1}>
                    <Grid item xs={6} lg={6} md={6}>
                      <Box className={classes.topHolder} padding="0 12px" width="100%">
                        <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>Per Student Cost</p>
                      </Box>
                    </Grid>
                    {
                      availableEdumacPackages.map(each => {
                        return (
                          <Grid item xs={3} lg={3} md={3} sm={3}>
                            <Box className={classes.topHolder} padding="0 12px" width="100%">
                              <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>{each.perstudentcost}/student/month</p>
                            </Box>
                          </Grid>
                        )
                      })
                    }

                  </Grid>

                  <Grid style={{ maxHeight: "50px",justifyContent:'space-between' }} container spacing={1}>
                    <Grid item xs={6}>
                      <Box className={classes.topHolder} padding="0 12px" width="100%">
                        <p style={{ fontSize: "1rem", padding: "12px" }}>No. of Students</p>
                      </Box>
                    </Grid>
                    {
                      availableEdumacPackages.map(each => {
                        return (
                          <Grid item xs={3} lg={3} md={3} sm={3}>
                            <Box className={classes.topHolder} padding="0 12px" width="100%">
                              <p style={{ fontSize: "1rem", padding: "12px" }}>{each.studentCount > 99 ? "Unlimited" : `${each.studentCount}`}</p>
                            </Box>
                          </Grid>
                        )
                      })
                    }

                  </Grid>

                  <Grid style={{ maxHeight: "50px",justifyContent:'space-between' }} container spacing={1}>
                    <Grid item xs={6}>
                      <Box className={classes.topHolder} padding="0 12px" width="100%">
                        <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>No. of Batches</p>
                      </Box>
                    </Grid>
                    {
                      availableEdumacPackages.map(each => {
                        return (
                          <Grid item xs={3} lg={3} md={3} sm={3}>
                            <Box className={classes.topHolder} padding="0 12px" width="100%">
                              <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>{each.batchCount > 99 ? "Unlimited" : `${each.batchCount}`}</p>
                            </Box>
                          </Grid>
                        )
                      })
                    }

                  </Grid>

                  <Grid style={{ maxHeight: "50px",justifyContent:'space-between' }} container spacing={1}>
                    <Grid item xs={6}>
                      <Box className={classes.topHolder} padding="0 12px" width="100%">
                        <p style={{ fontSize: "1rem", padding: "12px" }}>Payment Cycle & Plan</p>
                      </Box>
                    </Grid>
                    {
                      availableEdumacPackages.map(each => {
                        return (
                          <Grid item xs={3} lg={3} md={3} sm={3}>
                            <Box className={classes.topHolder} padding="0 12px" width="100%">
                              <p style={{ fontSize: "1rem", padding: "12px" }}>{each.paymentcycle} | {each.paymentplan}</p>
                            </Box>
                          </Grid>
                        )
                      })
                    }

                  </Grid>

                  <Grid style={{ maxHeight: "50px" ,justifyContent:'space-between'}} container spacing={1}>
                    <Grid item xs={6}>
                      <Box className={classes.topHolder} padding="0 12px" width="100%">
                        <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>Payment Grace Period</p>
                      </Box>
                    </Grid>
                    {
                      availableEdumacPackages.map(each => {
                        return (
                          <Grid item xs={3} lg={3} md={3} sm={3}>
                            <Box className={classes.topHolder} padding="0 12px" width="100%">
                              <p style={{ backgroundColor: "whitesmoke", fontSize: "1rem", padding: "12px" }}>{each.graceperiod}</p>
                            </Box>
                          </Grid>
                        )
                      })
                    }

                  </Grid>

                </>
            }
          </Box>
        </Container>
      </MiniDrawer>
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(PurchaseEdumacPackage))
);