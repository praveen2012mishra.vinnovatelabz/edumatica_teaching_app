import { createStyles, withStyles, WithStyles } from '@material-ui/styles'
import React, { FunctionComponent, useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { RootState } from '../../../store';
import { Organization, Tutor, User } from '../../common/contracts/user'
import MiniDrawer from '../../common/components/sidedrawer';
import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Container, Divider, FormControl, FormHelperText, Grid, IconButton, Input, MenuItem, Select, Tooltip, Typography } from '@material-ui/core';
import PersonWithBook from '../../../assets/images/study.png';
import { exceptionTracker, isAdmin, isAdminTutor, isOrganization, isOrgTutor, isParent, isStudent } from '../../common/helpers';
import { fetchBatchesList } from '../../common/api/academics';
import { Batch } from '../../academics/contracts/batch';
import { Redirect, RouteComponentProps, useHistory, withRouter } from 'react-router';
import MoneyIcon from '@material-ui/icons/Money';
import TodayIcon from '@material-ui/icons/Today';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import {BillingCycle} from '../enums/billing_cycle'
import { PaymentPackageInterface, PaymentPackageInterfaceReq } from '../contracts/feeStructure_interface';
import { createCourseBundle, fetchCourseBundlebyId, updateCourseBundle } from '../helper/api';
import { useSnackbar } from 'notistack';
import { Link as RouterLink, } from 'react-router-dom';
import { BillingPlan } from '../enums/billing_type';
import { CourseBundle } from '../contracts/courseBundle_interface';
import * as dateFns from "date-fns"
import { fontOptions } from '../../../theme';
import { isString, toNumber } from 'lodash';

const styles = createStyles({
  heading: {
    margin: '0',
    fontWeight: 500,
    fontSize: '24px',
    letterSpacing: '1px',
    color: '#212121'
  },
  subHeading: {
    margin: '5px 0 15px 0',
    fontWeight: 500,
    fontSize: '20px',
    lineHeight: '23px',
    color: '#000000'
  },
  label: {
    fontWeight: 500,
    fontSize: '16px',
    marginTop: '5px',
    color: '#1C2559'
  },
  btn: {
    border: '1px solid #4C8BF5'
  },
  formInput: {
    lineHeight: '18px',
    color: '#151522',

    '& input::placeholder': {
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '18px',
      color: 'rgba(0, 0, 0, 0.54)'
    }
  },
  checkLabel: {
    '& .MuiFormControlLabel-label': {
      fontFamily:fontOptions.family,
      fontSize: '15px',
      lineHeight: '19px',
      letterSpacing: '0.25px',
      color: '#202842'
    }
  },
  addBtn: {
    '& button': {
      padding: '10px 35px'
    }
  },
  clearBtn: {
    '& button': {
      border: '1px solid #666666',
      color: '#666666',
      padding: '10px 35px'
    }
  }
});

interface Props extends WithStyles<typeof styles> {}
interface Props
  extends RouteComponentProps<{
    bundleId: string;
  }> {
  authUser: User;
}

const ValidationSchema = yup.object().shape({
  // batchName: yup.string().required('Batch number is a required field').min(5),
  // startDate: yup.string().required('Start Date is a required field'),
  // endDate: yup.string().required('End Date is a required field')
});

const EditCourseBundle: FunctionComponent<Props> = ({classes, match, authUser}) => {

  const { handleSubmit, register, errors, setError } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });

  const bundleId = match.params.bundleId;
  // const user = localStorage.getItem('authUser');
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const history = useHistory()
  const [batch, setBatch] = useState<Batch>();
  const [courseBundle, setCourseBundle] = useState<CourseBundle | null>(null)
  const [formCourse, setFormCourse] = useState<string | null>('')
  const [redirectTo, setRedirectTo] = useState('');
  const [months, setMonths] = useState<number>(0)
  const [days, setDays] = useState<number>(0);
  const [fees, setFees] = useState<number>(1000);
  const [discount, setDiscount] = useState<number>(0); 
  const [grace, setGrace] = useState<number>(3);
  const [billingCycle, setBillingCycle] = useState<string>(BillingCycle.PER_MONTH)
  const [paymentPackageId, setPaymentPackageId] = useState<string>('')

  useEffect(() => {
    (async () => {
      try {
        if (authUser === null) return;
        if(isStudent(authUser) === true || isOrgTutor(authUser) === true || isAdmin(authUser) === true || isParent(authUser) === true) history.push('/profile/dashboard')
        const courseBundleResponse = await fetchCourseBundlebyId(bundleId)
        const [courseBundle] = await Promise.all([
          courseBundleResponse
        ]);
        const batch:Batch | undefined = courseBundle.batch
        if(batch === undefined) {
          history.push("/payments/feestructure")
          return
        }
        if(!batch) return
        const batchEndDate = batch.batchenddate;
        const batchStartDate = batch.batchstartdate;
        const months = dateFns.differenceInCalendarMonths(dateFns.parseISO(batchEndDate),dateFns.parseISO(batchStartDate))
        const days = dateFns.differenceInCalendarDays(dateFns.parseISO(batchEndDate),dateFns.parseISO(batchStartDate))
        const bundle:CourseBundle = courseBundle;
        bundle.paymentpackages.map(ele => {
          if(ele.paymentcycle === BillingCycle.PER_MONTH) {
            setFees(ele.totalprice)
            setDiscount(ele.discount)
            setGrace(ele.graceperiod)
            setBillingCycle(ele.paymentcycle)
            setPaymentPackageId(ele._id)
          }
          if(ele.paymentcycle === BillingCycle.LUMPSUM) {
            setFees(ele.totalprice)
            setDiscount(ele.discount)
            setGrace(ele.graceperiod)
            setBillingCycle(ele.paymentcycle)
            setPaymentPackageId(ele._id)
          }
        })
        setFormCourse(`${batch.boardname} - ${batch.classname} - ${batch.subjectname}`)
        setCourseBundle(courseBundle)
        setBatch(batch)
        setMonths(months)
        setDays(days)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if (error.response?.status === 401) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const selectPaymentCycle:string[] = [BillingCycle.LUMPSUM, BillingCycle.HALFYEARLY, BillingCycle.QUARTERLY, BillingCycle.PER_MONTH]

  const clearForm = (e: React.MouseEvent<HTMLButtonElement>) => {
    setFees(1000)
    setDiscount(0)
    setGrace(3)
  };

  const updateBundle = () => {
    const totalprice = isString(fees) === true ? toNumber(fees) : fees
    const dis = isString(discount) === true ? toNumber(discount) : discount
    if(totalprice > 99999 || totalprice <= 0) {
      enqueueSnackbar("Monthly Fees cannot be greater than 99999", {
        variant: 'error', preventDuplicate: false, persist: false,
      });
      setFees(1000)
      return
    }
    if(dis>99) {
      enqueueSnackbar("Discount cannot be more than 99", {
        variant: 'error', preventDuplicate: false, persist: false,
      });
      setDiscount(0)
      return
    }
    updateCourseBundle(bundleId, paymentPackageId, totalprice, dis).then(response => {
      if(response.code === 0) {
        history.push("/payments/feestructure")
        enqueueSnackbar("Course Bundle successfully updated", {
          variant: 'success', preventDuplicate: false, persist: false,
        });
      }
    }).catch(error => {
      console.log(error)
      exceptionTracker(error.response?.data.message);
        if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
    })
  }

  return (
    <React.Fragment>
      <MiniDrawer>
      <Container maxWidth="md">
        <Box
          bgcolor="white"
          marginY="20px"
          boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
          borderRadius="4px"
        >
          <Box borderBottom="1px solid rgba(224, 224, 224, 0.5)">
            <Grid container>
              <Grid item xs={12} md={7}>
                <Box padding="20px 30px" display="flex" alignItems="center">
                  <img src={PersonWithBook} alt="Person with Book" />

                  <Box marginLeft="15px" display="flex" alignItems="center">
                    <Typography component="span" color="secondary">
                      <Box component="h3" className={classes.heading}>
                        Fee Structure
                      </Box>
                    </Typography>
                    <Box marginLeft="10px" display="flex" justifyContent="flex-end">
                    <Box marginLeft="20px">
                      <IconButton
                        color="secondary"
                        size="small"
                        component={RouterLink}
                        to={`/payments/feestructure`}
                        className={classes.btn}
                      >
                        <ArrowBackIcon/>
                      </IconButton>
                    </Box>
                  </Box>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>

        <Box
          bgcolor="white"
          marginY="20px"
          boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
          borderRadius="4px"
          padding="18px"
        >
          <form>
            <Grid container>
              <Grid item xs={12} md={4}>
                <FormControl fullWidth margin="normal">
                  <Box className={classes.label}>Batch</Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={8}>
                <FormControl fullWidth margin="normal">
                <Input
                    placeholder="Course"
                    required
                    disabled
                    // inputRef={feesRef}
                    className={classes.formInput}
                    value={batch?.batchfriendlyname}
                  />
                </FormControl>
                {/* {errors.batchName && (
                  <FormHelperText error>
                    {errors.batchName.message}
                  </FormHelperText>
                )} */}
              </Grid>
            </Grid>

            
              <Grid container>
              <Grid item xs={12} md={4}>
                <FormControl fullWidth margin="normal">
                  <Box className={classes.label}>Course</Box>
                </FormControl>
              </Grid>

              <Grid item xs={12} md={8}>
                <FormControl fullWidth margin="normal">
                <Input
                    placeholder="Course"
                    required
                    disabled
                    // inputRef={feesRef}
                    className={classes.formInput}
                    value={formCourse}
                  />
                </FormControl>
              </Grid>
            </Grid>
            

            {
             !batch ? '' :
            
                <Box width="100%" display="flex" flexDirection="column">
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Course Fees (in Rs.)</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Enter Fees"
                          required
                          // disabled={disableAll}
                          // inputRef={feesRef}
                          endAdornment={
                            <React.Fragment>
                              <Tooltip title="Add 100">
                                <IconButton
                                  onClick={() => {
                                    setFees(Number(fees) + 100);
                                  }}
                                >
                                  <MoneyIcon />
                                </IconButton>
                              </Tooltip>
                            </React.Fragment>
                          }
                          type="number"
                          className={classes.formInput}
                          value={fees as number}
                          onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                            setFees(e.target.value as number)
                          }
                        />
                        <FormHelperText>Enter your fees</FormHelperText>
                      </FormControl>
                      {/* {errors.batchName && (
                              <FormHelperText error>
                                {errors.batchName.message}
                              </FormHelperText>
                            )} */}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Discount (in %)</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Enter Discount"
                          required
                          // disabled={disableAll}
                          // inputRef={feesRef}
                          endAdornment={
                            <React.Fragment>
                              <Tooltip title="Add 1">
                                <IconButton
                                  onClick={() => {
                                    setDiscount(Number(discount) + 1);
                                  }}
                                >
                                  <SentimentSatisfiedAltIcon />
                                </IconButton>
                              </Tooltip>
                            </React.Fragment>
                          }
                          type="number"
                          className={classes.formInput}
                          value={discount as number}
                          onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                            setDiscount(e.target.value as number)
                          }
                        />
                        <FormHelperText>Enter discount rate(if any)</FormHelperText>
                      </FormControl>
                      {/* {errors.batchName && (
                              <FormHelperText error>
                                {errors.batchName.message}
                              </FormHelperText>
                            )} */}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Payment Plan</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Payment Plan"
                          required
                          disabled
                          className={classes.formInput}
                          value={BillingPlan.PREPAID}
                        />
                        {/* <FormHelperText>PREPAID | POSTPAID</FormHelperText> */}
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Payment Cycle</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Payment Plan"
                          required
                          disabled
                          className={classes.formInput}
                          value={billingCycle}
                        />
                        <FormHelperText>
                          {
                            billingCycle === BillingCycle.LUMPSUM ? "One Time Payment" : "Monthly payment"
                          }
                        </FormHelperText>
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Grace Period (in days)</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Enter Grace Period"
                          required
                          disabled
                          // inputRef={feesRef}
                          // endAdornment={
                          //   <React.Fragment>
                          //     <Tooltip title="Add 1">
                          //       <IconButton
                          //         onClick={() => {
                          //           setGraceA(Number(graceA) + 1);
                          //         }}
                          //       >
                          //         <TodayIcon />
                          //       </IconButton>
                          //     </Tooltip>
                          //   </React.Fragment>
                          // }
                          type="number"
                          className={classes.formInput}
                          value={grace as number}
                          // onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          //   setGraceA(e.target.value as number)
                          // }
                        />
                        <FormHelperText>
                          Grace period(Free usage of platform)
                        </FormHelperText>
                      </FormControl>
                      {/* {errors.batchName && (
                              <FormHelperText error>
                                {errors.batchName.message}
                              </FormHelperText>
                            )} */}
                    </Grid>
                  </Grid>
                </Box>
            }
            

            <Box display="flex" justifyContent="flex-end" marginTop="20px">
            
              <Box
                marginRight="20px"
                display="inline-block"
                className={classes.clearBtn}
              >
                <Button
                  disableElevation
                  size="small"
                  variant="outlined"
                  onClick={clearForm}
                >
                  Clear
                </Button>
              </Box>

              <Box className={classes.addBtn}>
                <Button
                  disableElevation
                  color="primary"
                  size="small"
                  variant="contained"
                  onClick={handleSubmit(updateBundle)}
                >
                  Update
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      </Container>
      </MiniDrawer>
    </React.Fragment>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(EditCourseBundle))
);