import React, { FunctionComponent, useEffect, useState } from 'react';
import { 
  Container, Box, Grid, Typography, Input, 
  Toolbar, FormControl, InputAdornment, IconButton,
  Link, Tooltip
} from '@material-ui/core'
import { Link as RouterLink } from 'react-router-dom';
import PersonWithBook from '../../../assets/images/study.png';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { fontOptions } from '../../../theme';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
} from '@material-ui/icons';
import NotificationsIcon from '@material-ui/icons/Notifications';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import VisibilityIcon from '@material-ui/icons/Visibility';
import MiniDrawer from '../../common/components/sidedrawer';
import Datagrids from '../../common/components/datagrids'
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import * as dateFns from "date-fns"
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import StudentFeedetails from '../components/modals/studentFeedetails'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    }
  }),
);

interface StudentPay {
  packageId: string
  paymentCycle: string
  name: string;
  course: string;
  totalfee: number;
  paidfee: number | string;
  remainingfee: number | string;
  paidOn: string;
}

interface StudentCollection {
  packageId: string
  paymentCycle: string
  name: string;
  discountDeductedfee: number;
  paid: boolean;
  date: number | undefined | null;
  paidAmount: number | null;
  renewalDate : Date | null;
  currentGracePeriod: number | null;
}

interface BatchCollection {
  id: string | undefined;
  name: string;
  course: string;
  tutor: string | undefined;
  fee: number;
  discount: number;
  netearnings: number;
  collection: StudentCollection[];
}

interface Props {
  history: object,
  location: {
    state: {
      batch: BatchCollection
    }
  },
  match: object
}

interface EnhancedTableToolbarProps {
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>
  setFocused: React.Dispatch<React.SetStateAction<boolean>>
}

const EnhancedTableToolbar: FunctionComponent<EnhancedTableToolbarProps> = ({
  searchText,
  setSearchText,
  setFocused
}) => {
  return (
    <Toolbar>
      <div style={{width:'70%'}}>
        <Tooltip title="Back" style={{float: 'right', marginTop: '15px'}}>
          <Link
            to={{pathname: '/payments/feestructure'}}
            component={RouterLink}
          >
            <IconButton aria-label="back">
              <ArrowBackIcon />
            </IconButton>
          </Link>
        </Tooltip>
      </div>
      <Box display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <Box margin="15px 10px 10px 25px">
          <FormControl fullWidth margin="normal">
            <Input
              name="search"
              inputProps={{ inputMode: 'search' }}
              placeholder="Search"
              onFocus={() => setFocused(true)} 
              onBlur={() => setFocused(false)}
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              endAdornment={
                searchText.trim() !== '' ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={() => setSearchText('')}>
                      <ClearIcon />
                    </IconButton>
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <IconButton disabled size="small">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </FormControl>
          {(searchText.trim() !== '') &&
            <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666'}}>
              Filtered Table using Keyword '{searchText}'
            </Typography>
          }
        </Box>
      </Box>
    </Toolbar>
  );
};

const BatchwiseEarnings: FunctionComponent<Props> = ({ location }) => {
  const classes = useStyles();
  console.log(location)
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);
  const [studentPay, setStudentPay] = useState<StudentPay[]>()
  const [selectedStudentPay, setSelectedStudentPay] = useState<StudentPay>()
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    if(location.state && location.state.batch && focused === false){
      (async () => {
        const batchCollection = location.state.batch;
        
        let studentPay: StudentPay[] = []

        batchCollection.collection.forEach(collection => {
          console.log(collection)
          const student = {
            packageId: collection.packageId,
            paymentCycle: collection.paymentCycle,
            name: collection.name,
            course: batchCollection.course,
            totalfee: batchCollection.fee,
            paidfee: collection.paidAmount ? collection.paidAmount : '-',
            remainingfee: (collection.paidAmount ? 
              (batchCollection.fee - collection.paidAmount === 0 ? '-' : batchCollection.fee - collection.paidAmount ) 
              : batchCollection.fee
            ),
            paidOn: (collection.date ? new Date(new Date(0).setUTCSeconds(collection.date)).toLocaleDateString() : 
              ((dateFns.differenceInDays(new Date(collection.renewalDate as Date), new Date()) < (collection.currentGracePeriod as number + 2)) ? 'Grace Period' : '-')
            )
          }
          studentPay.push(student)
        })

        if(searchText !== '') {
          studentPay = studentPay.filter(pay => 
            pay.course.toLowerCase().includes(searchText.toLowerCase()) ||
            pay.name.toLowerCase().includes(searchText.toLowerCase())
          )
        }

        setStudentPay(studentPay)
      })();
    }
  }, [location, focused, searchText]);

  const notifyAction = (selectedRowDetails: StudentPay) => {
    if(selectedRowDetails) {
       
    }
  }

  const deleteAction = (selectedRowDetails: StudentPay) => {
    if(selectedRowDetails) {
        
    }
  }

  const viewAction = (selectedRowDetails: StudentPay) => {
    if(selectedRowDetails) {
      setSelectedStudentPay(selectedRowDetails)
      setOpenModal(true)
    }
  }

  const buttonData = [
    { title: 'Notify',
      action: notifyAction,
      icon: <NotificationsIcon />
    },
    { title: 'Delete',
      action: deleteAction,
      icon: <NotInterestedIcon />
    },
    { title: 'View',
      action: viewAction,
      icon: <VisibilityIcon />
    }
  ];

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.75 },
    { field: 'name', headerName: 'Name', flex: 1 },
    { field: 'course', headerName: 'Course', flex: 1 },
    { field: 'totalfee', headerName: 'Total Fee (in Rs)', flex: 1 },
    { field: 'paidfee', headerName: 'Paid Fee (in Rs)', flex: 1 },    
    { field: 'remainingfee', headerName: 'Remaining Fee (in Rs)', flex: 1 },
    { field: 'paidOn', headerName: 'Date of Payment', flex: 1 },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          name: params.getValue("name") as string,
          course: params.getValue("course") as string
        }

        const selectedRowDetails = studentPay?.find((bat, index) => {
          return (bat.name === selectedRow.name && bat.course === selectedRow.course && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as StudentPay);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = studentPay? studentPay.map((detail, index) => {
    return ({
      id: (index + 1),
      name: detail.name,
      course: detail.course,
      totalfee: detail.totalfee,
      paidfee: detail.paidfee,
      remainingfee: detail.remainingfee,
      paidOn: detail.paidOn,
      packageId: detail.packageId,
      paymentCycle: detail.paymentCycle,
    })
  }) : []

  return (
    <div>
      <MiniDrawer>
      
      <Container maxWidth="lg">
        <Box marginY="20px">
          <Grid container style={{backgroundColor: 'white', marginBottom: '20px'}}>
            <Grid item xs={12} md={7}>
              <Box padding="20px 30px" display="flex" alignItems="center">
                <img src={PersonWithBook} alt="Person with Book" />

                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Student Payment
                    </Box>
                  </Typography>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} md={5}>
              <EnhancedTableToolbar
                searchText={searchText}
                setSearchText={setSearchText}
                setFocused={setFocused}
              />
            </Grid>
          </Grid>

          <Box bgcolor="white">
            <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
          </Box>
        </Box>
      </Container>

      <StudentFeedetails
        openModal={openModal}
        onClose={() => setOpenModal(false)}
        selectedStudentPay={selectedStudentPay}
      />
      </MiniDrawer>
    </div>
  );
}

export default BatchwiseEarnings