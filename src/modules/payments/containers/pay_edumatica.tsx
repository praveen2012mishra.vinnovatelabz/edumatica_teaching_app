import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import React, { useState, useEffect, FunctionComponent } from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { RootState } from '../../../store';
import {
  User,
  Tutor,
  Student,
  Parent,
  Organization,
  Admin
} from '../../common/contracts/user';
import {
  isAdminTutor,
  isOrgTutor,
  isStudent,
  isParent,
  isAdmin,
  isOrganization
} from '../../common/helpers';
import AdminTutorPayToEdumac from '../components/layouts/adminTutor_payEdumac';
import OrganizationPayToEdumac from '../components/layouts/organization_payEdumac';

const styles = createStyles({
  root: {
    flexGrow: 1
  },
})

interface Props extends WithStyles<typeof styles> {}
interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
}

const PayEdumatica: FunctionComponent<Props> = ({
  classes,
  authUser,
  match,
}) => {
  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo('/login');
    }
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }
  if (isAdminTutor(authUser)) { 
    return <AdminTutorPayToEdumac profile={authUser as Tutor}
     />;
  }
  if (isOrganization(authUser)) { 
    return <OrganizationPayToEdumac profile={authUser as Organization}
     />;
  }

  return (
    <Redirect to={'/login'} />
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default withStyles(styles)(
  connect(mapStateToProps)(withRouter(PayEdumatica))
);
