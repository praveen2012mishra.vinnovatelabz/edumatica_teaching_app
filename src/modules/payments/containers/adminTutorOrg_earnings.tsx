import React, { FunctionComponent, useEffect, useState } from 'react';
import { Container, Box, Grid, Typography, IconButton, Tooltip } from '@material-ui/core';
import PersonWithBook from '../../../assets/images/study.png';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { fontOptions } from '../../../theme';
import { exceptionTracker } from '../../common/helpers';
import { Redirect } from 'react-router';
import { fetchStudentPaymentAccount, fetchCourseBundle } from '../helper/api';
import { PurchasedCoursePackage } from '../contracts/studentPaymentAccount_interface';
import Datagrids from '../../common/components/datagrids'
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import { Batch } from '../../academics/contracts/batch';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { LocationDisabledSharp } from '@material-ui/icons';
import MiniDrawer from '../../common/components/sidedrawer';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    },
    subheading: {
      fontFamily: fontOptions.family,
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
    },
  }),
);

interface RedirectProp {
  pathname: string;
  state: {
    batch: BatchCollection;
  }
}

interface Props {
  history: object,
  location: {
    state: {
      participants: string[];
    }
  },
  match: object;
}

interface StudentCollection {
  packageId: string
  paymentCycle: string
  name: string;
  discountDeductedfee: number;
  paid: boolean;
  date: number | undefined | null;
  paidAmount: number | null;
  renewalDate : Date | null;
  currentGracePeriod: number | null;
}

interface BatchCollection {
  id: string | undefined;
  name: string;
  course: string;
  tutor: string | undefined;
  fee: number;
  discount: number;
  netearnings: number;
  collection: StudentCollection[];
}

const AdminTutorOrgEarnings: FunctionComponent<Props> = ({location}) => {
  const classes = useStyles();
  const [redirectTo, setRedirectTo] = useState<RedirectProp | string>('');
  const [batchCollection, setBatchCollection] = useState<BatchCollection[]>([])
  const [totalEarnings, setTotalEarnings] = useState(0)

  useEffect(() => {
    (async () => {
      try {
        const courseBundleResponse = await fetchCourseBundle();
        const [courseBundleList] = await Promise.all([
          courseBundleResponse
        ]);

        let batchCollection: BatchCollection[] = []
        courseBundleList.forEach((list: any) => {
          if (!list.batch) return

          const currentBatch:Batch = list.batch;
          const batchDetail = {
            id: currentBatch._id,
            name: currentBatch.batchfriendlyname,
            course: `${currentBatch.boardname} - ${currentBatch.classname} - ${currentBatch.subjectname}`,
            tutor: currentBatch.tutorId?.tutorName,
            fee: list.paymentpackages[0].totalprice as number,
            discount: list.paymentpackages[0].discount as number,
            netearnings: 0,
            collection: []
          }

          batchCollection.push(batchDetail)
        })

        const studentPaymentAccResponse = await fetchStudentPaymentAccount(location.state.participants)

        if(batchCollection.length > 0) {
          let totalEarnings = 0;
          studentPaymentAccResponse.forEach((element: any) => {
            element.purchasedPackage.forEach((elem: PurchasedCoursePackage) => {
              const batchIndex = batchCollection.findIndex(bat => bat.id === elem.batch._id);
              if(elem.paymentTransactions.length < 1 && batchIndex > -1) {
                const collection = {
                  packageId: elem._id,
                  paymentCycle: elem.paymentPlans[0].paymentcycle,
                  name: element.student.studentName,
                  discountDeductedfee: batchCollection[batchIndex].fee * ((100 - batchCollection[batchIndex].discount)/100),
                  paid: false,
                  date: null,
                  paidAmount: null,
                  renewalDate: elem.renewalDate,
                  currentGracePeriod: elem.currentGracePeriod
                }
                
                batchCollection[batchIndex].collection.push(collection)
              } else if(elem.paymentTransactions.length > 0 && batchIndex > -1) {
                elem.paymentTransactions.forEach(transac => {
                  const collection = {
                    packageId: elem._id,
                    paymentCycle: elem.paymentPlans[0].paymentcycle,
                    name: element.student.studentName,
                    discountDeductedfee: batchCollection[batchIndex].fee,
                    paid: true,
                    date: transac.created_at,
                    paidAmount: transac.amount_paid,
                    renewalDate: elem.renewalDate,
                    currentGracePeriod: elem.currentGracePeriod
                  }

                  batchCollection[batchIndex].netearnings = batchCollection[batchIndex].netearnings + transac.amount_paid
                  totalEarnings = totalEarnings + transac.amount_paid;
                  batchCollection[batchIndex].collection.push(collection)
                })
              }
            });
          });
          setTotalEarnings(totalEarnings)
        }
        setBatchCollection(batchCollection);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [LocationDisabledSharp]);

  if (((redirectTo as string).length && (redirectTo as string).length > 0) || ((redirectTo as RedirectProp).pathname)) {
    return <Redirect to={redirectTo} />;
  }

  const expandBatch = (selectedRowDetails: BatchCollection) => {
    if(selectedRowDetails) {
      setRedirectTo({pathname: '/payments/earnings', state: { batch: selectedRowDetails }})    
    }
  }

  const buttonData = [
    { title: 'View',
      action: expandBatch,
      icon: <ArrowForwardIcon />
    }
  ];

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'tutor', headerName: 'Tutor', flex: 1 },
    { field: 'name', headerName: 'Batch', flex: 1 },
    { field: 'course', headerName: 'Course', flex: 1 },
    { field: 'netearnings', headerName: 'Net Earnings', flex: 1 },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          tutor: params.getValue("tutor") as string,
          name: params.getValue("name") as string
        }

        const selectedRowDetails = batchCollection.find((bat, index) => {
          return (bat.tutor === selectedRow.tutor && bat.name === selectedRow.name && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as BatchCollection);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = batchCollection? batchCollection.map((batch, index) => {
    return ({
      id: (index + 1),
      tutor: batch.tutor,
      name: batch.name,
      course: batch.course,
      netearnings: String(batch.netearnings)
    })
  }) : []

  return (
    <div>
      <MiniDrawer>
      
      <Container maxWidth="lg">
        <Box marginY="20px">
          <Grid container style={{backgroundColor: 'white', marginBottom: '20px'}}>
            <Grid item xs={12}>
              <Box padding="20px 30px">
                <Grid container spacing={3}>
                  <Grid item xs={1} alignItems="center">
                    <img src={PersonWithBook} alt="Person with Book" />
                  </Grid>
                  <Grid item xs={11}>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <Typography component="span" color="secondary">
                          <Box component="h3" className={classes.heading}>
                            My Payment
                          </Box>
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Grid container spacing={3}>
                          <Grid item xs={3}>
                            <Typography className={classes.subheading}>
                              Total Paid: Rs.{String(totalEarnings)}
                            </Typography>
                          </Grid>
                          {/* <Grid item xs={3}>
                            <Typography className={classes.subheading}>
                              Deductions
                            </Typography>
                          </Grid>
                          <Grid item xs={3}>
                            <Typography className={classes.subheading}>
                              Net Earnings
                            </Typography>
                          </Grid> */}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Box>
            </Grid>    
          </Grid>

          <Box bgcolor="white">
            <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
          </Box>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
}

export default AdminTutorOrgEarnings