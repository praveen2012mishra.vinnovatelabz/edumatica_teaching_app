import React, { FunctionComponent, useEffect, useState } from 'react';
import Modal from '../../../common/components/modal';
import {
  Box,
  Typography,
  Container
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { fontOptions } from '../../../../theme';
import Button from '../../../common/components/form_elements/button';
import { fetchStudentPaymentAccount, payOffline } from '../../helper/api';
import { exceptionTracker } from '../../../common/helpers';
import { PurchasedCoursePackage } from '../../contracts/studentPaymentAccount_interface';
import { useSnackbar } from 'notistack';

interface StudentPay {
  packageId: string;
  paymentCycle: string;
  name: string;
  course: string;
  totalfee: number;
  paidfee: number | string;
  remainingfee: number | string;
  paidOn: string;
}

interface Props {
  openModal: boolean;
  onClose: () => any;
  selectedStudentPay: StudentPay | undefined;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      fontFamily: fontOptions.family,
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      color: '#666666'
    },
    subheading: {
      fontFamily: fontOptions.family,
      fontWeight: fontOptions.weight.light,
      fontSize: fontOptions.size.small,
      color: '#212121'
    },
    subbox: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '10px',
      borderColor: '#c1e3f2',
      borderStyle: 'solid',
      borderWidth: '1px 0px',
      "&:hover": {
        background: "#efefef"
      },
    },
  }),
);

const StudentFeedetails: FunctionComponent<Props> = ({openModal, onClose, selectedStudentPay}) => {
  const classes = useStyles();
  const {enqueueSnackbar} = useSnackbar()
  const [redirectTo, setRedirectTo] = useState('');
  // console.log(selectedStudentPay)
  // useEffect(() => {
  //   (async () => {
  //     try {
  //       if (!selectedStudentPay) return;
  //       // const batchesListResponse = await fetchBBBBatches();
  //       let studentPaymentAccResponse = await fetchStudentPaymentAccount([])
  //       studentPaymentAccResponse = studentPaymentAccResponse[0]
  //       const [studentPaymentAccRes] = await Promise.all([
  //         studentPaymentAccResponse
  //       ]);
  //       setFeePackage(studentPaymentAccRes.purchasedPackage)
  //     } catch (error) {
  //       exceptionTracker(error.response?.data.message);
  //       if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //         setRedirectTo('/login');
  //       }
  //     }
  //   })();
  // }, [selectedStudentPay]);

  const payNow = () => {
    if(selectedStudentPay?.totalfee)
    payOffline(selectedStudentPay?.totalfee, selectedStudentPay?.packageId, "MONTHLY").then(response => {
      if(response.code === 0) {
        enqueueSnackbar("Payment Success", {
          variant:"success"
        })
      }
    })
  }

  return (
    <Modal
      open={openModal}
      handleClose={onClose}
      header={
        <Box display="flex" alignItems="center">
          <Box marginLeft="15px">
            <Typography component="span" color="secondary">
              <Box component="h3" color="white" fontWeight="400" margin="0">
                Student Details
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <Container >
        <Box className={classes.subbox}>
          <Box>
            <Typography className={classes.heading}>Student Name</Typography>
            <Typography className={classes.subheading}>{selectedStudentPay?.name}</Typography>
          </Box>
        </Box>
        <Box className={classes.subbox}>
          <Box>
            <Typography className={classes.heading}>Course</Typography>
            <Typography className={classes.subheading}>{selectedStudentPay?.course}</Typography>
          </Box>
        </Box>
        <Box className={classes.subbox}>
          <Box>
            <Typography className={classes.heading}>Fee Status</Typography>
            <Typography className={classes.subheading}>
              {selectedStudentPay?.paidfee === '-' ? 'Pending' : (selectedStudentPay?.remainingfee === '-' ? 'Paid' : 'Partialy Paid')}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.subbox}>
          <Box>
            <Typography className={classes.heading}>Payment Done date</Typography>
            <Typography className={classes.subheading}>{selectedStudentPay?.paidOn}</Typography>
          </Box>
        </Box>

        <Box>
          <Box>
            <Button variant="contained" color="primary" style={{marginTop: '20px', float: 'right'}}>
              Download Invoice
            </Button>
            <Button onClick={payNow} variant="contained" color="primary" style={{marginRight: '20px', marginTop: '20px', float: 'right'}}>
              Paid Offline
            </Button>
          </Box>
        </Box>
      </Container>
    </Modal>
  );
}

export default StudentFeedetails
