import { 
  Box, Button, Container, FormControl, 
  Grid, InputLabel, MenuItem, Select, Tab, Tabs, Typography, useTheme,
  CircularProgress, Backdrop, FormControlLabel, Radio
} from '@material-ui/core';
import { WithStyles, withStyles } from '@material-ui/styles';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import React, { FunctionComponent, useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { RootState } from '../../../../store';
import { Student } from '../../../common/contracts/user';
import MiniDrawer from '../../../common/components/sidedrawer';
import { exceptionTracker } from '../../../common/helpers';
import { generatePaymentOrder, fetchStudentPaymentAccount } from '../../helper/api';
import { CourseBundle } from '../../contracts/courseBundle_interface';
import SwipeableViews from 'react-swipeable-views';
import Scrollbars from 'react-custom-scrollbars';
import { PurchasedCoursePackage, StudentPaymentAccount } from '../../contracts/studentPaymentAccount_interface';
import { Batch } from '../../../academics/contracts/batch';
import { PaymentPackageInterfaceReq } from '../../contracts/feeStructure_interface';
import * as dateFns from "date-fns"
import PaymentNavbar from '../../../common/components/paymentNavbar';
import { fontOptions } from '../../../../theme'
import {useSnackbar} from "notistack"
import crypto from 'crypto'
import axios from 'axios';
import { Transaction } from '../../contracts/transaction_interface';

const useStyles = makeStyles((theme: Theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const styles = createStyles({
  typography_1: {
    color: '#fff',
    fontSize: '2.5rem',
    fontWeight: 700
  },
  typography_2: {
    borderRadius: '10px',
    color: '#00B9F5',
    backgroundColor: '#fff',
    fontSize: '2.5rem',
    fontWeight: 700,
    padding: '2px 25px'
  },
  root: {
    flexGrow: 1
  },
  mainbox: {
    width: '100%',
    height: '50vH'
  },
  subbox: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '30px 20px',
    borderColor: '#c1e3f2',
    borderStyle: 'solid',
    borderWidth: '1px 0px',
    "&:hover": {
      cursor: "pointer",
      background: "#efefef"
    },
  },
  activeSubbox: {
    background: "#efefef"
  },
  divider: {
    borderColor: '#c1e3f2',
    borderStyle: 'solid',
    borderWidth: '0px 1px 0px 0px',
  },
  batchListHead: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.medium,
    fontWeight: fontOptions.weight.normal,
  },
  batchList: {
    fontFamily: fontOptions.family,
    fontSize: fontOptions.size.small,
    fontWeight: fontOptions.weight.light,
  },
  courseRate: {
    textAlign: 'center'
  },
  courseDiscount: {
    textDecoration: 'line-through',
    textDecorationColor: '#DC143C',
    textDecorationThickness: '2px'
  }
});

function loadScript(src:string) {
	return new Promise((resolve) => {
		const script = document.createElement('script')
		script.src = src
		script.onload = () => {
			resolve(true)
		}
		script.onerror = () => {
			resolve(false)
		}
		document.body.appendChild(script)
	})
}

async function displayRazorpay(response:any, profile:Student, handleBackdropOpen: () => void) {
  const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js')

  if (!res) {
    alert('Razorpay SDK failed to load. Are you online?')
    return
  }

  console.log(response)

  const options = {
    key: process.env.RAZORPAY_SECRET,
    currency: response.currency,
    amount: response.amount,
    order_id: response.id,
    name: 'Payment',
    description: 'Pay to EDUMAC',
    // image: {Logo},
    handler: function async(response: any) {
      console.log(response)
      const data = {
        orderCreationId: response.id,
        razorpayPaymentId: response.razorpay_payment_id,
        razorpayOrderId: response.razorpay_order_id,
        razorpaySignature: response.razorpay_signature
      };
      handleBackdropOpen()
    },
    prefill: {
      name: profile.studentName,
      email: profile.emailId,
      phone_number: profile.mobileNo
    },
    // notes: {
    //   address: 'MU-Zero Corporate Office'
    // },
    modal: {
      ondismiss: function () {
        console.log('Checkout form closed');
      }
    }
    // theme: {
    //     color: "coral",
    // },
  };
  // @ts-ignore
  const paymentObject = new window.Razorpay(options)
  paymentObject.open()
}

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

interface PackageActivePlanIndex {
  package: string;
  index: number
}

interface Props extends WithStyles<typeof styles> {
  profile: Student;
}

const StudentFeeStructureView: FunctionComponent<Props> = ({
  classes,
  profile
}) => {
  const user = localStorage.getItem('authUser');
  const [redirectTo, setRedirectTo] = useState('');
  const [activePackage, setActivePackage] = useState<PurchasedCoursePackage | null>(null)
  const [activeBatch, setactiveBatch] = useState<Batch | null>(null)
  const [packages, setPackages] = useState<PurchasedCoursePackage[]>([])
  const [plans, setPlans] = useState<PaymentPackageInterfaceReq[]>([])
  const [packageActivePlanIndex, setPackageActivePlanIndex] = useState<PackageActivePlanIndex[]>([])
  const [paymentsDue, setPaymentsDue] = useState<PurchasedCoursePackage[] | null>(null)
  const [pastTransactions, setPastTransactions] = useState<Transaction[] | null>(null)
  const [postPaymentRefresh, setPostPaymentRefresh] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [pay, setPay] = useState<boolean>(false)

  const [signature, setSignature] = useState("")
  const formRef = useRef<HTMLFormElement | null>(null);
  const [postData,setPostData] = useState<{ [key: string]: string; }>({})

  const theme = useTheme();
  const styles = useStyles();
  const {enqueueSnackbar} = useSnackbar();
  const [value, setValue] = React.useState(0);

  useEffect(() => {
    (async () => {
      try {
        if (user === null || !profile._id) return;
        // const batchesListResponse = await fetchBBBBatches();
        let studentPaymentAccResponse = await fetchStudentPaymentAccount([profile._id])
        studentPaymentAccResponse = studentPaymentAccResponse[0]
        const [studentPaymentAccRes] = await Promise.all([
          studentPaymentAccResponse
        ]);
        setPackages(studentPaymentAccRes.purchasedPackage)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo, postPaymentRefresh]);

  useEffect(() => {
    let dueArr: PurchasedCoursePackage[] = []
    let pastTransactionArr: Transaction[] = []
    packages.forEach(el => {
      pastTransactionArr = [...pastTransactionArr,...el.paymentTransactions]

      const filter = dateFns.differenceInDays(new Date(el.renewalDate), new Date());
      if(el.paymentComplete !== true && filter < (el.currentGracePeriod + 2)) {
        dueArr.push(el)
      }
    })

    if(paymentsDue !== null && paymentsDue.length !== 0) {
      let payStatus;

      paymentsDue.forEach(due => {
        const status = dueArr.some(arr => arr._id === due._id)
        if(status === false) {
          payStatus = 'success'
        }
      })

      if(payStatus === 'success') {
        enqueueSnackbar('Transaction Success', { variant: "success" })
      } else {
        enqueueSnackbar('Your Transaction is being processed. Please check the status again in a while', { variant: "success" })
      }
    }
    setPaymentsDue(dueArr)
    setPastTransactions(pastTransactionArr)
  }, [packages])

  useEffect(()=>{
    if(Object.keys(postData).length!==0){
      const secretKey = "ab34a3156630e16436d838aeee81613d2b3df653";
      const sortedkeys = Object.keys(postData);
      var signatureData = "";

      sortedkeys.sort();
      for (var i = 0; i < sortedkeys.length; i++) {
        var k = sortedkeys[i];
        // @ts-ignore
        signatureData += k + postData[k];
      }
      console.log(signatureData)
      var signature = crypto.createHmac('sha256',secretKey).update(signatureData).digest('base64');
      setSignature(signature)
      }
  },[postData])


  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleBackdropClose = () => {
    setOpen(false);
    setPostPaymentRefresh(prevPostpaymentRefresh => !prevPostpaymentRefresh)
  };

  const handleBackdropOpen = () => {
    setOpen(true);
    setTimeout(() => handleBackdropClose(), 7000);
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  const activateBundle = (bundle: PurchasedCoursePackage, batch: Batch | undefined) => {
    if(!batch) return
    setActivePackage(bundle)
    setPlans(bundle.paymentPlans)
    setactiveBatch(batch)
  }

  const clickToPay = (amount:number, paymentcycle:string) => {
    if(!activePackage || !activePackage._id || !activePackage.batch._id) return
    // 2.5% add to amount
    // const amount_with_charge = (102.5 * amount)/100;
    generatePaymentOrder(parseFloat(amount.toFixed(2)), activePackage._id, activePackage?.batch._id, paymentcycle).then(response => {
      console.log(response)
      setPostData({
        appId : response.appId,
        orderId : response.id,
        orderAmount : response.amount.toString(),
        orderCurrency : response.currency,
        orderNote : "test",
        customerName : profile.studentName,
        customerPhone : profile.mobileNo,
        customerEmail : profile.emailId,
        returnUrl : response.returnUrl,
        notifyUrl : response.notifyUrl
        // notifyUrl : "http://8c4caa1fdfaa.ngrok.io/paymenthooks/payorder/authenticate"
      })
      setPay(true)
    }).catch(err => {
      console.log(err)
    })
    
  }

  const clearAll = () => {
    setPackageActivePlanIndex([])
    setActivePackage(null)
    setPlans([])
    setactiveBatch(null)
  }
 
  return (
    <div>
      <MiniDrawer>
      <PaymentNavbar />

      <Container style={{maxWidth:"88vW"}}>
        <Box margin="50px 0px 32px" width="100%" height="75vH" bgcolor="#fff" boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)">
          <Grid container spacing={3}>
            <Grid item xs={7} className={classes.divider}>
              <Box width="100%" padding="12px">
                <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor="primary"
                  textColor="primary"
                  variant="fullWidth"
                  aria-label="full width tabs example"
                  centered
                >
                  <Tab onClick={clearAll} label="Batch/Course" {...a11yProps(0)} />
                  <Tab onClick={clearAll} label="Payment Due" {...a11yProps(1)} />
                  <Tab onClick={clearAll} label="Previous Payments" {...a11yProps(2)} />
                </Tabs>

                <SwipeableViews
                  axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                  index={value}
                  onChangeIndex={handleChangeIndex}
                >
                  <TabPanel value={value} index={0} dir={theme.direction}>
                    <Box className={classes.mainbox}>
                      <Scrollbars>
                        <Box marginBottom="15px">
                          <Typography className={classes.batchListHead}>
                            {profile.studentName}
                          </Typography>
                          <Typography className={classes.batchList}>
                            {profile.className}
                          </Typography>
                        </Box>
                        {
                          packages.map(el => {
                            const activeBatch = el.batch;
                            const paymentPlans: PaymentPackageInterfaceReq[] = el.paymentPlans;
                            const currentIndex = (packageActivePlanIndex.some(list => list.package === el._id) ?
                              packageActivePlanIndex.find(list => list.package === el._id)?.index : -1
                            )
                            
                            return (
                              <Box onClick={() => activateBundle(el,activeBatch)}
                                className={(activePackage?._id === el._id) ? `${classes.activeSubbox} ${classes.subbox}` : `${classes.subbox}`} 
                              >
                                <Grid container spacing={1}>
                                  <Grid item sm={5}>
                                    <Typography className={classes.batchListHead}>
                                      <Radio size="small" checked={activePackage?._id === el._id} color="primary"/>
                                      {activeBatch?.batchfriendlyname}
                                    </Typography>
                                    <Typography className={classes.batchList} style={{marginLeft: '37px'}}>
                                      {activeBatch?.classname} - {activeBatch?.subjectname}
                                    </Typography>
                                  </Grid>

                                  <Grid item sm={4}>
                                    <FormControl style={{width:"120px"}} variant="outlined">
                                      <InputLabel style={{padding:"2px",backgroundColor:"#fff", border:"none"}}>Plans</InputLabel>
                                      <Select
                                        value={currentIndex}
                                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>{
                                          const selectsIndex = packageActivePlanIndex.findIndex(list => list.package === el._id)
                                          let currentPackageActivePlanIndex = [...packageActivePlanIndex];
                                          if(selectsIndex === -1) {
                                            currentPackageActivePlanIndex.push({package: el._id, index: e.target.value as number})
                                          } else {
                                            currentPackageActivePlanIndex[selectsIndex].index = e.target.value as number;
                                          }
                                          setPackageActivePlanIndex(currentPackageActivePlanIndex);
                                        }}
                                      >
                                        <MenuItem value="-1">Select Payment Cycle</MenuItem>
                                        {paymentPlans.map((ele, index) => (
                                          <MenuItem key={index} value={index}>
                                            {ele.paymentcycle}
                                          </MenuItem>
                                        ))}
                                      </Select>
                                    </FormControl>
                                  </Grid>

                                  <Grid item sm={3}>
                                    {/* <Box bgcolor="whitesmoke" border="1px solid skyblue" padding="4px" borderRadius="3px">
                                          {paymentPlans[0].paymentcycle}
                                        </Box> */}
                                    {
                                      (currentIndex as number) < 0 ? '' : 
                                      <>
                                        <Typography className={`${classes.batchList} ${classes.courseRate} ${classes.courseDiscount}`}>Rs. {paymentPlans[currentIndex as number].totalprice}</Typography>
                                        <Typography className={`${classes.batchListHead} ${classes.courseRate}`}>Rs. {paymentPlans[currentIndex as number].totalprice*((100-paymentPlans[currentIndex as number].discount)/100)}</Typography>
                                      </>
                                    }
                                  </Grid>
                                </Grid>
                              </Box>
                            )
                          })
                        }
                      </Scrollbars>
                    </Box>
                  </TabPanel>
                  <TabPanel value={value} index={1} dir={theme.direction}>
                    <Box className={classes.mainbox}>
                      <Scrollbars>
                        <Box marginBottom="15px">
                          <Typography className={classes.batchListHead}>
                            {profile.studentName}
                          </Typography>
                          <Typography className={classes.batchList}>
                            {profile.className}
                          </Typography>
                        </Box>
                        {
                          paymentsDue?.map(el => {
                            const activeBatch = el.batch;
                            const paymentPlans: PaymentPackageInterfaceReq[] = el.paymentPlans;
                            
                            return (
                              <Box className={classes.subbox}>
                                <Box>
                                  <Typography className={classes.batchListHead}>{activeBatch?.batchfriendlyname}</Typography>
                                  <Typography className={classes.batchList}>{activeBatch?.classname} - {activeBatch?.subjectname}</Typography>
                                </Box>
                                <Box>
                                  <Typography className={classes.batchList}>{new Date(el.renewalDate).toLocaleDateString()}</Typography>
                                </Box>
                                <Box>
                                  <Typography className={classes.batchList}>Rs. {paymentPlans[0].totalprice*((100-paymentPlans[0].discount)/100)}</Typography>
                                </Box>
                              </Box>
                            )
                          })
                        }
                      </Scrollbars>
                    </Box>              
                  </TabPanel>
                  <TabPanel value={value} index={2} dir={theme.direction}>
                    <Box className={classes.mainbox}>
                      <Scrollbars>
                        <Box marginBottom="15px">
                          <Typography className={classes.batchListHead}>
                            {profile.studentName}
                          </Typography>
                          <Typography className={classes.batchList}>
                            {profile.className}
                          </Typography>
                        </Box>
                        {
                          pastTransactions?.map(el => {
                            return (
                              <Box className={classes.subbox}>
                                <Box>
                                  <Typography className={classes.batchListHead}>{el.batchId?.batchfriendlyname}</Typography>
                                  <Typography className={classes.batchList}>{el.batchId?.classname} - {el.batchId?.subjectname}</Typography>
                                </Box>
                                <Box>
                                  <Typography className={classes.batchList}>{el.created_at ? new Date(new Date(0).setUTCSeconds(el.created_at)).toLocaleDateString() : ''}</Typography>
                                </Box>
                                <Box>
                                  <Typography className={classes.batchList}>{el.amount_paid}</Typography>
                                </Box>
                              </Box>
                            )
                          })
                        }
                      </Scrollbars>
                    </Box>
                  </TabPanel>
                </SwipeableViews>
              </Box>
            </Grid>
            <Grid item xs={5}>
              <div style={{ width: '100%', padding: '12px' }}>
                <Box display="flex" justifyContent="center" margin="70px 0px">
                  {
                    (packageActivePlanIndex.length !== 0 && activePackage && packageActivePlanIndex.some(list => list.package === activePackage._id) ? 
                      ((packageActivePlanIndex.find(list => list.package === activePackage._id) as PackageActivePlanIndex).index >= 0 ? 
                        <Box textAlign="center" color="#666666">
                          <Typography style={{fontSize: fontOptions.size.large, marginBottom: '5px'}}>
                            Sub Total - Rs {(plans[((packageActivePlanIndex.find(list => list.package === activePackage._id)) as PackageActivePlanIndex).index].totalprice*((100-plans[((packageActivePlanIndex.find(list => list.package === activePackage._id)) as PackageActivePlanIndex).index].discount)/100)).toFixed(2)}
                          </Typography>
                          <Typography style={{fontSize: fontOptions.size.medium, marginBottom: '5px'}}>
                            *Inclusive all taxes
                          </Typography>
                          {
                            ((activePackage?.paymentComplete === true) || (dateFns.differenceInDays(new Date((activePackage as PurchasedCoursePackage).renewalDate), new Date()) < ((activePackage as PurchasedCoursePackage).currentGracePeriod + 8)) ) ? 
                            <>
                              {
                                pay ? '' : 
                                <Button style={{marginBottom: '10px'}} size="large" onClick={() => clickToPay(plans[((packageActivePlanIndex.find(list => list.package === activePackage._id)) as PackageActivePlanIndex).index].totalprice*((100-plans[((packageActivePlanIndex.find(list => list.package === activePackage._id)) as PackageActivePlanIndex).index].discount)/100), plans[((packageActivePlanIndex.find(list => list.package === activePackage._id)) as PackageActivePlanIndex).index].paymentcycle)}  disableElevation  color="primary" variant="contained">
                                  <Typography style={{fontSize: fontOptions.size.mediumPlus}}>Proceed to Pay</Typography>
                                </Button>
                              }
                                
                              {
                                <form ref={(el) => formRef.current=el} id="redirectForm" method="post" action="https://test.cashfree.com/billpay/checkout/post/submit">
                                {Object.keys(postData).map((el)=>{
                                  return <input type="hidden" name={el} value={postData[el]} />
                                })}
                                <input type="hidden" name="signature" value={signature}/>
                                {/* <Button type="submit" color="primary" variant="contained" size="large" value="Pay">Confirm Pay to Edumac</Button> */}
                                {
                                  !pay ? '' : <Button type="submit" color="primary" disableElevation variant="contained" size="large" value="Pay" >Confirm</Button>
                                } 
                                
                              </form>
                              }
                              <Typography style={{fontSize: fontOptions.size.small, marginBottom: '5px'}}>100% Secure payment</Typography>
                              <Typography style={{fontSize: fontOptions.size.small, marginBottom: '5px'}}>Pay using any of our payment gateway</Typography>
                            </>
                            :
                            <Button style={{marginBottom: '10px'}} size="large" disabled disableElevation color="primary" variant="contained">
                              <Typography style={{fontSize: fontOptions.size.mediumPlus}}>Proceed to Pay</Typography>
                            </Button>
                          }
                        </Box>
                        : ''
                      ) : ''
                    )
                  }
                </Box>
              </div>
            </Grid>
          </Grid>
        </Box>
      </Container>

      <Backdrop className={styles.backdrop} open={open}>
        <div style={{textAlign: 'center'}}>
          <CircularProgress color="inherit" />
          <Typography style={{fontSize: fontOptions.size.medium, marginTop: '5px'}}>Please wait while transaction takes place</Typography>
        </div>
      </Backdrop>
      </MiniDrawer>
    </div>
  )
}

function mapStateToProps(state: RootState) {
  return {

  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(StudentFeeStructureView)
);
