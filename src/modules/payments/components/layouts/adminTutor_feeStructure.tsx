import { Box, Button, Input, InputAdornment, FormControl, Container, Grid, Toolbar, IconButton, Tooltip, Typography, Select, MenuItem } from '@material-ui/core';
import { createStyles, WithStyles, withStyles } from '@material-ui/styles';
import PersonWithBook from '../../../../assets/images/study.png';
import React, { FunctionComponent, useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { Redirect, useHistory } from 'react-router';
import { RootState } from '../../../../store';
// import { BBBBatch } from '../../../bbbconference/contracts/bbbBatch_interface';
// import { fetchBBBBatches } from '../../../bbbconference/helper/api';
import MiniDrawer from '../../../common/components/sidedrawer';
import { Tutor } from '../../../common/contracts/user';
import { exceptionTracker } from '../../../common/helpers';
import { CourseBundle } from '../../contracts/courseBundle_interface';
import { deleteCourseBundle, fetchCourseBundle } from '../../helper/api';
import { Link as RouterLink, } from 'react-router-dom';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
} from '@material-ui/icons';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { Batch } from '../../../academics/contracts/batch';
import { fontOptions } from '../../../../theme';
import Datagrids from '../../../common/components/datagrids'
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import { uniq } from 'lodash'
import { makeStyles, Theme } from '@material-ui/core/styles';

const styles = createStyles({
  heading: {
    margin: '0',
    fontWeight: fontOptions.weight.bold,
    fontSize: fontOptions.size.medium,
    letterSpacing: '1px',
    color: '#212121'
  },
  root: {
    width: '100%'
  },
  addBtn: {
    '& svg': {
      marginRight: '5px'
    }
  }
});

interface Data {
  id: string,
  course: string,
  batchname: string,
  batchstartdate: string,
  batchenddate: string,
  coursefee: number,
  discount: number,
  paymentplan: string,
  paymentcycle: string,
  graceperiod: number,
}

function createData(
  id: string,
  course: string,
  batchname: string,
  batchstartdate: string,
  batchenddate: string,
  coursefee: number,
  discount: number,
  paymentplan: string,
  paymentcycle: string,
  graceperiod: number,
): Data {
  return { id, course, batchname, batchstartdate, batchenddate, coursefee, discount, paymentplan, paymentcycle, graceperiod };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 130,
      marginTop: '23px'
    }
  }),
);

interface EnhancedTableToolbarProps {
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>
  setFocused: React.Dispatch<React.SetStateAction<boolean>>
  setRedirectTo: React.Dispatch<React.SetStateAction<string>>
}

const EnhancedTableToolbar: FunctionComponent<EnhancedTableToolbarProps> = ({
  searchText,
  setSearchText,
  setFocused,
  setRedirectTo
}) => {
  const classes = useStyles();

  const [feepages] = React.useState('feestruct');

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if(event.target.value === 'pay') {
      setRedirectTo('/payments/pay')
    }
  };

  return (
    <Toolbar>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <FormControl className={classes.formControl}>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={feepages}
            onChange={handleChange}
          >
            <MenuItem value='feestruct'>Fee Structure</MenuItem>
            <MenuItem value='pay'>Payment Desk</MenuItem>
          </Select>
        </FormControl>
      </Box>
      <Box display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <Box margin="15px 10px 10px 25px">
          <FormControl fullWidth margin="normal">
            <Input
              name="search"
              inputProps={{ inputMode: 'search' }}
              placeholder="Search"
              onFocus={() => setFocused(true)} 
              onBlur={() => setFocused(false)}
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              endAdornment={
                searchText.trim() !== '' ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={() => setSearchText('')}>
                      <ClearIcon />
                    </IconButton>
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <IconButton disabled size="small">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </FormControl>
          {(searchText.trim() !== '') &&
            <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666'}}>
              Filtered Table using Keyword '{searchText}'
            </Typography>
          }
        </Box>
      </Box>
    </Toolbar>
  );
};

interface Props extends WithStyles<typeof styles> {
  profile: Tutor;
}

const AdminTutorFeeStructure: FunctionComponent<Props> = ({
  classes,
  profile
}) => {
  const history = useHistory()
  const user = localStorage.getItem('authUser');
  const { enqueueSnackbar } = useSnackbar();
  const [courseBundle, setCourseBundle] = useState<CourseBundle[] | null>(null)
  const [redirectTo, setRedirectTo] = useState('');
  const [trigger, setTrigger] = useState<number>(0)
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);
  const [participants, setParticipants] = useState<string[]>([]);

  const [rows, setRows] = useState<Data[] | null>(null)

  useEffect(() => {
    (async () => {
      // if(focused === false) {
        try {
          if (user === null) return;
          const courseBundleResponse = await fetchCourseBundle();
          const [courseBundleList] = await Promise.all([
            courseBundleResponse
          ]);

          let datarows: Data[] = [];
          let participants: string[] = []
          const bundles: CourseBundle[] = courseBundleList;
          bundles.forEach(bundle => {
            if (!bundle.batch) return
            const currentBatch:Batch = bundle.batch
            participants = [...participants, ...bundle.participants]
            datarows.push(createData(
              bundle._id,
              `${currentBatch.boardname} - ${currentBatch.classname} - ${currentBatch.subjectname}`,
              currentBatch.batchfriendlyname,
              currentBatch.batchstartdate,
              currentBatch.batchenddate,
              bundle.paymentpackages[0].totalprice,
              bundle.paymentpackages[0].discount,
              bundle.paymentpackages[0].paymentplan,
              bundle.paymentpackages[0].paymentcycle,
              bundle.paymentpackages[0].graceperiod
            ))
          })

          participants = uniq(participants);

          if(searchText !== '') {
            datarows = datarows.filter(row => 
              row.course.toLowerCase().includes(searchText.toLowerCase()) ||
              row.batchname.includes(searchText) ||
              row.paymentplan.toLowerCase().includes(searchText.toLowerCase()) ||
              row.paymentcycle.toLowerCase().includes(searchText.toLowerCase())
            )
          }

          setParticipants(participants)
          setCourseBundle(courseBundleList)
          setRows(datarows)
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      // }
    })();
  }, [profile.mobileNo, trigger, searchText]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleEdit = (row: Data) => {
    // console.log(row)
    // dispatch(setCurrentStudent(row));
    history.push(`/payments/feestructure/edit/${row.id}`);
  };

  const handleDelete = (row: Data) => {
    // console.log(row)
    deleteCourseBundle(row.id).then(response => {
      if(response.code === 0) {
        setTrigger((Math.random() * 100) + 1)
        enqueueSnackbar("Course Bundle deleted successfully", {
          variant: 'success', preventDuplicate: false, persist: false,
        });
      }
    }).catch(error => {
      console.log(error)
      enqueueSnackbar("Could not delete Course Bundle", {
        variant: 'error', preventDuplicate: false, persist: false,
      });
      exceptionTracker(error.response?.data.message);
        if (error.response?.status === 401) {
          setRedirectTo('/login');
        }
    })
  };

  const buttonData = [
    {
      title: 'Edit Details',
      action: handleEdit,
      icon: <EditIcon />
    },
    {
      title: 'Delete Details',
      action: handleDelete,
      icon: <DeleteIcon />
    }
  ];

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.75 },
    { field: 'course', headerName: 'Course', flex: 1 },
    { field: 'batchname', headerName: 'Batch', flex: 1 },
    { field: 'batchstartdate', headerName: 'Starts On', flex: 1 },    
    { field: 'batchenddate', headerName: 'Ends On', flex: 1 },
    { field: 'coursefee', headerName: 'Course Fee', flex: 1 },
    { field: 'discount', headerName: 'Discount', flex: 1 },
    { field: 'paymentplan', headerName: 'Payment Plan', flex: 1 },
    { field: 'paymentcycle', headerName: 'Payment Cycle', flex: 1 },
    { field: 'graceperiod', headerName: 'Grace Period', flex: 1 },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          course: params.getValue("course") as string,
          batchname: params.getValue("batchname") as string
        }

        const selectedRowDetails = rows?.find((row, index) => {
          return (row.course === selectedRow.course && row.batchname === selectedRow.batchname && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as Data);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = rows? rows.map((row, index) => {
    return ({
      id: (index + 1),
      course: row.course,
      batchname: row.batchname,      
      batchstartdate: moment(`${row.batchstartdate}`).utc().format('DD/MM/YYYY'),
      batchenddate: moment(`${row.batchenddate}`).utc().format('DD/MM/YYYY'),
      coursefee: 'Rs. ' + row.coursefee,
      discount: row.discount + ' %',
      paymentplan: row.paymentplan,
      paymentcycle: row.paymentcycle,
      graceperiod: row.graceperiod + ' days from the date of purchase'
    })
  }) : []

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box marginY="20px">
          <Grid container style={{backgroundColor: 'white', marginBottom: '20px'}}>
            <Grid item xs={12} md={7}>
              <Box padding="20px 30px" display="flex" alignItems="center">
                <img src={PersonWithBook} alt="Person with Book" />

                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Fee Structure
                    </Box>
                  </Typography>

                  <Box marginLeft="10px" display="flex">
                    <Box marginLeft="20px" className={classes.addBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="small"
                        component={RouterLink}
                        to={`/payments/feestructure/create`}
                      >
                        + Add
                      </Button>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="small"
                        component={RouterLink}
                        to={{pathname: '/payments/allearnings', state: { participants: participants }}}
                        style={{marginLeft: '20px'}}
                      >
                        View Earnings
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} md={5}>
              <EnhancedTableToolbar
                searchText={searchText}
                setSearchText={setSearchText}
                setFocused={setFocused}
                setRedirectTo={setRedirectTo}
              />
            </Grid>
          </Grid>

          <Box bgcolor="white">
            <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
          </Box>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
}

function mapStateToProps(state: RootState) {
  return {

  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(AdminTutorFeeStructure)
);
