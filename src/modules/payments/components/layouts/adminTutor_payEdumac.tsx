import React, { createRef, FunctionComponent, useEffect, useRef, useState } from 'react'
import { WithStyles, withStyles } from '@material-ui/styles';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { RootState } from '../../../../store';
import { exceptionTracker } from '../../../common/helpers';
import { Tutor } from '../../../common/contracts/user';
import { fetchOrgPaymentAccount, generateOrgPaymentOrder } from '../../helper/api';
import { OrganizationPaymentAccount } from '../../contracts/orgPaymentAccount_interface';
import { Box, Button, Container, Backdrop, Typography, CircularProgress, MenuItem, Grid, FormControl, InputLabel, Select } from '@material-ui/core';
import PaymentNavbar from '../../../common/components/paymentNavbar';
import { fontOptions } from '../../../../theme'
import * as dateFns from "date-fns";
import crypto from 'crypto'
import axios from 'axios';
import MiniDrawer from '../../../common/components/sidedrawer';

const useStyles = makeStyles((theme: Theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  divider: {
    borderColor: '#c1e3f2',
    borderStyle: 'solid',
    borderWidth: '0px 1px 0px 0px',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 130,
    float: 'right',
    marginTop: '20px',
    marginBottom: '30px'
  }
}));

const styles = createStyles({
  typography_1: {
    color: '#fff',
    fontSize: '2.5rem',
    fontWeight: 700
  },
  typography_2: {
    borderRadius: '10px',
    color: '#00B9F5',
    backgroundColor: '#fff',
    fontSize: '2.5rem',
    fontWeight: 700,
    padding: '2px 25px'
  },
  root: {
    flexGrow: 1
  },
})

function loadScript(src:string) {
	return new Promise((resolve) => {
		const script = document.createElement('script')
		script.src = src
		script.onload = () => {
			resolve(true)
		}
		script.onerror = () => {
			resolve(false)
		}
		document.body.appendChild(script)
	})
}

async function displayRazorpay(response:any, profile:Tutor, handleBackdropOpen: () => void) {
  const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js')

  if (!res) {
    alert('Razorpay SDK failed to load. Are you online?')
    return
  }

  console.log(response)

  const options = {
    key: process.env.RAZORPAY_SECRET,
    currency: response.currency,
    amount: response.amount,
    order_id: response.id,
    name: 'Payment',
    description: 'Pay to EDUMAC',
    // image: {Logo},
    handler: function async(response: any) {
      console.log(response);
      const data = {
        orderCreationId: response.id,
        razorpayPaymentId: response.razorpay_payment_id,
        razorpayOrderId: response.razorpay_order_id,
        razorpaySignature: response.razorpay_signature
      };
      handleBackdropOpen()
    },
    prefill: {
      name: profile.tutorName,
      email: profile.emailId,
      phone_number: profile.mobileNo
    },
    // notes: {
    //   address: 'MU-Zero Corporate Office'
    // },
    modal: {
      ondismiss: function () {
        console.log('Checkout form closed');
      }
    }
    // theme: {
    //     color: "coral",
    // },
  };
  // @ts-ignore
  const paymentObject = new window.Razorpay(options)
  paymentObject.open()
}

interface Props extends WithStyles<typeof styles> {
  profile: Tutor;
}

const AdminTutorPayToEdumac:FunctionComponent<Props> = ({classes, profile}) => {
  const user = localStorage.getItem('authUser');
  const styles = useStyles();

  const [redirectTo, setRedirectTo] = useState('');
  const [paymentAcc, setPaymentAcc] = useState<OrganizationPaymentAccount | null>(null);
  const [open, setOpen] = React.useState(false);
  const [postPaymentRefresh, setPostPaymentRefresh] = useState(false);
  const [activePlanIndex, setActivePlanIndex] = useState("")
  const [signature, setSignature] = useState("")
  const formRef = useRef<HTMLFormElement | null>(null);
  const [postData,setPostData] = useState<{ [key: string]: string; }>({})
  const [pay, setPay] = useState<boolean>(false)
  const [feepages] = React.useState('pay');

  useEffect(()=>{
    if(Object.keys(postData).length!==0){
      const secretKey = "ab34a3156630e16436d838aeee81613d2b3df653";
    const sortedkeys = Object.keys(postData);
    var signatureData = "";

    sortedkeys.sort();
    for (var i = 0; i < sortedkeys.length; i++) {
      var k = sortedkeys[i];
      // @ts-ignore
      signatureData += k + postData[k];
    }
    console.log(signatureData)
    var signature = crypto.createHmac('sha256',secretKey).update(signatureData).digest('base64');
    setSignature(signature)
    }
  },[postData])


  useEffect(() => {
    (async () => {
      try {
        if (user === null || !profile._id) return;

        const orgPaymentAccResponse = await fetchOrgPaymentAccount()
        const [orgPaymentAccRes] = await Promise.all([
          orgPaymentAccResponse
        ]);
        setPaymentAcc(orgPaymentAccRes)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [profile.mobileNo, postPaymentRefresh]);

  // useEffect(() => {
  //   setTimeout(() => {
  //     if(signature !== "") formRef.current?.submit()
  //   }, 3000);
  // }, [signature])
  // useEffect(() => {
  //   if(signature !== "") formRef.current?.submit()
  // }, [signature])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleBackdropClose = () => {
    setOpen(false);
    setPostPaymentRefresh(prevPostpaymentRefresh => !prevPostpaymentRefresh)
  };

  const handleBackdropOpen = () => {
    setOpen(true);
    setTimeout(() => handleBackdropClose(), 7000);
  }

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if(event.target.value === 'feestruct') {
      setRedirectTo('/payments/feestructure')
    }
  };

  const clickToPay = () => {
    if(!paymentAcc || !paymentAcc.totaldebitBalance) return
    // 2.5% add to paymentAcc.totaldebitBalance
    const amount =  paymentAcc.totaldebitBalance;
    // const amount_with_charge = (102.5 * amount)/100;
    const purchasedEdumacPackageId = paymentAcc.purchasedEdumacPackage._id
    generateOrgPaymentOrder("Tutor", purchasedEdumacPackageId, parseFloat(amount.toFixed(2)),"MONTHLY").then(response => {
      console.log(response.amount.toString())
      setPostData({
        appId : response.appId,
        orderId : response.id,
        orderAmount : response.amount.toString(),
        orderCurrency : response.currency,
        orderNote : "test",
        customerName : profile.tutorName,
        customerPhone : profile.mobileNo,
        customerEmail : profile.emailId,
        returnUrl : response.returnUrl,
        notifyUrl : response.notifyUrl
        // notifyUrl : "http://8c4caa1fdfaa.ngrok.io/paymenthooks/payorder/authenticate"
      })
      setPay(true)
    }).catch(err => {
      console.log(err)
    })
    
  }

  return (
    <div>
      <MiniDrawer>
      <PaymentNavbar />

      <Container maxWidth="lg">
        <Box>
          <FormControl className={styles.formControl}>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={feepages}
              onChange={handleChange}
            >
              <MenuItem value='pay'>Payment Desk</MenuItem>
              <MenuItem value='feestruct'>Fee Structure</MenuItem>
            </Select>
          </FormControl>
          <Grid container spacing={3} style={{backgroundColor: 'white', height: '49vH'}}>
            <Grid item xs={6} className={styles.divider}>
              <Box>
                <Grid container spacing={1}>
                  <Grid item sm={12}>
                    <div style={{ width: '100%', padding: '12px' }}>
                      <Box display="flex" justifyContent="center" marginTop="10%" textAlign="center">
                        <Box>
                          <Typography style={{color: '#606060'}}>Please Select a Payment Cycle</Typography>
                          <FormControl variant="outlined" style={{marginTop: '10px'}}>
                            <InputLabel>Plans</InputLabel>
                            <Select style={{minWidth: '150px'}}
                              value={activePlanIndex}
                              onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                                setActivePlanIndex(e.target.value as string)
                              }
                            >
                              <MenuItem value={''}>Select Payment Cycle</MenuItem>
                              {(paymentAcc && paymentAcc.purchasedEdumacPackage.paymentcycle) &&
                                <MenuItem value={paymentAcc.purchasedEdumacPackage.paymentcycle}>
                                  {paymentAcc.purchasedEdumacPackage.paymentcycle}
                                </MenuItem>
                              }
                            </Select>
                          </FormControl>
                        </Box>
                      </Box>
                    </div>    
                  </Grid>
                </Grid>
              </Box>
            </Grid>
            <Grid item xs={6}>
              <div style={{ width: '100%', padding: '12px' }}>
                <Box display="flex" justifyContent="center" marginTop="13%"> 
                  {
                    (!activePlanIndex || !paymentAcc) ? '' : 
                    <Box>
                      {
                        pay ? '' : <Button color="primary" variant="contained" size="large" onClick={clickToPay}>Pay Edumac</Button>
                      }
                      
                      {
                        <form ref={(el) => formRef.current=el} id="redirectForm" method="post" action="https://test.cashfree.com/billpay/checkout/post/submit">
                        {Object.keys(postData).map((el)=>{
                          return <input type="hidden" name={el} value={postData[el]} />
                        })}
                        <input type="hidden" name="signature" value={signature}/>
                        {/* <Button type="submit" color="primary" variant="contained" size="large" value="Pay">Confirm Pay to Edumac</Button> */}
                        {
                        !pay ? '' : <Button type="submit" color="primary" disabled={(dateFns.differenceInDays(new Date(paymentAcc.renewalDate), new Date()) > 3)} disableElevation variant="contained" size="large" value="Pay" >Confirm</Button>
                        }
                      </form>
                      }
                    </Box>
                  }
                </Box>
              </div>
            </Grid>
          </Grid>
        </Box>
      </Container>

      <Backdrop className={styles.backdrop} open={open}>
        <div style={{textAlign: 'center'}}>
          <CircularProgress color="inherit" />
          <Typography style={{fontSize: fontOptions.size.medium, marginTop: '5px'}}>Please wait while transaction takes place</Typography>
        </div>
      </Backdrop>
      </MiniDrawer>
    </div>
  )
}

function mapStateToProps(state: RootState) {
  return {

  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(AdminTutorPayToEdumac)
);
