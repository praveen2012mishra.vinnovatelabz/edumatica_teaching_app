import axios from "axios";
import { PaymentPackageInterfaceReq } from "../contracts/feeStructure_interface";
import { BUY_EDUMACPACKAGE_PAY_ORDER, COURSE_BUNDLE, GET_MASTER_PACKAGE, ORG_PAYMENT_ACCOUNT, ORG_PAY_ORDER, PAY_OFFLINE, PAY_ORDER, STUDENT_PAYMENT_ACCOUNT, USE_REFERRAL } from "./routes";

export const createCourseBundle = async (batchId: string, packageArr: PaymentPackageInterfaceReq[]) => {
  const response = await axios.post(COURSE_BUNDLE, {
    batchId,
    packageArr
  });
  return response.data;
};

export const fetchCourseBundle = async () => {
  const response = await axios.get(COURSE_BUNDLE)
  return response.data.courseBundleArr
}

export const fetchCourseBundlebyId = async(bundleId: string) => {
  const response = await axios.get(`${COURSE_BUNDLE}/${bundleId}`)
  return response.data.courseBundle
}

export const updateCourseBundle = async(bundleId: string, paymentPackageId: string, newfees: number, newdiscount: number) => {
  const response = await axios.patch(COURSE_BUNDLE, {
    bundleId,
    paymentPackageId,
    newfees,
    newdiscount,
  });
  return response.data;
}

export const deleteCourseBundle = async (bundleId: string) => {
  const response = await axios.delete(COURSE_BUNDLE, {params: {bundleId}})
  return response.data
}

export const fetchStudentPaymentAccount = async(studentList: string[]) => {
  const response = await axios.post(STUDENT_PAYMENT_ACCOUNT, {studentList})
  return response.data.studentAcc
}

export const generatePaymentOrder = async(amount: number, purchasedPackageId: string, batchId: string, paymentcycle:string) => {
  const response = await axios.post(PAY_ORDER, {
    purchasedPackageId,
    batchId,
    amount,
    paymentcycle
  });
  return response.data.payOrder
}

export const fetchOrgPaymentAccount = async() => {
  const response = await axios.get(ORG_PAYMENT_ACCOUNT)
  return response.data.paymentAcc
}

export const generateOrgPaymentOrder = async(accountType: string, purchasedEdumacPackageId: string, amount: number, paymentcycle:string) => {
  const response = await axios.post(ORG_PAY_ORDER, {
    accountType,
    purchasedEdumacPackageId,
    amount,
    paymentcycle
  });
  return response.data.payOrder
}

export const generateEdumacPackagePaymentOrder = async(accountType: string, purchasedEdumacPackageId: string, amount: number, paymentcycle:string, newPackagePlanId:string) => {
  const response = await axios.post(BUY_EDUMACPACKAGE_PAY_ORDER, {
    accountType,
    purchasedEdumacPackageId,
    amount,
    paymentcycle,
    newPackagePlanId
  });
  return response.data.payOrder
}

// export const authenticatePaymentOnSuccess = async(orderCreationId:string, razorpayPaymentId:string, razorpayOrderId:string, razorpaySignature:string) => {
//   const response = await axios.post(AUTHENTICATE_PAYMENT_ON_SUCCESS, {
//     orderCreationId,
//     razorpayPaymentId,
//     razorpayOrderId,
//     razorpaySignature,
//   });
//   return response.data
// }

export const registerReferralCode = async(accountType: string, referralCode: string) => {
  const response = await axios.get(USE_REFERRAL, {params: {accountType, referralCode}})
  return response.data
}

export const getMasterPackages = async() => {
  const response = await axios.get(GET_MASTER_PACKAGE)
  return response.data.masterPackages
}

export const payOffline = async(amount: number, purchasedCoursePackageId:string, paymentcycle: string) => {
  const response = await axios.post(PAY_OFFLINE, {
    amount,
    purchasedCoursePackageId,
    paymentcycle
  });
  return response.data
}