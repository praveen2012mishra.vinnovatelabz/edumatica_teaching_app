const BASE_ROUTE = process.env.REACT_APP_API;

export const COURSE_BUNDLE = BASE_ROUTE + '/payments/feestructure';
export const STUDENT_PAYMENT_ACCOUNT = BASE_ROUTE + '/payments/studentpaymentaccount';
export const PAY_ORDER = BASE_ROUTE + '/payments/payorder'
export const ORG_PAYMENT_ACCOUNT = BASE_ROUTE + '/payments/orgpaymentaccount'
export const ORG_PAY_ORDER = BASE_ROUTE + '/payments/orgpayorder'
export const USE_REFERRAL = BASE_ROUTE + '/payments/validateReferral'
export const GET_MASTER_PACKAGE = BASE_ROUTE + '/payments/getMasterPackages'
export const PAY_OFFLINE = BASE_ROUTE + '/payments/validateOfflinePayment'
export const BUY_EDUMACPACKAGE_PAY_ORDER = BASE_ROUTE + '/payments/purchaseEdumacPackagepayOrder'
// export const AUTHENTICATE_PAYMENT_ON_SUCCESS = BASE_ROUTE + '/paymenthooks/payorder/authenticate'