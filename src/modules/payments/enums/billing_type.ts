export enum BillingPlan {
  PREPAID = 'PREPAID',
  POSTPAID = 'POSTPAID',
}