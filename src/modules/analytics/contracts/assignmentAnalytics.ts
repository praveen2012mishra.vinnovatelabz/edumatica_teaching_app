import { BBBBatch } from "../../bbbconference/contracts/bbbBatch_interface";
import { AssignedStudent } from "../../common/contracts/assignment";

export interface AssignedAssignmentAnalytics {
  assignment: string,
  batchId: BBBBatch,
  groups: AssignedStudent[][],
  deadline: Date,
}