import React, { FunctionComponent, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { RootState } from '../../../store';
import {
  User,
  Tutor,
  Student,
  Parent,
  Organization,
  Admin
} from '../../common/contracts/user';
import {
  isAdminTutor,
  isStudent,
  isParent,
  isAdmin,
  isOrganization,
  isOrgTutor
} from '../../common/helpers';
import EdumacAdminAnalytics from '../components/layouts/edumacAdmin_analytics';
import OrgAnalytics from '../components/layouts/org_analytics';
import ParentAnalytics from '../components/layouts/parent_analytics';
import StudentPerformance from '../components/layouts/student_performance';
import TutorViewStudentPerformance from '../components/layouts/tutorViewStudent_performance';

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
}

const UserAnalytics: FunctionComponent<Props> = ({ authUser, match }) => {
  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo('/login');
    }
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if (isAdminTutor(authUser) || isOrgTutor(authUser)) {
    return <TutorViewStudentPerformance profile={authUser as Tutor} />;
  }
  if (isStudent(authUser)) {
    return <StudentPerformance profile={authUser as Student} />;
  }
  if(isParent(authUser)){
    return <ParentAnalytics profile={authUser as Parent} />;
  }
  if(isAdmin(authUser)){
    return <EdumacAdminAnalytics profile={authUser as Admin} />;
  }
  if(isOrganization(authUser)){
    return <OrgAnalytics profile={authUser as Organization} />;;
  }
  return (
    <Redirect to={'/login'} />
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
});

export default connect(mapStateToProps)(withRouter(UserAnalytics));
