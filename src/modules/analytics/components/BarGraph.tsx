import React, { FunctionComponent } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

// const data = [
//   {
//     name: "Page A",
//     myDataKey: 4000,
//     pv: 2400,
//     amt: 2400
//   },
//   {
//     name: "Page B",
//     myDataKey: 3000,
//     pv: 1398,
//     amt: 2210
//   },
//   {
//     name: "Page C",
//     myDataKey: 2000,
//     pv: 9800,
//     amt: 2290
//   },
//   {
//     name: "Page D",
//     myDataKey: 2780,
//     pv: 3908,
//     amt: 2000
//   },
//   {
//     name: "Page E",
//     myDataKey: 1890,
//     pv: 4800,
//     amt: 2181
//   },
//   {
//     name: "Page F",
//     myDataKey: 2390,
//     pv: 3800,
//     amt: 2500
//   },
//   {
//     name: "Page G",
//     myDataKey: 3490,
//     pv: 4300,
//     amt: 2100
//   }
// ];
// name is important in dataset as name will be on x-axis
// const dataKeys = {
//   myDataKey:"blue", pv:"red", amt:"green"
// }
// <BarGraph dataset={data} datakeys={dataKeys} width={700} height={500}/>

interface Props {
  dataset: any[];
  datakeys: any;
  width: number;
  height: number;
  right?: number;
  left?: number;
}

const BarGraph: FunctionComponent<Props> = ({dataset, datakeys, width, height, right = 30, left = 20}) => {

  const keys = Object.keys(datakeys)

  return (
    <BarChart
      width={width}
      height={height}
      data={dataset}
      margin={{
        top: 5,
        right: right,
        left: left,
        bottom: 5
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      {
        keys.map(key => {
          return (
            <Bar dataKey={key} fill={datakeys[key]} />
          )
        })
      }
    </BarChart>
  );
}

export default BarGraph
