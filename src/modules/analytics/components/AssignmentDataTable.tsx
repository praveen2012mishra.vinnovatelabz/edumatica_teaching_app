import React, { FunctionComponent } from 'react';
import {
  DataGrid,
  GridCellClassParams,
  GridColumns,
  GridToolbar,
} from '@material-ui/data-grid';
import { Box, Container, makeStyles } from '@material-ui/core';
import clsx from 'clsx';

interface RowData {
  id: number;
  studentName: string;
  status: string;
  marksObtained: number;
  feedback: string;
}

interface Props {
  gridData: RowData[];
}

const useStyles = makeStyles({
  root: {
    '& .super-app-theme--cell': {
      backgroundColor: 'rgba(224, 183, 60, 0.55)',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.negative': {
      backgroundColor: '#d47483',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.positive': {
      backgroundColor: 'rgba(157, 255, 118, 0.49)',
      color: '#1a3e72',
      fontWeight: '600'
    }
  },
  paginate:{
    display:"flex"
  }
});

export const AssignmentDataTable: FunctionComponent<Props> = ({
  gridData
  // answeredQuestions
}) => {
  const classes = useStyles();

  const columns: GridColumns = [
    { field: 'id', headerName: 'Sl.No', flex:0.75},
    { field: 'studentName', headerName: 'Student Name', flex:1},
    { field: 'status', headerName: 'Status', flex:1},
    { field: 'marksObtained', headerName: 'Marks Obtained', flex:1},
    { field: 'feedback', headerName: 'Feedback', flex:1.5}
  ];

  return (
    <React.Fragment>
      <Box
        className={classes.root}
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
        }}
      >
        <DataGrid
          loading={gridData.length === 0}
          pagination
          components={{
            Toolbar: GridToolbar
          }}
          autoPageSize
          rows={gridData}
          columns={columns}
        />
      </Box>
    </React.Fragment>
  );
};
