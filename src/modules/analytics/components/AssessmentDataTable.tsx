import React, { FunctionComponent } from 'react';
import {
  DataGrid,
  GridCellClassParams,
  GridColumns,
  GridToolbar,
} from '@material-ui/data-grid';
import { Box, Container, makeStyles } from '@material-ui/core';
import clsx from 'clsx';

interface RowData {
  id: string;
  answer: string;
  yourAnswer: string;
  result: number;
  marks: number;
  timeSpent: number;
  visits: number;
}

interface Props {
  gridData: RowData[];
}

const useStyles = makeStyles({
  root: {
    '& .super-app-theme--cell': {
      backgroundColor: 'rgba(224, 183, 60, 0.55)',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.negative': {
      backgroundColor: '#d47483',
      color: '#1a3e72',
      fontWeight: '600'
    },
    '& .super-app.positive': {
      backgroundColor: 'rgba(157, 255, 118, 0.49)',
      color: '#1a3e72',
      fontWeight: '600'
    }
  },
  paginate:{
    display:"flex"
  }
});

export const AssessmentDataTable: FunctionComponent<Props> = ({
  gridData
  // answeredQuestions
}) => {
  const classes = useStyles();

  const columns: GridColumns = [
    { field: 'id', headerName: 'Qns No.', flex:0.75},
    { field: 'answer', headerName: 'Correct Ans.', flex:1},
    {
      field: 'yourAnswer',
      headerName: 'Marked Ans.',
      type: 'string',
      flex:1
    },
    { field: 'result', headerName: 'Result', cellClassName: (params: GridCellClassParams) =>
    clsx('super-app', {
      negative: (params.value as string) === "Incorrect",
      positive: (params.value as string) === "Correct",
    }), flex:1},
    { field: 'marks', headerName: 'Marks Obtained', flex:1},
    { field: 'timeSpent', headerName: 'Time Spent (in sec)', flex:1},
    { field: 'visits', headerName: 'Revisits', flex:1}
  ];

  // const [rows,setRows] = useState<RowData[]>([])
  // console.log(rows)

  // {
  //     assessmentAnswers?.find((val,ind)=>ind===currentSection)?.answers.map((answer,index)=>{
  // return [(index+1).toString(),"Check",answer.join(","),]
  //     }).forEach((record,index)=>{
  //         console.log(record)
  //         return <TableRow key={index}>
  //             {record.map((data,ind)=>{
  //                 return <TableCell key={ind}>
  //                     {data}
  //                 </TableCell>
  //             })}
  //         </TableRow>
  //     })
  // }

  // useEffect(()=>{
  //     setRows(()=>{
  //         return gridData?.map((question,index)=>{
  //             return {id:question.id, answer:question.answer, yourAnswer:question.yourAnswer, result:question.result,marks:question.marks,timeSpent:question.timeSpent, visits:(question.visits-1)} as RowData
  //         }) as RowData[]
  //     })
  // },[gridData])

  return (
    <React.Fragment>
      <Box
        className={classes.root}
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
        }}
      >
        <DataGrid
          loading={gridData.length === 0}
          pagination
          components={{
            Toolbar: GridToolbar
          }}
          autoPageSize
          rows={gridData}
          columns={columns}
        />
      </Box>
    </React.Fragment>
  );
};
