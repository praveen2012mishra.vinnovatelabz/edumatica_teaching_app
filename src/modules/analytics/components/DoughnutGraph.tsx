import React, { FunctionComponent } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip } from 'recharts';

interface Props {
  dataset: any[];
  COLORS: string[];
  size: number;
}

const DoughnutGraph:FunctionComponent<Props> = ({dataset, COLORS, size}) => {
  const CustomTooltip = ({ active, payload, label }: any) => {
    if (active && payload && payload.length && payload[0].name !== 'TRANSPARENT') {
      return (
        <div className="custom-tooltip">
          <p className="label">{`${payload[0].name} : ${Number(payload[0].value).toFixed(2)} %`}</p>
        </div>
      );
    }
  
    return null;
  };

  return (
    <PieChart width={size} height={size}>
      <Pie
        data={dataset}
        cx={((size/2) + 25)}
        cy={size/2}
        innerRadius={size/4}
        outerRadius={((size/4) + 15)}
        fill="#8884d8"
        paddingAngle={2}
        dataKey="value"
        startAngle={90}
        endAngle={-270}
      >
        {dataset.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
      </Pie>
      <Tooltip content={<CustomTooltip />} />
    </PieChart>
  );
}

export default DoughnutGraph
