import { AppBar, Box, Button, Container, CssBaseline, Divider, Drawer, FormControl, FormHelperText, Grid, InputLabel, List, ListItem, ListItemIcon, ListItemText, makeStyles, MenuItem, Select, Switch, Theme, Toolbar, Typography } from '@material-ui/core';
import { ChevronRight } from '@material-ui/icons';
import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { RootState } from '../../../../store';
import { Assessment } from '../../../assessment/contracts/assessment_interface';
import Minidrawer from '../../../common/components/sidedrawer';
import { Student, Tutor } from '../../../common/contracts/user';
import { exceptionTracker, isAdminTutor, isOrgTutor } from '../../../common/helpers';
import { AssessmentAnswers } from '../../../student_assessment/contracts/assessment_answers';
import { OngoingAssessment } from '../../../student_assessment/contracts/assessment_interface';
import {
  attemptedAssessmentAnswers,
  //attemptedAssessmentData,
  getAssignedAssessments,
  getAttemptAssessmentbyAssessmentId
} from '../../helpers/api';
import { AssessmentDataTable } from '../AssessmentDataTable';
//import BarGraph from '../BarGraph';
import LineGraph from '../LineGraph';
import PieGraph from '../PieGraph';
//import StackedBarGraph from '../StackedBarGraph';
import Navbar from '../../../common//components/navbar';
import AnalyticsIcon from '../../../../assets/images/analytics_icon.png';
import { getAssignedAssignments, feedbackAssignmentsAnalytics } from '../../../common/api/assignment';
import { fetchBBBBatches, getPostIndividualMeetingEventsInfo, getPostMeetingEventsInfo } from '../../../bbbconference/helper/api';
import { BatchStudent, BBBBatch } from '../../../bbbconference/contracts/bbbBatch_interface';
import { Meeting } from '../../../bbbconference/contracts/meeting_interface';
import { BBBEvents } from '../../../bbbconference/contracts/bbbevent_interface';
import { BBBAttendance } from '../../../bbbconference/contracts/bbbAttendance_interface';
import { AttendanceOption } from '../../../bbbconference/enums/attendance_options';
import { AssignedAssignmentRes, AssignedBatch } from '../../../common/contracts/assignment';
import { StudentAttendanceDataTable } from '../../../bbbconference/components/StudentAttendanceDataTable';
import BarGraph from '../BarGraph';
import { AssignedAssignmentAnalytics } from '../../contracts/assignmentAnalytics';
import { AssignmentDataTable } from '../AssignmentDataTable';
import randomColor from 'randomcolor'
import { fetchOrgPaymentAccount, fetchStudentPaymentAccount } from '../../../payments/helper/api';
import { OrganizationPaymentAccount } from '../../../payments/contracts/orgPaymentAccount_interface';
import { getStudentsOfTutor } from '../../../common/api/tutor';
import { Transaction } from '../../../payments/contracts/transaction_interface';
import * as dateFns from "date-fns"


const drawerWidth = 240;

const styles =  makeStyles((theme: Theme) =>
createStyles({
  topbox: {
    // border: '1px solid lightgray',
    margin: '8px 0',
    width: '100%',
    height: '420px',
    backgroundColor: '#fff',
    // borderRadius: '15px',
    padding: '12px',
    fontWeight: 500
  },
  databox: {
    // border: '1px solid lightgray',
    margin: '8px 0',
    width: '100%',
    // height: '320px',
    backgroundColor: '#fff',
    // borderRadius: '15px',
    // padding: '12px',
    fontWeight: 500
  },
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    zIndex: 0
  },
  contain: {
    position: 'fixed',
    left: drawerWidth + theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      left: drawerWidth + theme.spacing(9) + 1,
    },
    padding: '0px 20px'
  },
  drawerPaper: {
    width: drawerWidth,
    marginLeft: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(9) + 1,
    },
    zIndex: 0
  },
  drawerContainer: {
    overflow: 'auto',
    zIndex: 0
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}),
);

interface AssessmentTableData {
  id: string;
  answer: string;
  yourAnswer: string;
  result: number;
  marks: number;
  timeSpent: number;
  visits: number;
}

interface PieData {
  name: string;
  value: number;
}

interface AssessmentTimeTakenLineGraphData {
  name: string;
  timeSpent: number;
  revisits: number;
}

interface AssignmentTableData {
  id: number;
  studentName: string;
  status: string;
  marksObtained: number;
  feedback: string;
}

interface Props {
  profile: Tutor;
  //urlDirect: String;
}

const TutorViewStudentPerformance: FunctionComponent<Props> = ({
  profile
}) => {
  const classes = styles();
  const [redirectTo, setRedirectTo] = useState('');
  
  // Assessment
  const [viewAssessment, setViewAssessment] = useState<boolean>(true)
  const [activeAssessmentIndex, setActiveAssessmentIndex] = useState(-1)
  const [activeAttemptAssessmentStudentIndex, setActiveAttemptAssessmentStudentIndex] = useState(-1)
  const [allAssessments, setAllAssessments] = useState<Assessment[] | null>(null)
  const [attemptAssessmentData, setAttemptAssessmentData] = useState<
    any[] | null
  >(null);
  const [
    attemptAssessment,
    setAttemptAssessment
  ] = useState<OngoingAssessment | null>(null);

  const [gridData, setGridData] = useState<AssessmentTableData[] | null>(null);
  // const [
  //   assessmentTakenPieChartData,
  //   setAssessmentTakenPieChartData
  // ] = useState<PieData[] | null>(null);
  const [
    assessmentTimeTakenLineGraphData,
    setAssessmentTimeTakenLineGraphData
  ] = useState<AssessmentTimeTakenLineGraphData[] | null>(null);

  const [marksObtained, setMarksObtained] = useState<number>(0);
  const [pieGraphMarksData, setPieGraphMarksData] = useState<any[]>([])
  const [totalMarks, setTotalMarks] = useState<number>(0)

  
  // Assignment
  const [viewAssignment, setViewAssignment] = useState<boolean>(false)
  const [allAssignments, setAllAssignments] = useState<AssignedAssignmentRes[] | null>(null)
  const [activeAssignmentIndex, setActiveAssignmentIndex] = useState(-1)
  const [activeAssignment, setActiveAssignment] = useState<AssignedAssignmentRes | null>(null)
  const [assignedBatches, setAssignedBatches] = useState<AssignedAssignmentAnalytics[] | null>(null)
  const [activeAssignedBatchIndex, setActiveAssignedBatchIndex] = useState(-1)
  const [gridAssignmnetData, setGridAssignmnetData] = useState<AssignmentTableData[] | null>(null)
  const [assignmentBarGraphData, setAssignmentBarGraphData] = useState<any[]>([]);
  const [assignmentBarGraphDataKey, setAssignmentBarGraphDataKey] = useState<any | null>(null)


  // Attendance
  const [viewAttendance, setViewAttendance] = useState<boolean>(false)
  const [batches, setBatches] = useState<BBBBatch[]>([]);
  const [activeBatchIndex, setActiveBatchIndex] = useState(-1)
  const [activeBatch, setActiveBatch] = useState<BBBBatch | null>(null)
  let [batchMeetings, setBatchMeetings] = useState<Meeting[] | null>(null);
  const [activeMeetingIndex, setActiveMeetingIndex] = useState(-1)
  const [activeClass, setActiveClass] = useState<Meeting | null>(null)
  let [individualMeetingInfo, setIndividualMeetingInfo] = useState<Meeting | null>(null);
  let [attandanceChart, setAttandanceChart] = useState<BBBAttendance[] | null>(null);
  const [barGraphData, setBarGraphData] = useState<any[]>([]);
  const [barGraphDataKey, setBarGraphDataKey] = useState<any | null>(null);


  // Payments
  const [viewPayments, setViewPayments] = useState<boolean>(false);
  const [studentList, setStudentList] = useState<Student[] | null>(null);
  const [orgPaymentAccount, setOrgPaymentAccount] = useState<OrganizationPaymentAccount | null>(null);
  const [paymentBatches, setPaymentBatches] = useState<BBBBatch[]>([]);
  const [activePaymentBatchIndex, setActivePaymentBatchIndex] = useState(-1);
  const [allTransactionBarData, setAllTransactionBarData] = useState<any[] | null>(null);
  const [dataKeyOne, setDataKeyOne] = useState<any | null>(null);
  const [transactionBatchView, setTransactionBatchView] = useState<boolean>(false)

  function createAssignmentData(
    id: number,
    studentName: string,
    status: string,
    marksObtained: number,
    feedback: string,
  ) {
    return { id, studentName, status, marksObtained, feedback};
  }

  const COLORS = [randomColor(), randomColor()];

  function createData(
    id: string,
    answer: string,
    yourAnswer: string,
    result: number,
    marks: number,
    timeSpent: number,
    visits: number
  ) {
    return { id, answer, yourAnswer, result, marks, timeSpent, visits };
  }

  function createLineData(name: string, timeSpent: number, revisits: number) {
    return { name, timeSpent, revisits };
  }
  
  const lineGraphDataKeys = {
    timeSpent: randomColor(),
    revisits: randomColor()
  };

  useEffect(() => {
    (async () => {
      try {
        if (!profile._id) return;
        const allAssignedAssessments = await getAssignedAssessments();
        const batchesListResponse = await fetchBBBBatches();
        // const allAssignedAssignments = await getAssignedAssignments({tutorId:undefined});
        const paymentAccountRes = await fetchOrgPaymentAccount();
        const studentListRes = await getStudentsOfTutor();
        // const [assignedAssessments, batchesList, assignmentList, accountDetails, students] = await Promise.all([
        //   allAssignedAssessments, batchesListResponse, allAssignedAssignments, paymentAccountRes, studentListRes
        // ]);
        const [assignedAssessments, batchesList, accountDetails, students] = await Promise.all([
          allAssignedAssessments, batchesListResponse, paymentAccountRes, studentListRes
        ]);
        if(isAdminTutor(profile)) {
          setBatches(batchesList);
          setPaymentBatches(batchesList);
        }
        if(isOrgTutor(profile)) {
          const sortedbatches = batchesList.filter(function (batch:BBBBatch) {
            return profile._id === batch.tutorId;
          });
          setBatches(sortedbatches)
          setPaymentBatches(sortedbatches)
        }
        setOrgPaymentAccount(accountDetails)
        // setAllAssignments(assignmentList)
        setAllAssessments(assignedAssessments)
        // @ts-ignore
        setStudentList(students.studentList)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [profile.mobileNo]);


// Assessment useEffect
  useEffect(() => {
    if(allAssessments && activeAssessmentIndex>=0) {
      getStudentAssessments(allAssessments[activeAssessmentIndex]._id)
      setTotalMarks(allAssessments[activeAssessmentIndex].totalMarks)
    }
  }, [activeAssessmentIndex])

  useEffect(() => {
    if(attemptAssessmentData && activeAttemptAssessmentStudentIndex>=0) {
      getAnswers(
        attemptAssessmentData[activeAttemptAssessmentStudentIndex]._id,
        attemptAssessmentData[activeAttemptAssessmentStudentIndex]
      );
      setAttemptAssessment(attemptAssessmentData[activeAttemptAssessmentStudentIndex]);
    }
  }, [activeAttemptAssessmentStudentIndex])

// Attendance useEffect
  useEffect(() => {
    if(!batches || activeBatchIndex<0) return
    setActiveBatch(batches[activeBatchIndex])
    const currentBatch:BBBBatch = batches[activeBatchIndex]
    if(currentBatch !== undefined && currentBatch._id !== undefined) {
      getPostMeetingEventsInfo(currentBatch._id).then((result) => {
        if (result.message === 'Success') setBatchMeetings(result.data);
      });
    }
  }, [activeBatchIndex])

  useEffect(() => {
    if(!batchMeetings || activeMeetingIndex<0) return
    setActiveClass(batchMeetings[activeMeetingIndex])
    const currentClass = batchMeetings[activeMeetingIndex]
    if(currentClass && currentClass.internalMeetingID) {
      getPostIndividualMeetingEventsInfo(currentClass.internalMeetingID).then((result) => {
        if (result.message === 'Success') {
          setIndividualMeetingInfo(result.data);
          createTable(result.data);
        }
      });
    }
  }, [activeMeetingIndex])

// Assignment useEffect
  useEffect(() => {
    if(!allAssignments || activeAssignmentIndex<0) return
      setActiveAssignment(allAssignments[activeAssignmentIndex])
      feedbackAssignmentsAnalytics(allAssignments[activeAssignmentIndex].assignmentId).then((value) => {
        setAssignedBatches(value)
      })
  }, [activeAssignmentIndex])

  useEffect(() => {
    if(!assignedBatches || activeAssignedBatchIndex<0) return
    function graphData(
      name: string,
      totalmarks: number,
    ) {
      return {name, totalmarks};
    }
    var barData: any[] = []
      const currentStudents = assignedBatches[activeAssignedBatchIndex].batchId.students
      const currentgroups = assignedBatches[activeAssignedBatchIndex].groups
      let gridData:AssignmentTableData[] = []
      currentgroups.forEach(el => {
        const currentStudentName = currentStudents.filter(ele => {
          return ele._id === el[0].studentId
        })
        gridData.push(
          createAssignmentData(
            // @ts-ignore
            currentgroups.indexOf(el)+1,
            currentStudentName[0].studentName,
            el[0].assignmentStatus,
            // @ts-ignore
            el[0].marks,
            el[0].feedback
          )
        )
        if(el[0].assignmentStatus === "Evaluated") {
          barData.push(
            // @ts-ignore
            graphData(currentStudentName[0].studentName, parseInt(el[0].marks))
          )
        }else {
          barData.push(
            // @ts-ignore
            graphData(currentStudentName[0].studentName, 0)
          )
        }
      })
      const dataKeys = {
        totalmarks:randomColor()
      }
      setAssignmentBarGraphData(barData)
      setAssignmentBarGraphDataKey(dataKeys)
      setGridAssignmnetData(gridData)
  }, [activeAssignedBatchIndex])


  // Payments useEffect

  useEffect(() => {
    function graphData(
      name: string,
      earning: number
    ) {
      return {name, earning};
    }
    var barData: any[] = []
    const dataKeys = {
      earning:randomColor()
    }
    var monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
    var monthlySpread = [0,0,0,0,0,0,0,0,0,0,0,0]
    let studentIdArr:string[] = [];
    studentList?.map(el => {
      if(!el._id) return
      studentIdArr.push(el._id)
    })
    fetchStudentPaymentAccount(studentIdArr).then(response => {
      // console.log(response)
      // @ts-ignore
      response.map(ele => {
        // @ts-ignore
        ele.purchasedPackage.map(each => {
          // @ts-ignore 
          each.paymentTransactions.map(elem => {
            const month = dateFns.getMonth(elem.created_at)
            monthlySpread[month-1] = monthlySpread[month-1] + elem.netAmountToBene
            // console.log(elem,month,monthlySpread)
          })
        })
      })
      monthlySpread.map(el => {
        barData.push(
          graphData(monthName[monthlySpread.indexOf(el)],el)
        )
      })
      setDataKeyOne(dataKeys)
      setAllTransactionBarData(barData)
    })
  }, [studentList])


  useEffect(() => {
    function graphData(
      name: string,
      earning: number
    ) {
      return {name, earning};
    }
    var barData: any[] = []
    var monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
    var monthlySpread = [0,0,0,0,0,0,0,0,0,0,0,0]

    const activeBatch = paymentBatches[activePaymentBatchIndex]
    // console.log(activeBatch)
    if(activeBatch === undefined) return
    const studentList = activeBatch.students.map(ele => {
      return ele._id
    })
    fetchStudentPaymentAccount(studentList).then(response => {
      // @ts-ignore
      response.map(ele => {
        // @ts-ignore
        ele.purchasedPackage.map(each => {
          // @ts-ignore 
          each.paymentTransactions.map(elem => {
            const month = dateFns.getMonth(elem.created_at)
            monthlySpread[month-1] = monthlySpread[month-1] + elem.netAmountToBene
            // console.log(elem,month,monthlySpread)
          })
        })
      })
      monthlySpread.map(el => {
        barData.push(
          graphData(monthName[monthlySpread.indexOf(el)],el)
        )
      })
      // setAllTransactionBarData(barData)
    }).catch(err => {

    })
  }, [activePaymentBatchIndex])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }


  // Assessment
  const getStudentAssessments = (assessmentID:string) => {
    getAttemptAssessmentbyAssessmentId(assessmentID).then(response => {
      setAttemptAssessmentData(response)
    })
  }

  const getAnswers = (
    assessmentId: string,
    eachAssessment: any
  ) => {
    if (!profile._id) return;
    attemptedAssessmentAnswers(eachAssessment.studentId._id, assessmentId)
      .then((result) => {
        let assessmentAnswers: AssessmentAnswers = result;
        var tableData: AssessmentTableData[] = [];
        var serialNo = 0;
        assessmentAnswers.forEach((answer, sectionIndex) => {
          // console.log(sectionIndex,answer)
          // console.log(sectionIndex,eachAssessment)
          answer.forEach((eachAnswer, index) => {
            serialNo++;
            tableData.push(
              createData(
                serialNo.toString(),
                // eachAnswer.index,
                eachAnswer.correct,
                eachAnswer.your,
                eachAnswer.result,
                eachAnswer.marks,
                eachAssessment.assessmentData.sectionsData[sectionIndex]
                  .questions[index].timeSpent,
                eachAssessment.assessmentData.sectionsData[sectionIndex]
                  .questions[index].visits
              )
            );
          });
        });
        // console.log(tableData)
        setGridData(tableData);
        serialNo = 0;
        var lineData: AssessmentTimeTakenLineGraphData[] = [];
        tableData.forEach((data) => {
          lineData.push(
            createLineData(data.id, (data.timeSpent / 60), data.visits)
          );
        });
        setAssessmentTimeTakenLineGraphData(lineData);
        var sum = tableData.reduce(function (prev, cur) {
          return prev + cur.marks;
        }, 0);
        setMarksObtained(sum);
        const pieMarksData = [
          { name: 'Marks Obtained', value: sum },
          {
            name: 'Marks Lost',
            value: (totalMarks) - sum
          }
        ];
        setPieGraphMarksData(pieMarksData)
      })
      .catch((err) => {
        console.log(err);
      });
  };


  // Attendance
  const createTable = (individualMeetingInfo: Meeting | undefined) => {
    if (individualMeetingInfo === undefined) return;
    const events: BBBEvents[] = individualMeetingInfo.events;
    function createData(
      userID: string | undefined,
      userName: string | undefined,
      entryTime: string | Date,
      attended: string,
      ispresent: string
    ) {
      return {userID, userName, entryTime, attended, ispresent };
    }
    var rows: BBBAttendance[] = []
    // var defaulters: any [] = []
    const meetingEndEvent = events.filter(function (items) {
      return items.type === 'meeting-ended';
    });
    //console.log(meetingEndEvent)
    var totalMeetingTime =
      meetingEndEvent[0].eventTriggerTime - individualMeetingInfo.createTime;

    const students_array:BatchStudent[] | undefined = (batches.find(batch => batch._id === individualMeetingInfo.meetingID))?.students
    // console.log(students_array)
    // const tutorID = (batches.find(batch => batch._id === individualMeetingInfo.meetingID))?.tutorId
    // console.log(batches.find(batch => batch._id === individualMeetingInfo.meetingID))

    students_array?.map(student => {
      let total_time = 0
      // eslint-disable-next-line
      const userEvent = events.filter(function (item) {
        if(item.userID === student._id) {
          return item.type === 'user-joined' || item.type === 'user-left';
        }
      });

      // console.log('',userEvent,'\n',userEvent.length)

      const length = userEvent.length
      if(length === 0){
        rows.push(createData(student._id , student.studentName, '-', '0', AttendanceOption.IS_ABSENT))
        return
      }

      // userEvent.map(eachEvent => {
      //   if(eachEvent.type === "user-joined"){
      //     if(eachEvent.userID === undefined) return
      //     students_array.map(student => {
      //       if(student !== eachEvent.userID) {
      //         rows.push(createData(student, '-', '-', '-', AttendanceOption.IS_ABSENT))
      //       }
      //     })
      //   }
      // })
      //console.log(userEvent)
      
      if((length % 2) !== 0) {
        for (let i=0; i<userEvent.length - 1; i+=2){
          const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
          total_time = total_time + time
        }
        total_time = total_time + (meetingEndEvent[0].eventTriggerTime - userEvent[length-1].eventTriggerTime)
        const percentage = (total_time/totalMeetingTime)*100
        var ispresent = AttendanceOption.IS_ABSENT;
        if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
        rows.push(
          createData(userEvent[0].userID, userEvent[0]?.name, (new Date(userEvent[0].eventTriggerTime)).toLocaleTimeString(), percentage.toFixed(2), ispresent)
        )
      }
      if((length % 2) === 0) {
        for (let i=0; i<userEvent.length; i+=2){
          const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
          total_time = total_time + time
        }
        const percentage = (total_time/totalMeetingTime)*100;
        // eslint-disable-next-line
        var ispresent = AttendanceOption.IS_ABSENT;
        if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
        rows.push(
          createData(userEvent[0].userID, userEvent[0]?.name, (new Date(userEvent[0].eventTriggerTime)).toLocaleTimeString(), percentage.toFixed(2), ispresent)
        )
      }
    })
    // students_array?.filter(function(student){

    // })
    setAttandanceChart(rows);
    // console.log(rows)

    function graphData(
      name: string,
      attendance: number
    ) {
      return {name, attendance};
    }
    var barData: any[] = []
    rows.forEach(row => {
      if(!row.userName) return
      const totalTime:number = parseInt(row.attended);
      barData.push(graphData(row.userName, totalTime));
    })
    const dataKeys = {
      attendance:randomColor()
    }
    setBarGraphDataKey(dataKeys)
    setBarGraphData(barData)
  };


  // Payments


  return (
    <div>
      <Minidrawer>
      <div style={{ width: '100%' , minHeight: '100vH' }}>
      <Box display="flex" flexDirection="row" minHeight= '100vH'>
      <Box>
      <div
        style={{position: 'sticky', top:0 , left: 100, width: drawerWidth, height: '100%', zIndex: 1}}
      >
        <div style={{height:"100%",backgroundColor:"#4C8BF5", marginTop:"2px"}} className={classes.drawerContainer}>
          {/* <AnalyticsIcon/> */}
            <Box marginTop="40px" padding="50px 10px" display="flex" flexDirection="column" alignItems="center" gridRowGap="15px">
              <img style={{marginBottom:"40px"}} height="80px" src={AnalyticsIcon} alt=""/>
              <Button style={{color:viewAssessment === true ? "aquamarine" : "#fff"}}  disableElevation variant="text" onClick={() => {
                setViewAssessment(true)
                setViewAttendance(false)
                setViewAssignment(false)
                setViewPayments(false)
              }}>Performance Analytics</Button>
              <Button style={{color:viewAttendance === true ? "aquamarine" : "#fff"}} disableElevation variant="text" onClick={() => {
                setViewAttendance(true)
                setViewAssignment(false)
                setViewAssessment(false)
                setViewPayments(false)
              }}>Attendance Analytics</Button>
              <Button style={{color:viewAssignment === true ? "aquamarine" : "#fff"}} disableElevation variant="text" onClick={() => {
                setViewAssignment(true)
                setViewAssessment(false)
                setViewAttendance(false)
                setViewPayments(false)
              }}>Assignment Analytics</Button>
              {
                isAdminTutor(profile) !== true ? '' :
                <Button style={{color:viewPayments === true ? "aquamarine" : "#fff"}} disableElevation variant="text" onClick={() => {
                  setViewPayments(true)
                  setViewAssignment(false)
                  setViewAssessment(false)
                  setViewAttendance(false)
                }}>Payments Analytics</Button>
              }
            </Box>
        </div>
      </div>
      </Box>
      <Box paddingLeft="30px">
        <Box>
          <h1 style={{fontSize:"2.5rem",color:"#4C8BF5"}}>Edumatica Report</h1>

          {
            viewAssessment !== true ? '' :
            <Box>
              <h2>Assesment Evalution Report</h2>
              <Box marginBottom="30px" display="flex" alignItems="center" gridColumnGap="30px">
                {
                  !allAssessments ? '' :
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Assessment</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeAssessmentIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveAssessmentIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Assessment</MenuItem>
                        {allAssessments.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.assessmentname} - {each.boardname} {each.classname} {each.subjectname}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected assessment</FormHelperText>
                    </FormControl>
                  </Box>
                }
                {
                  !attemptAssessmentData ? '' : 
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Assigned Students</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeAttemptAssessmentStudentIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveAttemptAssessmentStudentIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Assessment</MenuItem>
                        {attemptAssessmentData.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.studentId.studentName}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected student</FormHelperText>
                    </FormControl>
                  </Box>
                }
              </Box>

              {!gridData ? (
                ''
              ) : (
                <Box height="600px !important" className={classes.databox}>
                  <AssessmentDataTable gridData={gridData} />
                </Box>
              )}

            <Grid container spacing={3}>
            <Grid item lg={3} md={12} xs={12}>
                {!attemptAssessment ? (
                  ''
                ) : (
                  <Box width="100%" height="420px" display="flex" flexDirection="column" justifyContent="center" alignItems="center" gridRowGap="20px" padding="10px">
                    
                    <Box display="flex" justifyContent="center" alignItems="center" bgcolor="#fff" padding="10px" width="100%" height="100%" borderRadius="10px">
                      <PieGraph
                          dataset={pieGraphMarksData}
                          COLORS={COLORS}
                          size={300}
                        />
                    </Box>
                    {
                      !attemptAssessment.assessmentData ? '' : 
                    
                    <Box bgcolor="#fff" padding="10px" width="100%" height="100%" borderRadius="10px">
                        {marksObtained} marks obtained out of{' '}
                            {totalMarks}
                        <br/>
                        Student went out of test window{' '}{attemptAssessment.assessmentData.unfocussed} times
                        <br/>
                        Student spent{' '}{(attemptAssessment.assessmentData.unfocussedTime / 60).toFixed(2)}min out of test window
                    </Box>
                    }
                  </Box>
                )}
              </Grid>
              <Grid item lg={9} md={12} xs={12}>
                {!assessmentTimeTakenLineGraphData ? (
                  ''
                ) : (
                  <Box
                    borderRadius="10px"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    className={classes.topbox}
                  >
                    <Box marginLeft="-25px" marginBottom="-18px">
                      <LineGraph
                        dataset={assessmentTimeTakenLineGraphData}
                        datakeys={lineGraphDataKeys}
                        width={1200}
                        height={400}
                      />
                    </Box>
                  </Box>
                )}
              </Grid>
            </Grid>

            </Box>
          }


{/* Attendance */}
          {
            viewAttendance !== true ? '' :
            <Box>
              <h2>Attendence Report</h2>
              <Box marginBottom="30px" display="flex" alignItems="center" gridColumnGap="30px">
                {
                  !batches ? '' :
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Attendance</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeBatchIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveBatchIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Batch</MenuItem>
                        {batches.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.batchfriendlyname} - {each.boardname} {each.classname} {each.subjectname}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected batches</FormHelperText>
                    </FormControl>
                  </Box>
                }
                {
                  !batchMeetings ? '' : 
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Previous Classes</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeMeetingIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                        setActiveMeetingIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Class</MenuItem>
                        {batchMeetings.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.createDate}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected class</FormHelperText>
                    </FormControl>
                  </Box>
                }
              </Box>
              {
                !attandanceChart ? '' :
                <Box bgcolor="#fff">
                  <StudentAttendanceDataTable gridData={attandanceChart} />
                </Box>
              }

              {
                !barGraphDataKey ? '' :  
                <Box marginTop="18px" width="100%" height="520px" bgcolor="#fff" paddingTop="14px" paddingRight="30px !important" display="flex" alignItems="center" justifyContent="center">
                  <BarGraph dataset={barGraphData} datakeys={barGraphDataKey} width={1500} height={520}/>
                </Box>
              }

            </Box>
          }

          
{/* Assignment */}
          {
            viewAssignment !== true ? '' :
            <Box>
              <h2>Assignment Evaluation Report</h2>
              <Box marginBottom="30px" display="flex" alignItems="center" gridColumnGap="30px">

                {
                  !allAssignments ? '' :
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Assignments</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeAssignmentIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveAssignmentIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Assignment</MenuItem>
                        {allAssignments.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.assignmentname} - {each.boardname} {each.classname} {each.subjectname}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected assignments</FormHelperText>
                    </FormControl>
                  </Box>
                }
                {
                  !assignedBatches ? '' :
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Batches</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeAssignedBatchIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveAssignedBatchIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Batch</MenuItem>
                        {assignedBatches.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.batchId.batchfriendlyname} - {each.deadline}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected batch</FormHelperText>
                    </FormControl>
                  </Box>
                }
                
              </Box>
              {
                !gridAssignmnetData ? '' :
                <Box height="420px" bgcolor="#fff">
                  <AssignmentDataTable gridData={gridAssignmnetData} />
                </Box>
              }

              {
                !assignmentBarGraphDataKey ? '' :  
                <Box marginTop="18px" width="100%" height="520px" bgcolor="#fff" paddingTop="14px" paddingRight="30px !important" display="flex" alignItems="center" justifyContent="center">
                  <BarGraph dataset={assignmentBarGraphData} datakeys={assignmentBarGraphDataKey} width={1500} height={520}/>
                </Box>
              }

            </Box>
          }

{/* Payments */}
          {
            viewPayments !== true ? '' : isAdminTutor(profile) !== true ? '' :
            <Box>
              <Box display="flex" width="100%" alignItems="center" justifyContent="space-between">
                <h2>Payments related Report</h2>
                <Box display="flex" alignItems="center">
                  <p>View Batchwise</p>
                  <Switch
                    checked={transactionBatchView}
                    onChange={() => setTransactionBatchView(!transactionBatchView)}
                  />
                </Box>
              </Box>
              {
                transactionBatchView === false ? 
                <>
                  {
                    !allTransactionBarData ? '' :
                    <Box marginTop="18px" width="100%" height="520px" bgcolor="#fff" paddingTop="14px" paddingRight="30px !important" display="flex" alignItems="center" justifyContent="center">
                      <BarGraph dataset={allTransactionBarData} datakeys={dataKeyOne} width={1500} height={520}/>
                    </Box>
                  }
                </>
                :
                <>
                  <Box marginBottom="30px" display="flex" alignItems="center" gridColumnGap="30px">
                    {
                      !paymentBatches ? '' :
                      <Box padding="0 8px" bgcolor="#fff">
                        <FormControl className={classes.formControl}>
                          <InputLabel id="demo-simple-select-autowidth-label">Payments</InputLabel>
                          <Select
                            style={{minWidth:"100px"}}
                            value={activePaymentBatchIndex}
                            onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                              setActivePaymentBatchIndex(e.target.value as number)
                            }
                          >
                            <MenuItem value="-1">Select Batch</MenuItem>
                            {paymentBatches.map((each, index) => (
                              <MenuItem key={index} value={index}>
                                {each.batchfriendlyname} - {each.boardname} {each.classname} {each.subjectname}
                              </MenuItem>
                            ))}
                          </Select>
                          <FormHelperText>View Analytics of selected batches</FormHelperText>
                        </FormControl>
                      </Box>
                    }
                  </Box>
                </>
              }
            </Box>
          }

        </Box>
      </Box>
      </Box>
      </div>
      </Minidrawer>
    </div>
  );
};

function mapStateToProps(state: RootState) {
  return {};
}

export default (
  connect(mapStateToProps)(TutorViewStudentPerformance)
);
