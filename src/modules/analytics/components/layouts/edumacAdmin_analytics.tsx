import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import React, { FunctionComponent } from 'react'
import { connect } from 'react-redux';
import { RootState } from '../../../../store';
import { Admin } from '../../../common/contracts/user';

const styles = createStyles({

});

interface Props extends WithStyles<typeof styles> {
  profile: Admin;
  //urlDirect: String;
}

const EdumacAdminAnalytics: FunctionComponent<Props> = ({classes ,profile}) => {
  return (
      <h1>Analytics Admin</h1>
  )
}

function mapStateToProps(state: RootState) {
  return {
  
  };
}

export default withStyles(styles)(
  connect(mapStateToProps)(EdumacAdminAnalytics)
);