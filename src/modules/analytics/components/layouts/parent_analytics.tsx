import { AppBar, Box, Button, CssBaseline, Drawer, FormControl, FormHelperText, Grid, InputLabel, makeStyles, MenuItem, Select, Theme, Toolbar } from '@material-ui/core';
import { createStyles, withStyles, WithStyles } from '@material-ui/styles';
import React, { FunctionComponent, useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { RootState } from '../../../../store';
import { Children, Parent } from '../../../common/contracts/user';
import Navbar from '../../../common//components/navbar';
import AnalyticsIcon from '../../../../assets/images/analytics_icon.png';
import { OngoingAssessment } from '../../../student_assessment/contracts/assessment_interface';
import randomColor from 'randomcolor';
import { Batch } from '../../../academics/contracts/batch';
import { attemptedAssessmentAnswers, attemptedAssessmentData } from '../../helpers/api';
import Minidrawer from '../../../common/components/sidedrawer'
import { fetchBatchesList } from '../../../common/api/academics';
import { exceptionTracker } from '../../../common/helpers';
import { Meeting } from '../../../bbbconference/contracts/meeting_interface';
import { fetchBBBBatches, getPostIndividualMeetingEventsInfo, getPostMeetingEventsInfo } from '../../../bbbconference/helper/api';
import { AttendanceOption } from '../../../bbbconference/enums/attendance_options';
import { BBBEvents } from '../../../bbbconference/contracts/bbbevent_interface';
import { AssessmentAnswers } from '../../../student_assessment/contracts/assessment_answers';
import { Redirect } from 'react-router';
import { AssessmentDataTable } from '../AssessmentDataTable';
import PieGraph from '../PieGraph';
import LineGraph from '../LineGraph';
import { OverAllAttendanceDataTable } from '../OverallAttendanceDataTable';
import { BBBBatch } from '../../../bbbconference/contracts/bbbBatch_interface';

const drawerWidth = 240;

const styles =  makeStyles((theme: Theme) =>
createStyles({
  topbox: {
    // border: '1px solid lightgray',
    margin: '8px 0',
    width: '100%',
    height: '420px',
    backgroundColor: '#fff',
    // borderRadius: '15px',
    padding: '12px',
    fontWeight: 500
  },
  databox: {
    // border: '1px solid lightgray',
    margin: '8px 0',
    width: '100%',
    // height: '320px',
    backgroundColor: '#fff',
    // borderRadius: '15px',
    // padding: '12px',
    fontWeight: 500
  },
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    zIndex: 0
  },
  contain: {
    position: 'fixed',
    left: drawerWidth + theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      left: drawerWidth + theme.spacing(9) + 1,
    },
    padding: '0px 20px'
  },
  drawerPaper: {
    width: drawerWidth,
    marginLeft: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(9) + 1,
    },
    zIndex: 0
  },
  drawerContainer: {
    overflow: 'auto',
    zIndex: 0
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}),
);

interface AttendanceData {
  id: number,
  subject: string,
  overallAttendance: number
}

interface AssessmentTableData {
  id: string;
  answer: string;
  yourAnswer: string;
  result: number;
  marks: number;
  timeSpent: number;
  visits: number;
}

interface PieData {
  name: string;
  value: number;
}

interface AssessmentTimeTakenLineGraphData {
  name: string;
  timeSpent: number;
  revisits: number;
}

interface Props {
  profile: Parent;
  parentChildren: Children;
}

const ParentAnalytics: FunctionComponent<Props> = ({profile, parentChildren}) => {
  const user = localStorage.getItem('authUser');
  const classes = styles();
  const [redirectTo, setRedirectTo] = useState('');

  // Assessment
  const [viewAssessment, setViewAssessment] = useState<boolean>(false)
  const [activeAssessmentIndex, setActiveAssessmentIndex] = useState(-1)
  const [attemptAssessmentData, setAttemptAssessmentData] = useState<
  OngoingAssessment[] | null
  >(null);
  const [
    attemptAssessment,
    setAttemptAssessment
  ] = useState<OngoingAssessment | null>(null);

  const [gridData, setGridData] = useState<AssessmentTableData[] | null>(null);
  const [
    assessmentTimeTakenLineGraphData,
    setAssessmentTimeTakenLineGraphData
  ] = useState<AssessmentTimeTakenLineGraphData[] | null>(null);

  const [marksObtained, setMarksObtained] = useState<number>(0);
  const [pieGraphMarksData, setPieGraphMarksData] = useState<any[]>([])
  const [totalMarks, setTotalMarks] = useState<number>(0)

  const [
    assessmentTakenPieChartData,
    setAssessmentTakenPieChartData
  ] = useState<PieData[] | null>(null);

  // Attendance
  const [viewAttendance, setViewAttendance] = useState<boolean>(true)
  const [batches, setBatches] = useState<BBBBatch[] | null>(null);
  const [overAllAttendanceData, setOverAllAttendanceData] = useState<AttendanceData[]>([])
  const [overAllAttendancePie, setOverAllAttendancePie] = useState<any[]>([])
  const [attendancePieColour, setAttendancePieColour] = useState<string[]>([])
  console.log(batches)
  const COLORS = [randomColor(), randomColor()];

  function createData(
    id: string,
    answer: string,
    yourAnswer: string,
    result: number,
    marks: number,
    timeSpent: number,
    visits: number
  ) {
    return { id, answer, yourAnswer, result, marks, timeSpent, visits };
  }

  function createLineData(name: string, timeSpent: number, revisits: number) {
    return { name, timeSpent, revisits };
  }

  function allAttendanceData(
    id: number,
    subject: string,
    overallAttendance: number
  ) {
    return { id, subject, overallAttendance };
  }

  const lineGraphDataKeys = {
    timeSpent: randomColor(),
    revisits: randomColor()
  };

  useEffect(() => {
    (async () => {
      try {
        if (!parentChildren.current || !user) return;
        const attemptedAssessmentDataResponse = await attemptedAssessmentData(
          parentChildren.current
        );
        const batchesListResponse = await fetchBBBBatches();
        const [attemptedAssessment, batchesList] = await Promise.all([
          attemptedAssessmentDataResponse, batchesListResponse
        ]);
        setAttemptAssessmentData(attemptedAssessment);
        if (attemptedAssessment) {
          const assessments: OngoingAssessment[] = attemptedAssessment;
          var taken = 0;
          var skipped = 0;
          assessments.forEach((each) => {
            if (each.isSubmitted) {
              taken++;
            } else {
              skipped++;
            }
          });
          const pieData = [
            { name: 'Assessment taken', value: taken },
            { name: 'Assessment skipped', value: skipped }
          ];
          setAssessmentTakenPieChartData(pieData);
        }
        // console.log(attemptedAssessment);
        // const sortedbatches = batchesList.filter(function (batch) {
        //   return JSON.parse(user).batches.includes(batch._id);
        // });
        setBatches(batchesList)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [profile.mobileNo, parentChildren]);


  useEffect(() => {
    if(attemptAssessmentData && activeAssessmentIndex>=0) {
      setAttemptAssessment(attemptAssessmentData[activeAssessmentIndex])
      setTotalMarks(attemptAssessmentData[activeAssessmentIndex].assessment.totalMarks)
      getAnswers(attemptAssessmentData[activeAssessmentIndex]._id, attemptAssessmentData[activeAssessmentIndex])
    }
  }, [activeAssessmentIndex])


  // Attendance
  useEffect(() => {
    if (!user) return
    batches?.map(batch => {
      // @ts-ignore
      getPostMeetingEventsInfo(batch._id).then((result) => {
        if (result.message !== 'Success') return
        const meetings: Meeting[] = result.data
        var noPresent = 0;
        var noAbsent = 0;
        meetings.forEach(eachClass => {
          getPostIndividualMeetingEventsInfo(eachClass.internalMeetingID).then((result) => {
            if (result.message !== 'Success') return
            var ispresent = AttendanceOption.IS_ABSENT;
            const individualMeetingInfo: Meeting = result.data;
            const events: BBBEvents[] = individualMeetingInfo.events;
            const userEvent = events.filter(function (item) {
              if(item.userID === parentChildren.current) {
                return item.type === 'user-joined' || item.type === 'user-left';
              }
            });
            let total_time = 0
            const meetingEndEvent = events.filter(function (items) {
              return items.type === 'meeting-ended';
            });
            var totalMeetingTime = meetingEndEvent[0].eventTriggerTime - individualMeetingInfo.createTime;
            userEvent.forEach(eachEvent => {
              if(eachEvent.type === "user-joined"){
                if(eachEvent.userID === undefined) return
                  if( parentChildren.current !== eachEvent.userID) {
                    ispresent = AttendanceOption.IS_ABSENT
                  }
              }
            })
            if(userEvent.length === 0){
              ispresent = AttendanceOption.IS_ABSENT
            }
            const length = userEvent.length
            if(userEvent.length !== 0 && (length % 2) !== 0) {
              for (let i=0; i<userEvent.length - 1; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              total_time = total_time + (meetingEndEvent[0].eventTriggerTime - userEvent[length-1].eventTriggerTime)
              const percentage = (total_time/totalMeetingTime)*100
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            if(userEvent.length !== 0 && (length % 2) === 0) {
              for (let i=0; i<userEvent.length; i+=2){
                const time = userEvent[i+1].eventTriggerTime - userEvent[i].eventTriggerTime
                total_time = total_time + time
              }
              const percentage = (total_time/totalMeetingTime)*100;
              // eslint-disable-next-line
              ispresent = AttendanceOption.IS_ABSENT;
              if (percentage > 75) ispresent = AttendanceOption.IS_PRESENT;
            }
            console.log(ispresent)
            if(ispresent === AttendanceOption.IS_PRESENT) {
              noPresent++
            } else{
              noAbsent++
            }
            if(meetings.indexOf(eachClass) === (meetings.length - 1)){
              setOverAllAttendanceData([...overAllAttendanceData,allAttendanceData(batches.indexOf(batch)+1, batch.subjectname, (noPresent/(noPresent+noAbsent))*100)])
            }
          });
          // console.log(overAllAttendanceData)
        })
      });
    })
  }, [batches])

  useEffect(() => {
    if(batches?.length === overAllAttendanceData.length){
      overAllAttendanceData.forEach(el=> {
        const name = el.subject
        const value = el.overallAttendance
        setOverAllAttendancePie([...overAllAttendancePie, {name,value}])
        setAttendancePieColour([...attendancePieColour, randomColor()])
      })
    }
  }, [overAllAttendanceData])


  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const getAnswers = (
    assessmentId: string,
    eachAssessment: OngoingAssessment
  ) => {
    if (!parentChildren.current) return;
    attemptedAssessmentAnswers(parentChildren.current, assessmentId)
      .then((result) => {
        let assessmentAnswers: AssessmentAnswers = result;
        var tableData: AssessmentTableData[] = [];
        var serialNo = 0;
        assessmentAnswers.forEach((answer, sectionIndex) => {
          // console.log(sectionIndex,answer)
          // console.log(sectionIndex,eachAssessment)
          answer.forEach((eachAnswer, index) => {
            serialNo++;
            tableData.push(
              createData(
                serialNo.toString(),
                // eachAnswer.index,
                eachAnswer.correct,
                eachAnswer.your,
                eachAnswer.result,
                eachAnswer.marks,
                eachAssessment.assessmentData.sectionsData[sectionIndex]
                  .questions[index].timeSpent,
                eachAssessment.assessmentData.sectionsData[sectionIndex]
                  .questions[index].visits
              )
            );
          });
        });
        // console.log(tableData)
        setGridData(tableData);
        serialNo = 0;
        var lineData: AssessmentTimeTakenLineGraphData[] = [];
        tableData.forEach((data) => {
          lineData.push(
            createLineData(data.id, data.timeSpent / 60, data.visits)
          );
        });
        setAssessmentTimeTakenLineGraphData(lineData);
        var sum = tableData.reduce(function (prev, cur) {
          return prev + cur.marks;
        }, 0);
        setMarksObtained(sum);
        const pieMarksData = [
          { name: 'Marks Obtained', value: sum },
          {
            name: 'Marks Lost',
            value: (eachAssessment.assessment.totalMarks) - sum
          }
        ];
        setPieGraphMarksData(pieMarksData)
      })
      .catch((err) => {
        console.log(err);
      });
  };


  return (
    <div>
      <Minidrawer>
      <div style={{ width: '100%' , minHeight: '100vH' }}>
      <Box display="flex" flexDirection="row" minHeight= '100vH'>
      <Box>
      <div
        style={{position: 'sticky', top:0 , left: 100, width: drawerWidth, height: '100%', zIndex: 1}}
      >
        <div style={{height:"100%",backgroundColor:"#4C8BF5", marginTop:"2px"}} className={classes.drawerContainer}>
          {/* <AnalyticsIcon/> */}
            <Box marginTop="40px" padding="50px 10px" display="flex" flexDirection="column" alignItems="center" gridRowGap="15px">
              <img style={{marginBottom:"40px"}} height="80px" src={AnalyticsIcon} alt=""/>
              <Button style={{color:"#fff"}} disableElevation variant="text" onClick={() => {
                setViewAssessment(false)
                setViewAttendance(true)
              }}>OverAll Analytics</Button>
              <Button style={{color:"#fff"}} disableElevation variant="text" onClick={() => {
                setViewAssessment(true)
                setViewAttendance(false)
              }}>Performance Analytics</Button>
            </Box>
        </div>
      </div>
      </Box>
      <Box paddingLeft="30px">
        <Box>
          <h1 style={{fontSize:"2.5rem",color:"#4C8BF5"}}>Edumatica Report</h1>
          {
            viewAssessment !== true ? '' :
            <Box>
              <h2>Assesment Evalution Report</h2>
              <Box marginBottom="30px" display="flex" alignItems="center" gridColumnGap="30px">
                {
                  !attemptAssessmentData ? '' :
                  <Box padding="0 8px" bgcolor="#fff">
                    <FormControl className={classes.formControl}>
                      <InputLabel id="demo-simple-select-autowidth-label">Assessment</InputLabel>
                      <Select
                        style={{minWidth:"100px"}}
                        value={activeAssessmentIndex}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setActiveAssessmentIndex(e.target.value as number)
                        }
                      >
                        <MenuItem value="-1">Select Assessment</MenuItem>
                        {attemptAssessmentData.map((each, index) => (
                          <MenuItem key={index} value={index}>
                            {each.assessment.assessmentname} - {each.assessment.boardname} {each.assessment.classname} {each.assessment.subjectname}
                          </MenuItem>
                        ))}
                      </Select>
                      <FormHelperText>View Analytics of selected assessment</FormHelperText>
                    </FormControl>
                  </Box>
                }
              </Box>

              {!gridData ? (
                ''
              ) : (
                <Box height="600px !important" className={classes.databox}>
                  <AssessmentDataTable gridData={gridData} />
                </Box>
              )}

            <Grid container spacing={3}>
            <Grid item lg={3} md={12} xs={12}>
                {!attemptAssessment ? (
                  ''
                ) : (
                  <Box width="100%" height="420px" display="flex" flexDirection="column" justifyContent="center" alignItems="center" gridRowGap="20px" padding="10px">
                    
                    <Box display="flex" justifyContent="center" alignItems="center" bgcolor="#fff" padding="10px" width="100%" height="100%" borderRadius="10px">
                      <PieGraph
                          dataset={pieGraphMarksData}
                          COLORS={COLORS}
                          size={300}
                        />
                    </Box>
                    
                    {
                      !attemptAssessment.assessmentData ? '' : 
                    
                    <Box bgcolor="#fff" padding="10px" width="100%" height="100%" borderRadius="10px">
                        {marksObtained} marks obtained out of{' '}
                            {totalMarks}
                        <br/>
                        Student went out of test window{' '}{attemptAssessment.assessmentData.unfocussed} times
                        <br/>
                        Student spent{' '}{(attemptAssessment.assessmentData.unfocussedTime / 60).toFixed(2)}min out of test window
                    </Box>
                    }

                  </Box>
                )}
              </Grid>
              <Grid item lg={9} md={12} xs={12}>
                {!assessmentTimeTakenLineGraphData ? (
                  ''
                ) : (
                  <Box
                    borderRadius="10px"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    className={classes.topbox}
                  >
                    <Box marginLeft="-25px" marginBottom="-18px">
                      <LineGraph
                        dataset={assessmentTimeTakenLineGraphData}
                        datakeys={lineGraphDataKeys}
                        width={1200}
                        height={400}
                      />
                    </Box>
                  </Box>
                )}
              </Grid>
            </Grid>

            </Box>
          }
          {
            viewAttendance !== true ? '' :
            <Box>
              <h2>OverAll Report</h2>
              {
                !overAllAttendanceData || overAllAttendancePie.length !== overAllAttendanceData.length? '' :
                <>
                <Grid container spacing={3}>
                  <Grid item lg={6} md={12} xs={12}>
                    <Box height="400px !important" className={classes.databox}>
                      <OverAllAttendanceDataTable gridData={overAllAttendanceData} />
                    </Box>
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    <Box width="100%" height="400px" display="flex" justifyContent="center" alignItems="center">
                      <Box display="flex" flex flexDirection="column" justifyContent="center" alignItems="center">
                          {
                            batches?.map(batch => {
                              return (
                                <h5 style={{color:`${attendancePieColour[batches.indexOf(batch)]}`}}>{batch.batchfriendlyname}</h5>
                              )
                            })
                          }
                        </Box>
                      <PieGraph
                        dataset={overAllAttendancePie}
                        COLORS={attendancePieColour}
                        size={500}
                      />
                    </Box>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item lg={6} md={12} xs={12}>
                    {
                      !assessmentTakenPieChartData ? '' : 
                      <Box width="100%" height="400px" display="flex" justifyContent="center" alignItems="center">
                        <Box display="flex" flex flexDirection="column" justifyContent="center" alignItems="center">
                          <h5 style={{color:`${COLORS[0]}`}}>Assessment Taken</h5>
                          <h5 style={{color:`${COLORS[1]}`}}>Assessment Skipped</h5>
                        </Box>
                        <PieGraph
                          dataset={assessmentTakenPieChartData}
                          COLORS={COLORS}
                          size={500}
                        />
                      </Box>
                    }
                  </Grid>
                  <Grid item lg={6} md={12} xs={12}>
                    
                  </Grid>
                </Grid>
                </>
              }
            </Box>

          }
        </Box>
      </Box>
      </Box>
      </div>
      </Minidrawer>
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  parentChildren: state.authReducer.parentChildren as Children
});

export default (
  connect(mapStateToProps)(ParentAnalytics)
);