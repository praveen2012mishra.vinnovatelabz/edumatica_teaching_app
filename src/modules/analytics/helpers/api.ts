import axios from 'axios';
import { GET_ATTEMPTED_ASSESSMENT_DATA, GET_ATTEMPT_ASSESSMENT_ANSWERS, GET_ASSIGNED_ASSESSMENTS, GET_ATTEMPT_ASSESSMENTS_BY_ASSESSMENTID } from './routes';

export const getAssignedAssessments = async() => {
  const response = await axios.get(GET_ASSIGNED_ASSESSMENTS)
  return response.data.assessmentArr
}

export const getAttemptAssessmentbyAssessmentId = async(assessmentId: string) => {
  const response = await axios.get(GET_ATTEMPT_ASSESSMENTS_BY_ASSESSMENTID, {params: {assessmentId}})
  return response.data.attemptAssessmentArr
}

export const attemptedAssessmentData = async (studentId: string) => {
  const response = await axios.get(GET_ATTEMPTED_ASSESSMENT_DATA, {params: {studentId}})
  return response.data.attemptAssessmentDocs
}

export const attemptedAssessmentAnswers = async (studentId: string, attemptAssessmentId: string) => {
  const response = await axios.get(GET_ATTEMPT_ASSESSMENT_ANSWERS, {params: {studentId, attemptAssessmentId}})
  return response.data.attemptAssessmentAnswers
}