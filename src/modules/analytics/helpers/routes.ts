const BASE_ROUTE = process.env.REACT_APP_API;

export const GET_ASSIGNED_ASSESSMENTS = BASE_ROUTE + '/attemptassessment/getAllAssignedAssessments'
export const GET_ATTEMPT_ASSESSMENTS_BY_ASSESSMENTID = BASE_ROUTE + '/attemptassessment/getAttemptAssessmentbyAssessmentId'
export const GET_ATTEMPTED_ASSESSMENT_DATA = BASE_ROUTE + '/analytics/studentassessment';
export const GET_ATTEMPT_ASSESSMENT_ANSWERS = BASE_ROUTE + '/analytics/studentassessmentanswers';