import React, { FunctionComponent, useState } from 'react';
import {
  Box,
  Typography,
  Grid
} from '@material-ui/core';
import Button from '../../common/components/form_elements/button';
import Modal from '../../common/components/modal';
import Dropzone from '../../common/components/dropzone/dropzone';
import UploadedContent from '../../common/components/dropzone/previewers/uploadedContent'
import { Student, User } from '../../common/contracts/user';
import { isParent, isStudent } from '../../common/helpers';

interface Props {
  profile : User
  openModal: boolean;
  handleClose: () => any;
  uploadSyllabus: () => any;
  downloadSyllabus: () => any;
  droppedFiles: File[];
  setDroppedFiles: React.Dispatch<React.SetStateAction<File[]>>;
}

const UploadSyllabusModal: FunctionComponent<Props> = ({
  profile,
  openModal,
  handleClose,
  uploadSyllabus,
  droppedFiles,
  setDroppedFiles,
  downloadSyllabus
}) => {
  const uploadFile = () => {
    if (droppedFiles.length < 1) return undefined;
    uploadSyllabus();
    handleClose();
  };

  const addingFile = (file: File[]) => {
    if(file.length > 0) {
      setDroppedFiles(file)
    }
  }

  const removingFile = () => {
    setDroppedFiles([])
  }

  return (
    <Modal
      open={openModal}
      handleClose={handleClose}
      header={
        <Box display="flex" alignItems="center">
          <Box marginLeft="15px">
            <Typography component="span" color="secondary">
              <Box component="h3" color="white" fontWeight="400" margin="0">
                {(isStudent(profile) || isParent(profile)) ? 'Download Syllabus' : 'Upload Syllabus'}
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >     
 
      <Grid container spacing={3}>
    
      {!(isStudent(profile) || isParent(profile)) &&
        <Grid item xs={12}>

          <Dropzone
            onChange={(files) => addingFile(files)}
            files={droppedFiles}
            acceptedFiles={['application/pdf']}
            maxFileSize={104857600} // 100 MB
            contenttype='pdf'
          />
        </Grid>}
        <Grid item xs={12}>
          <UploadedContent
            files={droppedFiles}
            
            onRemoveItem={!(isStudent(profile) || isParent(profile)) ? removingFile: undefined}
            contenttype='pdf'
          />
        </Grid>
      </Grid>
    
      

      <Box display="flex" justifyContent="flex-end" marginTop="10px">
        <Button
          variant="contained"
          color="primary"
          onClick={downloadSyllabus}
          disabled={droppedFiles.length<1}
          style={{marginRight: '10px'}}
        >
          Download
        </Button>
        {
          !(isStudent(profile) || isParent(profile)) && 
          <Button
          variant="contained"
          color="primary"
          onClick={uploadFile}
          disabled={droppedFiles.length<1}
        >
          Upload
        </Button>
        }
        
      </Box>
    </Modal>
  );
};

export default UploadSyllabusModal;
