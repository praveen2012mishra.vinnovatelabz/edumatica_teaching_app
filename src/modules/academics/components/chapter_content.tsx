import React, { FunctionComponent, useState } from 'react';
import { StackActionItem } from '../../common/contracts/stack_action';
import { Chapter } from '../contracts/chapter';
import { Course } from '../contracts/course';
import { QuizMetaData } from '../contracts/quiz_meta';
import { ContentType } from '../enums/content_type';
import Documents from './content_types/documents';
import EmbeddedLinks from './content_types/embedded_links';
import Images from './content_types/images';
import Slides from './content_types/slides';
import Videos from './content_types/videos';
import Quizes from './content_types/quizes';
import { User } from '../../common/contracts/user'
interface Props {
  profile : User  ;
  chapter: Chapter;
  activeContentType: string;
  stackActions : StackActionItem<ContentType>[];
  emitStackAction: (action: StackActionItem) => any;
  removeStackAction: (filename:string, action:string)=>any ,
  activeCourse: Course;
  chapters: Chapter[];
  setChapters: (chapters: Chapter[]) => void;
  activeChapterIndex: number;
  quizMeta: QuizMetaData;
  setQuizMeta: (quiz: QuizMetaData) => void;
  updateVersion : number ;
}

const ChapterContent: FunctionComponent<Props> = ({
  profile,
  chapter,
  activeContentType,
  stackActions,
  emitStackAction,
  removeStackAction,
  activeCourse,
  chapters,
  setChapters,
  activeChapterIndex,
  quizMeta,
  setQuizMeta,
  updateVersion
}) => {
  const [imagesVersion,setImagesVersion] = useState<number>(0)
  const [videosVersion,setVideosVersion] = useState<number>(0)
  const [documentsVersion,setDocumentsVersion] = useState<number>(0)
  const [slidesVersion,setSlidesVersion] = useState<number>(0)

  React.useEffect(()=>{
    switch(activeContentType) {
      case ContentType.IMAGE : 
      setImagesVersion(prev=>prev+1)
      case ContentType.VIDEO : 
      setVideosVersion(prev=>prev+1)

      case ContentType.SLIDE : 
      setSlidesVersion(prev=>prev+1)

      case ContentType.DOCUMENT : 
      setDocumentsVersion(prev=>prev+1)
    }
    
  },[updateVersion])
  switch (activeContentType) {
    case ContentType.SLIDE:
      return (
        <Slides
          profile=  {profile}
          activeCourse={activeCourse}
          stackActions ={stackActions}
          removeStackAction ={removeStackAction}
          slidesVersion={slidesVersion}
          chapter = {chapter}
          emitStackAction={emitStackAction}
        />
      );
    case ContentType.EMBED_LINK:
      return (
        <EmbeddedLinks
        profile=  {profile}
        activeCourse={activeCourse}
        removeStackAction ={removeStackAction}
        chapter = {chapter}
          emitStackAction={emitStackAction}
        />
      );
    case ContentType.VIDEO:
      return (
        <Videos
        profile=  {profile}
        stackActions={stackActions}
        activeCourse={activeCourse}
        videosVersion= {videosVersion}
        removeStackAction ={removeStackAction}
        chapter = {chapter}
        emitStackAction={emitStackAction}
        />
      );
    case ContentType.IMAGE:
      return (
        <Images
        profile=  {profile}
        stackActions={stackActions}
        activeCourse={activeCourse}
        imagesVersion = {imagesVersion}
        removeStackAction ={removeStackAction}
        chapter = {chapter}
          emitStackAction={emitStackAction}
        />
      );
    case ContentType.DOCUMENT:
      return (
        <Documents
        profile=  {profile}
        stackActions={stackActions}
        activeCourse={activeCourse}
        documentsVersion = {documentsVersion}
        removeStackAction ={removeStackAction}
        chapter = {chapter}
          emitStackAction={emitStackAction}
        />
      );
    default:
      return (
        <Quizes
          chapter= {chapter}
          activeCourse={activeCourse}
          emitStackAction={emitStackAction}
          quizMeta={quizMeta}
          setQuizMeta={setQuizMeta}
        />
      );
  }
};

export default ChapterContent;
