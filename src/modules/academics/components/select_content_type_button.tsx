import React, { FunctionComponent } from 'react';
import { Box, Button } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentBtn: {
      '& button': {
        border: '1px solid rgba(102, 102, 102, 0.2)',
        borderRadius: '3px',
        fontSize: fontOptions.size.small,
        fontWeight: fontOptions.weight.normal,
        fontFamily:fontOptions.family,
        textAlign: 'center',
        color: '#666666',
        padding: '8px 8px'
      }
    },
    activeContentBtn: {
      '& button': {
        color: '#202842',
        border: '1px solid #00B9F5'
      }
    }
  })
);

interface Props {
  icon: React.ReactNode;
  isActive: boolean;
  onClick: () => any;
}

const SelectContentTypeButton: FunctionComponent<Props> = ({
  children,
  icon,
  isActive,
  onClick
}) => {
  const classes = useStyles();
  return (
    <Box
      className={`${classes.contentBtn} ${
        isActive ? classes.activeContentBtn : ''
      }`}
    >
      <Button
        disableElevation
        variant="outlined"
        size="small"
        startIcon={icon}
        onClick={onClick}
        style={{padding: '10px 20px'}}
      >
        {children}
      </Button>
    </Box>
  );
};

export default SelectContentTypeButton;
