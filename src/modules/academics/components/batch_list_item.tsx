import React, { FunctionComponent, useEffect, useState } from 'react';
import GroupWorkIcon from '../../../assets/images/group-icon.png';
import GroupIcon from '../../../assets/images/students.png';
import EventIcon from '../../../assets/images/calendar.png';
import moment from 'moment';
import { Batch } from '../contracts/batch';
import DetailedCard from '../../common/components/detailedcards';

interface Subheading {
  icon: any[],
  text: string
}

interface Content {
  head: string,
  value: string
}

interface Fullcontent {
  icon: any[],
  text: Content[]
}

interface Props {
  item: Batch;
  editItem: () => any;
  removeItem: () => any;
}

const BatchListItem: FunctionComponent<Props> = ({
  item,
  editItem,
  removeItem
}) => {

  const [bgcolor, setBgcolor] = useState('');
  const [heading, setHeading] = useState('');
  const [subheading, setSubheading] = useState<Subheading[]>();
  const [fullcontent, setFullcontent] = useState<Fullcontent>();

  useEffect(() => {
    const batchIcon = <img src={GroupWorkIcon} alt="Batch Icon" />
    const studentsIcon =  <img src={GroupIcon} alt="Students Icon" />
    const calenderIcon = <img src={EventIcon} alt="Calender Icon" />

    setBgcolor("#FFB2B2");
    setHeading(item.classname + ' | ' + item.subjectname);
    setSubheading([{icon: [batchIcon], text: item.batchfriendlyname}, {icon: [studentsIcon], text: item.students.length + ' Students'}]);
    setFullcontent({icon: [calenderIcon], text: 
      [{head: 'Batch Start', value: moment(item.batchstartdate).format('DD-MM-YYYY')}, 
      {head: 'Batch End', value: moment(item.batchenddate).format('DD-MM-YYYY')}]
    });
  }, [item]);

  return (
    <DetailedCard bgcolor={bgcolor} heading={heading}
      subheading={subheading as Subheading[]} fullcontent={fullcontent as Fullcontent} 
      editItem={editItem} removeItem={removeItem}
    />
  );
};
export default BatchListItem;

