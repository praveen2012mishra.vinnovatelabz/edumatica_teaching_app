import React, { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  Paper
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import EditIcon from '../../../../assets/images/edit-shadow.png';
import DeleteIcon from '../../../../assets/images/delete-shadow.png';
import { StackActionType } from '../../../common/enums/stack_action_type';
import { ContentType } from '../../enums/content_type';
import { ChapterContent } from '../../contracts/chapter_content';
import { StackActionItem } from '../../../common/contracts/stack_action';
import Button from '../../../common/components/form_elements/button';
import ReactPlayer from 'react-player/lazy';
import {useSnackbar} from "notistack"
import { Chapter } from '../../contracts/chapter';
import { Course } from '../../contracts/course';
import { fetchSignedContents } from '../../../common/api/academics';
import { fontOptions } from '../../../../theme';
import { User } from '../../../common/contracts/user'
import { isParent, isStudent } from '../../../common/helpers';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    centerAlignedText: {
      textAlign: 'center',
      width: '101.5%'
    },
    floatRight: {
      float: 'right'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props {
  profile : User
  activeCourse : Course
  chapter : Chapter,
  emitStackAction: (action: StackActionItem) => any;
  removeStackAction : (filename : string, action:string) =>any
}

interface FormData {
  embedLink: string;
}

const ValidationSchema = yup.object().shape({
  embedLink: yup
    .string()
    .required('embed link is a required field')
    .url('embed link must be a valid URL')
});

export const EmbeddedLinks: FunctionComponent<Props> = ({
  profile,
  activeCourse,
  chapter,
  emitStackAction,
  removeStackAction
}) => {
  const {enqueueSnackbar} = useSnackbar()

  const { handleSubmit, register, errors, getValues, setValue } = useForm<
    FormData
  >({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });

  const classes = useStyles();
  const [data,setData] = React.useState<ChapterContent[]>([])
  const [newlink, setNewlink] = React.useState<string[]>([])

  React.useEffect(()=>{
    fetchEmbedLinks()
  },[])

  const fetchEmbedLinks = async () =>{
    try{
        const chapterContents = await fetchSignedContents({
        boardname: activeCourse.board !== undefined ? activeCourse.board : ('' as string),
        classname: activeCourse.className !== undefined ? activeCourse.className : ('' as string),
        subjectname: activeCourse.subject !== undefined ? activeCourse.subject : ('' as string),
        chaptername: chapter.name as string,
        contenttype: ContentType.EMBED_LINK
      });
      setData(chapterContents)
    }
    catch(error){
      console.log(error)
    }
    
  }



  const allowedDomain = [
    'youtube', 'twitch', 'facebook', 'vimeo', 'soundcloud', 
    'streamable', 'wistia', 'mixcloud', 'dailymotion' 
  ]

  const createEmbedLink = ({ embedLink }: FormData) => {
    const url = (new URL(embedLink)).hostname;
    let domain = url.replace('www.','');
    domain = domain.replace('.com','');
    if(allowedDomain.includes(domain)) {
      const embedLinkIndex = data.findIndex(
        (content) => content.uuid === embedLink
      );
  
      // We won't add the embed link if the link already exists in the
      // array.
      if (embedLinkIndex !== -1) return;
  
      emitStackAction({
        type: StackActionType.CREATE,
        payload: {
          data: embedLink,
          type: ContentType.EMBED_LINK,
          chaptername: chapter.name,
        boardname:activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject
        }
      });
      
      const link = [...newlink, ...[embedLink]]
      setNewlink(link)
      
    } else {
      enqueueSnackbar("Video from this Domain cannot be added", { variant: "error" })
    }
    setValue('embedLink', '');
  };

  const deleteEmbedLink = ({ embedLink }: FormData) => {
    emitStackAction({
      type: StackActionType.DELETE,
      payload: {
        data: embedLink,
        type: ContentType.EMBED_LINK,
        chaptername: chapter.name,
        boardname:activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject
      }
    });

    let docUpd = [...data]
    docUpd = docUpd.filter((item) => item.uuid !== embedLink);
    setData(docUpd)
  };

  const updateEmbedLink = ({ embedLink }: FormData) => {
    emitStackAction({
      type: StackActionType.DELETE,
      payload: {
        data: embedLink,
        type: ContentType.EMBED_LINK,
        chaptername: chapter.name,
        boardname:activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject
      }
    });

    setValue('embedLink', embedLink);
  };

  const embedLinks = [...data.map((content) => content.uuid), ...newlink];

  const embedLink = getValues('embedLink');

  return (
    <div>
      {!(isStudent(profile) || isParent(profile)) && 
      <React.Fragment>
          <Box
        component="h3"
        fontWeight={fontOptions.weight.bold}
        fontSize={fontOptions.size.small}
        fontFamily={fontOptions.family}
        line-height="21px"
        color="#666666"
        margin="20px 0"
      >
        Add Embed Link
      </Box>

      <form onSubmit={handleSubmit(createEmbedLink)}>
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item xs={10}>
            <FormControl fullWidth margin="none">
              <Input
                name="embedLink"
                placeholder="Enter YouTube Video Link"
                inputRef={register}
                className={errors.embedLink ? classes.error : ''}
              />

              {errors.embedLink && (
                <FormHelperText error>
                  {errors.embedLink.message}
                </FormHelperText>
              )}
            </FormControl>
          </Grid>

          <Grid item xs={2}>
            <Box className={classes.floatRight}>
              <Button
                disableElevation
                color="primary"
                type="submit"
                variant="contained"
                size="small"
              >
                <AddIcon /> Add
              </Button>
            </Box>
          </Grid>
        </Grid>
      </form>
        </React.Fragment>}
      

      {embedLink && embedLink.length > 0 && errors.embedLink === undefined && (
        <Box marginY="5px">
          <ReactPlayer controls url={embedLink} width='100%' light={true} />
        </Box>
      )}

      <Box
        component="h3"
        fontWeight={fontOptions.weight.bold}
        fontSize={fontOptions.size.small}
        line-height="21px"
        color="#666666"
        margin="20px 0"
      >
        Contents
      </Box>
        <Grid container spacing={3}>
          {embedLinks.map((link) => (
            <Grid item xs={6}>
              <Paper className={classes.centerAlignedText}>
                <ReactPlayer controls url={link} width='100%' light={true} />
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <IconButton
                      size="small"
                      onClick={() => updateEmbedLink({ embedLink: link as string })}
                      style={{ marginRight: '5px' }}
                    >
                      <img src={EditIcon} alt="Edit Icon" />
                    </IconButton>
                  </Grid>
                  <Grid item xs={6}>
                    <IconButton
                      size="small"
                      onClick={() => deleteEmbedLink({ embedLink: link as string })}
                    >
                      <img src={DeleteIcon} alt="Delete Icon" />
                    </IconButton>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          ))}
        </Grid>
    </div>
  );
};

export default EmbeddedLinks;
