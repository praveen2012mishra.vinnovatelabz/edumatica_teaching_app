import React, { FunctionComponent, useState, useEffect } from 'react';
import { 
  Box, FormControl, InputLabel, Select, MenuItem, Grid, InputBase
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { StackActionItem } from '../../../common/contracts/stack_action';
import { StackActionType } from '../../../common/enums/stack_action_type';
import { ContentType } from '../../enums/content_type';
import Dropzone from '../../../common/components/dropzone/dropzone';
import { ChapterContent } from '../../contracts/chapter_content';
import UploadedChapterContent from '../../../common/components/dropzone/previewers/uploadedChapterContent'
import { fetchSignedContents } from '../../../common/api/academics';
import { Course } from '../../contracts/course';
import { Chapter } from '../../contracts/chapter';
import { fontOptions } from '../../../../theme';
import { uniqBy } from 'lodash' 
import {useSnackbar} from "notistack"
import { User } from '../../../common/contracts/user'
import { isParent, isStudent } from '../../../common/helpers';



const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    subHeading: {
      fontWeight: 500,
      lineHeight: '19px',
      color: '#666666',

      '& span': {
        background: '#00B9F5',
        fontSize: '12px',
        lineHeight: '14px',
        color: 'rgba(255, 255, 255, 0.87)',
        borderRadius: '50%',
        padding: '4px 7px',
        marginLeft: '10px'
      }
    },
    imageContainer: {
      width: '273px',
      height: '193px',
      margin: '20px 30px 20px 0',
      '& img': {
        height: '100%',
        width: '100%',
        border: '1px dashed #DDDDDD',
        borderRadius: '5px'
      }
    }
  })
);

interface Props {
  profile : User
  activeCourse : Course
  chapter : Chapter
  stackActions:StackActionItem<ContentType>[]
  emitStackAction: (action: StackActionItem) => any;
  removeStackAction : (filename : string , action: string) => any,
  slidesVersion: number
}

interface SelectProp {
  label: string | undefined;
  value: string | undefined;
}

export const Slides: FunctionComponent<Props> = ({ profile,activeCourse,chapter ,stackActions, emitStackAction, slidesVersion, removeStackAction }) => {
  const [docs, setDocs] = React.useState<ChapterContent[]>([]);
  const [subtype, setSubtype] = React.useState<string | undefined>('Class Content');
  const [selectOption, setSelectOption] = React.useState<SelectProp[]>([]);
  const {enqueueSnackbar} = useSnackbar()
  const [open, setOpen] = React.useState(false);
  const [dropFiles,setDropFiles]= useState<File[]>(stackActions.filter((action)=>{
    return action.type==StackActionType.CREATE && action.payload.type == ContentType.SLIDE
  }).map((action)=>action.payload.data))
  const classes = useStyles();

  

  useEffect(() => {
    setSelectOption([]);

    const currentSelectOption: SelectProp[] = [
      {label: 'Class Content', value: 'Class Content'},
      {label: 'Extra Material', value: 'Extra Material'}
    ];

    docs.forEach(element => {
      currentSelectOption.push(
        {label: element.contentsubtype, value: element.contentsubtype}
      )
    })
    setSelectOption(uniqBy(currentSelectOption, 'label'));
  }, [docs]);


  useEffect(()=>{
    fetchSlides()
    setDropFiles([])
  },[slidesVersion])

  const fetchSlides = async () =>{
    try{
        const chapterContents = await fetchSignedContents({
        boardname: activeCourse.board !== undefined ? activeCourse.board : ('' as string),
        classname: activeCourse.className !== undefined ? activeCourse.className : ('' as string),
        subjectname: activeCourse.subject !== undefined ? activeCourse.subject : ('' as string),
        chaptername: chapter.name as string,
        contenttype: ContentType.SLIDE
      });
      setDocs(chapterContents)
    }
    catch(error){
      console.log(error)
    }
    
  }


  const handleClose = () => setOpen(false);
  const handleOpen = () => setOpen(true);

  const handleDroppedFiles = (files: File[]) => {
    if (files.length < 1) return;
    setDropFiles((prev) => {
      return [...files, ...prev]
    })

    const newContentArr: ChapterContent[] = []
    files.forEach((file) => {
      emitStackAction({
        type: StackActionType.CREATE,
        payload: {
          data: file,
          type: ContentType.SLIDE,
          subtype: subtype,
          chaptername: chapter.name,
        boardname:activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject
        }
      });

      const newContent = {
        chapter: chapter.name,
        contentname: file.name,
        contenttype: "pdf",
        contentsubtype: subtype
      }
      
      newContentArr.push(newContent)
    });

    const docUpd = [...docs, ...newContentArr]
    setDocs(docUpd)
  };

  const removeFileItem = (filename: string) => {
    emitStackAction({
      type: StackActionType.DELETE,
      payload: {
        data: filename ,
        type: ContentType.SLIDE,
        chaptername: chapter.name,
        boardname:activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject
      }
    });

    let docUpd = [...docs]
    docUpd = docUpd.filter((item) => item.contentname !== filename);
    setDocs(docUpd)
  };

  const removeDropFileItem = (file:File, index: Number) =>{
    setDropFiles((prev)=>{
      return prev.filter((dropFile:File,ind:Number)=>{
        return (dropFile.name!=file.name && ind==index)
      })
    })
    removeStackAction(file.name , StackActionType.CREATE)
  }

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if(!event.target.value) {
      setSubtype(selectOption[0].value);
    } else {
      setSubtype(event.target.value as string);
    }
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.which === 13) {
      if(e.currentTarget.value){
        const currentSelectOption = [...selectOption];
        const check =currentSelectOption.filter((selectProp)=>{
          return selectProp.value?.toLowerCase() == e.currentTarget.value.toLowerCase()
        }).length
        if(check==0){
          currentSelectOption.push(
            {label: e.currentTarget.value as string, value: e.currentTarget.value as string}
          )
          setSelectOption(currentSelectOption);
          setSubtype(e.currentTarget.value as string)
        }
        else{
          enqueueSnackbar('Added type already exists',{variant:'warning'})
          setSubtype(selectOption[0].value)
        }
      }
      
      handleClose();
      e.currentTarget.value = "";
    }
    e.stopPropagation();
  };

  

  return (
    <Box>
      {!(isStudent(profile) || isParent(profile)) && 
      <React.Fragment>

     
      <Box
        component="h3"
        fontWeight={fontOptions.weight.bold}
        fontSize={fontOptions.size.small}
        fontFamily={fontOptions.family}
        line-height="21px"
        color="#666666"
        margin="20px 0"
      >
        Upload Slides
      </Box>

      <Grid container spacing={3}>
        <Grid item md={3}>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="demo-simple-select-outlined-label">Type</InputLabel>
            <Select
              open={open}
              onClose={handleClose}
              onOpen={handleOpen}
              value={subtype}
              onChange={handleChange}
              label="Type"
            >
              <MenuItem value="" onClick={handleOpen}>
                <InputBase
                  onKeyDown={handleKeyDown}
                  placeholder="Type Header & press Enter"
                />
              </MenuItem>

              {selectOption.map(element => {
                return(
                  <MenuItem key={element.value} value={element.value}>{element.label}</MenuItem>
                )
              })}
            </Select>
            
          </FormControl>
        </Grid>
        <Grid item md={9}>
          <Dropzone
            onChange={handleDroppedFiles}
            files={dropFiles}
            acceptedFiles={['application/pdf']}
            maxFileSize={104857600} // 100 MB
            contenttype='pdf'
          />
        </Grid>
      </Grid>
      </React.Fragment>}

      <UploadedChapterContent
        files={dropFiles}
        onRemoveItem={!(isStudent(profile) || isParent(profile))?removeFileItem:undefined}
        onRemoveDropFile = {removeDropFileItem}
        contenttype='pdf'
        fileStructure={selectOption}
        data={docs}
      />

      {/* TODO: Replace docs with original array value and loop through the images  */}
      {/* {docs && (
        <Box marginTop="30px">
          <Typography className={classes.subHeading}>
            Slides Uploaded
            <span>2</span>
          </Typography>
          <Box display="flex">
            <Box className={classes.imageContainer}>
              <img src={GirlWithCup} alt="documents" />
            </Box>
            <Box className={classes.imageContainer}>
              <img src={GirlWithCup} alt="documents" />
            </Box>
          </Box>
        </Box>
      )} */}
    </Box>
  );
};

export default Slides;
