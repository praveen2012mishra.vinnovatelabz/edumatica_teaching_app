import React, { FunctionComponent, useState } from 'react';
import { Box, IconButton } from '@material-ui/core';
import {
  Edit as EditIcon,
  RemoveCircle as RemoveCircleIcon
} from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Chapter } from '../contracts/chapter';
import EditChapterModal from '../components/edit_chapter_modal';
import { fontOptions } from '../../../theme';
import {  User } from '../../common/contracts/user';
import { isParent, isStudent } from '../../common/helpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chapterItem: {
      background: 'rgba(241, 243, 247, 0.2)',
      border: '1px solid rgba(151, 151, 151, 0.2)',
      borderRadius: '5px',
      cursor: 'pointer',
      margin: '5px 0',
      padding: '21px 20px 24px 20px',
      wordBreak: 'break-word',
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '19px',
      color: '#242E4C'
    },

    isActive: {
      background: '#00B9F5',
      border: '1px solid #00B9F5',
      color: '#ffffff'
    }
  })
);

interface Props {
  profile : User
  chapter: Chapter;
  chapterNumber: number;
  isActive: boolean;
  handleOnClick: () => any;
  editItem: (name: string) => any;
  removeItem: () => any;
}

const ChapterListItem: FunctionComponent<Props> = ({
  profile,
  chapter,
  chapterNumber,
  isActive,
  handleOnClick,
  editItem,
  removeItem
}) => {
  const classes = useStyles();
  const [openEditChapterModal, setOpenEditChapterModal] = useState(false);

  const editChapter = async (name: string) => {
    setOpenEditChapterModal(false);
    editItem(name);
  };
  return (
    <Box
      display="flex"
      justifyContent="space-between"
      padding="5px"
      borderRadius='5px'
      className={`${classes.chapterItem} ${isActive ? classes.isActive : ''}`}
      onClick={handleOnClick}
    >
      Chapter {chapterNumber} - {chapter.name}
      {(chapter.isMaster === false && !(isStudent(profile) || isParent(profile)))  && (
        <Box display="flex">
          <IconButton
            size="small"
            onClick={() => setOpenEditChapterModal(true)}
          >
            <EditIcon color="secondary"  style={isActive ? {color:'#fff'} : {}} />
          </IconButton>
          <IconButton size="small" onClick={removeItem}>
            <RemoveCircleIcon color="secondary"  style={isActive ? {color:'#fff'} : {}} />
          </IconButton>
          <EditChapterModal
            openModal={openEditChapterModal}
            handleClose={() => setOpenEditChapterModal(false)}
            handleEditChapter={editChapter}
            chaptername={chapter.name}
          />
        </Box>
      )}
    </Box>
  );
};

//Chapter {chapterNumber} - {chapter.name}
export default ChapterListItem;
