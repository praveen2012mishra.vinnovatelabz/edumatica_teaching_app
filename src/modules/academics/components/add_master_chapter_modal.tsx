import React, { FunctionComponent, useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import Button from '../../common/components/form_elements/button';
import Modal from '../../common/components/modal';
import { Chapter } from '../contracts/chapter';

interface Props {
  openModal: boolean;
  handleClose: () => any;
  masterChapters: Chapter[]
  handleAddMasterChapter: (submit: boolean) => any;
}

const AddMasterChapterModal: FunctionComponent<Props> = ({
  openModal,
  handleClose,
  masterChapters,
  handleAddMasterChapter
}) => {
  // eslint-disable-next-line
  const [submit, setSubmit] = useState(false);

  const handleFormSubmission = () => {
    handleAddMasterChapter(true);
    setSubmit(true);
  };

  return (
    <Modal
      open={openModal}
      handleClose={() => {
        setSubmit(false);
        handleClose();
      }}
      header={
        <Box display="flex" alignItems="center">
          <Box marginLeft="15px">
            <Typography component="span" color="secondary">
              <Box component="h3" color="white" fontWeight="400" margin="0">
                Add Offered Chapters
              </Box>
            </Typography>
          </Box>
        </Box>
      }
    >
      <form onSubmit={handleFormSubmission}>
        {
              masterChapters.map(chapter => {
                return (
                  <Box key={chapter.name + '|' + chapter.orderNo} 
                    display="flex" justifyContent="space-between" 
                    bgcolor="lightblue" borderRadius="5px" padding="0 10px"
                  >
                    <h4>{chapter.name}</h4>
                  </Box>
                )
              })
            }

        <Box display="flex" justifyContent="flex-end" marginTop="10px">
          <Button
            disableElevation
            variant="contained"
            color="primary"
            type="submit"
          >
            Add
          </Button>
        </Box>
      </form>
    </Modal>
  );
};

export default AddMasterChapterModal;
