import React, { FunctionComponent, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Box,
  Container,
  FormHelperText,
  Grid
} from '@material-ui/core';
import { PictureAsPdf as PdfIcon } from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import PhotoIcon from '../../../../assets/images/images.png';
import DocumentsIcon from '../../../../assets/images/documents.png';
import VideoIcon from '../../../../assets/images/video.png';
import LinkIcon from '../../../../assets/images/embed-link.png';
import ListIcon from '../../../../assets/images/quiz.png';
import Scrollbars from 'react-custom-scrollbars';

// import { uniqBy } from 'lodash';
import {
  StackActionItem,
  ChapterActionItem
} from '../../../common/contracts/stack_action';
import { Chapter } from '../../contracts/chapter';
import { ContentType } from '../../enums/content_type';
import {     
  createCourseChapter,
  // fetchCourseChaptersList,
  updateChapterContent as updateChapterContentRequest,
  uploadFileOnUrl,
  fetchChapterContent,
  updateChapter,
  deleteChapter,
  fetchBatchesList,
  fetchMasterChaptersList,
  fetchCustomChaptersList,
  createCourseChapterFromMaster,
  reorderChapters
} from '../../../common/api/academics';
import {
  createOrgChapterContent as createChapterContentRequest,
  createOrgChapterContentFile,
  deleteOrgChapterContent,
  fetchOrgChapterContentUploadUrl
} from '../../../common/api/content';
import { fetchOrgCoursesList } from '../../../common/api/organization';
import {
  generateCourseChaptersSchema,
  generateQuestionSchemaForServer
} from '../../helpers';
import { getChapterStub } from '../../stubs/chapter';
import { Organization } from '../../../common/contracts/user';
import { Course } from '../../contracts/course';
import Button from '../../../common/components/form_elements/button';
import ChapterContent from '../../components/chapter_content';
import CourseContentSidebar from '../../components/course_content_sidebar';
import MiniDrawer from '../../../common/components/sidedrawer';
import SelectContentTypeButton from '../../components/select_content_type_button';
import { StackActionType } from '../../../common/enums/stack_action_type';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { QuestionApp } from '../../contracts/question';
import { QuizMetaData } from '../../contracts/quiz_meta';
// import axios from 'axios';
import { Batch } from '../../contracts/batch';
import { useSnackbar } from 'notistack';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    courseHeading: {
      margin: '0px',
      marginBottom: '6px',
      fontWeight: 500,
      color: '#666666'
    },
    helperText: {
      fontSize: fontOptions.size.small,
      fontWeight: fontOptions.weight.normal,
      fontFamily:fontOptions.family,
      color: '#979797'
    },
    saveBtn: {
      '& button': {
        fontSize: fontOptions.size.medium,
        fontWeight: fontOptions.weight.bold,
        fontFamily:fontOptions.family,
        lineHeight: '28px',
        padding: '14px 25px'
      }
    }
  })
);

interface Props {
  profile: Organization;
}

const OrgCourses: FunctionComponent<Props> = ({ profile }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [activeContentType, setActiveContentType] = useState(ContentType.IMAGE);
  const [activeChapterIndex, setActiveChapterIndex] = useState(0);
  const [chapters, setChapters] = useState<Chapter[]>([]);
  const [masterChapters, setMasterChapters] = useState<Chapter[]>([]);
  const [activeCourseIndex, setActiveCourseIndex] = useState(0);
  const [activeBatchIndex, setActiveBatchIndex] = useState(0);
  const [courses, setCourses] = useState<Course[]>([]);
  const [batches, setBatches] = useState<Batch[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  const [serverError, setServerError] = useState('');
  const [updateVersion,setUpdateVersion] = useState<number>(0)
  // const [quizName] = useState(
  //   Math.round(new Date().getTime() / 1000) + '_stamp_' + ContentType.QUIZ
  // );
  const [quizMeta, setQuizMeta] = useState<QuizMetaData>({
    title: '',
    duration: 0,
    marks: 0,
    contentname: ''
  });
  const [isReorderChapter, setIsReorderChapter] = useState(false);
  /**
   * Actions stack is the collection of actions occured on the item.
   * These actions then could be then followed to reach the desired state
   * for the item.
   *
   * For example, There could be NULL -> CREATE -> UPDATE operation for
   * item but without this stack all we would know about the UPDATE
   * operation of the item. But with Actions Stack we could follow the
   * trail and reach the desired state for the item.
   */
  const [stackActions, setStackActions] = useState<
    StackActionItem<ContentType>[]
  >([]);
  const [chapterActions, setChapterActions] = useState<ChapterActionItem[]>([]);
  const [trigger, setTrigger] = useState<number>(0)

  const activeCourse = courses[activeCourseIndex];
  const activeChapter = chapters[activeChapterIndex];
  const activeBatch = batches[activeBatchIndex];

  const classes = useStyles();
  eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Landed Organization Content Page');

  // Everytime, a new course is selected we'd fetch the chapters for the
  // selected course.
  useEffect(() => {
    (async () => {
      try {
        const coursesList = await fetchOrgCoursesList();
        const course = coursesList[activeCourseIndex];
        const batchesListResponse = await fetchBatchesList();
        let sortedbatches;
        if(!activeCourse) {
          sortedbatches = batchesListResponse.filter(function (batch) {
            return batch.boardname === course.board && batch.classname === course.className && batch.subjectname === course.subject
          });
        } else {
          sortedbatches = batchesListResponse.filter(function (batch) {
            return batch.boardname === activeCourse.board && batch.classname === activeCourse.className && batch.subjectname === activeCourse.subject
          });
        }
        if(!sortedbatches[activeBatchIndex]) setActiveBatchIndex(0) 
        const batch = sortedbatches[activeBatchIndex] ? sortedbatches[activeBatchIndex] : sortedbatches[0]
        
        if (course === undefined || batch._id === undefined) return;

        // const {
        //   masterChapters,
        //   customChapters
        // } = await fetchCourseChaptersList({
        //   batchId: batch._id,
        //   boardname: course.board,
        //   classname: course.className,
        //   subjectname: course.subject
        // });

        // uncomment this part to get master chapter list
        const masterChaptersPromise = fetchMasterChaptersList({
          boardname: course.board,
          classname: course.className,
          subjectname: course.subject
        })
        const customChaptersPromise = fetchCustomChaptersList({
          batchId: batch._id,
          boardname: course.board,
          classname: course.className,
          subjectname: course.subject
        })

        const [masterChapters, customChapters] = await Promise.all([
          masterChaptersPromise,
          customChaptersPromise
        ]);
        let structuredMasterChapters = generateCourseChaptersSchema(
          masterChapters,
          []
        );
        structuredMasterChapters = structuredMasterChapters.map((chapter, index) => {
          return { ...chapter, orderNo: index + 1 };
        });
        setMasterChapters(structuredMasterChapters)

        // remove [] and uncomment masterChapters to add to structuredChapters(all) list 
        let structuredChapters = generateCourseChaptersSchema(
          [],
          // masterChapters,
          customChapters
        );

        structuredChapters = structuredChapters.map((chapter, index) => {
          return { ...chapter, orderNo: index + 1 };
        });

        setActiveChapterIndex(0);
        setCourses(coursesList);
        setChapters(structuredChapters);
        setBatches(sortedbatches)
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [profile.mobileNo, activeCourseIndex, trigger]);

  // Everytime, a new chapter is selected we'd fetch the content for the
  // selected chapter.
  useEffect(() => {
    getChapterContent();
    // eslint-disable-next-line
  }, [profile.mobileNo, activeChapterIndex, activeChapter, activeCourse]);

  const removeStackAction = (filename:string,action: string) =>{
    setStackActions(prev=>{
      return prev.filter(el=>{
        return el.payload.data !=filename  && el.type!=action
      })
    })
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const getChapterContent = async () => {
    if (activeCourse === undefined || activeChapter === undefined) return;

    try {
      const chapterContent = await fetchChapterContent({
        batchId: activeBatch._id,
        boardname: activeCourse.board,
        classname: activeCourse.className,
        subjectname: activeCourse.subject,
        chaptername: activeChapter.name
      });

      const chapterIndex = chapters.findIndex(
        (chapter) => chapter.name === chapterContent.chaptername
      );

      const chaptersList = [...chapters];

      // @ts-ignore
      chaptersList[chapterIndex].contents = chapterContent.contents;

      setChapters(chaptersList);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const createChapterFromMaster = (submit: boolean) => {
    var newChapters:string[] = []
    masterChapters.forEach(chapter => {
      if(!activeBatch._id) return
      var name = chapter.name.substring(chapter.name.indexOf(":")+2, chapter.name.length);
      newChapters.push(name)
    })
    console.log(newChapters)
    if(submit !== true) return
    createCourseChapterFromMaster({
      batchId: activeBatch._id,
      batchfriendlyname: activeBatch.batchfriendlyname,
      chaptername: newChapters,
      boardname: activeCourse.board,
      classname: activeCourse.className,
      subjectname: activeCourse.subject
    }).then(response => {
      if(response.code === 0){
        setTrigger((Math.random() * 100) + 1)
        enqueueSnackbar("Successfully added chapters", {
          variant: 'success',preventDuplicate: false, persist: false,
        });
      }else{
        setTrigger((Math.random() * 100) + 1)
        enqueueSnackbar(response.message, {
          variant: 'error',preventDuplicate: false, persist: false,
        });
      }
    }).catch(err => {
      console.log(err)
      
    })
  }

  const createChapter = (name: string) => {
    if (activeCourse === undefined) return;

    setChapters([...chapters, getChapterStub({ name, isMaster: false })]);

    const createChapterItem = {
      batchId: activeBatch._id,
      batchfriendlyname: activeBatch.batchfriendlyname,
      chaptername: name,
      boardname: activeCourse.board,
      classname: activeCourse.className,
      subjectname: activeCourse.subject
    };
    setChapterActions([
      ...chapterActions,
      {
        type: StackActionType.CREATE,
        payload: createChapterItem
      }
    ]);
  };

  const editChapterItem = (index: number, name: string) => {
    const clonedChapters = [...chapters];
    const chapter = clonedChapters[index];
    const updateChapterItem = {
      batchId: activeBatch._id,
      batchfriendlyname: activeBatch.batchfriendlyname,
      chaptername: chapter.name,
      boardname: activeCourse.board,
      classname: activeCourse.className,
      subjectname: activeCourse.subject,
      newchaptername: name !== undefined ? name : ''
    };
    setChapterActions([
      ...chapterActions,
      {
        type: StackActionType.UPDATE,
        payload: updateChapterItem
      }
    ]);
    clonedChapters.splice(index, 1, getChapterStub({ name, isMaster: false }));
    setChapters(clonedChapters);
  };

  const removeChapterItem = (index: number) => {
    const clonedChapters = [...chapters];
    const chapter = clonedChapters[index];
    const deleteChapterItem = {
      batchId: activeBatch._id,
      batchfriendlyname: activeBatch.batchfriendlyname,
      chaptername: chapter.name,
      boardname: activeCourse.board,
      classname: activeCourse.className,
      subjectname: activeCourse.subject
    };

    setChapterActions([
      ...chapterActions,
      {
        type: StackActionType.DELETE,
        payload: deleteChapterItem
      }
    ]);

    clonedChapters.splice(index, 1);
    setChapters(clonedChapters);
  };

  const orderChapters = (chapters: Chapter[]) => {
    setChapters(chapters);
    setIsReorderChapter(true);
  };

  const handleSubmitChapterActions = async () => {
    if (activeCourse === undefined) return;
    if (!activeBatch) return

    let actions = [...chapterActions];

    // if delete action presents then remove create and update actions
    let deleteActionArr = actions.filter(
      (action) => action.type === StackActionType.DELETE
    );
    deleteActionArr.forEach((el) => {
      let updateActions = actions.filter(
        (data) =>
          data.type === StackActionType.UPDATE &&
          data.payload.newchaptername === el.payload.chaptername &&
          data.payload.batchId === el.payload.batchId
      );

      // remove create actions if update and delete present
      let createIndex;
      updateActions.forEach((updatedEl) => {
        createIndex = actions.findIndex(
          (data) =>
            data.type === StackActionType.CREATE &&
            data.payload.chaptername === updatedEl.payload.chaptername &&
            data.payload.batchId === el.payload.batchId
        );
        if (createIndex !== -1) {
          actions.splice(createIndex, 1);
        }
      });

      // remove update & delete action if delete present
      let updateIndex = actions.findIndex(
        (data) =>
          data.type === StackActionType.UPDATE &&
          data.payload.newchaptername === el.payload.chaptername &&
          data.payload.batchId === el.payload.batchId
      );
      if (updateIndex !== -1) {
        actions.splice(updateIndex, 1);
        let index = actions.findIndex(
          (data) =>
            data.type === StackActionType.DELETE &&
            data.payload.chaptername === el.payload.chaptername &&
            data.payload.batchId === el.payload.batchId
        );
        actions.splice(index, 1);
      }

      // remove delete action if both create and update present
      if (createIndex !== -1 && updateIndex !== -1) {
        let deleteIndex = actions.findIndex(
          (action) => action.payload.chaptername === el.payload.chaptername &&
          action.payload.batchId === el.payload.batchId
        );
        actions.splice(deleteIndex, 1);
      }

      // remove delete & create action if delete present
      let deleteCreateIndex = actions.findIndex(
        (data) =>
          data.type === StackActionType.CREATE &&
          data.payload.chaptername === el.payload.chaptername &&
          data.payload.batchId === el.payload.batchId
      );
      if (deleteCreateIndex !== -1) {
        actions.splice(deleteCreateIndex, 1);
        let index = actions.findIndex(
          (data) =>
            data.type === StackActionType.DELETE &&
            data.payload.chaptername === el.payload.chaptername &&
            data.payload.batchId === el.payload.batchId
        );
        actions.splice(index, 1);
      }
    });

    actions.forEach(async (action) => {
      try {
        if (StackActionType.CREATE === action.type) {
          await createCourseChapter(action.payload);
        } else if (StackActionType.UPDATE === action.type) {
          // @ts-ignore
          await updateChapter(action.payload);
        } else if (StackActionType.DELETE === action.type) {
          await deleteChapter(action.payload);
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    });
    if (isReorderChapter) {
      await reorderChapters(chapters);
    }
    setChapterActions([]);
    setIsReorderChapter(false);
  };

  const updateCourse = async () => {
    if (activeCourse === undefined) return;

    // TODO: Check usage of this code => removing multiple create quiz
    // const uniqueLastOperationOnItem = uniqBy(stackActions.reverse(), (v) => {
    //   if (ContentType.QUIZ === v.payload.type) {
    //     return JSON.stringify([v.type]);
    //   }

    //   return JSON.stringify([
    //     v.payload.type,
    //     v.payload.data.name ? v.payload.data.name : v.payload.data
    //   ]);
    // }).reverse();

    const uniqueLastOperationOnItem = stackActions;

    if (uniqueLastOperationOnItem.length === 0) {
      enqueueSnackbar('No Content was Added', { variant: 'warning'});
    }

    try {
      for (let i = 0; i < uniqueLastOperationOnItem.length; i++) {
        const action = uniqueLastOperationOnItem[i];
        console.log(action)
        if (StackActionType.CREATE === action.type) {
          const stackActionPayloadType = action.payload.type;
          console.log(stackActionPayloadType)
          if (
            stackActionPayloadType === ContentType.DOCUMENT ||
            stackActionPayloadType === ContentType.IMAGE ||
            stackActionPayloadType === ContentType.SLIDE ||
            stackActionPayloadType === ContentType.VIDEO
          ) {
            const file = action.payload.data as File;

            // TODO: Pass course & chapter from with action.
            const serverBucket = await fetchOrgChapterContentUploadUrl({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contenttype: file.type,
              contentlength: file.size
            });

            await uploadFileOnUrl(serverBucket.url, file);
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Saved Content ' + stackActionPayloadType + ' on S3' );

            let newContent = {
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contenttype: action.payload.type,
              contentlength: file.size,
              contentname: new Date().getTime() + '_stamp_' + file.name,
              uuid: serverBucket.uuid
            }

            if(action.payload.subtype) {
              newContent = {...newContent, ...{contentsubtype: action.payload.subtype}}
            }

            // TODO: Pass course & chapter from with action.
            const response = await createOrgChapterContentFile(newContent);
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Added Content ' + stackActionPayloadType + ' on db' );
            if(response.message === 'Success') {
              enqueueSnackbar('Saved Successfully', { variant: 'success'});
            }
          } else if (stackActionPayloadType === ContentType.EMBED_LINK) {
            const embedLink = action.payload.data as string;

            // TODO: Pass course & chapter from with action.
            const response = await createChapterContentRequest({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contenttype: action.payload.type,
              contentname: embedLink,
              title: 'string',
              duration: 1,
              marks: 2,
              uuid: embedLink
            });
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Added Content ' + stackActionPayloadType + ' on db' );
            if(response.message === 'Success') {
              enqueueSnackbar('Saved Successfully', { variant: 'success'});
            }
          } else if (stackActionPayloadType === ContentType.QUIZ) {
            const questions = action.payload.data as QuestionApp[];
            // Change This
            // const newQuizes = chapters[activeChapterIndex].contents.filter(
            //   (quiz) =>
            //     quiz.chapter === activeChapter.name &&
            //     quiz.contenttype === ContentType.QUIZ
            // );

            // TODO: Pass course & chapter from with action.
            const response = await createChapterContentRequest({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contenttype: action.payload.type,
              contentname:
                Math.floor(100000000 + Math.random() * 900000000) +
                '_stamp_' +
                ContentType.QUIZ,
              title: action.payload.quizMeta?.title,
              duration: action.payload.quizMeta?.duration,
              marks: action.payload.quizMeta?.marks,
              questions: questions.map((question) =>
                generateQuestionSchemaForServer(question)
              )
            });
            if (response.message === 'Success') {
              eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Added Content ' + stackActionPayloadType + ' on db' );
              getChapterContent();
              enqueueSnackbar('Saved Successfully', { variant: 'success'});
            }
          }
        } else if (StackActionType.DELETE === action.type) {
          const stackActionPayloadType = action.payload.type;

          if (
            stackActionPayloadType === ContentType.DOCUMENT ||
            stackActionPayloadType === ContentType.IMAGE ||
            stackActionPayloadType === ContentType.SLIDE ||
            stackActionPayloadType === ContentType.VIDEO
          ) {
            const filename = action.payload.data as string;

            const response = await deleteOrgChapterContent({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contentname: filename
            });
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Removed Content ' + stackActionPayloadType + ' from db' );
            if(response.message === 'Success') {
              enqueueSnackbar('Deleted Successfully', { variant: 'success'});
            }
          } else if (stackActionPayloadType === ContentType.EMBED_LINK) {
            const embedLink = action.payload.data as string;

            const response = await deleteOrgChapterContent({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contentname: embedLink
            });
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Removed Content ' + stackActionPayloadType + ' from db' );
            if(response.message === 'Success') {
              enqueueSnackbar('Deleted Successfully', { variant: 'success'});
            }
          } else if (stackActionPayloadType === ContentType.QUIZ) {
            const contentName = action.payload.data as string;
            const response = await deleteOrgChapterContent({
              boardname: action.payload.boardname,
              chaptername: action.payload.chaptername,
              classname: action.payload.classname,
              subjectname: action.payload.subjectname,
              contentname: contentName
            });
            eventTracker(GAEVENTCAT.contentManagement, 'Organization Content Page', 'Removed Content ' + stackActionPayloadType + ' from db' );
            if(response.message === 'Success') {
              enqueueSnackbar('Deleted Successfully', { variant: 'success'});
            }
          }
        } else if (StackActionType.UPDATE === action.type) {
          const questions = action.payload.data as QuestionApp[];

          // TODO: Pass course & chapter from with action.
          const response = await updateChapterContentRequest({
            boardname: action.payload.boardname,
            chaptername: action.payload.chaptername,
            classname: action.payload.classname,
            subjectname: action.payload.subjectname,
            contenttype: action.payload.type,
            contentname: quizMeta.contentname,
            title: quizMeta.title,
            duration: quizMeta.duration,
            marks: quizMeta.marks,
            questions: questions.map((question) =>
              generateQuestionSchemaForServer(question)
            )
          });
          setServerError(response.message);
        }
      }

      // Destroy previous stack actions so nothing gets done twice.
      setStackActions([]);
      setUpdateVersion(prev=>prev+1)
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        setStackActions([]);
        enqueueSnackbar(error.response?.data.message, { variant: 'warning'});
      }
    }
  };


  // const createChapterContent = (
  //   chapterIndex: number,
  //   contentType: ContentType,
  //   data: any,
  //   boardname:string,
  //   classname:string,
  //   subjectname:string,
  //   chaptername:string,
  //   contentSubtype?: string | null,
  // ) => {
    // let chaptersList = [...chapters];

    // switch (contentType) {
    //   case ContentType.DOCUMENT:
    //   case ContentType.IMAGE:
    //   case ContentType.SLIDE:
    //   case ContentType.VIDEO:
    //     const file = data as File;

    //     let newContentRequest = {
    //       contentname: file.name,
    //       contentType: contentType,
    //       boardname,
    //       classname,
    //       subjectname,
    //       chaptername
    //     }

    //     if(contentSubtype) {
    //       newContentRequest = {...newContentRequest, ...{contentsubtype: contentSubtype}}
    //     }

    //     contentRequests.push(newContentRequest)
        


    //     chaptersList[chapterIndex].contents = [
    //       ...chaptersList[chapterIndex].contents,
    //       newContent
    //     ];

    //     break;
    //   case ContentType.EMBED_LINK:
    //     const embedLink = data as string;
    //     contentRequests.push({
    //       contentname: embedLink,
    //       contentType: contentType,
    //       uuid: embedLink,
    //       chaptername,
    //       boardname,
    //       classname,
    //       subjectname
    //     })
        
        
    //     chaptersList[chapterIndex].contents = [
    //       ...chaptersList[chapterIndex].contents,
    //       {
    //         contentname: embedLink,
    //         contenttype: contentType,
    //         uuid: embedLink,
    //         chaptername,
    //         boardname,
    //         classname,
    //         subjectname
    //       }
    //     ];

    //     break;
    //   case ContentType.QUIZ:
    //     const questions = data as QuestionApp[];
        

    //     contentRequests.push({
    //       chaptername,
    //       contentname:
    //         Math.floor(100000000 + Math.random() * 900000000) +
    //         '_stamp_' +
    //         ContentType.QUIZ,
    //       contentType: contentType,
    //       questions: questions,
    //       title : quizMeta.title,
    //       duration : quizMeta.duration,
    //       marks : quizMeta.marks,
    //       boardname,
    //       classname,
    //       subjectname
    //     })
    //     chaptersList[chapterIndex].contents = [
    //       ...chaptersList[chapterIndex].contents,
          
    //     ];

    //     const chapterQuizContentIndex =
    //       chaptersList[chapterIndex].contents.length - 1;
    //     chaptersList[chapterIndex].contents[
    //       chapterQuizContentIndex
    //     ].questions = questions;
    //     chaptersList[chapterIndex].contents[chapterQuizContentIndex].title =
    //       quizMeta.title;
    //     chaptersList[chapterIndex].contents[chapterQuizContentIndex].duration =
    //       quizMeta.duration;
    //     chaptersList[chapterIndex].contents[chapterQuizContentIndex].marks =
    //       quizMeta.marks;
    //     break;
    //   default:
    //     throw new Error(`Invalid content type [${contentType}].`);
    // }

    // setChapters(chaptersList);
  // };

  // const removeChapterContent = async (
  //   chapterIndex: number,
  //   contentType: ContentType,
  //   data: any,
  //   boardname:string,
  //   classname:string,
  //   subjectname:string,
  //   chaptername:string
  // ) => {
    // let chaptersList = [...chapters];

    // switch (contentType) {
    //   case ContentType.DOCUMENT:
    //   case ContentType.IMAGE:
    //   case ContentType.SLIDE:
    //   case ContentType.VIDEO:
    //   const filename = data as string;

    //     contentRequests = contentRequests.filter((req)=>{
    //       req.contentname !==filename && req.contentType!==contentType && req.chaptername!==chaptername
    //     })

    //     chaptersList[chapterIndex].contents = chaptersList[
    //       chapterIndex
    //     ].contents.filter(
    //       (chapter) =>
    //         chapter.contenttype !== contentType ||
    //         chapter.contentname !== filename
    //     );

    //     break;
    //   case ContentType.EMBED_LINK:
    //     const embedLink = data as string;
    //     contentRequests = contentRequests.filter((req)=>{
    //       req.contentname !==filename && req.contentType!==contentType && req.chaptername!==chaptername
    //     })
    //     chaptersList[chapterIndex].contents = chaptersList[
    //       chapterIndex
    //     ].contents.filter(
    //       (chapter) =>
    //         chapter.contenttype !== contentType || chapter.uuid !== embedLink
    //     );

    //     break;
    //   case ContentType.QUIZ:
    //     const contentName = data as string;
    //     contentRequests = contentRequests.filter((req)=>{
    //       req.contentname !==contentName && req.contentType!==contentType && req.chaptername!==chaptername
    //     })
    //     chaptersList[chapterIndex].contents = chaptersList[
    //       chapterIndex
    //     ].contents.filter((chapter) => chapter.contentname !== contentName);
    //     break;
    //   default:
    //     throw new Error(`Invalid content type [${contentType}].`);
    // }
    // setChapters(chaptersList);
  // };

  // const updateChapterContent = (
  //   chapterIndex: number,
  //   contentType: ContentType,
  //   data: any,
  //   boardname:string,
  //   classname:string,
  //   subjectname:string,
  //   chaptername:string
  // ) => {
    // let chaptersList = [...chapters];

    // switch (contentType) {
    //   case ContentType.QUIZ:
    //     const questions = data as QuestionApp[];

    //     contentRequests = contentRequests.map((req)=>{
    //       if(req.contentType==contentType && req.chaptername== chaptername && req.contentname == quizMeta.contentname) {
    //         return {
    //             ...req,
    //             chaptername,
    //             contentType: contentType,
    //             contentname: quizMeta.contentname,
    //             questions: questions,
    //             title : quizMeta.title,
    //             duration : quizMeta.duration,
    //             marks : quizMeta.marks,
    //             boardname,
    //             classname,
    //             subjectname
    //         }
    //       } 
    //       else{
    //         return req
    //       }
    //     })

    //     const chapterQuizContentIndex = chaptersList[
    //       chapterIndex
    //     ].contents.findIndex(
    //       (content) => ContentType.QUIZ === content.contenttype
    //     );

    //     if (chapterQuizContentIndex === -1) return;

    //     if (
    //       chaptersList &&
    //       chaptersList[chapterIndex] &&
    //       chaptersList[chapterIndex].contents &&
    //       chaptersList[chapterIndex].contents[chapterQuizContentIndex] &&
    //       chaptersList[chapterIndex].contents[chapterQuizContentIndex].questions
    //     ) {
    //       chaptersList[chapterIndex].contents[
    //         chapterQuizContentIndex
    //       ].questions = questions;
    //     }

    // break;
    //   default:
    //     throw new Error(`Invalid content type [${contentType}].`);
    // }

    // setChapters(chaptersList);
  // };

  const handleEmittedStackAction = (action: StackActionItem<ContentType>) => {
    setServerError('');
    setStackActions((currentActions) => [...currentActions, action]);


    // switch (action.type) {
    //   case StackActionType.CREATE:
    //     createChapterContent(
    //       activeChapterIndex,
    //       action.payload.type,
    //       action.payload.data,
    //       (action.payload.subtype) ? action.payload.subtype : 'Uncategorized',
    //       action.payload.boardname,
    //       action.payload.classname,
    //       action.payload.subjectname,
    //       action.payload.chaptername,
          
    //     );
    //     break;
    //   case StackActionType.DELETE:
    //     removeChapterContent(
    //       activeChapterIndex,
    //       action.payload.type,
    //       action.payload.data,
    //       action.payload.boardname,
    //       action.payload.classname,
    //       action.payload.subjectname,
    //       action.payload.chaptername,
    //     );
    //     break;
    //   case StackActionType.UPDATE:
    //     updateChapterContent(
    //       activeChapterIndex,
    //       action.payload.type,
    //       action.payload.data,
    //       action.payload.boardname,
    //       action.payload.classname,
    //       action.payload.subjectname,
    //       action.payload.chaptername,
    //     );
    //     break;
    //   default:
        // throw new Error(`Invalid bucket action type [${action.type}].`);
    // }
  };


  return (
    <div>
      <MiniDrawer>

      <Container maxWidth={false} style={{margin: '20px 0px'}}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <CourseContentSidebar
              profile={profile}
              masterChapters={masterChapters}
              activeChapterIndex={activeChapterIndex}
              chapters={chapters}
              handleClickOnChapter={(chapterIndex) =>
                setActiveChapterIndex(chapterIndex)
              }
              handleAddMasterChapter={createChapterFromMaster}
              handleAddChapter={createChapter}
              handleUpdateChapters={(chapters) => orderChapters(chapters)}
              activeCourseIndex={activeCourseIndex}
              courses={courses}
              handleSelectCourse={(courseIndex) =>
                setActiveCourseIndex(() => courseIndex)
              }
              activeBatchIndex={activeBatchIndex}
              batches={batches}
              handleSelectBatch={(batchIndex) => 
                setActiveBatchIndex(() => batchIndex)
              }
              editChapterItem={(chapterIndex, name) =>
                editChapterItem(chapterIndex, name)
              }
              removeChapterItem={(chapterIndex) =>
                removeChapterItem(chapterIndex)
              }
              handleSubmitChapterActions={handleSubmitChapterActions}
            />
          </Grid>

          <Grid item xs={12} md={8}>
            <Scrollbars autoHeight autoHeightMax="78vH" autoHeightMin="78vH"
              style={{
                backgroundColor: "white",
                boxShadow: '0px 4px 20px rgb(7 40 137 / 4%)',
                borderRadius: '5px',
                padding: '10px 0px'
              }}
            >
              <Box padding="10px 20px" borderBottom="1px solid #DCDFF1">
                <Grid container alignItems="center">
                  <Grid item xs={12} md={6}>
                    <Box component="h2" className={classes.courseHeading}>
                      {activeChapter &&
                        activeChapter.name &&
                        `Chapter ${activeChapterIndex + 1} - ${
                          activeChapter.name
                        }`}
                    </Box>
                    <FormHelperText className={classes.helperText}>
                      Select how this flow should be shown
                    </FormHelperText>
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <Box
                      display="flex"
                      justifyContent="flex-end"
                      className={classes.saveBtn}
                    >
                      {
                        batches.length === 0 ? '' : 
                        <Button
                        disableElevation
                        color="primary"
                        size="large"
                        variant="contained"
                        onClick={updateCourse}
                      >
                        Save flow
                      </Button>
                      }
                    </Box>
                    <Box display="flex" justifyContent="flex-end" color="red">
                      {serverError}
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              <Box padding="20px">
                <Box display="flex" justifyContent="space-between">
                  <SelectContentTypeButton
                    icon={<img src={PhotoIcon} alt="upload" />}
                    isActive={activeContentType === ContentType.IMAGE}
                    onClick={() => setActiveContentType(ContentType.IMAGE)}
                  >
                    Images
                  </SelectContentTypeButton>

                  <SelectContentTypeButton
                    icon={<PdfIcon />}
                    isActive={activeContentType === ContentType.SLIDE}
                    onClick={() => setActiveContentType(ContentType.SLIDE)}
                  >
                    PDF
                  </SelectContentTypeButton>

                  <SelectContentTypeButton
                    icon={<img src={DocumentsIcon} alt="upload" />}
                    isActive={activeContentType === ContentType.DOCUMENT}
                    onClick={() => setActiveContentType(ContentType.DOCUMENT)}
                  >
                    Docs
                  </SelectContentTypeButton>

                  <SelectContentTypeButton
                    icon={<img src={VideoIcon} alt="upload" />}
                    isActive={activeContentType === ContentType.VIDEO}
                    onClick={() => setActiveContentType(ContentType.VIDEO)}
                  >
                    Video
                  </SelectContentTypeButton>

                  <SelectContentTypeButton
                    icon={<img src={LinkIcon} alt="upload" />}
                    isActive={activeContentType === ContentType.EMBED_LINK}
                    onClick={() => setActiveContentType(ContentType.EMBED_LINK)}
                  >
                    Embed Link
                  </SelectContentTypeButton>
                </Box>

                {activeChapter && (
                  <ChapterContent
                    profile={profile}
                    chapter={activeChapter}
                    updateVersion={updateVersion}
                    removeStackAction={removeStackAction}
                    activeContentType={activeContentType}
                    stackActions={stackActions}
                    emitStackAction={handleEmittedStackAction}
                    activeCourse={activeCourse}
                    chapters={chapters}
                    setChapters={setChapters}
                    activeChapterIndex={activeChapterIndex}
                    quizMeta={quizMeta}
                    setQuizMeta={setQuizMeta}
                  />
                )}
              </Box>
            </Scrollbars>
          </Grid>
        </Grid>
      </Container>
      </MiniDrawer>
    </div>
  );
};

export default OrgCourses;
