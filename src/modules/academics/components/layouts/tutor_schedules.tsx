import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Box, Container, Grid, Paper, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Add as AddIcon,
  BorderColor as EditIcon,
  Delete as DeleteIcon,
  Schedule as ScheduleIcon
} from '@material-ui/icons';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Appointments,
  Resources,
  Scheduler,
  WeekView
} from '@devexpress/dx-react-scheduler-material-ui';
import { generateSchedulerSchema } from '../../../common/helpers';
import {
  updateTutorSchedule,
  deleteTutorSchedule,
  fetchBatchDetails,
  fetchSchedulesList
} from '../../../common/api/academics';
import { Tutor } from '../../../common/contracts/user';
import { AppointmentSchedule, Schedule } from '../../contracts/schedule';
import Calender from '../../../../assets/images/schedule-calendar.png';
import StudentWithMonitor from '../../../../assets/images/schedule-student.png';
import Button from '../../../common/components/form_elements/button';
import MiniDrawer from '../../../common/components/sidedrawer';
import ConfirmationModal from '../../../common/components/confirmation_modal';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../../common/helpers';
import { Redirect } from 'react-router-dom';
import EditTutorScheduleModal from '../edit_tutor_schedule_modal';
import { fontOptions } from '../../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    todayCell: {
      backgroundColor: theme.palette.background.paper,
      border: 0
    },
    weekdayCell: {
      backgroundColor: theme.palette.background.default,
      border: 0
    },
    today: {
      backgroundColor: theme.palette.background.paper,
      textTransform: 'uppercase',

      '& .Cell-highlightedText-100': {
        color: '#333333',
        fontWeight: fontOptions.weight.normal
      }
    },
    weekday: {
      backgroundColor: theme.palette.background.default,
      textTransform: 'uppercase',
      color: '#333333'
    },
    paperContainer: {
      background: '#F9F9F9',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      borderRadius: '4px',

      '& .makeStyles-stickyElement-68': {
        background: '#F9F9F9'
      }
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#666666'
    },
    addBtn: {
      '& a': {
        fontFamily: fontOptions.family,
        fontWeight: 'bold',
        textTransform: 'uppercase'
      },
      '& svg': {
        marginRight: '5px'
      }
    },
    className: {
      fontFamily:  fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: '#FFFFFF',
      margin: '0 10px 0 0'
    },
    subjectName: {
      fontWeight: fontOptions.weight.bold,
      fontSize:fontOptions.size.small,
      fontFamily:fontOptions.family,
      color: '#FFFFFF',
      margin: '0 10px 0 0'
    },
    clockIcon: {
      color: '#fff'
    },
    scheduleDuration: {
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      color: '#FFFFFF'
    },
    studentsList: {
      fontFamily:  fontOptions.family,
      fontSize: fontOptions.size.small,
      letterSpacing: '0.25px',
      color: '#FFFFFF'
    },
    editBtn: {
      '& button': {
        border: '2px solid #FFFFFF',
        borderRadius: '5px',
        fontFamily:  fontOptions.family,
        fontWeight: fontOptions.weight.bolder,
        fontSize: fontOptions.size.small,
        letterSpacing: '1px',
        color: '#FFFFFF'
      },
      '& svg': {
        marginLeft: '8px'
      },
      '& img': {
        marginLeft: '8px',
        height: '20px',
        width: '17px'
      }
    }
  })
);

const TimeTableCell: FunctionComponent<WeekView.TimeTableCellProps> = (
  props
) => {
  const { startDate } = props;

  const date = new Date(startDate as Date);

  const classes = useStyles();

  if (date.getDate() === new Date().getDate()) {
    return <WeekView.TimeTableCell {...props} className={classes.todayCell} />;
  }

  return <WeekView.TimeTableCell {...props} className={classes.weekdayCell} />;
};

const DayScaleCell: FunctionComponent<WeekView.DayScaleCellProps> = (props) => {
  const { today } = props;

  const classes = useStyles();

  if (today) {
    return <WeekView.DayScaleCell {...props} className={classes.today} />;
  }

  return <WeekView.DayScaleCell {...props} className={classes.weekday} />;
};

interface ScheduleSummaryProps {
  item: AppointmentSchedule;
  deleteSchedule: () => any;
  handleUpdateSchedule: (schedule: Schedule) => any;
}

const ScheduleSummary: FunctionComponent<ScheduleSummaryProps> = ({
  item,
  deleteSchedule,
  handleUpdateSchedule
}) => {
  const [students, setStudents] = useState('');
  const [redirectTo, setRedirectTo] = useState('');
  const [editScheduleModal, setEditScheduleModal] = useState(false);

  const classes = useStyles();

  useEffect(() => {
    (async () => {
      try {
        const batch = await fetchBatchDetails({
          batchfriendlyname: item.schedule.batch
            ? item.schedule.batch.batchfriendlyname
            : ''
        });

        setStudents(
          batch.students.map((student) => student.studentName).join(', ')
        );
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [item]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <Box bgcolor="#4285F4" borderRadius="7px">
      <Grid container alignItems="center">
        <Grid item xs={12} md={6}>
          <Box padding="18px 0 18px 18px" display="flex" alignItems="center">
            <img src={StudentWithMonitor} alt="Student with Monitor" />

            <Box marginLeft="15px">
              <Box display="flex" marginBottom="8px">
                <Box display="flex" alignItems="center">
                  <Box component="h3" className={classes.className}>
                    {item.schedule.batch && item.schedule.batch.classname}
                  </Box>
                  <Box component="h3" className={classes.subjectName}>
                    [{item.schedule.batch && item.schedule.batch.subjectname}]
                  </Box>
                </Box>

                <Box display="flex" alignItems="center">
                  <Box
                    display="flex"
                    alignItems="center"
                    marginLeft="15px"
                    marginRight="5px"
                  >
                    <ScheduleIcon className={classes.clockIcon} />
                  </Box>

                  <Typography className={classes.scheduleDuration}>
                    {item.schedule.fromhour} - {item.schedule.tohour}
                  </Typography>
                </Box>
              </Box>

              <Box>
                <Typography className={classes.studentsList}>
                  {students}
                </Typography>
              </Box>
            </Box>
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <Box display="flex" justifyContent="flex-end" padding="18px">
            <Box marginRight="15px" className={classes.editBtn}>
              <Button
                disableElevation
                variant="outlined"
                color="default"
                size="large"
                onClick={deleteSchedule}
              >
                <Box component="span" marginRight="5px">
                  Delete Schedule
                </Box>

                <DeleteIcon fontSize='small' />
              </Button>
            </Box>

            <Box className={classes.editBtn}>
              <Button
                disableElevation
                variant="outlined"
                color="default"
                size="large"
                onClick={() => {
                  eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Started Updation of Tutor Schedule');
                  setEditScheduleModal(true);
                }}
              >
                <Box component="span" marginRight="5px">
                  Edit Schedule
                </Box>
                <EditIcon fontSize='small' />
              </Button>
            </Box>
          </Box>
        </Grid>
      </Grid>
      <EditTutorScheduleModal
        openModal={editScheduleModal}
        handleClose={() => setEditScheduleModal(false)}
        selectedSchedule={item}
        handleUpdateSchedule={handleUpdateSchedule}
      />
    </Box>
  );
};

const generateInstancesSchema = (schedules: Schedule[]) => {
  const subjects = schedules.map((schedule) =>
    schedule.batch ? schedule.batch.subjectname : ''
  );

  return [...Array.from(subjects)].map((subject) => ({
    id: subject,
    text: subject
  }));
};

interface Props {
  profile: Tutor;
}

const TutorSchedules: FunctionComponent<Props> = ({ profile }) => {
  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [
    selectedSchedule,
    setSelectedSchedule
  ] = useState<AppointmentSchedule | null>(null);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [showScheduleSummary, setShowScheduleSummary] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');

  const classes = useStyles();

  const fetchSchedule = async () => {
    try {
      const schedulesList = await fetchSchedulesList();

      // set selected schedule with updated data
      schedulesList.forEach((schedule) => {
        if (schedule._id === selectedSchedule?.schedule._id) {
          let currentSchedule = selectedSchedule?.schedule;
          currentSchedule = {
            ...currentSchedule,
            dayname: schedule.dayname,
            fromhour: schedule.fromhour,
            tohour: schedule.tohour
          };
          setSelectedSchedule({
            ...selectedSchedule,
            schedule: currentSchedule as Schedule
          } as AppointmentSchedule);
        }
      });
      setSchedules(schedulesList);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  useEffect(() => {
    fetchSchedule();
    // eslint-disable-next-line
  }, [profile.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const deleteSchedule = async (item: AppointmentSchedule) => {
    const schedulesList = [...schedules];
    const scheduleIndex = schedulesList.findIndex(
      (schedule) =>
        schedule.batch === item.schedule.batch &&
        schedule.dayname === item.schedule.dayname &&
        schedule.fromhour === item.schedule.fromhour &&
        schedule.mobileNo === item.schedule.mobileNo &&
        schedule.tohour === item.schedule.tohour
    );
    const clonedSchedule = schedulesList[scheduleIndex];

    try {
      schedulesList.splice(scheduleIndex, 1);

      setSchedules(schedulesList);
      setSelectedSchedule(null);
      setOpenConfirmationModal(false);
      setShowScheduleSummary(false);
      await deleteTutorSchedule({
        dayname: item.schedule.dayname,
        fromhour: item.schedule.fromhour
      });
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        schedulesList.push(clonedSchedule);
        setSchedules(schedulesList);
      }
    }
  };

  const currentDate = new Date();

  const schedulerData = generateSchedulerSchema(schedules);

  const resources = [
    {
      fieldName: 'subject',
      title: 'Subjects',
      instances: generateInstancesSchema(schedules)
    }
  ];

  const handleUpdateSchedule = async (schedule: Schedule) => {
    try {
      await updateTutorSchedule({
        scheduleId: schedule._id,
        dayname: schedule.dayname,
        fromhour: schedule.fromhour,
        tohour: schedule.tohour
      });
      eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Updated Tutor Schedule');
      fetchSchedule();
      setShowScheduleSummary(false);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box marginY="20px">
          <Paper className={classes.paperContainer}>
            <Box padding="20px">
              <Box marginBottom="20px">
                <Grid container>
                  <Grid item xs={12} md={6}>
                    <Box display="flex" alignItems="center">
                      <img src={Calender} alt="Calender" />

                      <Box marginLeft="15px">
                        <Typography component="span" color="secondary">
                          <Box component="h3" className={classes.heading}>
                            Class Schedule
                          </Box>
                        </Typography>
                      </Box>
                    </Box>
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <Box display="flex" justifyContent="flex-end">
                      <Box marginRight="10px" className={classes.addBtn}>
                        <Button
                          disableElevation
                          variant="contained"
                          color="primary"
                          size="large"
                          component={RouterLink}
                          to={`/profile/tutor/schedules/create`}
                        >
                          <Box display="flex" alignItems="center">
                            <AddIcon />
                          </Box>{' '}
                          Add Schedule
                        </Button>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              <Box
                maxHeight="500px"
                borderBottom="1px solid rgba(0, 0, 0, 0.1)"
                style={{ overflowY: 'auto' }}
              >
                <Scheduler data={schedulerData}>
                  <ViewState currentDate={currentDate} />
                  <WeekView
                    timeTableCellComponent={TimeTableCell}
                    dayScaleCellComponent={DayScaleCell}
                    startDayHour={6}
                    endDayHour={24}
                  />
                  <Appointments
                    appointmentComponent={(props) => (
                      <Appointments.Appointment
                        {...props}
                        onClick={(schedule) => {
                          setShowScheduleSummary(true);
                          setSelectedSchedule(schedule.data);
                        }}
                      />
                    )}
                  />
                  <Resources data={resources} mainResourceName="subject" />
                </Scheduler>
              </Box>
            </Box>
            {showScheduleSummary
              ? selectedSchedule && (
                  <ScheduleSummary
                    item={selectedSchedule}
                    deleteSchedule={() => setOpenConfirmationModal(true)}
                    handleUpdateSchedule={handleUpdateSchedule}
                  />
                )
              : ''}
          </Paper>
        </Box>
      </Container>
      {selectedSchedule && (
        <ConfirmationModal
          header="Delete Schedule"
          helperText="Are you sure you want to delete?"
          openModal={openConfirmationModal}
          onClose={() => setOpenConfirmationModal(false)}
          handleDelete={() => deleteSchedule(selectedSchedule)}
        />
      )}
      </MiniDrawer>
    </div>
  );
};

export default TutorSchedules;
