import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  IconButton,
  InputLabel,
  MenuItem,
  Select
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Add as AddIcon } from '@material-ui/icons';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import Scrollbars from 'react-custom-scrollbars';

import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult
} from 'react-beautiful-dnd';
import { Chapter } from '../contracts/chapter';
import { Course } from '../contracts/course';
import AddChapterModal from '../components/add_chapter_modal';
import ChapterListItem from '../components/chapter_list_item';
import Button from '../../common/components/form_elements/button';
import Tooltip from '../../common/components/tooltip';
import { Batch } from '../contracts/batch';
import AddMasterChapterModal from './add_master_chapter_modal';
import { fontOptions } from '../../../theme';
import UploadSyllabusModal from './upload_syllabus_modal'
import {useSnackbar} from "notistack"
import FileDownload from 'js-file-download';
import { fetchUploadUrlForSyllabus, uploadFileOnUrl, updateSyllabus, fetchDownloadUrlForSyllabus, downloadFile } from '../../common/api/academics'
import { User } from '../../common/contracts/user';
import { isParent, isStudent } from '../../common/helpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    addChapterButton: {
      background: '#FFF',
      border: '2px solid #F2F2F2',
      color: theme.palette.secondary.main,
      position: 'absolute',
      right: '20px',
      top: '-25px',

      '&:hover': {
        background: '#FFF'
      }
    },
    courseHeading: {
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.bold,
      lineHeight: '21px',
      color: '#10182F'
    },
    helperText: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.normal,
      color: '#979797',
      margin: '15px 0'
    }
  })
);

function reorder(list: Chapter[], startIndex: number, endIndex: number) {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);

  result.splice(endIndex, 0, removed);

  return result;
}

interface Props {
  profile : User
  masterChapters: Chapter[];
  activeChapterIndex: number;
  chapters: Chapter[];
  handleClickOnChapter: (chapterIndex: number) => any;
  handleAddMasterChapter: (submit: boolean) => any;
  handleAddChapter: (name: string) => any;
  handleUpdateChapters: (chapters: Chapter[]) => any;
  activeCourseIndex: number;
  courses: Course[];
  handleSelectCourse: (courseIndex: number) => any;
  activeBatchIndex: number;
  batches: Batch[];
  handleSelectBatch: (batchIndex: number) => any;
  editChapterItem: (index: number, name: string) => any;
  removeChapterItem: (index: number) => any;
  handleSubmitChapterActions: () => void;
}

const CourseContentSidebar: FunctionComponent<Props> = ({
  profile,
  masterChapters,
  activeChapterIndex,
  chapters,
  handleClickOnChapter,
  handleAddMasterChapter,
  handleAddChapter,
  handleUpdateChapters,
  activeCourseIndex,
  courses,
  handleSelectCourse,
  activeBatchIndex,
  batches,
  handleSelectBatch,
  editChapterItem,
  removeChapterItem,
  handleSubmitChapterActions
}) => {
  const { enqueueSnackbar } = useSnackbar();

  const [openAddChapterModal, setOpenAddChapterModal] = useState(false);
  const [openAddMasterChapterModal, setOpenAddMasterChapterModal] = useState(false);
  const [openUploadSyllabusModal, setOpenUploadSyllabusModal] = useState(false);
  const [droppedFiles, setDroppedFiles] = useState<File[]>([]);
  const [addchaptererror, setAddchaptererror] = useState<boolean>(false);

  useEffect(() => {
    if(batches[activeBatchIndex] && batches[activeBatchIndex].syllabus) {
      const file = new File([""], "syllabus.pdf", {type: 'application/pdf'})
      setDroppedFiles([file])
    }
  }, [batches.length]);



  const addChapter = async (name: string) => {
    if(name.length > 4) {
      setOpenAddChapterModal(false);
      handleAddChapter(name);
    } else {
      setAddchaptererror(true);
    }
  };

  const addMasterChapter = async (submit: boolean) => {
    setOpenAddMasterChapterModal(false);
    handleAddMasterChapter(submit);
  };

  const uploadSyllabus = async() => {
    try {
      const droppedFile = droppedFiles[0];
      if(droppedFile.size !== 0) {
        const fileType = droppedFile.type
        const awsBucket = await fetchUploadUrlForSyllabus(batches[activeBatchIndex]._id as string, fileType);
        await uploadFileOnUrl(awsBucket.url, droppedFile);
        await updateSyllabus(batches[activeBatchIndex]._id as string, awsBucket.uuid)
        enqueueSnackbar("Successfully Updated Syllabus", {variant: 'success'});
      } else {
        enqueueSnackbar("File Size Detected Zero", {variant: 'warning'});
      }
    } catch (error) {
      console.log(error)
    }
  }

  const downloadSyllabus = async() => {
    const droppedFile = droppedFiles[0];
    if(droppedFile.size === 0) {
      const awsBucket = await fetchDownloadUrlForSyllabus(batches[activeBatchIndex]._id as string, batches[activeBatchIndex].syllabus as string);
      setTimeout(() => {
        const response = {
          file: awsBucket.url,
        };
        window.open(response.file);
      }, 100);
    } else {
      FileDownload(droppedFile, batches[activeBatchIndex].batchfriendlyname + '_syllabus.pdf');
    }
  }

  const onDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    let items = reorder(
      chapters,
      result.source.index,
      result.destination.index
    );
    items = items.map((item, index) => {
      if (item.orderNo !== index + 1) {
        return { ...item, orderNo: index + 1 };
      }
      return item;
    });
    handleUpdateChapters(items);
  };

  const getChapterNumber = (chapterIndex: number) => chapterIndex + 1;

  const classes = useStyles();

  return (
    <Scrollbars autoHeight autoHeightMax="78vH" autoHeightMin="78vH"
      style={{
        backgroundColor: "white",
        boxShadow: '0px 4px 20px rgb(7 40 137 / 4%)',
        borderRadius: '5px',
        padding: '10px 0px'
      }}
    >
      <Box padding="10px 20px">
        <Box
          margin="0 0 10px 0"
          display="flex"
          justifyContent="space-between"
          alignItems="center"
        >
          <Box component="h3" className={classes.courseHeading}>
            Select Courses
          </Box>
          <Box>
            {
              batches.length === 0 ? '' :
              <React.Fragment>
                <Button
                  disableElevation
                  color="primary"
                  size="large"
                  variant="contained"
                  style={{marginRight: '10px'}}
                  onClick={() => {setOpenUploadSyllabusModal(true)}}
                >
                  Syllabus
                </Button>
                {
                  !(isStudent(profile) || isParent(profile)) && <Button
                  disableElevation
                  color="primary"
                  size="large"
                  variant="contained"
                  onClick={handleSubmitChapterActions}
                >
                  Save
                </Button>
                }
                
            </React.Fragment>
            }
          </Box>
        </Box>

        <FormControl variant="outlined" fullWidth>
          <InputLabel>Select</InputLabel>
          <Select
            label="Select Course"

            inputProps={{
              name: 'course'
            }}
            value={courses.length > 0 ? activeCourseIndex : ''}
            onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
              handleSelectCourse(e.target.value as number)
            }
          >
            {courses.map((course, index) => (
              <MenuItem key={index} value={index}>
                {course.board} - {course.className} - {course.subject}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>Please select a course</FormHelperText>
        </FormControl>
        {
          batches.length === 0 ? 
          <p style={{
            color:"crimson", fontSize: fontOptions.size.small,fontFamily:fontOptions.family,
            fontWeight: fontOptions.weight.normal, marginLeft:"12px"}}
          >
          Please create a batch first</p>:
        <FormControl style={{marginTop:'18px'}} variant="outlined" fullWidth>
          <InputLabel>Select Batch</InputLabel>
          <Select
            
            label="Select Batch"
            inputProps={{
              name: 'batch'
            }}
            value={courses.length > 0 ? activeBatchIndex : ''}
            onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
              handleSelectBatch(e.target.value as number)
            }
          >
            {batches.map((batch, index) => (
              <MenuItem key={index} value={index}>
                {batch.batchfriendlyname}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>Please select one of the available batch</FormHelperText>
        </FormControl>
      }
      </Box>
      {
        !(isStudent(profile) || isParent(profile)) && 
        <Box position="relative">
        <Tooltip title="Add New Custom Chapter">
          <IconButton
            className={classes.addChapterButton}
            onClick={() => setOpenAddChapterModal(true)}
          >
            <AddIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Add Textbook Chapters">

          <IconButton
          style={{marginRight:'60px'}}
            className={classes.addChapterButton}
            onClick={() => setOpenAddMasterChapterModal(true)}
          >
            <ImportContactsIcon />
          </IconButton>
        </Tooltip>

        <Divider />
      </Box>
      }
      

      <Box padding="20px">
        <FormHelperText className={classes.helperText}>
          List of chapters available in the course
        </FormHelperText>

        <Box>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable" isDropDisabled={isStudent(profile)|| isParent(profile)}>
              {(provided) => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {chapters.map((chapter, index) => (
                    <Draggable
                      key={index}
                      draggableId={index.toString()}
                      isDragDisabled={isStudent(profile) || isParent(profile)}
                      index={index}
                    >
                      {(provided) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <ChapterListItem
                            key={index}
                            chapter={chapter}
                            profile = {profile}
                            chapterNumber={getChapterNumber(index)}
                            isActive={index === activeChapterIndex}
                            handleOnClick={() => handleClickOnChapter(index)}
                            editItem={(name) => editChapterItem(index, name)}
                            removeItem={() => removeChapterItem(index)}
                          />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </Box>
      </Box>

      <AddChapterModal
        openModal={openAddChapterModal}
        handleClose={() => setOpenAddChapterModal(false)}
        addchaptererror={addchaptererror}
        handleAddChapter={addChapter}
      />
      {!(isStudent(profile) || isParent(profile)) && <AddMasterChapterModal
        openModal={openAddMasterChapterModal}
        handleClose={() => setOpenAddMasterChapterModal(false)}
        masterChapters={masterChapters}
        handleAddMasterChapter={addMasterChapter}
      />}
      
      <UploadSyllabusModal
        profile = {profile}
        openModal={openUploadSyllabusModal}
        handleClose={() => setOpenUploadSyllabusModal(false)}
        uploadSyllabus={uploadSyllabus}
        downloadSyllabus={downloadSyllabus}
        droppedFiles={droppedFiles}
        setDroppedFiles={setDroppedFiles}
      />
      </Scrollbars>
  );
};

export default CourseContentSidebar;
