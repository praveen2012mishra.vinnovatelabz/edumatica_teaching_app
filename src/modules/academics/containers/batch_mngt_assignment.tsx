import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../../store';
import { User, Children } from '../../common/contracts/user';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { AssignmentRes, AssignedAssignmentRes, StudentAssignmentRes } from '../../common/contracts/assignment';

import { getStudent } from '../../common/api/tutor';
import { getAssignments, getStudentAssignments, getAssignedAssignments } from '../../common/api/assignment';

import {
  Typography,
  Link,
  Card,
  Tooltip,
  IconButton,
  CardContent
} from '@material-ui/core';

import {
  isParent,
  isStudent,
  isTutor
} from '../../common/helpers';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tprimary: {
      fontSize: theme.typography.pxToRem(25),
      color: 'black'
    },
    tsecondary: {
      fontSize: theme.typography.pxToRem(15),
      color: '#696969'
    },
    root: {
      width: '100%'
    }
  }),
);

interface BatchAssignment {
  assignmentId: string;
  assignmentname: string;
  marks: number;
  breif: string;
  deadline: Date;
}

interface Props {
  authUser: User,
  parentChildren: Children;
  board: string | undefined;
  classname: string | undefined;
  subject: string | undefined;
  batchID: string | undefined
}

const BatchMngtAssignment:FunctionComponent<Props> = ({ authUser, parentChildren, board, classname, subject, batchID }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [reloadAssignment, setReloadAssignment] = useState(false);

  const [stuAssignment, setStuAssignment] = useState<StudentAssignmentRes[]>([]);
  const [assignment, setAssignment] = useState<AssignmentRes[]>([]);
  const [assignedAssignment, setAssignedAssignment] = useState<AssignedAssignmentRes[]>([]);
  const [batchAssignment, setBatchAssignment] = useState<BatchAssignment[]>([])

  useEffect(() => {
    
      (async () => {
        try {
          if(isStudent(authUser)) {
            const student = await getStudent();
            const studentAssignmentsRes: StudentAssignmentRes[] = await getStudentAssignments({
              batches: student.batches
            });
            const batAssignment: BatchAssignment[] = studentAssignmentsRes.map((assign, count) => {
              return {
                assignmentId: String(count),
                assignmentname: assign.assignmentname,
                marks: assign.marks,
                breif: assign.brief,
                deadline: assign.deadline
              }
            })
            setBatchAssignment(batAssignment)
          } else if(isParent(authUser)) {
            const studentAssignmentsRes: StudentAssignmentRes[] = await getStudentAssignments({
              batches: [batchID as string]
            });
            const batAssignment: BatchAssignment[] = studentAssignmentsRes.map((assign, count) => {
              return {
                assignmentId: String(count),
                assignmentname: assign.assignmentname,
                marks: assign.marks,
                breif: assign.brief,
                deadline: assign.deadline
              }
            })
            setBatchAssignment(batAssignment)
          } else {
            const assignedAssignmentsRes: AssignedAssignmentRes[] = await getAssignedAssignments({
              tutorId: isTutor(authUser) ? authUser._id : undefined
            });

            const filtered = assignedAssignmentsRes.filter(assign => {
              return assign.boardname === board && assign.subjectname === subject && assign.classname === classname && assign.batches.some(bat => bat.batchId === batchID)
            })

            const batAssignment: BatchAssignment[] = filtered.map(assign => {
              const index = assign.batches.findIndex(batc => batc.batchId === batchID)
              return {
                assignmentId: assign.assignmentId,
                assignmentname: assign.assignmentname,
                marks: assign.marks,
                breif: assign.brief,
                deadline: assign.batches[index].deadline
              }
            })
            setBatchAssignment(batAssignment)
          }
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
  }, [reloadAssignment, authUser, subject, classname, board, batchID]);

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'assignmentname', headerName: 'Name', flex: 1, renderCell: datagridCellExpand  },
    { field: 'marks', headerName: 'Marks', flex: 1, renderCell: datagridCellExpand },
    { field: 'breif', headerName: 'Breif', flex: 1, renderCell: datagridCellExpand },
    { field: 'deadline', headerName: 'Deadline', flex: 1, renderCell: datagridCellExpand },
  ];

  const gridRows = batchAssignment.map((row, index) => {
    return ({
      id: (index + 1),
      assignmentname: row.assignmentname,
      marks: row.marks,
      breif: row.breif ? row.breif : '-',
      deadline: new Date(row.deadline).toLocaleString()
    })
  })

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if(batchAssignment.length < 1) {
    return (
      <div style={{margin: '27px 0px', textAlign: 'center'}}>
        <Typography className={classes.tsecondary}>
            No Assignment Yet
        </Typography>
        {(!isStudent(authUser) && !isParent(authUser)) &&
          <Typography>
            <Link
              to={{pathname: '/profile/assignment'}}
              component={RouterLink}
            >
              Click here to Create an Assignment
            </Link>
          </Typography>
        }
      </div>
    )
  }

  return (
    <div style={{margin: '0px 0px'}}>
      {(!isStudent(authUser) && !isParent(authUser)) &&
        <Typography style={{textAlign: 'center', marginBottom: '25px'}}>
          <Link
            to={{pathname: '/profile/assignment'}}
            component={RouterLink}
          >
            Click here to Create an Assignment
          </Link>
        </Typography>
      }

      <Card variant="outlined" style={{margin: '20px 0px'}}>
        <CardContent style={{padding:'0'}}>
          <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
        </CardContent>
      </Card>
    </div>
  )
}


const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(BatchMngtAssignment);
