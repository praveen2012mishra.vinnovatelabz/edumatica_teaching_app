import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Chapter } from '../contracts/chapter';
import { RootState } from '../../../store';
import { User } from '../../common/contracts/user';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FolderIcon from '@material-ui/icons/Folder';
import {
  Accordion,
  Typography,
  AccordionSummary,
  AccordionDetails,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  FormControl,
  Select,
  MenuItem,
  InputLabel,
  Link
} from '@material-ui/core';

import {
  isStudent,
  isOrgTutor,
  isParent
} from '../../common/helpers';
import { 
  fetchCustomChaptersList, fetchChapterContent
} from '../../common/api/academics';
import { generateCourseChaptersSchema } from '../helpers';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tsecondary: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      color: '#696969'
    },
    heading: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.bold,
      flexBasis: '33.33%',
      flexShrink: 0,
    },
  }),
);

interface Props {
  authUser: User,
  board: string | undefined,
  classname: string | undefined,
  subject: string | undefined,
  batchID: string | undefined
}

const BatchMngtNotes:FunctionComponent<Props> = ({ authUser, board, classname, subject, batchID }) => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState('');

  const [expanded, setExpanded] = useState<string | false>(false);
  const [filetype, setFiletype] = useState('pdf');

  const [chapters, setChapters] = useState<Chapter[]>([]);

  useEffect(() => {
    if(board && classname && subject && batchID){
      (async () => {
        try {
          const customChaptersPromise = fetchCustomChaptersList({
            batchId: batchID,
            boardname: board,
            classname: classname,
            subjectname: subject
          })


          const [customChapters] = await Promise.all([
            customChaptersPromise
          ]);

          let structuredChapters = generateCourseChaptersSchema(
            [],
            customChapters
          );
  
          structuredChapters = structuredChapters.map((chapter, index) => {
            return { ...chapter, orderNo: index + 1 };
          });
  
          setChapters(structuredChapters);
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
    }
  }, [board, classname, subject, batchID]);

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setFiletype(event.target.value as string);
  };

  const handleChange = (panel: string, activeChap: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
    fetchActiveContent(activeChap);
  };

  const fetchActiveContent = (activeChap: string) => {
    if(board && classname && subject && batchID) {
      (async () => {
        try {
          const chapterContent = await fetchChapterContent({
            batchId: batchID,
            boardname: board,
            classname: classname,
            subjectname: subject,
            chaptername: activeChap
          });
    
          const chapterIndex = chapters.findIndex(
            (chapter) => chapter.name === chapterContent.chaptername
          );
    
          const chaptersList = [...chapters];
    
          // @ts-ignore
          chaptersList[chapterIndex].contents = chapterContent.contents;
    
          setChapters(chaptersList);
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
    }
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if(chapters.length < 1) {
    return (
      <div style={{margin: '27px 0px', textAlign: 'center'}}>
        <Typography className={classes.tsecondary}>
            No Chapter Lists Yet
        </Typography>
        {(!isStudent(authUser) && !isOrgTutor(authUser) && !isParent(authUser)) &&
          <Typography>
            <Link
              to={{pathname: '/profile/courses'}}
              component={RouterLink}
            >
              Click here to Add Contents
            </Link>
          </Typography>
        }
      </div>
    )
  }

  return (
    <div style={{margin: '27px 0px'}}>
      {(!isStudent(authUser) && !isOrgTutor(authUser) && !isParent(authUser)) &&
        <Typography style={{textAlign: 'center', marginBottom: '25px'}}>
          <Link
            to={{pathname: '/profile/courses'}}
            component={RouterLink}
          >
            Click here to Add Contents
          </Link>
        </Typography>
      }

      {chapters.map(chapter => {
        return(
          <Accordion expanded={expanded === String(chapter.orderNo)} 
            onChange={handleChange(String(chapter.orderNo), chapter.name)}
            key={chapter.orderNo}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>{chapter.name}</Typography>
            </AccordionSummary>
            <AccordionDetails>
                {(chapter.contents.length > 0) &&
                  <div>
                    <FormControl variant="filled">
                      <InputLabel id="demo-simple-select-filled-label">Filetype</InputLabel>
                      <Select
                        labelId="demo-simple-select-filled-label"
                        id="demo-simple-select-filled"
                        value={filetype}
                        onChange={handleSelectChange}
                      >
                        <MenuItem value="pdf">PDF</MenuItem>
                        <MenuItem value="image">Image</MenuItem>
                        <MenuItem value="embed-link">Embed Link</MenuItem>
                        <MenuItem value="video">Video</MenuItem>
                        <MenuItem value="document">Document</MenuItem>
                        <MenuItem value="quiz">Quiz</MenuItem>
                      </Select>
                    </FormControl>
                    {(chapter.contents.filter(element => element.contenttype === filetype)).map(content => {
                      return(
                        <List> 
                          <ListItem>
                            <ListItemAvatar>
                              <Avatar>
                                <FolderIcon />
                              </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                              primary={content.contentname}
                            />
                          </ListItem>
                        </List>
                      )
                    })}
                  </div>
                }
                {(chapter.contents.length === 0) &&
                  <Typography className={classes.tsecondary}>
                    No Content
                  </Typography>
                }
            </AccordionDetails>
          </Accordion>
        );
      })}
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(BatchMngtNotes);
