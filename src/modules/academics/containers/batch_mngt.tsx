//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../../store';
import { Batch } from '../contracts/batch';
import { User, Student, Children, Tutor } from '../../common/contracts/user';

import { createStyles, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import {
  Search as SearchIcon,
  Clear as ClearIcon
} from '@material-ui/icons';
import { 
  Typography,
  IconButton,
  Tooltip,
  Container,
  Box,
  CardContent,
  Card,
  Grid,
  Button as MuButton,
  Link,
  FormControl,
  Select,
  InputLabel,
  Chip,
  MenuItem,
  Input,
  InputAdornment
} from '@material-ui/core';
import Button from '../../common/components/form_elements/button'
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import { useSnackbar } from "notistack"

import { updateTutorBatch, fetchTutorStudentsListid, fetchBatchesList } from '../../common/api/academics';
import { fetchOrgBatchesList, updateOrgBatch, fetchStudentBatchesList } from '../../common/api/batch';
import {
  getOrgStudentsList,
} from '../../common/api/organization';
import {
  isOrganization,
  isStudent,
  isOrgTutor,
  isParent,
  isAdminTutor
} from '../../common/helpers';

import MiniDrawer from '../../common/components/sidedrawer';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'
import BatchMngtAssignment from './batch_mngt_assignment';
import BatchMngtAssessment from './batch_mngt_assessment';
import BatchMngtNotes from './batch_mngt_notes';
import BatchMngtClasses from './batch_mngt_classes';
import { fontOptions } from '../../../theme';
import { eventTracker, GAEVENTCAT } from '../../common/helpers';

import moment from 'moment';
import { values } from 'lodash';

interface Data {
  performace: string;
  mobile: string;
  name: string;
  id: string;
}

function createData(
  name: string,
  mobile: string,
  performace: string,
  id: string
): Data {
  return { name, mobile, performace, id };
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name: string, pAdditions: string[], theme: Theme) {
  return {
    fontWeight:
      pAdditions.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    tprimary: {
      fontSize: fontOptions.size.mediumPlus,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.bold,
      color: '#404040'
    },
    tsecondary: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      color: '#666666'
    },
    active: {
      color: '#4C8BF5'
    },
    inactive: {
      color: '#696969'
    },
    formControl: {
      margin: theme.spacing(1),
      width: '100%',
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    }
  }),
);

interface Props {
  authUser: User,
  parentChildren: Children,
  history: object,
  location: {
    state: {
      batch: string
    }
  },
  match: object
}

const BatchMngt:FunctionComponent<Props> = ({ location, authUser, parentChildren }) => {
  const classes = useStyles();
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar()

  const [redirectTo, setRedirectTo] = useState('');

  const [section, setSection ] = useState<string>('assignment');
  const [reloadStudent, setReloadStudent] = useState(false);
  const [showStudentAdd, setShowStudentAdd] = useState(false);
  const [pAdditions, setPAdditions] = React.useState<string[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);

  const [batch, setBatch] = useState<Batch>();
  const [students, setStudents] = useState<Student[]>([]);
  const [nonBatchstudents, setNonBatchStudents] = useState<Student[]>([]);

  eventTracker(GAEVENTCAT.batchManagement, 'Batch Management Expand Page', 'Landed Batch Management Expand Page');

  useEffect(() => {
    if(location.state && location.state.batch){
      (async () => {
        try {
          const batchesList = (isOrganization(authUser) ?
            await fetchOrgBatchesList() : 
            (isStudent(authUser) ? 
              await fetchStudentBatchesList(authUser._id ? authUser._id : '') : 
              (isParent(authUser) ?
                (parentChildren.current ? 
                  await fetchStudentBatchesList(parentChildren.current ? parentChildren.current : '') : 
                  []
                ) :
                await fetchBatchesList()
              )
            )
          );
          const batchDetails = batchesList.find(batch => batch.batchfriendlyname === location.state.batch); 
          if(batchDetails) {
            const tutorid = (isAdminTutor(authUser) || isOrgTutor(authUser)) ? (batchDetails.tutorId) : (batchDetails.tutorId as Tutor)._id
            const studentsList = isOrganization(authUser) ? await getOrgStudentsList() : await fetchTutorStudentsListid(tutorid as string);
            let batchStudents = studentsList.filter(student => {
              return (student._id ? (batchDetails? (batchDetails.students.includes(student._id)) : false) : false)
            });
  
            const nonbatchStudents = studentsList.filter(list => (list.boardName === batchDetails?.boardname && list.className === batchDetails.classname)).filter(student => {
              return (student._id ? (batchDetails? (!batchDetails.students.includes(student._id)) : false) : false)
            });
  
            if(searchText !== '') {
              batchStudents = batchStudents.filter(bat => 
                bat.studentName.toLowerCase().includes(searchText.toLowerCase()) ||
                bat.mobileNo.includes(searchText)
              )
            }
  
            setBatch(batchDetails)
            setStudents(batchStudents)
            setNonBatchStudents(nonbatchStudents)
          }
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
    }
  }, [authUser, location, reloadStudent, searchText, parentChildren]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const removeStudent = async (selectedRowDetails: Data) => {
    const id = selectedRowDetails.id;
    if(batch && batch.students.length > 1 && id) {
      const newStudentList = students.filter(student => student._id !== id).map(element => element.mobileNo);

      try {
        const updatedBatch = {
          students: newStudentList,
          boardname: batch.boardname,
          classname: batch.classname,
          subjectname: batch.subjectname,
          batchfriendlyname: batch.batchfriendlyname,
          batchenddate: moment(batch.batchenddate).format('YYYY-MM-DD'),
          batchstartdate: moment(batch.batchstartdate).format('YYYY-MM-DD'),
          batchicon: '',
          tutor: authUser.mobileNo,
          batchId: batch._id,
          _id: batch._id
        };
  
        isOrganization(authUser) ? await updateOrgBatch(updatedBatch) : await updateTutorBatch(updatedBatch);
        setReloadStudent(prevReloadStudent => !prevReloadStudent)
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    } else {
      enqueueSnackbar("Cannot Remove Students from Strength 1 Class", { variant: "warning" })
    }
  };

  const buttonData = [
    {
      title: 'Remove',
      action: removeStudent,
      icon: <RemoveCircleOutlineIcon />
    }
  ];

  if(isStudent(authUser) || isOrgTutor(authUser) || isParent(authUser)) {
    buttonData.pop();
  }

  const rows = students.map((student) => {     
    return(
      createData(
        student.studentName,
        student.mobileNo,
        'View',
        student._id ? student._id : ''
      )
    )
  });

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'name', headerName: 'Name', flex: 1, renderCell: datagridCellExpand },
    { field: 'mobile', headerName: 'Mobile', flex: 1, renderCell: datagridCellExpand },
    { field: 'performace', headerName: 'Performance', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          name: params.getValue("name") as string,
          mobile: params.getValue("mobile") as string
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.name === selectedRow.name && row.mobile === selectedRow.mobile && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as Data);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  if(isStudent(authUser) || isOrgTutor(authUser) || isParent(authUser)) {
    gridColumns.pop();
  }

  const gridRows = rows.map((row, index) => {
    return ({
      id: (index + 1),
      name: row.name,
      mobile: row.mobile,
      performace: row.performace
    })
  })

  const names = nonBatchstudents.map(student => {
    return {id: student._id, name: student.studentName}
  })

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if(!(event.target.value as string[]).includes('null')) {
      setPAdditions(event.target.value as string[]);
    }
  };

  const addStudent = async () => {
    if(batch) {
      const newAdditions = nonBatchstudents.filter(student => {
        return (student._id ? pAdditions.includes(student._id) : false)
      });

      const newStudentList = [...students, ...newAdditions].map(element => element.mobileNo);;
      try {
        const updatedBatch = {
          students: newStudentList,
          boardname: batch.boardname,
          classname: batch.classname,
          subjectname: batch.subjectname,
          batchfriendlyname: batch.batchfriendlyname,
          batchenddate: moment(batch.batchenddate).format('YYYY-MM-DD'),
          batchstartdate: moment(batch.batchstartdate).format('YYYY-MM-DD'),
          batchicon: '',
          tutor: batch.tutorId?.mobileNo as string,
          batchId: batch._id,
          _id: batch._id
        };
  
        isOrganization(authUser) ? await updateOrgBatch(updatedBatch) : await updateTutorBatch(updatedBatch);
        setReloadStudent(prevReloadStudent => !prevReloadStudent)
        setNonBatchStudents((prev)=>{
          return prev.filter(el=>newAdditions.map(el=>el._id).includes(el._id))
        })
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
    setShowStudentAdd(false);
    setPAdditions([]);
  };

  if(!location.state || !location.state.batch) {
		return (
			<Redirect to='/profile/batches' />
		)
	}

  return ( 
    <div>
      <MiniDrawer>

      <Box marginY="20px">
        <Container>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6}>

              <Card variant="outlined">
                <CardContent>
                  <Grid container spacing={3} alignItems="center" >
                    <Grid item xs={12} md={6}>
                      <Typography className={classes.tsecondary}>
                        { batch?.boardname } | { batch?.classname } | { batch?.subjectname } {(isStudent(authUser) || isOrganization(authUser) || isParent(authUser)) && batch?.tutorId && ('| '+ batch.tutorId.tutorName)}
                      </Typography>
                      <Typography className={classes.tprimary}>
                        { batch?.batchfriendlyname } 
                      </Typography>
                    </Grid>

                    <Grid item xs={12} md={6} justify="flex-end">
                      <Tooltip title="Back" style={{float: 'right'}}>
                        <Link
                          to={{pathname: '/profile/batches'}}
                          component={RouterLink}
                        >
                          <IconButton aria-label="back">
                            <ArrowBackIcon />
                          </IconButton>
                        </Link>
                      </Tooltip>

                      {(!isStudent(authUser) && !isOrgTutor(authUser) && !isParent(authUser)) &&
                        <Tooltip title="Add Students" style={{float: 'right'}}>
                          <IconButton aria-label="add" 
                            onClick={() => {
                              setShowStudentAdd(true);
                            }}
                          >
                            <PersonAddIcon />
                          </IconButton>
                        </Tooltip>
                      }

                      <Input
                        name="search"
                        inputProps={{ inputMode: 'search' }}
                        onFocus={() => setFocused(true)} 
                        onBlur={() => setFocused(false)}
                        placeholder="Search"
                        value={searchText}
                        onChange={(e) => {
                          setSearchText(e.target.value);
                        }}
                        style={{width: '50%', marginTop: '5px'}}
                        endAdornment={
                          searchText.trim() !== '' ? (
                            <InputAdornment position="end">
                              <IconButton size="small" onClick={() => setSearchText('')}>
                                <ClearIcon />
                              </IconButton>
                            </InputAdornment>
                          ) : (
                            <InputAdornment position="end">
                              <IconButton disabled size="small">
                                <SearchIcon />
                              </IconButton>
                            </InputAdornment>
                          )
                        }
                      />
                      {(searchText.trim() !== '') &&
                        <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666', wordWrap: 'break-word'}}>
                          Filtered Table using Keyword '{searchText}'
                        </Typography>
                      }
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>  

              <Card variant="outlined" style={{margin: '20px 0px'}}>
                <CardContent >
                  {!showStudentAdd &&
                    <div>
                      <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
                    </div>
                  }

                  {showStudentAdd &&
                    <div>
                      <Typography className={classes.tsecondary} style={{textAlign: 'center'}}>
                        Add Students
                      </Typography>

                      <FormControl className={classes.formControl}>
                        <InputLabel id="demo-mutiple-chip-label">Students</InputLabel>
                        <Select
                          labelId="demo-mutiple-chip-label"
                          id="demo-mutiple-chip"
                          multiple
                          value={pAdditions}
                          onChange={handleChange}
                          input={<Input id="select-multiple-chip" />}
                          renderValue={(selected) => (
                            <div className={classes.chips}>
                              {(selected as string[]).map((value) => (
                                <Chip key={value} label={names.filter(el=>el.id==value)[0].name} className={classes.chip} />
                              ))}
                            </div>
                          )}
                          MenuProps={MenuProps}
                        >
                          {names.map((name) => (
                            <MenuItem key={name.id} value={name.id}
                              style={getStyles(name.id ? name.id : '', pAdditions, theme)}
                            >
                              {name.name}
                            </MenuItem>
                          ))}
                          {(names.length == 0) &&
                            <MenuItem value="null">No students</MenuItem>
                          }
                        </Select>
                      </FormControl>
                      
                      <Grid container spacing={3} style={{marginTop: '10px'}}>
                        <Grid item xs={12} md={6}>
                          <Button style={{width: '100%', textAlign: 'center'}}
                            variant="contained" color="secondary"
                            onClick={() => {
                              setShowStudentAdd(false);
                              setPAdditions([]);
                            }}
                          >
                            Cancel
                          </Button>
                        </Grid>
                        <Grid item xs={12} md={6}>
                          <Button style={{width: '100%', textAlign: 'center'}}
                            variant="contained" color="primary" onClick={addStudent}
                          >
                            Add Students
                          </Button>
                        </Grid>
                      </Grid>
                    </div>
                  }
                </CardContent>
              </Card>

            </Grid>

            <Grid item xs={12} md={6}>
              <Grid container alignItems="center" 
                style={{ minHeight: '95px', textAlign: 'center'}}
              >
                <Grid item xs>
                  <MuButton onClick={() => setSection('assignment')}
                    className={section === 'assignment'
                      ? classes.active
                      : classes.inactive
                    }
                  >
                    Assignment
                  </MuButton>
                </Grid>
                <Grid item xs>
                  <MuButton onClick={() => setSection('assessment')}
                    className={section === 'assessment'
                      ? classes.active
                      : classes.inactive
                    }
                  >
                    Assessment
                  </MuButton>
                </Grid>
                <Grid item xs>
                  <MuButton onClick={() => setSection('notes')}
                    className={section === 'notes'
                      ? classes.active
                      : classes.inactive
                    }
                  >
                    Notes
                  </MuButton>
                </Grid>
                <Grid item xs>
                  <MuButton onClick={() => setSection('classes')}
                    className={section === 'classes'
                      ? classes.active
                      : classes.inactive
                    }
                  >
                    Classes
                  </MuButton>
                </Grid>
              </Grid>

              {(section === 'assignment') && 
                <BatchMngtAssignment 
                  board={batch?.boardname}
                  classname={batch?.classname}
                  subject={batch?.subjectname} 
                  batchID={batch?._id}
                />
              }

              {(section === 'assessment') && 
                <BatchMngtAssessment 
                  subject={batch?.subjectname}
                />
              }

              {(section === 'notes') && 
                <BatchMngtNotes
                  board={batch?.boardname}
                  classname={batch?.classname}
                  subject={batch?.subjectname} 
                  batchID={batch?._id}
                />
              }

              {(section === 'classes') && 
                <BatchMngtClasses 
                  batchID={batch?._id}
                />
              }
            </Grid>
         
          </Grid>
        </Container>
      </Box>
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(BatchMngt);
