import React, { FunctionComponent, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { RootState } from '../../../store';
import { Tutor, User, Organization, Children } from '../../common/contracts/user';
import { isOrganization, isParent, isStudent, isTutor } from '../../common/helpers';
import TutorCourses from '../components/layouts/tutor_courses';
import OrgCourses from '../components/layouts/org_courses';
import StudentCourses from '../components/layouts/student_courses';

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
  children : Children
}

const Courses: FunctionComponent<Props> = ({ authUser,  children}) => {
  const [redirectTo, setRedirectTo] = useState('');

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo(`/profile/personal-information`);
    }
  }, [authUser.mobileNo]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if (isTutor(authUser)) {
    return <TutorCourses profile={authUser as Tutor} />;
  }
  else if(isOrganization(authUser)){
    return <OrgCourses profile={authUser as Organization} />
  } 
  else if(isParent(authUser)){
    return <StudentCourses profile={authUser} children={children}  />
  }
  else{
    return <StudentCourses profile={authUser} children={children} />
  }
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  children : state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(withRouter(Courses));
