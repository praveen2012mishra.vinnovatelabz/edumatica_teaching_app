import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  Box,
  Checkbox,
  Chip,
  Container,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  Select,
  MenuItem,
  IconButton,
  Input,
  Typography
} from '@material-ui/core';
import { RemoveCircle as RemoveCircleIcon } from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  fetchOrgCoursesList,
  getOrgStudentsList,
  fetchOrgTutorsList
} from '../../common/api/organization';
import {
  createOrgBatch,
  deleteOrgBatch,
  fetchOrgBatchesList,
  updateOrgBatch
} from '../../common/api/batch';
import { RootState } from '../../../store';
import { Batch } from '../contracts/batch';
import { User, Student, Tutor } from '../../common/contracts/user';
import { Course } from '../contracts/course';
import BatchIcon from '../../../assets/images/batch.png';
import ListIcon from '../../../assets/images/bullet-list.png';
import AddIcon from '../../../assets/images/move-right.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import ConfirmationModal from '../../common/components/confirmation_modal';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import OrgBatchListItem from '../components/org_batch_list_item';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { Redirect } from 'react-router-dom';
import { StackActionType } from '../../common/enums/stack_action_type';
import Datepickers from '../../common/components/datepickers'
import { fontOptions } from '../../../theme';
import TransferLists from '../../common/components/transferLists';
import {useSnackbar} from 'notistack'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#666666'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      marginTop: '5px',
      color: '#1C2559'
    },
    formInput: {
      lineHeight: '18px',
      color: '#151522',

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        fontFamily:fontOptions.family,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    checkLabel: {
      '& .MuiFormControlLabel-label': {
        fontFamily: fontOptions.family,
        fontSize: fontOptions.size.small,
        lineHeight: '19px',
        letterSpacing: '0.25px',
        color: '#202842'
      }
    },
    addIcon: {
      '& button': {
        background: '#FDFDFD',
        border: '1px solid #F2F2F2',
        borderRadius: '0',
        padding: '20px 25px'
      }
    },
    studentChip: {
      padding: '10px',
      background: '#FFFFFF',
      boxShadow: '0px 10px 20px rgb(31 32 65 / 5%)',
      border: '1px solid rgba(102, 102, 102, 0.3)',
      borderRadius: '5px',
      minHeight: '70px',

      '& .MuiChip-root': {
        background: '#FDE5AD',
        borderRadius: '20px',
        padding: '10px 8px',
        fontFamily: fontOptions.family,
        fontSize: fontOptions.size.small,
        color: '#202842'
      },
      '& .MuiChip-deleteIcon': {
        color: '#ABAEC2'
      }
    },
    addBtn: {
      '& button': {
        padding: '10px 35px'
      }
    },
    clearBtn: {
      '& button': {
        border: '1px solid #666666',
        color: '#666666',
        padding: '10px 35px'
      }
    },
    dateError: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      color: '#F44E41'
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface inputs {
  id: string,
  value: string
}

interface Props extends RouteComponentProps {
  authUser: User;
}

interface FormData {
  pageError: string;
  tutor: string;
  batchName: string;
  courseIndex: number;
}

interface DateError {
  start: string | boolean;
  end: string | boolean;
}

const ValidationSchema = yup.object().shape({
  batchName: yup.string().required('Batch number is a required field').min(5),
});

const CreateBatchOrg: FunctionComponent<Props> = ({ authUser, history }) => {
  const { handleSubmit, register, errors, setError } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const {enqueueSnackbar} = useSnackbar()
  const [batches, setBatches] = useState<Batch[]>([]);
  const [courseIndex, setCourseIndex] = useState(-1);
  const [courses, setCourses] = useState<Course[]>([]);
  const [tutorIndex, setTutorIndex] = useState(-1);
  const [tutors, setTutors] = useState<Tutor[]>([]);
  const [filteredBatches, setFilteredBatches] = useState<Batch[]>([]);
  const [batchName, setBatchName] = useState('');
  const [batchId, setBatchId] = useState('');
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  const [dateError, setDateError] = useState<DateError>({start: false, end: false})
  const [batchDurationError,setBatchDurationError] = useState<boolean>(false)
  const [redirectTo, setRedirectTo] = useState('');
  const [studentsList, setStudentsList] = useState<Student[]>([]);
  const [operation, setOperation] = useState(StackActionType.CREATE);
  const [currentBatchIndex, setCurrentBatchIndex] = useState(-1);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [left, setLeft] = React.useState<inputs[]>([]);
  const [right, setRight] = React.useState<inputs[]>([]);
  //const [selected, setSelected] = React.useState<string[]>([]);

  const classes = useStyles();
  eventTracker(GAEVENTCAT.batchManagement, 'Organization Batches Create Page', 'Landed Organization Batches Create Page');

  useEffect(() => {
    (async () => {
      try {
        const coursesList = await fetchOrgCoursesList();
        setCourses(coursesList);

        if(coursesList.length === 1) {
          setCourseIndex(0)
        }

        const tutorsList = await fetchOrgTutorsList();
        setTutors(tutorsList);

        if(tutorsList.length === 1) {
          setTutorIndex(0)
        }

        let batchesList = await fetchOrgBatchesList();
        const studentsList = await getOrgStudentsList();

        // update batches by student mobile number
        batchesList = batchesList.map(list => {
          return {
            ...list,
            ...{
              students: list.students.map(stud => {
                return (studentsList.find(stuList => stuList._id === stud) as Student).mobileNo
              })
            }
          }
        })

        setBatches(batchesList);
        setFilteredBatches(batchesList);
        setStudentsList(studentsList);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo]);

  useEffect(() => {
    if(courseIndex < 0) {
      setLeft([])
    } else {
      const selectedcourse = courses[courseIndex];
      const filteredStudents = studentsList.filter(stud =>
        (stud.boardName === selectedcourse.board && stud.className === selectedcourse.className)
      )
      if(operation === StackActionType.UPDATE) {
        // filter selected students from student list and
        // update in checked students

        const selectedbatch = batches[currentBatchIndex];
        
        const left = filteredStudents.filter(list => !selectedbatch.students.includes(list.mobileNo)).map(list => {return {id: list._id as string, value: list.studentName}})
        const right = studentsList.filter(list => selectedbatch.students.includes(list.mobileNo)).map(list => {return {id: list._id as string, value: list.studentName}})

        setLeft(left);
        setRight(right);
      } else {
        const left:inputs[] = filteredStudents.map(list => {return {id: list._id as string, value: list.studentName}})
        setLeft(left)
      }
    }
  }, [courseIndex, studentsList])

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleStartDateChange = (date: Date | null) => {
    if(date) {
      setDateError({start: false, end: dateError.end})
    }
    if(endDate && date){
      if(date?.setHours(0,0,0,0)==endDate.setHours(0,0,0,0)){
        setBatchDurationError(true)
      }
      else{
        setBatchDurationError(false)
      }
    }
    
    setStartDate(date);
  };

  const handleEndDateChange = (date: Date | null) => {
    if(date) {
      setDateError({start: dateError.start, end: false})
    }
    if(startDate && date){
      if(date?.setHours(0,0,0,0)==startDate.setHours(0,0,0,0)){
        setBatchDurationError(true)
      }
      else{
        setBatchDurationError(false)
      }
    }
    else{
      setBatchDurationError(false)
    }
    setEndDate(date);
  };

  console.log(endDate?.setHours(0,0,0,0)==startDate?.setHours(0,0,0,0))

  const selectTutor = (index: number) => {
    setTutorIndex(index);
    if (index > -1) {
      let filteredBatches = batches.filter(
        (batch) => batch.tutorId?.mobileNo === tutors[index].mobileNo
      );
      setFilteredBatches(filteredBatches);
    } else {
      setFilteredBatches(batches);
    }
  };

  const removeBatchItem = async (index: number) => {
    try {
      const selectedBatch = filteredBatches[index];
      await deleteOrgBatch(selectedBatch);
      const clonedFilteredBatches = [...filteredBatches];
      clonedFilteredBatches.splice(index, 1);
      setFilteredBatches(clonedFilteredBatches);
      const clonedBatches = batches.filter(
        (batch) => batch.batchfriendlyname !== selectedBatch.batchfriendlyname
      );
      setOpenConfirmationModal(false);
      setBatches(clonedBatches);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const saveBatch = async () => {
    //e.preventDefault();
    if(!courses[courseIndex]) {
      setError('courseIndex', 'Invalid Data', 'Select a Course');
      return;
    }

    if (tutorIndex < 0) {
      setError('tutor', 'Invalid Data', 'Select Tutor');
      return;
    }

    if(batchDurationError){
      return
    }
    

    if (!startDate) {
      setDateError({end: dateError.end, start: 'Start date Required'})
      return;
    }

    if (!endDate) {
      setDateError({start: dateError.start, end: 'End date Required'})
      return;
    }

    if (right.length < 1) return;

    const selectedIds = right.map(list => list.id);
    const studentsNumbers = studentsList.filter(list => selectedIds.includes(list._id as string)).map(list => list.mobileNo);

    const course = courses[courseIndex];
    const boardname = course.board;
    const classname = course.className;
    const subjectname = course.subject;

    const batch: Batch = {
      students: studentsNumbers,
      boardname: boardname,
      classname: classname,
      subjectname: subjectname,
      batchfriendlyname: batchName,
      batchenddate: endDate.toLocaleDateString("fr-CA"),
      batchstartdate: startDate.toLocaleDateString("fr-CA"),
      batchicon: '',
      tutor: tutors[tutorIndex].mobileNo,
      tutorId: tutors[tutorIndex]
    };

    try {
      await createOrgBatch(batch);
      eventTracker(GAEVENTCAT.batchManagement, 'Organization Batches Create Page', 'Created Organization Batch');
      setBatches([...batches, batch]);
      setFilteredBatches([...filteredBatches, batch]);
      clearBatchDetails();
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const editBatch = (batch: Batch, batchIndex: number) => {
    setCurrentBatchIndex(batchIndex);
    setOperation(StackActionType.UPDATE);

    const currentCourseIndex =
      courses &&
      courses.findIndex(
        (course) =>
          course.board === batch.boardname &&
          course.className === batch.classname &&
          course.subject === batch.subjectname
      );
      if(currentCourseIndex===-1){
        enqueueSnackbar('Corresponding course not found. Please add this course. ', {variant:'warning'})
        return 
      }
      
    setCourseIndex(currentCourseIndex);
    setBatchName(batch.batchfriendlyname);
    setStartDate(new Date(batch.batchstartdate));
    setEndDate(new Date(batch.batchenddate));
    setBatchId(batch._id as string);

    // filter selected students from student list and
    // update in checked students

    const filteredStudents = studentsList.filter(stud =>
      (stud.boardName === batch.boardname && stud.className === batch.classname)
    )
    const left = filteredStudents.filter(list => !batch.students.includes(list.mobileNo)).map(list => {return {id: list._id as string, value: list.studentName}})
    const right = studentsList.filter(list => batch.students.includes(list.mobileNo)).map(list => {return {id: list._id as string, value: list.studentName}})

    setLeft(left);
    setRight(right);

    // get current tutor index
    const selectedTutor = batch.tutorId?.tutorName;
    const selectedTutorIndex = tutors.findIndex(
      (tutor) => tutor.tutorName === selectedTutor
    );
    setTutorIndex(selectedTutorIndex);
  };

  const updateBatch = async () => {
    if (!startDate) {
      setDateError({end: dateError.end, start: 'Start date Required'})
      return;
    }

    if (!endDate) {
      setDateError({start: dateError.start, end: 'End date Required'})
      return;
    }

    if (right.length < 1) return;
    const selectedIds = right.map(list => list.id);
    const studentsNumbers = studentsList.filter(list => selectedIds.includes(list._id as string)).map(list => list.mobileNo);

    const course = courses[courseIndex];
    const boardname = course.board;
    const classname = course.className;
    const subjectname = course.subject;

    const batchesList = [...batches];
    const filteredBatchList = [...filteredBatches];
    const previousBatch = batchesList[currentBatchIndex];
    const previousFilteredBatch = filteredBatchList[currentBatchIndex];

    try {
      const updatedBatch = {
        students: studentsNumbers,
        boardname: boardname,
        classname: classname,
        subjectname: subjectname,
        batchfriendlyname: batchName,
        batchenddate: endDate.toLocaleDateString("fr-CA"),
        batchstartdate: startDate.toLocaleDateString("fr-CA"),
        batchicon: '',
        tutor: tutors[tutorIndex].mobileNo,
        tutorId: tutors[tutorIndex],
        batchId: batchId,
        _id: batchId
      };

      batchesList[currentBatchIndex] = updatedBatch;
      filteredBatchList[currentBatchIndex] = updatedBatch;

      setBatches(batchesList);
      setFilteredBatches(filteredBatchList);

      await updateOrgBatch(updatedBatch);
      eventTracker(GAEVENTCAT.batchManagement, 'Organization Batches Create Page', 'Updated Organization Batch');
      setOperation(StackActionType.CREATE);
      clearBatchDetails();
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        batchesList[currentBatchIndex] = previousBatch;
        filteredBatchList[currentBatchIndex] = previousFilteredBatch;
        setBatches(batchesList);
        setFilteredBatches(filteredBatchList);
      }
    }
  };

  const clearBatchDetails = () => {
    setCourseIndex(-1);
    setTutorIndex(-1);
    setBatchName('');
    setStartDate(null);
    setEndDate(null);
    setRight([]);
  };
  let showClearBtn = false;
  if (
    courseIndex !== -1 ||
    tutorIndex !== -1 ||
    batchName ||
    startDate ||
    endDate ||
    left.length ||
    right.length
  ) {
    showClearBtn = true;
  }

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box
          bgcolor="white"
          marginY="20px"
          boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
          borderRadius="4px"
        >
          <Grid container>
            <Grid item xs={12} md={8}>
              <Box
                padding="20px 30px"
                display="flex"
                alignItems="center"
                borderRight="1px solid #C4C4C4"
              >
                <img src={BatchIcon} alt="Create Batch" />

                <Box marginLeft="15px">
                  <Typography component="span" color="primary">
                    <Box component="h3" className={classes.heading}>
                      {operation === StackActionType.CREATE
                        ? 'Create Batch'
                        : 'Edit Batch'}
                    </Box>
                  </Typography>
                </Box>
              </Box>

              <Box padding="0px 40px 10px 40px" borderRight="1px solid #C4C4C4">
                <form>
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Course</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={courseIndex}
                          className={errors.courseIndex ? classes.error : ''}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => setCourseIndex(e.target.value as number)}
                        >
                          <MenuItem value="-1">Select course</MenuItem>
                          {courses.map((course, index) => (
                            <MenuItem key={index} value={index}>
                              {course.board} - {course.className} -{' '}
                              {course.subject}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      {errors.courseIndex && (
                        <FormHelperText error>
                          {errors.courseIndex.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Tutor</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={tutorIndex}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => selectTutor(e.target.value as number)}
                          displayEmpty
                          className={errors.tutor ? classes.error : ''}
                        >
                          <MenuItem value="-1">Select tutor</MenuItem>
                          {tutors.map((tutor, index) => (
                            <MenuItem key={index} value={index}>
                              {tutor.tutorName}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      {errors.tutor && (
                        <FormHelperText error>
                          {errors.tutor.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Batch Name</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          name="batchName"
                          placeholder="Enter Batch Name"
                          value={batchName}
                          inputProps={{ maxLength: 50 }}
                          inputRef={register}
                          onChange={(e) => setBatchName(e.target.value)}
                          className={errors.batchName ? `${classes.error} ${classes.formInput}` : classes.formInput}
                        />
                      </FormControl>
                      {errors.batchName && (
                        <FormHelperText error>
                          {errors.batchName.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Batch Start</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl disabled={operation==StackActionType.UPDATE} fullWidth margin="normal">
                        <Datepickers selectedDate={startDate} handleDateChange={handleStartDateChange} 
                          minDate={operation==StackActionType.UPDATE ? undefined : new Date()}
                        />
                      </FormControl>
                      {dateError.start &&
                        <Typography className={classes.dateError}>{dateError.start}</Typography>
                      }
                      {batchDurationError &&
                        <Typography className={classes.dateError}>Batch Duration should be atleast 1 day</Typography>
                      }
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Batch End</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl disabled={operation==StackActionType.UPDATE} fullWidth margin="normal">
                        <Datepickers selectedDate={endDate} handleDateChange={handleEndDateChange}
                          minDate={new Date(startDate ? startDate : new Date())}
                        />
                      </FormControl>
                      {dateError.end &&
                        <Typography className={classes.dateError}>{dateError.end}</Typography>
                      }
                      {batchDurationError &&
                        <Typography className={classes.dateError}>Batch Duration should be atleast 1 day</Typography>
                      }
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={5}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Students</Box>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                      <TransferLists left={left} right={right} setLeft={setLeft} setRight={setRight} />
                    </Grid>
                  </Grid>

                  <Box
                    display="flex"
                    justifyContent="flex-end"
                    marginTop="20px"
                  >
                    {showClearBtn && (
                      <Box
                        marginRight="20px"
                        display="inline-block"
                        className={classes.clearBtn}
                      >
                        <Button
                          disableElevation
                          size="large"
                          variant="outlined"
                          onClick={() => {
                            clearBatchDetails();
                            setOperation(StackActionType.CREATE);
                          }}
                        >
                          Clear
                        </Button>
                      </Box>
                    )}

                    {operation === StackActionType.CREATE ? (
                      <Box className={classes.addBtn}>
                        <Button
                          disableElevation
                          color="primary"
                          size="large"
                          variant="contained"
                          onClick={handleSubmit(saveBatch)}
                        >
                          Save
                        </Button>
                      </Box>
                    ) : (
                      <Box className={classes.addBtn}>
                        <Button
                          disableElevation
                          color="primary"
                          size="large"
                          variant="contained"
                          onClick={updateBatch}
                        >
                          Update
                        </Button>
                      </Box>
                    )}
                  </Box>
                  <Box
                    display="flex"
                    justifyContent="flex-end"
                    marginTop="20px"
                  >
                    {errors.pageError && (
                      <FormHelperText error>
                        {errors.pageError.message}
                      </FormHelperText>
                    )}
                  </Box>
                </form>
              </Box>
            </Grid>

            <Grid item xs={12} md={4}>
              <Box
                padding="33px 30px"
                display="flex"
                alignItems="center"
                borderBottom="1px solid #C4C4C4"
              >
                <img src={ListIcon} alt="Batch List" />

                <Box
                  component="h3"
                  fontWeight={fontOptions.weight.normal}
                  fontSize={fontOptions.size.small}
                  fontFamily={fontOptions.family}
                  color="rgba(0, 0, 0, 0.5)"
                  margin="0 0 0 10px"
                >
                  List of all Batches
                </Box>
              </Box>

              <Box
                padding="20px 30px"
                maxHeight="450px"
                style={{ overflowY: 'auto' }}
              >
                {batches.map((item, index) => (
                  <OrgBatchListItem
                    key={index}
                    item={item}
                    editItem={() => editBatch(item, index)}
                    removeItem={() => {
                      setCurrentBatchIndex(index);
                      setOpenConfirmationModal(true);
                    }}
                  />
                ))}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <ConfirmationModal
        header="Delete Batch"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => setOpenConfirmationModal(false)}
        handleDelete={() => removeBatchItem(currentBatchIndex)}
      />
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(withRouter(CreateBatchOrg));
