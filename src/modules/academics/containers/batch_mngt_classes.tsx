import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect, useHistory } from 'react-router-dom';

import { connect, useDispatch } from 'react-redux';

import { RootState } from '../../../store';
import { Schedule, AppointmentSchedule } from '../contracts/schedule';
import { User, Children } from '../../common/contracts/user';
import { MeetingRoles } from '../../bbbconference/enums/meeting_roles';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  CalendarToday as CalendarIcon,
  WatchLater as ClockIcon,
  Lock as LockIcon
} from '@material-ui/icons';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

import {
  List,
  Typography,
  Link,
  Button,
  Box
} from '@material-ui/core'
import CourseBlue from '../../../assets/svgs/course-blue.svg';

import { fetchSchedulesList, fetchStudentSchedulesList, deleteTutorSchedule, fetchParentSchedulesList } from '../../common/api/academics';
import { generateSchedulerSchema, isParent } from '../../common/helpers';
import {
  deleteOrgSchedule
} from '../../common/api/schedule';
import {
  createMeeting,
  createWebHook,
  getMeetingInfo,
  joinMeeting,
  isMeetingRunning
} from '../../bbbconference/helper/api';
import {
  createNewMeeting,
  joinMeetingById
} from '../../bbbconference/store/actions';
import {
  isOrganization,
  isStudent,
  isOrgTutor, 
  eventTracker, 
  GAEVENTCAT
} from '../../common/helpers';

import axios from 'axios';
import { xml2js } from 'xml-js';
import moment from 'moment';
import { sortBy } from 'lodash';

import ConfirmationModal from '../../common/components/confirmation_modal';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tsecondary: {
      fontSize: theme.typography.pxToRem(15),
      fontFamily:fontOptions.family,
      color: '#696969'
    },
    root: {
      width: '100%'
    },
    courseName: {
      fontSize: fontOptions.size.small,fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '19px',
      color: '#405169',
      marginBottom: '8px'
    },
    batchName: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.normal,
      lineHeight: '15px',
      color: '#000000'
    },
    courseDetails: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      fontWeight: fontOptions.weight.normal,
      color: '#000000'
    },
    courseImage: {
      background: '#FBFAF9',
      borderRadius: '12px',
      padding: '12px 13px'
    },
    startBtn: {
      '& a': {
        fontSize: fontOptions.size.small,
        fontFamily:fontOptions.family,
        fontWeight: fontOptions.weight.normal,
        lineHeight: '15px',
        color: '#FFFFFF'
      }
    },
    halfOpacity: {
      opacity: '0.5'
    },
    thirdOpacity: {
      opacity: '0.75'
    }
  }),
);

interface Props {
  authUser: User,
  parentChildren: Children,
  batchID: string | undefined
}

const BatchMngtClasses: FunctionComponent<Props> = ({ authUser, batchID, parentChildren }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const [redirectTo, setRedirectTo] = useState('');
  const [reloadClasses, setReloadClasses] = useState(false);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);

  // eslint-disable-next-line
  const [scheduleTobeDeleted, setScheduleTobeDeleted] = useState('');

  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [apSchedules, setApSchedules] = useState<AppointmentSchedule[]>([]);

  useEffect(() => {
    (async () => {
      try {
        const schedulesList = ((isStudent(authUser)) ? 
          await fetchStudentSchedulesList() : 
          (isParent(authUser) ?
            (parentChildren.current ? 
              await fetchParentSchedulesList(parentChildren.current) :
              []
            ) :
            await fetchSchedulesList()
          )
        )
        const batchSchedulesList = schedulesList.filter(sched => sched.batch._id === batchID);
        // const todaySchedules = batchSchedulesList.filter(sched => sched.dayname === moment().format('dddd'));
        // const upcomingClasses = sortBy(todaySchedules.filter(sched => sched.tohour > moment().format("HH:mm")), ['fromhour']);
        const schedulerSchema = generateSchedulerSchema(batchSchedulesList)
        setSchedules(batchSchedulesList);
        setApSchedules(schedulerSchema)
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, [authUser.mobileNo, reloadClasses, parentChildren]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if(schedules.length < 1) {
    return (
      <div style={{margin: '27px 0px', textAlign: 'center'}}>
        <Typography className={classes.tsecondary}>
            No Schedules Yet
        </Typography>
        {(!isStudent(authUser) && !isOrgTutor(authUser) && !isParent(authUser)) &&
          <Typography>
            <Link
              to={{pathname: isOrganization(authUser) ? '/profile/org/schedules/create' : '/profile/tutor/schedules/create'}}
              component={RouterLink}
            >
              Click here to Create a Schedule
            </Link>
          </Typography>
        }
      </div>
    )
  }

  const join_Meeting_tutor = (sched: Schedule) => {
    if(sched) {
      const selectedSchedule = apSchedules.find(sche => sche.schedule._id === sched._id);
      if(selectedSchedule) {
        const MEETING_ID = selectedSchedule.schedule.batch?._id;
        const m_name = `${selectedSchedule.schedule.batch?.batchfriendlyname}+${selectedSchedule.title}`;
        const userName = localStorage.getItem('authUser');
        const scheduleID = selectedSchedule.schedule._id;

        if (
          MEETING_ID !== undefined &&
          m_name !== undefined &&
          scheduleID !== undefined
        ) {
          const MEETING_NAME = m_name.split(' ').join('+');
          getMeetingInfo(MEETING_ID).then((res) => {
            axios.get(res.data).then((res) => {
              const js_res: any = xml2js(res.data, { compact: true });
              if (js_res.response.returncode._text === 'FAILED') {
                //create meeting if no meeting found
                //create meeting specific hook before creating meeting
                createWebHook(MEETING_ID);
                //create meeting after hook is created
                createMeeting(MEETING_ID, MEETING_NAME, scheduleID).then((res) => {
                  dispatch(createNewMeeting(res.data));
                  if (userName !== null) joinBlock(MEETING_ID, userName);
                });
              } else {
                if (userName !== null) joinBlock(MEETING_ID, userName);
              }
            });
          });
        }
      }
    }
  }

  const joinBlock = (MEETING_ID: string, userName: string) => {
    const uuid = JSON.parse(userName)._id;
    let u_name = JSON.parse(userName).tutorName;
    u_name = u_name.split(' ');
    u_name = u_name.join('+');
    const USER_NAME = `Tutor:+${u_name}`;
    joinMeeting(MEETING_ID, USER_NAME, MeetingRoles.TUTOR, uuid).then((res) => {
      eventTracker(GAEVENTCAT.bbb, 'BBB JOIN', 'Join Meeting by Tutor');
      dispatch(joinMeetingById(res.data));
    });
  };

  const removeSchedule = async (id: string) => {
    if(id) {
      const clonedSchedule = [...schedules];
      const scheduleTbDel = clonedSchedule.find(sched => sched._id === id);
      setOpenConfirmationModal(false);
      try {
        if(scheduleTbDel) {
          isOrganization(authUser) ? await deleteOrgSchedule({
            dayname: scheduleTbDel.dayname,
            fromhour: scheduleTbDel.fromhour,
            tutorId: scheduleTbDel.tutorId?._id
          }) : await deleteTutorSchedule({
            dayname: scheduleTbDel.dayname,
            fromhour: scheduleTbDel.fromhour
          })
          setReloadClasses(prevReloadClasses => !prevReloadClasses)
        }
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
  }

  const join_Meeting_students = (sched: Schedule) => {
    if(sched) {
      const selectedSchedule = apSchedules.find(sche => sche.schedule._id === sched._id);
      if(selectedSchedule) {
        var flag = 0;
        //console.log(item.schedule.batch)
        const MEETING_NAME = `${selectedSchedule.schedule.dayname} - ${selectedSchedule.schedule.fromhour} to ${selectedSchedule.schedule.tohour} | ${selectedSchedule.schedule.batch?.batchfriendlyname} | ${selectedSchedule.title}`;
        const MEETING_ID = selectedSchedule.schedule.batch?._id;
        const userName = localStorage.getItem('authUser');
        if (MEETING_ID !== undefined && userName !== null) {
          const uuid = JSON.parse(userName)._id;
          const u_name = JSON.parse(userName).studentName;
          const user_name = u_name.split(' ');
          const USER_NAME = `${user_name.join('+')}+ID:${
            JSON.parse(userName).enrollmentId
          }`;
          //checking if meeting is running
          isMeetingRunning(MEETING_ID).then((response) => {
            const js_response: any = response.data;
            if (js_response.response.running._text === 'false') {
              //window.alert('Class yet to be started by Tutor')
              //history.push(`/meetings/${JSON.parse(userName).mobileNo}/${MEETING_ID}/${uuid}/${USER_NAME}`)
              history.push(
                `/meetings/${MEETING_ID}/${uuid}/${USER_NAME}/${MEETING_NAME}`
              );
              return;
            }
            // Check if user already present in meeting room
            getMeetingInfo(MEETING_ID).then((res) => {
              axios.get(res.data).then((res) => {
                const js_res: any = xml2js(res.data, { compact: true });
                //console.log(js_res)

                if (Array.isArray(js_res.response.attendees.attendee) === true) {
                  const members: any[] = js_res.response.attendees.attendee;
                  //console.log(members)
                  members.forEach((member) => {
                    if (member.userID._text === uuid) flag = 1;
                  });
                }
                if (flag === 0) {
                  //joining meeting
                  joinMeeting(
                    MEETING_ID,
                    USER_NAME,
                    MeetingRoles.STUDENT,
                    uuid
                  ).then((res) => {
                    eventTracker(GAEVENTCAT.bbb, 'BBB JOIN', 'Join Meeting by Student');
                    dispatch(joinMeetingById(res.data));
                  });
                } else {
                  window.alert(
                    'Multiple Instance Alert. You are already in the classroom'
                  );
                }
              });
            });
          });
        }
      }
    }
  };

  const courseIcon = (status: string) => {
    return (
      <Box marginRight="20px">
        <img
          src={CourseBlue}
          className={`${classes.courseImage} ${
            (status === 'Ongoing') ? classes.halfOpacity : ''
          }`}
          alt="Course"
        />
      </Box>
    );
  };

  const courseActionButton = (status: string, schedule: Schedule) => {
    let isActive = (status === 'Ongoing') ? true : false
    const restricted = isParent(authUser)
    isActive = !restricted && isActive
    return (
      <Box
        className={`${classes.startBtn} ${
          isActive ? classes.thirdOpacity : ''
        }`}
      >
        <Button
          disableElevation
          color="primary"
          variant="contained"
          onClick={() => {isStudent(authUser) ? join_Meeting_students(schedule) : join_Meeting_tutor(schedule)}}
          disabled={!isActive}
        >
          {`${isActive ? 'Start Class' : ''}`}
          {isActive ? (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <ArrowForwardIcon fontSize="small" />
            </Box>
          ) : (
            <Box
              component="span"
              display="flex"
              alignItems="center"
              marginLeft="5px"
            >
              <LockIcon fontSize="small" />
            </Box>
          )}
          {`${isActive ? '' : 'Start Class'}`}
        </Button>
      </Box>
    )
  }

  return (
    <div style={{margin: '27px 0px'}}>
      {(!isStudent(authUser) && !isOrgTutor(authUser) && !isParent(authUser)) &&
        <Typography style={{textAlign: 'center', marginBottom: '25px'}}>
          <Link
            to={{pathname: isOrganization(authUser) ? '/profile/org/schedules/create' : '/profile/tutor/schedules/create'}}
            component={RouterLink}
          >
            Click here to Create a new Schedule
          </Link>
        </Typography>
      }

      <List className={classes.root}>
        {schedules.map(schedule => {
          const status = (moment().format("HH:mm") >= schedule.fromhour) ? 'Ongoing' : ('Starts at ' + schedule.fromhour)
          return(
            <Box
              bgcolor="white"
              borderRadius="3px"
              marginTop="16px"
              padding="8px 20px 8px 8px"
            >
              <Box display="flex" justifyContent="space-between">
                <Box display="flex" alignItems="center">

                  {courseIcon(status)}

                  <Box marginRight="10px">
                    <Typography variant="subtitle2" className={classes.courseName}>
                      {schedule.batch && schedule.batch.classname} -{' '}
                      {schedule.batch && schedule.batch.subjectname}
                    </Typography>
                    <Typography variant="subtitle1" className={classes.batchName}>
                      {schedule.batch && schedule.batch.batchfriendlyname},{' '}
                      {schedule.batch && schedule.batch.boardname}
                    </Typography>
                  </Box>
                </Box>

                <Box display="flex" alignItems="center">
                  <Box display="flex" marginRight="15px">
                    <Box
                      component="span"
                      display="flex"
                      alignItems="center"
                      marginRight="15px"
                      className={classes.courseDetails}
                    >
                      <Box
                        component="span"
                        display="flex"
                        alignItems="center"
                        marginRight="5px"
                      >
                        <ClockIcon />
                      </Box>{' '}
                      {schedule.fromhour}
                    </Box>

                    <Box component="span" display="flex" alignItems="center">
                      <Box
                        component="span"
                        display="flex"
                        alignItems="center"
                        marginRight="5px"
                        className={classes.courseDetails}
                      >
                        <CalendarIcon />
                      </Box>{' '}
                      Today
                    </Box>
                  </Box>

                  {courseActionButton(status, schedule)}
                </Box>
              </Box>
            </Box>
          )
        })}
      </List>

      <ConfirmationModal
        header="Delete Schedule"
        helperText="This will Delete All Events in this Series. Continue?"
        openModal={openConfirmationModal}
        onClose={() => setOpenConfirmationModal(false)}
        handleDelete={() => removeSchedule(scheduleTobeDeleted)}
      />
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(BatchMngtClasses);
