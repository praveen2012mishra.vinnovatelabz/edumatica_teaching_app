//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../../store';
import { Assessment } from '../../assessment/contracts/assessment_interface';
import { User, Children } from '../../common/contracts/user';
import { OngoingAssessment } from '../../student_assessment/contracts/assessment_interface';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';

import {
  Delete as DeleteIcon
} from '@material-ui/icons';
import {
  Typography,
  Link,
  Card,
  Tooltip,
  IconButton,
  CardContent
} from '@material-ui/core';

import {
  isParent,
  isStudent
} from '../../common/helpers';
import { getAttemptAssessments, getChildAttemptAssessments } from '../../student_assessment/helper/api';
import { getAssessmentsForTutor, getAssignedAssessmentData, deleteAssessment } from '../../assessment/helper/api';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tprimary: {
      fontSize: theme.typography.pxToRem(25),
      color: 'black'
    },
    tsecondary: {
      fontSize: theme.typography.pxToRem(15),
      color: '#696969'
    },
    root: {
      width: '100%'
    }
  }),
);

interface Props {
  authUser: User,
  parentChildren: Children;
  subject: string | undefined
}

const BatchMngtAssessment:FunctionComponent<Props> = ({ authUser, subject, parentChildren }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [redirectTo, setRedirectTo] = useState('');
  const [reloadAssessment, setReloadAssessment] = useState(false);

  const [assessments, setAssessments] = useState<Assessment[]>([]);
  // eslint-disable-next-line
  const [attemptAssessments, setAttemptAssessments] = useState<OngoingAssessment[]>([]);
  const [assignedAssessments, setAssignedAssessments] = useState<string[]>([]);

  useEffect(() => {
    if(subject){
      (async () => {
        try {
          if(isStudent(authUser)) {
            const response = await getAttemptAssessments();
            setAttemptAssessments(response);
            
            const allAssessments = response.map(element => element.assessment)
            setAssessments(allAssessments.filter(element => element.subjectname === subject));
          } else if(isParent(authUser)) {
            const response = parentChildren.current ? await getChildAttemptAssessments(parentChildren.current) : [];
            setAttemptAssessments(response);
            
            const allAssessments = response.map(element => element.assessment)
            setAssessments(allAssessments.filter(element => element.subjectname === subject));
          } else {
            const allAssessments = await getAssessmentsForTutor(false);
            setAssessments(allAssessments.filter(element => element.subjectname === subject));

            const response = await getAssignedAssessmentData();
            setAssignedAssessments(response);
          }
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
    }
  }, [subject, reloadAssessment, authUser]);

  const handleDeleteAction = (data: Assessment) => {
    if (assignedAssessments.includes(data._id)) {
      enqueueSnackbar(
        'Assessment has been assigned to some students. Cannot be Deleted',
        { variant: 'warning' }
      );
    } else {
      (async () => {
        try {
          await deleteAssessment('?assessmentId=' + encodeURI(data._id));
          setReloadAssessment(prevReloadAssessment => !prevReloadAssessment)
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      })();
    }
  }
  
  const buttonData = [
    {
      title: 'Delete',
      action: handleDeleteAction,
      icon: <DeleteIcon />
    }
  ];

  if(isStudent(authUser) || isParent(authUser)) {
    buttonData.pop();
  }

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'assessmentname', headerName: 'Name', flex: 1, renderCell: datagridCellExpand  },
    { field: 'totalMarks', headerName: 'Marks', flex: 1, renderCell: datagridCellExpand },
    { field: 'duration', headerName: 'Duration', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          assessmentname: params.getValue("assessmentname") as string
        }

        const selectedRowDetails = assessments.find((row, index) => {
          return (row.assessmentname === selectedRow.assessmentname && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as Assessment);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  if(isStudent(authUser) || isParent(authUser)) {
    gridColumns.pop();
    gridColumns.push({ field: 'isSubmitted', headerName: 'Status', flex: 1 })
  }

  const gridRows = assessments.map((row, index) => {
    const thisAssessment = attemptAssessments.find(list => list.assessment._id === row._id) as OngoingAssessment
    const statusObject = (isStudent(authUser) || isParent(authUser)) ? { isSubmitted: thisAssessment.isSubmitted ? 'Submitted' : 'Not Submitted' } : {}
    return ({...{
      id: (index + 1),
      assessmentname: row.assessmentname,
      totalMarks: row.totalMarks,
      duration: row.duration
    }, ...statusObject})
  })

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  if(assessments.length < 1) {
    return (
      <div style={{margin: '27px 0px', textAlign: 'center'}}>
        <Typography className={classes.tsecondary}>
            No Assessment Yet
        </Typography>
        {(!isStudent(authUser) && !isParent(authUser)) &&
          <Typography>
            <Link
              to={{pathname: '/profile/assessment/create'}}
              component={RouterLink}
            >
              Click here to Create an Assessment
            </Link>
          </Typography>
        }
      </div>
    )
  }

  return (
    <div style={{margin: '0px 0px'}}>
      {(!isStudent(authUser) && !isParent(authUser)) &&
        <Typography style={{textAlign: 'center', marginBottom: '25px'}}>
          <Link
            to={{pathname: '/profile/assessment/create'}}
            component={RouterLink}
          >
            Click here to Create an Assessment
          </Link>
        </Typography>
      }

      <Card variant="outlined" style={{margin: '20px 0px'}}>
        <CardContent style={{padding:'0'}}>
          <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
        </CardContent>
      </Card>
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(BatchMngtAssessment);