//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link as RouterLink, RouteComponentProps, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { RootState } from '../../../store';
import { Batch } from '../contracts/batch';
import { User, Children } from '../../common/contracts/user';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import {
  Delete as DeleteIcon,
  Add as AddIcon,
  Search as SearchIcon,
  Clear as ClearIcon,
} from '@material-ui/icons';
import { 
  IconButton,
  Tooltip,
  Container,
  Box,
  Typography,
  Grid,
  Button,
  FormControl,
  Input,
  InputAdornment
} from '@material-ui/core';
import BatchIcon from '../../../assets/images/batch.png';

import { fetchBatchesList, deleteTutorBatch } from '../../common/api/academics';
import { deleteOrgBatch, fetchOrgBatchesList, fetchStudentBatchesList } from '../../common/api/batch';

import {
  isOrganization,
  isStudent,
  isAdminTutor,
  isOrgTutor,
  isParent
} from '../../common/helpers';
import MiniDrawer from '../../common/components/sidedrawer';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'
import ConfirmationModal from '../../common/components/confirmation_modal';
import { fontOptions } from '../../../theme';
import { eventTracker, GAEVENTCAT } from '../../common/helpers';

interface Data {
  boardname: string;
  subjectname: string;
  classname: string;
  batchfriendlyname: string;
  id: string;
  tutor: string;
}

function createData(
  batchfriendlyname: string,
  boardname: string,
  classname: string,
  subjectname: string,
  id: string,
  tutor: string
): Data {
  return { batchfriendlyname, boardname, classname, subjectname, id, tutor };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    tprimary: {
      fontSize: theme.typography.pxToRem(30),
      fontFamily:fontOptions.family,
      color: '#696969'
    },
    addBtn: {
      '& svg': {
        marginRight: '5px'
      }
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#212121'
    },
    floatRight: {
      float: 'right'
    }
  }),
);

interface RedirectProp {
  pathname: string;
  state: {
    batch: string;
    id: string;
  }
}

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
  parentChildren: Children;
}

const Batches: FunctionComponent<Props> = ({ authUser, parentChildren }) => {
  const classes = useStyles();

  const [redirectTo, setRedirectTo] = useState<RedirectProp | string>('');

  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [batchTobeDeleted, setBatchTobeDeleted] = useState('');
  const [reloadBatch, setReloadBatch] = useState(false);
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);

  const [batches, setBatches] = useState<Batch[]>([]);

  eventTracker(GAEVENTCAT.batchManagement, 'Batch Management Page', 'Landed Batch Management Page');

  useEffect(() => {
    (async () => {
      // if(focused === false) {
        try {
          let batchesList = (isOrganization(authUser) ?
            await fetchOrgBatchesList() : 
            (isStudent(authUser) ? 
              await fetchStudentBatchesList(authUser._id ? authUser._id : '') : 
              (isParent(authUser) ? 
                (parentChildren.current ? 
                  await fetchStudentBatchesList(parentChildren.current ? parentChildren.current : '') : 
                  []
                ) :
                await fetchBatchesList()
              )
            )
          );
          if(searchText !== '') {
            batchesList = batchesList.filter(bat => 
              bat.batchfriendlyname.toLowerCase().includes(searchText.toLowerCase()) ||
              bat.boardname.toLowerCase().includes(searchText.toLowerCase()) ||
              bat.classname.toLowerCase().includes(searchText.toLowerCase()) ||
              bat.subjectname.toLowerCase().includes(searchText.toLowerCase())
            )
          }
          setBatches(batchesList);
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      // }
    })();
    // eslint-disable-next-line
  }, [authUser.mobileNo, reloadBatch, searchText, parentChildren]);

  if (((redirectTo as string).length && (redirectTo as string).length > 0) || ((redirectTo as RedirectProp).pathname)) {
    return <Redirect to={redirectTo} />;
  }

  const expandBatch = (selectedRowDetails: Data) => {
    if(selectedRowDetails.batchfriendlyname) {
      setRedirectTo({pathname: '/profile/batches/expand', state: { batch: selectedRowDetails.batchfriendlyname, id: selectedRowDetails.id }})    
    }
  }

  const initRemoveBatch = (selectedRowDetails: Data) => {
    if(selectedRowDetails.id) {
      setBatchTobeDeleted(selectedRowDetails.id);
      setOpenConfirmationModal(true);
    }
  }

  const buttonData = [
    { title: 'Expand',
      action: expandBatch,
      icon: <ArrowForwardIcon />
    },
    {
      title: 'Delete',
      action: initRemoveBatch,
      icon: <DeleteIcon />
    }
  ];

  if(isStudent(authUser) || isOrgTutor(authUser) || isParent(authUser)) {
    buttonData.pop();
  }

  const rows = batches.map((batch) => {
    return(
      createData(
        batch.batchfriendlyname,
        batch.boardname,
        batch.classname,
        batch.subjectname,
        batch._id ? batch._id : '',
        batch.tutorId ? batch.tutorId.tutorName : ''
      )
    )
  });

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'batchfriendlyname', headerName: 'Batch', flex: 1, renderCell: datagridCellExpand },
    { field: 'boardname', headerName: 'Board', flex: 1, renderCell: datagridCellExpand },
    { field: 'classname', headerName: 'Class', flex: 1, renderCell: datagridCellExpand },
    { field: 'subjectname', headerName: 'Subject', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          batchfriendlyname: params.getValue("batchfriendlyname") as string,
          boardname: params.getValue("boardname") as string
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.batchfriendlyname === selectedRow.batchfriendlyname && row.boardname === selectedRow.boardname && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as Data);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  if(isStudent(authUser) || isParent(authUser) || isOrganization(authUser)) {
    const poped = gridColumns.pop();
    gridColumns.push({ field: 'tutor', headerName: 'Tutor', flex: 1 })
    gridColumns.push(poped as GridColDef)
  }

  const gridRows = rows.map((row, index) => {
    const tutorObject = (isStudent(authUser) || isParent(authUser) || isOrganization(authUser)) ? { tutor: row.tutor } : {}
    return ({...{
      id: (index + 1),
      batchfriendlyname: row.batchfriendlyname,
      boardname: row.boardname,
      classname: row.classname,
      subjectname: row.subjectname
    }, ...tutorObject})
  })

  const removeBatch = async (id: string) => {
    if(id) {
      const clonedBatches = [...batches];
      const batchTbDel = clonedBatches.find(batch => batch._id === id);
      setOpenConfirmationModal(false);
      try {
        if(batchTbDel) {
          isOrganization(authUser) ? await deleteOrgBatch(batchTbDel) : await deleteTutorBatch(batchTbDel);
          setReloadBatch(prevReloadBatch => !prevReloadBatch)
        }
      } catch (error) {
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box bgcolor="white" marginY="20px">
          <Grid container>
            <Grid item xs={12} md={7} justify="space-between">
              <Box padding="20px 30px" display="flex" alignItems="center">
                <img src={BatchIcon} alt="Person with Book" />

                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Batches
                    </Box>
                  </Typography>

                  {(isAdminTutor(authUser) || isOrganization(authUser)) &&
                    <Box marginLeft="10px" display="flex">
                      <Box marginLeft="20px" className={classes.addBtn}>
                        <Button
                          disableElevation
                          variant="contained"
                          color="primary"
                          size="large"
                          component={RouterLink}
                          to={{pathname: isOrganization(authUser) ? '/profile/org/batches/create' : '/profile/tutor/batches/create'}}
                        >
                          <Box display="flex" alignItems="center">
                            <AddIcon />
                          </Box>{' '}
                          Add Batch
                        </Button>
                      </Box>
                    </Box>
                  }
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={5} justify="flex-end">
              <Box display="flex"
                alignItems="center"
                marginRight="20px"
                className={classes.floatRight}
              >
                <Box margin="15px 10px 10px 25px">
                  <FormControl fullWidth margin="normal">
                    <Input
                      name="search"
                      inputProps={{ inputMode: 'search' }}
                      onFocus={() => setFocused(true)} 
                      onBlur={() => setFocused(false)}
                      placeholder="Search"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                      }}
                      endAdornment={
                        searchText.trim() !== '' ? (
                          <InputAdornment position="end">
                            <IconButton size="small" onClick={() => setSearchText('')}>
                              <ClearIcon />
                            </IconButton>
                          </InputAdornment>
                        ) : (
                          <InputAdornment position="end">
                            <IconButton disabled size="small">
                              <SearchIcon />
                            </IconButton>
                          </InputAdornment>
                        )
                      }
                    />
                  </FormControl>
                  {(searchText.trim() !== '') &&
                    <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666'}}>
                      Filtered Table using Keyword '{searchText}'
                    </Typography>
                  }
                </Box>
              </Box>
            </Grid>
          </Grid>
          <Datagrids gridRows={gridRows} gridColumns={gridColumns} disableCheckbox={true} />
        </Box>
      </Container>

      <ConfirmationModal
        header="Delete Batch"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => setOpenConfirmationModal(false)}
        handleDelete={() => removeBatch(batchTobeDeleted)}
      />
      </MiniDrawer>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User,
  parentChildren: state.authReducer.parentChildren as Children
});

export default connect(mapStateToProps)(Batches);
