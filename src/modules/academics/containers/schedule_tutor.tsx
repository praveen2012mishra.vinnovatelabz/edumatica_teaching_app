import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import {
  Box,
  Container,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  FormHelperText,
  Select,
  TextField,
  Typography
} from '@material-ui/core';
import {
  Add as AddIcon,
  RemoveCircleOutline as RemoveCircleIcon,
} from '@material-ui/icons';
import GroupWorkIcon from '../../../assets/images/group-icon.png';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import QueryBuilderRoundedIcon from '@material-ui/icons/QueryBuilderRounded';
import {
  createTutorSchedule,
  deleteTutorSchedule,
  fetchBatchesList,
  fetchSchedulesList,
  updateTutorSchedule
} from '../../common/api/academics';
import { RootState } from '../../../store';
import { Schedule } from '../contracts/schedule';
import { Weekday } from '../../common/enums/weekday';
import { User } from '../../common/contracts/user';
import Calender from '../../../assets/images/schedule-calendar.png';
import CalenderCircle from '../../../assets/svgs/calender-circle.svg';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import ConfirmationModal from '../../common/components/confirmation_modal';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { Batch } from '../contracts/batch';
import DetailedCard from '../../common/components/detailedcards';
import {useSnackbar} from "notistack"
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#666666'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      marginTop: '5px',
      color: '#1C2559'
    },
    formInput: {
      lineHeight: '18px',
      color: '#151522'
    },
    checkLabel: {
      '& .MuiFormControlLabel-label': {
        fontSize: fontOptions.size.small,
        fontFamily:fontOptions.family,
        lineHeight: '19px',
        letterSpacing: '0.25px',
        color: '#202842'
      }
    },
    addBtn: {
      '& button': {
        padding: '8px 12px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        fontFamily:fontOptions.family,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          fontSize: fontOptions.size.medium,
          fontFamily:fontOptions.family,
          marginRight: '5px'
        }
      }
    },
    clearBtn: {
      '& button': {
        border: '1px solid #666666',
        color: '#666666',
        padding: '10px 35px'
      }
    },
    removeIcon: {
      color: '#666666'
    },
    editScheduleIcon: {
      color: '#666666'
    },
    removeScheduleIcon: {
      color: '#F47C7B'
    },
    tableHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      lineHeight: '20px',
      textTransform: 'uppercase',
      color: '#666666'
    },
    tableData: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      lineHeight: '20px',
      textTransform: 'capitalize',
      letterSpacing: '1px',
      color: '#000000'
    },
    saveBtn: {
      '& button': {
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '5px',
        padding: '14px 55px',
        fontWeight: fontOptions.weight.bold,
        fontFamily:fontOptions.family,
        fontSize: fontOptions.size.medium
      }
    },
    cancelBtn: {
      marginRight: '10px',

      '& button': {
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '5px',
        background: '#FFFFFF',
        border: '1px solid #666666',
        lineHeight: '18px',
        color: '#666666',
        padding: '14px 55px',
        fontWeight: fontOptions.weight.normal,
        fontFamily:fontOptions.family,
        fontSize: fontOptions.size.small
      }
    },
    dayName: {
      fontWeight: fontOptions.weight.normal,
      fontFamily:fontOptions.family,
      fontSize: fontOptions.size.medium,
      color: '#405169',
      margin: '0 0 0 20px'
    },
    scheduleName: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      lineHeight: '18px',
      color: '#365969',
      marginBottom: '13px'
    },
    scheduleDetails: {
      fontWeight: fontOptions.weight.normal,
      fontFamily:fontOptions.family,
      fontSize: fontOptions.size.small,
      lineHeight: '16px',

      color: '#333333'
    }
  })
);

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
}

interface FormData {
  pageError: string;
  serverError: string;
}

const CreateScheduleTutor: FunctionComponent<Props> = ({ authUser }) => {
  const {enqueueSnackbar} = useSnackbar()

  const { errors, setError, clearError } = useForm<FormData>({
    mode: 'onBlur'
  });
  const [batchIndex, setBatchIndex] = useState(0);
  const [day, setDay] = useState(Weekday.MONDAY);
  const [sessionStartTime, setSessionStartTime] = useState('06:00');
  const [sessionEndTime, setSessionEndTime] = useState('07:00');

  const [batches, setBatches] = useState<Batch[]>([]);
  const [draftSchedules, setDraftSchedules] = useState<Schedule[]>([]);
  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [currentScheduleIndex, setCurrentScheduleIndex] = useState(-1);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const [editMode, setEditMode] = useState<string | boolean>(false);

  const classes = useStyles();
  eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Landed Tutor Schedule');

  useEffect(() => {
    // Redirect the user to error location if he is not accessing his own
    // profile.
    if (!authUser.mobileNo) {
      setRedirectTo(`/profile/personal-information`);
      return;
    }

    (async () => {
      try {
        if(editMode === false) {
          const batchesListResponse = fetchBatchesList();
          const schedulesListResponse = fetchSchedulesList();

          const [batchesList, schedulesList] = await Promise.all([
            batchesListResponse,
            schedulesListResponse
          ]);

          setBatches(batchesList);
          setSchedules(schedulesList);
        }
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, [authUser.mobileNo, editMode]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const addDraftSchedule = (e: React.FormEvent) => {
    e.preventDefault();
    clearError('pageError');
    if (sessionEndTime <= sessionStartTime) {
      setError(
        'pageError',
        'Invalid Data',
        'End time cannot be less than or equal to start time'
      );
      return;
    }
    if (
      draftSchedules.filter(
        (schedule) =>
          schedule.batch?.batchfriendlyname ===
            batches[batchIndex].batchfriendlyname &&
          schedule.fromhour === sessionStartTime &&
          schedule.tohour === sessionEndTime &&
          schedule.dayname === day
      ).length > 0
    ) {
      setError('pageError', 'Invalid Data', 'Schedule already added');
      return;
    } else {
      clearError('pageError');
    }
    setDraftSchedules([
      ...draftSchedules,
      {
        mobileNo: authUser.mobileNo,
        dayname: day,
        fromhour: sessionStartTime,
        tohour: sessionEndTime,
        batch: batches[batchIndex]
      }
    ]);
  };

  const removeDraftSchedule = (scheduleIndex: number) => {
    const draftSchedulesCloned = [...draftSchedules];

    draftSchedulesCloned.splice(scheduleIndex, 1);

    setDraftSchedules(draftSchedulesCloned);
  };

  const removeSchedule = async (scheduleIndex: number) => {
    try {
      const clonedSchedules = [...schedules];

      const schedule = clonedSchedules[scheduleIndex];

      clonedSchedules.splice(scheduleIndex, 1);

      setSchedules(clonedSchedules);
      setOpenConfirmationModal(false);
      await deleteTutorSchedule({
        dayname: schedule.dayname,
        fromhour: schedule.fromhour
      });
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const editSchedule = (schedule: Schedule, index: number) => {
    if(schedule._id) {
      setEditMode(schedule._id);
      setDay(schedule.dayname as Weekday);
      setBatchIndex(batches.findIndex(batch => batch._id === schedule.batch?._id))
      setSessionStartTime(schedule.fromhour);
      setSessionEndTime(schedule.tohour);
      eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Started Updation of Tutor Schedule');
    }
  }

  const updateSchedule = async() => {
    if(sessionStartTime >= sessionEndTime) {
      enqueueSnackbar("End time should be greater than Start Time", { variant: "warning" })
    } else if(editMode && day && batches[batchIndex] && sessionStartTime && sessionEndTime) {
      try {
        const updatedSchedule = {
          scheduleId: editMode as string,
          dayname: day,
          batch: batches[batchIndex]._id,
          fromhour: sessionStartTime,
          tohour: sessionEndTime
        };

        await updateTutorSchedule(updatedSchedule);
        eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Updated Tutor Schedule');
        setDay(Weekday.MONDAY);
        setBatchIndex(0)
        setSessionStartTime('06:00');
        setSessionEndTime('07:00')
        setEditMode(false);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    }
  }

  const saveDraftSchedules = async () => {
    clearError('serverError');
    let clonedBatches: Batch[] = [];

    draftSchedules.forEach((draftSchedule) => {
      const structuredDraftScheduleIndex = clonedBatches.findIndex(
        (structuredDraftSchedule) =>
          draftSchedule.batch &&
          structuredDraftSchedule.boardname === draftSchedule.batch.boardname &&
          structuredDraftSchedule.classname === draftSchedule.batch.classname &&
          structuredDraftSchedule.subjectname ===
            draftSchedule.batch.subjectname &&
          structuredDraftSchedule.batchfriendlyname ===
            draftSchedule.batch.batchfriendlyname
      );

      const schedule = {
        fromhour: draftSchedule.fromhour,
        tohour: draftSchedule.tohour,
        dayname: draftSchedule.dayname
      };

      if (
        structuredDraftScheduleIndex > -1 &&
        clonedBatches[structuredDraftScheduleIndex] &&
        clonedBatches[structuredDraftScheduleIndex].schedules
      ) {
        clonedBatches[structuredDraftScheduleIndex].schedules = [
          ...(clonedBatches[structuredDraftScheduleIndex]
            .schedules as Schedule[]),
          schedule
        ];
      } else {
        const clonedBatch = batches[batchIndex];

        clonedBatch.schedules = [schedule];

        clonedBatches = [...clonedBatches, clonedBatch];
      }
    });

    clonedBatches.forEach(async (clonedBatch) => {
      try {
        await createTutorSchedule({
          batchfriendlyname: clonedBatch.batchfriendlyname,
          schedules: clonedBatch.schedules as Schedule[]
        });
        eventTracker(GAEVENTCAT.schedule, 'Tutor Schedule', 'Created Tutor Schedule');
        setSchedules([...schedules, ...draftSchedules]);
        setDay(Weekday.MONDAY);
        setBatchIndex(0)
        setSessionStartTime('06:00');
        setSessionEndTime('07:00')
        setDraftSchedules([]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    });
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box bgcolor="white" marginY="20px">
          <Grid container>
            <Grid item xs={12} md={8}>
              <Box
                padding="20px 30px"
                display="flex"
                alignItems="center"
                borderBottom="1px solid rgba(224, 224, 224, 0.5)"
                borderRight="1px solid #C4C4C4"
              >
                <img src={Calender} alt="Calender" />

                <Box marginLeft="20px">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Add Schedule
                    </Box>
                  </Typography>
                </Box>
              </Box>

              <Box padding="20px 30px" borderRight="1px solid #C4C4C4">
                <form onSubmit={addDraftSchedule}>
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Day</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={day}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => setDay(e.target.value as Weekday)}
                        >
                          <MenuItem value="Monday">Monday</MenuItem>
                          <MenuItem value="Tuesday">Tuesday</MenuItem>
                          <MenuItem value="Wednesday">Wednesday</MenuItem>
                          <MenuItem value="Thursday">Thursday</MenuItem>
                          <MenuItem value="Friday">Friday</MenuItem>
                          <MenuItem value="Saturday">Saturday</MenuItem>
                          <MenuItem value="Sunday">Sunday</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Batch Name</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={batchIndex}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => setBatchIndex(e.target.value as number)}
                        >
                          {batches.map((option, index) => (
                            <MenuItem key={index} value={index}>
                              {option.batchfriendlyname} - {option.boardname} - {option.classname} -{' '}
                              {option.subjectname}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Session Start Time</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <TextField
                          type="time"
                          value={sessionStartTime}
                          onChange={(e) => setSessionStartTime(e.target.value)}
                          InputLabelProps={{
                            shrink: true
                          }}
                          inputProps={{
                            step: 1800 // 30 min
                          }}
                          className={classes.formInput}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={classes.label}>Session End Time</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <TextField
                          type="time"
                          value={sessionEndTime}
                          onChange={(e) => setSessionEndTime(e.target.value)}
                          InputLabelProps={{
                            shrink: true
                          }}
                          inputProps={{
                            step: 1800 // 30 min
                          }}
                          className={classes.formInput}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>

                  <FormControl fullWidth margin="normal">
                    <Box display="flex" justifyContent="flex-end">
                      <Box className={classes.addBtn}>
                        {!editMode &&
                          <Button
                            disableElevation
                            color="primary"
                            size="small"
                            variant="contained"
                            type="submit"
                          >
                            <AddIcon /> Add
                          </Button>
                        }
                      </Box>
                    </Box>
                  </FormControl>
                  <Box display="flex" justifyContent="flex-end">
                    {errors.pageError && (
                      <FormHelperText error>
                        {errors.pageError.message}
                      </FormHelperText>
                    )}
                  </Box>
                </form>

                {draftSchedules && draftSchedules.length > 0 && (
                  <Box>
                    <Box
                      borderBottom="1px dashed #EAE9E4"
                      borderTop="1px dashed #EAE9E4"
                      marginY="20px"
                      paddingY="18px"
                    >
                      <Box marginBottom="10px">
                        <Grid container>
                          <Grid item xs={3}>
                            <Box className={classes.tableHeading}>DAY</Box>
                          </Grid>

                          <Grid item xs={3}>
                            <Box className={classes.tableHeading}>
                              BATCH NAME
                            </Box>
                          </Grid>

                          <Grid item xs={6}>
                            <Box className={classes.tableHeading}>
                              SESSION TIMINGS
                            </Box>
                          </Grid>
                        </Grid>
                      </Box>

                      {draftSchedules.map((draftSchedule, index) => (
                        <Box marginTop="5px" key={index}>
                          <Grid container alignItems="center">
                            <Grid item xs={3}>
                              <Box className={classes.tableData}>
                                {draftSchedule.dayname}
                              </Box>
                            </Grid>

                            <Grid item xs={3}>
                              <Box className={classes.tableData}>
                                {draftSchedule.batch &&
                                  draftSchedule.batch.batchfriendlyname}
                              </Box>
                            </Grid>

                            <Grid item xs={4}>
                              <Box className={classes.tableData}>
                                {draftSchedule.fromhour} -{' '}
                                {draftSchedule.tohour}
                              </Box>
                            </Grid>

                            <Grid item xs={2}>
                              <Box display="flex">
                                <IconButton
                                  size="small"
                                  onClick={() => removeDraftSchedule(index)}
                                >
                                  <RemoveCircleIcon
                                    className={classes.removeIcon}
                                  />
                                </IconButton>
                              </Box>
                            </Grid>
                          </Grid>
                        </Box>
                      ))}
                    </Box>
                  </Box>
                )}

                <Box display="flex" justifyContent="flex-end" marginTop="20px">
                  <Box className={classes.cancelBtn}>
                    <Button
                      disableElevation
                      size="large"
                      variant="contained"
                      onClick={() => {
                        setDay(Weekday.MONDAY);
                        setSessionStartTime('06:00');
                        setSessionEndTime('07:00')
                        setEditMode(false);
                      }}
                    >
                      Cancel
                    </Button>
                  </Box>

                  <Box className={classes.saveBtn}>
                    {!editMode &&
                      <Button
                        disableElevation
                        color="primary"
                        size="large"
                        variant="contained"
                        onClick={saveDraftSchedules}
                      >
                        Add
                      </Button>
                    }
                    {editMode &&
                      <Button
                        disableElevation
                        color="primary"
                        size="large"
                        variant="contained"
                        onClick={updateSchedule}
                      >
                        Update
                      </Button>
                    }
                  </Box>
                </Box>
                <Box display="flex" justifyContent="flex-end">
                  {errors.serverError && (
                    <FormHelperText error>
                      {errors.serverError.message}
                    </FormHelperText>
                  )}
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={4}>
              <Box
                padding="25px 30px"
                display="flex"
                alignItems="center"
                borderBottom="1px solid rgba(224, 224, 224, 0.5)"
              >
                <img src={CalenderCircle} alt="Calender" />

                <Box component="h3" className={classes.dayName}>
                  {day}
                </Box>
              </Box>

              <Box
                padding="20px 30px"
                maxHeight="500px"
                style={{ overflowY: 'auto' }}
              >
                {schedules
                  .filter(
                    (schedule) =>
                      schedule.dayname.toLowerCase() === day.toLowerCase()
                  )
                  .map((schedule, index) => (
                    <DetailedCard bgcolor="#C1E3F2" 
                      heading={schedule.batch?.classname + ' - ' + schedule.batch?.subjectname}
                      subheading={[{icon: [<img src={GroupWorkIcon} alt="Batch Icon" />], text: schedule.batch?.batchfriendlyname as string}]} 
                      fullcontent={{icon: [<QueryBuilderRoundedIcon />], text: 
                        [{head: 'Schedule Start', value: schedule.fromhour}, 
                        {head: 'Schedule End', value: schedule.tohour}]
                      }} 
                      editItem={() => editSchedule(schedule, index)} 
                      removeItem={() => { setCurrentScheduleIndex(index); setOpenConfirmationModal(true) }}
                    />
                  ))}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <ConfirmationModal
        header="Delete Schedule"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => setOpenConfirmationModal(false)}
        handleDelete={() => removeSchedule(currentScheduleIndex)}
      />
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(withRouter(CreateScheduleTutor));
