import { ChapterContent } from './chapter_content';

export interface Chapter {
  name: string;
  isMaster: boolean;
  contents : ChapterContent[]
  orderNo?: number;
}
