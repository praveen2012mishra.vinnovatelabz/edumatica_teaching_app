import { combineReducers, createReducer } from '@reduxjs/toolkit';
import { setSubjects, setTutorSubjects } from './actions';
import { Subject } from '../contracts/subject';
import { Course } from '../contracts/course';
import { RootState } from '../../../store';
import { AnyAction } from 'redux'

const SUBJECTS: Subject[] = [];
const subjects = createReducer(SUBJECTS, {
  [setSubjects.type]: (_, action) => action.payload,
});

const TUTOR_SUBJECTS: Course[] = [];
const tutorSubjects = createReducer(TUTOR_SUBJECTS, {
  [setTutorSubjects.type]: (_, action) => action.payload,
});

const initState = {
  subjects: SUBJECTS,
  tutorSubjects: TUTOR_SUBJECTS
};

const appReducer = combineReducers({
  subjects,
  tutorSubjects,
})

export const academicsReducer = (state: RootState, action: AnyAction) => {
  if (action.type === 'USER_LOGOUT') {
    state = initState
  }

  return appReducer(state, action)
}
