import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect  } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import {
  Box,
  Container,
  Divider,
  FormControl,
  Grid,
  FormHelperText,
  Input,
  Typography,
  TextField,
  Select,
  MenuItem
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { RootState } from '../../../store';
import { updateOrgTutor } from '../../common/api/organization';
import { Tutor } from '../../common/contracts/user';
import StudentWithMonitor from '../../../assets/svgs/student-with-monitor-light.svg';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { CurrentTutor } from '../contracts/tutor';
import { Redirect } from 'react-router-dom';
import {
  fetchQualificationsList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode
} from '../../common/api/academics';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import {
  NAME_PATTERN,
  PHONE_PATTERN,
  EMAIL_PATTERN,
  PIN_PATTERN
} from '../../common/validations/patterns';
import { useSnackbar } from 'notistack';

interface Props
  extends RouteComponentProps<{ mode: string; username: string }> {
  currentTutor: CurrentTutor;
}

interface SelectOption {
  title: string;
  value: string;
}

interface FormData {
  pageError: string;
  serverError: string;
  tutorName: string;
  enrollmentId: string;
  mobileNo: string;
  emailId: string;
  qualifications: string;
  schoolName: string;
  cityName: string;
  stateName: string;
  pinCode: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

const ValidationSchema = yup.object().shape({
  tutorName: yup.string().required('Tutor number is a required field').min(5)
});

const TutorUpdate: FunctionComponent<Props> = ({ currentTutor, match }) => {
  const disableAll = match.params.mode === 'view' ? true : false;
  const { errors, setError, clearError } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const classes = useStyles();
  eventTracker(GAEVENTCAT.teacherProfileCreate, 'Edit Tutor Page', 'Landed Edit Tutor Page');
  
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [name, setName] = useState('');
  const [enrollmentId, setEnrollmentId] = useState<string | number>('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [pinCode, setPinCode] = useState('');
  const [cityName, setCityName] = useState('');
  const [stateName, setStateName] = useState('');
  const [qualification, setQualification] = useState('');
  const [qualificationsList, setQualificationsList] = useState<SelectOption[]>([]);
  const [schoolsList, setSchoolsList] = useState<SelectOption[]>([]);
  const [school, setSchool] = useState('');
  const [tutorId, setTutorId] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const qualificationsListResponse = await fetchQualificationsList();

        const structuredQualificationsList = qualificationsListResponse.map(
          (qualification) => ({
            title: `${qualification.degree} (${qualification.subjectName})`,
            value: qualification.degree
          })
        );

        setQualificationsList([
          ...structuredQualificationsList,
          { title: 'Other', value: 'Other' }
        ]);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: currentTutor.cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);

        setName(currentTutor.name);
        setEnrollmentId(
          currentTutor.enrollmentId ? currentTutor.enrollmentId : '-'
        );
        setPhone(currentTutor.mobileNo);
        setEmail(currentTutor.email);
        setPinCode(currentTutor.pinCode);
        setCityName(currentTutor.cityName);
        setStateName(currentTutor.stateName);
        setQualification(currentTutor.qualifications);
        setSchool(currentTutor.schoolname)
        if(currentTutor.tutorId) setTutorId(currentTutor.tutorId);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, []);

  if (redirectTo && redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchool('');
        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
          setCityName('');
          setStateName('');
          setSchoolsList([]);
        }
      }
    } else {
      setCityName('');
      setStateName('');
      setSchoolsList([]);
    }
  };

  const saveTutors = async () => {
    if (!name.length) {
      setError('tutorName', 'Invalid Data', 'Tutor Name cannot be empty');
      return;
    } else {
      clearError('tutorName');
    }
    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'tutorName',
        'Invalid Data',
        'Tutor Name cannot be less than 5 character excluding spaces'
      );
      return;
    } else {
      clearError('tutorName');
    }

    if (!NAME_PATTERN.test(name)) {
      setError('tutorName', 'Invalid Data', 'Invalid tutor name');
      return;
    } else {
      clearError('tutorName');
    }

    if (!phone.length) {
      setError('mobileNo', 'Invalid Data', 'Mobile number cannot be empty');
      return;
    } else {
      clearError('mobileNo');
    }
    if (!phone.match(PHONE_PATTERN)) {
      setError('mobileNo', 'Invalid Data', 'Invalid mobile number');
      return;
    } else {
      clearError('mobileNo');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email id cannot be empty');
      return;
    } else {
      clearError('emailId');
    }
    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (qualification == null) {
      setError(
        'qualifications',
        'Invalid Data',
        'qualification cannot be empty'
      );
      return;
    } else {
      clearError('qualifications');
    }
    if (qualification.length < 3) {
      setError(
        'qualifications',
        'Invalid Data',
        'qualification must be minimum 3 characters long'
      );
      return;
    } else {
      clearError('qualifications');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'Pin Code cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    if (!PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid pin code');
      return;
    } else {
      clearError('pinCode');
    }

    if (!cityName.length) {
      setError('cityName', 'Invalid Data', 'City name cannot be empty');
      return;
    } else {
      clearError('cityName');
    }

    if (!stateName.length) {
      setError('stateName', 'Invalid Data', 'State name cannot be empty');
      return;
    } else {
      clearError('stateName');
    }

    if (!school.length) {
      setError('schoolName', 'Invalid Data', 'School cannot be empty');
      return;
    } else {
      clearError('schoolName');
    }

    const tutor: Tutor = {
      mobileNo: phone,
      emailId: email,
      qualifications: qualification ? [qualification] : [],
      schoolName: school ? school : '',
      tutorName: name,
      enrollmentId: enrollmentId !== '' ? enrollmentId : '',
      pinCode: pinCode ? pinCode : '',
      cityName: cityName ? cityName : '',
      stateName: stateName ? stateName : '',
      courseDetails: [],
      _id: tutorId,
    };

    clearError('serverError');
    try {
      await updateOrgTutor(tutor);
      eventTracker(GAEVENTCAT.teacherProfileCreate, 'Edit Tutor Page', 'Tutor Updated by Organization')
      setRedirectTo(`/profile/tutors`);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="md">
        <Box bgcolor="white" marginY="20px">
          <Grid container>
            <Grid item xs={12} md={12}>
              <Box
                padding="20px 30px"
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <img src={StudentWithMonitor} alt="Enroll Tutors" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box component="h3" fontWeight="600" margin="0">
                      {disableAll ? 'View Tutor' : 'Edit Tutor'}
                    </Box>
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>

          <Divider />

          <Grid container justify="center">
            <Grid item xs={12} md={8}>
              <Box padding="20px">
                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Tutor Name
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Enter Full Name"
                        inputProps={{ maxLength: 50 }}
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        readOnly={disableAll}
                        className={errors.tutorName ? classes.error : ''}
                      />
                    </FormControl>
                    {errors.tutorName && (
                      <FormHelperText error>
                        {errors.tutorName.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Enrollment ID
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Enter enrollment id"
                        inputProps={{ maxLength: 50 }}
                        value={enrollmentId}
                        onChange={(e) => setEnrollmentId(e.target.value)}
                        readOnly={disableAll}
                      />
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Tutor Mobile Number
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                        placeholder="Enter Tutor Mobile Number"
                        value={phone}
                        disabled
                        onChange={(e) => setPhone(e.target.value)}
                        readOnly={disableAll}
                        className={errors.mobileNo ? classes.error : ''}
                      />
                    </FormControl>
                    {errors.mobileNo && (
                      <FormHelperText error>
                        {errors.mobileNo.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Email Address
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Your Email Address"
                        value={email}
                        disabled
                        onChange={(e) => setEmail(e.target.value)}
                        readOnly={disableAll}
                        className={errors.emailId ? classes.error : ''}
                      />
                    </FormControl>
                    <FormHelperText>
                        Email ID cannot be changed.
                      </FormHelperText>
                    {errors.emailId && (
                      <React.Fragment>
                      <FormHelperText error>
                        {errors.emailId.message}
                      </FormHelperText>
                      </React.Fragment>
                      
                    )}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Qualifications
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Select
                        value={qualification}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                        setQualification(e.target.value as string)
                          // setBoardAndFetchClasses(e.target.value as string)
                        }
                        displayEmpty
                        readOnly={disableAll}
                        className={errors.qualifications ? classes.error : ''}
                      >
                        <MenuItem value="">Select Qualification</MenuItem>
                        {qualificationsList.length > 0 &&
                          qualificationsList.map((item) => (
                            <MenuItem value={item.value} key={item.value}>
                              {item.title}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                    {/* {qualification?.value === 'Other' && <TypeOther />} */}
                    {errors.qualifications && (
                      <FormHelperText error>
                        {errors.qualifications.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        PIN Code
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="PIN Code"
                        value={pinCode}
                        inputProps={{ maxLength: 6 }}
                        onChange={(e) => onPinCodeChange(e.target.value)}
                        readOnly={disableAll}
                        className={errors.pinCode ? classes.error : ''}
                      />
                    </FormControl>
                    {errors.pinCode && (
                      <FormHelperText error>
                        {errors.pinCode.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        City
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Box>{cityName}</Box>
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        State
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Box>{stateName}</Box>
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box fontWeight="bold" marginTop="5px">
                        Schools / Others
                      </Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Select
                        value={school}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                        setSchool(e.target.value as string)
                          // setBoardAndFetchClasses(e.target.value as string)
                        }
                        displayEmpty
                        readOnly={disableAll}
                        className={errors.schoolName ? classes.error : ''}
                      >
                        <MenuItem value="">Select School</MenuItem>
                        {schoolsList.length > 0 &&
                          schoolsList.map((item) => (
                            <MenuItem value={item.value} key={item.value}>
                              {item.title}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                    {/* {school?.value === 'Other' && <TypeOther />} */}
                    {errors.schoolName && (
                      <FormHelperText error>
                        {errors.schoolName.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>
              </Box>
              {!disableAll && (
                <Box padding="20px">
                  <FormControl fullWidth margin="normal">
                    <Box display="flex" justifyContent="flex-end">
                      <Box>
                        <Button
                          disableElevation
                          color="primary"
                          size="large"
                          variant="contained"
                          onClick={saveTutors}
                        >
                          Save
                        </Button>
                        {errors.serverError && (
                          <FormHelperText error>
                            {errors.serverError.message}
                          </FormHelperText>
                        )}
                      </Box>
                    </Box>
                  </FormControl>
                </Box>
              )}
            </Grid>
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser,
  currentTutor: state.studentTutorReducer.tutor
});

export default connect(mapStateToProps)(TutorUpdate);
