import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import {
  Box,
  Container,
  Divider,
  FormControl,
  Grid,
  FormHelperText,
  Input,
  MenuItem,
  Select,
  Typography,
  TextField
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { RootState } from '../../../store';
import { Tutor, Student } from '../../common/contracts/user';
import StudentWithMonitor from '../../../assets/images/student-with-monitor.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker } from '../../common/helpers';
import { Redirect } from 'react-router-dom';
import { Standard } from '../../academics/contracts/standard';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode
} from '../../common/api/academics';
import { Board } from '../../academics/contracts/board';
import { CurrentStudent } from '../contracts/student';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import {
  EMAIL_PATTERN,
  NAME_PATTERN,
  PHONE_PATTERN,
  PIN_PATTERN
} from '../../common/validations/patterns';
import { updateStudentsOfOrganization } from '../../common/api/organization'
import { isTutor, eventTracker, GAEVENTCAT } from '../../common/helpers'
import { useSnackbar } from 'notistack';
import { getParentForTutor, updateStudentsOfTutor } from '../../common/api/tutor';
import { fontOptions } from '../../../theme';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      marginTop: '5px'
    },
    addBtn: {
      '& button': {
        padding: '12px 24px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface Props
  extends RouteComponentProps<{ mode: string; username: string }> {
  authUser: Tutor;
  currentStudent: CurrentStudent;
}

type RoleStatusType ='FRESHER'|'ACTIVE'|'DEACTIVATE'

interface FormData {
  pageError: string;
  serverError: string;
  studentName: string;
  boardName: string;
  className: string;
  mobileNo: string;
  parentMobileNo: string;
  emailId: string;
  schoolName: string;
  cityName: string;
  stateName: string;
  pinCode: string;
}

interface SelectOption {
  title: string;
  value: string;
}

const ValidationSchema = yup.object().shape({
  studentName: yup
    .string()
    .required('Student number is a required field')
    .min(5)
});

const StudentUpdate: FunctionComponent<Props> = ({
  match,
  authUser,
  currentStudent
}) => {
  const disableAll = match.params.mode === 'view' ? true : false;
  const { errors, setError, clearError } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [name, setName] = useState('');
  const [enrollmentId, setEnrollmentId] = useState('');
  const [board, setBoard] = useState('');
  const [standard, setStandard] = useState(''); // Standard describes Class of the Tutor.
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [parentMobileNo, setParentMobileNo] = useState('');
  const [parentRoleStatus,setParentRoleStatus]  = useState<RoleStatusType>('FRESHER')
  const [pinCode, setPinCode] = useState('');
  const [roleStatus,setRoleStatus] = useState<RoleStatusType>('ACTIVE')
  const [cityName, setCityName] = useState('');
  const [stateName, setStateName] = useState('');
  const [school, setSchool] = useState('');
  const [boards, setBoards] = useState<Board[]>([]);
  const [classes, setClasses] = useState<Standard[]>([]);
  const [schoolsList, setSchoolsList] = useState<SelectOption[]>([]);

  const styles = useStyles();
  eventTracker(GAEVENTCAT.studentProfileCreate, 'Edit Students Page', 'Landed Edit Students Page');

  useEffect(() => {
    (async () => {
      try {
        const [boardsList] = await Promise.all([fetchBoardsList()]);

        setBoards(boardsList);
        
        const getCityArr = await fetchCitiesByPinCode({
          pinCode: currentStudent.pincode
        });

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);

        setName(currentStudent.name);
        setEnrollmentId(currentStudent.enrollmentId);
        setPhone(currentStudent.mobileNo);
        setEmail(currentStudent.email);
        setPinCode(currentStudent.pincode);
        setRoleStatus(currentStudent.roleStatus)
        setCityName(currentStudent.city);
        setStateName(currentStudent.state);
        setBoard(currentStudent.board);
        setSchool(currentStudent.schoolname)
        // setStandard(currentStudent.classname);
        setParentMobileNo(currentStudent.parentMobileNo);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
    // eslint-disable-next-line
  }, []);

  useEffect(()=>{
    if(parentMobileNo){
      if(parentMobileNo!='' && parentMobileNo.length==10){
        callGetParentForTutor(parentMobileNo)
      }
    }
    
  },[parentMobileNo])

  useEffect(()=>{
    setBoardAndFetchClasses()
  },[board])

  const callGetParentForTutor = async (mobileNumber:string) =>{
    try{
      const response = await getParentForTutor(mobileNumber)
      setParentRoleStatus(response.parent.roleStatus)
    }
    catch(error){
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  }


  if (redirectTo && redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const setBoardAndFetchClasses = async () => {
    try {
      // setBoard(board);
      if (board.length > 0) {
        const classListResponse = await fetchClassesList({ boardname: board });
        setClasses(classListResponse);
        if(board==currentStudent.board){
          setStandard(currentStudent.classname)
        }
      } else {
        setClasses([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchool('');
        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
          setCityName('');
          setStateName('');
          setSchoolsList([]);
        }
      }
    } else {
      setCityName('');
      setStateName('');
      setSchoolsList([]);
    }
  };

  const saveStudents = async () => {
    if (!NAME_PATTERN.test(name)) {
      setError('studentName', 'Invalid Data', 'Invalid student name');
      return;
    } else {
      clearError('studentName');
    }
    if (!name.length) {
      setError(
        'studentName',
        'Invalid Data',
        'Student name cannot not be empty'
      );
      return;
    } else {
      clearError('studentName');
    }
    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'studentName',
        'Invalid Data',
        'Student Name cannot be less than 5 character excluding spaces'
      );
      return;
    } else {
      clearError('studentName')
    }
    if(phone==undefined){
      setError('mobileNo', 'Invalid Data', 'Mobile number cannot be empty');
      return;
    }
    else{
      if (!phone.length) {
        setError('mobileNo', 'Invalid Data', 'Mobile number cannot be empty');
        return;
      } else {
        clearError('mobileNo');
      }
      if (!phone.match(PHONE_PATTERN)) {
        setError('mobileNo', 'Invalid Data', 'Invalid Mobile number');
        return;
      } else {
        clearError('mobileNo');
      }
    }
    
    if (email===undefined) {
      setError('emailId', 'Invalid Data', 'Email cannot be empty');
      return;
    } else {
      clearError('emailId');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email cannot be empty');
      return;
    } else {
      clearError('emailId');
    }

    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }
    if(parentMobileNo!=undefined){
      if (parentMobileNo.length > 0 && !parentMobileNo.match(PHONE_PATTERN)) {
        setError(
          'parentMobileNo',
          'Invalid Data',
          'Invalid parent mobile number'
        );
        return;
      } else {
        clearError('parentMobileNo');
      }
    }
    else {
      clearError('parentMobileNo');
    }
    

    if (parentMobileNo === phone) {
      setError(
        'parentMobileNo',
        'Invalid Data',
        'Parent Mobile number cannot be same as student mobile number'
      );
      return;
    } else {
      clearError('parentMobileNo');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'Pin Code cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    if (!PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid pin code');
      return;
    } else {
      clearError('pinCode');
    }

    if (!cityName.length) {
      setError('cityName', 'Invalid Data', 'City cannot be empty');
      return;
    } else {
      clearError('cityName');
    }

    if (!stateName.length) {
      setError('stateName', 'Invalid Data', 'State cannot be empty');
      return;
    } else {
      clearError('stateName');
    }

    if (!school.length) {
      setError('schoolName', 'Invalid Data', 'School cannot be empty');
      return;
    } else {
      clearError('schoolName');
    }

    if(board==undefined){
      setError('boardName', 'Invalid Data', 'Board cannot be empty');
      return;
    }
    else {
      if (!board.length) {
        setError('boardName', 'Invalid Data', 'Board cannot be empty');
        return;
      } else {
        clearError('boardName');
      }
    }
  

    
    if(standard==undefined){
      setError('boardName', 'Invalid Data', 'Board cannot be empty');
      return;
    }
    else{
      if (!standard.length) {
        setError('className', 'Invalid Data', 'Class cannot be empty');
        return;
      } else {
        clearError('className');
      }
    }
    

    const student: Student = {
      boardName: board,
      className: standard,
      mobileNo: phone,
      emailId: email,
      parentMobileNo: parentMobileNo,
      schoolName: school ? school : '',
      studentName: name,
      enrollmentId: enrollmentId !== '' ? enrollmentId : undefined,
      pinCode: pinCode ? pinCode : '',
      cityName: cityName ? cityName : '',
      stateName: stateName ? stateName : '',
      roleStatus: roleStatus ? roleStatus : 'ACTIVE'
    };

    clearError('serverError');
    try {
      if (isTutor(authUser)) {
        await updateStudentsOfTutor(student);
        eventTracker(GAEVENTCAT.studentProfileCreate, 'Edit Students Page', 'Students Updated by Tutor')
      } else {
        await updateStudentsOfOrganization(student);
        setName('');
        setBoard('');
        setStandard('');
        setPhone('');
        setParentMobileNo('');
        setEmail('');
        eventTracker(GAEVENTCAT.studentProfileCreate, 'Edit Students Page', 'Students Updated by Organization')
      }
      setRedirectTo(`/profile/students`);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="md">
        <Box
          bgcolor="white"
          marginY="20px"
          boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
          borderRadius="4px"
        >
          <Grid container>
            <Grid item xs={12} md={12}>
              <Box
                padding="20px 30px"
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <img src={StudentWithMonitor} alt="Enroll Students" />

                <Box marginLeft="15px">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={styles.heading}>
                      {disableAll ? 'View Students' : 'Edit Students'}
                    </Box>
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>

          <Divider />

          <Grid container justify="center">
            <Grid item xs={12} md={8}>
              <Box padding="20px">
                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Student Name</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Enter Full Name"
                        inputProps={{ maxLength: 50 }}
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        readOnly={disableAll}
                        className={errors.studentName ? styles.error : ''}
                      />
                    </FormControl>
                    {errors.studentName && (
                      <FormHelperText error>
                        {errors.studentName.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Enrollment ID</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Enter enrollment id"
                        inputProps={{ maxLength: 50 }}
                        value={enrollmentId}
                        onChange={(e) => setEnrollmentId(e.target.value)}
                        readOnly={disableAll}
                      />
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Student Mobile Number</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                        placeholder="Enter Student Mobile Number"
                        value={phone}
                        disabled = {roleStatus!="FRESHER"}
                        onChange={(e) => setPhone(e.target.value)}
                        readOnly={disableAll}
                        className={errors.mobileNo ? styles.error : ''}
                      />
                    </FormControl>
                    {errors.mobileNo ? (
                      <FormHelperText error>
                        {errors.mobileNo.message}
                      </FormHelperText>):
                      roleStatus!='FRESHER'&&<FormHelperText >
                      Mobile No cannot be changed as user have already signed up.
                    </FormHelperText>
                    }
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Parent Mobile Number</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                        placeholder="Enter Parent Mobile Number"
                        value={parentMobileNo}
                        onChange={(e) => setParentMobileNo(e.target.value)}
                        readOnly={disableAll}
                        disabled = {parentRoleStatus!='FRESHER'}
                        className={errors.parentMobileNo ? styles.error : ''}

                      />
                    </FormControl>
                    {errors.parentMobileNo ? (
                      <FormHelperText error>
                        {errors.parentMobileNo.message}
                      </FormHelperText>):
                      parentRoleStatus!='FRESHER'&&<FormHelperText >
                      Mobile No cannot be changed as user have already signed up.
                    </FormHelperText>
                    }
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Email Address</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="Your Email Address"
                        value={email}
                        disabled = {roleStatus!="FRESHER"}
                        onChange={(e) => setEmail(e.target.value)}
                        readOnly={disableAll}
                        className={errors.emailId ? styles.error : ''}
                      />
                    </FormControl>
                    
                    {errors.emailId ? (
                      <FormHelperText error>
                        {errors.emailId.message}
                      </FormHelperText>):
                      roleStatus!='FRESHER'&&<FormHelperText >
                      Email ID cannot be changed as user have already signed up.
                    </FormHelperText>
                    }
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>PIN Code</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Input
                        placeholder="PIN Code"
                        value={pinCode}
                        inputProps={{ maxLength: 6 }}
                        onChange={(e) => onPinCodeChange(e.target.value)}
                        readOnly={disableAll}
                        className={errors.pinCode ? styles.error : ''}
                      />
                    </FormControl>
                    {errors.pinCode && (
                      <FormHelperText error>
                        {errors.pinCode.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>City</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Box>{cityName}</Box>
                    </FormControl>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>State</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Box>{stateName}</Box>
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Schools / Others</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Select
                        value={school}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                        setSchool(e.target.value as string)
                          // setBoardAndFetchClasses(e.target.value as string)
                        }
                        displayEmpty
                        readOnly={disableAll}
                        className={errors.schoolName ? styles.error : ''}
                      >
                        <MenuItem value="">Select School</MenuItem>
                        {schoolsList.length > 0 &&
                          schoolsList.map((item) => (
                            <MenuItem value={item.value} key={item.value}>
                              {item.title}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                    {/* {school?.value === 'Other' && <TypeOther />} */}
                    {errors.schoolName && (
                      <FormHelperText error>
                        {errors.schoolName.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Board</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Select
                        value={board}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                        setBoard(e.target.value as string)
                          // setBoardAndFetchClasses(e.target.value as string)
                        }
                        displayEmpty
                        readOnly={disableAll}
                        className={errors.boardName ? styles.error : ''}
                      >
                        <MenuItem value="">Select a board</MenuItem>
                        {boards.length > 0 &&
                          boards.map((item) => (
                            <MenuItem value={item.boardName} key={item.boardID}>
                              {item.boardName} ({item.boardDescriptions})
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                    {errors.boardName && (
                      <FormHelperText error>
                        {errors.boardName.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>

                <Grid container>
                  <Grid item xs={12} md={4}>
                    <FormControl fullWidth margin="normal">
                      <Box className={styles.label}>Class</Box>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} md={8}>
                    <FormControl fullWidth margin="normal">
                      <Select
                        value={standard}
                        onChange={(e: React.ChangeEvent<{ value: unknown }>) =>
                          setStandard(e.target.value as string)
                        }
                        displayEmpty
                        readOnly={disableAll}
                        className={errors.className ? styles.error : ''}
                      >
                        <MenuItem value="">Select a class</MenuItem>
                        {classes.length > 0 &&
                          classes.map((standard) => (
                            <MenuItem
                              value={standard.className}
                              key={standard.classID}
                            >
                              {standard.className}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                    {errors.className && (
                      <FormHelperText error>
                        {errors.className.message}
                      </FormHelperText>
                    )}
                  </Grid>
                </Grid>
              </Box>
              {!disableAll && (
                <Box display="flex" justifyContent="flex-end" marginY="30px">
                  <Box className={styles.addBtn}>
                    <Button
                      disableElevation
                      color="primary"
                      size="large"
                      variant="contained"
                      onClick={saveStudents}
                    >
                      Save
                    </Button>
                    {errors.serverError && (
                      <FormHelperText error>
                        {errors.serverError.message}
                      </FormHelperText>
                    )}
                  </Box>
                </Box>
              )}
            </Grid>
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as Tutor,
  currentStudent: state.studentTutorReducer.student
});

export default connect(mapStateToProps)(StudentUpdate);
