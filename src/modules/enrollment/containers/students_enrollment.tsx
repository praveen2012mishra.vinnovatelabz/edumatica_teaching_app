import React, { FunctionComponent, useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Autocomplete } from '@material-ui/lab';
import {
  Box,
  Container,
  FormControl,
  Grid,
  IconButton,
  FormHelperText,
  Input,
  MenuItem,
  Select,
  Typography,
  TextField,
  Button as MuButton
} from '@material-ui/core';
import {
  Add as AddIcon,
  CheckCircle as CheckCircleIcon,
  RemoveCircleOutline as RemoveCircleIcon
} from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AutocompleteOption } from '../../common/contracts/autocomplete_option';
import { RootState } from '../../../store';
import { addStudentsOfTutor } from '../../common/api/tutor';
import { Tutor, Student } from '../../common/contracts/user';
import StudentWithMonitor from '../../../assets/images/student-with-monitor.png';
import DownloadIcon from '../../../assets/images/download-icon.png';
import UploadIcon from '../../../assets/images/upload-icon.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker } from '../../common/helpers';
import { Redirect } from 'react-router-dom';
import { Standard } from '../../academics/contracts/standard';
import {
  fetchBoardsList,
  fetchClassesList,
  fetchCitySchoolsList,
  fetchCitiesByPinCode
} from '../../common/api/academics';
import { Board } from '../../academics/contracts/board';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import {
  EMAIL_PATTERN,
  PHONE_PATTERN,
  PIN_PATTERN,
  NAME_PATTERN
} from '../../common/validations/patterns';
import { addStudentsOfOrganization } from '../../common/api/organization';
import { isTutor, isOrganization, eventTracker, GAEVENTCAT } from '../../common/helpers';
import * as XLSX from 'xlsx';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';
import { fontOptions } from '../../../theme';
import {setAuthUser} from "../../auth/store/actions"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paddClass: {
      padding: '0px 30px'
    },
    heading: {
      margin: '0',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    },
    subHeading: {
      margin: '5px 0 15px 0',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: '23px',
      color: '#000000'
    },
    helperText: {
      fontSize: fontOptions.size.small,
      letterSpacing: '0.15px',
      color: 'rgba(0, 0, 0, 0.6)'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      marginTop: '5px'
    },
    studentsList: {
      margin: '0',
      fontWeight: fontOptions.weight.normal,
      color: '#151522'
    },
    removeIcon: {
      color: 'rgba(0, 0, 0, 0.5)'
    },
    addBtn: {
      '& button': {
        padding: '10px 16px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.small,
        lineHeight: '18px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        borderRadius: '5px',

        '& svg': {
          fontSize: fontOptions.size.medium
        }
      }
    },
    clearBtn: {
      '& button': {
        border: '1px solid #666666',
        color: '#666666'
      }
    },
    error: {
      "&:not(.Mui-disabled)::before": {
        borderColor: "#F44E42"
      }
    }
  })
);

interface TutorStudentRowProps {
  item: Student;
  handleRemoveItem: () => any;
}

const TutorStudentRow: FunctionComponent<TutorStudentRowProps> = ({
  item,
  handleRemoveItem
}) => {
  const styles = useStyles();
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="space-between"
      padding="20px 30px"
      borderBottom="1px solid rgba(224, 224, 224, 0.5)"
    >
      <Box marginRight="15px">
        <Typography component="span">
          <Box component="h4" className={styles.studentsList}>
            {item.studentName} ({item.mobileNo})
          </Box>
        </Typography>
      </Box>

      <IconButton size="small" onClick={handleRemoveItem}>
        <RemoveCircleIcon className={styles.removeIcon} />
      </IconButton>
    </Box>
  );
};

interface Props {
  authUser: Tutor;
}

interface FormData {
  pageError: string;
  serverError: string;
  studentName: string;
  boardName: string;
  className: string;
  mobileNo: string;
  parentMobileNo: string;
  emailId: string;
  schoolName: string;
  cityName: string;
  stateName: string;
  pinCode: string;
}

const ValidationSchema = yup.object().shape({
  studentName: yup
    .string()
    .required('Student number is a required field')
    .min(5)
});

const StudentsEnrollment: FunctionComponent<Props> = ({ authUser }) => {
  const { errors, setError, clearError } = useForm<FormData>({
    mode: 'onBlur',
    validationSchema: ValidationSchema
  });
  const { enqueueSnackbar } = useSnackbar();
  const [redirectTo, setRedirectTo] = useState('');
  const [name, setName] = useState('');
  const [enrollmentId, setEnrollmentId] = useState('');
  const [board, setBoard] = useState('');
  const [standard, setStandard] = useState(''); // Standard describes Class of the Tutor.
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [parentMobileNo, setParentMobileNo] = useState('');
  const [pinCode, setPinCode] = useState('');
  const [cityName, setCityName] = useState('');
  const [stateName, setStateName] = useState('');
  const [school, setSchool] = useState<AutocompleteOption | null>();
  const [students, setStudents] = useState<Student[]>([]);
  const [roleStatus,setRoleStatus] = useState<'FRESHER'|'ACTIVE'|'DEACTIVATE'>('FRESHER')
  const [boards, setBoards] = useState<Board[]>([]);
  const [classes, setClasses] = useState<Standard[]>([]);
  const [schoolsList, setSchoolsList] = useState<AutocompleteOption[]>([]);
  const dispatch= useDispatch()

  const styles = useStyles();
  eventTracker(GAEVENTCAT.studentProfileCreate, 'Add Students Page', 'Landed Add Students Page');

  useEffect(() => {
    (async () => {
      console.log(authUser)
      try {
        const [boardsList] = await Promise.all([fetchBoardsList()]);

        setBoards(boardsList);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        }
      }
    })();
  }, []);

  if (redirectTo && redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  // const TypeOther = () => (
  //   <FormControl fullWidth margin="normal">
  //     <Input placeholder="Others" />
  //   </FormControl>
  // );

  const setBoardAndFetchClasses = async (board: string) => {
    try {
      setBoard(board);
      if (board.length > 0) {
        const classListResponse = await fetchClassesList({ boardname: board });
        setClasses(classListResponse);
      } else {
        setClasses([]);
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const getCityStateName = async (pin: string) => {
    try {
      const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
      return getCityArr;
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      } else {
        enqueueSnackbar('Service not available in this area', {
          variant: 'error'
        });
      }
    }
  };

  //PIN Code based city and state selection
  const onPinCodeChange = async (pin: string) => {
    setPinCode(pin);
    if (PIN_PATTERN.test(pin)) {
      try {
        const getCityArr = await fetchCitiesByPinCode({ pinCode: pin });
        setCityName(getCityArr[0].cityName);
        setStateName(getCityArr[0].stateName);

        const schoolsListResponse = await fetchCitySchoolsList({
          cityName: getCityArr[0].cityName
        });

        const structuredSchoolsList = schoolsListResponse.map((school) => ({
          title: `${school.schoolName} (${school.schoolAddress})`,
          value: school.schoolName
        }));

        setSchool(null);
        setSchoolsList([
          ...structuredSchoolsList,
          { title: 'Other', value: 'Other' }
        ]);
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          setRedirectTo('/login');
        } else {
          enqueueSnackbar('Service not available in this area', {
            variant: 'error'
          });
          setPinCode('');
          setCityName('');
          setStateName('');
          setSchoolsList([]);
        }
      }
    } else {
      setCityName('');
      setStateName('');
      setSchoolsList([]);
    }
  };

  const addStudent = async (e: React.FormEvent) => {
    e.preventDefault();
    clearError('serverError');

    if (!name.length) {
      setError(
        'studentName',
        'Invalid Data',
        'Student name cannot not be empty'
      );
      return;
    } else {
      clearError('studentName');
    }

    const remSpace = name.replace(/ /g, "")
    if (remSpace.length < 5) {
      setError(
        'studentName',
        'Invalid Data',
        'Student name cannot be less than 5 character excluding spaces'
      );
      return;
    } else {
      clearError('studentName');
    }

    if (!NAME_PATTERN.test(name)) {
      setError('studentName', 'Invalid Data', 'Invalid student name');
      return;
    } else {
      clearError('studentName');
    }

    if (!phone.length) {
      setError('mobileNo', 'Invalid Data', 'Mobile number cannot not be empty');
      return;
    } else {
      clearError('mobileNo');
    }

    if (!phone.match(PHONE_PATTERN)) {
      setError('mobileNo', 'Invalid Data', 'Invalid mobile number');
      return;
    } else {
      clearError('mobileNo');
    }

    if (!email.length) {
      setError('emailId', 'Invalid Data', 'Email cannot not be empty');
      return;
    } else {
      clearError('emailId');
    }
    if (!EMAIL_PATTERN.test(email.toLowerCase())) {
      setError('emailId', 'Invalid Data', 'Invalid Email');
      return;
    } else {
      clearError('emailId');
    }

    if (parentMobileNo && !parentMobileNo.match(PHONE_PATTERN)) {
      setError(
        'parentMobileNo',
        'Invalid Data',
        'Invalid parent mobile number'
      );
      return;
    } else {
      clearError('parentMobileNo');
    }

    if (parentMobileNo === phone) {
      setError(
        'parentMobileNo',
        'Invalid Data',
        'Parent Mobile number cannot be same as student mobile number'
      );
      return;
    } else {
      clearError('parentMobileNo');
    }

    if (!pinCode.length) {
      setError('pinCode', 'Invalid Data', 'Pin Code cannot be empty');
      return;
    } else {
      clearError('pinCode');
    }
    if (!PIN_PATTERN.test(pinCode)) {
      setError('pinCode', 'Invalid Data', 'Invalid pin code');
      return;
    } else {
      clearError('pinCode');
    }

    if (!cityName.length) {
      setError('cityName', 'Invalid Data', 'City cannot be empty');
      return;
    } else {
      clearError('cityName');
    }

    if (!stateName.length) {
      setError('stateName', 'Invalid Data', 'State cannot be empty');
      return;
    } else {
      clearError('stateName');
    }

    if (school == null) {
      setError('schoolName', 'Invalid Data', 'School cannot be empty');
      return;
    } else {
      clearError('schoolName');
    }

    if (!board.length) {
      setError('boardName', 'Invalid Data', 'Board cannot be empty');
      return;
    } else {
      clearError('boardName');
    }

    if (!standard.length) {
      setError('className', 'Invalid Data', 'Class cannot be empty');
      return;
    } else {
      clearError('className');
    }

    const filteredStudents = students.filter(function (el) {
      return el.mobileNo === phone;
    });

    if (filteredStudents.length > 0) {
      setError('pageError', 'Invalid Data', 'Mobile Number already used');
      return;
    } else {
      clearError('pageError');
    }

    const student: Student = {
      boardName: board,
      className: standard,
      mobileNo: phone,
      emailId: email,
      parentMobileNo: parentMobileNo,
      schoolName: school?.value ? school.value : '',
      studentName: name,
      enrollmentId: enrollmentId !== '' ? enrollmentId : undefined,
      pinCode: pinCode ? pinCode : '',
      cityName: cityName ? cityName : '',
      stateName: stateName ? stateName : '',
      roleStatus : roleStatus
    };

    setStudents([...students, student]);

    setName('');
    setEnrollmentId('');
    setPhone('');
    setParentMobileNo('');
    setEmail('');
    setPinCode('');
    setCityName('');
    setStateName('');
    setSchool(null);
    setBoard('');
    setStandard('');
  };

  const removeStudent = async (index: number) => {
    const studentsDraft = students.filter(
      (student, sIndex) => sIndex !== index
    );

    setStudents(studentsDraft);
  };

  const saveStudents = async () => {
    clearError('serverError');
    try {
      if (isTutor(authUser)) {
        await addStudentsOfTutor(students).then((val) => {
          const newAuthUser = Object.assign({},authUser,{studentList: [...authUser.studentList as Array<string>,...val.addedStudentList]})
          dispatch(setAuthUser(newAuthUser))
        })
        eventTracker(GAEVENTCAT.studentProfileCreate, 'Add Students Page', 'Student Added by Tutor');
      }
      if (isOrganization(authUser)) {
        await addStudentsOfOrganization(students).then((val) => {
          const newAuthUser = Object.assign({},authUser,{studentList: [...authUser.studentList as Array<string>,...val.addedStudentList]})
          dispatch(setAuthUser(newAuthUser))
        })
        eventTracker(GAEVENTCAT.studentProfileCreate, 'Add Students Page', 'Student Added by Organization');
      }
      setRedirectTo(`/profile/students`);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const isUniqueMobile = (mobile: string[]) => {
    let mobileNo,
      unique = true;
    mobile.sort().sort((a, b) => {
      if (a === b) {
        mobileNo = a;
        unique = false;
      }
      return 0;
    });
    return { mobileNo: mobileNo, isUnique: unique };
  };

  const validateStudents = (structuredStudents: Student[]) => {
    let uploadError = '';

    structuredStudents.map(student => {
      if(student.parentMobileNo.length < 10) {
              enqueueSnackbar("Must Include Parents mobile number", {
                variant: 'error', preventDuplicate: false, persist: false,
              });
              return
            }
    })
    const studentMobile = isUniqueMobile(
      structuredStudents.map((student) => student.mobileNo)
    );
    const parentMobile = isUniqueMobile(
      structuredStudents.map((student) => student.parentMobileNo)
    );
    const uniqueMobile = isUniqueMobile(
      structuredStudents
        .map((student) => {
          return [student.mobileNo, student.parentMobileNo];
        })
        .flat()
    );
    if (!studentMobile.isUnique) {
      uploadError = `Student mobile number "${studentMobile.mobileNo}" must be unique`;
    } else if (!parentMobile.isUnique) {
      uploadError = `Parent mobile number "${parentMobile.mobileNo}" must be unique`;
    } else if (!uniqueMobile.isUnique) {
      uploadError = `Parent mobile number "${uniqueMobile.mobileNo}" cannot be same as student mobile number`;
    }

    structuredStudents &&
      structuredStudents.every((student) => {
        if (!uploadError) {
          if (!student.studentName) {
            uploadError = `Student name should not be empty`;
            return false;
          }
          if(student.parentMobileNo.length < 10) {
            uploadError = `Parent Mobile number should not be empty`
          }
          if (student.studentName == null || student.studentName.replace(/ /g, "").length < 5) {
            uploadError = `Student name "${student.studentName}" cannot be less than 5 character excluding spaces`;
            return false;
          }
          if (!NAME_PATTERN.test(student.studentName)) {
            uploadError = `Invalid student name "${student.studentName}"`;
            return false;
          }
          if (!student.mobileNo) {
            uploadError = `Student mobile number should not be empty`;
            return false;
          }
          if (
            student.mobileNo == null ||
            !student.mobileNo.toString().match(PHONE_PATTERN)
          ) {
            uploadError = `Student Mobile number "${student.mobileNo}" must be 10 digit number`;
            return false;
          }
          if (
            student.parentMobileNo &&
            !student.parentMobileNo.toString().match(PHONE_PATTERN)
          ) {
            uploadError = `Parent mobile number "${student.parentMobileNo}" must be 10 digit number`;
            return false;
          }
          if (student.parentMobileNo === student.mobileNo) {
            uploadError = `Parent mobile number "${student.parentMobileNo}" cannot be same as student mobile number`;
            return false;
          }
          if (!student.emailId) {
            uploadError = 'Email cannot be empty';
            return false;
          }

          if (!EMAIL_PATTERN.test(student.emailId.toLowerCase())) {
            uploadError = 'Invalid Email';
            return false;
          }

          if (!student.pinCode) {
            uploadError = 'Pin Code cannot be empty';
            return false;
          }
          if (!PIN_PATTERN.test(student.pinCode)) {
            uploadError = 'Invalid pin code';
            return false;
          }

          if (!student.cityName) {
            uploadError = 'City cannot be empty';
            return false;
          }

          if (!student.stateName) {
            uploadError = 'State cannot be empty';
            return false;
          }

          if (!student.schoolName) {
            uploadError = 'School cannot be empty';
            return false;
          }

          if (!student.boardName) {
            uploadError = 'Board cannot be empty';
            return false;
          }

          if (!student.className) {
            uploadError = 'Class cannot be empty';
            return false;
          }

          const filteredStudents = students.filter(function (el) {
            return el.mobileNo === student.mobileNo;
          });

          if (filteredStudents.length > 0) {
            uploadError = `Mobile number "${student.mobileNo}" already used`;
            return false;
          }
        }
        return true;
      });

    if (!uploadError) {
      setStudents([...students, ...structuredStudents]);
    } else {
      enqueueSnackbar(uploadError, {
        variant: 'error'
      });
    }
  };

  const readExcel = (file: File | null) => {
    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      if (file) {
        fileReader.readAsArrayBuffer(file);
        fileReader.onload = (e) => {
          const bufferArray = e.target ? e.target.result : '';
          const wb = XLSX.read(bufferArray, { type: 'buffer' });
          const wsname = wb.SheetNames[0];
          const ws = wb.Sheets[wsname];

          /* Convert array to json*/
          const jsonData = XLSX.utils.sheet_to_json(ws);
          resolve(jsonData);
        };
        fileReader.onerror = (error) => {
          reject(error);
        };
      }
    });
    promise.then(async (studentsArr: any) => {
      const structuredStudents: Student[] = await Promise.all(
        studentsArr &&
          studentsArr.map(async (student: any) => {
            let citiesArr = await getCityStateName(student.Pin_Code);

            return {
              boardName: student.Board,
              className: student.Class,
              mobileNo: student.Student_Mobile,
              parentMobileNo: student.Parent_Mobile,
              emailId: student.Email_Id,
              schoolName: student.Schools,
              studentName: student.Student_Name,
              pinCode: student.Pin_Code,
              cityName:
                citiesArr && citiesArr[0].cityName ? citiesArr[0].cityName : '',
              stateName:
                citiesArr && citiesArr[0].stateName
                  ? citiesArr[0].stateName
                  : '',
              enrollmentId: student.Enrollment_Id
            };
          })
      );
      validateStudents(structuredStudents);
    });
  };

  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg">
        <Box
          bgcolor="white"
          marginY="20px"
          boxShadow="0px 10px 20px rgba(31, 32, 65, 0.05)"
          borderRadius="4px"
        >
          <Box borderBottom="1px solid rgba(224, 224, 224, 0.5)">
            <Grid container>
              <Grid item xs={12} md={6}>
                <Box padding="20px 30px" display="flex" alignItems="center">
                  <img src={StudentWithMonitor} alt="Enroll Students" />

                  <Box marginLeft="15px">
                    <Typography component="span" color="secondary">
                      <Box component="h3" className={styles.heading}>
                        Enroll Students
                      </Box>
                    </Typography>
                  </Box>
                </Box>
              </Grid>

              <Grid item xs={12} md={6}>
                <Box
                  display="flex"
                  alignItems="center"
                  justifyContent="flex-end"
                  padding="20px 30px"
                >
                  <Box marginRight="10px">
                    <div>
                      <input
                        accept=".xls, .xlsx"
                        style={{ display: 'none' }}
                        id="contained-button-file"
                        type="file"
                        onChange={(e) => {
                          readExcel(e.target.files && e.target.files[0]);
                          e.target.value = '';
                        }}
                      />
                      <label htmlFor="contained-button-file">
                        <MuButton
                          component="span"
                          className={styles.helperText}
                        >
                          Bulk Upload
                          <Box
                            display="flex"
                            alignItems="center"
                            marginLeft="15px"
                          >
                            <img src={UploadIcon} alt="Upload Students" />
                          </Box>
                        </MuButton>
                      </label>
                    </div>
                  </Box>

                  <Box>
                    <MuButton>
                      <Link
                        className={styles.helperText}
                        style={{ textDecoration: 'none' }}
                        to="/files/students.xlsx"
                        target="_blank"
                        download
                      >
                        Download Template
                      </Link>
                      <Box display="flex" alignItems="center" marginLeft="15px">
                        <img src={DownloadIcon} alt="Download Students" />
                      </Box>
                    </MuButton>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Grid container>
            <Grid item xs={12} md={8}>
              <Box padding="20px 0">
                <form onSubmit={addStudent}>
                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}></Grid>

                    <Grid item xs={12} md={8}>
                      <Box component="h5" className={styles.subHeading}>
                        Enroll Students
                      </Box>
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Student Name</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Enter Full Name"
                          inputProps={{ maxLength: 50 }}
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                          className={errors.studentName ? styles.error : ''}
                        />
                      </FormControl>
                      {errors.studentName && (
                        <FormHelperText error>
                          {errors.studentName.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Enrollment ID</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Enter enrollment id"
                          inputProps={{ maxLength: 50 }}
                          value={enrollmentId}
                          onChange={(e) => setEnrollmentId(e.target.value)}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>
                          Student Mobile Number
                        </Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                          placeholder="Enter Student Mobile Number"
                          value={phone}
                          onChange={(e) => setPhone(e.target.value)}
                          className={errors.mobileNo ? styles.error : ''}
                        />
                      </FormControl>
                      {errors.mobileNo && (
                        <FormHelperText error>
                          {errors.mobileNo.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Parent Mobile Number</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          inputProps={{ inputMode: 'numeric', maxLength: 10 }}
                          placeholder="Enter Parent Mobile Number"
                          value={parentMobileNo}
                          onChange={(e) => setParentMobileNo(e.target.value)}
                          className={errors.parentMobileNo ? styles.error : ''}
                        />
                      </FormControl>
                      {errors.parentMobileNo && (
                        <FormHelperText error>
                          {errors.parentMobileNo.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Email Address</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="Your Email Address"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          className={errors.emailId ? styles.error : ''}
                        />
                      </FormControl>
                      {errors.emailId && (
                        <FormHelperText error>
                          {errors.emailId.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>PIN Code</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Input
                          placeholder="PIN Code"
                          value={pinCode}
                          inputProps={{ maxLength: 6 }}
                          onChange={(e) => onPinCodeChange(e.target.value)}
                          className={errors.pinCode ? styles.error : ''}
                        />
                      </FormControl>
                      {errors.pinCode && (
                        <FormHelperText error>
                          {errors.pinCode.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>City</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Box>{cityName}</Box>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>State</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Box>{stateName}</Box>
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Schools / Others</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Autocomplete
                          key={school === null ? 'true' : 'false'}
                          options={schoolsList}
                          getOptionLabel={(option: AutocompleteOption) =>
                            option.title
                          }
                          autoComplete
                          includeInputInList
                          value={school}
                          onChange={(e, node) => setSchool(node)}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              placeholder="Select School"
                            />
                          )}
                          className={errors.schoolName ? styles.error : ''}
                        />
                      </FormControl>
                      {/* {school?.value === 'Other' && <TypeOther />} */}
                      {errors.schoolName && (
                        <FormHelperText error>
                          {errors.schoolName.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Board</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={board}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) =>
                            setBoardAndFetchClasses(e.target.value as string)
                          }
                          displayEmpty
                          className={errors.boardName ? styles.error : ''}
                        >
                          <MenuItem value="">Select a board</MenuItem>
                          {boards.length > 0 &&
                            boards.map((item) => (
                              <MenuItem
                                value={item.boardName}
                                key={item.boardID}
                              >
                                {item.boardName} ({item.boardDescriptions})
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                      {errors.boardName && (
                        <FormHelperText error>
                          {errors.boardName.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Grid container className={styles.paddClass}>
                    <Grid item xs={12} md={4}>
                      <FormControl fullWidth margin="normal">
                        <Box className={styles.label}>Class</Box>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                      <FormControl fullWidth margin="normal">
                        <Select
                          value={standard}
                          onChange={(
                            e: React.ChangeEvent<{ value: unknown }>
                          ) => setStandard(e.target.value as string)}
                          displayEmpty
                          className={errors.className ? styles.error : ''}
                        >
                          <MenuItem value="">Select a class</MenuItem>
                          {classes.length > 0 &&
                            classes.map((standard) => (
                              <MenuItem
                                value={standard.className}
                                key={standard.className}
                              >
                                {standard.className}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                      {errors.className && (
                        <FormHelperText error>
                          {errors.className.message}
                        </FormHelperText>
                      )}
                    </Grid>
                  </Grid>

                  <Box
                    display="flex"
                    justifyContent="flex-end"
                    borderTop="1px solid rgba(224, 224, 224, 0.5)"
                    marginTop="20px"
                    padding="20px 30px"
                  >
                    <FormControl margin="normal">
                      <Box className={styles.addBtn}>
                        <Button
                          disableElevation
                          color="primary"
                          size="large"
                          variant="contained"
                          type="submit"
                        >
                          <AddIcon /> Add
                        </Button>
                      </Box>
                    </FormControl>
                  </Box>
                  {errors.pageError && (
                    <FormHelperText error>
                      {errors.pageError.message}
                    </FormHelperText>
                  )}
                </form>
              </Box>
            </Grid>

            <Grid item xs={12} md={4}>
              <Box
                display="flex"
                flexDirection="column"
                padding="20px 0"
                height="100%"
                borderLeft="1px solid rgba(224, 224, 224, 0.5)"
              >
                <Box display="flex" alignItems="center" marginLeft="30px">
                  <CheckCircleIcon color="primary" fontSize="large" />

                  <Box marginLeft="15px">
                    <Typography component="span" color="secondary">
                      <Box component="h5" className={styles.subHeading}>
                        Enrolled Students
                      </Box>
                    </Typography>
                  </Box>
                </Box>

                <Box flexGrow="1">
                  {students.map((item, index) => (
                    <TutorStudentRow
                      key={index}
                      item={item}
                      handleRemoveItem={() => removeStudent(index)}
                    />
                  ))}
                </Box>

                {students && students.length > 0 && (
                  <Box
                    borderTop="1px solid rgba(224, 224, 224, 0.5)"
                    padding="30px 0"
                  >
                    <Box display="flex" justifyContent="center">
                      <Box marginRight="50px" className={styles.clearBtn}>
                        <Button
                          disableElevation
                          size="large"
                          variant="outlined"
                          onClick={() => {
                            setStudents([]);
                            clearError('serverError');
                          }}
                        >
                          Clear
                        </Button>
                      </Box>

                      <Box>
                        <Button
                          disableElevation
                          color="primary"
                          size="large"
                          variant="contained"
                          onClick={saveStudents}
                        >
                          Save
                        </Button>
                        {errors.serverError && (
                          <FormHelperText error>
                            {errors.serverError.message}
                          </FormHelperText>
                        )}
                      </Box>
                    </Box>
                  </Box>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as Tutor
});

export default connect(mapStateToProps)(StudentsEnrollment);
