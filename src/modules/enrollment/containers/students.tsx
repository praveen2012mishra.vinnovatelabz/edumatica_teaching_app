//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useEffect, useState } from 'react';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import {
  Link as RouterLink,
  Redirect,
  RouteComponentProps
} from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import clsx from 'clsx';
import {
  createStyles,
  lighten,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import {
  Box,
  Container,
  FormControl,
  Grid,
  InputAdornment,
  Input,
  Toolbar,
  Typography,
  IconButton,
  Tooltip
} from '@material-ui/core';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
  Delete as DeleteIcon,
  Edit as EditIcon,
  Visibility as ViewIcon,
  Add as AddIcon
} from '@material-ui/icons';
import PersonWithBook from '../../../assets/images/study.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import {
  isOrganization,
  isAdminTutor,
  exceptionTracker,
  eventTracker, 
  GAEVENTCAT
} from '../../common/helpers';
import { RootState } from '../../../store';
import { User, Student } from '../../common/contracts/user';
import { fetchTutorStudentsList } from '../../common/api/academics';
import { deleteStudents } from '../../common/api/profile';
import {
  getOrgStudentsList,
  deleteStudentsOfOrganization
} from '../../common/api/organization';
import { isTutor } from '../../common/helpers';
import { setCurrentStudent } from '../store/actions';
import { CurrentStudent } from '../contracts/student';
import ConfirmationModal from '../../common/components/confirmation_modal';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'
import { fontOptions } from '../../../theme';
import { setAuthUser } from '../../auth/store/actions';

function createData(
  name: string,
  enrollmentId: string,
  mobileNo: string,
  parentMobileNo: string,
  email: string,
  pincode: string,
  city: string,
  state: string,
  board: string,
  classname: string,
  schoolname: string,
  roleStatus: 'FRESHER'| 'ACTIVE' | 'DEACTIVATE'
): CurrentStudent {
  return {
    name,
    enrollmentId,
    mobileNo,
    parentMobileNo,
    email,
    pincode,
    city,
    state,
    board,
    classname,
    schoolname,
    roleStatus
  };
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      height: '100%'
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85)
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark
          },
    title: {
      flex: '1 1 100%'
    }
  })
);

interface EnhancedTableToolbarProps {
  numSelected: number;
  removeItem: () => any;
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>
  setFocused: React.Dispatch<React.SetStateAction<boolean>>
}

const EnhancedTableToolbar: FunctionComponent<EnhancedTableToolbarProps> = ({
  numSelected,
  removeItem,
  searchText,
  setSearchText,
  setFocused
}) => {
  const classes = useToolbarStyles();
  //const { numSelected } = props;

  return (
    <Toolbar>
      {numSelected > 0 ? (
        <Box
          className={clsx(classes.root, {
            [classes.highlight]: numSelected > 0
          })}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginLeft="auto"
        >
          <Typography
            className={classes.title}
            color="inherit"
            variant="subtitle1"
            component="div"
          >
            {numSelected} selected
          </Typography>

          <Tooltip title="Delete">
            <IconButton aria-label="delete" onClick={removeItem}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </Box>
      ) : null}
      <Box display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <Box margin="15px 10px 10px 25px">
          <FormControl fullWidth margin="normal">
            <Input
              name="search"
              inputProps={{ inputMode: 'search' }}
              onFocus={() => setFocused(true)} 
              onBlur={() => setFocused(false)}
              placeholder="Search"
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              endAdornment={
                searchText.trim() !== '' ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={() => setSearchText('')}>
                      <ClearIcon />
                    </IconButton>
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <IconButton disabled size="small">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </FormControl>
          {(searchText.trim() !== '') &&
            <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666'}}>
              Filtered Table using Keyword '{searchText}'
            </Typography>
          }
        </Box>
      </Box>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    },
    addBtn: {
      '& svg': {
        marginRight: '5px'
      }
    },
  })
);

interface Props extends RouteComponentProps<{}> {
  authUser: User;
}

const Students: FunctionComponent<Props> = ({ authUser, history }) => {
  const classes = useStyles();
  const [selected, setSelected] = React.useState<string[]>([]);
  const [students, setStudents] = React.useState<Student[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);
  const [redirectTo, setRedirectTo] = useState('');
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);

  const dispatch = useDispatch();
  eventTracker(GAEVENTCAT.studentProfileCreate, 'Students List Page', 'Landed Students List Page');

  useEffect(() => {
    (async () => {
      // if(focused === false) {
        try {
          var studentsList = [];
          if (isTutor(authUser)) {
            studentsList = await fetchTutorStudentsList();
          } else {
            studentsList = await getOrgStudentsList();
          }
          if(searchText !== '') {
            studentsList = studentsList.filter(stud => 
              stud.studentName.toLowerCase().includes(searchText.toLowerCase()) ||
              stud.mobileNo.includes(searchText) ||
              stud.boardName.toLowerCase().includes(searchText.toLowerCase()) ||
              stud.className.toLowerCase().includes(searchText.toLowerCase()) ||
              stud.schoolName.toLowerCase().includes(searchText.toLowerCase())
            )
          }
          setStudents(studentsList);
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      // }
    })();
  }, [authUser, searchText]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleEditStudent = (row: CurrentStudent) => {
    dispatch(setCurrentStudent(row));
    history.push('/profile/students/edit');
  };

  const handleViewStudent = (row: CurrentStudent) => {
    dispatch(setCurrentStudent(row));
    history.push('/profile/students/view');
  };

  const handleDeleteStudent = (row: CurrentStudent) => {
    const index = rows.findIndex(data => {
      return (data.name === row.name && data.mobileNo === row.mobileNo)
    })

    setSelection({selectionModel: [index + 1]})
    setOpenConfirmationModal(true);
  }

  const buttonData = [
    {
      title: 'Edit Details',
      action: handleEditStudent,
      icon: <EditIcon />
    },
    {
      title: 'View Details',
      action: handleViewStudent,
      icon: <ViewIcon />
    },
    {
      title: 'Delete Details',
      action: handleDeleteStudent,
      icon: <DeleteIcon />
    }
  ];

  const rows = students.map((student) =>
    createData(
      student.studentName,
      student.enrollmentId ? student.enrollmentId : '',
      student.mobileNo,
      student.parentMobileNo,
      student.emailId,
      student.pinCode,
      student.cityName,
      student.stateName,
      student.boardName,
      student.className,
      student.schoolName,
      student.roleStatus
    )
  )

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'name', headerName: 'Name', flex: 1, renderCell: datagridCellExpand },
    { field: 'mobileNo', headerName: 'Mobile', flex: 1, renderCell: datagridCellExpand },    
    { field: 'board', headerName: 'Board', flex: 1, renderCell: datagridCellExpand },
    { field: 'classname', headerName: 'Class', flex: 1, renderCell: datagridCellExpand },
    { field: 'schoolname', headerName: 'School', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          mobileNo: params.getValue("mobileNo") as string,
          name: params.getValue("name") as string
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.mobileNo === selectedRow.mobileNo && row.name === selectedRow.name && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as CurrentStudent);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = rows.map((row, index) => {
    return ({
      id: (index + 1),
      name: row.name,
      mobileNo: row.mobileNo,      
      board: row.board,
      classname: row.classname,
      schoolname: row.schoolname
    })
  })

  const setSelection = (selected: any) => {
    const selectedRow = selected.selectionModel.map((index: string) => {
      const row = rows[(Number(index) - 1)]
      return row.mobileNo
    })

    setSelected(selectedRow);
  }

  const removeStudents = async () => {
    setOpenConfirmationModal(false);
    try {
      const updatedStudents = students.filter(
        (el) => !selected.includes(el.mobileNo.toString())
      );
      if (isTutor(authUser)) {
        await deleteStudents(selected).then((val) => {
          const newAuthUser = Object.assign({},authUser,{studentList: authUser.studentList?.filter((cur) => !val.data.removedStudentList.includes(cur))})
          dispatch(setAuthUser(newAuthUser))
        })
      }
      if(isOrganization(authUser)) {
        await deleteStudentsOfOrganization(selected).then((val) => {
          const newAuthUser = Object.assign({},authUser,{studentList: authUser.studentList?.filter((cur) => !val.data.removedStudentList.includes(cur))})
          dispatch(setAuthUser(newAuthUser))
        })
      }
      setStudents(updatedStudents);
      setSelected([]);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box bgcolor="white" marginY="20px">
          <Grid container>
            <Grid item xs={12} md={7}>
              <Box padding="20px 30px" display="flex" alignItems="center">
                <img src={PersonWithBook} alt="Person with Book" />

                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Students
                    </Box>
                  </Typography>

                  <Box marginLeft="10px" display="flex">
                    <Box marginLeft="20px" className={classes.addBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="small"
                        component={RouterLink}
                        to={`/profile/students/create`}
                        style={{
                          backgroundColor:'#fff',
                          border:'1px solid rgb(76, 139, 245)',
                          color:'rgb(76, 139, 245)'
                               }}
                      >
                        <Box display="flex" alignItems="center">
                          <AddIcon />
                        </Box>{' '}
                        Add Student
                      </Button>
                    </Box>

                    <Box marginLeft="20px" className={classes.addBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="small"
                        component={RouterLink}
                        to={
                          isOrganization(authUser)
                            ? `/profile/org/batches/create`
                            : isAdminTutor(authUser)
                            ? `/profile/tutor/batches/create`
                            : ''
                        }
                        style={{
                          backgroundColor:'#fff',
                          border:'1px solid rgb(76, 139, 245)',
                          color:'rgb(76, 139, 245)'
                               }}
                      >
                        <Box display="flex" alignItems="center">
                          <AddIcon />
                        </Box>{' '}
                        Create Batch
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={5}>
              <EnhancedTableToolbar
                numSelected={selected.length}
                removeItem={() => removeStudents()}
                searchText={searchText}
                setSearchText={setSearchText}
                setFocused={setFocused}
              />
            </Grid>
          </Grid>

          <Datagrids gridRows={gridRows} gridColumns={gridColumns} setSelection={setSelection} />
        </Box>
      </Container>
      <ConfirmationModal
        header="Delete Student"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => {setOpenConfirmationModal(false); setSelected([])}}
        handleDelete={() => removeStudents()}
      />
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Students);
