import React, { FunctionComponent, useEffect, useState } from 'react';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import {
  Link as RouterLink,
  Redirect,
  RouteComponentProps
} from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import clsx from 'clsx';
import {
  createStyles,
  lighten,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import {
  Box,
  Container,
  FormControl,
  Grid,
  InputAdornment,
  Input,
  Toolbar,
  Typography,
  IconButton,
  Tooltip
} from '@material-ui/core';
import {
  Search as SearchIcon,
  Clear as ClearIcon,
  Delete as DeleteIcon,
  Edit as EditIcon,
  Visibility as ViewIcon,
  Add as AddIcon,
} from '@material-ui/icons';
import PersonWithBook from '../../../assets/images/study.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT, isOrganization } from '../../common/helpers';
import { RootState } from '../../../store';
import { User, Tutor } from '../../common/contracts/user';
import {
  fetchOrgTutorsList,
  deleteOrganizationTutors
} from '../../common/api/organization';
import { setCurrentTutor } from '../store/actions';
import { CurrentTutor } from '../contracts/tutor';
import ConfirmationModal from '../../common/components/confirmation_modal';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'
import { fontOptions } from '../../../theme';
import { setAuthUser } from '../../auth/store/actions';

function createData(
  mobileNo: string,
  name: string,
  email: string,
  schoolname: string,
  ownerId: string | number,
  qualifications: string,
  pinCode: string,
  cityName: string,
  stateName: string,
  tutorId: string | undefined,
  enrollmentId: string | number
): CurrentTutor {
  return {
    mobileNo,
    name,
    email,
    schoolname,
    ownerId,
    qualifications,
    pinCode,
    cityName,
    stateName,

    tutorId,
    enrollmentId
  };
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      height: '100%'
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85)
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark
          },
    title: {
      flex: '1 1 100%'
    }
  })
);

interface EnhancedTableToolbarProps {
  numSelected: number;
  removeItem: () => any;
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>
  setFocused: React.Dispatch<React.SetStateAction<boolean>>
}

const EnhancedTableToolbar: FunctionComponent<EnhancedTableToolbarProps> = ({
  numSelected,
  removeItem,
  searchText,
  setSearchText,
  setFocused
}) => {
  const classes = useToolbarStyles();
  //const { numSelected } = props;

  return (
    <Toolbar>
      {numSelected > 0 ? (
        <Box
          className={clsx(classes.root, {
            [classes.highlight]: numSelected > 0
          })}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginLeft="auto"
        >
          <Typography
            className={classes.title}
            color="inherit"
            variant="subtitle1"
            component="div"
          >
            {numSelected} selected
          </Typography>

          <Tooltip title="Delete">
            <IconButton aria-label="delete" onClick={removeItem}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </Box>
      ) : null}
      <Box display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <Box margin="15px 10px 10px 25px">
          <FormControl fullWidth margin="normal">
            <Input
              name="search"
              inputProps={{ inputMode: 'search' }}
              placeholder="Search"
              onFocus={() => setFocused(true)} 
              onBlur={() => setFocused(false)}
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              endAdornment={
                searchText.trim() !== '' ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={() => setSearchText('')}>
                      <ClearIcon />
                    </IconButton>
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <IconButton disabled size="small">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </FormControl>
          {(searchText.trim() !== '') &&
            <Typography style={{marginTop: '5px', fontSize: fontOptions.size.small, color: '#666666'}}>
              Filtered Table using Keyword '{searchText}'
            </Typography>
          }
        </Box>
      </Box>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      letterSpacing: '1px',
      color: '#212121'
    },
    addBtn: {
      '& svg': {
        marginRight: '5px'
      }
    }
  })
);

interface Props extends RouteComponentProps<{}> {
  authUser: User;
}

const Tutors: FunctionComponent<Props> = ({ authUser, history }) => {
  const classes = useStyles();
  const [selected, setSelected] = React.useState<string[]>([]);
  const [tutors, setTutors] = React.useState<Tutor[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  const [searchText, setSearchText] = useState<string>('');
  const [focused, setFocused] = useState(false);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);

  const dispatch = useDispatch();
  eventTracker(GAEVENTCAT.teacherProfileCreate, 'Tutor List Page', 'Landed Tutor List Page');

  useEffect(() => {
    (async () => {
      // if(focused === false) {
        try {
          let tutors = await fetchOrgTutorsList();
          if(searchText !== '') {
            tutors = tutors.filter(tut => 
              tut.tutorName.toLowerCase().includes(searchText.toLowerCase()) ||
              tut.mobileNo.includes(searchText) ||
              tut.emailId.toLowerCase().includes(searchText.toLowerCase()) ||
              tut.schoolName.toLowerCase().includes(searchText.toLowerCase())
            )
          }
          setTutors(tutors);
        } catch (error) {
          exceptionTracker(error.response?.data.message);
          if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
            setRedirectTo('/login');
          }
        }
      // }
    })();
  }, [authUser, searchText]);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const handleEditTutor = (row: CurrentTutor) => {
    dispatch(setCurrentTutor(row));
    history.push('/profile/tutors/edit');
  };

  const handleViewTutor = (row: CurrentTutor) => {
    dispatch(setCurrentTutor(row));
    history.push('/profile/tutors/view');
  };

  const handleDeleteTutor = (row: CurrentTutor) => {
    const index = rows.findIndex(data => {
      return (data.name === row.name && data.mobileNo === row.mobileNo)
    })

    setSelection({selectionModel: [index + 1]})
    setOpenConfirmationModal(true);
  }

  const buttonData = [
    {
      title: 'Edit Details',
      action: handleEditTutor,
      icon: <EditIcon />
    },
    {
      title: 'View Details',
      action: handleViewTutor,
      icon: <ViewIcon />
    },
    {
      title: 'Delete Details',
      action: handleDeleteTutor,
      icon: <DeleteIcon />
    }
  ];

  const rows = tutors.map((tutor) =>
    createData(
      tutor.mobileNo,
      tutor.tutorName,
      tutor.emailId,
      tutor.schoolName,
      tutor.ownerId ? tutor.ownerId : '',
      tutor.qualifications.join(),
      tutor.pinCode,
      tutor.cityName,
      tutor.stateName,
      tutor._id,
      tutor.enrollmentId ? tutor.enrollmentId : ''
    )
  );

  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'mobileNo', headerName: 'Mobile', flex: 1, renderCell: datagridCellExpand },
    { field: 'enrollmentId', headerName: 'Enrollment ID', flex: 1, renderCell: datagridCellExpand },
    { field: 'name', headerName: 'Name', flex: 1, renderCell: datagridCellExpand },
    { field: 'email', headerName: 'Email', flex: 1, renderCell: datagridCellExpand },
    { field: 'schoolname', headerName: 'School', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          mobileNo: params.getValue("mobileNo") as string,
          name: params.getValue("name") as string
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.mobileNo === selectedRow.mobileNo && row.name === selectedRow.name && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          return (
            <Tooltip key={index} title={button.title}>
              <IconButton
                onClick={() => {
                  button.action(selectedRowDetails as CurrentTutor);
                }}
                size="small"
              >
                {button.icon}
              </IconButton>
            </Tooltip>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = rows.map((row, index) => {
    return ({
      id: (index + 1),
      mobileNo: row.mobileNo,
      enrollmentId: row.enrollmentId ? row.enrollmentId : '-',
      name: row.name,
      email: row.email,
      schoolname: row.schoolname
    })
  })

  const setSelection = (selected: any) => {
    const selectedRow = selected.selectionModel.map((index: string) => {
      const row = rows[(Number(index) - 1)]
      return row.mobileNo
    })

    setSelected(selectedRow);
  }

  const removeTutors = async () => {
    setOpenConfirmationModal(false);
    try {
      const updatedTutors = tutors.filter(
        (el) => !selected.includes(el.mobileNo.toString())
      );
      if(isOrganization(authUser) && authUser.tutorList) {
        const tutorstbd = tutors.filter((el) => selected.includes(el.mobileNo.toString())).map(el => el._id as string)
        const leftTutor = authUser.tutorList.filter(list => !tutorstbd.includes(list))
        console.log(leftTutor, tutorstbd, authUser.tutorList)
        const newAuthUser = {...authUser, ...{tutorList: leftTutor}}
        setAuthUser(newAuthUser)
      }
      await deleteOrganizationTutors(selected);
      setTutors(updatedTutors);
      setSelected([]);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  return (
    <div>
      <MiniDrawer>

      <Container maxWidth="lg">
        <Box bgcolor="white" marginY="20px">
          <Grid container>
            <Grid item xs={12} md={7}>
              <Box padding="20px 30px" display="flex" alignItems="center">
                <img src={PersonWithBook} alt="Person with Book" />

                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Tutors
                    </Box>
                  </Typography>

                  <Box marginLeft="10px" display="flex">
                    <Box marginLeft="20px" className={classes.addBtn}>
                      <Button
                        disableElevation
                        variant="contained"
                        color="primary"
                        size="small"
                        component={RouterLink}
                        to={`/profile/tutors/create`}
                      >
                        <Box display="flex" alignItems="center">
                          <AddIcon />
                        </Box>{' '}
                        Add Tutor
                      </Button>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={5}>
              <EnhancedTableToolbar
                numSelected={selected.length}
                removeItem={() => removeTutors()}
                searchText={searchText}
                setSearchText={setSearchText}
                setFocused={setFocused}
              />
            </Grid>
          </Grid>

          <Datagrids gridRows={gridRows} gridColumns={gridColumns} setSelection={setSelection} />
        </Box>
      </Container>

      <ConfirmationModal
        header="Delete Student"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => {setOpenConfirmationModal(false); setSelected([])}}
        handleDelete={() => removeTutors()}
      />
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Tutors);
