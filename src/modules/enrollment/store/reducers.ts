import { combineReducers, createReducer } from '@reduxjs/toolkit';
import { setCurrentTutor, setCurrentStudent } from './actions';
import { CurrentTutor } from '../contracts/tutor';
import { CurrentStudent } from '../contracts/student';
import { RootState } from '../../../store';
import { AnyAction } from 'redux'

const TUTOR: CurrentTutor = {
  mobileNo: '',
  name: '',
  email: '',
  schoolname: '',
  ownerId: '',
  qualifications: '',
  pinCode: '',
  cityName: '',
  stateName: '',
  tutorId: '',
  enrollmentId: ''
};
const tutor = createReducer(TUTOR, {
  [setCurrentTutor.type]: (_, action) => action.payload
});

const STUDENT: CurrentStudent = {
  name: '',
  enrollmentId: '',
  mobileNo: '',
  parentMobileNo: '',
  email: '',
  pincode: '',
  city: '',
  state: '',
  board: '',
  classname: '',
  schoolname: '',
  roleStatus :'ACTIVE'
};
const student = createReducer(STUDENT, {
  [setCurrentStudent.type]: (_, action) => action.payload
});

const initState = {
  tutor: TUTOR,
  student: STUDENT
};

const appReducer = combineReducers({
  tutor,
  student
})

export const studentTutorReducer = (state: RootState, action: AnyAction) => {
  if (action.type === 'USER_LOGOUT') {
    state = initState
  }
  return appReducer(state, action)
}
