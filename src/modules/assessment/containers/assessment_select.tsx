//@ts-nocheck
// @flow
/* eslint import/no-webpack-loader-syntax: 0 */
import React, { FunctionComponent, useState, useEffect } from 'react';
import { Link as RouterLink, RouteComponentProps } from 'react-router-dom';
import { GridColDef, GridCellParams } from '@material-ui/data-grid';
import { connect } from 'react-redux';
import { createStyles, makeStyles, Theme, lighten, darken } from '@material-ui/core/styles';
import { AssignedAssessment } from '../helper/contracts'
import clsx from 'clsx';
import {
  Box,
  Container,
  FormControlLabel,
  Grid,
  Switch,
  Typography,
  IconButton,
  InputAdornment,
  FormControl,
  Input,
  Tooltip,
  Toolbar
} from '@material-ui/core';
import AsmBooks from '../../../assets/svgs/asmBooks.svg'
import {
  Search as SearchIcon,
  Clear as ClearIcon,
  Edit as EditIcon,
  Delete as DeleteIcon,
  Add as AddIcon,
  ListAlt as ListAltIcon,
  AssignmentInd as AssignmentIndIcon,
  CloudUpload as CloudUploadIcon
} from '@material-ui/icons';
import AssessmentIcon from '../../../assets/images/assessment-checklist.png';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { RootState } from '../../../store';
import { User } from '../../common/contracts/user';
import { Redirect } from 'react-router-dom';
import {
  deleteAssessments,
  getAssessmentsForTutor,
  getAssignedAssessmentData,
  createAssessmentCopy,
  fetchAssignedAssessment
  // publishAssessment
} from '../helper/api';
import { Assessment } from '../contracts/assessment_interface';
import { isOrganization } from '../../common/helpers';
import { useSnackbar } from 'notistack';
import ConfirmationModal from '../../common/components/confirmation_modal';
import Datagrids, {datagridCellExpand} from '../../common/components/datagrids'
import { fontOptions } from '../../../theme';
import { fetchOrgBatchesList } from '../../common/api/batch';
import { fetchBatchesList } from '../../common/api/academics';
import { uniq } from 'lodash'
import { Batch } from '../../academics/contracts/batch';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#212121'
    },
    addBtn: {
      '& svg': {
        marginRight: '5px'
      }
    },
    title: {
      fontSize: fontOptions.size.large
    },
    addAssi: {
      '& button': {
        color: '#4C8BF5',
        backgroundColor: '#FFFFFF',
        '&:hover': {
          backgroundColor: darken('#FFFFFF', 0.1),
        }
      },
      paddingTop: '12px'
    },
    drafts: {
      '& button': {
        color: '#FFFFFF',
        backgroundColor: '#F9BD33',
        '&:hover': {
          backgroundColor: darken('#F9BD33', 0.1),
        }
      }
    },
  })
);

interface Props extends RouteComponentProps<{ username: string }> {
  authUser: User;
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      height: '100%'
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85)
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark
          },
    title: {
      flex: '1 1 100%',
      textAlign: 'center'
    }
  })
);

interface EnhancedTableToolbarProps {
  numSelected: number;
  removeItem: () => any;
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>;
  authUser: User;
  myAssessmentFilter: boolean
  setMyAsssessmentFilter: React.Dispatch<React.SetStateAction<boolean>>
}

const EnhancedTableToolbar: FunctionComponent<EnhancedTableToolbarProps> = ({
  numSelected,
  removeItem,
  searchText, 
  setSearchText,
  authUser,
  myAssessmentFilter,
  setMyAsssessmentFilter
}) => {
  const classes = useToolbarStyles();

  return (
    <Toolbar>
      {numSelected > 0 ? (
        <Box
          className={clsx(classes.root, {
            [classes.highlight]: numSelected > 0
          })}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginLeft="auto"
        >
          <Typography
            className={classes.title}
            color="inherit"
            variant="subtitle1"
            component="div"
          >
            {numSelected} selected
          </Typography>

          <Tooltip title="Delete">
            <IconButton aria-label="delete" onClick={removeItem}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </Box>
      ) : null}
      {!isOrganization(authUser) ? (
        <Box display="flex"
          justifyContent="space-between"
          alignItems="center"
          marginLeft="auto"
        >
          <Box margin="15px 10px 10px 25px">
            <FormControlLabel
              control={
                <Switch
                  checked={myAssessmentFilter}
                  onChange={(
                    ev: React.ChangeEvent<HTMLInputElement>
                  ) => {
                    const checked = ev.target.checked;
                    setMyAsssessmentFilter(checked);
                  }}
                  name="myAssessmentFilter"
                  color="primary"
                />
              }
              label="Private"
            />
          </Box>
        </Box>
      ) : null}
      <Box display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginLeft="auto"
      >
        <Box margin="15px 10px 10px 25px">
          <FormControl fullWidth margin="normal">
            <Input
              name="search"
              inputProps={{ inputMode: 'search' }}
              placeholder="Search"
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
              endAdornment={
                searchText.trim() !== '' ? (
                  <InputAdornment position="end">
                    <IconButton size="small" onClick={() => setSearchText('')}>
                      <ClearIcon />
                    </IconButton>
                  </InputAdornment>
                ) : (
                  <InputAdornment position="end">
                    <IconButton disabled size="small">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }
            />
          </FormControl>
        </Box>
      </Box>
    </Toolbar>
  );
};

const Assessment_selection: FunctionComponent<Props> = ({
  authUser,
  history
}) => {
  const classes = useStyles();
  const [selected, setSelected] = React.useState<string[]>([]);
  const [rows, setRows] = React.useState<Assessment[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  const [searchText, setSearchText] = useState<string>('');
  const [sourceData, setSourceData] = useState<Assessment[]>([]);
  const [myAssessmentFilter, setMyAsssessmentFilter] = useState<boolean>(false);
  const [assignedAssessments, setAssignedAssessments] = useState<string[]>([]);
  const [openConfirmationModal, setOpenConfirmationModal] = useState(false);
  const [assigned, setAsstgned] = useState<AssignedAssessment[]>()
  const [batchDetails, setBatchDetails] = useState<Batch[]>()

  eventTracker(GAEVENTCAT.assessment, 'Assessment List Page', 'Landed Assessment List Page');

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    getassessments(myAssessmentFilter);
    // eslint-disable-next-line
  }, [setMyAsssessmentFilter]);

  useEffect(() => {
    if (searchText.trim() !== '') {
      setRows(search(sourceData, searchText));
      return;
    } else {
      setRows(sourceData);
    }
  }, [searchText, sourceData]);

  useEffect(() => {
    getAssignedAssessments();
  }, []);

  const handleEditDetails = (data: Assessment) => {
    if (assignedAssessments.includes(data._id)) {
      enqueueSnackbar(
        'Assessment has been assigned to some students. Cannot be Edited',
        { variant: 'warning' }
      );
    } else {
      history.push(
        '/profile/assessment/edit?assessmentId=' + encodeURI(data._id)
      );
    }
  };

  const handleAssignStudents = (data: Assessment) => {
    history.push(
      '/profile/assessment_assign?assessmentId=' + encodeURI(data._id)
    );
  };

  // const handlePublishAssessment = async (data: Assessment) =>{
  //   try{
  //     const response = await publishAssessment(data)
  //     return response
  //   }
  //   catch(error){
  //     if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
  //       setRedirectTo('/login');
  //     }
  //   }
  // }

  const handleEditQuestions = (data: Assessment) => {
    if (assignedAssessments.includes(data._id)) {
      enqueueSnackbar(
        'Assessment has been assigned to some students. Cannot be Edited',
        { variant: 'warning' }
      );
      enqueueSnackbar('Would you like to create an Editable copy?', {
        variant: 'info',
        persist: true,
        action: (key) => {
          return SnackbarAction(key as number, data);
        }
      });
    } else {
      history.push(
        '/profile/assessment_questions?assessmentId=' + encodeURI(data._id)
      );
    }
  }

  const handleDeleteAssessment = (data: Assessment) => {
    const index = rows.findIndex(row => {
      return (row.assessmentname === data.assessmentname && row.boardname === data.boardname)
    })

    setSelection({selectionModel: [index + 1]})
    setOpenConfirmationModal(true);
  }

  const removeAssessments = async () => {
    setOpenConfirmationModal(false);
    try {
      if(selected.some(item => assignedAssessments.includes(item))) {
        const assessmentCbd = selected.filter(item => assignedAssessments.includes(item))
        const assessmentNameCbd = rows.filter(row => assessmentCbd.includes(row._id)).map(row => {
          return row.assessmentname
        }).join(', ')
        enqueueSnackbar(
          'Assessment ' + assessmentNameCbd + 'has been assigned to some students. Cannot be Deleted',
          { variant: 'warning' }
        );
      } else {
        const converted = selected.map(data => encodeURI(data))
        deleteAssessments(converted).then(() => {
          eventTracker(GAEVENTCAT.assessment, 'Assessment Create Update Page', 'Assessment Deleted');
          getassessments(myAssessmentFilter);
        });
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
    setSelected([]);
  }

  // const buttonData = [
  //   {
  //     title: 'Edit Details',
  //     action: handleEditDetails,
  //     icon: <EditIcon />,
  //     // publishCondition : false
  //   },
  //   {
  //     title: 'Assign to Students',
  //     action: handleAssignStudents,
  //     icon: <AssignmentIndIcon />,
  //     // publishCondition : true
  //   },
  //   // { title:'Publish Assessment',
  //   // action : handlePublishAssessment,
  //   // icon: <CloudUploadIcon /> ,
  //   // publishCondition : false
  //   // },
  //   {
  //     title: 'Edit Questions',
  //     action: handleEditQuestions,
  //     icon: <ListAltIcon />,
  //     // publishCondition : false
  //   },
  //   {
  //     title: 'Delete Assessment',
  //     action: handleDeleteAssessment,
  //     icon: <DeleteIcon />,
  //     // publishCondition : false
  //   }
  // ];

  const viewAssessment = (selectedRowDetails: Assessment) => {
    if (assignedAssessments.includes(selectedRowDetails._id)) {
      history.push(
        '/profile/assessment_published?assessmentId=' + encodeURI(selectedRowDetails._id)
      );
    } else {
      editDraft(selectedRowDetails as Assessment)
    }
  }

  const editDraft = (data: Assessment) => {
    history.push(
      '/profile/assessment_status?assessmentId=' + encodeURI(data._id)
    );
  }

  const buttonData = [
    {
      title: 'View',
      action: viewAssessment,
      color: '#4C8BF5'
    }
  ];


  const gridColumns: GridColDef[] = [
    { field: 'id', headerName: 'Sl No.', flex: 0.5 },
    { field: 'assessmentname', headerName: 'Name', flex: 1, renderCell: datagridCellExpand },
    { field: 'boardname', headerName: 'Board', flex: 1, renderCell: datagridCellExpand },
    { field: 'classname', headerName: 'Class', flex: 1, renderCell: datagridCellExpand },
    { field: 'batches', headerName: 'Batch', flex: 1, renderCell: datagridCellExpand },
    { field: 'subjectname', headerName: 'Subject', flex: 1, renderCell: datagridCellExpand },
    { field: 'dnt', headerName: 'Date & Time', flex: 1, renderCell: datagridCellExpand },
    { field: 'totalMarks', headerName: 'Total Marks', flex: 1, renderCell: datagridCellExpand },
    { field: 'action', headerName: 'Action', flex: 1,
      disableClickEventBubbling: true,
      renderCell: (params: GridCellParams) => {
        const selectedRow = {
          id: params.getValue("id") as number,
          assessmentname: params.getValue("assessmentname") as string,
          boardname: params.getValue("boardname") as string
        }

        const selectedRowDetails = rows.find((row, index) => {
          return (row.assessmentname === selectedRow.assessmentname && row.boardname === selectedRow.boardname && index === (selectedRow.id - 1))
        })

        const buttonSet = buttonData.map((button, index) => {
          let btnText = (selectedRowDetails?.sections.some(list => list.questions.length > 0) && (assignedAssessments.includes(selectedRowDetails._id))) ? 'View' : 'Draft'
          const thisassign = assigned?.find(list => list.assessment._id === selectedRowDetails?._id)

          let endsAt;
          if(thisassign && thisassign.endDate && selectedRowDetails) {
            endsAt = new Date(new Date(thisassign.endDate).getTime() + selectedRowDetails.duration * 60000);
          }

          btnText = (thisassign && (new Date(thisassign.startDate) < new Date()) && (new Date(thisassign.solutionTime) > new Date())) ? 'Ongoing' : btnText
          return ( 
            // selectedRowDetails?.published == button.publishCondition &&
            <Box key={index} className={(selectedRowDetails?.sections.some(list => list.questions.length > 0) && (assignedAssessments.includes(selectedRowDetails._id))) ? '' : classes.drafts}>
              <Button
                onClick={() => {
                  (selectedRowDetails?.sections.some(list => list.questions.length > 0) && (assignedAssessments.includes(selectedRowDetails._id))) ? button.action(selectedRowDetails as Assessment) : editDraft(selectedRowDetails as Assessment)
                }}
                variant="contained"
                color="primary"
                disableElevation
                disabled={thisassign && (new Date(thisassign.startDate) < new Date()) && (new Date(thisassign.solutionTime) > new Date())}
              >
                {btnText}
              </Button>
            </Box>
          );
        })
  
        return <div>{buttonSet}</div>;
      }
    }
  ];

  const gridRows = rows.map((row, index) => {
    console.log(row)
    let thisbatchList = '-'
    let dateNtime = '-'
    if(assigned && batchDetails) {
      const thisbatch = uniq(assigned.filter(list => list.assessment._id === row._id).map(list => list.batchId))
      const thisbatchDet = batchDetails.filter(list => thisbatch.includes(list._id as string))
      thisbatchList = thisbatchDet.map(list => list.batchfriendlyname).join(', ')
      const thisassign = assigned.find(list => list.assessment._id === row._id)
      if(thisassign) {
        dateNtime = new Date(thisassign.startDate).toLocaleString()
      }
    }
    return ({
      id: (index + 1),
      assessmentname: row.assessmentname,
      boardname: row.boardname,
      classname: row.classname,
      batches: thisbatchList,
      subjectname: row.subjectname,
      dnt: dateNtime,
      totalMarks: row.totalMarks,
    })
  })

  const setSelection = (selected: any) => {
    const selectedRow = selected.selectionModel.map((index: string) => {
      const row = rows[(Number(index) - 1)]
      return row._id
    })

    setSelected(selectedRow);
  }

  const getAssignedAssessments = async () => {
    try {
      const response = await getAssignedAssessmentData();
      setAssignedAssessments(response);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        setRedirectTo('/login');
      }
    }
  };

  const search = (sourceData: Assessment[], text: string): Assessment[] => {
    const lowerText = text.toLowerCase();
    return sourceData.filter((row: Assessment) => {
      if (row.boardname.toLowerCase().indexOf(lowerText) !== -1) {
        return true;
      }
      if (row.assessmentname.toLowerCase().indexOf(lowerText) !== -1) {
        return true;
      }
      if (row.subjectname.toLowerCase().indexOf(lowerText) !== -1) {
        return true;
      }
      if (row.classname.toLowerCase().indexOf(lowerText) !== -1) {
        return true;
      }
      return false;
    });
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const getassessments = async (privateBool: boolean) => {
    try {
      const response = await getAssessmentsForTutor(privateBool);
      setSourceData(response);
      const assignedAssessmentResp = await fetchAssignedAssessment(undefined, undefined)
      setAsstgned(assignedAssessmentResp);
      const batchesList = (isOrganization(authUser) ?
        await fetchOrgBatchesList() : await fetchBatchesList()
      );
      setBatchDetails(batchesList)
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const createAssessmentDuplicate = async (
    ev: React.MouseEvent<unknown>,
    assessmentData: Assessment
  ) => {
    try {
      const response = await createAssessmentCopy(assessmentData);
      eventTracker(GAEVENTCAT.assessment, 'Assessments List Page', 'Assessment Duplicated');
      setSourceData((prev) => {
        return [...prev, response];
      });
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401)
        // setRedirectTo('/login')
        enqueueSnackbar('Error creating duplicate.Please try again', {
          variant: 'info'
        });
    }
  };

  const SnackbarAction = (key: number, assessmentData: Assessment) => {
    return (
      <React.Fragment>
        <Button
          onClick={(ev: React.MouseEvent<unknown>) => {
            createAssessmentDuplicate(ev, assessmentData);
            closeSnackbar(key);
          }}
        >
          Create Copy
        </Button>
        <Button
          onClick={() => {
            closeSnackbar(key);
          }}
        >
          Cancel
        </Button>
      </React.Fragment>
    );
  };
  
  return (
    <div>
      <MiniDrawer>
      <Container maxWidth="lg" style={{padding: '30px 2.5%'}}>
      <Box
          bgcolor="#4C8BF5"
          padding="20px 30px"
          marginBottom="30px"
          position="relative"
          borderRadius="14px"
          color='#fff'
        >
          <Grid item container>
            <Grid item sm={8}>
              <Box style={{height: '100%'}}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  style={{ height: '100%' }}
                >
                  <Grid item xs={12}>
                    <Typography className={classes.title}>
                      All Assessment
                    </Typography>
                    <Typography>
                      Create Assessment | Add questions | Publish
                    </Typography>
                    <Box className={classes.addAssi}>
                      <Button variant="contained" disableElevation onClick={() => setRedirectTo('/profile/assessment/create')}>
                        Add Assessment
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
            <Grid item sm={4}>
              <Box style={{height: '100%'}}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  style={{ height: '100%' }}
                >
                  <Grid item xs={12}>
                    <img src={AsmBooks} alt="books" style={{height: '150px', float: 'right'}}/>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box
          bgcolor="#ffffff"
          borderRadius="14px"
          padding="25px"
          marginTop='25px'
        >
          <Grid container>
            <Grid item xs={12} md={7} lg={7}>
              <Box padding="20px 30px" display="flex" alignItems="center">
                <Box marginLeft="15px" display="flex" alignItems="center">
                  <Typography component="span" color="secondary">
                    <Box component="h3" className={classes.heading}>
                      Assessments
                    </Box>
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item xs={12} md={5}>
              <EnhancedTableToolbar
                numSelected={selected.length}
                removeItem={() => removeAssessments()}
                searchText={searchText}
                setSearchText={setSearchText}
                authUser={authUser}
                myAssessmentFilter={myAssessmentFilter}
                setMyAsssessmentFilter={setMyAsssessmentFilter}
              />
            </Grid>

          </Grid>
          <Datagrids gridRows={gridRows} gridColumns={gridColumns} setSelection={setSelection} /> 
        </Box>
      </Container>
      <ConfirmationModal
        header="Delete Student"
        helperText="Are you sure you want to delete?"
        openModal={openConfirmationModal}
        onClose={() => {setOpenConfirmationModal(false); setSelected([])}}
        handleDelete={() => removeAssessments()}
      />
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Assessment_selection);
