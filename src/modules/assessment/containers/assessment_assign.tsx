import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { User } from '../../common/contracts/user';
import { connect } from 'react-redux';
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Container,
  Typography,
  Input,
  Checkbox,
  ListItemText
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { RootState } from '../../../store';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker
} from '@material-ui/pickers';
import AssessmentIcon from '../../../assets/images/assessment-checklist.png';
import {
  getAssessmentsForTutor,
  getBatches,
  assignAssessment,
  getStudentsData,
  fetchAssignedAssessment
} from '../helper/api';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { Redirect } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { Assessment } from '../contracts/assessment_interface';
import { Batch } from '../contracts/batch_interface';
import StudentTransferList from '../components/studentTransferList';
import { Student } from '../contracts/student_interface';
import { StudentInfo } from '../contracts/studentInfo_interface';
import { fontOptions } from '../../../theme';
import AsmPaper from '../../../assets/svgs/asmPaper.svg'
import { AssignedAssessment } from '../helper/contracts'
import { uniq } from 'lodash'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#212121'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      marginBottom: '10px',
      color: '#1C2559'
    },
    subLabel: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
    },
    addBtn: {
      '& a': {
        padding: '8px 16px'
      }
    },
    createBtn: {
      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontFamily:fontOptions.family,
        fontSize: fontOptions.size.medium,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        padding: '10px 24px'
      }
    },
    cancelBtn: {
      '& button': {
        background: '#FFFFFF',
        border: '1px solid #666666',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        fontFamily:fontOptions.family,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#666666',
        padding: '10px 24px'
      }
    },
    title: {
      fontSize: fontOptions.size.large
    },
    addAssi: {
      '& button': {
        padding: '10px 30px 10px 30px',
      },
    },
  })
);

function addMinutes(date: Date, minutes: number) {
  console.log(date, minutes)
  return new Date(date.getTime() + minutes * 60000);
}

interface Props
  extends RouteComponentProps<{ mode: string; username: string }> {
  authUser: User;
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const Assessment_assign: FunctionComponent<Props> = ({
  location,
  history,
  authUser
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [assessments, setAssessments] = useState<Assessment[]>([]);
  const [
    selectedAssessment,
    setSelectedAssessment
  ] = useState<Assessment | null>(null);
  const [batches, setBatches] = useState<Batch[]>([]);
  const [selectedBatch, setSelectedBatch] = useState<Batch | null>(null);
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  // eslint-disable-next-line
  const [loading, setLoading] = useState<boolean>(true);
  const [redirectTo, setRedirectTo] = useState('');
  const [studentsList, setStudentList] = useState<Student[]>([]);
  const [studentsLeft, setStudentLeft] = useState<Student[]>([]);
  const [studentsRight, setStudentsRight] = useState<Student[]>([]);
  const [studentData, setStudentData] = useState<StudentInfo[]>([]);
  const [solutionDelay, setSolutionDelay] = useState<number>(0);
  const [studentSelList, setStudentSelList] = React.useState<string[]>([]);
  const [assigned, setAssigned] = useState<AssignedAssessment[]>()
  const [republish, setRepublish] = useState(true);

  const classes = useStyles();

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const repub = params.get('repub');
    if(repub) {
      setRepublish(true)
    } else {
      setRepublish(false)
    }
    getassessments(false);
    getStudentData();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    getAssignedAssessment();
  }, [assessments])

  useEffect(() => {
    if (selectedAssessment !== null) {
      getBatchList({
        boardname: selectedAssessment.boardname,
        classname: selectedAssessment.classname,
        subjectname: selectedAssessment.subjectname
      });
    }
    // eslint-disable-next-line
  }, [selectedAssessment]);

  useEffect(() => {
    if (selectedAssessment !== null ) {
      var studentsArr: Student[] = [];
      if (
        selectedBatch?.students === undefined ||
        selectedBatch?.students === null
      ) {
        enqueueSnackbar('No students in this batch', { variant: 'warning' });
      } else {
        studentsArr = selectedBatch?.students;
      }
      setStudentLeft(studentsArr);
      setStudentList(studentsArr)
      setStudentsRight([]);
    }
    // eslint-disable-next-line
  }, [selectedBatch]);

  const getStudentData = async () => {
    try {
      const response = await getStudentsData();
      setStudentData(response);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        // setRedirectTo('/login')
      }
    }
  };

  const getAssignedAssessment = async () => {
    try {
      const params = new URLSearchParams(location.search);
      const assessmentId = params.get('assessmentId');
      if(assessmentId) {
        const assignedAssessmentResp = await fetchAssignedAssessment(undefined, assessmentId as string)
        setAssigned(assignedAssessmentResp)
        if(assessments.length > 0) {
          const thisAssessment = assessments.find(list => list._id === assignedAssessmentResp[0].assessment._id)
          if(thisAssessment) {
            setSelectedAssessment(thisAssessment)
            setStartDate(new Date(assignedAssessmentResp[0].startDate))
            setEndDate(new Date(assignedAssessmentResp[0].endDate))
            let solutionDel =(assignedAssessmentResp[0].solutionTime.getTime() - assignedAssessmentResp[0].endDate.getTime()) / 1000;
            solutionDel /= 60;
            setSolutionDelay(solutionDel)
          }
        }
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  }

  const handleStudentChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if((event.target.value as string[]).length > 0) {
    }
    setStudentSelList(event.target.value as string[]);
    const selectedSt = studentsList.filter(list => (event.target.value as string[]).includes(list._id as string))
    const nselectedSt = studentsList.filter(list => !(event.target.value as string[]).includes(list._id as string))
    setStudentsRight(selectedSt)
    setStudentLeft(nselectedSt)
  };

  const getassessments = async (privateBool: boolean) => {
    try {
      const response = await getAssessmentsForTutor(privateBool);
      setAssessments(response);
      const params = new URLSearchParams(location.search);
      const assessmentId = params.get('assessmentId');
      if (assessmentId) {
        setSelectedAssessment(
          response.filter((el) => el._id === assessmentId)[0]
        );
      }
      setLoading(false);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const clearForm = () => {
    setSelectedBatch(null);
    // setSelectedAssessment(null);
    setStartDate(null);
    setEndDate(null);
    enqueueSnackbar('All fields cleared', { variant: 'info' });
  };

  const getBatchList = async (queryData: Object) => {
    const batchList = await getBatches(queryData);
    if (batchList.length === 0) {
      enqueueSnackbar(
        'No available batches for current assessment. Please add new batch',
        { variant: 'info' }
      );
    } else {
      setBatches(batchList);
      if(batchList.length === 1) {
        setSelectedBatch(batchList[0])
      }
    }
  };

  const getAddedMarks = () => {
    var addedMarks = 0;
    selectedAssessment?.sections.forEach((section) => {
      section.questions.forEach((question) => {
        addedMarks = addedMarks + (question.marks as number);
      });
    });
    return addedMarks;
  };

  const checkValidation = () => {
    if (startDate?.toString() === 'Invalid Date' || startDate === null) {
      enqueueSnackbar('Please assign a valid Start Date', {
        variant: 'warning'
      });
      return false;
    }
    if ((startDate as Date) < new Date()) {
      enqueueSnackbar('Start Date cannot be in past', { variant: 'warning' });
      return false;
    }
    if (endDate?.toString() === 'Invalid Date' || endDate === null) {
      enqueueSnackbar('Please assign a valid End Date', {
        variant: 'warning'
      });
      return false;
    }
    if ((endDate as Date) < new Date()) {
      enqueueSnackbar('End Date cannot be in past', { variant: 'warning' });
      return false;
    }
    if ((endDate as Date) < (startDate as Date)) {
      enqueueSnackbar('End Date cannot be before Start Date', {
        variant: 'warning'
      });
      return false;
    }
    if (selectedAssessment === null) {
      enqueueSnackbar('Please select a valid assessment', {
        variant: 'warning'
      });
      return false;
    }
    if (getAddedMarks() !== selectedAssessment.totalMarks) {
      enqueueSnackbar(
        'Total Marks should be equal to added marks. Please add/remove questions.',
        { variant: 'warning' }
      );
      return false;
    }
    if (
      !selectedAssessment.sections.some(
        (section) => section.questions.length > 0
      )
    ) {
      enqueueSnackbar('At least one question needs to be added to assessment', {
        variant: 'warning'
      });
      return false;
    }

    if (studentsRight.length === 0) {
      enqueueSnackbar('At least one student needs to be selected', {
        variant: 'warning'
      });
      return false;
    }

    if(assigned) {
      const thisbatch = uniq(assigned.map(list => list.batchId))
      const thisStudent = uniq(assigned.map(list => list.studentId))
      let foo = true

      studentsRight.forEach(element => {
        if(thisbatch.includes(selectedBatch?._id as string) && thisStudent.includes(element._id as string)) {
          enqueueSnackbar(`Student ${element.studentName} has been Asssigned with this Assessment already`, {
            variant: 'warning'
          });
          foo = false;
        }
      });
      return foo
    }

    return true;
  };

  const assignAssessmentData = async () => {    
    if (checkValidation()) {
      const assessmentData = {
        startDate: startDate,
        endDate: endDate,
        assessment: selectedAssessment?._id,
        //@ts-ignore
        solutionTime: addMinutes(endDate as Date, solutionDelay + selectedAssessment?.duration)
      };
      try {
        const response = await assignAssessment(
          assessmentData,
          studentsRight.map((el) => { return {
            student:el._id as string,
            batch: selectedBatch?._id as string
          }})
        );
        if (response.status === 200) {
          enqueueSnackbar('Successfully Assigned Assessments', {
            variant: 'success'
          });
          eventTracker(GAEVENTCAT.assessment, 'Assign Assessment Page', 'Assessment Assigned');
          history.push('/profile/assessment');
        } else {
          enqueueSnackbar('Failure in assigning the assessment', {
            variant: 'warning'
          });
        }
      } catch (err) {
        exceptionTracker(err.response?.data.message);
        if (err.response?.status === 401) {
          setRedirectTo('/login');
        }
      }
    }
  };

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <div>
      <MiniDrawer>
      <Grid container style={{ margin: '20px 0' }}>
        <Container style={{ display: 'flex', justifyContent: 'center' }}>
        <Grid item xl={12} lg={12} md={12}>
          <Box
            bgcolor="#4C8BF5"
            padding="20px 30px"
            marginBottom="30px"
            position="relative"
            borderRadius="14px"
            color='#fff'
          >
            <Grid item container>
              <Grid item sm={8}>
                <Box style={{height: '100%'}}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <Typography className={classes.title}>
                        Assign Assessment
                      </Typography>
                      <Typography>
                        Assign Assessment to Batches
                      </Typography>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
              <Grid item sm={4}>
                <Box style={{height: '100%'}}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <img src={AsmPaper} alt="books" style={{height: '150px', float: 'right'}}/>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box
          bgcolor="#ffffff"
          borderRadius="14px"
          padding="25px"
          marginTop='25px'
        >
          <Grid container spacing={1}>
            <Grid item xs={12} style={{marginBottom: '25px'}}>
              <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium}}>
                Assign Assessment
              </Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Assessment</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                  <FormControl style={{width: '75%'}} margin="normal">
                    <InputLabel
                      className={classes.subLabel}
                      shrink={true}
                    >
                      Select Assessment
                    </InputLabel>
                    <Select
                      required
                      readOnly={republish}
                      value={
                        selectedAssessment === null
                          ? ''
                          : JSON.stringify(selectedAssessment)
                      }
                      onChange={(
                        e: React.ChangeEvent<{ value: unknown }>
                      ) => {
                        const val = e.target.value as string;
                        if (val === '') {
                          setSelectedAssessment(null);
                        } else {
                          setSelectedAssessment(JSON.parse(val));
                        }
                      }}
                      displayEmpty
                    >
                      <MenuItem value="">Select Assessment</MenuItem>
                      {assessments.map((options, index) => {
                        return (
                          <MenuItem
                            key={index}
                            value={JSON.stringify(options)}
                          >
                            {options.assessmentname}
                            {' - '}
                            {options.boardname}
                            {', '}
                            {options.classname}
                            {', '}
                            {options.subjectname}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Batch</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                  <FormControl style={{width: '75%'}} margin="normal">
                    <InputLabel
                      className={classes.subLabel}
                      shrink={true}
                    >
                      Select Batch
                    </InputLabel>
                    <Select
                      required
                      value={
                        selectedBatch === null
                          ? ''
                          : JSON.stringify(selectedBatch)
                      }
                      onChange={(
                        e: React.ChangeEvent<{ value: unknown }>
                      ) => {
                        setStudentList([])
                        setStudentsRight([])
                        setStudentLeft([])
                        setStudentSelList([])
                        const val = e.target.value as string;
                        if (val === '') {
                          setSelectedBatch(null);
                        } else {
                          setSelectedBatch(JSON.parse(val));
                        }
                      }}
                      displayEmpty
                    >
                      <MenuItem value="">Select Batch</MenuItem>
                      {batches.map((options, index) => {
                        return (
                          <MenuItem
                            key={index}
                            value={JSON.stringify(options)}
                          >
                            {options.batchfriendlyname}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Students</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                {studentsList.length > 0 || studentsRight.length > 0 ? (
                  <FormControl style={{width: '75%'}} margin="normal">
                    <Select
                      labelId="demo-mutiple-checkbox-label"
                      id="demo-mutiple-checkbox"
                      multiple
                      value={studentSelList}
                      onChange={handleStudentChange}
                      input={<Input />}
                      renderValue={(selected) => {
                        return (
                          (selected as string[]).map(sel => {
                            const selstu = (studentsList.find(stu => stu._id === sel)) as Student
                            return selstu.studentName
                          }).join(', ')
                        )
                      }}
                      MenuProps={MenuProps}
                    >
                      {studentsList.map((stu) => (
                        <MenuItem key={stu._id} value={stu._id}>
                          <Checkbox color="primary" checked={studentSelList.indexOf(stu._id as string) > -1} />
                          <ListItemText primary={stu.studentName} />
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  ) : (
                  <React.Fragment>
                    {selectedBatch !== null && (
                      <Typography variant="body1" style={{ color: 'gray' }}>
                        {studentsLeft.filter(
                          (value) =>
                            !studentData
                              .filter(
                                (val) => val._id === selectedAssessment?._id
                              )[0]
                              .studentArr.includes(value._id)
                        ).length === 0 &&
                          '*All the students from this batch are already have been assigned'}
                      </Typography>
                    )}
                  </React.Fragment>
                )}
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Start Date</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                  <FormControl style={{width: '75%'}} margin="normal">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDateTimePicker
                        disablePast
                        readOnly={republish}
                        variant="dialog"
                        fullWidth
                        id="date-picker-inline1"
                        label="Start Date"
                        format="dd/MM/yyyy HH:mm"
                        mask="__/__/____ __:__"
                        name="startDate"
                        inputVariant="standard"
                        required
                        value={startDate}
                        onChange={(e) => setStartDate(e as Date)}
                        KeyboardButtonProps={{
                          'aria-label': 'change date'
                        }}
                      />
                    </MuiPickersUtilsProvider>

                    <FormHelperText>
                      Assignment Start time
                    </FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} md={6}>
            <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Last Entry Time</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                  <FormControl style={{width: '75%'}} margin="normal">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDateTimePicker
                        disablePast
                        readOnly={republish}
                        variant="dialog"
                        fullWidth
                        format="dd/MM/yyyy HH:mm"
                        mask="__/__/____ __:__"
                        id="date-picker-inline2"
                        label="Grace Time"
                        name="endDate"
                        inputVariant="standard"
                        required
                        value={endDate}
                        onChange={(e) => setEndDate(e as Date)}
                        KeyboardButtonProps={{
                          'aria-label': 'change date'
                        }}
                      />
                    </MuiPickersUtilsProvider>

                    <FormHelperText>Assignment Grace End time</FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>

              <Grid container>
                <Grid item xs={12} md={4}>
                  <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                    <Box>Release Solution</Box>
                  </FormControl>
                </Grid>

                <Grid item xs={12} md={8}>
                  <FormControl style={{width: '75%'}} margin="normal">
                    <InputLabel
                      className={classes.subLabel}
                      shrink={true}
                    >
                      Delay
                    </InputLabel>
                    <Select
                      readOnly={republish}
                      required
                      value={solutionDelay}
                      onChange={(
                        e: React.ChangeEvent<{ value: unknown }>
                      ) => {
                        const val = e.target.value as number;
                        setSolutionDelay(val);
                      }}
                      displayEmpty
                    >
                      <MenuItem value={0}>Immediately</MenuItem>
                      <MenuItem value={720}>After 12 hrs</MenuItem>
                      <MenuItem value={1440}>After 24 hrs</MenuItem>
                      <MenuItem value={2880}>After 48 hrs</MenuItem>
                      <MenuItem value={4320}>After 72 hrs</MenuItem>
                    </Select>
                    <FormHelperText>
                      Reference from End Time
                    </FormHelperText>
                  </FormControl>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <span style={{float: 'right', paddingRight: '7%', paddingTop: '25px'}} className={classes.addAssi}>
                <Button color="primary" disableElevation variant="outlined" onClick={() => setRedirectTo('/profile/assessment')}>
                  Save in Drafts
                </Button>
                <Button style={{marginLeft: '15px'}} color="primary" disableElevation variant="contained" onClick={() => assignAssessmentData()}>
                  Assign
                </Button>
              </span>
            </Grid>
          </Grid>
        </Box>
        </Grid>
        </Container>
      </Grid>
      
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Assessment_assign);
