import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { User } from '../../common/contracts/user';
import { connect } from 'react-redux';
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Tooltip,
  Typography,
  Container
} from '@material-ui/core';
import AsmPaper from '../../../assets/svgs/asmPaper.svg'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AssessmentIcon from '../../../assets/images/assessment-checklist.png';
import { RootState } from '../../../store';
import { getCoursesOfTutor } from '../../common/api/tutor';
import { fetchOrgCoursesList } from '../../common/api/organization';
import 'date-fns';
import {
  addAssessment,
  getAssessmentsForTutor,
  getAssessment,
  patchAssessment,
  getTopics
} from '../helper/api';
import { Topic } from "../contracts/topics_interface";
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { Course } from '../../academics/contracts/course';
import { Redirect } from 'react-router-dom';
import AddAlarmIcon from '@material-ui/icons/AddAlarm';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import { useSnackbar } from 'notistack';
import { Assessment } from '../contracts/assessment_interface';
import { Section } from '../contracts/section_interface';
import { isOrganization } from '../../common/helpers';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      letterSpacing: '1px',
      color: '#212121'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
      marginBottom: '10px',
      color: '#1C2559'
    },
    subLabel: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,
    },
    formInput: {
      fontSize: fontOptions.size.small,
      fontFamily:fontOptions.family,

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        fontFamily:fontOptions.family,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    addBtn: {
      '& a': {
        padding: '8px 16px'
      }
    },
    addAssi: {
      '& button': {
        padding: '10px 30px 10px 30px',
      },
    },
    createBtn: {
      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        fontFamily:fontOptions.family,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        padding: '10px 24px'
      }
    },
    cancelBtn: {
      '& button': {
        background: '#FFFFFF',
        border: '1px solid #666666',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        fontFamily:fontOptions.family,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#666666',
        padding: '10px 24px'
      }
    },
    title: {
      fontSize: fontOptions.size.large
    },
  })
);

interface Props
  extends RouteComponentProps<{ mode: string; username: string }> {
  authUser: User;
}

const Assessment_create: FunctionComponent<Props> = ({
  match,
  location,
  history,
  authUser
}) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const mode = match.params.mode;
  const blankCourse = {
    board: '',
    className: '',
    subject: ''
  } as Course;
  const [disableAll, setDisableAll] = useState<boolean>(mode === 'edit');
  const [assessmentTitles, setAssessmentTitles] = useState<string[]>([]);
  const [course, setCourse] = useState<Course[]>([]);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [selectedCourseData, setSelectedCourseData] = useState<Course>(
    blankCourse
  );
  const [name, setName] = useState('');
  const [duration, setDuration] = useState<number>(10);
  const [sections, setSections] = React.useState<Section[]>([
    {
      title: 'Section A',
      questions: [],
      totalMarks: 0,
      duration: 0
    }
  ] as Section[]);
  const [sectionsCount, setSectionsCount] = useState<number | null>(
    sections.length
  );
  const [marks, setMarks] = useState<number>(10);
  const [instructions, setInstructions] = useState('');
  const [redirectTo, setRedirectTo] = useState('');
  const [editId, setEditId] = useState<string | null>(null);
  const [sourceAssessmentName, setSourceAssessmentName] = useState<
    string | null
  >(null)

  const letterArray: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

  const courseRef = React.useRef<HTMLElement | null>(null);
  const nameRef = React.useRef<HTMLElement | null>(null);
  const durationRef = React.useRef<HTMLElement | null>(null);
  const sectionRef = React.useRef<HTMLElement | null>(null);
  const marksRef = React.useRef<HTMLElement | null>(null);
  const instructionRef = React.useRef<HTMLElement | null>(null);

  const classes = useStyles();

  useEffect(() => {
    if (
      sectionsCount !== null &&
      (sectionsCount as number) > 0 &&
      (sectionsCount as number) < 7
    ) {
      modifySections(sectionsCount as number);
    } else {
      return;
    }
    // eslint-disable-next-line
  }, [sectionsCount]);

  useEffect(() => {
    getCourses();
    getAssessmentsTitles(false);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (mode === 'edit') {
      searchAssessment();
    }
    // eslint-disable-next-line
  }, []);

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  const searchAssessment = async () => {
    try {
      const response = (await getAssessment(location.search)) as Assessment;

      const assessmentCourse: Course = {
        board: response.boardname,
        className: response.classname,
        subject: response.subjectname
      };
      setSelectedCourseData(assessmentCourse);
      setSelectedCourse(JSON.stringify(assessmentCourse));
      setMarks(response.totalMarks);
      setName(response.assessmentname);
      setInstructions(response.instructions);
      setDuration(response.duration);
      setSections(response.sections);
      setEditId(response._id);
      setSourceAssessmentName(response.assessmentname.toLowerCase());
      setSectionsCount(response.sections.length);
      setDisableAll(false);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        setRedirectTo('/login');
      } else {
        enqueueSnackbar("Assessment data couldn't be loaded.Try Again", {
          variant: 'error'
        });
      }
    }
  };

  const getAssessmentsTitles = async (privateBool: boolean) => {
    try {
      const response = await getAssessmentsForTutor(privateBool);
      setAssessmentTitles(
        response.map((el: Assessment) => {
          return el.assessmentname.toLowerCase();
        })
      );
    } catch (err) {
      exceptionTracker(err.response?.data.message);

      if (err.response?.status === 401) {
        setRedirectTo('/login');
      }
    }
  };

  const getCourses = async () => {
    try {
      const response = isOrganization(authUser)
        ? await fetchOrgCoursesList()
        : await getCoursesOfTutor();

      setCourse(response);

      if(response.length === 1 && mode === 'create') {
        setSelectedCourse(JSON.stringify(response[0]))
        setSelectedCourseData(response[0])
      }
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const createAssessment = async (addQuestionBool: boolean) => {
    if (checkValidation()) {
      var assessment = {
        boardname: selectedCourseData.board,
        classname: selectedCourseData.className,
        subjectname: selectedCourseData.subject,
        sections: sections,
        assessmentname: name,
        instructions: instructions,
        duration: duration,
        totalMarks: marks
      };
      if (mode === 'edit') {
        try{
          if (addQuestionBool) {
            const promise1 = new Promise(async(resolve, reject) => {
              const TopicArr :Topic[] = []
              const response = await getTopics('?boardname=' + assessment.boardname + '&classname=' + assessment.classname + '&subjectname=' + assessment.subjectname)
              response.forEach((doc, index)=>{
                if(TopicArr.findIndex(el=>el.title===doc._id.topic)===-1){
                  TopicArr.push({
                    title:doc._id.topic,
                    questionSet:[[doc._id.questions,doc.questions]]
                  } as Topic)
                }
                else{
                  const currentTopic = TopicArr[TopicArr.findIndex(el=>el.title===doc._id.topic)]
                  TopicArr[TopicArr.findIndex(el=>el.title===doc._id.topic)] = {
                    ...currentTopic,
                    questionSet:[...currentTopic.questionSet,[doc._id.questions,doc.questions]]
                  }
                }
      
                if(response.length === index + 1) {
                  resolve(TopicArr)
                }
              })
              if(response.length === 0) {
                resolve(TopicArr)
              }
            });
            
            promise1.then(async (value) => {
              console.log(value)
              if((value as Topic[]).length===0){
                enqueueSnackbar("No Questions available for Selected Courses", { variant: "warning" })
              } else {
                await patchAssessment(
                  '?assessmentId=' + editId + '&update=false',
                  assessment as Assessment
                );
                eventTracker(GAEVENTCAT.assessment, 'Assessment Create Update Page', 'Assessment Created');
                history.push(
                  '/profile/assessment_questions?assessmentId=' +
                  encodeURI(editId as string)
                );
              }
            });
          } else {
            await patchAssessment(
              '?assessmentId=' + editId + '&update=false',
              assessment as Assessment
            );
            eventTracker(GAEVENTCAT.assessment, 'Assessment Create Update Page', 'Assessment Created');
            history.push('/profile/assessment');
          }
        }
        catch(err){
          exceptionTracker(err.response?.data.message);
          if (err.response?.status === 401) {
          setRedirectTo('/login');
          }
          if (err.response?.status === 422) {
              console.log(err.response?.data.message)
              // enqueueSnackbar(err.response?.data.message,{variant:"warning"})
            }

        }
        
        
      } else {
        try{
          if (addQuestionBool) {
            const promise1 = new Promise(async(resolve, reject) => {
              const TopicArr :Topic[] = []
              const response = await getTopics('?boardname=' + assessment.boardname + '&classname=' + assessment.classname + '&subjectname=' + assessment.subjectname)
              response.forEach((doc, index)=>{
                if(TopicArr.findIndex(el=>el.title===doc._id.topic)===-1){
                  TopicArr.push({
                    title:doc._id.topic,
                    questionSet:[[doc._id.questions,doc.questions]]
                  } as Topic)
                }
                else{
                  const currentTopic = TopicArr[TopicArr.findIndex(el=>el.title===doc._id.topic)]
                  TopicArr[TopicArr.findIndex(el=>el.title===doc._id.topic)] = {
                    ...currentTopic,
                    questionSet:[...currentTopic.questionSet,[doc._id.questions,doc.questions]]
                  }
                }
      
                if(response.length === index + 1) {
                  resolve(TopicArr)
                }
              })

              if(response.length === 0) {
                resolve(TopicArr)
              }
            });
            
            promise1.then(async (value) => {
              if((value as Topic[]).length===0){
                enqueueSnackbar("No Questions available for Selected Courses", { variant: "warning" })
              } else {
                const response = await addAssessment(assessment);
                eventTracker(GAEVENTCAT.assessment, 'Assessment Create Update Page', 'Assessment Created');
                history.push(
                  '/profile/assessment_questions?assessmentId=' +
                  encodeURI(response._id as string)
                );
              }
            });
          } else {
            const response = await addAssessment(assessment);
            eventTracker(GAEVENTCAT.assessment, 'Assessment Create Update Page', 'Assessment Created');
            history.push('/profile/assessment');
          }

        }
        catch(err){
          exceptionTracker(err.response?.data.message);
          if (err.response?.status === 401) {
          setRedirectTo('/login');
          }
          if (err.response?.status === 422) {
              console.log(err.response?.data.message)
              // enqueueSnackbar(err.response?.data.message,{variant:"warning"})
            }
        }
        
      }
    } else {
    }
  };

  const SnackbarAction = (key: number, newSectionCount: number) => {
    return (
      <React.Fragment>
        <Button
          onClick={() => {
            setSections((prev) => {
              while (prev.length > newSectionCount) {
                prev.pop();
              }
              return prev;
            });
            closeSnackbar(key);
          }}
        >
          Confirm
        </Button>
        <Button
          onClick={() => {
            closeSnackbar(key);
          }}
        >
          Dismiss
        </Button>
      </React.Fragment>
    );
  };

  const modifySections = (newCount: number): void => {
    const prevSectionLength: number = sections.length;
    const newSectionLength: number = newCount;
    const sectionsWithQuestions: Section[] = [];
    if (newSectionLength < prevSectionLength) {
      sections.forEach((section: Section, index) => {
        if (index >= newSectionLength) {
          if (section.questions.length > 0) {
            sectionsWithQuestions.push(section);
          }
        }
      });

      if (sectionsWithQuestions.length > 0) {
        var sectionString: string = '';
        sectionsWithQuestions.forEach((section: Section) => {
          sectionString += section.title + ' ';
        });
        enqueueSnackbar(
          'You are overwriting data for following sections:' +
            sectionString +
            '. Please Confirm.',
          {
            variant: 'info',
            persist: true,
            action: (key) => {
              return SnackbarAction(key as number, newSectionLength);
            }
          }
        );
      } else {
        setSections((prev) => {
          while (prev.length > newSectionLength) {
            prev.pop();
          }
          return prev;
        });
      }
    } else if (newSectionLength > prevSectionLength)
      setSections((prev) => {
        while (prev.length < newSectionLength) {
          prev.push({
            title: 'Section ' + letterArray[prev.length],
            questions: [],
            totalMarks: 0,
            duration: 0
          } as Section);
        }
        return prev;
      });
  };

  const checkValidation = () => {
    if (
      !(
        selectedCourseData.board.length > 0 &&
        selectedCourseData.className.length > 0 &&
        selectedCourseData.subject.length > 0
      )
    ) {
      enqueueSnackbar('Please select a valid course', { variant: 'error' });
      courseRef.current?.focus();
      return false;
    }
    if (!(name.length >= 5 && name.length <= 50)) {
      enqueueSnackbar('Assessment name should contain 5-50 characters', {
        variant: 'error'
      });
      nameRef.current?.focus();
      return false;
    }
    if (
      assessmentTitles.includes(name.toLowerCase()) &&
      mode === 'edit' &&
      name.toLowerCase() !== sourceAssessmentName
    ) {
      enqueueSnackbar('An assessment with same name exists', {
        variant: 'error'
      });
      nameRef.current?.focus();
      return false;
    }
    if (!(duration >= 10 && duration <= 300)) {
      enqueueSnackbar('Duration should be between 10-300 mins', {
        variant: 'error'
      });
      durationRef.current?.focus();
      return false;
    }
    if (!(marks >= 10 && marks <= 500)) {
      enqueueSnackbar('Marks should be between 10-500', { variant: 'error' });
      marksRef.current?.focus();
      return false;
    }
    if (!(instructions.length >= 10 && instructions.length <= 1000)) {
      enqueueSnackbar('Instructions should be between 10-1000 characters', {
        variant: 'error'
      });
      instructionRef.current?.focus();
      return false;
    }
    if (!(sections.length > 0 && sections.length <= 6)) {
      enqueueSnackbar('No. of sections should be between 1-6', {
        variant: 'error'
      });
      sectionRef.current?.focus();
      return false;
    }
    return true;
  };
  const clearForm = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setDuration(0);
    setName('');
    setInstructions('');
    setMarks(0);
  };

  return (
    <div>
      <MiniDrawer>
        <Grid container style={{ margin: '20px 0' }}>
          <Container style={{ display: 'flex', justifyContent: 'center' }}>
            <Grid item xl={12} lg={12} md={12}>
              <Box
                bgcolor="#F9BD33"
                padding="20px 30px"
                marginBottom="30px"
                position="relative"
                borderRadius="14px"
                color='#fff'
              >
                <Grid item container>
                  <Grid item sm={8}>
                    <Box style={{height: '100%'}}>
                      <Grid
                        container
                        direction="row"
                        alignItems="center"
                        justify="center"
                        style={{ height: '100%' }}
                      >
                        <Grid item xs={12}>
                          <Typography className={classes.title}>
                            Add Assessment
                          </Typography>
                          <Typography>
                            Create Assessment
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                  <Grid item sm={4}>
                    <Box style={{height: '100%'}}>
                      <Grid
                        container
                        direction="row"
                        alignItems="center"
                        justify="center"
                        style={{ height: '100%' }}
                      >
                        <Grid item xs={12}>
                          <img src={AsmPaper} alt="books" style={{height: '150px', float: 'right'}}/>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                </Grid>
              </Box>

              <Box
                bgcolor="#ffffff"
                borderRadius="14px"
                padding="25px"
                marginTop='25px'
              >
                <Grid container spacing={1}>
                  <Grid item xs={12} style={{marginBottom: '25px'}}>
                    <Typography style={{color: '#3D3D3D', fontSize: fontOptions.size.medium}}>
                      Assessment Information
                    </Typography>
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <Grid container>
                      <Grid item xs={12} md={4}>
                        <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                          <Box>Course</Box>
                        </FormControl>
                      </Grid>

                      <Grid item xs={12} md={8}>
                        <FormControl
                          style={{width: '75%'}}
                          margin="normal"
                          disabled={disableAll}
                        >
                          <InputLabel
                            className={classes.subLabel}
                            shrink={true}
                          >
                            Course
                          </InputLabel>
                          <Select
                            required
                            value={selectedCourse}
                            inputRef={courseRef}
                            onChange={(
                              e: React.ChangeEvent<{ value: unknown }>
                            ) => {
                              setSelectedCourse(e.target.value as string);
                              if (e.target.value !== '') {
                                setSelectedCourseData(
                                  JSON.parse(e.target.value as string) as Course
                                );
                              } else {
                                setSelectedCourseData(blankCourse);
                              }
                            }}
                            displayEmpty
                          >
                            <MenuItem value="">Select Course</MenuItem>
                            {course.map((options, index) => {
                              return (
                                <MenuItem
                                  key={index}
                                  value={JSON.stringify(options)}
                                >
                                  {options.board}
                                  {', '}
                                  {options.className}
                                  {', '}
                                  {options.subject}
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </FormControl>
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item xs={12} md={4}>
                        <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                          <Box>Total Marks</Box>
                        </FormControl>
                      </Grid>

                      <Grid item xs={12} md={8}>
                        <FormControl style={{width: '75%'}} margin="normal">
                          <InputLabel
                            className={classes.subLabel}
                            shrink={true}
                          >
                            Enter Marks
                          </InputLabel>
                          <Input
                            placeholder="Enter Marks"
                            required
                            disabled={disableAll}
                            inputRef={marksRef}
                            endAdornment={
                              <React.Fragment>
                                <Tooltip title="Add 10 Marks">
                                  <IconButton
                                    onClick={() => {
                                      setMarks(Number(marks) + 10);
                                    }}
                                  >
                                    <NoteAddIcon />
                                  </IconButton>
                                </Tooltip>
                              </React.Fragment>
                            }
                            type="number"
                            className={classes.formInput}
                            value={marks as number}
                            onChange={(
                              e: React.ChangeEvent<{ value: unknown }>
                            ) => setMarks(e.target.value as number)}
                          />
                          <FormHelperText>10-500 marks allowed</FormHelperText>
                        </FormControl>
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item xs={12} md={4}>
                        <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                          <Box>Instructions</Box>
                        </FormControl>
                      </Grid>

                      <Grid item xs={12} md={8}>
                        <FormControl style={{width: '75%'}} margin="normal">
                          <Input
                            placeholder="Enter  Instructions"
                            required
                            multiline
                            disabled={disableAll}
                            rows={4}
                            inputRef={instructionRef}
                            value={instructions}
                            className={classes.formInput}
                            onChange={(e) => setInstructions(e.target.value)}
                          />
                          <FormHelperText>
                            10-1000 characters allowed
                          </FormHelperText>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={12} md={6}>
                    <Grid container>
                      <Grid item xs={12} md={4}>
                        <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                          <Box>Name</Box>
                        </FormControl>
                      </Grid>

                      <Grid item xs={12} md={8}>
                        <FormControl style={{width: '75%'}} margin="normal">
                          <InputLabel
                            className={classes.subLabel}
                            shrink={true}
                          >
                            Enter Name
                          </InputLabel>
                          <Input
                            placeholder="Assessment Name"
                            value={name}
                            disabled={disableAll}
                            inputRef={nameRef}
                            onChange={(e) => setName(e.target.value)}
                            className={classes.formInput}
                          />
                          <FormHelperText>
                            5-50 characters allowed and should be unique!
                          </FormHelperText>
                        </FormControl>
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item xs={12} md={4}>
                        <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                          <Box>Duration</Box>
                        </FormControl>
                      </Grid>

                      <Grid item xs={12} md={8}>
                        <FormControl style={{width: '75%'}} margin="normal">
                          <InputLabel
                            className={classes.subLabel}
                            shrink={true}
                          >
                            Enter Duration(Mins)
                          </InputLabel>
                          <Input
                            placeholder="Enter Duration"
                            required
                            inputRef={durationRef}
                            type="number"
                            disabled={disableAll}
                            value={duration as number}
                            endAdornment={
                              <Tooltip title="Add 10 mins">
                                <IconButton
                                  onClick={() => {
                                    setDuration(Number(duration) + 10);
                                  }}
                                >
                                  <AddAlarmIcon />
                                </IconButton>
                              </Tooltip>
                            }
                            className={classes.formInput}
                            onChange={(
                              e: React.ChangeEvent<{ value: unknown }>
                            ) => setDuration(e.target.value as number)}
                          />
                          <FormHelperText>10-300 mins allowed</FormHelperText>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={12}>
                    <span style={{float: 'right', paddingRight: '7%', paddingTop: '25px'}} className={classes.addAssi}>
                      <Button color="primary" disableElevation variant="outlined" onClick={() => createAssessment(false)}>
                        Save in Drafts
                      </Button>
                      <Button style={{marginLeft: '15px'}} color="primary" disableElevation variant="contained" onClick={() => createAssessment(true)}>
                        Next
                      </Button>
                    </span>
                  </Grid>
                </Grid>
              </Box>
            </Grid>
          </Container>
        </Grid>
      </MiniDrawer>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Assessment_create);
