import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { User } from '../../common/contracts/user';
import { connect } from 'react-redux';
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  Input,
  Avatar,
  InputLabel,
  MenuItem,
  Container,
  Paper,
  Select,
  Tooltip,
  Typography
} from '@material-ui/core';
import AssiUploadDet from '../../../assets/svgs/assiUploadDet.svg'
import AsmPaper from '../../../assets/svgs/asmPaper.svg'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AssessmentIcon from '../../../assets/images/assessment-checklist.png';
import { RootState } from '../../../store';
import { getCoursesOfTutor } from '../../common/api/tutor';
import { fetchOrgCoursesList } from '../../common/api/organization';
import IconCal from '../../../assets/svgs/iconCal.svg'
import IconNumb from '../../../assets/svgs/iconNumb.svg'
import IconUser from '../../../assets/svgs/iconUser.svg'
import DeleteBin from '../../../assets/svgs/deleteBin.svg'
import 'date-fns';
import {
  addAssessment,
  getAssessmentsForTutor,
  getAssessment,
  patchAssessment,
  getAssignedAssessmentData,
  deleteAssessments,
} from '../helper/api';
import Button from '../../common/components/form_elements/button';
import MiniDrawer from '../../common/components/sidedrawer';
import { exceptionTracker, eventTracker, GAEVENTCAT } from '../../common/helpers';
import { Course } from '../../academics/contracts/course';
import { Redirect } from 'react-router-dom';
import AddAlarmIcon from '@material-ui/icons/AddAlarm';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import { useSnackbar } from 'notistack';
import { Assessment } from '../contracts/assessment_interface';
import { Section } from '../contracts/section_interface';
import { isOrganization } from '../../common/helpers';
import { fontOptions } from '../../../theme';
import assessmentBook from '../../../assets/images/assessment-book.png'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    assessInfoText: {
      fontWeight: fontOptions.weight.normal,
      fontSize: fontOptions.size.medium,
      lineHeight: fontOptions.size.mediumPlus,
      marginBottom: '20px'
    },
    assessmentHeader: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
    },
    assessmentContainer: {
      borderRadius: '10px',
      background: '#F9BD33',
      color: '#fff',
      padding: '30px',
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: '20px'
    },
    assessmentFormContainer: {
      borderRadius: '10px',
      background: '#fff',
      color: '#000',
      padding: '30px',
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: '20px',
      flexDirection: 'column'
    },
    assessmentSubHeading: {
      //fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      lineHeight: fontOptions.size.medium,
    },
    assessmentHeading: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.large,
      lineHeight: fontOptions.size.large,
    },
    heading: {
      margin: '0px',
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.medium,
      fontFamily: fontOptions.family,
      letterSpacing: '1px',
      color: '#212121'
    },
    label: {
      fontWeight: fontOptions.weight.bold,
      fontSize: fontOptions.size.small,
      fontFamily: fontOptions.family,
      marginBottom: '10px',
      color: '#1C2559'
    },
    subLabel: {
      fontSize: fontOptions.size.small,
      fontFamily: fontOptions.family,
    },
    formInput: {
      fontSize: fontOptions.size.small,
      fontFamily: fontOptions.family,

      '& input::placeholder': {
        fontWeight: fontOptions.weight.normal,
        fontSize: fontOptions.size.small,
        fontFamily: fontOptions.family,
        lineHeight: '18px',
        color: 'rgba(0, 0, 0, 0.54)'
      }
    },
    addBtn: {
      '& a': {
        padding: '8px 16px'
      }
    },
    createBtn: {
      '& button': {
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        fontFamily: fontOptions.family,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#FFFFFF',
        padding: '10px 24px'
      }
    },
    cancelBtn: {
      '& button': {
        background: '#FFFFFF',
        border: '1px solid #666666',
        borderRadius: '5px',
        fontWeight: fontOptions.weight.bold,
        fontSize: fontOptions.size.medium,
        fontFamily: fontOptions.family,
        lineHeight: '28px',
        letterSpacing: '1px',
        color: '#666666',
        padding: '10px 24px'
      }
    },
    title: {
      fontSize: fontOptions.size.large
    },
  })
);

interface Props
  extends RouteComponentProps<{ mode: string; username: string }> {
  authUser: User;
}

const Assessment_status: FunctionComponent<Props> = ({
  match,
  location,
  history,
  authUser
}) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const [myAssessmentFilter, setMyAsssessmentFilter] = useState<boolean>(false);
  const [sourceData, setSourceData] = useState<Assessment[]>([]);
  const [redirectTo, setRedirectTo] = useState('');
  const [assignedAssessments, setAssignedAssessments] = useState<string[]>([]);

  useEffect(() => {
    getassessments(myAssessmentFilter);
    getAssignedAssessments();
    // eslint-disable-next-line
  }, [setMyAsssessmentFilter, location]);

  const getAssignedAssessments = async () => {
    try {
      const response = await getAssignedAssessmentData();
      setAssignedAssessments(response);
    } catch (err) {
      exceptionTracker(err.response?.data.message);
      if (err.response?.status === 401) {
        setRedirectTo('/login');
      }
    }
  };

  const getassessments = async (privateBool: boolean) => {
    try {
      let response = await getAssessmentsForTutor(privateBool);
      const params = new URLSearchParams(location.search);
      const assessmentId = params.get('assessmentId');
      response = response.filter(list => list._id === assessmentId)
      setSourceData(response);
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  };

  const handleEditDetails = (data: Assessment) => {
    if (assignedAssessments.includes(data._id)) {
      enqueueSnackbar(
        'Assessment has been assigned to some students. Cannot be Edited',
        { variant: 'warning' }
      );
    } else {
      history.push(
        '/profile/assessment/edit?assessmentId=' + encodeURI(data._id)
      );
    }
  };

  const publishthis = (data: Assessment) => {
    history.push('/profile/assessment_assign?assessmentId=' + encodeURI(data._id));
  }

  const deletethis = async(data: Assessment) => {
    try {
      await deleteAssessments([data._id])
      history.push(
        '/profile/assessment'
      );
    } catch (error) {
      exceptionTracker(error.response?.data.message);
      if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
        setRedirectTo('/login');
      }
    }
  }

  if (redirectTo.length > 0) {
    return <Redirect to={redirectTo} />;
  }

  let totalmark = 0;
  sourceData[0]?.sections.forEach(sec => {
    sec.questions.forEach(ques => {
      totalmark = totalmark + ques.marks
    })
  })

  return (
    <div>
      <MiniDrawer>

      <Grid container style={{ margin: '20px 0' }}>
        <Container style={{ display: 'flex', justifyContent: 'center' }}>
          <Grid item xl={12} lg={12} md={12}>
          <Box
            bgcolor="#F9BD33"
            padding="20px 30px"
            marginBottom="30px"
            position="relative"
            borderRadius="14px"
            color='#fff'
          >
            <Grid item container>
              <Grid item sm={8}>
                <Box style={{height: '100%'}}>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="center"
                    style={{ height: '100%' }}
                  >
                    <Grid item xs={12}>
                      <Typography className={classes.title}>
                        {sourceData[0] && sourceData[0].assessmentname}
                      </Typography>
                      <Typography>
                        {sourceData[0] && sourceData[0].boardname} - {sourceData[0] && sourceData[0].boardname} - {sourceData[0] && sourceData[0].subjectname} 
                      </Typography>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
              <Grid item sm={4}>
                <Box>
                  <Grid container>
                    <Grid item xs={12}>
                      <Box>
                        <div>
                          <Tooltip title="Delete">
                            <img src={DeleteBin} 
                              alt="Delete" style={{backgroundColor: '#FFFFFF', float: 'right', borderRadius: '50%', paddingTop: '3px', cursor: 'pointer'}}
                              onClick={() => deletethis(sourceData[0])} 
                            />
                          </Tooltip>
                        </div>
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="25px"
            marginTop='25px'
          >
            <Grid container>
              <Grid item xs={12} sm={6} md={3}>
                <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                  <Grid item xs={2} style={{paddingTop: '5px'}}>
                    <img src={IconNumb} alt="bullets" />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Course</Typography>
                    <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{sourceData[0] && sourceData[0].boardname} - {sourceData[0] && sourceData[0].subjectname} </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                  <Grid item xs={2} style={{paddingTop: '5px'}}>
                    <img src={IconCal} alt="bullets" />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Last Saved on</Typography>
                    <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{sourceData[0] && new Date(sourceData[0].updatedon as Date).toLocaleString()} </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                  <Grid item xs={2} style={{paddingTop: '5px'}}>
                    <img src={IconNumb} alt="bullets" />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Total Marks</Typography>
                    <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{sourceData[0] && sourceData[0].totalMarks} </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Grid container direction="row" alignItems="center" justify="center" style={{ height: '100%' }}>
                  <Grid item xs={2} style={{paddingTop: '5px'}}>
                    <img src={IconCal} alt="bullets" />
                  </Grid>
                  <Grid item xs={10}>
                    <Typography style={{color: '#4C8BF5', fontWeight: fontOptions.weight.bold}}>Duration</Typography>
                    <Typography style={{color: '#3D3D3D', fontWeight: fontOptions.weight.bold}}>{sourceData[0] && sourceData[0].duration}</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Box>

          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="25px"
            marginTop='25px'
          >
            <Typography style={{fontSize: fontOptions.size.mediumPlus}}>
              <span>
                Status
              </span>
              {(sourceData[0] && sourceData[0].sections.some(list => list.questions.length < 1)) ?
                <span style={{marginLeft: '15px', color: '#F9BD33'}}>Draft</span> :
                ((sourceData[0] && !assignedAssessments.includes(sourceData[0]._id)) ?
                  <span style={{marginLeft: '15px', color: '#F9BD33'}}>Draft</span>:
                  <span style={{marginLeft: '15px', color: '#4C8BF5'}}>Published</span>
                )
              }
            </Typography>
          </Box>

           <Box style={{float: 'right', paddingTop: '25px'}}>
           <Box marginRight="15px" className={classes.createBtn}>
                <Button
                  variant="outlined" color="primary"
                  onClick={() => handleEditDetails(sourceData[0])}
                  style={{color:'#4C8BF5'}}
                >
                  Edit
                </Button>
                <Button
                  color="primary"
                  style={{marginLeft: '15px'}}
                  variant="contained"
                  onClick={() => publishthis(sourceData[0])}
                  disabled={sourceData[0] && sourceData[0].sections.some(list => list.questions.length < 1 ||
                    sourceData[0] && sourceData[0].totalMarks > totalmark
                  )}
                >
                  {((sourceData[0] && !assignedAssessments.includes(sourceData[0]._id)) ?
                  'Publish':'Assign'
                  )}
                </Button>
            </Box>
           </Box>

          </Grid>
        </Container>
      </Grid>
      </MiniDrawer>

    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  authUser: state.authReducer.authUser as User
});

export default connect(mapStateToProps)(Assessment_status);
