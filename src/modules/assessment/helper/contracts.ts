import {Assessment} from "../contracts/assessment_interface"
import {Batch} from "../contracts/batch_interface"
import {QuestionBody} from "../contracts/qustions_interface"
import {CommonQuestionBody} from "../contracts/common_question_interface"
import {StudentInfo} from "../contracts/studentInfo_interface"
interface TopicDoc {
    _id:{
        topic:string,
        questions:string
    },
    questions:Array<QuestionBody|CommonQuestionBody>
}


type GeneralQuestion = QuestionBody | CommonQuestionBody

export interface getAssessmentsResponse {
    assessmentList:Assessment[]
}

export interface getAssessmentResponse {
    assessment:Assessment
}

export interface getTopicsResponse {
    topics:TopicDoc[]
}


export interface getQuestionsResponse {
    questions : GeneralQuestion[]
}

export interface getBatchesResponse {
    batchList:Batch[]
}

export interface getStudentsDataResponse { 
    studentInfo : StudentInfo[]
}

export interface getAssignedAssessmentsResponse{
    assessmentArr:string[]
}


export interface getCreateAssessmentCopyResponse{
    assessmentDoc: Assessment
}

export interface AssignedAssessment {
    ownerId: string,
    studentId: string,
    batchId: string,
    startTime: Date,
    endTime: Date,
    startDate: Date,
    endDate: Date,
    currentQuestion: number,
    answers: any,
    attemptedQuestions: any,
    assessment: Assessment,
    assessmentData: any,
    solutionTime: Date,
    isSubmitted: boolean,
    isStarted: boolean,
    updatedby: string,
    updatedon: Date
}