import { combineReducers, createReducer } from "@reduxjs/toolkit";
import { setAssessment, setQuestions } from "./actions";
import { RootState } from '../../../store';
import { AnyAction } from 'redux'

const ASSESSMENT_INITAL_SETP = {};
const assessmentSetp = createReducer(ASSESSMENT_INITAL_SETP, {
  [setAssessment.type]: (_, action) => action.payload,
});

const QUESTIONS = {};
const questions = createReducer(QUESTIONS, {
  [setQuestions.type]: (_, action) => action.payload,
});

const initState = {
  assessmentSetp: ASSESSMENT_INITAL_SETP,
  questions: QUESTIONS
};

const appReducer = combineReducers({
  assessmentSetp,
  questions
})

export const assessmentReducers = (state: RootState, action: AnyAction) => {
  if (action.type === 'USER_LOGOUT') {
    state = initState
  }

  return appReducer(state, action)
}
