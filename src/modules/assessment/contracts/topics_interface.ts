import { QuestionBody } from "./qustions_interface"
import {CommonQuestionBody} from "./common_question_interface"


export interface Topic {
    title:string;
    questionSet: [string,Array<QuestionBody|CommonQuestionBody>][]
}