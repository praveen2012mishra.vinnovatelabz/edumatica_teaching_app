interface imageObject {
  filename:string,
  encoding:string
};
export interface QuestionBody {
  _id:string,
  srno:string,
  boardname: string;
  classname: string;
  subjectname: string;
  chaptername: string;
  commonQuestionPart: string;
  subTopic: string ;
  nestedQuestions:QuestionBody[];
  questionDescription?: string;
  option1: string;
  option2: string;
  option3: string;
  option4: string;
  answer: string[] ;
  answerDescription: string;
  imageLinks:imageObject[];
  marks:number;
  complexity:string;
  source:"user" | "MasterQuestion";
  type:"single"|"multiple"|"numeric";
  percentageError:number;
  checksum:string,
  negativeMarking:number
  upNegmarks?: number;
  upMarks?: number;
}
