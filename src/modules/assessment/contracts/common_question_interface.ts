import {QuestionBody} from "./qustions_interface"


export interface CommonQuestionBody { 
  _id:string,
  srno:string,
  boardname: string;
  classname: string;
  subjectname: string;
  chaptername: string;
  subTopic : string ;
  commonQuestionPart: string;
  nestedQuestions:QuestionBody[]
  complexity:'comprehension';
  source:"user" | "MasterQuestion"
  checksum:string,
}
