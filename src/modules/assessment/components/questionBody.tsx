import React, { FunctionComponent, Dispatch } from "react";
import {
    Box,
    Checkbox,
    Grid,
    Typography,
    Tooltip,
    FormControl,
    Select,
    MenuItem
  } from "@material-ui/core";
import {QuestionBody} from "../contracts/qustions_interface"
import AddBoxIcon from '@material-ui/icons/AddBox';
import 'katex/dist/katex.min.css';
import {Assessment} from "../contracts/assessment_interface"
import "./katexCustom.css"
import {useSnackbar} from "notistack"
import CheckRoundedIcon from '@material-ui/icons/CheckRounded';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { fontOptions } from "../../../theme";
//@ts-ignore
import { parse, HtmlGenerator } from 'latex.js'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    latex: {
      '& p': {
        marginTop: '0px',
      },
    },
  })
);

interface Props {
    qans: QuestionBody,
    selectedQues : string[]
    setSelectedQues : Dispatch<React.SetStateAction<string[]>>;
    data:Assessment|null;
    setData:Dispatch<React.SetStateAction<Assessment|null>>;
    currentSection:number
    currentQues: string;
    setCurrentQues: React.Dispatch<React.SetStateAction<string>>;
    rowsPerPage: number;
    page: number;
    qIndex: number;
  }

  const QuestionText: FunctionComponent<Props> = ({
    qans,
    selectedQues,
    setSelectedQues,
    data,
    setData,
    currentSection,
    currentQues,
    setCurrentQues,
    rowsPerPage,
    page,
    qIndex
  }) => {

    
    const {enqueueSnackbar} = useSnackbar()
    const classes = useStyles();

    const latexParser =  (latexString:String) =>{
      let generator = new HtmlGenerator({ hyphenate: false })
      let doc = parse(latexString, { generator: generator }).htmlDocument()
      return doc.documentElement.outerHTML
    }

    return (
        <Grid container >
            <Grid item xs={1} style={{marginRight: '20px'}}>
                <Box marginTop="5px">
                    <Checkbox
                        color="primary"
                        name="questionDescription"
                        icon={<AddBoxIcon />} 
                        checkedIcon={<AddBoxIcon style={{color: '#4C8BF5'}} />}
                        checked={selectedQues.includes(qans.checksum)}
                        onClick={() => setCurrentQues(qans._id as string)}
                        onChange={(ev:React.ChangeEvent<HTMLInputElement>)=>{
                            const checked = ev.target.checked
                            if(checked){
                                const addedMarks = data?.sections.reduce((finalSection, currentSection) => {
                                    return {
                                      ...finalSection,
                                      questions: [...finalSection.questions, ...currentSection.questions]
                                    }
                                  }).questions?.findIndex((val, ind) => ind === 0) !== -1 ? data?.sections.reduce((finalSection, currentSection) => {
                                    return {
                                      ...finalSection,
                                      questions: [...finalSection.questions, ...currentSection.questions]
                                    }
                                  }).questions.reduce((finalQues, currentQues) => {
                                    return {
                                      ...finalQues,
                                      marks: Number(finalQues.marks) + Number(currentQues.marks)
                                    }
                                  }).marks : 0

                                  console.log(Number(addedMarks) + Number(qans.marks))
                                  if (Number(addedMarks) + Number(qans.marks) > Number(data?.totalMarks)) {
                                    enqueueSnackbar("Total added marks exceeding assignment marks. Please check Marks assignment section", { variant: "error" })
                                    return
                                  }
                                  else{
                                    setData((prev) => {
                                        return {
                                          ...prev,
                                          sections: prev?.sections.map((section, index) => {
                                            if (index !== currentSection) {
                                              return section
                                            }
                                            else {
                                              return {
                                                ...section,
                                                questions: [...section.questions, qans]
                                              }
                                            }
                                          })
                                        } as Assessment
                                      })
                                      setSelectedQues((prev) => {
                                        return [...prev, qans.checksum]
                                      })
                                  }

                                
                            }
                            else{
                                setData((prev) => {
                                    return {
                                      ...prev,
                                      sections: prev?.sections.map((section, index) => {
                                        return {
                                          ...section,
                                          questions:section.questions.filter(ques=>ques.checksum!==qans.checksum)
                                        }
                                      })
                                    } as Assessment
                                  })

                                  setSelectedQues((prev) => {
                                    const ind = prev.findIndex((val, ind) => { return val === qans.checksum })
                                    return prev.filter((val, index) => {
                                      return index !== ind
                                    })
                                  })
                            }
                        }}
                    />
                </Box>

            </Grid>
            <Grid item xs={10}>
                <Box

                    marginLeft="-20px"
                    marginTop="10px"
                    marginBottom="10px"
                >

                    <Typography variant="h5" >
                        <Grid container>
                          
                          <Grid item md={12} lg={12} sm={12} xs={3}>
                            <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal}}>
                              <div className={classes.latex} dangerouslySetInnerHTML={{ __html: latexParser(qans.questionDescription as string) }}/>
                            </Typography>
                          </Grid>
                        </Grid>
                        
                    </Typography>
                    {
                      qans?.imageLinks.filter((el)=>el.filename.substring(0,1)==="q").length>0 && qans.imageLinks.filter((el)=>el?.filename.substring(0,1)==="q").map((image)=>{
                      const data = image.encoding
                      return <img src={`data:image/jpeg;base64,${data}`} alt="question"/>
                    }) 
                    }
                </Box>
                <Box marginBottom="20px">
                    <Grid container>
                        {
                            qans.type !== "numeric" ?
                                <div style={{width: '100%'}}>
                                    {[qans.option1, qans.option2, qans.option3, qans.option4].filter((el)=>el.length!==0).map((option, index) => {
                                        return (
                                            <div key={index} style={{width: '100%'}}>
                                              <Grid container>
                                              <Grid item sm={1}>
                                                <Checkbox
                                                  style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal, padding: '13px 0px'}}
                                                  color="primary"
                                                  value={option}
                                                  name="option1"
                                                  checked={(qans.answer as string[]).includes(["A", "B", "C", "D"][index])}
                                                />
                                              </Grid>
                                              <Grid item sm={1}>
                                                <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal, padding: '10px 0px'}}>{['A', 'B', 'C', 'D'][index]}.</Typography>
                                              </Grid>
                                              <Grid item sm={10} >
                                                <Box display="flex" justifyContent="flex-start">
                                                  <Box>
                                                    <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal, padding: '10px 0px'}}>
                                                      <div className={classes.latex} dangerouslySetInnerHTML={{ __html: latexParser(option as string) }}/>
                                                    </Typography>
                                                  </Box>
                                                </Box>
                                                {
                                                  qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).length > 0 && qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).map((image) => {
                                                    const data = image.encoding
                                                    return <img src={`data:image/jpeg;base64,${data}`} alt="question" />
                                                  })
                                                }
                                                {/* <Box component="h3" marginTop="10px">
                                                  <Typography variant = "body1">
                                                    <span>sdfdsf</span>
                                                    <span>sdfdsf</span>
                                                  <Latex children={option} fleqn displayMode/>
                                                    {(qans.answer as string[]).includes(["A", "B", "C", "D"][index]) &&
                                                      <CheckRoundedIcon style={{paddingLeft: '3px'}}/>
                                                    }
                                                  </Typography>
                                                </Box>
                                                {
                                                  qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).length > 0 && qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).map((image) => {
                                                    const data = image.encoding
                                                    return <img src={`data:image/jpeg;base64,${data}`} alt="question" />
                                                  })
                                                } */}
                                              </Grid>
                                              </Grid>
                                                {/* <Grid item xs={3} sm={2} md={1} lg={1}>
                                                    <Checkbox

                                                        color="primary"
                                                        value={option}
                                                        name="option1"
                                                        checked={(qans.answer as string[]).includes(["A", "B", "C", "D"][index])}
                                                    />
                                                </Grid>
                                                <Grid item xs={11} sm={11} md={11} lg={11}>
                                                    <Box component="h3" marginTop="10px">
                                                      <Typography variant = "body1" display="block">
                                                      <Latex children={option} fleqn displayMode/>
                                                      {(qans.answer as string[]).includes(["A", "B", "C", "D"][index]) &&
                                                        <CheckRoundedIcon style={{paddingLeft: '3px'}}/>
                                                      }
                                                      </Typography>
                                                        
                                                    </Box>
                                              {
                                                qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).length > 0 && qans.imageLinks.filter((el) => el.filename.substring(0, 1) === (index+1).toString()).map((image) => {
                                                  const data = image.encoding
                                                  return <img src={`data:image/jpeg;base64,${data}`} alt="question" />
                                                })
                                              }
                                                </Grid> */}
                                            </div>
                                        )
                                    })}
                                </div> :
                                <React.Fragment>
                                    <Grid item xs={4} sm={2} md={2} lg={1}>
                                        <Typography
                                            variant="body1"
                                        >
                                            Ans. -
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={7} sm={4} md={2} lg={2}>
                                        <Typography
                                            variant="body1">

                                            {Number(qans.answer[0]).toFixed(2)}

                                        </Typography>
                                    </Grid>

                                    <Grid item xs={11} sm={6} md={3} lg={3}>
                                        <Typography
                                            variant="body1">
                                            Allowed Ranges     =
                                      </Typography>
                                    </Grid>

                                    <Grid item xs={4} sm={2} md={1} lg={1}>
                                        <Typography
                                            variant="body1">
                                            Min :
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={7} sm={4} md={2} lg={2}>
                                        <Tooltip title="Change percentage error value to change values">
                                            <Typography
                                                variant="body1">

                                                {Number(Number(qans.answer[0]) * (1 - (qans.percentageError / 100))).toFixed(2)}

                                            </Typography>
                                        </Tooltip>
                                    </Grid>

                                    <Grid item xs={4} sm={2} md={1} lg={1}>
                                        <Typography
                                            variant="body1">
                                            Max:
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={7} sm={4} md={2} lg={2}>
                                        <Tooltip
                                            title="Change percentage error value to change values">
                                            <Typography
                                                variant="body1">

                                                {Number(Number(qans.answer[0]) * (1 + (qans.percentageError / 100))).toFixed(2)}

                                            </Typography>
                                        </Tooltip>
                                    </Grid>
                                </React.Fragment>
                        }
                    </Grid>
                </Box>
                <Box

                marginLeft="-20px"
                marginTop="10px"
                marginBottom="10px"
            >
                <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal}}>
                  <div className={classes.latex} dangerouslySetInnerHTML={{ __html: latexParser(qans.answerDescription as string) }}/>
                </Typography>
                {
                  qans.imageLinks.filter((el)=>el.filename.substring(0,1)==="s").length>0 && qans.imageLinks.filter((el)=>el.filename.substring(0,1)==="s").map((image)=>{
                  const data = image.encoding
                  return <img src={`data:image/jpeg;base64,${data}`} alt="question" />
                }) 
                }
            </Box>                
            </Grid>

            <Grid item xs={12} style={{marginTop: '10px'}}>
              <Box>
                <Accordion style={{border: '2px solid #4C8BF5'}} 
                  onClick={() => {
                    if(!data?.sections[currentSection].questions.some(list => list._id === qans._id)) {
                      enqueueSnackbar("Question doesn't belong to this Section", { variant: "warning" })
                    }
                  }}
                  onChange={() => {(currentQues !== qans._id) ? setCurrentQues(qans._id as string) : setCurrentQues('')}} 
                  expanded={currentQues === (qans._id as string) && (data?.sections[currentSection].questions.some(list => list._id === qans._id))}
                >
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon style={{color: '#4C8BF5'}} />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography style={{color: '#4C8BF5'}}>Marking Conditions</Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12} md={6} >
                        <Grid container>
                          <Grid item xs={12} md={4}>
                            <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                              <Box>Negative Marks</Box>
                            </FormControl>
                          </Grid>

                          <Grid item xs={12} md={8}>
                            <FormControl style={{width: '75%'}} margin="normal">
                            <Select
                            value={Number(data?.sections[currentSection].questions.find(list => list._id === qans._id)?.negativeMarking)}
                            type="number"
                            onChange={(
                              e: React.ChangeEvent<{
                                value: unknown;
                              }>
                            ) => {
                              const val = Number(e.target.value);
                              setData((prev) => {
                                return {
                                  ...prev,
                                  sections: prev?.sections.map(
                                    (section, index) => {
                                      if (
                                        index !== currentSection
                                      ) {
                                        return section;
                                      } else {
                                        return {
                                          ...section,
                                          questions: section.questions.map(
                                            (ques, ind) => {
                                              if (ques._id !== qans._id) {
                                                return ques;
                                              } else {
                                                return {
                                                  ...ques,
                                                  negativeMarking: Number(val)
                                                };
                                              }
                                            }
                                          )
                                        };
                                      }
                                    }
                                  )
                                } as Assessment;
                              });
                            }}
                          >
                            <MenuItem value="0">Enter -ve Marks</MenuItem>
                            {[1, 2, 3, 4, 5, 6]
                              .filter((el) => el <= qans.marks)
                              .map((el, index) => (
                                <MenuItem key={index} value={el}>
                                  {el}
                                </MenuItem>
                              ))}
                          </Select>
                            </FormControl>
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item xs={12} md={6}>
                        <Grid container>
                          <Grid item xs={12} md={4}>
                            <FormControl style={{width: '75%', paddingTop: '7px'}} margin="normal">
                              <Box>Enter Marks</Box>
                            </FormControl>
                          </Grid>

                          <Grid item xs={12} md={8}>
                            <FormControl style={{width: '75%'}} margin="normal">
                            <Select
                                margin="none"
                                required
                                value={Number(data?.sections[currentSection].questions.find(list => {
                                  return list._id === qans._id
                                })?.marks)}
                                onChange={(
                                  e: React.ChangeEvent<{
                                    value: unknown;
                                  }>
                                ) => {
                                  const val = e.target.value as number;
                                  setData((prev) => {
                                    return {
                                      ...prev,
                                      sections: prev?.sections.map(
                                        (section, index) => {
                                          if (
                                            index !== currentSection
                                          ) {
                                            return section;
                                          } else {
                                            return {
                                              ...section,
                                              questions: section.questions.map(
                                                (ques, ind) => {
                                                  if (ques._id !== qans._id) {
                                                    return ques;
                                                  } else {
                                                    return {
                                                      ...ques,
                                                      marks: Number(val)
                                                    };
                                                  }
                                                }
                                              )
                                            };
                                          }
                                        }
                                      )
                                    } as Assessment;
                                  });
                                }}
                                displayEmpty
                              >
                                <MenuItem value="0" style={{width:'200px'}}>Enter Marks</MenuItem>
                                <MenuItem value={1} style={{width:'200px'}}>01</MenuItem>
                                <MenuItem value={2} style={{width:'200px'}}>02</MenuItem>
                                <MenuItem value={3} style={{width:'200px'}}>03</MenuItem>
                                <MenuItem value={4} style={{width:'200px'}}>04</MenuItem>
                                <MenuItem value={5} style={{width:'200px'}}>05</MenuItem>
                                <MenuItem value={6} style={{width:'200px'}}>06</MenuItem>
                              </Select>
                            </FormControl>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </Box>
            </Grid>
        </Grid>
    )
  }

  export default QuestionText
