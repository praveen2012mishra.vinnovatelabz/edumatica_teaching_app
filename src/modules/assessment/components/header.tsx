import React, { Dispatch, FunctionComponent } from 'react';
import {
  Badge,
  Box,
  Grid,
  IconButton,
  Typography,
  FormControl,
  InputLabel,
  Input,
  LinearProgress,
  Link
} from '@material-ui/core';
import {
  Add as AddIcon,
  Cancel as CancelIcon
} from "@material-ui/icons"
import ClearRoundedIcon from '@material-ui/icons/ClearRounded';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Section } from '../contracts/section_interface';
import Button from "../../common/components/form_elements/button";
import { Assessment } from '../contracts/assessment_interface';
import { useSnackbar } from 'notistack';
import { fontOptions } from '../../../theme';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },

    tmarks: {
      background: '#f0a500',
      borderRadius: 20,
      padding: '3px 10px',
      marginRight: '8px',
    },
    selecctionMarks: {
      color: 'blue',
      fontSize: fontOptions.size.medium,
      fontFamily:fontOptions.family,
      padding: '7px',
    },
    slection: {
      color: ' blue',
    },
    selectionBar: {
      background: 'white',
      borderRadius: 25,
      padding: '8px',
    },
    sideIcons3: {
      width: "35px",
      height: "35px",
      background: "linear-gradient(90deg, rgb(251, 188, 5) 2.53%, rgb(232, 172, 0) 100%)",
      borderRadius: "40px",
      marginTop: '28px',
      color: "white",
      border: "1px solid linear-gradient(90deg, rgb(251, 188, 5) 2.53%, rgb(232, 172, 0) 100%)",
    },

    navigationBar: {
      '& > * + *': {
        marginLeft: theme.spacing(1),
      },
    },
    mainWrapper: {
      marginTop: '20px',
    },
    btnSp: {
      '& button': {
        height: '50px'
      },
    },
    titleContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
  })
);



interface Props {
  data: Assessment | null;
  setCurrentSection: (number: number) => void;
  currentSection: number;
  setData: Dispatch<React.SetStateAction<Assessment | null>>;
  removeSection: (i: number) => void;
  type:boolean;
  confirm?: boolean;
  setCurrentQues: React.Dispatch<React.SetStateAction<string>>;
}

export const Header: FunctionComponent<Props> = ({
  data,
  currentSection,
  setCurrentSection,
  setData,
  removeSection,
  type,
  confirm,
  setCurrentQues
}) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const titleRef = React.useRef<HTMLInputElement | null>(null);
  const [getEditStatus, setEditStatus] = React.useState(false);

  let totalmark = 0;
  data?.sections.forEach(sec => {
    sec.questions.forEach(ques => {
      if(confirm) {
        totalmark = totalmark + ques.marks
      } else {
        totalmark = totalmark + ques.marks
      }
    })
  })

  return (
    <div style={{marginBottom: '25px'}}>
      <Grid container>
        <Grid item xs={12}>
          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="15px"
          > 
            <Grid container>
              <Grid item xs={12}>
                <div style={{ width: '100%' }}>
                  <Box display="flex">
                    <Box width="100%" >
                      <Typography style={{color: '#3D3D3D'}}>Total Marks</Typography>
                    </Box>
                    <Box flexShrink={1}>
                      <Typography style={{color: '#3D3D3D'}}>{totalmark}/{data?.totalMarks}</Typography>
                    </Box>
                  </Box>
                </div>
              </Grid>
              <Grid item xs={12} style={{marginTop: '10px'}}>
                {data &&
                  <LinearProgress variant="determinate" value={((totalmark/data?.totalMarks)*100)} />
                }
              </Grid>
            </Grid>
          </Box>
        </Grid>

        <Grid item xs={12} >
          <Box
            bgcolor="#ffffff"
            borderRadius="14px"
            padding="15px"
            marginTop="15px"
          >
            <Grid container>
              <Grid item xs={12}>
                <div style={{ width: '100%' }}>
                  <Box display="flex">
                    <Box width="100%" >
                      <Typography style={{color: '#3D3D3D'}}>Section</Typography>
                    </Box>
                    {!confirm &&
                      <Box flexShrink={1}>
                        {(data?.sections.findIndex((val, ind) => ind === 5) === -1) &&
                          <Link
                            onClick={() => {
                              setCurrentQues('')
                              setData((prev) => {
                                return {
                                  ...prev,
                                  sections: [...prev?.sections as Section[], {
                                    title: "Section " + ["A", "B", "C", "D", "E", "F"][data?.sections.length],
                                    questions: [],
                                    totalMarks: 0,
                                    duration: 0,
                                  } as Section]
                                } as Assessment
                              })
                            }}
                            style={{cursor: 'pointer'}}
                          >
                            Add
                          </Link>
                        }
                        <Link style={{marginLeft: '5px', cursor: 'pointer'}} onClick={() => setEditStatus(!getEditStatus)}>Edit</Link>
                      </Box>
                    }
                  </Box>
                </div>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  {data?.sections.map((section, index) => {
                    return (
                      <Grid item key={index} xs={12} md={6}>
                        <Box margin="10px" key={index}>
                        {!getEditStatus ? (
                          <Box className={classes.btnSp}>
                            <Button
                              style={{width: '100%', 
                                background:currentSection === index ?'#4C8BF5' :'#DADBD8',
                                color: '#FFFFFF'
                              }}
                              disableElevation
                              variant="contained"
                              onClick={() => {
                                setCurrentSection(index);
                              }}
                            >
                            <Box className={classes.titleContainer}>
                              <Box>
                                <Typography>
                                  {section.title.substr(0,12)}
                                </Typography>
                              </Box>
                              <Box marginBottom="2px">{section.questions.length > 0 && (<span style={{
                                color:currentSection === index ?'#4C8BF5' :'#DADBD8',
                                background: '#fff',
                                padding: '2px 5px',
                                borderRadius: '50%',
                              }}>
                                  {section.questions.length}
                              </span>)}
                              </Box>
                              {!confirm &&
                                <Box>
                                  {
                                    index !== 0 && <IconButton
                                      onClick={() => {
                                        removeSection(index)
                                      }}
                                      size="small"
                                      style={{ position: "absolute", bottom: '20px', right: '-10px'}}
                                    >
                                      <CancelIcon />
                                    </IconButton>
                                  }
                                </Box>
                              }
                            </Box>
                          </Button>
                          </Box>
                          ) : (<>
                            <Box key={index} style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
                              <Input
                                placeholder="Section Title"
                                id={`${index}`}
                                inputRef={titleRef}
                                style={{ width: '90px', justifyContent: 'center', display: 'flex' }}
                                disabled={section.title === undefined}
                                value={section.title === undefined ? "Section Title" : section.title}
                                onChange={(ev: React.ChangeEvent<{ value: unknown }>) => {
                                  const val = ev.target.value as string;
                                  // @ts-ignore
                                  const id = ev.target.id as string;
                                  if (val.length > 12) {
                                    enqueueSnackbar('Section Title cannot be longer than 12 characters', { variant: 'warning' })
                                    return
                                  }
                                  setData((prev) => {
                                    return {
                                      ...prev,
                                      sections: prev?.sections.map((section, index) => {
                                        if (index === parseInt(id)) {
                                          return {
                                            ...section,
                                            title: val,
                                          };
                                        } else {
                                          return section;
                                        }
                                      }),
                                    } as Assessment;
                                  });
                                }}
                                onBlur={() => {
                                  if (
                                    (data?.sections[currentSection].title.length as number) < 4
                                  ) {
                                    enqueueSnackbar(
                                      'Section Title should be atleast 4 characters long',
                                      { variant: 'error' }
                                    );
                                    titleRef.current?.focus();
                                  }
                                }}
                              />

                            </Box>
                          </>)}
                        </Box>
                      </Grid>
                    );
                  })}
                </Grid>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </div>
  )
}

export default Header;
