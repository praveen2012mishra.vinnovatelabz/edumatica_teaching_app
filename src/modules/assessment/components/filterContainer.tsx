import React, { FunctionComponent, Dispatch } from "react";
import {
  Box,
  FormControl,
  Grid,
  InputLabel,
  List,
  ListItem,
  MenuItem,
  Paper,
  Select,
  SvgIconProps,
  Typography,
  RadioGroup,
  Radio,
  FormControlLabel
} from "@material-ui/core";
import lodash from "lodash"
import {createStyles, makeStyles,Theme} from "@material-ui/core/styles"
import {Topic} from "../contracts/topics_interface"
import TreeView from '@material-ui/lab/TreeView';
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import TurnedInIcon from '@material-ui/icons/TurnedIn';
import { fontOptions } from "../../../theme";

declare module 'csstype' {
  interface Properties {
    '--tree-view-color'?: string;
    '--tree-view-bg-color'?: string;
  }
}

type StyledTreeItemProps = TreeItemProps & {
  bgColor?: string;
  color?: string;
  labelIcon: React.ElementType<SvgIconProps>;
  labelInfo?: string;
  labelText: string;
};

const useTreeItemStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: theme.palette.text.secondary,
      '&:hover > $content': {
        backgroundColor: theme.palette.action.hover,
      },
      '&:focus > $content, &$selected > $content': {
        backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
        color: 'var(--tree-view-color)',
      },
      '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
        backgroundColor: 'transparent',
      },
    },
    content: {
      color: theme.palette.text.secondary,
      borderTopRightRadius: theme.spacing(2),
      borderBottomRightRadius: theme.spacing(2),
      paddingRight: theme.spacing(1),
      fontWeight: theme.typography.fontWeightMedium,
      '$expanded > &': {
        fontWeight: theme.typography.fontWeightRegular,
      },
    },
    group: {
      marginLeft: 0,
      '& $content': {
        paddingLeft: theme.spacing(2),
      },
    },
    expanded: {},
    selected: {},
    label: {
      fontWeight: 'inherit',
      color: 'inherit',
    },
    labelRoot: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0.5, 0),
    },
    labelIcon: {
      marginRight: theme.spacing(1),
    },
    labelText: {
      fontWeight: 'inherit',
      flexGrow: 1,
    },
  }),
);

function StyledTreeItem(props: StyledTreeItemProps) {
  const classes = useTreeItemStyles();
  const { labelText, labelIcon: LabelIcon, labelInfo, color, bgColor, ...other } = props;

  return (
    <TreeItem
      label={
        <div className={classes.labelRoot}>
          <LabelIcon color="inherit" className={classes.labelIcon} />
          <Typography variant="body2" className={classes.labelText}>
            {labelText}
          </Typography>
          {labelInfo && 
            <span style={{
              borderRadius: '50%',
              width: '20px',
              height: '20px',
              display: 'block',
              float: 'left',
              border: '2px solid #fff',
              color: '#fff',
              background: '#F9BD33',
              textAlign: 'center',
              margin: '0 10px',
              fontSize: fontOptions.size.xSmall
            }}>
              {labelInfo}
            </span>
          }
        </div>
      }
      style={{
        '--tree-view-color': color,
        '--tree-view-bg-color': bgColor,
      }}
      classes={{
        root: classes.root,
        content: classes.content,
        expanded: classes.expanded,
        selected: classes.selected,
        group: classes.group,
        label: classes.label,
      }}
      {...other}
    />
  );
}




const useStyles = makeStyles((theme:Theme)=>
    createStyles({
        listSelected: {
            background: "linear-gradient(90deg, rgb(6, 88, 224) 2.53%, rgb(66, 133, 244) 100%)",
            color: "white",
            borderColor: "linear-gradient(90deg, rgb(6, 88, 224) 2.53%, rgb(66, 133, 244) 100%)",
            borderRadius: "10px"
          },
          listOption: {
            background: "#0000",
            borderColor: "#0000",
            borderRadius: "10px"
          },
    })
)



interface Props {
    complexity : string;
    setComplexity: Dispatch<React.SetStateAction<string>>;
    topics: Topic[]
    currentTopic: number;
    setCurrentTopic: Dispatch<React.SetStateAction<number>>;
    setPage:Dispatch<React.SetStateAction<number>>
    selectedQues : string[] ;
    subTopic: string ;
    setSubTopic : Dispatch<React.SetStateAction<string>>
  }

const FilterContainer: FunctionComponent<Props> = ({
    complexity,
    setComplexity,
    topics,
    currentTopic,
    setCurrentTopic,
    setPage,
    selectedQues,
    subTopic,
    setSubTopic
  }) => {
      const classes = useStyles()
      return (
        <Grid>
        <Box bgcolor="#FFFFFF">
          <Box padding="20px">
            <Typography component="span" style={{color: '#979797'}}>
              <Box fontWeight="400" margin="0" >
                Complexity
              </Box>
            </Typography>

            <FormControl component="fieldset">
              <RadioGroup value={complexity} 
                onChange={(e: React.ChangeEvent<{ value: unknown }>) =>{
                  setComplexity(e.target.value as string)
                  setPage(0)
                }}
              >
                <Grid container>
                  <Grid item xs={6}>
                    <FormControlLabel value="All" control={<Radio />} label="All" />
                  </Grid>
                  {
                    topics[currentTopic].questionSet!==undefined && topics[currentTopic].questionSet.map(el=>
                      (
                        <Grid key={el[0]} item xs={6} >
                          <FormControlLabel value={el[0]} control={<Radio />} label={lodash.startCase(el[0])} />
                        </Grid>
                      )
                    ) 
                  }
                </Grid>
              </RadioGroup>
            </FormControl>

            <Box marginTop="20px">
              <Typography component="span" color="textPrimary">
                <Box component="h4" fontWeight="500" margin="0" >
                  Topics
                </Box>
              </Typography>
              <TreeView
                style={{marginTop: '25px'}}
                defaultExpanded={['0']}
                defaultCollapseIcon={<ArrowDropDownIcon />}
                defaultExpandIcon={<ArrowRightIcon />}
                defaultEndIcon={<div style={{ width: 24 }} />}
              >
                {topics?.map((topic: Topic, i: number) => {
                  return (
                    <Box marginBottom="8px" key={i}>
                      <StyledTreeItem
                        nodeId = {i.toString()}
                        labelText = {String(i + 1) + '. ' + lodash.startCase(topic.title)}
                        labelIcon = {BookmarksIcon}
                        labelInfo = {topics[i].questionSet[0][1].map(el=>el.checksum).filter(el=>selectedQues.includes(el)).length>0?topics[i].questionSet[0][1].map(el=>el.checksum).filter(el=>selectedQues.includes(el)).length.toString():undefined}
                        onClick={() => {
                          setCurrentTopic(i)
                          setSubTopic("")
                          setComplexity(topics[i].questionSet[0][0])
                          setPage(0)
                        }} 
                        key={i}>
                          {topics[i].questionSet[0][1].map(el => el.subTopic).filter((value, index, self) => self.indexOf(value) === index).map((el,elIndex)=>{
                            return (<React.Fragment key={elIndex}>
                                <StyledTreeItem 
                              nodeId= {i.toString()+elIndex.toString()}
                              labelText = {lodash.startCase(el)} 
                              labelIcon = {TurnedInIcon}
                              labelInfo = {topics[i].questionSet[0][1].filter(elem=>elem.subTopic===el).map(el=>el.checksum).filter(el=>selectedQues.includes(el)).length>0?topics[i].questionSet[0][1].filter(elem=>elem.subTopic===el).map(el=>el.checksum).filter(el=>selectedQues.includes(el)).length.toString():undefined}
                              onClick=  {()=>{
                                setCurrentTopic(i)
                                setSubTopic(el.toLowerCase())
                                setComplexity(topics[i].questionSet[0][0])
                                setPage(0)
                              }} />
                              </React.Fragment>)
                          })}
                        </StyledTreeItem>

                        
                    </Box>
                  )
                })}
              </TreeView>

            </Box>
          </Box>

        </Box>
      </Grid>
      )
  }

export default FilterContainer