import React, { FunctionComponent, Dispatch } from "react";
import {
    Box,
    Checkbox,
    Grid,
    Typography,
    Tooltip
  } from "@material-ui/core";
import {QuestionBody} from "../contracts/qustions_interface"
import {CommonQuestionBody} from "../contracts/common_question_interface"
import 'katex/dist/katex.min.css';
import {Assessment} from "../contracts/assessment_interface"
import "./katexCustom.css"
import {useSnackbar} from "notistack"
import QuestionText from "./questionBody"
import AddBoxIcon from '@material-ui/icons/AddBox';
import { fontOptions } from "../../../theme";
//@ts-ignore
import { parse, HtmlGenerator } from 'latex.js'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    latex: {
      '& p': {
        marginTop: '0px',
      },
    },
  })
);

interface Props {
    qans: CommonQuestionBody,
    selectedQues : string[]
    setSelectedQues : Dispatch<React.SetStateAction<string[]>>;
    data:Assessment|null;
    setData:Dispatch<React.SetStateAction<Assessment|null>>;
    currentSection:number;
    currentQues: string;
    setCurrentQues: React.Dispatch<React.SetStateAction<string>>;
    rowsPerPage: number;
    page: number;
    qIndex: number;
  }

  const CommonQuestionText: FunctionComponent<Props> = ({
    qans,
    selectedQues,
    setSelectedQues,
    data,
    setData,
    currentSection,
    currentQues,
    setCurrentQues,
    rowsPerPage,
    page,
    qIndex
  }) => {

    const classes = useStyles();
    const latexParser =  (latexString:String) =>{
      let generator = new HtmlGenerator({ hyphenate: false })
      let doc = parse(latexString, { generator: generator }).htmlDocument()
      return doc.documentElement.outerHTML
    }
    const {enqueueSnackbar} = useSnackbar()

    return (
        <Grid container >
            <Grid item xs={3} sm={2} md={1} lg={1}>
                <Box marginTop="5px">
                    <Checkbox
                        color="primary"
                        name="questionDescription"
                        icon={<AddBoxIcon />} 
                        checkedIcon={<AddBoxIcon style={{color: '#4C8BF5'}} />}
                        checked={qans.nestedQuestions.map(el=>el.checksum).every(el=>selectedQues.includes(el))}
                        // indeterminate = {qans.nestedQuestions.map(el=>el.checksum).every(el=>selectedQues.includes(el))?false:qans.nestedQuestions.map(el=>el.checksum).some(el=>selectedQues.includes(el))}
                        onChange={(ev:React.ChangeEvent<HTMLInputElement>)=>{
                            const checked = ev.target.checked
                            if(checked){
                                const addedMarks = data?.sections.reduce((finalSection, currentSection) => {
                                    return {
                                      ...finalSection,
                                      questions: [...finalSection.questions, ...currentSection.questions]
                                    }
                                  }).questions?.findIndex((val, ind) => ind == 0) !== -1 ? data?.sections.reduce((finalSection, currentSection) => {
                                    return {
                                      ...finalSection,
                                      questions: [...finalSection.questions, ...currentSection.questions]
                                    }
                                  }).questions.reduce((finalQues, currentQues) => {
                                    return {
                                      ...finalQues,
                                      marks: Number(finalQues.marks) + Number(currentQues.marks)
                                    }
                                  }).marks : 0

                                  const questionMarks = qans.nestedQuestions.reduce((finalQues,currentQues)=>{
                                    return {
                                        ...finalQues,
                                        marks: Number(finalQues.marks) + Number(currentQues.marks)
                                      }
                                  }).marks
                                  console.log(Number(addedMarks) + Number(questionMarks))
                                  if (Number(addedMarks) + Number(questionMarks) > Number(data?.totalMarks)) {
                                    enqueueSnackbar("Total added marks exceeding assignment marks. Please check Marks assignment section", { variant: "error" })
                                    return
                                  }
                                  else{
                                    setData((prev) => {
                                        return {
                                          ...prev,
                                          sections: prev?.sections.map((section, index) => {
                                            if (index !== currentSection) {
                                              return section
                                            }
                                            else {
                                              return {
                                                ...section,
                                                questions: [...section.questions, ...qans.nestedQuestions]
                                              }
                                            }
                                          })
                                        } as Assessment
                                      })
                                      setSelectedQues((prev) => {
                                        return [...prev, ...qans.nestedQuestions.map(el=>el.checksum)]
                                      })
                                  }

                                
                            }
                            else{
                                setData((prev) => {
                                    return {
                                      ...prev,
                                      sections: prev?.sections.map((section, index) => {
                                        return {
                                          ...section,
                                          questions:section.questions.filter((ques)=>{
                                              return !(qans.nestedQuestions.map(el=>el.checksum).includes(ques.checksum))
                                          })
                                        }
                                      })
                                    } as Assessment
                                  })

                                  setSelectedQues((prev) => {
                                    return prev.filter((ques)=>{
                                        return  !(qans.nestedQuestions.map(el=>el.checksum).includes(ques))
                                    })
                                  })
                            }
                        }}
                    />
                </Box>

            </Grid>
            <Grid item xs={9} sm={9} md={9} lg={9}>
                <Box

                    marginLeft="-20px"
                    marginTop="10px"
                    marginBottom="10px"
                >

                    <Typography variant="h5" >
                        <Grid container>
                          <Grid item xs={12}>
                          <Typography style={{fontSize: fontOptions.size.medium, fontWeight: fontOptions.weight.normal}}>
                            <div className={classes.latex} dangerouslySetInnerHTML={{ __html: latexParser(qans.commonQuestionPart as string) }}/>
                          </Typography>
                          </Grid>
                        </Grid>
                        
                    </Typography>
                    
                </Box>    
                <Box marginBottom="20px">
                    <Grid container>
                        {
                          qans.nestedQuestions.map((nestedqans)=>{
                            return <QuestionText qans={nestedqans} selectedQues={selectedQues} setSelectedQues={setSelectedQues} data={data} setData={setData} currentSection={currentSection} currentQues={currentQues} setCurrentQues={setCurrentQues} rowsPerPage={rowsPerPage} page={page} qIndex={qIndex} />
                          })
                        }
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    )
  }

  export default CommonQuestionText
