import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './reducers';

export const store:any = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
