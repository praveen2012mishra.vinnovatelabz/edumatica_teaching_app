import axios, { AxiosError } from 'axios';
import { useDispatch } from 'react-redux';
import {
  setAuthToken,
  setAuthUser,
  setAuthUserPermissions,
  setAuthUserRole,
  setRefreshToken,
  unsetAuthUser,
  setChildrenIfParent
} from './modules/auth/store/actions';
import { getTutor } from './modules/common/api/tutor';
import { Role } from './modules/common/enums/role';
// eslint-disable-next-line
import { permissions, User, Children } from './modules/common/contracts/user';
import { exceptionTracker } from './modules/common/helpers';
import {useSnackbar} from "notistack"

export class Bootstrap {
  static init() {
    Bootstrap.configureAxios();
    Bootstrap.setupAuthToken();
  }

  private static configureAxios() {
    // Automatically append the authorization token for all the requests
    // if the user is authenticated.
    axios.interceptors.request.use(function (config) {
      const authToken = localStorage.getItem('accessToken');
      const authUserRoles:string = localStorage.getItem("authUserRoles") as string;
      const currentUser = authUserRoles ? JSON.parse(authUserRoles).ownerRoleCombi : '';

      const serverURL = new URL(process.env.REACT_APP_API as string);
      const requestURL = new URL(config.url as string);

      if (serverURL.host === requestURL.host) {
        config.headers.Authorization =
          authToken && authToken.length > 0 ? `Bearer ${authToken}` : null;
        config.headers.currentUser = currentUser
      }
      return config;
    });

    // Dispatch must be defined here to work because it can not be
    // initialized conditionally.
    const dispatch = useDispatch();
    const {enqueueSnackbar} = useSnackbar();

    axios.interceptors.response.use(
      (response) => response,
      (error: AxiosError) => {
        const serverURL = new URL(process.env.REACT_APP_API as string);
        const requestURL = new URL(error.config.url as string);

        // Expire the session for the user if the server responds with the
        // 401 status code, i.e. Unauthenticated user.
        if (
          serverURL.host === requestURL.host &&
          (error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")
        ) {
          dispatch(setAuthToken(''));
          dispatch(setRefreshToken(''))
          dispatch(setAuthUser({}));
          dispatch(setAuthUserRole(''));
          dispatch(setChildrenIfParent({}));
          dispatch(unsetAuthUser());
          localStorage.clear(); 
        } else if(serverURL.host === requestURL.host && (error.response?.status === 401) && (error.response?.data.message === "TokenExpiredError")) {
          return new Promise((resolve, reject) => {
            refreshToken(axios, error.config)
              .then((result) => {
                resolve(result);
              })
              .catch((err) => {
                reject(err);
              });
          });
        }

        if(serverURL.host === requestURL.host && error.response?.status === 422) {
          if(error.response && error.response.data.errors) {
            const errmsgObj = error.response.data.errors[0];
            const errMsg = errmsgObj[Object.keys(errmsgObj)[0]];
            if(errMsg) {
              enqueueSnackbar(errMsg, { variant: "warning" })
            } else {
              enqueueSnackbar('Some Error Occurred', { variant: "warning" })
            }
          } else {
            enqueueSnackbar(error.response?.data.message, { variant: "warning" })
          }
        }

        return Promise.reject(error);
      }
    );

    const refreshToken = (axios:any, config:any) => {
      return new Promise(async (resolve, reject) => {
        const refreshToken = String(localStorage.getItem('refreshToken'));
        const route = process.env.REACT_APP_API + '/auth/refreshtoken';
        let token;
        try {
          token = await axios.post(route, {token: refreshToken});
        } catch (error) {
          if ((error.response?.status === 401) && (error.response?.data.message === "Unauthorized")) {
            return reject(error);
          }
        }
        dispatch(setAuthToken(String(token.data.accessToken)));
        config.headers.Authorization = ((token.data.accessToken && token.data.accessToken.length > 0) ? 
          `Bearer ${String(token.data.accessToken)}` : null
        );
        axios.request(config).then((result:any) => {
          return resolve(result);
        }).catch((err:any) => {
          console.log(err);
          return reject(err);
        });
      });
    }
    
  }

  private static async setupAuthToken() {
    const accessToken = localStorage.getItem('accessToken');
    const refreshToken = localStorage.getItem('refreshToken');
    const authUser = localStorage.getItem('authUser');
    const authUserRole = localStorage.getItem('authUserRole');
    const authPermissions = localStorage.getItem('authPermissions');
    const parentChildren = localStorage.getItem('parentChildren');

    const dispatch = useDispatch();
    const {enqueueSnackbar} = useSnackbar();

    dispatch(setAuthToken(''));
    dispatch(setRefreshToken(''));
    dispatch(setAuthUser({}));
    dispatch(setAuthUserRole(''));
    dispatch(setChildrenIfParent({}));
    dispatch(
      setAuthUserPermissions({ accesspermissions: [], controlpermissions: [] })
    );
    if (!accessToken || !authUser || !authUserRole || !authPermissions || !refreshToken || !parentChildren) {
      return;
    }

    const user: User = JSON.parse(authUser);
    const children: Children = JSON.parse(parentChildren);
    const permissions: permissions = JSON.parse(authPermissions);

    dispatch(setAuthToken(accessToken));
    dispatch(setRefreshToken(refreshToken));
    dispatch(setAuthUser(user));
    dispatch(setAuthUserRole(authUserRole as Role));
    dispatch(setChildrenIfParent(children));
    dispatch(setAuthUserPermissions(permissions));
    
    if (authUserRole === Role.TUTOR || authUserRole === Role.ORG_TUTOR) {
      try {
        // const tutor = await getTutor();

        // dispatch(setAuthUser(tutor));
      } catch (error) {
        exceptionTracker(error.response?.data.message);
        // Expire the session for the user if the server responds with the
        // 401 status code, i.e. Unauthenticated user.
        if ((error.response?.status === 401) && (error.response?.data.message !== "TokenExpiredError")) {
          dispatch(setAuthToken(''));
          dispatch(setAuthUser({}));
          dispatch(setAuthUserRole(''));
          dispatch(setRefreshToken(''));
          dispatch(
            setAuthUserPermissions({
              controlpermissions: [],
              accesspermissions: []
            })
          );
          dispatch(setChildrenIfParent({}));
          dispatch(unsetAuthUser());
          localStorage.clear(); 
        }

        if(error.response?.status === 422) {
          enqueueSnackbar(error.response?.data.message, { variant: "warning" })
        }

        throw error;
      }
    }
  }
}
