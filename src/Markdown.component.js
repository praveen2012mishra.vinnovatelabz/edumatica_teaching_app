import React from 'react';
import PropTypes from 'prop-types';

import Markdown from 'markdown-to-jsx';
import renderMathInElement from 'katex/dist/contrib/auto-render';

const MarkdownViewer = ({ content }) => {
  const ref = React.useRef(null);

  React.useEffect(() => {
    if (ref.current) {
      renderMathInElement(ref.current, {
        delimiters: [
          {left: "$$", right: "$$", display: true},
          {left: "$", right: "$", display: false},
        ]
      });
    }
  }, [ref.current]);

  return (
    <div ref={ref}>
      <Markdown>{content}</Markdown>
    </div>
  );
};

MarkdownViewer.propTypes = {
  content: PropTypes.string.isRequired,
};

MarkdownViewer.defaultProps = {};

export default MarkdownViewer