import React from 'react';
import { routes } from './routes';
import { Bootstrap } from './bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import GoogleAnalytics from './google_analytics';
import { useSnackbar } from 'notistack';
//import { askForPermissioToReceiveNotifications, initializeFirebase } from './push-notification';
import { Button } from '@material-ui/core';
import EventEmitter from './EventEmitter'
function App() {
  Bootstrap.init(); // Configure the application on the first page render.
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const SnackbarAction = (key: number) => {
    return (
      <React.Fragment>
        <Button
          onClick={() => {
            closeSnackbar(key);
          }}
        >
          Confirm
        </Button>
        <Button
          onClick={() => {
            closeSnackbar(key);
          }}
        >
          Cancel
        </Button>
      </React.Fragment>
    );
  };

  EventEmitter.on("notification", function(payload:any) {
    // console.log(payload)
    enqueueSnackbar(payload.notification.body, {
      variant: 'info',action: SnackbarAction, preventDuplicate: false, persist: true,
    });
  })

  return (
    
    <Router>
      <GoogleAnalytics>
        <Switch>
          {routes.map((route) =>
            route.roles === undefined ? (
              <Route
                key={route.path}
                path={route.path}
                render={(props) => <route.component {...props} />}
              />
            ) : (
              <Route
                key={route.path}
                path={route.path}
                render={(props) => {
                  if (
                    route.roles?.includes(
                      localStorage.getItem('authUserRole') as string
                    )
                  ) {
                    return <route.component {...props} />;
                  } else {
                    enqueueSnackbar(
                      'Route not accessible , redirecting to dashboard',
                      { variant: 'warning' }
                    );
                    return (
                      <Redirect
                        to={{
                          pathname: '/profile/dashboard',
                          state: { from: props.location }
                        }}
                      />
                    );
                  }
                }}
              />
            )
          )}
        </Switch>
      </GoogleAnalytics>
    </Router>
  );
}

export default App;
