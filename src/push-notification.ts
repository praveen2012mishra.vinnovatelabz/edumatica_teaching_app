import firebase from 'firebase';
import EventEmitter from './EventEmitter';

export const initializeFirebase = () => {
  firebase.initializeApp({
    apiKey: "AIzaSyC7XsiHtySM9lbzU501ve_YynIVp3ndxhg",
    authDomain: "mu-zero-edumatica.firebaseapp.com",
    projectId: "mu-zero-edumatica",
    storageBucket: "mu-zero-edumatica.appspot.com",
    messagingSenderId: "108477188227",
    appId: "1:108477188227:web:151b94ee096883dc5d33b6",
    measurementId: "G-SLD06XSCV3" // troque pelo seu sender id 
  });
  // use other service worker
  // navigator.serviceWorker
  //   .register('/my-sw.js')
  //   .then((registration) => {
  //     firebase.messaging().useServiceWorker(registration);
  //   });
}

export const askForPermissioToReceiveNotifications = async () => {
  try {

    const messaging = firebase.messaging();

    await messaging.requestPermission();
    const token = await messaging.getToken();
    // console.log('user_token: ', token);
    firebase.messaging().onMessage((payload) => {
      // console.log(payload)
      EventEmitter.emit('notification', payload)
    }, e => {
      console.log(e)
    })
    return token;
  } catch (error) {
    console.error(error);
  }
}
