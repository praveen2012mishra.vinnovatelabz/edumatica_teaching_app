import { ReactType } from 'react';
import {
  ForgotPassword,
  Login,
  Register,
  ResetPassword,
  SetPassword,
  ConfirmPassword,
  PrivacyPolicy,
  TermsConditions,
  UserGuide,
  LegalNotice,
  ConfirmEmail,
  OtpVerification,
  RoleSelection
} from './modules/auth/containers';
import { Home } from './modules/home/containers';
import {
  Students,
  StudentUpdate,
  Tutors,
  StudentsEnrollment,
  TutorsEnrollment,
  TutorUpdate
} from './modules/enrollment/containers';
import {
  ProfileDashboard,
  ProfileNotes,
  ProfileAssessments,
  ProfileOtherInformation,
  ProfilePersonalInformation,
  ProfileProcess,
  ProfileProcessOrganization,
  ProfileProcessParent,
  ProfileProcessStudent,
  ProfileDeactivated,
  ProfileSubjectInformation,
  ProfileSecurity,
  ProfileChangePassword,
  ProfileKyc
} from './modules/dashboard/containers';
import {
  CreateBatchTutor,
  CreateBatchOrg,
  Courses,
  CreateScheduleTutor,
  CreateScheduleOrg,
  Schedules,
  Batches,
  BatchMngt
} from './modules/academics/containers';
import { CourseQuiz } from './modules/quiz/containers';
import {
  AssessmentCreate,
  AssessmentQuestions,
  AssessmentSelect,
  ConfirmAsessment,
  AssignAssessment,
  PublishedAssessment,
  StatusAssessment
} from "./modules/assessment/containers";
import { StudentTest } from "./modules/student_assessment/containers";
import StudentAssessmentInstruction  from "./modules/student_assessment/containers/student_assessment_instruction";
import ProfileImage from './modules/dashboard/containers/profile_image';
import {Upload} from "./modules/upload/containers"
import {LandingPage} from "./modules/permissionstest/containers"
import {
  ListAssignment,
  CreateAssignment,
  PublishedAssignment,
  StudentUpload,
  BatchSubmissions,
  TutorEvaluation,
  TutorCorrection,
  SubmittedAssignment,
  StudentViewMarks,
  ViewPDF,
  StudentReupload,
  StudentWait
} from './modules/assignments/containers'

// import { Role } from "./modules/common/enums/role"
import { MeetingDashboard, MeetingFeedBack, MeetingWaiting } from './modules/bbbconference/containers';
import { PushNotify, SaveTemplate, UserNotifications } from './modules/notifications/containers';
import { UserAnalytics } from './modules/analytics/containers';
import { CreateCourseBundle, EditCourseBundle, FeeStructure, PayEdumatica, BatchwiseEarnings, AdminTutorOrgEarnings, PurchaseEdumacPackage } from './modules/payments/containers';

interface RouteDesc {
  path: string;
  component: ReactType;
  exact?: boolean;
  roles? : string[] 
}

export const routes: RouteDesc[] = [
  {
    path: '/quizes',
    component: CourseQuiz
  },
  {
    path: '/profile/students/create',
    component: StudentsEnrollment
  },
  {
    path: '/profile/students/:mode',
    component: StudentUpdate
  },
  {
    path: '/profile/students',
    component: Students
  },

  {
    path: '/profile/tutors/create',
    component: TutorsEnrollment
  },
  {
    path: '/profile/tutors/:mode',
    component: TutorUpdate
  },
  {
    path: '/profile/tutors',
    component: Tutors
  },
  {
    path: '/profile/tutor/batches/create',
    component: CreateBatchTutor
  },
  {
    path: '/profile/org/batches/create',
    component: CreateBatchOrg
  },
  {
    path: '/profile/tutor/schedules/create',
    component: CreateScheduleTutor
  },
  {
    path: '/profile/org/schedules/create',
    component: CreateScheduleOrg
  },
  {
    path: '/profile/schedules',
    component: Schedules
  },
  {
    path: '/profile/process/:step',
    component: ProfileProcess
  },
  {
    path: '/profile/org/process/:step',
    component: ProfileProcessOrganization
  },
  {
    path: '/profile/student/process/:step',
    component: ProfileProcessStudent
  },
  {
    path: '/profile/parent/process/:step',
    component: ProfileProcessParent
  },
  {
    path: '/profile/deactivated',
    component: ProfileDeactivated
  },
  {
    path: '/forgot-password/reset',
    component: ResetPassword
  },
  {
    path: '/forgot-password',
    component: ForgotPassword
  },
  {
    path: '/login/chooserole',
    component: RoleSelection
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register/verifyotp',
    component: OtpVerification
  },
  {
    path: '/register/security/confirm',
    component: ConfirmPassword
  },
  {
    path: '/register/security',
    component: SetPassword
  },
  {
    path: '/privacy-policy',
    component: PrivacyPolicy
  },
  {
    path: '/terms-conditions',
    component: TermsConditions
  },
  {
    path: '/user-guide',
    component: UserGuide
  },
  {
    path: '/legal-notice',
    component: LegalNotice
  },
  {
    path: '/register/:referralCode?',
    component: Register
  },
  {
    path: '/profile/assessments',
    component: ProfileAssessments
  },
  {
    path: '/profile/dashboard',
    component: ProfileDashboard
  },
  {
    path: '/profile/notes',
    component: Courses
  },
  {
    path: '/profile/others',
    component: ProfileOtherInformation
  },
  {
    path: '/profile/personal-information',
    component: ProfilePersonalInformation
  },
  {
    path: '/profile/profile-image',
    component: ProfileImage
  },
  {
    path: '/profile/security/change-password',
    component: ProfileChangePassword
  },
  {
    path: '/profile/security',
    component: ProfileSecurity,
    exact: true
  },
  {
    path: '/profile/subjects',
    component: ProfileSubjectInformation
  },
  {
    path: '/profile/courses',
    component: Courses
  },
  {
    path: '/profile/confirm_assessment',
    component: ConfirmAsessment
  },
  {
    path: '/profile/assessment/:mode',
    component: AssessmentCreate
  },
  {
    path: '/profile/assessment_questions',
    component: AssessmentQuestions
  },
  { 
    path: '/profile/assessment_assign', 
    component: AssignAssessment 
  },
  {
    path: '/profile/assessment_status',
    component: StatusAssessment
  },
  {
    path: '/profile/assessment_published',
    component: PublishedAssessment
  },
  {
    path: '/profile/assessment',
    component: AssessmentSelect
  },
  {
    path: '/profile/kyc',
    component: ProfileKyc
  },
  {
    path: '/students/student_assessement_test',
    component: StudentTest
  },
  {
    path: '/students/student_assessement_instruction',
    component: StudentAssessmentInstruction
  },
  {
    path: '/uploadData',
    component: Upload
  },
  {
    path: '/landingpage',
    component: LandingPage
  },
  {
    path: '/profile/assignment/create',
    component: CreateAssignment
  },
  {
    path: '/profile/assignment/published',
    component: PublishedAssignment
  },
  {
    path: '/profile/assignment/submitted',
    component: SubmittedAssignment
  },
  {
    path: '/profile/assignment/studentupload',
    component: StudentUpload
  },
  {
    path: '/profile/assignment/studentreupload',
    component: StudentReupload
  },
  {
    path: '/profile/assignment/studentwait',
    component: StudentWait
  },
  {
    path: '/profile/assignment/pdfview',
    component: ViewPDF
  },
  {
    path: '/profile/assignment/studentviewmarks',
    component: StudentViewMarks
  },
  {
    path: '/profile/assignment/batches',
    component: BatchSubmissions
  },
  {
    path: '/profile/assignment/evaluate',
    component: TutorEvaluation
  },
  {
    path: '/profile/assignment/correction',
    component: TutorCorrection
  },
  {
    path: '/profile/assignment',
    component: ListAssignment
  },
  {
    path: '/profile/batches/expand',
    component: BatchMngt
  },
  {
    path: '/profile/batches',
    component: Batches
  },
  {
    path: '/meetings/dashboard',
    component: MeetingDashboard
  },
  {
    path: '/meetings/:meetingID/:uuid/:uName/:meetingName',
    component: MeetingWaiting
  },
  {
    path: '/feedback/:meetingID/:scheduleID/:meetingName',
    component: MeetingFeedBack
  },
  {
    path: '/saveTemplates',
    component: SaveTemplate
  },
  {
    path: '/pushnotify',
    component: PushNotify
  },
  {
    path: '/notifications',
    component: UserNotifications
  },
  {
    path: '/profile/analytics',
    component: UserAnalytics
  },
  {
    path: '/payments/feestructure/create',
    component: CreateCourseBundle,
    exact: true
  },
  {
    path: '/payments/feestructure/edit/:bundleId',
    component: EditCourseBundle,
    exact: true
  },
  {
    path: '/payments/feestructure',
    component: FeeStructure,
    exact: true
  },
  {
    path: '/payments/allearnings',
    component: AdminTutorOrgEarnings
  },
  {
    path: '/payments/earnings',
    component: BatchwiseEarnings
  },
  {
    path: '/payments/pay',
    component: PayEdumatica,
    exact: true
  },
  {
    path: '/payments/edumaticapackage',
    component: PurchaseEdumacPackage,
    exact: true
  },
  {
    path: '/:role/:entityId/:token',
    component: ConfirmEmail,
    exact: true
  },
  {
    path: '/',
    component: Home,
    exact: true
  }
];
