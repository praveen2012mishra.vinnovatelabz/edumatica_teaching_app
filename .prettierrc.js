module.exports = {
  singleQuote: true,
  printWidth: 80,
  trailingComma: 'none',
  '[html]': {
    'editor.defaultFormatter': 'vscode.html-language-features'
  },
  '[javascript]': {
    'editor.defaultFormatter': 'esbenp.prettier-vscode'
  }
};
