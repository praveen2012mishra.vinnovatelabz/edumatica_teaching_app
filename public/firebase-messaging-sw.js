importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyC7XsiHtySM9lbzU501ve_YynIVp3ndxhg",
    authDomain: "mu-zero-edumatica.firebaseapp.com",
    projectId: "mu-zero-edumatica",
    storageBucket: "mu-zero-edumatica.appspot.com",
    messagingSenderId: "108477188227",
    appId: "1:108477188227:web:151b94ee096883dc5d33b6",
    measurementId: "G-SLD06XSCV3" // troque pelo seu sender id 
  });

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  let options = {
    body: payload.data.body,
    icon: payload.data.icon
  }
  return self.registration.showNotification(payload.data.title, options);
});